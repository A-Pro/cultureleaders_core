<?

include_once("class.Common.php");
class Log extends Common{

	/*
	*	constructor
	*/
	function Log(){
		return true;
	}
	
	/*
	*	Add string to log file
	*	
	*	mixed log_message ( string $mod_alias, string $log_type, string $message )
	*		$mod_alias - alias for module
	*		$log_type -	type of log (WARNING, NOTICE, ERROR, etc.)
	*		$message - log message
	*	return true if all done, else error string
	*
	*/
	function log_message($mod_alias='core', $log_type='mess', $message='empty'){
		$timestamp = date("Y-m-d\tH:i:s");
		$log_str = $timestamp."\t".$mod_alias."\t".$log_type."\t".$message."\n";
		$log_file = $this->CONF["logfile"];
		$fp = fopen($log_file, 'a');
		if (fwrite($fp, $log_str) === false){
			return "Cannot write to file ($log_file)";
		}
		fclose($fp);
		return true;
	}
	
	/*
	*	Get last few strings from lof file
	*	
	*	array log_tail ( int $count )
	*		$count - number of strings
	*
	*	return array:
	*	Array
	*	(
    *	[0] => Array
    *    	(
	*           [message] => some message
    *        	[log_type] => NOTICE
    *        	[mod_alias] => news
    *        	[time] => 16:44:53
    *        	[date] => 2004-11-04
    *    	)
	*
	*    [1] => Array
    *    	(
	*           [message] => some message
    *        	[log_type] => NOTICE
    *        	[mod_alias] => news
    *        	[time] => 16:44:54
    *        	[date] => 2004-11-04
    *    	)
	*
	*    [2] => Array
    *    	(
	*           [message] => some message
    *        	[log_type] => NOTICE
    *        	[mod_alias] => news
    *        	[time] => 16:44:55
    *        	[date] => 2004-11-04
    *    	)
	*	)
	*
	*/
	function log_tail_win($count = 0){
		$log_file = $this->CONF["logfile"];
		$fp = fopen($log_file, 'r') or die('File '.$log_file.' not found');
		fseek($fp,-$count*80,SEEK_END);
		$i = 0;
		while(false !== ($tmp_str = fgets($fp))){
			if ($tmp_str != ""){
				list($tmp[$i]["date"], $tmp[$i]["time"], $tmp[$i]["mod_alias"], $tmp[$i]["log_type"], $tmp[$i]["message"]) = split("\t", str_replace("\n", "", $tmp_str));
				$i++;
			}
		}
		fclose($fp);
		return $tmp;
	}
	function log_tail_linux($count = 0){
		$log_file = $this->CONF["logfile"];
		$fp = popen("tail -n $count $log_file", 'r');
		$i = 0;
		while(!feof($fp)){
			$tmp_str = fgets($fp);
			if ($tmp_str != ""){
				list($tmp[$i]["date"], $tmp[$i]["time"], $tmp[$i]["mod_alias"], $tmp[$i]["log_type"], $tmp[$i]["message"]) = split("\t", str_replace("\n", "", $tmp_str));
				$i++;
			}
		}
		pclose($fp);
		return $tmp;
	}
}// class
?>