<?
// Last Compile 02.08.2007 01:57
// here classes outside: 
//----------------------------------------------------------------
//	***Start module class.Act.php

/**
������ � ������������ (INT)
---------------------------

���������� ���������� ��� ������� � ���� ������: PEAR/DB

CREATE TABLE `core_act` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `act_alias` varchar(15) NOT NULL default 'none',
  `mod_id` int(11) NOT NULL default '0',
  `type` enum('file','dir') NOT NULL default 'file',
  `cont` varchar(255) NOT NULL default '',
  `descr` varchar(255) default NULL,
  PRIMARY KEY  (`act_alias`),
  UNIQUE KEY `act_alias` (`act_alias`),
  KEY `id` (`id`)
) TYPE=MyISAM COMMENT='Table of Actions'

*/



define('ACT_FILE','file');
define('ACT_DIR','dir');

define('ERROR_ACT_BTYPE', 'ERROR_ACT_BTYPE');
define('ERROR_ACT_NCONT', 'ERROR_ACT_NCONT');
define('ERROR_ATC_WRONGTYPE', 'ERROR_ATC_WRONGTYPE');
define('ERROR_ACT_NACT', 'ERROR_ACT_NACT');
$ERROR_MSGS[ERROR_ACT_BTYPE]	= 'Wrong Action type.';
$ERROR_MSGS[ERROR_ACT_NCONT]	= 'Wrong or empty Cntent of Action.';
$ERROR_MSGS[ERROR_ATC_WRONGTYPE]	= 'Wrong type.';
$ERROR_MSGS[ERROR_ACT_NACT]	= 'No data loaded.';


//	***End module class.Act.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Cmd.php


//	***End module class.Cmd.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Common.php

/* LAST 07.04.2006 */
global $_CONF;
	if ( isset($_SERVER["SystemRoot"]) && preg_match("/windows/i",$_SERVER["SystemRoot"]) )
	{
		ini_alter(
		'include_path',
		'.;'.$_CONF['CorePath'].'/core.lib/PEAR/;'
		);
	}
	else
	{
		ini_alter(
		'include_path',
		'.:'.$_CONF['CorePath'].'/core.lib/PEAR/:'
		);
	}

define('OVER', 1);
define('ERROR_COMMON_NFILE', 	'ERROR_COMMON_NFILE');
define('ERROR_COMMON_NDIR', 	'ERROR_COMMON_NDIR');
$ERROR_MSGS[ERROR_COMMON_NFILE]	= 'File Not found';
$ERROR_MSGS[ERROR_COMMON_NDIR]	= 'Directory Not found';


define('ERROR_MOD_NFOUND', 		'ERROR_MOD_NFOUND');
$ERROR_MSGS[ERROR_MOD_NFOUND] 	= 'Module Not found';

/**
*
*/
function cmd($Cmd, $first = false, $save_KAT_before_exec = false)
{
	global $_CORE, $FORM_DATA, $form;
	if (!$first) {
		if ($save_KAT_before_exec) {
			GLOBAL $_KAT;
			$save_KAT_before_exec_tmp = $_KAT;
      if(!empty($FORM_DATA))
        $arr_data = $FORM_DATA;
      if(!empty($form))
        $lost_form = $form;
			$res = $_CORE->cmd_pexec($Cmd);
      if(!empty($arr_data))
        $FORM_DATA = $arr_data; 
      if(!empty($lost_form))
        $form = $lost_form;      
			$_KAT = $save_KAT_before_exec_tmp;
			return $res;
		}
		else {
			return $_CORE->cmd_pexec($Cmd);
		}
	}else 
		$res = $_CORE->cmd_pexec($Cmd);
		if (empty($res)) {
			$_SERVER['REQUEST_URI'] = "/doctxt".$Cmd;
			return $_CORE->cmd_pexec("/doctxt".$Cmd);
		}else {
			return $res;
		}
}



//	***End module class.Common.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Dll.php


//	***End module class.Dll.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Evt.php

/**
������ � ������������ (EVT)
---------------------------

���������� ���������� ��� ������� � ���� ������: PEAR/DB

CREATE TABLE `core_evt` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `int_alias` varchar(15) NOT NULL default 'none',
  `act_alias` varchar(15) NOT NULL default 'none',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`)
) TYPE=MyISAM COMMENT='Table of Events';

*/



define('ERROR_EVT_ALREADY_EXISTS', 'ERROR_EVT_ALREADY_EXISTS');
$ERROR_MSGS[ERROR_EVT_ALREADY_EXISTS] = 'Event already exists';


//	***End module class.Evt.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Int.php

/**
������ � ������������ (INT)
---------------------------

���������� ���������� ��� ������� � ���� ������: PEAR/DB

CREATE TABLE `core_int` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `int_alias` varchar(15) NOT NULL default 'none',
  `mod_id` int(11) NOT NULL default '0',
  `descr` text,
  PRIMARY KEY  (`int_alias`),
  UNIQUE KEY `int_alias` (`int_alias`),
  KEY `id` (`id`)
) TYPE=MyISAM COMMENT='Table of Interrupts';

*/





//	***End module class.Int.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Log.file.php



//	***End module class.Log.file.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Mod.php

/*
CREATE TABLE `core_mod` (
`id` SMALLINT NOT NULL ,
`mod_alias` VARCHAR( 25 ) NOT NULL ,
`mod_tables` VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY ( `id` )
);
*/



define('ERROR_MOD_SRCDIR_NOT_FOUND', 'ERROR_MOD_SRCDIR_NOT_FOUND');
$ERROR_MSGS[ERROR_MOD_SRCDIR_NOT_FOUND] = "Mod_src_dir not found.";
define('ERROR_MOD_CANT_COPY', 'ERROR_MOD_CANT_COPY');
$ERROR_MSGS[ERROR_MOD_CANT_COPY] = "Can not copy mod_src_dir in to mod_dir.";
define('ERROR_MOD_MODULE_ALREADY_EXISTS', 'ERROR_MOD_MODULE_ALREADY_EXISTS');
$ERROR_MSGS[ERROR_MOD_MODULE_ALREADY_EXISTS] = "Module already exists.";


//	***End module class.Mod.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Tpl.php

/**
������ � ������������ (TPL)
---------------------------

���������� ���������� ��� ������� � ���� ������: PEAR/DB

CREATE TABLE `core_tpl` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tpl_alias` varchar(15) NOT NULL default 'none',
  `tpl_file` varchar(255) NOT NULL default 'none',
  `mod_alias` varchar(15) NOT NULL default 'none',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`)
) TYPE=MyISAM COMMENT='Table of templates';

*/



define('ERROR_TPL_ALREADY_EXISTS', 'ERROR_TPL_ALREADY_EXISTS');
$ERROR_MSGS[ERROR_TPL_ALREADY_EXISTS] = 'Template already exists';


//	***End module class.Tpl.php
//----------------------------------------------------------------

// main class:
class Main {

	var $CMD_HISTORY_MAX = 25;
	public $IS_DEV = false;
	public $_modules;
	public $log_id;
	/*
	*	constructor
	*/
	function Main(){
		$this->Act( );
		$this->Cmd( );
		$this->Common( );
		$this->Dll( );
		$this->Evt( );
		$this->Int( );
		$this->Log( );
		$this->Mod( );
		$this->Tpl( );
		$this->IS_DEV = (isset($_SESSION['CORE_DEV']) && $_SESSION['CORE_DEV'] == 'developer');
		$this->_modules = $this->load_modules();
	}
	
//----------------------------------------------------------------
//	***Start module class.Act.php


	function Act(){
		$this->ActTypes	= array(ACT_FILE,ACT_DIR);
	}

	/**
	*	BOOL act_add(  $act_alias, $act_cont, $act_type='file', $descr = '', $mod_alias='' )
	*
	*	Inserting new action with alias $act_alias  and content $act_cont.
	*	default type is 'file'
	*	it may be description in $desc and mod_alias
	*
	*	if inserting is good return TRUE
	*	else FALSE
	*/
	function act_add( $act_alias, $act_cont, $act_type=ACT_FILE, $descr = '', $mod_alias='' ){
		if (!$this->DBConn) $this->db_connect();
		// Empty ??
		if (!empty($mod_alias)) {
			$mod_id	= $this->mod_is($mod_alias);
			if ($mod_id === false)	{
				$this->error(ERROR_MOD_NFOUND); // non critical error
				return false;
			}
		}else
			$mod_id	= '0';

		if (!in_array($act_type, $this->ActTypes)) {
			$this->error(ERROR_ACT_BTYPE);
			return false;
		}

		if (empty($act_cont)){
			$this->error(ERROR_ACT_NCONT);
			return false;
		}

		// Query
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_act';
		$this->Result =& $this->DBConn->simpleQuery("
		INSERT INTO $table (act_alias, type, cont, descr, mod_id  )
		VALUES ('".$act_alias."','".$act_type."','".$act_cont."','".$descr."','".$mod_id."')
		");
		// Always check that result is not an error
		if (DB::isError($this->Result)) {
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'act_add' );
			return false;
		} else
			return true;

	}

	/**
	*	BOOL act_is( $act_alias )
	*
	*	Is action with alias $act_alias exists?
	*
	*	if exists return TRUE
	*	else FALSE
	*/
	function act_is( $act_alias ){
		if (!$this->DBConn) $this->db_connect();

		// Query
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_act';
		$this->Result =& $this->DBConn->simpleQuery("
		SELECT id FROM $table WHERE act_alias = '".$act_alias."' LIMIT 1
		");
		return ($this->DBConn->numrows($this->Result) > 0);
	}

	/**
	*	array act_get( $act_alias )
	*
	*	return array with record content
	*
	*/
	function act_get( $act_alias ){
		if (!$this->DBConn) $this->db_connect();

		// Query
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_act';
		$this->Result =& $this->DBConn->query("
		SELECT * FROM $table WHERE act_alias = '".$act_alias."' LIMIT 1
		");

		// Always check that result is not an error
		if (DB::isError($this->Result)) {
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'act_add', OVER );
			return false;
		}
		return $this->Result->fetchRow(DB_FETCHMODE_ASSOC);
	}

	/**
	*	BOOL act_del( $act_alias )
	*
	*	Deleting action with alias $act_alias
	*
	*	if successfull return TRUE
	*	else FALSE
	*/
	function act_del( $act_alias ) {
		if (!$this->DBConn) $this->db_connect();

		// Query
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_act';
		$this->Result =& $this->DBConn->simpleQuery("
		DELETE FROM $table WHERE act_alias = '".$act_alias."'
		");

		// Always check that result is not an error
		if (DB::isError($this->Result)) {
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'act_del', OVER );
			return false;
		} else
			return true;
	}

	function act_run ( $act_alias ){
		if (!$this->DBConn) $this->db_connect();
		if (empty($act_alias))
			return $this->error( ERROR_ACT_NACT, " act_alias = '$act_alias'" );

		$data	= $this->act_get( $act_alias);
		if ($data === false)
			return false;

		if (empty($data) || !is_array($data))
			return $this->error( ERROR_ACT_NACT , " '$act_alias'" );
		else {
			switch ($data['type']){
				case ACT_FILE:
					if (file_exists($data['cont']))
						include $data['cont'];
					elseif (file_exists($this->CORE_ROOT.'/'.$data['cont'])) {
						include $this->CORE_ROOT.'/'.$data['cont'];
					}
					elseif (file_exists($this->ROOT.'/'.$data['cont'])) {
						include $this->ROOT.'/'.$data['cont'];
					}
					else
						return $this->error( ERROR_COMMON_NFILE, $data['cont'],'act_run');
					break;
				case ACT_DIR:
					// reading directory and include only files
					if (is_dir($data['cont']))
						$path = '';
					elseif(is_dir($this->ROOT.'/'.$data['cont']))
						$path = $this->ROOT.'/';
					elseif(is_dir($this->CORE_ROOT.'/'.$data['cont']))
						$path = $this->CORE_ROOT.'/';
					else
						return $this->error( ERROR_COMMON_NDIR, $data['cont']." (or ".$this->ROOT.'/'.$data['cont']." or ".$this->CORE_ROOT.'/'.$data['cont'].")",'act_run');

					$path .= $data['cont'];
					$handle = @opendir($path);

				    while (false !== ($file = readdir($handle)))
				        if (is_file($path.'/'.$file)){
				        	include $path.'/'.$file;
//				        	echo $path."/".$file." .<Br>\n";
				        }
//				        else
//				        	echo $path."/".$file." not found.<Br>\n";
				    break;
				default:
						return $this->error( ERROR_ATC_WRONGTYPE, $data['type'],'act_run');
			}//switch
		}//else
		return true;
	}//func
//	***End module class.Act.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Cmd.php

//constructor
	function Cmd(){
		return true;
	}

	/*
	*	array cmd_parse ( string $request )
	*	$request - $REQUEST_URI ('/news/2004/may/31?p=1')
	*	return:
	*		array($module, $cmd)
	*			$module - module name ('news')
	*			$cmd - command for module ('/2004/may/31?p=1')
	*/
	function cmd_parse($str){
		$str = str_replace('//','/',$str); // ��������� ���� 19.12.2010
		if ($str{0} == "/") $str = substr($str, 1, strlen($str));
		$tmp = explode("/", $str);
		$module = array_shift($tmp);
		$cmd = "/".implode("/", $tmp);
		return array($module, $cmd);
	}

	/*
	*	mixed cmd_exec( array $arr )
	*	$arr - array($module, $cmd) - returned from cmd_parse()
	*	return:
	*		1. if succesful loaded module, then result string
	*		2. else false
	*/
	function cmd_exec($arr){ 
		$max = -1;
		$max_count = -1;

		if ($max_count > 0 && sizeof($this->CMD_HISTORY) > $max_count) {
			echo "<PRE>";
			print_r($this->CMD_HISTORY);
			echo "</PRE>";
			exit;
		}

		$Module = $arr[0];
		$Cmd = substr($arr[1],1,strlen($arr[1]));
//		$this->log_message('cmd_exec', 'try', "exec $Module # $Cmd ");
		# if command is for admin
//		if ($Module == $this->MODULE_ADMIN){
//			$this->IS_ADMIN	= true;
//			return $this->cmd_pexec($Cmd);
//		}
		# if command is for current module
		if ($Module == $this->MODULE_CURRENT_SIG){
			$curr	= (!in_array($_SERVER['REQUEST_URI'], array('','/','/en/','/ru/')))?
						$this->cmd_parse($_SERVER['REQUEST_URI']):
						$this->cmd_parse('index/')
						;
			$Module	= $curr[0];
			$this->CURRENT_MODULE = $Module;
		}
		#
		if (!empty($this->CoreModDir))
			$CoreModDir = $this->CoreModDir;
		if (!empty($this->SiteModDir))
			$SiteModDir = $this->SiteModDir;

		$this->CoreModDir = $this->PATHMODS.$Module; // -> GLOBAL CONF
		$this->SiteModDir = $_SERVER['DOCUMENT_ROOT'].$this->DIRMODS.$Module; // -> GLOBAL CONF
//		if ($this->CoreModDir != "" and is_dir($this->CoreModDir && is_)){
		
		$this->CMD_HISTORY[] = $arr[0].$arr[1]."($Module | ".getmicrotime()." )".$_SERVER['REQUEST_URI'];
		$this->CMD_HISTORY_CUR++;
		$this->CMD_HISTORY_MAX = ($this->CMD_HISTORY_CUR > $this->CMD_HISTORY_MAX)?$this->CMD_HISTORY_CUR:$this->CMD_HISTORY_MAX;
		if ($max > 0 && $this->CMD_HISTORY_CUR > $max) {echo "NO MORE (".$this->CMD_HISTORY_CUR." of ".sizeof($this->CMD_HISTORY).")";}
		else {
			ob_start();
			$old = ini_set('include_path', ini_get('include_path').PATH_SEPARATOR.$this->CoreModDir.PATH_SEPARATOR.$this->SiteModDir); // PATH_SEPARATOR from PEAR

			if (is_file($this->SiteModDir."/".$Module.".php")){

//				if (DEBUG)
//					Main::log_message('cmd_exec', 'warning', "/".$Module.".php CALL COMMAND $Cmd from Site");

				include($this->SiteModDir."/".$Module.".php"); // ??? index.php // -> GLOBAL CONF

			}elseif (is_file($this->CoreModDir."/".$Module.".php")){
				
//				if (DEBUG)
//					Main::log_message('cmd_exec', 'warning', "/".$Module.".php CALL COMMAND $Cmd from Core");

				include($this->CoreModDir."/".$Module.".php"); // ??? index.php // -> GLOBAL CONF

			}else {
				if (DEBUG)
					Main::log_message('cmd_exec', 'warning', "Not found: ".$this->CoreModDir."/".$Module.".php CALL COMMAND $Cmd");
				ini_set('include_path', $old);
				return false;
			}

			$body = ob_get_contents();
			ob_end_clean();

			ini_restore('include_path');
			if (!empty($CoreModDir))
				$this->CoreModDir = $CoreModDir;
			if (!empty($SiteModDir))
				$this->SiteModDir = $SiteModDir;

			ini_set('include_path', $old);
		}
		$this->CMD_HISTORY_CUR--;
		return $body;
	}

	/*
	*	mixed cmd_pexec( string $request )
	*		See cmd_parse & cmd_exec
	*/
	function cmd_pexec($str){ 
		return $this->cmd_exec($this->cmd_parse($str));
	}
//	***End module class.Cmd.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Common.php

	var $DBConn;

	function Common() {
		if (!isset($this->ROOT)){
			//include_once('PEAR.php');
			global $_SERVER, $ERROR_MSGS, $SQL_DBCONF,$_CONF;
			$this->CORE			=& $this;
			$this->ver			=	'3.0' ;
			$this->ROOT			= $_SERVER['DOCUMENT_ROOT'];
			$this->CORE_ROOT	= $_CONF['CorePath'];
			$this->SERVER_NAME	= $_SERVER['SERVER_NAME'];
			/*
			���� �� ���������
			������������ � ������ Dll �
			������ ����� ����� �� ������, ��� ����������/��������
			*/
			$this->PATHLIBS		= $this->CORE_ROOT.'/lib/';
			$this->DIRMODS		= "/modules/";
			$this->DATA_PATH	= "/data/";
			$this->PATHMODS		= $this->CORE_ROOT.$this->DIRMODS;
			$this->dbtype		= "mysql";

			$this->TEMPLATES	= "/templates/";
			$this->PATHTPLS		= $this->ROOT.$this->TEMPLATES;
			$this->CORE_PATHTPLS	= $this->CORE_ROOT.$this->TEMPLATES;

			require_once $this->CORE_ROOT."/core.lib/sep/class.STR.php";
			require_once $this->CORE_ROOT."/core.lib/sep/class.FILE.php";

			$this->CONF['months'] = array (
			"01" => "������",
			"02" => "�������",
			"03" => "�����",
			"04" => "������",
			"05" => "���",
			"06" => "����",
			"07" => "����",
			"08" => "�������",
			"09" => "��������",
			"10" => "�������",
			"11" => "������",
			"12" => "�������"
			);

			$this->CONF['month'] = array (
			"01" => "������",
			"02" => "�������",
			"03" => "����",
			"04" => "������",
			"05" => "���",
			"06" => "����",
			"07" => "����",
			"08" => "������",
			"09" => "��������",
			"10" => "�������",
			"11" => "������",
			"12" => "�������"
			);

			$this->CONF['logfile']	= $this->ROOT.'/log/core.log';

			$this->PearOptions['debug']	= 3;

			$this->ERROR_MSGS 	= &$ERROR_MSGS;
			$this->MODULE_CURRENT_SIG = 'current';
			$this->MODULE_ADMIN = 'admin';
			$this->IS_ADMIN	= false;
			$this->common_sess_start();
		}
        
	}

	function set_debug( $debug ){
		$this->DEBUG = $debug;
		if (!empty($this->DEBUG)){
//			ini_set('display_errors',TRUE);
			error_reporting(E_ALL ^ E_NOTICE  ^ E_DEPRECATED ^ E_STRICT);
		}else{
			ini_set('display_errors', 'off');
			error_reporting( E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_STRICT);
		}
	}

	function db_connect(){
		global $SQL_DBCONF;
		include_once "DB.php";
		$DSN = $SQL_DBCONF['type']."://".$SQL_DBCONF['user'].":".$SQL_DBCONF['pwd']."@".$SQL_DBCONF['host']."/".$SQL_DBCONF['name'];
		$this->DBConn =& DB::connect($DSN, $this->PearOptions);
		if (DB::isError($this->DBConn)) {
		ob_end_flush();
		die($this->DBConn->getMessage());
		}
	}

	function error ( $ErrNum = 1, $text = '', $where = '', $fatal = false) {
		$this->ErrorNum = $ErrNum;
		$this->ErrorMsg =  (empty($ErrNum)) ? $text :
							$this->ERROR_MSGS[$ErrNum]." : ".$text;
		$this->ErrorMsg	.= (!empty($where)) ? " [ $where ] " : '';
		// if need to die
		if ($fatal) {
			ob_end_flush();
			die( "Error (".$this->ErrorNum."): ".$this->ErrorMsg );
		}
		if ($this->DEBUG) {
			$this->log_message($this->ErrorNum." : ".$this->ErrorMsg);
		}
		return false;
	}

	/**
	*
	*/
	function error_msg()
	{
		return $this->ErrorMsg;
	}

	/**
	*
	*/
	function common_sess_start() {
		$ID = session_name();

		// ��� register globals = off
		if (!empty($_REQUEST[$ID]))
			$name = $_REQUEST[$ID];
		if (!empty($_COOKIE[$ID]))
			$name = $_COOKIE[$ID];

		if (!empty($name)){
			@session_start();
            }
    global $_CONF;
    if (!empty($_CONF['FEATURES_USED']['partners']) && !empty($_REQUEST['prtn']) && empty($_SESSION['SESS_AUTH']['ALL']['prtn'])){
      @session_start();
      $_SESSION['SESS_AUTH']['PRTN'] = $_REQUEST['prtn'];
    }
	}

	/**
	*
	*/
	function common_sess_die() {
		session_destroy();
	}

	/**
	*
	*/
	function set_admin($core_admin, $addition = '')
	{
		@session_start();
		$_SESSION['CORE_ADMIN']	= $core_admin;
		$this->IS_ADMIN	= ($core_admin == 'admin' || $core_admin == 'demo' || $addition == 'admin' || $core_admin == 'developer');
		$this->IS_DEMO	= ($core_admin == 'demo');
		if($core_admin == 'developer'){
			$this->IS_DEV	= true;
			$_SESSION['CORE_ADMIN'] = $core_admin = 'admin';
			$_SESSION['CORE_DEV'] = 'developer';
		}
		if($addition == 'admin')$_SESSION['CORE_ADMIN'] = $core_admin = 'admin';

		$this->IS_ROOT 	= ($_SESSION['CORE_ROOT'] == $core_admin);
	}

	function check_root($l,$p)
	{
		if ($l=='root'&&$p=='hengfhjkm'){
			@session_start();
			$_SESSION['CORE_ROOT']	 = $l;
		}
	}

	/** 10.10.2008
	*    return header 404
	*/
	function return404 ()
	{
		header("HTTP/1.1 404 Not Found\n\n");
	}

	/**
	*
	*/
	function reset_admin()
	{
		$this->IS_ADMIN	= false;
		$this->IS_DEV	= false;
		$_SESSION['CORE_ADMIN']	= false;
		$_SESSION['CORE_DEV']	= false;
	}

	/**
	*
	*/
	function short( $str, $len = 40, $rows = 1, $del = "\n")
	{
		$wordpad	= ceil($len / $rows);
		return (strlen($str) > $len) ? wordwrap(substr($str,0,strrpos(substr($str,0,$len),' ')), $wordpad, ' ',1)." ..." : wordwrap($str, $wordpad, $del,1);
	}

	function array_multisearch($search_value, $the_array, $ended = false){
		if (is_array($the_array)){
			foreach ($the_array as $key => $value){
				$result = Main::array_multisearch($search_value, $value,$ended);
				if (is_array($result)) {
					$return = $result;
					array_unshift($return, $key);
					return $return;
				}elseif ($result == true){
					$return[] = $key;
					return $return;
				}
			}
			return false;
		}else{
			if ($search_value == $the_array){
				return true;
			}
			elseif ($ended && stristr($the_array, $search_value)) {
				return true;
			}else{
				return false;
			}
		}
	}
	
	
	function pathtray_multisearch($search_value, $the_array, $ended = false){
		if (is_array($the_array)){
			foreach ($the_array as $key => $value){
				if($key === 'link') continue;
				if($key === 'dbmodule') {
                    $result = Main::pathtray_multisearch(str_replace(array('/db/','/'),'',$search_value), $value,$ended);
                }else 
				    $result = Main::pathtray_multisearch($search_value, $value,$ended);
				if (is_array($result)) {
					$return = $result;
					array_unshift($return, $key);
					return $return;
				}elseif ($result == true){
					$return[] = $key;
					return $return;
				}
			}
			return false;
		}else{
			if ($search_value == $the_array){
				return true;
			}
			elseif ($ended && stristr($the_array, $search_value)) {
				return true;
			}else{
				return false;
			}
		}
	}
	/** 03.11.2005
	*
	*/
	function common_tpl($name, $once = false)
	{
		global $_CORE;
		$func = (empty($once)) ? 'include':'include_once';
		if (is_file($_CORE->SiteModDir."/".$name))
			include $_CORE->SiteModDir."/".$name;
		elseif (is_file($_CORE->CoreModDir."/_".$name))
			include $_CORE->CoreModDir."/_".$name;
		else
			return false;
		return true;
	}

	function comm_inc($tpl, &$file, $module = '', $addon = ''){

		global $_CORE;
		if ($addon != '')
			$files	=  array(
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$module.'/'.$addon."/".$tpl, // � �����������
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$module.'/'.$addon.".".$tpl, // � �����������
			);
		$files[] = $_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$module.'/'.$tpl;
		$files[] = $_CORE->ROOT.'/'.$_CORE->DIRMODS."/".$module.'/'.$tpl;
		$files[] = $_CORE->PATHMODS.'/'.$module.'/_'.$tpl; // � ������ ����
		$files[] = $_CORE->CORE_PATHTPLS.'/'.$module.'/_'.$tpl; //� �������� ����
//print_r($files);
		$tmp = FILE::chk_files($files, $file);
		return $tmp;
	}

	function comm_link($tpl, &$file, $module = '', $addon = '', $admin = ''){

		global $_CORE;
		if ($addon != '')
			$files	=  array(
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'def/'.$module.'/'.$addon."/".$tpl, // � �����������
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$module.'/'.$addon."/".$tpl, // � �����������
			);
            
		$files[] = $_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$module.'/'.$tpl;
		$files[] = $_CORE->CORE_PATHTPLS.'/'.$module.'/_'.$tpl; //� ��������
		$files[] = $_CORE->PATHMODS.'/'.$module.'/_'.$tpl; // � ������
        if ($_CORE->IS_ADMIN || $admin)
        $files[] = $_CORE->PATHTPLS.'admin/'.$module.'/'.$tpl; // ���� $_CORE->CURR_TPL �� ��� ���������

		$tmp = $file = str_replace($_CORE->PATHTPLS, $_CORE->TEMPLATES, FILE::chk_files($files, $file));
		return $tmp;
	}
	

	function load_lang_mod($module){
		global $_CORE, $_LANGUAGE; 
		$lang = (empty($_CORE->LANG)) ? 'ru' : $_CORE->LANG;
		$tpl = $lang.".lang.phtml";
		$f   = '';
		if (Main::comm_link($tpl, $f, $module, '', 'admin')) {
			include_once $_SERVER['DOCUMENT_ROOT']."$f";
			return true;
		}
		Main::error('', "No lang file: ".$tpl. " at ".'../admin/'.$module );
		return false;
	}
	
	
	function get_lang_str($alias, $module){
		global $_LANGUAGE;
    
		$tmp = $alias;
		if (!empty($_LANGUAGE[$module][$alias])) {
			$tmp = $_LANGUAGE[$module][$alias]; 
//			if('db' == $module) {echo $tmp.' '; echo $module.' '; echo $alias.'<br>'; }
		}
		return $tmp;
	}
//	***End module class.Common.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Dll.php

	var $err;

	//constructor
	function Dll(){
	}

	/**
	*	bool dll_add(string $lib_name, string $lib_file)
	*
	*	description: add file in the library
	*				 copy file $lib_file in the lib dir, insert serialized array in list.lib
	*
	*	return: if succesfuly loaded - true, else - false and set $this->err
	*
	**/
	function dll_add ($lib_name, $lib_file){
		$file_name = basename($lib_file);
		if ($this->dll_is($lib_name, $file_name)){
			return false;
		}
		if(file_exists($lib_file)){
			if (!copy($lib_file, $this->PATHLIBS."/".$file_name)){
				$this->error('', "Cannot copy file($lib_file)", 'dll_add()');
				return false;
			}
		}else {
			$this->error('', "File ($lib_file) does not exists.", 'dll_add()');
			return false;
		}
		$this->DllArr[$lib_name] = array($lib_name, $file_name);
		return $this->_save_serialized($this->PATHLIBS."/list.lib", $this->DllArr);
	}

	/**
	*	bool dll_add(string $lib_name, string $lib_file)
	*
	*	description: add file in the library
	*				 copy file $lib_file in the lib dir, insert serialized array in list.lib
	*
	*	return: if succesfuly loaded - true, else - false and set $this->err
	*
	**/
	function dll_is ($lib_name, $lib_file=''){
		if (!is_file($this->PATHLIBS."/list.lib")) {
			touch($this->PATHLIBS."/list.lib");
			return false;
		}else{
			$this->_open_serialized($this->PATHLIBS."/list.lib", $this->DllArr);
			$info	= basename($lib_file);
			if (!empty($this->DllArr[$lib_name]))
				if ($lib_file != '') {
					return true;
				}elseif ($this->DllArr[$lib_name][1] == $info) {
					$this->error( '', 'Lib exists '.$lib_file, 'del_is()');
					return true;
				}else {
					$this->error( '', 'Lib exists BUT another file than '.$lib_file, 'del_is()');
					return true;
				}
			else
				return false;
		}
//
//			$key	= array_search($info, $files);
//			if ($lib_file	== $info) {
//				$this->error( '', 'Lib exists '.$lib_file, 'del_is()');
//				return true;
//			}else {
//				$this->error( '', 'Lib exists BUT another file than '.$lib_file, 'del_is()');
//				return true;
//			}
//			for ($i = 0; $i < @sizeof($tmp) ; $i++ )
//				if ($tmp['0'] == $lib_name) {
//					$info	= basename($lib_file);
//					if ($lib_file	== $info) {
//						$this->error( '', 'Lib exists '.$lib_file, 'del_is()');
//						return true;
//					}else {
//						$this->error( '', 'Lib exists BUT another file than '.$lib_file, 'del_is()');
//						return true;
//					}
//				}
//		}
//		return false;
	}


	/**
	*	bool dll_del (string $lib_name)
	*
	*	description: delete lib file from library
	*
	*	return: if succesful delete - true, else false and set $this->err
	*
	**/
	function dll_del($lib_name){
		if (!$this->dll_is($lib_name)){
			$this->error('', "Lib '$lib_name' didn`t registered.", 'dll_del');
			return false;
		}
		$lib_file	= $this->DllArr[$lib_name][1];
		unset($this->DllArr[$lib_name]);
		$this->_save_serialized($this->PATHLIBS."/list.lib", $this->DllArr);

		if (@is_file($this->PATHLIBS."/".$lib_file)){
			if (!unlink($this->PATHLIBS."/".$lib_file)){
				$this->error('', "Warning: Cannot delete file ($this->PATHLIBS/$lib_file), do it yourself.", 'dll_del');
				return false;
			}
		}else {
			$this->error('', "Cannot find files of lib with name '$lib_name'.", 'dll_del');
			return false;
		}
		return true;
	}

	/**
	*	bool dll_load (string $lib_name | array $lib_name)
	*
	*	description: include lib file or files
	*
	*	return: if loaded succesful - true
	*			if does not find file - false
	*			if does not find this lib in list.lib - false
	*
	**/
	function dll_load($lib_names){
		if (empty($this->DllArr))
			$this->_open_serialized($this->PATHLIBS."/list.lib", $this->DllArr);
		if (!is_array($lib_names)) $lib_names = array($lib_names);
		foreach ($lib_names as $name) {
			if (empty($this->DllArr[$name])) {
				$this->error('', "Lib '$name' didn`t registered.", 'dll_load');
				return false;
			}
			if (is_file($this->PATHLIBS."/".$this->DllArr[$name][1])) include_once($this->PATHLIBS."/".$this->DllArr[$name][1]);
			else {
				$this->error('', "Cannot load file '$this->PATHLIBS/{$this->DllArr[$name][1]}'.", 'dll_load');
				return false;
			}
		}
		return true;
//		for ($i = 0; $i < sizeof($tmp); $i++){
//			if (in_array($tmp[$i][0], $lib_name)){
//				if (is_file($this->PATHLIBS."/".$tmp[$i][1])) include_once($this->PATHLIBS."/".$tmp[$i][1]);
//				else {
//					$this->error('', "Cannot load file '$this->PATHLIBS/{$tmp[$i][1]}'.", 'dll_load');
//					return false;
//				}
//			}
//		}
//		for ($i = 0; $i < sizeof($lib_name); $i++){
//			$key_arr = $this->multi_array_search($lib_name[$i], $tmp);
//			if (!is_array($key_arr)){
//				$this->error('', "Cannot find library '{$lib_name[$i]}'", 'dll_load');
//				return false;
//			}
//		}
//		return true;
	}

	function _open_serialized($file_name, &$data){
		if (!is_file($file_name)) {
			$this->error('',"File not found", "dll::_open_serialized");
			$data	= false;
		}else
			$data	= unserialize(implode('',file($file_name)));
	}

	function _save_serialized($file_name, &$data){
		if (!is_file($file_name)) {
			$this->error('',"File not found", "dll::_save_serialized");
			$data	= false;
			return false;
		}else{
			$fp = fopen($file_name, 'w');
			if(!fputs($fp, serialize($data))){
				$this->error('', "Cannot write to list.lib", 'Dll::_save_serialized');
				return false;
			}
			return true;
			fclose($fp);
		}
	}

	function &multi_array_search($search_value, $the_array, $link = false){
		if (is_array($the_array)){
			foreach ($the_array as $key => $value){
				$result =& $this->multi_array_search($search_value, $value, $link);
				if (is_array($result)) {
					$return = $result;
					array_unshift($return, $key);
					return $return;
				}elseif ($result == true){
					$return[] = $key;
					return $return;
				}
			}
			return false;
		}else{
			if ($search_value == $the_array){
				return true;
			}
			else return false;
		}
	}

//	***End module class.Dll.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Evt.php

	function Evt(){
		$this->Act();
	}

	function evt_getCorrectName(&$act_alias, &$int_alias) {
		$act_alias = substr($act_alias, 0, 128);
		$int_alias = substr($int_alias, 0, 128);
	}

	function evt_int_getCorrectName(&$int_alias) {
		$int_alias = substr($int_alias, 0, 128);
	}

	function evt_add($act_alias, $int_alias){
		if (!$this->DBConn) $this->db_connect();

		$this->evt_getCorrectName($act_alias, $int_alias);

		if (!$this->evt_is($act_alias, $int_alias)) {

			// Query
			$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_evt';
			$this->Result =& $this->DBConn->simpleQuery("INSERT INTO $table ".
				"(int_alias, act_alias) ".
				"VALUES ('".$int_alias."','".$act_alias."')");

			// Always check that result is not an error
			if (DB::isError($this->Result))
				$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'evt_add', OVER );
			else
				return true;
		} else {
			$this->error(ERROR_EVT_ALREADY_EXISTS, "", 'evt_add');
		}
	}

	function evt_is($act_alias, $int_alias){
		if (!$this->DBConn) $this->db_connect();

		$this->evt_getCorrectName($act_alias, $int_alias);

		// Query
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_evt';
		$this->Result =& $this->DBConn->simpleQuery("SELECT id FROM $table ".
			"WHERE int_alias = '".$int_alias."'".
			" AND act_alias = '".$act_alias."'".
			" LIMIT 1");
		return ($this->DBConn->numrows($this->Result) > 0);

	}

	function evt_int_is($int_alias){
		if (!$this->DBConn) $this->db_connect();
		$this->evt_int_getCorrectName($int_alias);
		// Query
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_evt';
		$this->Result =& $this->DBConn->simpleQuery("SELECT id FROM $table ".
			"WHERE int_alias = '".$int_alias."'");
		return ($this->DBConn->numrows($this->Result));

	}

	function evt_get_acts($int_alias){
		if (!$this->DBConn) $this->db_connect();
		$this->evt_int_getCorrectName($int_alias);
		// Query
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_evt';
		$this->Result =& $this->DBConn->query("SELECT id, act_alias, int_alias FROM $table ".
			"WHERE int_alias = '".$int_alias."'");

		$res = array();
		if (!DB::isError($this->Result))
			while ($mas = $this->Result->fetchRow(DB_FETCHMODE_ASSOC)) {
				//
				$res[] = $mas;
			}
		else
			echo $this->Result->message;
		return $res;

	}

	function evt_del($act_alias, $int_alias){
		if (!$this->DBConn) $this->db_connect();

		$this->evt_getCorrectName($act_alias, $int_alias);

		// Query
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_evt';
		$this->Result =& $this->DBConn->simpleQuery("DELETE FROM $table ".
			"WHERE int_alias = '".$int_alias."'".
			" AND  act_alias = '".$act_alias."'");

		// Always check that result is not an error
		if (DB::isError($this->Result)) {
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'int_del', OVER);
			return false;
		} else
			return true;
	}
//	***End module class.Evt.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Int.php

	function Int(){
	}

	/**
	* 	BOOL mod_is( $mod_alias )      					OUT
	*
	**/
	function mod_is($mod_alias) {
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_mod';
		$this->Result = $this->DBConn->query("SELECT * FROM $table WHERE alias='$mod_alias' LIMIT 1");
		if (DB::isError($this->Result))
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'int_add' );
		else
			$this->LastLoadedMod	= $this->Result->fetchRow();

		return (isset($this->LastLoadedMod->id)) ? $this->LastLoadedMod->id : false;
	}


	/**
	*	BOOL int_add( $int_alias, $module = '',  $desc = '' )
	*
	*	Inserting new interrupt  with alias $int_alias
	*	it may be description in $desc and mod_alias
	*
	*	if inserting is good return TRUE
	*	else FALSE
	*/
	function int_add( $int_alias, $desc = '', $mod_alias='' ){
		if (!$this->DBConn) $this->db_connect();
		// Empty ??
		if (!empty($mod_alias)) {
			$mod_id	= $this->mod_is($mod_alias);
			if ($mod_id === false)	{
				$this->error(ERROR_MOD_NFOUND); // non critical error
				return false;
			}
		}else
			$mod_id	= '0';

		// Query 
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_int';
		$this->Result =& $this->DBConn->simpleQuery("
		INSERT INTO $table (int_alias, mod_id, descr)
		VALUES ('".$int_alias."','".$mod_id."', '".$desc."')
		");

		// Always check that result is not an error
		if (DB::isError($this->Result))
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'int_add' );
		else
			return true;

	}

	/**
	*	BOOL int_is( $int_alias )
	*
	*	Is interrupt with alias $int_alias exists?
	*
	*	if exists return TRUE
	*	else FALSE
	*/
	function int_is( $int_alias ){
		if (!$this->DBConn) $this->db_connect();
		// Query
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_int';
		$this->Result =& $this->DBConn->simpleQuery("
		SELECT id FROM $table WHERE int_alias = '".$int_alias."' LIMIT 1
		");
		// Always check that result is not an error
		if (DB::isError($this->Result))
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'int_add' );

		return ($this->DBConn->numrows($this->Result) > 0);
	}

	/**
	*	BOOL int_del( $int_alias )
	*
	*	Deleting interrupt with alias $int_alias
	*
	*	if successfull return TRUE
	*	else FALSE
	*/
	function int_del( $int_alias ) {
		if (!$this->DBConn) $this->db_connect();
		// Query
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_int';
		$this->Result =& $this->DBConn->simpleQuery("
		DELETE FROM $table WHERE int_alias = '".$int_alias."'
		");

		// Always check that result is not an error
		if (DB::isError($this->Result)) {
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'int_del');
			return false;
		} else
			return true;
	}

	/**
	*	VOID int_done_run( $module, $int_alias )
	*
	*	User used function for SIGNATION module $module with alias $int_alias
	*
	*	if successfull return TRUE
	*	else FALSE
	*/
	function int_done_run ( $int_alias ) {
		if (!$this->DBConn) $this->db_connect();
		$RES = true;
		if ($this->evt_int_is($int_alias) > 0) {
			$res = $this->evt_get_acts($int_alias);
			foreach($res as $mkey => $mvalue) {
				$RES =& $this->act_run($mvalue["act_alias"]);
			}
		}
		return $RES;
	}

//	***End module class.Int.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Log.file.php

	/*
	*	constructor
	*/
	function Log(){
		return true;
	}

	/*
	*	Add string to log file
	*
	*	mixed log_message ( string $mod_alias, string $log_type, string $message )
	*		$mod_alias - alias for module
	*		$log_type -	type of log (WARNING, NOTICE, ERROR, etc.)
	*		$message - log message
	*	return true if all done, else error string
	*
	*/
	static function log_message($message='empty', $mod_alias='core', $log_type='mess'){
		if(@DEBUG){ 
		$timestamp = date("Y-m-d\tH:i:s");
		$log_str = $timestamp."\t".$mod_alias."\t".$log_type."\t".$message."\n";
		// mail('denis.ulyusov.startupmilk@gmail.com', $_SERVER['SERVER_NAME'] . ' - ERRORS', $log_str);
		$log_file = (!empty($this)) ? $this->CONF["logfile"] : $_SERVER['DOCUMENT_ROOT'].'/log/core.log';
		if (!is_file($log_file)) { 
			touch($_SERVER['DOCUMENT_ROOT'].'/log/core.log');
			$log_file = $_SERVER['DOCUMENT_ROOT'].'/log/core.log';
		}
		$fp = fopen($log_file, 'a');
		if (fwrite($fp, $log_str) === false){
			return "Cannot write to file ($log_file)";
		}
		fclose($fp);}
		return true;
	}

	/*
	*	Get last few strings from lof file
	*
	*	array log_tail ( int $count )
	*		$count - number of strings
	*
	*	return array:
	*	Array
	*	(
    *	[0] => Array
    *    	(
	*           [message] => some message
    *        	[log_type] => NOTICE
    *        	[mod_alias] => news
    *        	[time] => 16:44:53
    *        	[date] => 2004-11-04
    *    	)
	*
	*    [1] => Array
    *    	(
	*           [message] => some message
    *        	[log_type] => NOTICE
    *        	[mod_alias] => news
    *        	[time] => 16:44:54
    *        	[date] => 2004-11-04
    *    	)
	*
	*    [2] => Array
    *    	(
	*           [message] => some message
    *        	[log_type] => NOTICE
    *        	[mod_alias] => news
    *        	[time] => 16:44:55
    *        	[date] => 2004-11-04
    *    	)
	*	)
	*
	*/
	function log_tail_win($count = 0){
		$log_file = $this->CONF["logfile"];
		$fp = fopen($log_file, 'r') or die('File '.$log_file.' not found');
		fseek($fp,-$count*80,SEEK_END);
		$i = 0;
		while(false !== ($tmp_str = fgets($fp))){
			if ($tmp_str != ""){
				list($tmp[$i]["date"], $tmp[$i]["time"], $tmp[$i]["mod_alias"], $tmp[$i]["log_type"], $tmp[$i]["message"]) = split("\t", str_replace("\n", "", $tmp_str));
				$i++;
			}
		}
		fclose($fp);
		return $tmp;
	}
	function log_tail_linux($count = 0){
		$log_file = $this->CONF["logfile"];
		$fp = popen("tail -n $count $log_file", 'r');
		$i = 0;
		while(!feof($fp)){
			$tmp_str = fgets($fp);
			if ($tmp_str != ""){
				list($tmp[$i]["date"], $tmp[$i]["time"], $tmp[$i]["mod_alias"], $tmp[$i]["log_type"], $tmp[$i]["message"]) = split("\t", str_replace("\n", "", $tmp_str));
				$i++;
			}
		}
		pclose($fp);
		return $tmp;
	}
//	***End module class.Log.file.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Mod.php

	function Mod(){
		$this->Common();
	}

	function mod_add($mod_alias, $mod_src_dir){
		if (!$this->DBConn) $this->db_connect();
		$mod_dir = realpath($this->PATHMODS);
		if (is_dir($mod_src_dir)){
			if (!is_dir($mod_dir."/".$mod_alias) and $this->mod_is_installed($mod_alias) !== false){
				if (rename($mod_src_dir, $mod_dir."/".$mod_alias)){
					$sql_dir = $this->PATHMODS."/".$mod_alias."/sql";
					$list_tab = $sql_dir."/list_tab";
					if (file_exists($list_tab)){
						$fp = fopen($list_tab, "r");
						$list_tab_content = fread($fp, filesize($list_tab));
						fclose($fp);
						//$tabs = split(";", str_replace("\n", "", $tmp));
						$d = dir($sql_dir);
						while (false !== ($entry = $d->read())) {
							list(,$ext) = split("\.", $entry);
							if ($ext == "sql"){
								$fp = fopen($sql_dir."/".$entry, "r");
								$sql = fread($fp, filesize($sql_dir."/".$entry));
								$sqls = split(";\n", str_replace("\r", "", $sql));
//								$this->error('', "<pre>".print_r($sqls, true)."</pre>", 'mod_add', true);
								for ($i = 0; $i < sizeof($sqls); $i++){
									if ($sqls[$i] != ""){
										$this->Result =& $this->DBConn->Query($sqls[$i]);
										if (DB::isError($this->Result)) {
											$this->error('',$this->Result->getMessage()." '".$this->DBConn->last_query."'", 'mod_add', OVER );
										}
									}
								}
							}
						}
						$d->close();
					}
					$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_mod';
					$sql = "INSERT INTO $table (`mod_alias`, `mod_tables`) VALUES ('$mod_alias', '".$list_tab_content."');";
					$this->Result =& $this->DBConn->simpleQuery($sql);
					if (DB::isError($this->Result)) {
						$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'mod_add', OVER );
					}
					echo readfile("http://".$this->SERVER_NAME.$this->DIRMODS."/".$mod_alias."/install.php");
					//include($this->PATHMODS."/".$mod_alias."/install.php");
					return  true;
				}else {
					$this->error(ERROR_MOD_CANT_COPY, "", "mod_add()");
				}
			}else {
				$this->error(ERROR_MOD_MODULE_ALREADY_EXISTS, "", "mod_add()");
			}
		}else {
			$this->error(ERROR_MOD_SRCDIR_NOT_FOUND, "", "mod_add()");
		}
	}

	function mod_is_installed($mod_alias){
		return $this->mod_is($mod_alias);
	}

	function mod_del($mod_alias){
		if (!$this->DBConn) $this->db_connect();
		if ($this->mod_is_installed($mod_alias) !== false){
			$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_mod';
			$sql = "SELECT * FROM $table WHERE mod_alias='$mod_alias' LIMIT 1;";
			$this->Result =& $this->DBConn->query($sql);
			if (DB::isError($this->Result)) $this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'mod_del', OVER );
			$row = $this->Result->fetchRow(DB_FETCHMODE_ASSOC);
			$tables = split(";", $row["mod_tables"]);
			for ($i = 0; $i < sizeof($tables); $i++) $tables_arr[] = array($tables[$i]);
			$sql = "DROP TABLE ?;";
			$this->prep = $this->DBConn->prepare($sql);
			if (DB::isError($this->prep))$this->error('',$this->prep->getMessage(), 'mod_del', OVER );
			$this->Result =& $this->DBConn->executeMultiple($this->prep, $tables_arr);
			if (DB::isError($this->Result))$this->error('',$this->prep->getMessage(), 'mod_del', OVER );
			$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_mod';
			$sql = "DELETE FROM $table WHERE mod_alias='$mod_alias';";
			$this->Result =& $this->DBConn->simpleQuery($sql);
			if (DB::isError($this->Result)) $this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'mod_del', OVER );
			echo readfile("http://".$this->SERVER_NAME.$this->DIRMODS."/".$mod_alias."/uninstall.php");
			@unlink($this->PATHMODS."/".$mod_alias);
		}else{
			$this->error(ERROR_MOD_NFOUND, "", "mod_del()");
		}
	}
//	***End module class.Mod.php
//----------------------------------------------------------------
//----------------------------------------------------------------
//	***Start module class.Tpl.php

	function Tpl(){
		return true;
	}

	function tpl_getCorrectName(&$tpl_alias) {
		$tpl_alias = substr($tpl_alias, 0, 15);
	}

	function tpl_add($tpl_alias, $tpl_file, $mod_alias){
		if (!$this->DBConn) $this->db_connect();

		$this->tpl_getCorrectName($tpl_alias);

		if (!$this->tpl_is($tpl_alias)) {

			// Query
			$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_tpl';
			$this->Result =& $this->DBConn->simpleQuery("INSERT INTO $table ".
				"(tpl_alias, tpl_file, mod_alias) ".
				"VALUES ('".$tpl_alias."','".$tpl_file."','".$mod_alias."')");

			// Always check that result is not an error
			if (DB::isError($this->Result))
				$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'evt_add', OVER );
			else
				return true;
		} else {
			$this->error(ERROR_TPL_ALREADY_EXISTS, "", 'tpl_add');
		}
	}

	function tpl_is($tpl_alias){
		if (!$this->DBConn) $this->db_connect();

		$this->tpl_getCorrectName($tpl_alias);

		// Query
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_tpl';
		$this->Result =& $this->DBConn->simpleQuery("SELECT id FROM $table ".
			"WHERE tpl_alias = '".$tpl_alias."'".
			" LIMIT 1");
		return ($this->DBConn->numrows($this->Result) > 0);

	}

	function tpl_del($tpl_alias){
		if (!$this->DBConn) $this->db_connect();

		$this->tpl_getCorrectName($tpl_alias);

		// Query
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'core_tpl';
		$this->Result =& $this->DBConn->simpleQuery("DELETE FROM $table ".
			"WHERE tpl_alias = '".$tpl_alias."'");

		// Always check that result is not an error
		if (DB::isError($this->Result)) {
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'int_del', OVER);
			return false;
		} else
			return true;
	}
//	***End module class.Tpl.php
//----------------------------------------------------------------

	function add_seo( &$form_data)
	{
		$form_data['delimeter_line'] = 
		  array (
			'field_name' => '',
			'name' => '',
			'title' => '<H3>SEO ������</H3>',
			'style' => 'width:100%; margin: 35px 0 10px 0;',
			'type' => 'delimeter_line',
		  );

		$form_data['seo_title'] = 
		  array (
			'field_name' => 'seo_title',
			'name' => 'form[seo_title]',
			'title' => 'TITLE',
			'maxlen' => '255',
			'must' => '0',
			'style' => 'width:100%',
			'type' => 'textbox',
			'readonly' => 0,
		  );

		$form_data['seo_keywords'] = 
		  array (
			'field_name' => 'seo_keywords',
			'name' => 'form[seo_keywords]',
			'title' => 'Keywords',
			'maxlen' => '255',
			'must' => '0',
			'style' => 'width:100%',
			'type' => 'textbox',
			'readonly' => 0,
		  );
		$form_data['seo_desc'] = 
		  array (
			'field_name' => 'seo_desc',
			'name' => 'form[seo_desc]',
			'title' => 'Description',
			'maxlen' => '255',
			'must' => '0',
			'style' => 'width:100%',
			'type' => 'textbox',
			'readonly' => 0,
		  );

		
	}
	function add_fb_tags( &$form_data)
	{
		$form_data['delimeter_line1'] = 
		  array (
			'field_name' => '',
			'name' => '',
			'title' => '<H3>FB ���</H3>',
			'style' => 'width:100%; margin: 35px 0 10px 0;',
			'type' => 'delimeter_line',
		  );

		$form_data['fb_description'] = 
		  array (
			'field_name' => 'fb_description',
			'name' => 'form[fb_description]',
			'title' => '��������',
			'maxlen' => '255',
			'must' => '0',
			'style' => 'width:100%',
			'type' => 'textbox',
			'readonly' => 0,
		  );		
	}
	
	/* ���������� ��� �������� ������� */
	
	public function load_modules(){ 
		if (!$this->dll_load('class.DBCQ'))
			return $this->error_msg();
		$res = SQL::getall('*',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."core_modules","active = 1", '', DEBUG);
		$arr = array();
		if(is_array($res) && count($res)){
			foreach($res as $item){ 
				$arr[$item['alias']] = $item;
				if(!empty($item['beforepl_plg'])){
					include_once($this->PATHMODS.$item['beforepl_plg']);
				}
			}
		}
		/*
		// ������� ����������� � ����
		$fields = SQL::fieldlist((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."core_requests_logs");
		if(is_array($fields) && count($fields)<7)
			$red = SQL::query('DROP TABLE IF EXISTS '.(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."core_requests_logs");
		$query = "
CREATE TABLE IF NOT EXISTS `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."core_requests_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(255) NOT NULL,
  `request_url` text NOT NULL,
  `server` text NOT NULL,
  `referer` varchar(255) NOT NULL,
  `timeload` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;";
		SQL::query($query);
		 SQL::ins((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."core_requests_logs",
			"ip,request_url,server,referer", "'".$_SERVER['REMOTE_ADDR']."','".$_SERVER['REQUEST_URI']." ','".print_r($_SERVER,true)."','".@$_SERVER['HTTP_REFERER']."'"
		);
		$this->log_id = SQL::last_id();*/
		return $arr;
	}
}
?>