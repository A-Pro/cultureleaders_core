<?

// * config

$dir 		= ".";
$SkipFiles 	= array("make.php", "index.html", "undex.html", 
			"class.log.db.php", "class.Main.php", "class.Main.tpl");


// * constants
echo date("d.m.Y h:i")."\n\n";
define ("FILE_TPL", "class.Main.tpl");
define ("COMMENT_INCLUDES", false);
define ("STR_CONSTRUCTOR_PRE", "\t\t");
define ("STR_COMMENT_LINE", "<?//----------------------------------------------------------------\n?>");
define ("STR_START_MODULE", STR_COMMENT_LINE."<?//\t***Start module %module%\n?>");
define ("STR_END_MODULE", "<?//\t***End module %module%\n?>".STR_COMMENT_LINE);

define ("ERR_END_CLASS_BUT_NOT_CLASS", 
	"������� ���������� �������� ������������ ������");
define ("ERR_START_CLASS_BUT_NOT_END_OLD_CLASS", 
	"������� ������ �������� ������, �� ��� �� ��������� �������� �����������");
define ("ERR_TOO_MANY_CLASSES_AT_FILE", 
	"����� ������ ������ � �����");
define ("ERR_NOT_ONE_CLASS_AT_FILE", 
	"� ����� �� ���� �����");
define ("ERR_BAD_FILECLASS_NAME", 
	"�������� ������ �������� ������");
define ("ERR_CANT_FIND_TPL_FILE", 
	"�� ���� ����� ���� ������� ������");
// * variables

$globalContent 	= 
$globalOutside	= "";
$arrClasses    	= array();

$my_comment_include = (COMMENT_INCLUDES) ? "//\\1\\2(\\3);" : "";

// * functions

function error($file, $line, $error){
	
	print "Error at ".$file.", at line ".$line.": ".$error."\n";	
	die;

}


// * begin work

// ---------------------------------------------------------------------------------------
// Open a known directory, and proceed to read its contents
if (is_dir($dir) && $dh = opendir($dir)) {
	while (($file = readdir($dh)) !== false) {
        	if (!in_array($file, $SkipFiles)) {
            		list($content, $outside) = get_class_body( $file );

			// 
			if ($content) {
//			if (strlen($content) > 0) {
				preg_match("/class\.([^.]+)\.([^.]+\.)?php/i", $file, $matches);
				$classname = $matches[1];
				if (strlen($classname) == 0) {
					error($file, -1, ERR_BAD_FILECLASS_NAME);
				}
				$START_MODULE = str_replace("%module%", $file, STR_START_MODULE);
				$END_MODULE   = str_replace("%module%", $file, STR_END_MODULE);

				$globalOutside .= $START_MODULE.$outside.$END_MODULE;				
				
				$START_MODULE = str_replace("<?", "", $START_MODULE);
				$START_MODULE = str_replace("?>", "", $START_MODULE);
				$END_MODULE   = str_replace("<?", "", $END_MODULE);
				$END_MODULE   = str_replace("?>", "", $END_MODULE);

				$globalContent .= $START_MODULE.$content.$END_MODULE;				
				$arrClasses[]	= $classname;
			}
		}
	}
	closedir($dh);

	// -------------------------------------------------------------------------------

	$myConstruct = "";
	foreach ($arrClasses as $mkey => $mname) {
		$myConstruct .= STR_CONSTRUCTOR_PRE."\$this->$mname( );\n";
	}

	// -------------------------------------------------------------------------------

	if (!file_exists( FILE_TPL )) {
		error("_make_", "-1", ERR_CANT_FIND_TPL_FILE);
	}
	
	$mycontent = implode ("", file( FILE_TPL ) );

	$mycontent = str_replace("%DATE%",		  date("d.m.Y h:i"), $mycontent);
	$mycontent = str_replace("%OUTSIDE%",     $globalOutside, $mycontent);
	$mycontent = str_replace("%CONSTRUCTOR%", $myConstruct,   $mycontent);
	$mycontent = str_replace("%CONTENT%",     $globalContent, $mycontent);

	$mycontent = str_replace("?><?",          "",             $mycontent);
	$mycontent = str_replace("?>\n<?",        "",             $mycontent);

	$handle = fopen("$dir/class.Main.php", "w+");	
	fwrite($handle, $mycontent);
	fclose($handle);

	print "Create file?.. Hmm. Check it!";
}

// ---------------------------------------------------------------------------------------

/**
*	Getting filename and return array( $content, $outside )
*
* 	@content	= is between strings: class .... { and finaly "}"
*	@outside 	= other content of the files
* 
**/
function get_class_body( $file ){

	global $my_comment_include;

	if (!is_file($file))
		return array(false,false);

	$all       = file( $file );

	$flagClass = false;
	$countClass = 0;

	$outside   =
	$content   = "";
	

	foreach($all as $mline => $mstr){

		$cstr = trim($mstr);
		$mstr = rtrim($mstr);

		if ($cstr ==  "}// class") { 			// * end class

			if ($flagClass) {
				$flagClass = false;
			} else {
				error($file, $mline, ERR_END_CLASS_BUT_NOT_CLASS);
			}
		} else if ((substr($mstr, 0, 5) == "class")&&
			   (substr($cstr, -1) == "{")) {	// * start class
	
			if (!$flagClass) {
				$flagClass = true;
				if (++$countClass > 1) {
					error($file, $mline, ERR_TOO_MANY_CLASSES_AT_FILE);
				}
			}

		} else {					// * check text
			if ($flagClass) {
				$content .= $mstr."\n";
			} else {
				$mstr = preg_replace("/(include)(_once)?\((.+?)\)\;/i", $my_comment_include, $mstr);
				$outside .= $mstr."\n";				
			}
		}

//		if ($countClass != 1)
//			error($file, $mline, ERR_NOT_ONE_CLASS_AT_FILE);
	}

	return array($content, $outside);
	
}

?>