<?

class Cmd {
	
//constructor
	function Cmd(){
		return true;
	}
	
	/*
	*	array cmd_parse ( string $request )
	*	$request - $REQUEST_URI ('/news/2004/may/31?p=1')
	*	return:
	*		array($module, $cmd)
	*			$module - module name ('news')
	*			$cmd - command for module ('/2004/may/31?p=1')
	*/
	function cmd_parse($str){
		if ($str{0} == "/") $str = substr($str, 1, strlen($str));
		$tmp = explode("/", $str);
		$module = array_shift($tmp);
		$cmd = "/".implode("/", $tmp);
		return array($module, $cmd);
	}
	
	/*
	*	mixed cmd_exec( array $arr )
	*	$arr - array($module, $cmd) - returned from cmd_parse()
	*	return:
	*		1. if succesful loaded module, then result string
	*		2. else false
	*/
	function cmd_exec($arr){
		$max = -1;
		$max_count = -1;
		
		if ($max_count > 0 && sizeof($this->CMD_HISTORY) > $max_count) {
			echo "<PRE>";
			print_r($this->CMD_HISTORY);
			echo "</PRE>";
			exit;
		}
			
		$Module = $arr[0];
		$Cmd = substr($arr[1],1,strlen($arr[1]));
//		$this->log_message('cmd_exec', 'try', "exec $Module # $Cmd ");
		# if command is for admin
//		if ($Module == $this->MODULE_ADMIN){
//			$this->IS_ADMIN	= true;
//			return $this->cmd_pexec($Cmd);
//		}
		# if command is for current module
		if ($Module == $this->MODULE_CURRENT_SIG){
			$curr	= (!in_array($_SERVER['REQUEST_URI'], array('','/','/en/','/ru/')))?
						$this->cmd_parse($_SERVER['REQUEST_URI']):
						$this->cmd_parse('index/')
						;
			$Module	= $curr[0];
			$this->CURRENT_MODULE = $Module;
		}
		#
		if (!empty($this->CoreModDir)) 
			$CoreModDir = $this->CoreModDir;
		if (!empty($this->SiteModDir)) 
			$SiteModDir = $this->SiteModDir;

		$this->CoreModDir = $this->PATHMODS.$Module; // -> GLOBAL CONF
		$this->SiteModDir = $_SERVER['DOCUMENT_ROOT'].$this->DIRMODS.$Module; // -> GLOBAL CONF
//		if ($this->CoreModDir != "" and is_dir($this->CoreModDir && is_)){

		$this->CMD_HISTORY[] = $arr[0].$arr[1]."($Module)";
		$this->CMD_HISTORY_CUR++;
		$this->CMD_HISTORY_MAX = ($this->CMD_HISTORY_CUR > $this->CMD_HISTORY_MAX)?$this->CMD_HISTORY_CUR:$this->CMD_HISTORY_MAX;
		if ($max > 0 && $this->CMD_HISTORY_CUR > $max) {echo "NO MORE (".$this->CMD_HISTORY_CUR." of ".sizeof($this->CMD_HISTORY).")";}
		else {
			ob_start();
			$old = ini_set('include_path', ini_get('include_path').PATH_SEPARATOR.$this->CoreModDir.PATH_SEPARATOR.$this->SiteModDir); // PATH_SEPARATOR from PEAR
			
			if (is_file($this->SiteModDir."/".$Module.".php")){
				include($this->SiteModDir."/".$Module.".php"); // ??? index.php // -> GLOBAL CONF
	
			}elseif (is_file($this->CoreModDir."/".$Module.".php")){
				
				include($this->CoreModDir."/".$Module.".php"); // ??? index.php // -> GLOBAL CONF
				
			}else {
				if ($this->DEBUG)
					$this->log_message('cmd_exec', 'warning', "Not found: ".$this->CoreModDir."/".$Module.".php CALL COMMAND $Cmd");
				ini_set('include_path', $old);
				return false;
			}
			
			$body = ob_get_clean();
			
			ini_restore('include_path');
			if (!empty($CoreModDir)) 
				$this->CoreModDir = $CoreModDir;
			if (!empty($SiteModDir)) 
				$this->SiteModDir = $SiteModDir;
				
			ini_set('include_path', $old);
		}
		$this->CMD_HISTORY_CUR--;
		return $body;
	}
	
	/*
	*	mixed cmd_pexec( string $request )
	*		See cmd_parse & cmd_exec
	*/
	function cmd_pexec($str){
		return $this->cmd_exec($this->cmd_parse($str));	
	}
}// class
?>