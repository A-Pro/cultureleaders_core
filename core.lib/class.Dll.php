<?
include_once("class.Common.php");
class Dll extends Common {
	
	var $err;
	
	//constructor
	function Dll(){
	}
	
	/**
	*	bool dll_add(string $lib_name, string $lib_file)
	*	
	*	description: add file in the library
	*				 copy file $lib_file in the lib dir, insert serialized array in list.lib
	*	
	*	return: if succesfuly loaded - true, else - false and set $this->err
	*
	**/
	function dll_add ($lib_name, $lib_file){
		$file_name = basename($lib_file);
		if ($this->dll_is($lib_name, $file_name)){
			return false;
		}
		if(file_exists($lib_file)){
			if (!copy($lib_file, $this->PATHLIBS."/".$file_name)){
				$this->error('', "Cannot copy file($lib_file)", 'dll_add()');
				return false;
			}
		}else {
			$this->error('', "File ($lib_file) does not exists.", 'dll_add()');
			return false;
		}
		$this->DllArr[$lib_name] = array($lib_name, $file_name);
		return $this->_save_serialized($this->PATHLIBS."/list.lib", $this->DllArr);
	}

	/**
	*	bool dll_add(string $lib_name, string $lib_file)
	*	
	*	description: add file in the library
	*				 copy file $lib_file in the lib dir, insert serialized array in list.lib
	*	
	*	return: if succesfuly loaded - true, else - false and set $this->err
	*
	**/
	function dll_is ($lib_name, $lib_file=''){
		if (!is_file($this->PATHLIBS."/list.lib")) {
			touch($this->PATHLIBS."/list.lib");
			return false;
		}else{
			$this->_open_serialized($this->PATHLIBS."/list.lib", $this->DllArr);
			$info	= basename($lib_file);
			if (!empty($this->DllArr[$lib_name]))
				if ($lib_file != '') {
					return true;
				}elseif ($this->DllArr[$lib_name][1] == $info) {
					$this->error( '', 'Lib exists '.$lib_file, 'del_is()');
					return true;
				}else {
					$this->error( '', 'Lib exists BUT another file than '.$lib_file, 'del_is()');
					return true;
				}
			else 
				return false;
		}
//
//			$key	= array_search($info, $files);
//			if ($lib_file	== $info) {
//				$this->error( '', 'Lib exists '.$lib_file, 'del_is()');
//				return true;
//			}else {
//				$this->error( '', 'Lib exists BUT another file than '.$lib_file, 'del_is()');
//				return true;
//			}
//			for ($i = 0; $i < @sizeof($tmp) ; $i++ )
//				if ($tmp['0'] == $lib_name) {
//					$info	= basename($lib_file);
//					if ($lib_file	== $info) {
//						$this->error( '', 'Lib exists '.$lib_file, 'del_is()');
//						return true;
//					}else {
//						$this->error( '', 'Lib exists BUT another file than '.$lib_file, 'del_is()');
//						return true;
//					}
//				}
//		}
//		return false;
	}

	
	/**
	*	bool dll_del (string $lib_name)
	*	
	*	description: delete lib file from library
	*	
	*	return: if succesful delete - true, else false and set $this->err
	*
	**/
	function dll_del($lib_name){
		if (!$this->dll_is($lib_name)){
			$this->error('', "Lib '$lib_name' didn`t registered.", 'dll_del');
			return false;
		}
		$lib_file	= $this->DllArr[$lib_name][1];
		unset($this->DllArr[$lib_name]);
		$this->_save_serialized($this->PATHLIBS."/list.lib", $this->DllArr);

		if (@is_file($this->PATHLIBS."/".$lib_file)){
			if (!unlink($this->PATHLIBS."/".$lib_file)){
				$this->error('', "Warning: Cannot delete file ($this->PATHLIBS/$lib_file), do it yourself.", 'dll_del');
				return false;
			}
		}else {
			$this->error('', "Cannot find files of lib with name '$lib_name'.", 'dll_del');
			return false;
		}
		return true;
	}
	
	/**
	*	bool dll_load (string $lib_name | array $lib_name)
	*	
	*	description: include lib file or files
	*	
	*	return: if loaded succesful - true 
	*			if does not find file - false
	*			if does not find this lib in list.lib - false
	*
	**/
	function dll_load($lib_names){
		if (empty($this->DllArr))
			$this->_open_serialized($this->PATHLIBS."/list.lib", $this->DllArr);
		if (!is_array($lib_names)) $lib_names = array($lib_names);
		foreach ($lib_names as $name) {
			if (empty($this->DllArr[$name])) {
				$this->error('', "Lib '$name' didn`t registered.", 'dll_load');
				return false;
			}
			if (is_file($this->PATHLIBS."/".$this->DllArr[$name][1])) include_once($this->PATHLIBS."/".$this->DllArr[$name][1]);
			else {
				$this->error('', "Cannot load file '$this->PATHLIBS/{$this->DllArr[$name][1]}'.", 'dll_load');
				return false;
			}
		}
		return true;
//		for ($i = 0; $i < sizeof($tmp); $i++){
//			if (in_array($tmp[$i][0], $lib_name)){
//				if (is_file($this->PATHLIBS."/".$tmp[$i][1])) include_once($this->PATHLIBS."/".$tmp[$i][1]);
//				else {
//					$this->error('', "Cannot load file '$this->PATHLIBS/{$tmp[$i][1]}'.", 'dll_load');
//					return false;
//				}
//			}
//		}
//		for ($i = 0; $i < sizeof($lib_name); $i++){
//			$key_arr = $this->multi_array_search($lib_name[$i], $tmp);
//			if (!is_array($key_arr)){
//				$this->error('', "Cannot find library '{$lib_name[$i]}'", 'dll_load');
//				return false;
//			}
//		}
//		return true;
	}
	
	function _open_serialized($file_name, &$data){
		if (!is_file($file_name)) {
			$this->error('',"File not found", "dll::_open_serialized");
			$data	= false;
		}else
			$data	= unserialize(implode('',file($file_name)));
	}

	function _save_serialized($file_name, &$data){
		if (!is_file($file_name)) {
			$this->error('',"File not found", "dll::_save_serialized");
			$data	= false;
			return false;
		}else{
			$fp = fopen($file_name, 'w');
			if(!fputs($fp, serialize($data))){
				$this->error('', "Cannot write to list.lib", 'Dll::_save_serialized');
				return false;
			}
			return true;
			fclose($fp);
		}
	}

	function multi_array_search($search_value, $the_array){
		if (is_array($the_array)){
			foreach ($the_array as $key => $value){
				$result = $this->multi_array_search($search_value, $value);
				if (is_array($result)) {
					$return = $result;
					array_unshift($return, $key);
					return $return;
				}elseif ($result == true){
					$return[] = $key;
					return $return;
				}
			}
			return false;
		}else{
			if ($search_value == $the_array){
				return true;
			}
			else return false;
		}
	}

}// class
?>