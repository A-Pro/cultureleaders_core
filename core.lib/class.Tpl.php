<?
/**
������ � ������������ (TPL)
---------------------------

���������� ���������� ��� ������� � ���� ������: PEAR/DB

CREATE TABLE `core_tpl` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tpl_alias` varchar(15) NOT NULL default 'none',
  `tpl_file` varchar(255) NOT NULL default 'none',
  `mod_alias` varchar(15) NOT NULL default 'none',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`)
) TYPE=MyISAM COMMENT='Table of templates';
    
*/

include_once('class.Act.php');

define('ERROR_TPL_ALREADY_EXISTS', 'ERROR_TPL_ALREADY_EXISTS');
$ERROR_MSGS[ERROR_TPL_ALREADY_EXISTS] = 'Template already exists';

class Tpl extends Common {
	
	function Tpl(){
		return true;
	}

	function tpl_getCorrectName(&$tpl_alias) {
		$tpl_alias = substr($tpl_alias, 0, 15);
	}
	
	function tpl_add($tpl_alias, $tpl_file, $mod_alias){
		if (!@$this->DBConn) $this->db_connect();

		$this->tpl_getCorrectName($tpl_alias);

		if (!$this->tpl_is($tpl_alias)) {

			// Query 
			$this->Result =& $this->DBConn->simpleQuery("INSERT INTO core_tpl ".
				"(tpl_alias, tpl_file, mod_alias) ".
				"VALUES ('".$tpl_alias."','".$tpl_file."','".$mod_alias."')");
		
			// Always check that result is not an error
			if (DB::isError($this->Result)) 
				$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'evt_add', OVER );
			else 
				return true;
		} else {
			$this->error(ERROR_TPL_ALREADY_EXISTS, "", 'tpl_add');
		}		
	}
	
	function tpl_is($tpl_alias){
		if (!@$this->DBConn) $this->db_connect();

		$this->tpl_getCorrectName($tpl_alias);

		// Query 
		$this->Result =& $this->DBConn->simpleQuery("SELECT id FROM core_tpl ".
			"WHERE tpl_alias = '".$tpl_alias."'".
			" LIMIT 1");
		return ($this->DBConn->numrows($this->Result) > 0);
	
	}
	
	function tpl_del($tpl_alias){
		if (!@$this->DBConn) $this->db_connect();

		$this->tpl_getCorrectName($tpl_alias);

		// Query 
		$this->Result =& $this->DBConn->simpleQuery("DELETE FROM core_tpl ".
			"WHERE tpl_alias = '".$tpl_alias."'");

		// Always check that result is not an error
		if (DB::isError($this->Result)) {
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'int_del', OVER);
			return false;
		} else 
			return true;
	}
}// class

?>