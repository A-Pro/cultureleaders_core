<?
/**
������ � ������������ (EVT)
---------------------------

���������� ���������� ��� ������� � ���� ������: PEAR/DB

CREATE TABLE `core_evt` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `int_alias` varchar(15) NOT NULL default 'none',
  `act_alias` varchar(15) NOT NULL default 'none',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`)
) TYPE=MyISAM COMMENT='Table of Events';
    
*/

include_once('class.Act.php');

define('ERROR_EVT_ALREADY_EXISTS', 'ERROR_EVT_ALREADY_EXISTS');
$ERROR_MSGS[ERROR_EVT_ALREADY_EXISTS] = 'Event already exists';

class Evt extends Act {
	
	function Evt(){
		$this->Act();
	}

	function evt_getCorrectName(&$act_alias, &$int_alias) {
		$act_alias = substr($act_alias, 0, 15);
		$int_alias = substr($int_alias, 0, 15);
	}
	
	function evt_int_getCorrectName(&$int_alias) {
		$int_alias = substr($int_alias, 0, 15);
	}
	
	function evt_add($act_alias, $int_alias){
		if (!@$this->DBConn) $this->db_connect();

		$this->evt_getCorrectName($act_alias, $int_alias);

		if (!$this->evt_is($act_alias, $int_alias)) {

			// Query 
			$this->Result =& $this->DBConn->simpleQuery("INSERT INTO core_evt ".
				"(int_alias, act_alias) ".
				"VALUES ('".$int_alias."','".$act_alias."')");
		
			// Always check that result is not an error
			if (DB::isError($this->Result)) 
				$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'evt_add', OVER );
			else 
				return true;
		} else {
			$this->error(ERROR_EVT_ALREADY_EXISTS, "", 'evt_add');
		}		
	}
	
	function evt_is($act_alias, $int_alias){
		if (!@$this->DBConn) $this->db_connect();

		$this->evt_getCorrectName($act_alias, $int_alias);

		// Query 
		$this->Result =& $this->DBConn->simpleQuery("SELECT id FROM core_evt ".
			"WHERE int_alias = '".$int_alias."'".
			" AND act_alias = '".$act_alias."'".
			" LIMIT 1");
		return ($this->DBConn->numrows($this->Result) > 0);
	
	}
	
	function evt_int_is($int_alias){
		if (!@$this->DBConn) $this->db_connect();
		$this->evt_int_getCorrectName($int_alias);
		// Query 
		$this->Result =& $this->DBConn->simpleQuery("SELECT id FROM core_evt ".
			"WHERE int_alias = '".$int_alias."'");
		return ($this->DBConn->numrows($this->Result));
	
	}
	
	function evt_get_acts($int_alias){
		if (!@$this->DBConn) $this->db_connect();
		$this->evt_int_getCorrectName($int_alias);
		// Query 
		$this->Result =& $this->DBConn->query("SELECT id, act_alias, int_alias FROM core_evt ".
			"WHERE int_alias = '".$int_alias."'");

		$res = array();
		if (!DB::isError($this->Result))
			while ($mas = $this->Result->fetchRow(DB_FETCHMODE_ASSOC)) {
				//
				$res[] = $mas;
			}
		else 	
			echo $this->Result->message;
		return $res;
	
	}
	
	function evt_del($act_alias, $int_alias){
		if (!@$this->DBConn) $this->db_connect();

		$this->evt_getCorrectName($act_alias, $int_alias);

		// Query 
		$this->Result =& $this->DBConn->simpleQuery("DELETE FROM core_evt ".
			"WHERE int_alias = '".$int_alias."'".
			" AND  act_alias = '".$act_alias."'");

		// Always check that result is not an error
		if (DB::isError($this->Result)) {
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'int_del', OVER);
			return false;
		} else 
			return true;
	}
}// class

?>