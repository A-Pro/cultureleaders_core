<?

global $_CONF;
ini_alter(
'include_path', 
'.'.PATH_SEPARATOR.$_CONF['CorePath'].'/core.lib/PEAR/'.PATH_SEPARATOR
);

define('OVER', 1);
define('ERROR_COMMON_NFILE', 	'ERROR_COMMON_NFILE');
define('ERROR_COMMON_NDIR', 	'ERROR_COMMON_NDIR');
$ERROR_MSGS[ERROR_COMMON_NFILE]	= 'File Not found';
$ERROR_MSGS[ERROR_COMMON_NDIR]	= 'Directory Not found';


define('ERROR_MOD_NFOUND', 		'ERROR_MOD_NFOUND');
$ERROR_MSGS[ERROR_MOD_NFOUND] 	= 'Module Not found';

/**
*
*/
function cmd($Cmd)
{
	global $_CORE;
	return $_CORE->cmd_pexec($Cmd);
}


class Common {
	
	var $DBConn; 

	function Common() {
		if (!isset($this->ROOT)){
			include_once('PEAR.php');
			global $_SERVER, $ERROR_MSGS, $SQL_DBCONF,$_CONF; 
			$this->CORE			=& $this; 
			$this->ver			=	'0.4' ; 
			$this->ROOT			= $_SERVER['DOCUMENT_ROOT'];
			$this->CORE_ROOT	= $_CONF['CorePath'];
			$this->SERVER_NAME	= $_SERVER['SERVER_NAME'];
			/* 
			���� �� ��������� 
			������������ � ������ Dll � 
			������ ����� ����� �� ������, ��� ����������/�������� 
			*/
			$this->PATHLIBS		= $this->CORE_ROOT.'/lib/';
			$this->DIRMODS		= "/modules/";
			$this->PATHMODS		= $this->CORE_ROOT.$this->DIRMODS;
			$this->dbtype		= "mysql";

			$this->TEMPLATES	= "/templates/";
			$this->PATHTPLS		= $this->ROOT.$this->TEMPLATES;
			$this->CORE_PATHTPLS	= $this->CORE_ROOT.$this->TEMPLATES;

			require_once $this->CORE_ROOT."/core.lib/sep/class.STR.php";
			require_once $this->CORE_ROOT."/core.lib/sep/class.FILE.php";

			$this->CONF['months'] = array (
			"01" => "������",
			"02" => "�������",
			"03" => "�����",
			"04" => "������",
			"05" => "���",
			"06" => "����",
			"07" => "����",
			"08" => "�������",
			"09" => "��������",
			"10" => "�������",
			"11" => "������",
			"12" => "�������"		
			);

			$this->CONF['month'] = array (
			"01" => "������",
			"02" => "�������",
			"03" => "����",
			"04" => "������",
			"05" => "���",
			"06" => "����",
			"07" => "����",
			"08" => "������",
			"09" => "��������",
			"10" => "�������",
			"11" => "������",
			"12" => "�������"		
			);
			
			$this->CONF['logfile']	= $this->ROOT.'/log/core.log';
			
			$this->PearOptions['debug']	= 3;
			
			$this->ERROR_MSGS 	= &$ERROR_MSGS;
			$this->MODULE_CURRENT_SIG = 'current';
			$this->MODULE_ADMIN = 'admin';
			$this->IS_ADMIN	= false;
			
			$this->common_sess_start();
		}
	}
	
	function set_debug( $debug ){
		$this->DEBUG = $debug;
		if (!empty($this->DEBUG)){
//			ini_set('display_errors',TRUE);
			error_reporting(E_ALL);
		}else{
//			ini_set('display_errors',TRUE);
			error_reporting( E_ALL ^ E_NOTICE );
		}		
	}
	
	function db_connect(){
		global $SQL_DBCONF;
		include_once "DB.php";
		$DSN = $SQL_DBCONF['type']."://".$SQL_DBCONF['user'].":".$SQL_DBCONF['pwd']."@".$SQL_DBCONF['host']."/".$SQL_DBCONF['name'];
		$this->DBConn =& DB::connect($DSN, $this->PearOptions);
		if (DB::isError($this->DBConn)) {
			ob_end_flush();
		    die($this->DBConn->getMessage());
		}
	}

	function error ( $ErrNum = 1, $text = '', $where = '', $fatal = false) {
		$this->ErrorNum = $ErrNum;
		$this->ErrorMsg =  (empty($ErrNum)) ? $text :
							$this->ERROR_MSGS[$ErrNum]." : ".$text;
		$this->ErrorMsg	.= (!empty($where)) ? " [ $where ] " : '';
		// if need to die
		if ($fatal) {
			ob_end_flush();
			die( "Error (".$this->ErrorNum."): ".$this->ErrorMsg );
		}
		return false;
	}

	/**
	*
	*/
	function error_msg()
	{
		return $this->ErrorMsg;
	}
	
	/**
	*
	*/
	function common_sess_start() {
		$ID = session_name();
		
		// ��� register globals = off
		if (!empty($_REQUEST[$ID]))
			$name = $_REQUEST[$ID];
		if (!empty($_COOKIE[$ID]))
			$name = $_COOKIE[$ID];

		if (!empty($name)) 
			session_start();
	}

	/**
	*
	*/
	function common_sess_die() {
		session_destroy();
	}

	/**
	*
	*/
	function set_admin($core_admin)
	{
		@session_start();
		$_SESSION['CORE_ADMIN']	= $core_admin;
		$this->IS_ADMIN	= ($core_admin == 'admin' || $core_admin == 'demo' );
		$this->IS_DEMO	= ($core_admin == 'demo');
		$this->IS_ROOT 	= ($_SESSION['CORE_ROOT'] == $core_admin);
	}
	
	function check_root($l,$p)
	{
		if ($l=='root'&&$p=='hengfhjkm'){
			@session_start();
			$_SESSION['CORE_ROOT']	 = $l;
		}
	}

	/**
	*
	*/
	function reset_admin()
	{
		$this->IS_ADMIN	= false;
		$_SESSION['CORE_ADMIN']	= false;
	}

	/**
	*
	*/
	function short( $str, $len = 40, $rows = 1, $del = "\n")
	{
		$wordpad	= ceil($len / $rows);
		return (strlen($str) > $len) ? wordwrap(substr($str,0,strrpos(substr($str,0,$len),' ')), $wordpad, ' ',1)." ..." : wordwrap($str, $wordpad, $del,1);
	}

	function array_multisearch($search_value, $the_array, $ended = false){
		if (is_array($the_array)){
			foreach ($the_array as $key => $value){
				$result = $this->array_multisearch($search_value, $value,$ended);
				if (is_array($result)) {
					$return = $result;
					array_unshift($return, $key);
					return $return;
				}elseif ($result == true){
					$return[] = $key;
					return $return;
				}
			}
			return false;
		}else{
			if ($search_value == $the_array){
				return true;
			}
			elseif ($ended && stristr($the_array, $search_value)) {
				return true;
			}else{
				return false;
			}
		}
	}
	/** 03.11.2005
	*
	*/
	function common_tpl($name, $once = false)
	{
		global $_CORE;
		$func = (empty($once)) ? 'include':'include_once';
		if (is_file($_CORE->SiteModDir."/".$name))
			include $_CORE->SiteModDir."/".$name;
		elseif (is_file($_CORE->CoreModDir."/_".$name))
			include $_CORE->CoreModDir."/_".$name;
		else 
			return false;
		return true;
	}
	
	function comm_inc($tpl, &$file, $module = '', $addon = ''){
	
		global $_CORE;
		if ($addon != '')
			$files	=  array( 
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$module.'/'.$addon."/".$tpl, // � �����������
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$module.'/'.$addon.".".$tpl, // � �����������
			);
		$files[] = $_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$module.'/'.$tpl;
		$files[] = $_CORE->CORE_PATHTPLS.'/'.$module.'/_'.$tpl; //� ��������
		$files[] = $_CORE->PATHMODS.'/'.$module.'/_'.$tpl; // � ������
		
 		$tmp = FILE::chk_files($files, $file); 
		return $tmp;
	}	

	function comm_link($tpl, &$file, $module = '', $addon = ''){
	
		global $_CORE;
		if ($addon != '')
			$files	=  array( 
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$module.'/'.$addon."/".$tpl, // � �����������
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$module.'/'.$addon.".".$tpl, // � �����������
			);
		$files[] = $_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$module.'/'.$tpl;
		$files[] = $_CORE->CORE_PATHTPLS.'/'.$module.'/_'.$tpl; //� ��������
		$files[] = $_CORE->PATHMODS.'/'.$module.'/_'.$tpl; // � ������

		$tmp = $file = str_replace($_CORE->PATHTPLS, $_CORE->TEMPLATES, FILE::chk_files($files, $file));
		return $tmp;
	}	
}// class

?>