<?
/**
������ � ������������ (INT)
---------------------------

���������� ���������� ��� ������� � ���� ������: PEAR/DB

CREATE TABLE `core_int` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `int_alias` varchar(15) NOT NULL default 'none',
  `mod_id` int(11) NOT NULL default '0',
  `descr` text,
  PRIMARY KEY  (`int_alias`),
  UNIQUE KEY `int_alias` (`int_alias`),
  KEY `id` (`id`)
) TYPE=MyISAM COMMENT='Table of Interrupts';
    
*/

include( 'class.Common.php' ); 


class Int extends Common {
	
	function Int(){
	}
	
	/**
	* 	BOOL mod_is( $mod_alias )      					OUT
	*
	**/
	function mod_is($mod_alias) {
		$this->Result = $this->DBConn->query("SELECT * FROM core_mod WHERE alias='$mod_alias' LIMIT 1");
		$this->LastLoadedMod	= @$this->Result->fetchRow();
		
		return (isset($this->LastLoadedMod->id)) ? $this->LastLoadedMod->id : false;
	}
  	
	
	/**
	*	BOOL int_add( $int_alias, $module = '',  $desc = '' )
	*
	*	Inserting new interrupt  with alias $int_alias
	*	it may be description in $desc and mod_alias
	*
	*	if inserting is good return TRUE
	*	else FALSE
	*/
	function int_add( $int_alias, $desc = '', $mod_alias='' ){
		if (!@$this->DBConn) $this->db_connect();
		// Empty ??
		if (!empty($mod_alias)) {
			$mod_id	= $this->mod_is($mod_alias);
			if ($mod_id === false)	{
				$this->error(ERROR_MOD_NFOUND); // non critical error
				return false;
			}
		}else 
			$mod_id	= '0';
		
		// Query 
		$this->Result =& $this->DBConn->simpleQuery("
		INSERT INTO core_int (int_alias, mod_id) 
		VALUES ('".$int_alias."','".$mod_id."')
		");
		
		// Always check that result is not an error
		if (DB::isError($this->Result)) 
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'int_add', OVER );
		else 
			return true;
		
	}

	/**
	*	BOOL int_is( $int_alias )
	*
	*	Is interrupt with alias $int_alias exists?
	*
	*	if exists return TRUE
	*	else FALSE
	*/
	function int_is( $int_alias ){
		if (!@$this->DBConn) $this->db_connect();
		// Query 
		$this->Result =& $this->DBConn->simpleQuery("
		SELECT id FROM core_int WHERE int_alias = '".$int_alias."' LIMIT 1
		");
		return ($this->DBConn->numrows($this->Result) > 0);
	}

	/**
	*	BOOL int_del( $int_alias )
	*
	*	Deleting interrupt with alias $int_alias
	*
	*	if successfull return TRUE
	*	else FALSE
	*/
	function int_del( $int_alias ) {
		if (!@$this->DBConn) $this->db_connect();
		// Query 
		$this->Result =& $this->DBConn->simpleQuery("
		DELETE FROM core_int WHERE int_alias = '".$int_alias."'
		");
		
		// Always check that result is not an error
		if (DB::isError($this->Result)) {
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'int_del', OVER);
			return false;
		} else 
			return true;
	}
	
	/**
	*	VOID int_done_run( $module, $int_alias )
	*
	*	User used function for SIGNATION module $module with alias $int_alias
	*
	*	if successfull return TRUE
	*	else FALSE
	*/
	function int_done_run ( $int_alias ) {
		if (!@$this->DBConn) $this->db_connect();
		$RES = true;
		if ($this->evt_int_is($int_alias) > 0) {
			$res = $this->evt_get_acts($int_alias);
			foreach($res as $mkey => $mvalue) {
				$RES =& $this->act_run($mvalue["act_alias"]);
			}
		}
		return $RES;
	}
	
}// class 

?>