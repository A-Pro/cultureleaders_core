<?
class FILE {


	static function rn_dir( $from, $to, $move = false, $deep=0 ){
		global $FILE_rn_dir_mkdir_list, $FILE_rn_dir_copy_list;
		$result	= true;
		if (!is_dir($from)) return;
		if (!is_dir($to) && $move != 'del') $FILE_rn_dir_mkdir_list[] = $to; // mkdir($to);
		$d		= dir($from);
		while (false !== ($entry = $d->read())) {
			if (!in_array($entry,array('.','..'))) {
				if (is_dir($from.$entry)) {
					$result &= FILE::rn_dir( $from.$entry."/", $to.$entry."/", $move,$deep+1);
				}else {
					if ($move != 'del') $FILE_rn_dir_copy_list[] = array($from.$entry, $to.$entry); //copy($from.$entry, $to.$entry);
//					echo "$from . $entry -> $to . $entry";
					if ($move) if (unlink($from.$entry)) $result &= true;
				}
			}
			$result &= true;
		}
		$d->close();
		if ($move) if (rmdir($from)) $result &= true;
//		echo intval($result);
//		if (!$result) echo $from;

		if ($deep == 0){
			if (is_array($FILE_rn_dir_mkdir_list)) for ($i=0;$i<sizeof($FILE_rn_dir_mkdir_list);$i++)
				mkdir($FILE_rn_dir_mkdir_list[$i]);
			if (is_array($FILE_rn_dir_copy_list)) for ($i=0;$i<sizeof($FILE_rn_dir_copy_list);$i++)
				copy($FILE_rn_dir_copy_list[$i][0], $FILE_rn_dir_copy_list[$i][1]);
//			print_r($FILE_rn_dir_mkdir_list);
//			print_r($FILE_rn_dir_copy_list);
		}

		return $result;
	}

	/**
	*
	*/
	static function read_dir( $dir )
	{
		$tmp = array();

		if (!empty($dir)) if ($handle = opendir($dir)) {
		   while (false !== ($file = readdir($handle))) 
			   if ($file != "." && $file != "..")
				   $tmp[] =  $file;
		   closedir($handle); 
		}else {
			return 0;
		}
		return $tmp;
	}

	/**
	*
	*/
	static function chk_files( $arr, &$result)
	{
		foreach ($arr as $result) {
// 			echo $result."\n"; 
 			if (is_file($result)) return $result; 
		}
		$result = '';
		return false;
	}

	/**
	*
	*/
	static function chk_dirs( $arr, &$result)
	{
		foreach ($arr as $result) {
// 			echo $result."\n"; 
 			if (is_dir($result)) return $result; 
		}
		$result = '';
		return false;
	}

	static function f_size($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');
  
    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);
  
//    $bytes /= pow(1024, $pow);
  
    return round($bytes, $precision) . ' ' . $units[$pow];
	}
	
	static function deldir($dir)
	{
		if(file_exists($dir)) {
		$open = opendir($dir);
		while(false !== ($file = readdir($open)))
		{
			$i++;
			if($file != '.' && $file != '..'){
				if(is_dir($dir.'/'.$file)) {
					self::deldir($dir.'/'.$file);
					
					rmdir($dir.'/'.$file);
				}
				else{
					unlink($dir.'/'.$file);
				}
			}
		}
		rmdir($dir);
		}
	}  
} 

?>