
<?php
/*************************************************************
*       Arabic/Roman Numeral Convertor        *
*                              *
* For converting between integers and Roman Numerals as well *
* as checking the validity of Roman Numerals.        *
*                              *
*   1  I                         *
*   5  V     5,000   V               *
*   10  X     10,000  X               *
*   50  L     50,000  L               *
*   100 C     100,000  C               *
*   500 D     500,000  D               *
*   1000 M     1,000,000 M               *
*                              *
* Copyright (C) Dmitry Zarezenko, 2005           *
*    E-mail : DIKGenius_php@rambler.ru          *
*************************************************************/
	global $_CONV;
	$_CONV['counter'] = Array();
	$_CONV['romans'] = Array("I" => 1, "V" => 5, "X" => 10, "L" => 50, "C" => 100, "D" => 500, "M" => 1000, "" => 0);
	$_CONV['subs']  = Array("I" => true, "X" => true, "C" => true, "M" => true);
	
	class ArRoConvertor{
  // Constructor
  function ArRoConvertor (){
  
  }
  /***********************************
  * These first arrays are used by  *
  * the functions which convert   *
  * roman numerals into arabic.   *
  ***********************************/
  

  
  /***********************************
  * This first function is a pretty *
  * simple and generic function used *
  * to define and throw errors    *
  ***********************************/
  
  function createError($ErrorName, $ErrorMessage) {
    $theError = new Error();
    $theError->name = $ErrorName;
    $theError->message = $ErrorMessage;
    $theError->throw();
  }
  
  /***********************************
  * These next four functions are  *
  * used to test for errors in the  *
  * input and convert roman numerals *
  * into arabic integers.      *
  ***********************************/
  
  function checkRom($Rcur, $Rnext, $lSub, $n, $l) {
    if (!isSet($_CONV['romans'][$Rcur]) || (!isSet($_CONV['romans'][$Rnext]) && (($n+1) < $l))) {
      ArRoConvertor::createError("InputError", "Not a Roman Numeral");
    }
    else if ($_CONV['romans'][$Rcur] >= $lSub) {
      ArRoConvertor::createError("InputError", "Not a Properly Formed Numeral");
    }
  }
  
  function testSub($cR, $nR, $pR) {
    if (isSet ($_CONV['romans'][$cR]) && isSet ($_CONV['romans'][$nR]) && isSet ($_CONV['romans'][$pR])){
      if ($_CONV['romans'][$cR] < $_CONV['romans'][$nR]) {
        if (($_CONV['romans'][$pR] == $_CONV['romans'][$nR]) && ($_CONV['subs'][$nR] != true)) {
          ArRoConvertor::createError("InputError", "Not a Properly Formed Numeral");
        }
        else if (($_CONV['subs'][$cR] == true) && (10*$_CONV['romans'][$cR] >= $_CONV['romans'][$nR])) {
          return true;
        }
        else {
          ArRoConvertor::createError("InputError", "Not a Properly Formed Numeral");
          return false;
        }
      }
    }
    return false;
  }
  
  function testRom($rome) {
    if (!isSet ($_CONV['counter'][$rome])) return false;
    if ($_CONV['counter'][$rome] < 3) {
      return true;
    }
    else {
      ArRoConvertor::createError("InputError", "Not a Properly Formed Numeral");
    }
    return false;
  }
  
  function RomanToArabic ($rNumb) {
    $_CONV['counter']["I"] = 0;
    $_CONV['counter']["V"] = 2;
    $_CONV['counter']["X"] = 0;
    $_CONV['counter']["L"] = 2;
    $_CONV['counter']["C"] = 0;
    $_CONV['counter']["D"] = 2;
    $_CONV['counter']["M"] = -1000000000; // Negative infinity
    $intNumb = 0;
    $lastNumb = 1000000000; // Positive infinity
    $thisNumb = 0;
    $lastSub = 1000000000; // Positive infinity
  
    $rNumb= strtoupper ($rNumb);
    /*$tmpNumb= preg_replace ( "/[^IVXLCDM]/m", '', $rNumb);
    if ($tmpNumb!= $rNumb) ArRoConvertor::createError("Warning", "$rNumb is not a Roman numeral, was changed on $tmpNumb");
    $rNumb= $tmpNumb;*/
    $len= strlen ($rNumb);
    for ($i= 0; $i< $len; $i++){
      $currentR = $rNumb[$i];
      @$nextR  = $rNumb[$i+1];
      @$prevR  = $rNumb[$i-1];
  
      ArRoConvertor::checkRom ($currentR, $nextR, $lastSub, $i, $len);
      if (ArRoConvertor::testSub ($currentR, $nextR, $prevR)) {
        $thisNumb = $_CONV['romans'][$nextR] - $_CONV['romans'][$currentR];
        $i ++;
        $lastSub = $_CONV['romans'][$currentR];
      }
      else if (ArRoConvertor::testRom($currentR)){
        $thisNumb = $_CONV['romans'][$currentR];
        $_CONV['counter'][$currentR] ++;
      }
      if ($thisNumb > $lastNumb) {
        ArRoConvertor::createError("InputError", "Not a Properly Formed Numeral");
      }
      else {
        $intNumb += $thisNumb;
        $lastNumb = $thisNumb;
      }
    }
    return $intNumb;
  }
  
  /***********************************
  * The next two functions are used *
  * for converting an arabic integer *
  * into a roman Numeral.      *
  ***********************************/
  
  function Cut($num, $n){
    return ($num - ($num % $n ) ) / $n;
  }
  
  function ArabicToRoman ($aNumb) {

    if (!is_int ($aNumb)){

      ArRoConvertor::createError("InputError", "$aNumb is not a Arabic Integer");
      return "";
    }
    $rNumb= "";
    while ($aNumb> 5999) { $rNumb.= "M"; $aNumb-= 1000;}
    if(($aNumb > 0)) {
	
      $mill = Array("", "M", "MM", "MMM", "MMMM", "MMMMM");
      $cent = Array("", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM");
      $tens = Array("", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC");
      $ones = Array("", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX");
  
      $m = ArRoConvertor::Cut($aNumb, 1000); $aNumb%= 1000;
      $c = ArRoConvertor::Cut($aNumb, 100); $aNumb%= 100;
      $t = ArRoConvertor::Cut($aNumb, 10); $aNumb%= 10;
      
      return $rNumb . $mill[$m] . $cent[$c] . $tens[$t] . $ones[$aNumb];
    }
    else {
      ArRoConvertor::createError("InputError", "Numbers from 1 to 5999 only please.");
      return "";
    }
  }
}
?>
