<?
/*
CREATE TABLE `core_mod` (
`id` SMALLINT NOT NULL ,
`mod_alias` VARCHAR( 25 ) NOT NULL ,
`mod_tables` VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY ( `id` ) 
);
*/

include_once('class.Common.php');

define('ERROR_MOD_SRCDIR_NOT_FOUND', 'ERROR_MOD_SRCDIR_NOT_FOUND');
$ERROR_MSGS[ERROR_MOD_SRCDIR_NOT_FOUND] = "Mod_src_dir not found.";
define('ERROR_MOD_CANT_COPY', 'ERROR_MOD_CANT_COPY');
$ERROR_MSGS[ERROR_MOD_CANT_COPY] = "Can not copy mod_src_dir in to mod_dir.";
define('ERROR_MOD_MODULE_ALREADY_EXISTS', 'ERROR_MOD_MODULE_ALREADY_EXISTS');
$ERROR_MSGS[ERROR_MOD_MODULE_ALREADY_EXISTS] = "Module already exists.";


class Mod extends Common {
	
	function Mod(){
		$this->Common();
	}
	
	function mod_add($mod_alias, $mod_src_dir){
		if (!@$this->DBConn) $this->db_connect();
		$mod_dir = realpath($this->PATHMODS);
		if (is_dir($mod_src_dir)){
			if (!is_dir($mod_dir."/".$mod_alias) and $this->mod_is_installed($mod_alias) !== false){
				if (rename($mod_src_dir, $mod_dir."/".$mod_alias)){
					$sql_dir = $this->PATHMODS."/".$mod_alias."/sql";
					$list_tab = $sql_dir."/list_tab";
					if (file_exists($list_tab)){
						$fp = fopen($list_tab, "r");
						$list_tab_content = fread($fp, filesize($list_tab));
						fclose($fp);
						//$tabs = split(";", str_replace("\n", "", $tmp));
						$d = dir($sql_dir);
						while (false !== ($entry = $d->read())) {
							list(,$ext) = split("\.", $entry);
							if ($ext == "sql"){
								$fp = fopen($sql_dir."/".$entry, "r");
								$sql = fread($fp, filesize($sql_dir."/".$entry));
								$sqls = split(";\n", str_replace("\r", "", $sql));
//								$this->error('', "<pre>".print_r($sqls, true)."</pre>", 'mod_add', true);
								for ($i = 0; $i < sizeof($sqls); $i++){
									if ($sqls[$i] != ""){
										$this->Result =& $this->DBConn->Query($sqls[$i]);
										if (DB::isError($this->Result)) {
											$this->error('',$this->Result->getMessage()." '".$this->DBConn->last_query."'", 'mod_add', OVER );
										}
									}
								}
							}
						}
						$d->close();
					}
					$sql = "INSERT INTO core_mod (`mod_alias`, `mod_tables`) VALUES ('$mod_alias', '".$list_tab_content."');";
					$this->Result =& $this->DBConn->simpleQuery($sql);
					if (DB::isError($this->Result)) {
						$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'mod_add', OVER );
					}
					echo readfile("http://".$this->SERVER_NAME.$this->DIRMODS."/".$mod_alias."/install.php");
					//include($this->PATHMODS."/".$mod_alias."/install.php");
					return  true;
				}else {
					$this->error(ERROR_MOD_CANT_COPY, "", "mod_add()");
				}
			}else {
				$this->error(ERROR_MOD_MODULE_ALREADY_EXISTS, "", "mod_add()");
			}
		}else {
			$this->error(ERROR_MOD_SRCDIR_NOT_FOUND, "", "mod_add()");
		}
	}
	
	function mod_is_installed($mod_alias){
		return $this->mod_is($mod_alias);
	}
	
	function mod_del($mod_alias){
		if (!@$this->DBConn) $this->db_connect();
		if ($this->mod_is_installed($mod_alias) !== false){
			$sql = "SELECT * FROM core_mod WHERE mod_alias='$mod_alias' LIMIT 1;";
			$this->Result =& $this->DBConn->query($sql);
			if (DB::isError($this->Result)) $this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'mod_del', OVER );
			$row = $this->Result->fetchRow(DB_FETCHMODE_ASSOC);
			$tables = split(";", $row["mod_tables"]);
			for ($i = 0; $i < sizeof($tables); $i++) $tables_arr[] = array($tables[$i]);
			$sql = "DROP TABLE ?;";
			$this->prep = $this->DBConn->prepare($sql);
			if (DB::isError($this->prep))$this->error('',$this->prep->getMessage(), 'mod_del', OVER );
			$this->Result =& $this->DBConn->executeMultiple($this->prep, $tables_arr);
			if (DB::isError($this->Result))$this->error('',$this->prep->getMessage(), 'mod_del', OVER );
			$sql = "DELETE FROM core_mod WHERE mod_alias='$mod_alias';";
			$this->Result =& $this->DBConn->simpleQuery($sql);
			if (DB::isError($this->Result)) $this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'mod_del', OVER );
			echo readfile("http://".$this->SERVER_NAME.$this->DIRMODS."/".$mod_alias."/uninstall.php");
			@unlink($this->PATHMODS."/".$mod_alias);
		}else{
			$this->error(ERROR_MOD_NFOUND, "", "mod_del()");
		}
	}
}// class
?>