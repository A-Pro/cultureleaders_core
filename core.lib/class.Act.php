<?
/**
������ � ������������ (INT)
---------------------------

���������� ���������� ��� ������� � ���� ������: PEAR/DB

CREATE TABLE `core_act` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `act_alias` varchar(15) NOT NULL default 'none',
  `mod_id` int(11) NOT NULL default '0',
  `type` enum('file','dir') NOT NULL default 'file',
  `cont` varchar(255) NOT NULL default '',
  `descr` varchar(255) default NULL,
  PRIMARY KEY  (`act_alias`),
  UNIQUE KEY `act_alias` (`act_alias`),
  KEY `id` (`id`)
) TYPE=MyISAM COMMENT='Table of Actions'
    
*/

include_once( 'class.Int.php' ); 

define('ACT_FILE','file');
define('ACT_DIR','dir');

define('ERROR_ACT_BTYPE', 'ERROR_ACT_BTYPE');
define('ERROR_ACT_NCONT', 'ERROR_ACT_NCONT');
define('ERROR_ATC_WRONGTYPE', 'ERROR_ATC_WRONGTYPE');
define('ERROR_ACT_NACT', 'ERROR_ACT_NACT');
$ERROR_MSGS[ERROR_ACT_BTYPE]	= 'Wrong Action type.';
$ERROR_MSGS[ERROR_ACT_NCONT]	= 'Wrong or empty Cntent of Action.';
$ERROR_MSGS[ERROR_ATC_WRONGTYPE]	= 'Wrong type.';
$ERROR_MSGS[ERROR_ACT_NACT]	= 'No data loaded.';

class Act extends Int {
	
	
	function Act(){
		$this->ActTypes	= array(ACT_FILE,ACT_DIR);
	}
	
	/**
	*	BOOL act_add(  $act_alias, $act_cont, $act_type='file', $descr = '', $mod_alias='' )
	*
	*	Inserting new action with alias $act_alias  and content $act_cont.
	*	default type is 'file'
	*	it may be description in $desc and mod_alias
	*
	*	if inserting is good return TRUE
	*	else FALSE
	*/
	function act_add( $act_alias, $act_cont, $act_type=ACT_FILE, $descr = '', $mod_alias='' ){
		if (!@$this->DBConn) $this->db_connect();
		// Empty ??
		if (!empty($mod_alias)) {
			$mod_id	= $this->mod_is($mod_alias);
			if ($mod_id === false)	{
				$this->error(ERROR_MOD_NFOUND); // non critical error
				return false;
			}
		}else 
			$mod_id	= '0';
			
		if (!in_array($act_type, $this->ActTypes)) {
			$this->error(ERROR_ACT_BTYPE);
			return false;
		}
		
		if (empty($act_cont)){
			$this->error(ERROR_ACT_NCONT);
			return false;
		}
		
		// Query 
		$this->Result =& $this->DBConn->simpleQuery("
		INSERT INTO core_act (act_alias, type, cont, descr, mod_id  ) 
		VALUES ('".$act_alias."','".$act_type."','".$act_cont."','".$descr."','".$mod_id."')
		");
		// Always check that result is not an error
		if (DB::isError($this->Result)) {
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'act_add', OVER );
			return false;
		} else
			return true;
		
	}

	/**
	*	BOOL act_is( $act_alias )
	*
	*	Is action with alias $act_alias exists?
	*
	*	if exists return TRUE
	*	else FALSE
	*/
	function act_is( $act_alias ){
		if (!@$this->DBConn) $this->db_connect();

		// Query 
		$this->Result =& $this->DBConn->simpleQuery("
		SELECT id FROM core_act WHERE act_alias = '".$act_alias."' LIMIT 1
		");
		return ($this->DBConn->numrows($this->Result) > 0);
	}
	
	/**
	*	array act_get( $act_alias )
	*
	*	return array with record content
	*
	*/
	function act_get( $act_alias ){
		if (!@$this->DBConn) $this->db_connect();
		
		// Query 
		$this->Result =& $this->DBConn->query("
		SELECT * FROM core_act WHERE act_alias = '".$act_alias."' LIMIT 1
		");
		
		// Always check that result is not an error
		if (DB::isError($this->Result)) {
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'act_add', OVER );
			return false;
		}
		return $this->Result->fetchRow(DB_FETCHMODE_ASSOC);
	}

	/**
	*	BOOL act_del( $act_alias )
	*
	*	Deleting action with alias $act_alias
	*
	*	if successfull return TRUE
	*	else FALSE
	*/
	function act_del( $act_alias ) {
		if (!@$this->DBConn) $this->db_connect();

		// Query 
		$this->Result =& $this->DBConn->simpleQuery("
		DELETE FROM core_act WHERE act_alias = '".$act_alias."'
		");
		
		// Always check that result is not an error
		if (DB::isError($this->Result)) {
			$this->error('',$this->Result->getMessage().$this->DBConn->last_query, 'act_del', OVER );
			return false;
		} else
			return true;
	}
	
	function act_run ( $act_alias ){
		if (!@$this->DBConn) $this->db_connect();
		if (empty($act_alias))
			return $this->error( ERROR_ACT_NACT, "act_alias = '$act_alias'" );
		
		$data	= $this->act_get( $act_alias);
		if ($data === false)
			return false;

		if (empty($data) || !is_array($data))
			return $this->error( ERROR_ACT_NACT );
		else {
			switch ($data['type']){
				case ACT_FILE:
					if (file_exists($data['cont']) || file_exists($this->CoreModDir.'/'.$data['cont']) )
						include $data['cont'];
					else 
						return $this->error( ERROR_COMMON_NFILE, $data['cont'],'act_run');
					break;
				case ACT_DIR:
					// reading directory and include only files
					if (is_dir($data['cont']))
						$path = '';
					elseif(is_dir($this->ROOT.'/'.$data['cont']))
						$path = $this->ROOT.'/';
					elseif(is_dir($this->CORE_ROOT.'/'.$data['cont']))
						$path = $this->CORE_ROOT.'/';
					else 
						return $this->error( ERROR_COMMON_NDIR, $data['cont']." (or ".$this->ROOT.'/'.$data['cont']." or ".$this->CORE_ROOT.'/'.$data['cont'].")",'act_run');
						
					$path .= $data['cont'];
					$handle = @opendir($path);
					
				    while (false !== ($file = readdir($handle)))
				        if (is_file($path.'/'.$file)){
				        	include $path.'/'.$file;
//				        	echo $path."/".$file." .<Br>\n";
				        }
//				        else 
//				        	echo $path."/".$file." not found.<Br>\n";
				    break;
				default:
						return $this->error( ERROR_ATC_WRONGTYPE, $data['type'],'act_run');
			}//switch
		}//else
		return true;
	}//func
}// class

?>