<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : class.DBCQ.php 
// PROJECT NAME : SQL/Lib
// DATE         : 10.12.2004
// Author       : Andrew Lyadkov
// Discription  : combine Connect, Query, lib.sql_query
// ...........  :
// Comment      : 
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
/*
20.03.2007 - add function SQL::ins_set, get_set
9.01.2006	- add function SQL::query($cmd,$debug)
10.12.2004 	- create
22.06.06 Метод FetchArray. Добавлен второй опциональный аргумент ass={0,1} чтобы получить только ассоциативные соответствия
*/
///////////////////////////////////////////////////////
////////   DB FUNCTIONS  //////////////////////////////
///////////////////////////////////////////////////////
define ("SQL_DB_TYPE","mysql");

/**
 * Proxy class for auto-loging queries for database to Log-table
 * 
 * @uses $SQL_DBCONF
 *
 */
class SQL  {

		var $HostName;		
		var $UserName;		
		var $UserPassword;	
		var $Port="5432";	
		var $DataBase;		
		var $DBType	= 'mysql';

		// Определение параметров для работы с Базой Данных
		var $Connect;		// Указатель на соединение с Базой Данных
		var $CloseConnect;	// Указатель на закрытие соединения с Базой Данных
		var	$Tables;		// список таблиц базы
		var	$Field;			// список полей таблицы

		// Определение переменных ошибок
		var $ErrorOpenConnect;	// Ошибка соединения с Базой Данных
		var $ErrorCloseConnect;	// Ошибка зыкрытия соединения с Базой Данных

		/**
		*
		*/
		function SQL()
		{
			$this->obj = true;
		}

		//////////////////////////////////////////////
		//  Конструктор класса (Соединение с БД)    //
		//////////////////////////////////////////////
		static function connect() {
			global $SQL_DBCONF;
			if (empty($SQL_DBCONF)) die("SQL_DBCONF is EMPTY! [SQL::connect]");
/*
			if (@$this->obj) {
				$usr	= $this->UserName     =  $SQL_DBCONF['user'];
				$pwd	= $this->UserPassword =  $SQL_DBCONF['pwd'];
				$name	= $this->DataBase     =  $SQL_DBCONF['name'];
				$port	= $this->Port			=  $SQL_DBCONF['port'];
				$host	= $this->HostName     =  $SQL_DBCONF['host'];
			}else{*/
				$usr	= $SQL_DBCONF['user'];
				$pwd	= $SQL_DBCONF['pwd'];
				$name	= $SQL_DBCONF['name'];
				$port	= $SQL_DBCONF['port'];
				$host	= $SQL_DBCONF['host'];
//			}
	
			global $SQL_DBLINK;

			// Соединение с SQL
			$SQL_DBLINK = @mysql_connect($host,$usr,$pwd) or die(mysql_error());
			if (!mysql_select_db($name)) die(mysql_error());
                        mysql_query( 'SET NAMES cp1251' );
                       	return true;
		}// End of fucntion 
		
		//////////////////////////////////////////////
		//  Последний ID												    //
		//////////////////////////////////////////////
		function last_id( $res='' )
		{
			global $SQL_DBLINK, $SQL_DBCONF, $ERROR;
			if (empty($SQL_DBLINK) || empty($SQL_DBCONF['name'])) return "Wrong SQL_DBCONF or $SQL_DBLINK [SQL::flist]";
			return mysql_insert_id($SQL_DBLINK);
		}

		//////////////////////////////////////////////
		//  Список полей таблицы				    //
		//////////////////////////////////////////////
		function flist( $table='' )
		{
			global $SQL_DBLINK, $SQL_DBCONF, $ERROR;
			if (empty($SQL_DBLINK) || empty($SQL_DBCONF['name'])) return "Wrong SQL_DBCONF or $SQL_DBLINK [SQL::flist]";
			$result= mysql_list_fields($SQL_DBCONF['name'], $table, $SQL_DBLINK);
			if (!$result) { return mysql_error(); }
			else for ($i=0; $i < mysql_num_fields($result); $i++){
				$fileds[$i]['name']	= mysql_field_name($result, $i);
				$fileds[$i]['type']	= mysql_field_type($result, $i);
				$fileds[$i]['len']	= mysql_field_len($result, $i);
				$fileds[$i]['flags']	= mysql_field_flags($result, $i);
			}
			return $fileds;
		}
		
		//////////////////////////////////////////////
		//  Список полей новый						    //
		//////////////////////////////////////////////
		function fieldlist( $table='' )
		{
			global $SQL_DBLINK, $SQL_DBCONF, $ERROR;
			if (empty($SQL_DBLINK))	
				if (!SQL::connect())
					return false;
			if (empty($SQL_DBLINK) || empty($SQL_DBCONF['name'])) return "Wrong SQL_DBCONF [".$SQL_DBCONF['name']."] or $SQL_DBLINK [SQL::fieldlist]";
			// mysql_list_fields устаревшая ( Deprecated )
			$result = mysql_query("SHOW COLUMNS FROM ".$table."", $SQL_DBLINK);
			if (!$result) { return mysql_error(); }
			else {
				$fields = array();
				for ($i=0; $i < mysql_num_rows($result); $i++){
					$row	= mysql_fetch_row( $result);
					$fields[] = $row[0];
				}
			}
			return $fields;
		}

		//////////////////////////////////////////////
		//  Список таблиц						    //
		//////////////////////////////////////////////
		function tlist()
		{
			global $SQL_DBLINK, $SQL_DBCONF, $ERROR;
			if (empty($SQL_DBLINK))	
				if (!SQL::connect())
					return false;
			if (empty($SQL_DBLINK) || empty($SQL_DBCONF['name'])) return "Wrong SQL_DBCONF [".$SQL_DBCONF['name']."] or $SQL_DBLINK [SQL::tlist]";
			//$result = mysql_list_tables($SQL_DBCONF['name'], $SQL_DBLINK);
			// mysql_list_tables устаревшая ( Deprecated )
			$result = mysql_query("SHOW TABLES FROM ".$SQL_DBCONF['name']."", $SQL_DBLINK);
			if (!$result) { return mysql_error(); }
			else for ($i=0; $i < mysql_num_rows($result); $i++){
				$row	= mysql_fetch_row( $result);
				$tables[] = $row[0];
			}
			return $tables;
		}

		//////////////////////////////////////////////
		//          Уничтожение объекта             //
		//////////////////////////////////////////////
		function destroy() {
			global $SQL_DBLINK, $SQL_DBCONF, $ERROR;
			if (empty($SQL_DBLINK) || empty($SQL_DBCONF['name'])) return "Wrong SQL_DBCONF or $SQL_DBLINK [SQL::flist]";

			// Осуществляем закрытие соединения с Базой Данных
			$CloseConnect = mysql_close($SQL_DBLINK);

			// Проверка правильности закрытия соединения
			if (!$CloseConnect) {
				$this->ErrorCloseConnect = mysql_error($SQL_DBLINK);
				return;
			}
		}// End of Destroy
	
	 ////////////////////////////////////////////////////// insert_sql()
	//
	function ins( $into, $var, $val, $debug = 0 )
	{
		global $SQL_DBLINK, $SQL_DBCONF;
		if (empty($SQL_DBLINK))	
			if (!SQL::connect())
				return false;
		$cmd	= "INSERT INTO $into ( $var ) VALUES ( $val ) ";
		$result	= new Query( $SQL_DBLINK, $cmd );
		if( DEBUG && $result->ErrorQuery || $debug) 
		{
			Main::log_message  ( "".$cmd." (".$result->ErrorQuery.")" );
		}
		if (!$result->Result && $debug) error_log ( "Error : ".$result->ErrorQuery );
		return $result;
	}
	 //////////////////////////////////////////////////////	 update_sql()
	//
	function upd( $table, $set, $where='', $debug = 0 )
	{
		global $SQL_DBLINK, $SQL_DBCONF;
		if (empty($SQL_DBLINK))	
			if (!SQL::connect())
				return false;
		$cmd = "UPDATE $table SET $set ";
		$cmd .= (!empty($where)) ? " WHERE $where " : '';
		$result	= new Query( $SQL_DBLINK, $cmd );
		if( DEBUG && $result->ErrorQuery || $debug) 
		{
			Main::log_message  ( "".$cmd." (".$result->ErrorQuery.")" );
		}
		if (!$result->Result && $debug) Main::log_message ( "Error : ".$result->ErrorQuery );
		return $result;
	}

	/**
	 * Execute select-query to data base. Automaticaly connect to database.
	 *
	 * @param string $what part SQL-query, after WHAT.
	 * @param string $from part SQL-query, after FROM.
	 * @param sting $where part SQL-query, after WHERE.
	 * @param string $tail part SQL-query, with ORDER BY, GROUP BY and others.
	 * @return Query 
	 */
    static function sel( $what, $from, $where='', $tail='', $debug=0 )
	{
		global $SQL_DBLINK, $SQL_DBCONF;
		if (empty($SQL_DBLINK))	
			if (!SQL::connect())
				return false;
		$cmd = "SELECT $what FROM $from ";
		$cmd .= (!empty($where)) ? " WHERE $where " : '';
		$cmd .= $tail;
		// echo "<br />".$cmd."<br />";
		$result	= new Query( $SQL_DBLINK, $cmd );
		if( DEBUG && $result->ErrorQuery || $debug) 
		{
			Main::log_message  ( "".$cmd." (".$result->ErrorQuery.")" );
			if (!$result->Result && $debug) Main::log_message ( "Error : ".$result->ErrorQuery );
		}
		return $result;
	}

	/**
	 * get all records from query
	 *
	 * @param  string $what    part SQL-query, after SELECT.
	 * @param  string $from    part SQL-query, after FROM.
	 * @param  string $where   part SQL-query, after WHERE.
	 * @param  string $tail    part SQL-query, with ORDER BY, GROUP BY and others.
	 * @param  boolean $debug  does it need to print debug messages?
	 * @return array of records|boolean
	 */
	static function getall( $what, $from, $where='', $tail='', $debug = 0 ){
		$res = SQL::sel($what,$from,$where,$tail, $debug);
		if ($res->NumRows > 0) {
		    $arr = array();
            for ($i = 0; $i < $res->NumRows; $i++) {
                $res->FetchArray($i,1);
                $arr[$i] = $res->FetchArray;
            }
			return $arr;
		} else return false;
	}	
	
	/**
	 * get all records from query with key id
	 *
	 * @param  string $what    part SQL-query, after SELECT.
	 * @param  string $from    part SQL-query, after FROM.
	 * @param  string $where   part SQL-query, after WHERE.
	 * @param  string $tail    part SQL-query, with ORDER BY, GROUP BY and others.
	 * @param  boolean $debug  does it need to print debug messages?
	 * @return array of records|boolean
	 */
	static function getallkey( $what, $from, $where='', $tail='', $key = 'id', $debug = 0 ){
		$res = SQL::sel($what,$from,$where,$tail, $debug);
		if ($res->NumRows > 0) {
		    $arr = array();
            for ($i = 0; $i < $res->NumRows; $i++) {
                $res->FetchArray($i,1);
                $arr[$res->FetchArray[$key]] = $res->FetchArray;
            }
			return $arr;
		} else return false;
	}	    
	 //////////////////////////////////////////////////////	 delete_sql()
	//
	function del( $table, $where='', $debug = 0 )
	{
		global $SQL_DBLINK, $SQL_DBCONF;
		if (empty($SQL_DBLINK))	
			if (!SQL::connect())
				return false;
		$cmd = "DELETE FROM $table ";
		$cmd .= (!empty($where)) ? " WHERE $where " : '';
		$result	= new Query( $SQL_DBLINK, $cmd );
		if( DEBUG && $result->ErrorQuery || $debug) 
		{
			Main::log_message  ( "".$cmd." (".$result->ErrorQuery.")" );
		}
		if (!$result->Result && $debug) error_log ( "Error : ".$result->ErrorQuery );
		return $result;
	}
	
	function truncate( $table, $debug = 0 )
	{
		global $SQL_DBLINK, $SQL_DBCONF;
		if (empty($SQL_DBLINK))	
			if (!SQL::connect())
				return false;
		$cmd = "TRUNCATE TABLE $table ";
		$result	= new Query( $SQL_DBLINK, $cmd );
		if( DEBUG && $result->ErrorQuery || $debug) 
		{
			Main::log_message  ( "".$cmd." (".$result->ErrorQuery.")" );
		}
		if (!$result->Result && $debug) error_log ( "Error : ".$result->ErrorQuery );
		return $result;
	}

	 //////////////////////////////////////////////////////	 Query()
	//
	function query( $cmd, $debug = 0 )
	{
		global $SQL_DBLINK, $SQL_DBCONF;
		if (empty($SQL_DBLINK))	
			if (!SQL::connect())
				return false;
		$result	= new Query( $SQL_DBLINK, $cmd );
		if ($debug) Main::log_message  ( "".$cmd." (".$result->ErrorQuery.")" );
		if (!$result->Result && $debug) error_log ( "Error : ".$result->ErrorQuery );
		return $result;
	}

	//////////////////////////////////////////////////////
	//
	static function getval( $what, $from, $where='', $tail='', $debug=0 ){
		$res = SQL::sel($what,$from,$where,$tail, $debug);
		if ($res->NumRows > 0) {
			$res->FetchArray(0);
			return $res->FetchArray[0];
		}else
			return false;
	}
	
	static function getvals( $what, $from, $where='', $tail='', $debug=0 ){
		$res = SQL::sel($what,$from,$where,$tail, $debug);
		if ($res->NumRows > 0) {
		    $arr = array();
            for ($i = 0; $i < $res->NumRows; $i++) {
                $res->FetchArray($i,1);
                $arr[$i] = $res->FetchArray[$what];
            }
			return $arr;
		}else
			return false;
	}

	static function getrow( $what, $from, $where='', $tail='', $debug=0 ){
		$res = SQL::sel($what,$from,$where,$tail, $debug);
		if ($res->NumRows > 0) {
			$res->FetchArray(0);
			return $res->FetchArray;
		}else
			return false;
	}
	/**
	 * Enter description here...
	 *
	 * @param $pref 
	 * (array name)
	 * @param $val 
	 * (curent value)
	 * @param $id 
	 * (field_name)
	 * @param $field 
	 * (wich field for titles)
	 * @param $table 
	 * (from wich table)
	 * @param $add_all 
	 * (Add empty line)
	 * @param $where 
	 * ( extra filter )
	 * @param $tail 
	 * (extra SQL)
	 * @param $by_field 
	 * (if not fieled then by ID)
	 * @param $debug 
	 * (echo SQL)
	 * @return HTML (select)
	 */
	function col2sel ( $field_name, $val, $id, $field, $table, $add_all = false, $where='', $tail='', $by_field = false, $debug=0, $def = '') 
	{
		if ($val == '' && $def != '') $val = $def;
		$str = "<select name=\"". sprintf ("%s",$field_name)."\">";
		$str .= ($add_all) ? "<option value='0'>Все</option>" : '';
//		$sql = "SELECT * FROM $table ";
//		$sql .= (empty($where)) ? '' : "WHERE $where";
//		$sql .= (empty($tail)) ? '' : " $tail ";
//		$result = new Query( $db->Connect, $sql );
		$result = SQL::sel( "*", $table, (empty($where)) ? '' : "WHERE $where", (empty($tail)) ? '' : " $tail ", $debug);
		if (!$result->Result) 
		{
			error_log ( "Base Error:".$result->ErrorQuery."SQL: $sql");
			return '';
		}
		for($j=0; $j < $result->NumRows; $j++) 
		{
			$result->FetchArray($j);
			$row = $result->FetchArray;
			$str .= sprintf("\n<option value=\"%s\"", ($by_field)?$row[$field]:$row[$id]);
			if ( (!$by_field && $row[$id] == $val) || ($by_field && $row[$field] == $val)) 
				$str .= " SELECTED ";
			$str .= sprintf(">%s</option>", $row[$field]);
		}
		$str .= "</select>\n\n";

		return $str;
	}
	 ////////////////////////////////////////////////////////  _tab_Col2Arr
	//  -----------------------------------------------------------------
	//  converting table columnS into Array
	//  ver 2.0 simple
	//
	//  tested 12/03/02
	// 
	function col2arr( &$arr, $tab_name, $field=1, $key=0, $where='' )
	{
		global $db;
		$get_cols = (is_array($field)) ? join(",",$field) : $field;
		$select = (!empty($field) && empty($key)) ? " $get_cols " : " $get_cols, $key  ";
//		$cmd = "SELECT $select FROM $tab_name";
//		$cmd .= (empty($where)) ? '' : " WHERE $where ";
//		$result = new Query( $db->Connect, $cmd );
		$result	= SQL::sel( $select, $tab_name, (empty($where)) ? '' : " $where ", '',DEBUG);
		if ($result->Result && $result->NumRows > 0)
		{
			for ( $i=0; $i < $result->NumRows; $i++ )
			{
				$result->FetchArray($i);
				if (!empty($key))
					$arr[$result->FetchArray[$key]] = (is_array($field)) ? $result->FetchArray : $result->FetchArray[0];
				else 
					$arr[] = (is_array($field)) ? $result->FetchArray : $result->FetchArray[0];
			}
			return 1;
		}
		else
		{
			global $LandError; 
			$LandError= $cmd;
			return 0;
		}
	}	

	 ////////////////////////////////////////////////////////  _tab_GetVal()
	//  -----------------------------------------------------------------
	//  Take one Box from the Table
	//  ver 1.0 simple
	//
	function GetVal1( $tab_name, $where, $field=1 )
	{
		global $db;
		$cmd = "SELECT $field FROM $tab_name WHERE $where";
		$result = new Query( $db->Connect, $cmd );
		if ($result->Result && $result->NumRows > 0)
		{
			$result->FetchArray(0);
			return $result->FetchArray[$field];
		}
		else
			return 0;
	}	

 
	/**
	*
	*/
	function limit_sql( $offset, $limit)
	{
		global $db; 
		if (empty($offset)) $offset = 0;
		if (empty($limit)) $limit = 0;
		switch ($db->DBType) {
			case 'mysql':
				return 	 " LIMIT $offset, $limit ";
				break;
			case 'pgsql':
				return 	 " OFFSET $offset LIMIT $limit ";
				break;
		}
	}
	/**
	*
	*/
	function of_li($o=0,$l=0,$type='mysql')
	{
		switch ($type) {
			case 'mysql':
				return 	 " LIMIT $o, $l ";
				break;
			case 'pgsql':
				return 	 " OFFSET $o LIMIT $l ";
				break;
		}
	} 

	/**
	 * Execute insert-query to data base. Automaticaly connect to database.
	 *
	 * @param  string        $table   table for update
	 * @param  string|array  $set     part SQL-query, after SET.
	 * @param  boolean       $debug   does it need to print debug messages?
	 * @return Query                  result of query
	 */
	function ins_set( $into, $set, $debug = 00 )	{
		global $SQL_DBLINK, $SQL_DBCONF, $_ALIASES;
		if (empty($SQL_DBLINK))	
			if (!SQL::connect())
				return false;
	    $set = sql::get_set($set);
		$cmd	= "INSERT INTO $into SET $set ";
		return SQL::query($cmd, $debug);
	}

	/**
	 * get string with set's sentence for SQL::ins_set and SQL::upd
	 * NB! function return $arr itself if $arr is not array
	 * @param  array   $arr
	 * @return string
	 */
    function get_set($arr) {
        if (is_array($arr)) {
            $set = "";
            foreach ($arr as $mkey => $mval) $set .= ($set ? ",":"") . "`$mkey`='".str_replace("'", "`", $mval)."'";
        } else $set = $arr;
        return $set;
    }	
} 
?><?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : class.Connection.php 
// PROJECT NAME : CLASSES/Land
// DATE         : 15.09.2004
// Author       : Andrew Lyadkov (andrey@lyadkov.nnov.ru)
// Discription  : класс соединения с Базой Данных MySQL
// ...........  : 
// Comment      : ver 1.2
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
	class Connection {
		// Определение параметров подключения к Базе Данных
		var $HostName;		// ??мя хоста
		var $UserName;		// ??мя пользователя
		var $UserPassword;	// Пароль пользователя
		var $Port="5432";	// Номер порта
		var $DataBase;		// ??мя Базы Данных
		var $DBType	= 'mysql';

		// Определение параметров для работы с Базой Данных
		var $Connect;		// Указатель на соединение с Базой Данных
		var $CloseConnect;	// Указатель на закрытие соединения с Базой Данных
		var	$Tables;		// список таблиц базы
		var	$Field;			// список полей таблицы

		// Определение переменных ошибок
		var $ErrorOpenConnect;	// Ошибка соединения с Базой Данных
		var $ErrorCloseConnect;	// Ошибка зыкрытия соединения с Базой Данных

		//////////////////////////////////////////////
		//  Конструктор класса (Соединение с БД)    //
		//////////////////////////////////////////////
		function Connection($ConfigFile='',$Host_Name='',$User_Name='',$User_Password='',$Data_Base='',$Post='') {
			if ($ConfigFile!='' && $ConfigFile!='none' && is_file($ConfigFile)) {
				require "$ConfigFile";
			}
			if (is_array($Host_Name)) {
				$User_Name		= $Host_Name['user'];
				$User_Password	= $Host_Name['pwd'];
				$Data_Base		= $Host_Name['name'];
				$Port			= $Host_Name['port'];
				$Host_Name		= $Host_Name['host'];
			}
			// Присваиваем параметры подключения к БД
			$this->HostName     = $Host_Name;
			$this->UserName     = $User_Name;
			$this->UserPassword = $User_Password;
			$this->DataBase     = $Data_Base;
			$this->Port			= $Port;

			// Соединение с SQL
			$this->Connect = @mysql_connect($this->HostName,$this->UserName,$this->UserPassword);
			// Проверка соединения
			if (!$this->Connect) {
				$this->ErrorOpenConnect = mysql_error();
				return false;
			}
			if (!mysql_select_db($Data_Base))
				$this->ErrorOpenConnect = mysql_error();
                       mysql_query( 'SET NAMES cp1251' );
                       
		}// End of fucntion Connect()

		//////////////////////////////////////////////
		//  Список таблиц						    //
		//////////////////////////////////////////////
		function FieldsList( $table='' )
		{
			if (!$this->Connect) return false;
			$result= mysql_list_fields($this->DataBase, $table, $this->Connect);
			if (!$result) { $this->Error = mysql_error();return false;}
			else for ($i=0; $i < mysql_num_fields($result); $i++){
				$this->Field[$i]['name']	= mysql_field_name($result, $i);
				$this->Field[$i]['type']	= mysql_field_type($result, $i);
				$this->Field[$i]['len']	= mysql_field_len($result, $i);
				$this->Field[$i]['flags']	= mysql_field_flags($result, $i);
			}
			return $this->Field;
		}

		//////////////////////////////////////////////
		//  Список таблиц						    //
		//////////////////////////////////////////////
		function TablesList()
		{
			if (!$this->Connect) return false;
			$result = mysql_list_tables($this->DataBase, $this->Connect);
			if (!$result) { $this->Error = mysql_error();return false;}
			else for ($i=0; $i < mysql_num_rows($result); $i++){
				$row	= mysql_fetch_row( $result);
				$this->Tables[] = $row[0];
			}
			return $this->Tables;
		}

		//////////////////////////////////////////////
		//          Уничтожение объекта             //
		//////////////////////////////////////////////
		function Destroy() {
			// Осуществляем закрытие соединения с Базой Данных
			$this->CloseConnect = mysql_close($this->Connect);

			// Проверка правильности закрытия соединения
			if (!$this->CloseConnect) {
				$this->ErrorCloseConnect = mysql_error($this->CloseConnect);
				return;
			}
			else
				// Удаляем объект класса
				unset($this);
		}// End of Destroy
	}// End of class Connect
?>
<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : class.Query.php
// PROJECT NAME : CLASSES/Land
// DATE         : 26.09.2002
// Author       : Andrew Lyadkov (andrey@lyadkov.nnov.ru)
// Discription  : обработки запросов Базы Данных MySQL
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////

	class Query {
		// Определение параметров для работы с Базой Данных
		var $Result;	// Результат обработки запроса
		var $NumRows;	// Количество записей
		var $FetchRow;	// Объект результата запроса строки
		var $FetchArray;// Added by Land 10.12.01
		var $Tuples;	// Количество добавленных, измененных и удаленных записей
		var $Destroy;	// Указатель на освобождение памяти результата обработки

		// Определение переменных ошибок
		var $ErrorQuery;	// Ощибка обработки запроса
		var $ErrorFetchRow;	// Ошибка обработки результата запроса
		var $ErrorNumRows;	// Ошибка подсчета количества записей результата запроса
		var $ErrorTuples;	// Ошибка подсчета количества добавленных, измененных и удаленных записей

		//////////////////////////////////////////////
		//			Обработка запросов				//
		//////////////////////////////////////////////
		function Query($Connect_id,$Query) {
			if (empty($Connect_id)) {
				$this->ErrorQuery = "No connect to database.";
				return;
			}
			// Осуществляем запрос к Базе Данных
//			$this->Result = mysql_query($Query);
			$this->Result = mysql_query(
				str_replace(chr(0), '', 
					str_replace( '--', '-', 
						$Query
						)
					)	
				);

			// Проверка правильности осуществления запроса
			if (!$this->Result) {
				$this->ErrorQuery = mysql_error($Connect_id);
				return;
			}
			else {
				// Узнаем количество записей
				$this->NumRows = @mysql_num_rows($this->Result);
				$this->Tuples = mysql_affected_rows($Connect_id);
			}
		}// End of function Query

		//////////////////////////////////////////////
		//      Обработка результатов запроса       //
		//////////////////////////////////////////////
		function FetchRow($Row) {
			// Осуществляем получение объекта результата строки
			$this->FetchRow = mysql_fetch_row($this->Result);

			// Проверка правильности получения объекта
			if (!$this->FetchRow) {
				$this->ErrorFetchRow = mysql_error($this->FetchRow);
			}
		}// End of function FetchRow

		//////////////////////////////////////////////
		//      ADDED by Land (FETCH_ARRAY)       //
		//////////////////////////////////////////////
		function FetchArray($Row, $ass = '0') {
			// Осуществляем получение объекта результата строки
			if ($this->Result){
			  if ($ass == '1')
					  $this->FetchArray = mysql_fetch_array($this->Result, MYSQL_ASSOC);
			  else
				$this->FetchArray = mysql_fetch_array($this->Result);
			}else
				$this->ErrorFetchArray = mysql_error();

			// Проверка правильности получения объекта
			if (!$this->FetchArray) {
				$this->ErrorFetchArray = mysql_error();
			}
		}// End of function FetchRow


		//////////////////////////////////////////////
		//		Обработка результатов запроса		//
		//////////////////////////////////////////////
		function Destroy() {
			// Осуществляем освобождение памяти
			if (!$this->Result) return;
			$this->Destroy = @mysql_free_result($this->Result);

			// Проверка правильности освобождения памяти
			if (!$this->Destroy) {
				$this->ErrorDestroy = mysql_error();
			}
		}// End of function Destroy
	}// End of class Query
?>