<?php 
/**
 * 
 * ������������ ����� ��� ���� ����� �������
 * @author Ulyusov
 *
 */
class Modules {
	
	protected $_core,$_sql, $_lmail;
	public $mod;
	public $Errors = array();
	public $config;
	public $moduleTree = array(); // ������ ������ ��������� ������ ��� ������� ������
	
	
	
	public function __construct($mod='')
	{
		$this->setMod($mod);
		include_once 'class.DBCQ.php';
		$this->_sql = new SQL();
		include_once 'class.lMail.php';
		$this->_lmail = new lMail();
		$this->config = $this->get_config();
	}
	
	
	
	public function setMod($mod) {
		global $_CORE;
		$this->_core = $_CORE;
		$this->mod = $mod;
	}
	
	
	
	public function get_config(){
		// �������� ���� �� ����� ������
		global $_CORE;
		if(empty($this->mod)|| !isset($this->_core->_modules[$this->mod]) ) {
			// �������� ������
			array_push($this->Errors,"������ � ����� ������ �� ������");
			return false;
		}
		
		if(empty($this->_core->_modules[$this->mod]['config'])) return '';
		$form = STR::json_decode_cyr($this->_core->_modules[$this->mod]['config'],true);
		$form = STR::utf_to_win($form);
		return $form;
	}
	
	
	
	public function set_config($form){
		// �������� ���� �� ����� ������
		if(!isset($this->_core->_modules[$this->mod]) ){ 
			// �������� ������
			array_push($this->Errors,"������ � ����� ������ �� ������");
			return false;
		}
		if(!isset($this->_core->_modules[$this->mod]['config'])){ 
			// �������� ������
			array_push($this->Errors,"� ������� core_modules ����������� ���� config");
			return false;
		}
		$form = STR::win_to_utf($form,true);
		$config = STR::json_encode_cyr($form);
		$this->_sql->upd("`".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."core_modules`", "config = '".$config."'","alias = '".$this->mod."'");
		return true;
	}
	
	
	
	public function install(){
		if($this->_core->IS_DEV == false ) {
			array_push($this->Errors,"� ��� ������������ ����");
			return false; 
		}
		if(isset($this->_core->_modules[$this->mod])) {
			array_push($this->Errors,"������ ��� ����������");
			return false; 
		}
		$res = $this->_sql->ins( (defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."core_modules", 'alias, name, active, beforepl_plg', "'$this->mod','$this->mod','1', ''", DEBUG);
		if (!$res->Result){
			array_push($this->Errors,"������ MySQL: ".$res->ErrorQuery);
			return false;
		}
		return true;
	}
	
	
	
	public function getpath($path=''){
		global $_PROJECT;
		if (empty($path)) {
			$path = $_SERVER['REQUEST_URI'];
		}
		if (!empty($_PROJECT['PATH_2_CONTENT']))
			$_PROJECT['PATH_2_CONTENT'] = array();
		
		if (!in_array($path, array( '', '/'))) {
			$PATH_2_CONTENT_tmp	=& Main::array_multisearch( $path, $this->moduleTree, "and end");
			$up	= $last	= $prelast = $this->moduleTree;
			for ($i=0;$i<sizeof($PATH_2_CONTENT_tmp)-1; $i++ ) {
				if (!empty($up[$PATH_2_CONTENT_tmp[$i]]['name']) ) {
					$_PROJECT['PATH_2_CONTENT'][]	= array(
						'name'	=> $up[$PATH_2_CONTENT_tmp[$i]]['name'],
						'path'	=> $up[$PATH_2_CONTENT_tmp[$i]]['path']
						);
				}
				$up = $up[$PATH_2_CONTENT_tmp[$i]];
			}
		}
	}
}
?>