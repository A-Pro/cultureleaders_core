<?php

class lMail {
	
	/**  
	* $data['body']
	*/
	static function send( &$data )
	{
        global $_CONF;
        // if smtp need, than go
        if (!empty($data['smtp']) || $_CONF['FEATURES_USED']['smtp']) {
            return self::smtp_mail( $data );
        }
        
        // else
		$message = convert_cyr_string ($data['body'], "w", "k");
		$data['name'] = convert_cyr_string ($data['name'], "w", "k");
		$subject = convert_cyr_string ((!empty($data['subject']))?$data['subject']:'������ � ����� '.$_SERVER['HTTP_HOST'], "w", "k");
		//------- �������� ��������� --------

                if ($data['type']=="html"){
                 $headers="MIME-Version: 1.0\nContent-Type: text/html; charset=koi8-r;\nContent-transfer-Encoding: 8bit\nX-Mailer: PHP.";
                }else{
                  $headers="MIME-Version: 1.0\nContent-Type: text/plain; charset=koi8-r\nContent-transfer-Encoding: 8bit\nX-Mailer: PHP.";
                }

                $test_str = preg_replace("/(.)/e", "sprintf('=%02x',ord('$1'))", $data['name']);
//                $data['name'] = '=?KOI8-R?Q?'.$test_str.'?=';

            $from   = "From: ".$data['name']."<".$data['email'].">\n".$headers;
            $result = false;
			if ( !empty($data['mailto'] ) ){
				$result = @mail($data['mailto'], $subject, $message, $from);
			}else {
				$result = @mail(TO, $subject, $message, $from);
			}
                return $result;

	}
	
	
	
	/**
		$attach = array(
		    'http://vk-book.ru/img/scroll_mouse.jpg',
		    'http://vk-book.ru/img/ugly.png'
		); ������ ������ �� ������������� �������� (������ ����)
		
		$data['text'] - html ��� ������, ������ �� �������� � html ������ ���� ���� cid:scroll_mouse.jpg
	*/
	
	function sendhtml( $data, $attach = array(), $convert=true){
		if ($convert) {
    		$text 		= iconv("windows-1251","utf-8",$data['body']);
    		$from 		= iconv("windows-1251","utf-8",$data['name']."<".$data['email'].">");
    		$to 		= iconv("windows-1251","utf-8",(!empty($data['mailto']))?$data['mailto']:TO);
    		$subject 	= iconv("windows-1251","utf-8",(!empty($data['subject']))?$data['subject']:'������ � ����� '.$_SERVER['HTTP_HOST']);
		 }else{
    		$text 		= $data['body'];
    		$from 		= $data['name']."<".$data['email'].">";
    		$to 		= (!empty($data['mailto']))?$data['mailto']:TO;
    		$subject 	= (!empty($data['subject']))?$data['subject']:'������ � ����� '.$_SERVER['HTTP_HOST'];
		  
		 }
         
         if (!empty($data['selfemail']))
            $to .= ',' . $data['selfemail'];
            
		// ��������� ������ === >>>
		$headers = "From: $from\r\n";
		//$headers .= "To: $to\r\n";
		$headers .= "Subject: $subject\r\n";
		$headers .= "Date: " . date("r") . "\r\n";
		$headers .= "X-Mailer: zm php script\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: multipart/alternative;\r\n";
		$baseboundary = "------------" . strtoupper(md5(uniqid(rand(), true)));
		$headers .= "  boundary=\"$baseboundary\"\r\n";
		// <<< ====================
		 
		// ���� ������ === >>>
		$message  =  "--$baseboundary\r\n";
		$message .= "Content-Type: text/plain;\r\n";
		$message .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
		$message .= "--$baseboundary\r\n";
		$newboundary = "------------" . strtoupper(md5(uniqid(rand(), true)));
		$message .= "Content-Type: multipart/related;\r\n";
		$message .= "  boundary=\"$newboundary\"\r\n\r\n\r\n";
		$message .= "--$newboundary\r\n";
		$message .= "Content-Type: text/html; charset=utf-8\r\n";
		$message .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
		$message .= $text . "\r\n\r\n";
		// <<< ==============
		 
		// ����������� ����� ===>>>
		if(is_array($attach) && count($attach))
		foreach($attach as $key => $filename){
			$fileContent = @file_get_contents($filename,true);
			$imginfo = @getimagesize($filename);
			if( !is_numeric($key) ){
				$filename = $key;
			}else{
				$filename=basename($filename);
			}
			$message.="--$newboundary\r\n";
			
			
			if(!is_array($imginfo)){
				$mimeType = 'application/octet-stream;';
				$message.="Content-Type: $mimeType;\r\n";
				$message.=" name=\"$filename\"\r\n";
				$message.="Content-Transfer-Encoding: base64\r\n";
				$message.="Content-Disposition: attachment;\r\n";
				$message.=" filename=\"$filename\"\r\n\r\n";
				$message.=chunk_split(base64_encode($fileContent));
			}else{
				$mimeType = image_type_to_mime_type($imginfo[2]);
				$message.="Content-Type: $mimeType;\r\n";
				$message.=" name=\"$filename\"\r\n";
				$message.="Content-Transfer-Encoding: base64\r\n";
				$message.="Content-ID: <$filename>\r\n";
				$message.="Content-Disposition: inline;\r\n";
				$message.=" filename=\"$filename\"\r\n\r\n";
				$message.=chunk_split(base64_encode($fileContent));
			}
		}
		// <<< ====================
		 
		// ����������� ���� ������, ���������� �����������
		$message.="--$newboundary--\r\n\r\n";
		$message.="--$baseboundary--\r\n";
		 
		// �������� ������
		$result = @mail($to, $subject, $message , $headers);
		return $result;
		
	}


    /**
     * smtp_mail() - �������� ����������� ����� � ������������ ����� SMTP ������
     * v1.0.0
     * @param $data
     * @param array $attach
     * @param bool $convert
     * @return bool|string
     */
    static function smtp_mail ($data, $attach = array(), $convert = false )
    {
        global $_CONF;
        
        $mailSMTP = new SendMailSmtpClass($_CONF['smtp_login'], $_CONF['smtp_password'], $_CONF['smtp_server'], $_CONF['smtp_port'], "UTF-8");
        // $mailSMTP = new SendMailSmtpClass('�����', '������', '����', '����', '��������� ������');

		if ($convert) {
    		$text 		= iconv("windows-1251","utf-8",$data['body']);
    		$from 		= iconv("windows-1251","utf-8",$data['name']."<".$data['email'].">");
    		$to 		= iconv("windows-1251","utf-8",(!empty($data['mailto']))?$data['mailto']:TO);
    		$subject 	= iconv("windows-1251","utf-8",(!empty($data['subject']))?$data['subject']:'������ � ����� '.$_SERVER['HTTP_HOST']);
		 }else{
    		$text 		= $data['body'];
    		$from 		= $data['name']."<".$data['email'].">";
    		$to 		= (!empty($data['mailto']))?$data['mailto']:TO;
    		$subject 	= (!empty($data['subject']))?$data['subject']:'������ � ����� '.$_SERVER['HTTP_HOST'];
		  
		 }
         
        if (!empty($data['selfemail']))
            $to .= ',' . $data['selfemail'];
            
		// ����������� ����� ===>>>
        if(is_array($attach) && count($attach))
		foreach($attach as $key => $filename){
		  $mailSMTP->addFile($filename);
		}
 
        $from = array(
            $data['name'], // ��� �����������
            $data['email'] // ����� �����������
        );
         // ���������� ������
        $result =  $mailSMTP->send( $to, $data['subject'], $text, $from); 
        // $result =  $mailSMTP->send('���� ������', '���� ������', '����� ������', '����������� ������');
         
        if($result === true){
            return "Done";
        }else{
            return $result;
        }


    }

}

/**
* SendMailSmtpClass
* 
* ����� ��� �������� ����� ����� SMTP � ������������
* ����� �������� ����� SSL ��������
* ������������� �� �������� �������� yandex.ru, mail.ru � gmail.com, smtp.beget.com
*
* v 1.1
* ���������:
* - ����������� ������� ehlo � ����������, ���� �� ������ �� �������, �� ������ helo
* - ������ � ����������� utf-8 � windows-1251
* - ����������� �������� ���������� �����������
* - �������������� ������������ ���������� ������
* - ����������� �������� ������ � ������
* 
* @author Ipatov Evgeniy <admin@vk-book.ru>
* @version 1.1
*/
class SendMailSmtpClass {

    /**
    * 
    * @var string $smtp_username - �����
    * @var string $smtp_password - ������
    * @var string $smtp_host - ����
    * @var string $smtp_from - �� ����
    * @var integer $smtp_port - ����
    * @var string $smtp_charset - ���������
    *
    */   
    public $smtp_username;
    public $smtp_password;
    public $smtp_host;
    public $smtp_from;
    public $smtp_port;
    public $smtp_charset;
	public $boundary;
    public $addFile = false;
    public $multipart;
    
    public function __construct($smtp_username, $smtp_password, $smtp_host, $smtp_port = 25, $smtp_charset = "utf-8") {
        $this->smtp_username = $smtp_username;
        $this->smtp_password = $smtp_password;
        $this->smtp_host = $smtp_host;
        $this->smtp_port = $smtp_port;
        $this->smtp_charset = $smtp_charset;
		
		// ����������� ������
		$this->boundary = "--".md5(uniqid(time()));
		$this->multipart = "";
    }
    
    /**
    * �������� ������
    * 
    * @param string $mailTo - ���������� ������
    * @param string $subject - ���� ������
    * @param string $message - ���� ������
    * @param string $smtp_from - �����������. ������ � ������ � e-mail
    *
    * @return bool|string � ������ �������� ������ true, ����� ����� ������    
	*
    */
    function send($mailTo, $subject, $message, $smtp_from) {		
		// ���������� ����������� ������ � ��������
		$contentMail = $this->getContentMail($subject, $message, $smtp_from, $mailTo);		
        
        try {
            if(!$socket = @fsockopen($this->smtp_host, $this->smtp_port, $errorNumber, $errorDescription, 30)){
                throw new Exception($errorNumber.".".$errorDescription);
            }
            if (!$this->_parseServer($socket, "220")){
                throw new Exception('Connection error');
            }
			
			$server_name = $_SERVER["SERVER_NAME"];
            fputs($socket, "EHLO $server_name\r\n");
			if(!$this->_parseServer($socket, "250")){
				// ���� ������ �� ������� �� EHLO, �� ���������� HELO
				fputs($socket, "HELO $server_name\r\n");
				if (!$this->_parseServer($socket, "250")) {				
					fclose($socket);
					throw new Exception('Error of command sending: HELO');
				}				
			}
			
            fputs($socket, "AUTH LOGIN\r\n");
            if (!$this->_parseServer($socket, "334")) {
                fclose($socket);
                throw new Exception('Autorization error');
            }
			
            fputs($socket, base64_encode($this->smtp_username) . "\r\n");
            if (!$this->_parseServer($socket, "334")) {
                fclose($socket);
                throw new Exception('Autorization error');
            }
            
            fputs($socket, base64_encode($this->smtp_password) . "\r\n");
            if (!$this->_parseServer($socket, "235")) {
                fclose($socket);
                throw new Exception('Autorization error');
            }
			
            fputs($socket, "MAIL FROM: <".$this->smtp_username.">\r\n");
            if (!$this->_parseServer($socket, "250")) {
                fclose($socket);
                throw new Exception('Error of command sending: MAIL FROM');
            }
            
			$mailTo = str_replace(" ", "", $mailTo);
			$emails_to_array = explode(',', $mailTo);
			foreach($emails_to_array as $email) {
				fputs($socket, "RCPT TO: <{$email}>\r\n");
				if (!$this->_parseServer($socket, "250")) {
					fclose($socket);
					throw new Exception('Error of command sending: RCPT TO');
				}
			}
			
            fputs($socket, "DATA\r\n");     
            if (!$this->_parseServer($socket, "354")) {
                fclose($socket);
                throw new Exception('Error of command sending: DATA');
            }
            
            fputs($socket, $contentMail."\r\n.\r\n");
            if (!$this->_parseServer($socket, "250")) {
                fclose($socket);
                throw new Exception("E-mail didn't sent");
            }
            
            fputs($socket, "QUIT\r\n");
            fclose($socket);
        } catch (Exception $e) {
            return  $e->getMessage();
        }
        return true;
    }
	
		
	// ���������� ����� � ������
	public function addFile($path){
		$file = @fopen($path, "rb");
		if(!$file) {
			throw new Exception("File `{$path}` didn't open");
		}		
		$data = fread($file,  filesize( $path ) );
		fclose($file);
		$filename = basename($path);
        $multipart = '';
		$multipart .=  "\r\n--{$this->boundary}\r\n";   
		$multipart .= "Content-Type: application/octet-stream; name=\"$filename\"\r\n";   
		$multipart .= "Content-Transfer-Encoding: base64\r\n";   
		$multipart .= "Content-Disposition: attachment; filename=\"$filename\"\r\n";   
		$multipart .= "\r\n";
		$multipart .= chunk_split(base64_encode($data));  
        
		$this->multipart .= $multipart;
		$this->addFile = true;		
	}
    
	// ������� ������ �������
    private function _parseServer($socket, $response) {
        $responseServer = '';
        while (@substr($responseServer, 3, 1) != ' ') {
            if (!($responseServer = fgets($socket, 256))) {
                return false;
            }
        }
        if (!(substr($responseServer, 0, 3) == $response)) {
            return false;
        }
        return true;
    }
	
	// ���������� ����������� ������
	private function getContentMail($subject, $message, $smtp_from, $smtp_to = ''){	
		// ���� ��������� windows-1251, �� ������������ ����
		if( strtolower($this->smtp_charset) == "windows-1251" ){
			$subject = iconv('utf-8', 'windows-1251', $subject);
		}
        $contentMail = "Date: " . date("D, d M Y H:i:s") . " UT\r\n";
        //$contentMail .= 'Subject: =?' . $this->smtp_charset . '?B?'  . base64_encode($subject) . "=?=\r\n";
		$contentMail .= "Subject: $subject\r\n";
        
		// ��������� ������
		$headers = "MIME-Version: 1.0\r\n";
		// ��������� ������
		if($this->addFile){
			// ���� ���� �����
			$headers .= "Content-Type: multipart/mixed; boundary=\"{$this->boundary}\"\r\n"; 
		}else{
			$headers .= "Content-type: text/html; charset={$this->smtp_charset}\r\n"; 			
		}
		$headers .= "From: {$smtp_from[0]} <{$smtp_from[1]}>\r\n"; // �� ���� ������
        if ($smtp_to )
            $headers .= "To: $smtp_to\r\n"; // ����
            
        $contentMail .= $headers . "\r\n";
		
		if($this->addFile){
			// ���� ���� �����
			$multipart  = "--{$this->boundary}\r\n";   
			$multipart .= "Content-Type: text/html; charset=utf-8\r\n";   
			$multipart .= "Content-Transfer-Encoding: base64\r\n";   
			$multipart .= "\r\n";
			$multipart .= chunk_split(base64_encode($message)); 
			
			// �����
			$multipart .= $this->multipart; 
			$multipart .= "\r\n--{$this->boundary}--\r\n";
			
			$contentMail .= $multipart;
		}else{
			$contentMail .= $message . "\r\n";
		}
		
		// ���� ��������� windows-1251, �� ��� ������ ������������
		if( strtolower($this->smtp_charset) == "windows-1251" ){
			$contentMail = iconv('utf-8', 'windows-1251', $contentMail);
		}
		
		return $contentMail;
	}
	
}

