<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : class.cForm.php
// PROJECT NAME : BASE Work
// DATE         : 17.03.2002  (Last UpDate: 08.09.2005 )
// Author       : Andrew Lyadkov
// Discription  : For pleasant working with html forms
// ...........  :
// Comment      : Ver: 2.0
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
/*

class cForm DESCRIPTION:

VARS:
-----
		var $Error;					//  Last Error Message
		var $Bug_mes;				//  Message for caching bugs
		var $Arr = array();			//  Array wich we using

		// CSS
		var $Form_TD_title = array(		// Body_print <TD>title</TD> ATTRIBUTE
			'class'	=>	'',
			'align'	=>	'',
			'bgcolor'	=> ''
		);
		var $Form_TD_val = array(		// Body_print <TD>values</TD> ATTRIBUTE
			'class'	=>	'',
			'align'	=>	'',
			'bgcolor'	=> ''
		);

FUNCTIONS:
----------
		PUBLIC:
			cForm( & $Array ) - Constructor
			Body_print ( )
			Chk_come ( )
			GetSQL ( $action = 'insert' )
			Print_come ( $how='column', $what='', $titlef = '' )
			Print_styled ( &$styles, &$table = '' )

		PRIVATE;
			is_must( $val )
			show ( $field, $showval=1 )
			Conv_str ( $string )
			global_val( $name )
			get_key( $string )
			chk_photo( &$field )
			copy_file( & $field )

USAGE:
------
	$Arr = array( element = array () )
	==================================
	[COMMON]
		'title'	=> string,
		'name'	=> string,
		'must'	=> [ 1 | 0 ],
		'type'	=> 'textbox | password | hidden | textarea | select | checkbox | file | photo'
		'func'	=> string = eval( 'func' ) - must print on page
*/

class cForm
{
	var $Error;					//  Last Error Message
	var $Bug_mes;				//  Message for caching bugs
	var $Arr = array();			//  Array wich we using
	var	$NoTag = false;			// strip_tags or not
	var $EnabledTags	= "<b><a><i><h1>";
	var $skipfoto;
	var $uniqueId;
	
//	var
	var $tabs = false;
	var $tabsCounter = 0;

	// CSS
	var $Form_TD_title = array(		// Body_print <TD>title</TD> ATTRIBUTE
		'class'	=>	'',
		'align'	=>	'left',
		'bgcolor'	=> ''
	);
	var $Form_TD_val = array(		// Body_print <TD>values</TD> ATTRIBUTE
		'class'	=>	'',
		'align'	=>	'left',
		'bgcolor'	=> ''
	);
	var $Print_Table = array(		// Print_come <TABLE ATTRIBUTES>
		'width'	=> '',
		'spacing'	=> '1',
		'padding'	=> '3',
		'border'	=> '0'
	);


	///////////////////////////////////////////////////////////////////// cForm
	function cForm( & $Array, $NoTag = false ) // CONSTRUCTOR
	{
		$this->show_cwas_cal	= false;
		$this->Arr = $Array;
		$this->NoTag = $NoTag;
		$this->uniqueId = substr( uniqid(), -5);// substr( md5(json_encode($Array)), -5);
	}

	 ///////////////////////////////////////////////////// Body_print ()
	// -----------------------------------------------------------------------
	// $form_arr = array ( array( 'title', 'func', 'must', 'name', 'type' ....
	// Include: is_must(), show()
	// 17.03.2002
	// PUBLIC
	function Body_print ( $val='' )
	{  
		/*For tabs*/
		$this->tabs = '';
		$this->tabsCounter = 0;
		if ( empty($val) && is_array($this->Arr) ) {
			foreach ($this->Arr as $key => $value) {
				if ($value['type'] == 'delimeter_line' && ($value['subtype'] == 'tabs' || $this->tabsCounter>0)) {
					$this->tabsCounter++;
					$this->tabs .= "<li><a href=\"#tabs-{$this->tabsCounter}\">{$value['title']}</a></li>";
				}
			}
		}
		if ($this->tabsCounter) {
			echo '</table><div id="tabs">';
			echo "<ul>{$this->tabs}</ul>";
			$this->tabsCounter = 0;
			$this->tabs = '';
		}
		/*End of for tabs*/
		
		if (empty($val)&&is_array($this->Arr)) {
            $js_vars = array();
			foreach ($this->Arr as $key => $val)
			{
				$this->check_dependence($key, $js_vars);

				ob_start();
				$FormItem	= (empty($val['func'])) ? $this->show($val) :  eval($val['func']);
				$t = ob_get_contents();
				ob_end_clean();
				if (empty($FormItem))
					$FormItem = $t;
                	if ($val['type'] == 'matrix' && !$this->global_val( 'form[id]' ))
                        continue;
				if ($val['type'] == 'delimeter_line' && ($val['subtype'] == 'tabs' || $this->tabsCounter>0)) {
					echo $this->tabsCounter ? '</table></p></div>' : '';
					$this->tabsCounter++;
					echo "<div id=\"tabs-{$this->tabsCounter}\"><p><TABLE BORDER=0 width='100%' cellspacing=1 cellpadding=3 class=\"form\">";
				}elseif ($val['type'] == 'textarea' || $val['linebyline'] || ($val['type'] == 'multiselect_from_table' && $val['subtype'] == 'checkbox')){
					echo "<TR " .@$val['tr_class'].' data-unique = "'.$this->uniqueId.'_'.$key.'"' ."><TD valign=top colspan=2 ";
					echo (!empty($this->Form_TD_title['class'])) ? ' class = '.$this->Form_TD_title['class'] : '';
					echo (!empty($this->Form_TD_title['align'])) ? ' align = '.$this->Form_TD_title['align'] : '';
					echo (!empty($this->Form_TD_title['bgcolor'])) ? ' bgcolor = '.$this->Form_TD_title['bgcolor'] : '';
					echo  ">".$this->is_must($val).$val['title'].'<BR>';
					echo $FormItem;
					echo  "</TD></TR>";
				}elseif ($val['type'] != 'hidden'){
					echo "<TR " .@$val['tr_class']. ' data-unique = "'.$this->uniqueId.'_'.$key.'"' .">";

					if ($val['title_position'] != 'right'){
                        
                        if ( $val['type'] == 'matrix' ){
						$title = "<TD " .@$val['tdvar_class']." valign=top nowrap";
    						$title .=  (!empty($this->Form_TD_title['class'])) ? ' class = '.$this->Form_TD_title['class'] : '';
    						$title .=  (!empty($this->Form_TD_title['align'])) ? ' align = '.$this->Form_TD_title['align'] : '';
    						$title .=  (!empty($this->Form_TD_title['bgcolor'])) ? ' bgcolor = '.$this->Form_TD_title['bgcolor'] : '';
    						$title .=   "></TD>";
    						
    						$value = "<TD " .@$val['tdval_class']."";
    						$value .=  (!empty($this->Form_TD_val['class'])) ? ' class = '.$this->Form_TD_val['class'] : '';
    						$value .=  (!empty($this->Form_TD_val['align'])) ? ' align = '.$this->Form_TD_val['align'] : '';
    						$value .=  (!empty($this->Form_TD_val['bgcolor'])) ? ' bgcolor = '.$this->Form_TD_val['bgcolor'] : '';
    						$value .=  ">";
    						$value .=  $FormItem;
    						$value .=   "</TD>";
				        } else {
    						$title = "<TD " .@$val['tdvar_class']." valign=top nowrap";
    						$title .=  (!empty($this->Form_TD_title['class'])) ? ' class = '.$this->Form_TD_title['class'] : '';
    						$title .=  (!empty($this->Form_TD_title['align'])) ? ' align = '.$this->Form_TD_title['align'] : '';
    						$title .=  (!empty($this->Form_TD_title['bgcolor'])) ? ' bgcolor = '.$this->Form_TD_title['bgcolor'] : '';
    						$title .=   ">".$this->is_must($val).$val['title']."</TD>";
    						
    						$value = "<TD " .@$val['tdval_class']."";
    						$value .=  (!empty($this->Form_TD_val['class'])) ? ' class = '.$this->Form_TD_val['class'] : '';
    						$value .=  (!empty($this->Form_TD_val['align'])) ? ' align = '.$this->Form_TD_val['align'] : '';
    						$value .=  (!empty($this->Form_TD_val['bgcolor'])) ? ' bgcolor = '.$this->Form_TD_val['bgcolor'] : '';
    						$value .=  ">";
    						$value .=  $FormItem;
    						$value .=   "</TD>";
                        }

					} else {
						$title = "<TD></TD>";

						$value = "<TD " .@$val['tdval_class']."";
						$value .=  (!empty($this->Form_TD_val['class'])) ? ' class = '.$this->Form_TD_val['class'] : '';
						$value .=  (!empty($this->Form_TD_val['align'])) ? ' align = '.$this->Form_TD_val['align'] : '';
						$value .=  (!empty($this->Form_TD_val['bgcolor'])) ? ' bgcolor = '.$this->Form_TD_val['bgcolor'] : '';
						$value .=  ">";
						$value .=  $FormItem . $val['title'] . $this->is_must($val) ;

						if ((!empty($val['example']) || !empty($val['example_pref']))) {
							$value .= "<div class=example>" . @$val['example_pref'] . '<b class=inside>' . $val['example'] . '</b>' . @$val['example_suf'] . '</div>';
						}

						$value .=   "</TD>";
					}

					echo $title . $value;


					echo "</TR>";
				}else
					echo $FormItem;
			}
			if(!empty($js_vars)) {
			    $js_vars['unique_id'] = $this->uniqueId;
			    echo '<script>';

                echo "var js_vars_".$this->uniqueId." = '".json_encode($js_vars)."';";

                echo "function dependences_".$this->uniqueId."(form_field) { \n\r";
                echo "    var obj = jQuery.parseJSON(js_vars_".$this->uniqueId.");\n\r";
                echo "    var field = form_field.substr(5, form_field.length - 6);\n\r";

                echo "    for (key_depend in obj.listen[field]) { \n\r";
                echo "            key_depend = obj.listen[field][key_depend];\n\r";

                echo "        var show = true; var show_item;\n\r";
                echo "        for (key in obj.depend[key_depend]) { \n\r";
                echo "            show_item = false; ";
                echo "            var value = $('[data-unique = ".$this->uniqueId."_'+key+'] [name *='+key+']').eq(0).val();\n\r";
                echo "            if(typeof obj.depend[key_depend][key] == 'object'){ \n\r";
                echo "                for (j in obj.depend[key_depend][key]) { \n\r";
                echo "                    if(obj.depend[key_depend][key][j] == value){ \n\r";
                echo "                        show_item = true; \n\r";
                echo "                    } \n\r";
                echo "                } \n\r";
                echo "            }else{ \n\r";
                echo "                if(obj.depend[key_depend][key] == value){ \n\r";
                echo "                    show_item = true; \n\r";
                echo "                }\n\r";
                echo "            }\n\r";
                echo "            if( show_item == false || show == false ){ \n\r";
                echo "                show = false; \n\r";
//                echo "              alert('�� ����������');\n\r";
                echo "            }\n\r";
               // echo "            alert(typeof obj.depend[key_depend][key]);\n\r";
                echo "        }\n\r";
                echo "        if(show == true ){ \n\r";
                echo "            $('[data-unique = ".$this->uniqueId."_'+key_depend+']').show(); \n\r";
                echo "        }else{ \n\r";
                echo "            $('[data-unique = ".$this->uniqueId."_'+key_depend+']').hide(); \n\r";
                echo "        } \n\r";
                echo "    }\n\r";
//                echo "    alert('����������� ����������� ��� ����� ".$this->uniqueId." �� '+field);\n\r";
                echo "} \n\r";

                echo "$(function() { ";
                echo "    var obj = jQuery.parseJSON(js_vars_".$this->uniqueId.");";
                echo "    for (key in obj.listen) {";
                echo "       $('[data-unique = '+obj.unique_id+'_'+key+']').change(function(el){ ";
                echo "           dependences_".$this->uniqueId."(el.target.name, el.target.value);";
                echo "       });";
                echo "       dependences_".$this->uniqueId."('form['+key+']');";
                echo "    }";
                echo "}) ";


			    echo '</script>';
			    //echo "<tr><td colspan='2'><pre>".print_r($js_vars,1)."</pre>";
            }
		} elseif (!empty($this->Arr[$val])) {
				$val = $this->Arr[$val];
				ob_start();
				$FormItem	= (empty($val['func'])) ? $this->show($val) :  eval($val['func']);
				$t = ob_get_contents();
				ob_end_clean();
				if (empty($FormItem))
					$FormItem = $t;
				if ($val['type'] == 'multiselect_from_table' && $val['subtype'] == 'checkbox'){
					echo "<TR " .@$val['tr_class']."><TD valign=top colspan=2 ";
					echo (!empty($this->Form_TD_title['class'])) ? ' class = '.$this->Form_TD_title['class'] : '';
					echo (!empty($this->Form_TD_title['align'])) ? ' align = '.$this->Form_TD_title['align'] : '';
					echo (!empty($this->Form_TD_title['bgcolor'])) ? ' bgcolor = '.$this->Form_TD_title['bgcolor'] : '';
					echo  ">".$this->is_must($val).$val['title'].'<BR>';
					echo $FormItem;
					echo  "</TD></TR>";
				}elseif ($val['type'] != 'hidden'){
					echo "<TD valign=top ";
					echo (!empty($this->Form_TD_title['class'])) ? ' class = '.$this->Form_TD_title['class'] : '';
					echo (!empty($this->Form_TD_title['align'])) ? ' align = '.$this->Form_TD_title['align'] : '';
					echo (!empty($this->Form_TD_title['bgcolor'])) ? ' bgcolor = '.$this->Form_TD_title['bgcolor'] : '';
					echo  ">".$this->is_must($val).$val['title']."</TD><TD ";
					echo (!empty($this->Form_TD_val['class'])) ? ' class = '.$this->Form_TD_val['class'] : '';
					echo (!empty($this->Form_TD_val['align'])) ? ' align = '.$this->Form_TD_val['align'] : '';
					echo (!empty($this->Form_TD_val['bgcolor'])) ? ' bgcolor = '.$this->Form_TD_val['bgcolor'] : '';
					echo ">";
					echo $FormItem;
					echo  "</TD>";
				}else
					echo $FormItem;
		}
		
		if ($this->tabsCounter) {
			echo '</table></p></div>';
			global $_CORE;
			echo '<link  href="' . $_CORE->TEMPLATES . 'admin/JS/jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" type="text/css" rel="stylesheet" />';
			echo '<script src="' . $_CORE->TEMPLATES . 'admin/JS/jquery/js/jquery-1.6.2.min.js" type="text/javascript"></script>';
			echo '<script src="' . $_CORE->TEMPLATES . 'admin/JS/jquery/js/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>';
			echo '<script type="text/javascript">	$(document).ready(function(){  $("#tabs").tabs(); }); </script>';
			echo '</div><table>';
			$this->tabsCounter = 0;
		}
	} //func


	/**
	 * Checking Incoming form data for "MUST BE" fields.
	 *
	 * 17.03.2002 (01.10.02(in new style))
	 * Update: 11.02.2010 ���������
	 * @uses cForm::global_val();
	 * @return bool
	 */
	function Chk_come ( )
	{
		global $_CORE;
		
		$BAD = false;
//		$this->Error = "<B>������ ����������:</B>\n"; // string
		foreach ($this->Arr as $key => $val)
		{

			// if select_from_tree
			if ( !empty($val['must']) &&$val['type'] == 'select_from_tree') {
				$value	= $this->global_val( $val['name'] );
				if ( !isset($value) ) {	$this->Error .= "Empty : ".$val['title'].".<BR>";	$BAD = true; continue;	}
				$pid	= $this->Get_pid_fet( $val );
				$res	= SQL::sel( $val['id_ex_table'], $val['ex_table'], (!empty($pid)?$val['parent_id_ex_table']."='".$pid."' AND ":"").$val['ex_table_field']."='".$value."'", '', DEBUG );
				if ($res->NumRows==0) {
					$err = "Field is empty: ".$val['title']."<BR>";
					$this->Error .= $err;
					$this->Arr[$key]['example_pref'] = ($_CORE->lang['field_empty']) ? $_CORE->lang['field_empty'] : $err;
					$this->Arr[$key]['tdval_class'] = 'class=error';					
					$BAD = false;
				} elseif ($res->NumRows>1) {
					$err = "Dublicate field: ".$val['title']."<BR>";
					$this->Error .= $err;
					$this->Arr[$key]['example_pref'] = ($_CORE->lang['field_dublicate']) ? $_CORE->lang['field_empty'] : $err;
					$this->Arr[$key]['tdval_class'] = 'class=error';					
					$BAD = false;
				}
				continue;
			}

			// if multcheckbox
			if ($val['type'] == 'multcheckbox'){
				if (empty($val['must'])) continue;
				$tmp_bad = true;
				for($i = 0; $i < sizeof($val['arr']); $i++ ){
					$value = $this->global_val( str_replace("]","$i]", $val['name']) );
					$tmp_bad = $tmp_bad && empty($value);
				}
				if ($tmp_bad) {
					$err .= "Empty: ".$val['title'].".<BR>"; // string
					$this->Error .= $err;
					$this->Arr[$key]['example_pref'] = $err;
					$this->Arr[$key]['tdval_class'] = 'class=error';					
				}
				$BAD |= $tmp_bad;
				continue;
			}

			$value = $this->global_val( $val['name'] );

			if ($val['make_value_func']) {
				eval('$value = '.$val['make_value_func'].';' );
			}

			if ( !empty($val['must']) && empty($value) ) {
				$err = "Empty : ".$val['title']."<BR>"; // string
				$this->Error .= $err;
				$this->Arr[$key]['example_pref'] = ($_CORE->lang['field_empty']) ? $_CORE->lang['field_empty'] : $err;
				$this->Arr[$key]['tdval_class'] = 'class=error';					
				$BAD = true;
			}
			if ( isset($val['maxval']) && $val['maxval'] < (int)$value ) {
				$err = $val['title'] . ": entered value is greater than the maximum possible value!<br/>";
				$this->Error .= $err;
				$this->Arr[$key]['example_pref'] = ($_CORE->lang['field_empty']) ? $_CORE->lang['field_empty'] : $err;
				$this->Arr[$key]['tdval_class'] = 'class=error';					
				$BAD = true;
			}
			$BAD = ($BAD || ( $val['type'] == 'photo' && !$this->chk_photo( $val ) ));
		}
		return $BAD;
	} //func


	/**
	 * Make SQL Query String by using incomming form information.
	 * $action = [update|insert]; if !update it`s mean: insert
	 *
	 * @uses cForm::global_val()
	 * @uses cForm::get_key()
	 * @uses cForm::Conv_str()
	 * @param string $action
	 * @return unknown
	 */
	function GetSQL ( $action = 'insert' )
	{
		global $CHATSET, $SOURCE_CHARSET;
		$empty = 1;
		$sql_vars = $sql_vals = $sql_update = '';
		foreach ($this->Arr as $key => $val)
		{
			if ($val['type'] == 'delimeter_line') continue;
            if ($val['type'] == 'matrix') continue;
            if ($val['disabled']) continue;

			if ($val['type']=='select_popup'  || $val['sub_type']=='autocomplete') {
				if ($val['ex_table']!='ex_table') {
					$value = $this->Get_id_from_ex_table( $val, array( $val['ex_table_field'] => $this->global_val($val['name'])),0);
				} else {
					$val['ex_table'] 	= $this->global_val($this->Arr['ex_table']['name']);
					$val['id_ex_table'] = $this->Get_name_id_from_ex_table( $val['ex_table'] );
					$value = $this->Get_id_from_ex_table( $val, array( $val['ex_table_field'] => $this->global_val($val['name'])),1);
				}
			}elseif ($val['type']=='select_from_tree') { // becose tree has two parameters (parent_id, name). Added by Shesternin
				$tmp=$this->global_val($val['name']);
				if( !empty($tmp)) $value = $this->Get_id_from_ex_table( $val, array( $val['ex_table_field'] => $tmp, $val['parent_id_ex_table'] => $this->Get_pid_fet($val) ),0);
			} else {
                $value = $this->global_val($val['name']);
            }

            // If sub_type = int
            if(isset($val['sub_type']) && $val['sub_type'] == 'int'){
                if(!empty($value))
                    $value = preg_replace ("/\D/","",$value);
            }

			if ($val['make_value_func']) {
				eval('$value = '.$val['make_value_func'].';' );
			}
			// Checking Char-Coding
			if (!empty($CHARSET) && !empty($SOURCE_CHARSET) && $CHARSET != $SOURCE_CHARSET)
				$value = convert_cyr_string ($value, $CHARSET, $SOURCE_CHARSET);
			// Go next
			if (!empty( $value ) || $val['type'] == 'multcheckbox' || $action == 'update')
			{
				if (empty($val['parse_getin'])) // if don`t need any parsing before
				{
					if ($val['type'] == 'photo' || $val['type'] == 'file') // Photo comin
					{
//						echo "<pre>";
//						print_r($_POST);
//						print_r($_FILES);
//						print_r($val);
//						echo($value);
						if (!empty($value) && !$this->copy_file( $val )){ # bad coping
							return ($action == 'update' ) ? false : array(false,$this->Error);
						}
						// If come here, so the name of a picture already changed
						$value = $this->global_val ( $val['name'] );
						//print $value;die();
					}
	
					// multicheckbox type
					if ($val['type'] == 'multcheckbox') {
						$name = (empty($val['field_name'])) ? $this->get_key( $val['name'] ) : $val['field_name'];
						# thought all arr 
						for ($i = 0; $i < sizeof($val['arr']); $i++) {
							$field_name = $name.$i;
							$value = $this->Conv_str( $this->global_val ( str_replace("]","$i]",$val['name']) ) );

							$sql_vars .= ($empty) ? "`".$field_name."`" : ",`".$field_name."`";
							$sql_vals .= ($empty) ? "'".$value."'" : ",'".$value."'";
							$this->Arr[$field_name]['value'] = $value;
							$sql_update .= ($empty) ? sprintf(" `%s` = '%s'", $field_name, $value ) : sprintf(", `%s` = '%s'", $field_name, $value );
							$empty = 0;
						}

					// default
					}else{
						$field_name = (empty($val['field_name'])) ? $this->get_key( $val['name'] ) : $val['field_name'];
						$value = $this->Conv_str( $value );
						if (($val['type'] == 'photo' || $val['type'] == 'file' ) && $this->skipfoto[$val['field_name']]) true;
						else
						{
							$sql_vars .= ($empty) ? "`".$field_name."`" : ",`".$field_name."`";
							$sql_vals .= ($empty) ? "'".$value."'" : ",'".$value."'";
							$this->Arr[$key]['value'] = $value;
							$sql_update .= ($empty) ? sprintf(" `%s` = '%s'", $field_name, $value ) : sprintf(", `%s` = '%s'", $field_name, $value );
							$empty = 0;
						}
					}
				}//if val[parse_getin]

			}//if empty
		}//foreach
	//	print_r($this->Arr);
	//	die('!!!!!');
//		echo "Upd: $sql_update ins $sql_vars DATA $sql_vals";
//		exit;
		$this->Error = '';
		return ($action == 'update' ) ? $sql_update : array($sql_vars,$sql_vals);
	} //func

	 ///////////////////////////////////////////////////// Print_come ()
	// -----------------------------------------------------------------------
	// $form_arr = array ( array( 'title', 'name' ) )
	// Include: global_val
	// 17.03.2002
	// PUBLIC
	function Print_come ( $how='column', $what='', $titlef = '' )
	{
		$str = "<TABLE ";
		$str .= (empty($this->Print_Table['border'])) ? '' : " BORDER=\"".$this->Print_Table['border']."\"";
		$str .= (empty($this->Print_Table['width'])) ? '' : " WIDTH=\"".$this->Print_Table['width']."\"";
		$str .= (empty($this->Print_Table['spacing'])) ? '' : " cellspacing=\"".$this->Print_Table['spacing']."\"";
		$str .= (empty($this->Print_Table['padding'])) ? '' : " cellpadding=\"".$this->Print_Table['padding']."\"";
		$str .= ">\n";
		foreach ($this->Arr as $val)
		{
			$value = $this->global_val( $val['name'] );
			// ��������� ��������� + ������� � ������ ���������(���� ������� ����)
			if (!empty($value) && (is_array($what) && in_array($val['field_name'], $what) || !is_array($what)))
			{
				$str .= ($how=='column') ? '<TR>' : '';
				switch ($val['type'])
				{
					case 'photo':
						$str .= "<TD align=center colspan=2><IMG SRC=\"".$val['path']."/".$value."\" BORDER=0 >";
						break;
					case "textarea":
//						$str .= "<TD colspan=2><B>".$val['title']."</B></td></tr>";
						$str .= "<TD colspan=2>".$value."</td></tr>";
						break;
					case "select":
						$value	= $val['arr'][$value];
					default :
						$str .= ($how=='column') ? "<TD valign=top align=right nowrap><B>".$val['title']."</B><TD width=100%>" : "<TD>";
						if (!empty($val['get_func']))
							eval ($val['get_func']);
						$value = (is_array($titlef) && !empty($titlef[$val['field_name']])) ? sprintf($titlef[$val['field_name']],$value) : $value;
						$str .= $value."\n";
				}//switch
			}//if
		}//foreach
		$str .= "</TABLE>";
		return $str;
	} //func

	 ///////////////////////////////////////////////////// Print_come_styled ()
	// -----------------------------------------------------------------------
	// Include: global_val
	// 10.10.02
	// PUBLIC
	function Print_styled ( &$styles, &$tab_style )
	{
		if (!is_array($tab_style)) $tab_style = $this->Print_Table;
		// TABLE
		$str = '';
		if (!empty($tab_style['draw_table']))
		{
			$str = "<TABLE ";
			$str .= (empty($tab_style['border'])) ? '' : " BORDER=\"".$tab_style['border']."\"";
			$str .= (empty($tab_style['width'])) ? '' : " WIDTH=\"".$tab_style['width']."\"";
			$str .= (empty($tab_style['spacing'])) ? '' : " cellspacing=\"".$tab_style['spacing']."\"";
			$str .= (empty($tab_style['padding'])) ? '' : " cellpadding=\"".$tab_style['padding']."\"";
			$str .= ">\n";
		}
		// OTHER
		foreach ($styles as $name => $cont)
		{
			$val = $this->Arr[$name];
			$value = $this->global_val( $val['name'] );
			if (!empty($value)&&str_replace(" ",'',$value) !='')
			{
				if (!empty($val['get_func']))
					eval ($val['get_func']);
				if (empty($cont['var_val']))
					$cont['var_val'] = '';
				switch  ($cont['var_val'])
				{
					case 'val'	:
							$str .= sprintf($cont['printf'],$value);
							break;
					case "val_val":
							$str .= sprintf($cont['printf'],$value,$value);
							break;
					case 'dbval' :
							$str .= sprintf($cont['printf'],$val['title'],$value, $value);
							break;
					default:
							$str .= sprintf($cont['printf'],$val['title'],$value);
				}//switch
			}elseif(isset($cont['alt'])){
				$str .= $cont['alt'];
			}//if
		}//foreach

		if (!empty($tab_style['draw_table']))
			$str .= "</TABLE>";
		return $str;
	} //func


	 ////////////////////////////////////////////////////// Is_must ()
	//
	// PRIVATE
	function is_must( $val )
	{
		return (empty($val['must'])) ? '' : '<span class="star">*&nbsp;</span>';
	}// func


	/**
	 * Return HTML-code in string by using incoming form information.
	 *
	 *	textbox		- (need $field [ type, name, size, maxlen, hideval],  $showval )
	 *	password	- (need $field [ type, name, size, maxlen, hideval],  $showval )
	 *	hidden		- (need $field [ type, name ] )
	 *	textarea	- (need $field [ type, name, rows, cols, hideval],  $showval )
	 *	select		- (need $field [ type, name, arr, hideval ] ), $showval )
	 *	checkbox	- (need $field [ type, name, hideval ], $showval)
	 *	file		- (need $field [ type, name, hideval ] )
	 *  select_from_table	- (need $field [ type, name, size, maxlen, hideval, ex_table, id_ex_table, ex_table_field],  $showval )
	 *  select_from_tree	- (need $field [ type, name, size, maxlen, hideval, ex_table, id_ex_table, ex_table_field, parent_id_ex_table],  $showval )
	 *  select_popup		- (need $field [ type, name, size, maxlen, hideval, ex_table, id_ex_table, ex_table_field],  $showval )
	 *
	 * @uses cForm::global_val()
	 * @uses cForm::Conv_str()
	 * @uses cForm::Load_from_ex_table()
	 * @uses cForm::Get_id_from_ex_table()
	 * @uses cForm::Get_pid_fet()
	 * @uses cForm::Get_name_parent_id_from_ex_table()
	 * @uses cForm::Get_name_id_from_ex_table()
	 * @param string $how
	 * @param string $what
	 * @param string $titlef
	 * @return string
	 */
	function show ( $field, $showval=1, $also = '' )
	{
		global $_CONF,$_CORE;
		$value = $this->global_val( $field['name'] );
		$value = (!isset($value)) ? '' : (empty($field['noconv_str']) && !is_array($value))?$this->Conv_str($value) : $value;
		$showval = (!isset($field['hideval']));
		if (!empty($field['htmlspecchars'])){
			$value = htmlspecialchars($value, ENT_QUOTES, CODEPAGE);
		}

		$str = '';
		switch ($field['type']) {
			case 'delimeter_line':
				$str = "<HR ";
				$str .= (!empty($field['class'])) ? ' CLASS = "'.$field['class'].'" ' : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
				$str .= (!empty($field['style'])) ? ' STYLE = "'.$field['style'].'" ' : '';
				$str .= '>';
				break;
			// Added by Shesternin for external tables-link
			// ---------------------------------   SELECT FROM EXTENDED TABLE
			case 'select_from_table':
				$str .= '<select name="'.$field['name'].'"';
				$str .= ' class = "';
				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
				$str .= '" ';

				$str .= (!empty($field['size'])) ? ' size="'.$field['size'].'" ' : '';
				$str .= (!empty($field['readonly'])) ? " disabled " : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
				$str .= (!empty($also)) ? $also : '';
				$str .= ">\n";

				if (!empty($field['default']) && empty($value)){
					$value = $field['default'];
				}

                // Load from database
				if (!isset($field['arr'])) {
					$field['arr'] = array();
					$this->Load_from_ex_table( $field, $field['arr'], '', $field['options'], DEBUG, $value );

				}
                // if Prompt text enabled .. or "..."
				if(isset($field['prompt'])){
					$str .= ( $value == $field['prompt'] )	? "<option selected value=\"\" >".$field['prompt']."</option>\n" : "<option value=\"\" >".$field['prompt']."</option>\n";
				}else{
				    $str .= ( $value == 0 )	? "<option selected value=\"0\" >...</option>\n" : "<option value=\"0\" >...</option>\n";
				}
                // Show the results
				foreach ( $field['arr']as $i=>$v )
					$str .= ( $value == $i && $showval )	? "<option selected value=\"$i\" >".$v."</option>\n" :
																	"<option value=\"$i\" >".$v."</option>\n";
				$str .= "</select>\n";
				break;
            case 'autocomplete':
                // Load from database
                if (!isset($field['arr'])) {
                    $field['arr'] = array();
                    $this->Load_from_ex_table($field, $field['arr'], '', $field['options'], DEBUG, $value);
                }
                $str .= '<span id="sel_'.$field['field_name'].'">';
                $str .= (!empty($field['arr'][$value]))?$field['arr'][$value]: '';
                $str .= '</span>';
                $str .= ' <a '.((empty($value))?'style="display: none;"':'').' href="javascript:;" id="reset_'.$field['field_name'].'" >(�������� �����)</a>' . "\n";
                $str .= '<input type="hidden" name="'.$field['name'].'"';
                $str .= ' id="field_'.$field['field_name'].'" ';
                $str .= ' value = "'.((!empty($value))?htmlspecialchars($value, ENT_QUOTES, CODEPAGE) : '').'"';
                $str .= " >\n";
                $str .= '<input type ="text" name="autocomplete_'.$field['field_name'].'" ';
                $str .= ' id= "autocomplete_'.$field['field_name'].'" placeholder="�����..." ';
                $str .= (!empty($field['size'])) ? ' size="'.$field['size'].'" ' : '';
                $str .= (!empty($field['maxlen'])) ? ' maxlength = "'.$field['maxlen'].'" ' : '';
                $str .= ' class = "';
                $str .= (!empty($field['class'])) ? $field['class'].' ' : '';
                $str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
                $str .= '" ';

                $str .= (!empty($field['style'])) ? ' style = "'.$field['style'].'" ' : '';
                $str .= (!empty($field['also'])) ? $field['also'] : '';

                $str .= " >\n";

                $str .= "<script>\n";

                $str .= 'var availableTags = [';
                $k = 0;
                foreach ( $field['arr']as $i=>$v ){
                    $k++;
                    $str .= '{label:"' . addslashes($v) . '",value:'.$i.'}' ;
                    if($k < count($field['arr'])) $str .= ','."\n";
                }
                $str .= '];';

                $str .= '
                $( "#autocomplete_'.$field['field_name'].'" ).autocomplete({'
                    .'source: availableTags,'
                    .'minLength: 2,'
                    .'select: function(event, ui) { '
                        .'$( "#field_'.$field['field_name'].'" ).val(ui.item.value);'
                        .'$(this).val("");'
                        .'$("#reset_'.$field['field_name'].'").show();'
                        .'$("#sel_'.$field['field_name'].'").text(ui.item.label); '
                        .'return false; '
                    .'}'
                .'});';
                $str .= '
                $("#reset_'.$field['field_name'].'").bind("click",function(){'
                    .'$("#reset_'.$field['field_name'].'").hide();'
                    .'$("#field_'.$field['field_name'].'" ).val("");'
                    .'$("#autocomplete_'.$field['field_name'].'").val("");'
                    .'$("#sel_'.$field['field_name'].'").text("");'
                .'})';
                $str .= "</script>\n";


                break;
      // Added by Ulyusov for external tables-link 
			// --------------------------------- MULTISELECT FROM EXTENDED TABLE
			case 'multiselect_from_table':
				if (!empty($field['default']) && empty($value)){
					$value = $field['default'];
				}
				
				if (!empty($value) && !is_array($value)){ // if in filter form, we have array allready
					$value = explode(',',$value);
				}

				if (!isset($field['arr'])) {
					$field['arr'] = array();
					$this->Load_from_ex_table( $field, $field['arr'], '', $field['options'], DEBUG);
				}

				//$str .= ( $value == 0)	? "<OPTION SELECTED value=\"0\" >-----------\n" : "<OPTION value=\"0\" >----------\n";
				if (empty($value)){
					$value = array();
				}
        if(isset($field['subtype'])&& $field['subtype']=='checkbox'){
					$str .= '<table width="100%"  border="0" cellspacing="0" cellpadding="0">
								<tr>';
          $k=0;
					foreach ( $field['arr']as $i=>$v ){
						$in_row = (!empty($field['inrow']))?$field['inrow']:3;
						$str .= ($k % (int)$in_row == 0
								&& $k > 0
								&& $k < sizeof($field['arr'])) 
								? "</tr><tr>"
								: '';
						$str .= "<td>";
            
						$str .= '<label><input type="checkbox" name="'.$field['name'].'[]" value = "'.$i.'"   ';
						$str .= (!empty($field['class'])) ? ' class = "'.$field['class'].'" ' : '';
						$str .= (!empty($field['readonly'])) ? " readonly " : '';
						$str .= (!empty($field['also'])) ? $field['also'] : '';
						$str .= ( $showval &&  in_array($i,$value) ) ? " checked >\n" : ">\n"; // print_r($value);
						$str .= $field['arr'][$i];
						$str .= "</label></td>";
            $k++;
					}
					$str .= '</tr></table>';
        }else{
  				$str .= '<select multiple="multiple" name="'.$field['name'].'[]"';
				$str .= ' class = "';
				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
				$str .= '" ';

  				$str .= (!empty($field['size'])) ? ' size="'.$field['size'].'" ' : '';
  				$str .= (!empty($field['readonly'])) ? " disabled " : '';
  				$str .= (!empty($field['also'])) ? $field['also'] : '';
  				$str .= (!empty($also)) ? $also : '';
  				$str .= ">\n";
          
  				foreach ( $field['arr']as $i=>$v )
  					$str .= ( in_array($i,$value) && $showval )	? "<option selected value=\"$i\" >".$v."</option>\n" :
  																	"<option value=\"$i\" >".$v."</option>\n";
  				$str .= "</select>\n";
        }
				break;
			// ---------------------------------   POPUP SELECT (WITH SEARCH)
			case 'select_popup':
				if (!empty($field['default']) && empty($value)){
					$value = $field['default'];
				}
				if (!empty($value)) {
					if ($field['ex_table']!='ex_table') {
						$value = SQL::getval( $field['ex_table_field'], $field['ex_table'], $field['id_ex_table']."='".$value."'", '', $debug );
					} else {
						$field['ex_table'] 	= $this->global_val($this->Arr['ex_table']['name']);
						$field['id_ex_table'] = $this->Get_name_id_from_ex_table( $field['ex_table'] );
						$value = SQL::getval( $field['ex_table_field'], $field['ex_table'], $field['id_ex_table']."='".$value."'", '', $debug );
					}
				}
				$str = '';
				$str .= '<input type ="text" name= "'.$field['name'].'" ';
				$str .= (!empty($field['size'])) ? ' size="'.$field['size'].'" ' : '';
				$str .= (!empty($field['maxlen'])) ? ' maxlength = "'.$field['maxlen'].'" ' : '';
				$str .= ' class = "';
				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
				$str .= '" ';

				$str .= (!empty($field['style'])) ? ' style = "'.$field['style'].'" ' : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
				$str .= ( $showval ) ? ' value = "'.((!empty($value))?htmlspecialchars($value, ENT_QUOTES, CODEPAGE):$field['default']).'"' : '';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= " >\n";
				if (empty($field['readonly'])) {
					$str .= '<input type ="button" value="Search" ';
					$str .= 'onclick="'.(!empty($field['do_for_ex_popup'])?$field['do_for_ex_popup']:"WOpen('/index.php?t=".$field['ex_table']."&a=search_by_field&f=".$field['name']."&search[".$field['ex_table_field']."]='+document.db_form.elements['".$field['name']."'].value, 600, 500);").' return false;"';
					$str .= " >\n";
				}
				break;

			// ---------------------------------   SELECT_FROM_TREE
			case 'select_from_tree':
				if (!empty($value)) {
					$tree_name	= SQL::getval( $field['ex_table_field'], $field['ex_table'], $field['id_ex_table']."='".$value."'", '', 1 );
					$tree_pid	= SQL::getval( $field['parent_id_ex_table'], $field['ex_table'], $field['id_ex_table']."='".$value."'", '', 1 );
				}
				$str = '';
				$str .= '<input type ="hidden" name= "'.$this->Get_name_parent_id_from_ex_table($field).'" ';
				$str .= 'value = "'.(!empty($value)?$tree_pid:'').'"'." >\n";
				$str .= '<input type ="text" name= "'.$field['name'].'" ';
				$str .= (!empty($field['size'])) ? ' size="'.$field['size'].'" ' : '';
				$str .= (!empty($field['maxlen'])) ? ' maxlength = "'.$field['maxlen'].'" ' : '';
				$str .= ' class = "';
				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
				$str .= '" ';

				$str .= (!empty($field['style'])) ? ' style = "'.$field['style'].'" ' : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
				$str .= ' value = "'.(!empty($value)?$tree_name:'').'"';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= " >\n";
				$str .= '<input type ="button" value="Choose" ';
				$str .= 'onclick="'.(!empty($field['do_for_ex_popup'])?$field['do_for_ex_popup']:"WOpen('/index.php?t=".$field['ex_table']."&a=tree_list&f=".$field['name']."&pid_field=".$this->Get_name_parent_id_from_ex_table($field)."', 600, 500);").' return false;"';
				$str .= " >\n";
				$str .= '<div id="'.$this->Get_name_parent_id_from_ex_table($field).'_path">&nbsp</div>';

				break;
			// ---------------------------------   SELECT_TABLE
			case 'select_table':
				$str .= '<select name="'.$field['name'].'"';
				$str .= ' class = "';
				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
				$str .= '" ';

				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
				$str .= (!empty($also)) ? $also : '';
				$str .= ">\n";

				foreach ( $field['arr']as $v )
					$str .= "<option value=\"$v\" selected>".$v."</option>\n";
				$str .= "</select>\n";
				break;

// Endof added
			// ---------------------------------   TEXTBOX
			case 'textbox'	:
            case 'email'	:
				$str .= '<input type ="'.$field['type'].'" name= "'.$field['name'].'" ';
				$str .= (!empty($field['size'])) ? ' size="'.$field['size'].'" ' : '';
				$str .= (!empty($field['maxlen'])) ? ' maxlength = "'.$field['maxlen'].'" ' : '';
				$str .= ' class = "';
				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
				$str .= '" ';
				$str .= (!empty($field['style'])) ? ' style = "'.$field['style'].'" ' : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
                // $str .= (!empty($field['must'])) ? ' required ' : '';
				$str .= (!empty($field['placeholder'])) ? ' placeholder ="'.$field['placeholder'] .'" ': '';
				$str .= ( $showval ) ? ' value = "'.((!empty($value))?htmlspecialchars($value, ENT_QUOTES, CODEPAGE):$field['default']).'"' : '';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= " >\n";
				break;
			// ---------------------------------   TEXTDATE
			case 'textdate'	:
				$str .= '<input type ="text" name= "'.$field['name'].'" ';
				$str .= (!empty($field['size'])) ? ' size="'.$field['size'].'" ' : '';
				$str .= (!empty($field['maxlen'])) ? ' maxlength = "'.$field['maxlen'].'" ' : '';
				$str .= ' class = "';
				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
				$str .= '" ';

				$str .= ( $showval ) ? ' value = "'.((!empty($value))?htmlspecialchars($value, ENT_QUOTES, CODEPAGE):$field['default']).'"' : '';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= (!empty($field['style'])) ? ' style = "'.$field['style'].'" ' : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
				$str .= " >\n";
				$img_src = (KAT::inc_link('a_cal.gif',$t)) ? KAT::inc_link('a_cal.gif',$t) : '/templates/def/images/calendar.gif';
				$str	.= 	'<a href="javascript:showCalendar(document.forms.db_form.elements[\''.$field['name'].'\'].value.substring(0, 4), document.forms.db_form.elements[\''.$field['name'].'\'].value.substring(5, 7),\'cl_'.$field['name'].'\', \'db_form\', \''.$field['name'].'\');"><img name="cl_'.$field['name'].'" src="'.$img_src.'" border="0" title=" �������� ����  " alt=" �������� ����  " align="middle"></a>';

				if (empty($this->show_cwas_cal)) {
					$str .= '
				<div id="cal" style="z-index:5;visibility:hidden;display:none;position:absolute;left:-500px;top:-500px;">&nbsp;</div>
<script language="Javascript1.2">
var NS = (navigator.appName == "Netscape") ? 1 : 0;
var months = new Array(\'���\',\'���\',\'���\',\'���\',\'���\',\'����\',\'����\',\'���\',\'���\',\'���\',\'���\',\'���\');
var ver = parseInt(navigator.appVersion);
var IE = (navigator.appName == "Microsoft Internet Explorer") ? 1 : 0;
var cid = \'cal\';
var ct = 0;

function isLeapYear (x_year) {
	if (((x_year % 4)==0) && ((x_year % 100)!=0) || ((x_year % 400)==0)) {
		return (true);
		}
	else {
		return (false);
		}
	}

function getDOW(date) {
	dow = date.getDay();
	return (dow == 0 ? 7 : dow);
	}

function getMonthDays(date) {
	var month = date.getMonth()+1;
	if (month==1||month==3||month==5||month==7||month==8||month==10||month==12)	return 31;
	else if (month==4||month==6||month==9||month==11) return 30;
	else if (month==2) {
		if (isLeapYear(date.getYear()))	return 29;
		else return 28;
		}
	}

function setDateValues(formname, control, d, m, y) {
	m = (m+1);
	document.forms[formname].elements[control].value = y + \'-\' + (m < 10 ? \'0\' + m : m ) + \'-\' + (d < 10 ? \'0\' + d : d );
	hideCalendar();
	}

function hideCalendar() {
	if (NS) {
		var obj = document.getElementById(cid);
		obj.style.visibility = \'hidden\';
		obj.style.display = \'none\';
		}
	else {
		document.all[cid].style.visibility = \'hidden\';
		document.all[cid].style.display = \'none\';
		}
	clvis = false;
	document.body.oncontextmenu = function(){ return true; };
	}


function buildCalendar(y, m, img, formname, control) {
	var html = \'\';
	var cDate = new Date();
    if (y == \'\') y=0;
    if (m == \'\') m=0;
	var y = parseInt(y, 10);
	var m = parseInt(m, 10);
    
    if (y == 0) y='.(int)date("Y").';
    if (m == 0) m='.(int)date("m").';
    
	var fDate = new Date (y, (m-1), 1);
	var fDow = getDOW(fDate);
	var fDays = getMonthDays(fDate);

	html += \'<table class="calend" cellspacing="0" cellpadding="0" border="0" width="210" >\';
	html += \'<tr align="center">\';
	html += \'<td class="month"><a class="day nobg" href="javascript:showCalendar(\'+ (y-1) + \',\' + m + ", \'" + img + "\', \'" + formname + "\', \'" + control + \'\\\');" title=" Last year ">&nbsp;&lt;&lt;&nbsp;</a></td>\';
	html += \'<td class="month"><a class="day nobg" href="javascript:showCalendar(\'+((m-1)>=0?y:y-1)+\', \'+((m-1)>=0?m-1:12)+", \'" + img + "\',  \'" + formname + "\', \'" + control + \'\\\');" title=" Предыдущий месяц ">&nbsp;&lt;&nbsp;</a></td>\';
	html += \'<td colspan="3" class="month" style="cursor:\' + (NS ? \'pointer\' : \'hand\') + \';\' + (fDate.getMonth()==cDate.getMonth()&&fDate.getYear()==cDate.getYear()? \'color:#CC0000;\' : \'\') + \'" OnClick="hideCalendar();" title=" Закрыть ">\' + months[fDate.getMonth()] + \' \' + fDate.getFullYear() + \'</td>\';
	html += \'<td class="month"><a class="day nobg" href="javascript:showCalendar(\'+((m+1)!=13?y:y+1)+\', \'+((m+1)!=13?(m+1):1)+", \'" + img + "\',  \'" + formname + "\', \'" + control + \'\\\');" title=" Следующий месяц ">&nbsp;&gt;&nbsp;</a></td>\';
	html += \'<td class="month"><a class="day nobg" href="javascript:showCalendar(\'+(y+1)+\', \'+m+", \'" + img + "\',  \'" + formname + "\', \'" + control + \'\\\');" title=" Следующий год ">&nbsp;&gt;&gt;&nbsp;</a></td>\';
	html += \'</tr>\';
	html += \'<tr align="center" bgcolor="#FFFFFF"><td class="head">Mon</td><td class="head">Thu</td><td class="head">Wen</td><td class="head">Thi</td><td class="head">Fri</td><td class="head">Sat</td><td class="head">Sun</td></tr>\';
	html += \'<tr align="center" bgcolor="#EEEEEE">\';
	var fDate = new Date (((m-2)>=0?y:(y-1)), ((m-2)>=0?(m-2):12), 1);
	var fDays = getMonthDays(fDate);
	var pDow = getDOW(fDate);

	d = fDays+2-fDow;

	for (c = 1; c < fDow; c++) {
		html += \'<td class="gday">\'+d+\'</td>\';
		d++;
		}

	var fDate = new Date (y, (m-1), 1);
	var fDow = getDOW(fDate);
	var fDays = getMonthDays(fDate);
	var skipDOW = new Array(';
	if (!empty($field['skip']) && is_array($field['skip'])){
		$str .= join(',',$field['skip']);
	}
	$str .=');
	for (d = 1; d <= fDays;) {
		cday = ( (fDate.getYear() == cDate.getYear()) &&	(fDate.getMonth() == cDate.getMonth()) &&	(cDate.getDate() == d ) ) ? 1 : 0 ;
		fDow = getDOW(fDate);
		cl = (fDow == 7 ? \'day7\' : (fDow == 6 ? \'day6\' : \'day\' ));
		html += \'<td\' + (cday ? \' class="cday"\':\' class="day"\')+(!cday ? \' onMouseOver="this.style.backgroundColor=\\\'#AAAAAA\\\';" onMouseOut="this.style.backgroundColor=\\\'#EEEEEE\\\'"\':\'\') + \'>\';
		if (!skipDOW[fDow]){
			html += \'<a\' + (cday ? \' class="cday nobg"\':\' class="\'+cl+\' nobg"\') +\' href="javascript:\' + "setDateValues(\'" + formname + "\', \'" + control + "\', " + d + ", " + fDate.getMonth() + ", " + fDate.getFullYear() + \');">\';
			html += d;
			html += \'</a>\';
		}else
			html += d;
		html += \'</td>\' + (fDow == 7 ? \'</tr><tr bgcolor="#EEEEEE" align="center">\' : \'\' );
		d++;
		fDate.setDate(d);
		}

	var fDate = new Date (((m+1)!=13?(y+1):y), ((m+1)!=13?(m+1):1), 1);
	var fDays = getMonthDays(fDate);
	var d = 1;

	for (c = fDow; c < 7; c++) {
		var fDow = getDOW(fDate);
		html += \'<td class="gday">\'+d+\'</td>\';
		d++;
		fDate.setDate(d);
		}
	html += \'</tr></table>\';
	return html;
	}

function showCalendar(y, m, img, formname, control) {
	var html = buildCalendar(y, m, img, formname, control);
	var obj = (NS ? document.getElementById(cid) : document.all[cid]);

	if (NS) {
		var newNode = document.createElement(\'span\');
		obj.replaceChild(newNode, obj.firstChild);
		newNode.innerHTML = html;
		}
	else {
		obj.innerHTML = html;
		}
	TopCoord  = ObjTop(document.images[img], 2);
	LeftCoord = ObjLeft(document.images[img], 0);
	obj.style.top  = TopCoord+\'px\';
	obj.style.left = (img==\'cl_img2\'?LeftCoord-210+14:LeftCoord+0)+\'px\';
	obj.style.visibility = \'visible\';
	obj.style.display = \'inline\';
	document.body.oncontextmenu=function(){ return false; };
	}
function ObjLeft(elem, offset) {
if ((NS) && (ver==4)) {
return elem.x + offset;
}
else {
var X = 0;
do { X += elem.offsetLeft } while ((elem = elem.offsetParent) != null);
return X + offset;
}
}
function ObjTop(elem, offset) {
if ((NS) && (ver==4)) {
return elem.y + elem.height + offset;
}
else {
if (IE) {
var H = elem.height;
}
else {
var H = elem.height;
}
var Y = 0;
do { Y += elem.offsetTop } while ((elem = elem.offsetParent) != null);
return Y + H + offset;
}
}
</script>
';
				}
				$this->show_cwas_cal	= true;
				break;
            case 'color':
                $str .= "<div style='float:left; text-align:right; padding-top: 3px; width:20px; height:17px; background-color: ".((!empty($value))?"#".htmlspecialchars($value, ENT_QUOTES, ''):$field['default'])."; ' id='fish_".$field['field_name']."'>#</div> ";
				$str .= '<input type="text" id="color_'.$field['field_name'].'" name= "'.$field['name'].'" ';
				$str .= (!empty($field['size'])) ? ' size="'.$field['size'].'" ' : '';
				$str .= (!empty($field['maxlen'])) ? ' maxlength = "'.$field['maxlen'].'" ' : '';
				$str .= ' class = "';
				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
				$str .= '" ';

				$str .= ( $showval ) ? ' value = "'.((!empty($value))?htmlspecialchars($value, ENT_QUOTES, CODEPAGE):$field['default']).'"' : '';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= (!empty($field['style'])) ? ' style= "'.$field['style'].'" ' : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
				$str .= " >\n ";
// <input type="text" id="border" name="text" size="6" readonly="readonly" />
if (empty($colors_flag)){
$str .= " 
<script type=\"text/javascript\">
Colors = function (objectId, inputTextId, styleColor, pallete)
// �����������
{
	
	this._object = document.getElementById(objectId);
	this._objectId = objectId;
	this.inputText = document.getElementById(inputTextId);
	this.inputTextId = inputTextId;
	this.styleColor = styleColor;
	eval('this.oldColor = Colors.rgb2hex(this._object.style.'+styleColor+')');
	this.inputText.value = this.oldColor;
	
	this.pallete = pallete;
	
}

Colors.prototype.show = function ()
// ������ �������
{
	
	var table = '<span id=\"allcolorselementsselect'+this.inputTextId+'\" onclick=\"document.getElementById(\'allcolorselements'+this.inputTextId+'\').style.display=\'block\'; document.getElementById(\'allcolorselementslink'+this.inputTextId+'\').focus();\" style=\"cursor: pointer;\"><img height=\"20\" src=\"'+this.outImage+'\" title=\"������� ���� �� �������\" style=\"position:relative; \" onmouseover=\"this.src=\''+this.overImage+'\'\" onmouseout=\"this.src=\''+this.outImage+'\'\" /></span>';
	table += '<input id=\"setc'+this.inputTextId+'\" type=\"hidden\" value=\"0\" />';
	table += '<input id=\"oldColor'+this.inputTextId+'\" type=\"hidden\" value=\"#'+this.oldColor+'\" />';
	table += '<a id=\"allcolorselementslink'+this.inputTextId+'\" href=\"#\" onclick=\"return false;\" onblur=\"document.getElementById(\'allcolorselements'+this.inputTextId+'\').style.display=\'none\'; if (document.getElementById(\'setc'+this.inputTextId+'\').value!=\'1\') Colors.changecolor(document.getElementById(\'oldColor'+this.inputTextId+'\').value, \''+this._objectId+'\', \''+this.inputTextId+'\', \''+this.styleColor+'\'); document.getElementById(\'setc'+this.inputTextId+'\').value=\'0\'; \">';
	table += '<div id=\"allcolorselements'+this.inputTextId+'\" style=\"width: '+this.allwidth+'px; cursor: pointer; display: none; position: absolute; z-index:1000; background-color:#bbb; border:solid 1px #bbb;\">';
	
	for (var i=1, c=0; i<=this.rows; i++)
	{
		//table += '<tr height=\"20\">';
		for (var j=1; j<=this.cols; j++)
		{
			table += '<div style=\"background:'+this.pallete[c++]+'; float:left; width:'+this.width+'px; height:'+this.width+'px; margin:'+this.border+'px; border:solid '+this.border+'px #000;\" onmousedown=\"document.getElementById(\'setc'+this.inputTextId+'\').value=\'1\'; document.getElementById(\'oldColor'+this.inputTextId+'\').value=this.style.backgroundColor;\" onmousemove=\"Colors.changecolor(this.style.backgroundColor, \''+this._objectId+'\', \''+this.inputTextId+'\', \''+this.styleColor+'\');\" onmouseover=\"this.style.border=\'solid '+this.border+'px #ffff00\'\" onmouseout=\"this.style.border=\'solid '+this.border+'px #000\';\"></div>';
		}
		table += '<div style=\"clear:both;\"></div>';
	}
	table += '</div></a>';
	
	document.writeln(table);
}


Colors.rgb2hex = function (rgb) {
// ��������������� ������� �������������� �� ������� rgb(0,0,0) � hex

  if (!rgb)
  {
    return '000000';
  }
  if (rgb.substring(0,1)==\"#\")
  {
    return rgb.substring(1);
  }
  else
  {
	  var s,i,h='', x='0123456789abcdef';
	  var c = rgb.substring(4);
	  c = c.substring(0, c.length-1);
	  
	
	  if(c){
	    s=c.split(',');
	    for (i=0; i < 3; i++){
	      n  = parseInt(s[i]);
	      h += x.charAt(n>>4) + x.charAt(n&15);
	    }
	  return h;
	  }
  }
}

Colors.changecolor = function (thiscolor, _object, inputText, styleColor)
{
	eval('document.getElementById(_object).style.'+styleColor+' = thiscolor');
	document.getElementById(inputText).value = Colors.rgb2hex(thiscolor);
}

Colors.setup = function (params) {
	function param_default(pname, def) { if (typeof params[pname] == \"undefined\") { params[pname] = def; } };
	param_default(\"rows\",			5);
	param_default(\"cols\",			12);
	param_default(\"width\",			20);
	param_default(\"border\",			1);
	param_default(\"styleColor\",		\"color\");
	param_default(\"inputTextId\",	null);
	param_default(\"objectId\",		null);
	param_default(\"pallete\",        new Array (
	'#FFFFCC',	'#FFFF66',	'#FFCC66',	'#F2984C',	'#E1771E',	'#B47B10',	'#A9501B',	'#6F3C1B',	'#804000',	'#CC0000',	'#940F04',	'#660000',
	'#C3D9FF',	'#99C9FF',	'#66B5FF',	'#3D81EE',	'#0066CC',	'#6C82B5',	'#32527A',	'#2D6E89',	'#006699',	'#215670',	'#003366',	'#000033',
	'#CAF99B',	'#80FF00',	'#00FF80',	'#78B749',	'#2BA94F',	'#38B63C',	'#0D8F63',	'#2D8930',	'#1B703A',	'#11593C',	'#063E3F',	'#002E3F',
	'#FFBBE8',	'#E895CC',	'#FF6FCF',	'#C94093',	'#9D1961',	'#800040',	'#800080',	'#72179D',	'#6728B2',	'#6131BD',	'#341473',	'#400058',
	'#FFFFFF',	'#E6E6E6',	'#CCCCCC',	'#B3B3B3',	'#999999',	'#808080',	'#7F7F7F',	'#666666',	'#4C4C4C',	'#333333',	'#191919',	'#000000'
	));
	param_default(\"outImage\",		\"rgb.gif\");
	param_default(\"overImage\", 		\"on_rgb.gif\");
	
	
	var col = new Colors(params.objectId, params.inputTextId, params.styleColor, params.pallete);

	col.rows = params.rows;
	col.cols = params.cols;
	col.width = params.width;
	col.border = params.border;
	col.allwidth = col.cols*(col.width+col.border*4)+col.border;
	col.outImage = params.outImage
	col.overImage = params.overImage

	col.show();
	
	return col;
}   
</script> 
\n";
$colors_flag = true;
}
$str .= '<script type="text/javascript">

 Colors.setup({
  objectId    : "fish_'.$field['field_name'].'", // id �������
  inputTextId : "color_'.$field['field_name'].'", // id ���� text
  styleColor  : "backgroundColor", // �������� ����� ����������� ����� ���������
  outImage    : "'.KAT::inc_link('rgb.gif',$t).'", // url ��������, ��� ����� �� ������� ����� ������������
                                   // �������
  overImage   : "'.KAT::inc_link('on_rgb.gif',$t).'", // url ��������, ��� ��������� �� �� ��������� ����
  width       : 20,   //������ ���������� ����� �������
  border      : 1     //������ ������� ���������� �������
  });
 
</script>';
                break;
			// ---------------------------------   PASSWORD
			case 'password'	:
				$str .= '<input type="password" autocomplete="off" name= "'.$field['name'].'" ';
				$str .= (!empty($field['size'])) ? ' size="'.$field['size'].'" ' : '';
				$str .= (!empty($field['maxlen'])) ? ' maxlength= "'.$field['maxlen'].'" ' : '';
				$str .= ' class = "';
				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
				$str .= '" ';
				$str .= ( $showval ) ? ' value = "'.$value.'"' : '';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= (!empty($field['style'])) ? ' style = "'.$field['style'].'" ' : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
				$str .= " >\n";
				break;

			// ---------------------------------   HIDDEN
			case 'hidden'	:
				$str .= '<input type="hidden" name="'.$field['name'].'" value = "'.((!empty($value))?htmlspecialchars($value, ENT_QUOTES, CODEPAGE):$field['default']).'" >'."\n";
				break;
			// ---------------------------------   DATETIME
			case 'datetime'	:
				$str .= '<input type="text" name="'.$field['name'].'" class="jq_datetime'.((!empty($_CORE->CURR_TPL)) ? ' form-control' : '').'" value = "'.((!empty($value))?htmlspecialchars($value, ENT_QUOTES, CODEPAGE):$field['default']).'" >'."\n";
				$str .= '<link rel="stylesheet" type="text/css" href="/templates/admin/JS/datetimepicker/jquery.datetimepicker.css"/>';
				//$str .= '<script type="text/javascript" src="/templates/admin/JS/datetimepicker/jquery.js"></script>';
				$str .= '<script type="text/javascript" src="/templates/admin/JS/datetimepicker/jquery.datetimepicker.js"></script>';
				$str .= '<script>
					$(".jq_datetime").datetimepicker({
					  format:"'.(($field['dateformat'])?$field['dateformat']:'Y-m-d H:i').'",
					  lang:"ru",
					  dayOfWeekStart: 1,
                      timepicker:'.(($field['timepicker'])?$field['timepicker']:'true').',                      
					});
				</script>';
			break;
			// ---------------------------------   DATEmonth - bootstrap
			case 'bs_datemonth'	:
                $str .= '<div data-date-minviewmode="months" data-date-viewmode="years" data-date-format="mm/yyyy" data-date="'.$field['default'].'" class="input-append date dpMonths">
                      <input type="text" '.((!empty($field['readonly'])) ? " readonly " : '').' name="'.$field['name'].'" value="'.((!empty($value))?htmlspecialchars($value, ENT_QUOTES, CODEPAGE):$field['default']).'" size="'.$field['size'].'" class="form-control">
                      <span class="input-group-btn add-on">
                        <button class="btn btn-theme02" type="button" style="margin-left:-32px"><i class="fa fa-calendar"></i></button>
                        </span>
                    </div>
                    <link rel="stylesheet" type="text/css" href="/templates/def/js/bootstrap-datepicker/css/datepicker.css" />
                    <script type="text/javascript" src="/templates/def/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                    <script> $(".dpMonths").datepicker(); </script>
                    ';
			break;
            
			// ---------------------------------   TEXTAREA
			case 'textarea'	:

				$str .='<textarea id="'.$field['field_name'].'" name="'.$field['name'].'" rows="'.$field['rows'].'" cols="'.$field['cols'].'" ';
				if($field['wysiwyg'] == 'tinymce')
				{
					$str .= ' class = "mceEditor';
					$str .= (!empty($_CORE->CURR_TPL)) ? ' form-control ' : '';
					$str .= '" ';
				}
				else
				{
					$str .= ' class = "';
					$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
					$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
					$str .= '" ';
				}
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= (!empty($field['placeholder'])) ? ' placeholder ="'.$field['placeholder'] .'" ': '';
				$str .= (!empty($field['style'])) ? " style = '".$field['style']."'" : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
				$str .= '>';
				if ( $showval )
					$str .=  htmlspecialchars($value, ENT_QUOTES | ENT_SUBSTITUTE, CODEPAGE); // htmlapecialchars() 11.01.2006
				$str .= "</textarea>\n";
				if (isset($field['wysiwyg'])){// && !empty($field['wysiwyg_path']) && is_file($field['wysiwyg_path'])) {
					if ($field['wysiwyg'] == 'htmlarea') {
						if (empty($field['wysiwyg_path']) || !is_file($_SERVER['DOCUMENT_ROOT'].'/'.$field['wysiwyg_path'].'editor.js')) {
							$this->Error	= "Wrong HTMLAREA Path! ".$_SERVER['DOCUMENT_ROOT'].'/'.$field['wysiwyg_path'];
							$str	= $this->Error;
							break;
						}
						$str	.= '
									<script language="Javascript1.2">
									<!-- // load htmlarea
									_editor_url = "'.$field['wysiwyg_path'].'";                 // URL to htmlarea files and images
									var win_ie_ver = parseFloat(navigator.appVersion.split("MSIE")[1]);
									if (navigator.userAgent.indexOf("Mac")        >= 0) { win_ie_ver = 0; }
									if (navigator.userAgent.indexOf("Windows CE") >= 0) { win_ie_ver = 0; }
									if (navigator.userAgent.indexOf("Opera")      >= 0) { win_ie_ver = 0; }
									if (win_ie_ver >= 5.5) {
									 document.write(\'<scr\' + \'ipt src="\'+_editor_url+\'editor.js"\');
									 document.write(\' language="Javascript1.2"></scr\' + \'ipt>\');
									} else { document.write("<scr"+"ipt>function editor_generate() { return false; }</scr"+"ipt>"); }
									//-->
									</script>
									<script language="javascript1.2">
									   var config = new Object(); // create new config object
									   config.width = "'.$field['width'].'";
									   config.height = "400px";
									   config.bodyStyle = "'.$field['wysiwyg_body'].';";
									   config.debug = 0;
										config.toolbar = [
//										  ["fontname"],
//										  ["fontsize"],
										  ["bold","italic","underline","separator"],
										  ["strikethrough","subscript","superscript","separator"],
										  ["justifyleft","justifycenter","justifyright","separator"],
										  ["OrderedList","UnOrderedList","Outdent","Indent","separator"],
//										  ["forecolor","backcolor","separator"],
										//["custom1","custom2","custom3","separator","InsertImage"],
										  ["HorizontalRule","Createlink","htmlmode","separator"],
										  ["fontstyle"],
										];
									   editor_generate(\''.$field['name'].'\',config);
									   //editor_setmode("'.$field['name'].'");
										</script>
									';
					}elseif ($field['wysiwyg'] == 'htmlarea3') {
						if (empty($field['wysiwyg_path']) || !is_file($_SERVER['DOCUMENT_ROOT'].'/'.$field['wysiwyg_path'].'htmlarea.js')) {
							$this->Error	= "Wrong WYSIWYG Path! ".$_SERVER['DOCUMENT_ROOT'].'/'.$field['wysiwyg_path'];
							$str	= $this->Error;
							break;
						}
						$str	.= '

						<script type="text/javascript">
						_editor_url = "'.$field['wysiwyg_path'].'";
						_editor_lang = "'.((isset($field['lang'])) ? $field['lang'] : 'ru'). '";
						</script>
						<script type="text/javascript" src="'.$field['wysiwyg_path'].'/htmlarea.js"></script>
						<script type="text/javascript" src="'.$field['wysiwyg_path'].'/dialog.js"></script>
						<script type="text/javascript" src="'.$field['wysiwyg_path'].'/lang/ru.js"></script>
						<script type="text/javascript">
						if (typeof HTMLArea == "undefined") {
							htmlarea_init = function()
								{
									if(typeof HTMLArea != "undefined") HTMLArea.replace("'.$field['name'].'")
								}
							window.onload = htmlarea_init;
						}
						</script>

						';
					}elseif ($field['wysiwyg'] == 'htmlarea3.1') {
						if (empty($field['wysiwyg_path']) || !is_file($_SERVER['DOCUMENT_ROOT'].'/'.$field['wysiwyg_path'].'htmlarea.js')) {
							$this->Error	= "Wrong WYSIWYG Path! ".$_SERVER['DOCUMENT_ROOT'].'/'.$field['wysiwyg_path'];
							$str	= $this->Error;
							break;
						}
						include_once $_SERVER['DOCUMENT_ROOT']."/".$field['wysiwyg_path']."/backends/backend_conf.php";

						$str	.= '

						  <script type="text/javascript">
							_editor_url  = "'.AREAEDIT_INSTALL_URL.'";
						    _editor_lang = "ru";

						  </script>
						  <script type="text/javascript" src="'.AREAEDIT_INSTALL_URL.'/ddt/ddt.js"></script>
						  <script type="text/javascript" src="'.AREAEDIT_INSTALL_URL.'/htmlarea.js"></script>
						  <script type="text/javascript">
							areaedit_editors = null;
							areaedit_init    = null;
							areaedit_config  = null;
							areaedit_plugins = null;
							areaedit_init = areaedit_init ? areaedit_init : function()
							{
							  areaedit_plugins = areaedit_plugins ? areaedit_plugins :
							  [
							   "CharacterMap",
							   "ContextMenu",
							   "FullScreen",
							   "ListType",
							   "Stylist",
							   "TableOperations",
							   "InsertAnchor",
							   "Linker",
							   "ImageManager"
							  ];
							   if (!HTMLArea.loadPlugins(areaedit_plugins, areaedit_init))
									return;
							  areaedit_editors = areaedit_editors ? areaedit_editors :
							  [
								"'.$field['field_name'].'"
							  ];
							  areaedit_config = areaedit_config ? areaedit_config : new HTMLArea.Config();
							  areaedit_editors   = HTMLArea.makeEditors(areaedit_editors, areaedit_config, areaedit_plugins);
							  HTMLArea.startEditors(areaedit_editors);
							}
							// calling areaedit_init twice can cause a nasty little exception.
							 window.onload = areaedit_init;
							areaedit_config = function()
							{
							var config = new HTMLArea.Config();
							config.stylistLoadStylesheet("/styles/edit_styles.css");
							return config;
							}
						  </script>
						';

					}elseif ($field['wysiwyg'] == 'wymeditor') {
						if (empty($field['wysiwyg_path']) || !is_file($_SERVER['DOCUMENT_ROOT'].'/'.$field['wysiwyg_path'].'/jquery.wymeditor.pack.js')) {
							$this->Error	= "Wrong WYSIWYG Path! ".$_SERVER['DOCUMENT_ROOT'].'/'.$field['wysiwyg_path'];
							$str	= $this->Error;
							break;
						}
						$str	.= '
	<script type="text/javascript" src="/'.$field['wysiwyg_path'].'/jquery.wymeditor.pack.js"></script>
	<SCRIPT>
		jQuery(function() {
			jQuery(".wymeditor").wymeditor( {'.$field['wysiwyg_conf'].'} );
		});						
	</script>
		';
					}
					elseif($field['wysiwyg'] == 'tinymce' )
					{
						if(empty($_CONF['TmplExt'])) $_CONF['TmplExt'] = '.tpl.phtml';
						if(!isset($field['wysiwyg_path']))
						{
							$field['wysiwyg_path'] = '/modules/html/tinymce';
						}
						//echo $_CONF['CorePath'].$_CORE->TEMPLATES.'admin/tinymce/'.$field['wysiwyg'].$_CONF['TmplExt'];
												
						include_once($_CONF['CorePath'].$_CORE->TEMPLATES.'admin/tinymce/'.$field['wysiwyg'].$_CONF['TmplExt']);
						
					}
				}

				break;

			// ---------------------------------   SELECT
			case 'select'	:
				$str .= '<select name="'.$field['name'].'"';
				$str .= ' class = "';
				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
				$str .= '" ';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
				$str .= (!empty($also)) ? $also : '';
				$str .= ">\n";

				if (!empty($field['default']) && empty($value) && $value != 0){
					$value = $field['default'];
				}
				foreach ( $field['arr'] as $i => $arr_val )
					$str .= ( $value == $i && $showval )	? "<option selected value=\"$i\" >".$field['arr'][$i]."\n" :
																	"<option value=\"$i\" >".$field['arr'][$i]."\n";
				$str .= "</select>\n";
				break;
			// ---------------------------------   radiobtn
			case 'radiobtn':
				if (is_array($field['arr'])){
					$str .= '<table width="100%"  border="0" cellspacing="0" cellpadding="0">
								<tr>';
								$i = 0;
					foreach ( $field['arr'] as $key => $arr_val ){
						$in_row = (!empty($field['inrow']))?$field['inrow']:3;
						$str .= ($i % (int)$in_row == 0
								&& $i > 0
								&& $i < sizeof($field['arr'])) // ���� "- 1" ������ �����, ���� �� ����� �� ������
								? "</tr><tr>"
								: '';
						$str .= '<td style="width: 33%;">';
						if (!empty($field['default']) && empty($value) && $value !== 0){
							$value = $field['default'];
						}
						$str .= '<label><input type="radio" name="'.$field['name'].'" value="'.$key.'" ';
						$str .= ( $value == $key && $showval )	? "checked" : "";
						$str .= (!empty($field['class'])) ? ' class = "'.$field['class'].'" ' : '';
						$str .= '>'.$arr_val.'</label>';
						$str .= "</td>";
						$i++;
					}
					$str .= '</tr></table>';}
				break;
			// ---------------------------------   RELATIVE SELECT
			case 'select_rel'	:
				$str .= "<script language=javascript type='text/javascript'>op_".$field['field_name']." = new Array(".sizeof($field['arr']).");\n";
				foreach ( $field['arr'] as $i => $arr_val ) {
					$str .= "op_".$field['field_name']."[".$i."] = new Array(".sizeof($field['arr'][$i]).");\n";
					foreach ( $field['arr'][$i] as $j => $arr_val2 ) {
						if ($arr_val2 != '')
							$str .= "op_".$field['field_name']."[".$i."][".$j."] = new Option('".$field['arr'][$i][$j]."', $j);\n";
					}
				}
				$str .= "
				function make_".$field['field_name']."( f, n )
				{
					len = f.elements['".$field['name']."'].length;
					for (var i=j=0; i < op_".$field['field_name']."[n].length; i++) {
					  if (op_".$field['field_name']."[n][i]){
						  f.elements['".$field['name']."'].options[j++]=op_".$field['field_name']."[n][i];
					  }
					  if (i==0) {
						 f.elements['".$field['name']."'].options[i].selected=true;
					  }
				   }
					diff =  len - j;
					for (var h=0; h < diff; h++) {
						f.elements['".$field['name']."'].options[j] = null;
					}
				}
				</script>
				";
				$str .= '<select name="'.$field['name'].'"';
				$str .= ' class = "';
				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
				$str .= '" ';

				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
				$str .= (!empty($also)) ? $also : '';
				$str .= ">\n";
				// от чего зависит
				$start_val	= cForm::global_val($field['rel_from']);
				global $$field['rel_from'];
//				$str .= $start_val.' - rel : '.$$field['rel_from'];
				if (is_array($field['arr'][$start_val]))
					foreach ($field['arr'][$start_val] as $i => $tmp )
						if (!empty($field['arr'][$start_val][$i]))
							$str .= ( $value == $i && $showval )
								? "<option selected value=\"$i\" >".$field['arr'][$start_val][$i]."\n" :
								"<option value=\"$i\" >".$field['arr'][$start_val][$i]."\n";
				$str .= "</select>\n";
				break;


			// ---------------------------------   CHECKBOX
			case 'checkbox'	:
				$str .= '<input type="checkbox" name="'.$field['name'].'" value = "1"   ';
				$str .= (!empty($field['class'])) ? ' class = "'.$field['class'].'" ' : '';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
				$str .= ( $showval && !empty( $value ) ) ? " checked >\n" : ">\n";
				break;

			// ---------------------------------   CHECKBOX
			case 'multcheckbox':
				if (is_array($field['arr'])){
					$str .= '<table width="100%"  border="0" cellspacing="0" cellpadding="0">
								<tr>';
					for ($i = 0; $i < sizeof($field['arr']); $i++){
						$in_row = (!empty($field['inrow']))?$field['inrow']:3;
						$value = $this->global_val( str_replace("]","$i]",$field['name'] ));
						$str .= ($i % (int)$in_row == 0
								&& $i > 0
								&& $i < sizeof($field['arr'])) // ���� "- 1" ������ �����, ���� �� ����� �� ������
								? "</tr><tr>"
								: '';
						$str .= "<td>";
						$str .= '<input type="checkbox" name="'.str_replace("]","$i]",$field['name']).'" value = "1"   ';
						$str .= (!empty($field['class'])) ? ' class = "'.$field['class'].'" ' : '';
						$str .= (!empty($field['readonly'])) ? " readonly " : '';
						$str .= (!empty($field['also'])) ? $field['also'] : '';
						$str .= ( $showval && !empty( $value ) ) ? " checked >\n" : ">\n";
						$str .= $field['arr'][$i];
						$str .= "</td>";
					}
					$str .= '</tr></table>';				}
				break;
                
			// ---------------------------------   MATRIX
			case 'matrix' :
                if ($id = $this->global_val( 'form[id]' )){
                
    				$str .= '<a href="javascript:;" onclick="ShowPopUp(\'/clear/db/matrix_edit?dbname='.$field['DBname'].'&dbnamex='.$field['DBnameX'].'&dbnamey='.$field['DBnameY'].'&type='.$field['sub_type'].'&id='.$id.'\', 800, 600, \'yes\');" ';
    				$str .= (!empty($field['class'])) ? ' CLASS = "'.$field['class'].'" ' : '';
    				$str .= ">".$field['title']."</a>\n";
                    $str .= "<script>function ShowPopUp(docName, wW, wH, wS){
                         var pC = '';
                         if (document.all || document.layers || document.createTextNode){
                          posX = Math.round((screen.width-wW)/2);
                          posY = Math.round((screen.height-wH)/2);
                          pC = (document.all)? 'left='+posX+',top='+posY : 'screenX='+posX+',screenY='+posY;
                         }
                         popWindow = window.open(docName,'_blank','menubar=no,resizable=yes,toolbar=no,scrollbars='+wS+',status=no,width='+wW+',height='+wH+','+pC);
                         if( popWindow ) popWindow.focus();
                        }</script>\n";
                    break;
                } else continue;
				
                
			// ---------------------------------   FILE
			case 'file' :
            case 'filenew' :
                $str .= '<div class="fileupload fileupload-new" data-provides="fileupload">
                            <span class="btn btn-theme02 btn-file">
                            <span class="fileupload-new" '.(($value && $showval) ? 'style="display: none"' : '' ).'><i class="fa fa-paperclip"></i> ������� ����</span>
                            <span class="fileupload-exists"  '.(($value && $showval) ? 'style="display: inline"' : '' ).'><i class="fa fa-undo"></i> ��������</span>
                                <input type="file" class="default" name="'.$field['name'].'"
                            '.((!empty($field['also'])) ? $field['also'] : '') .'
                            >
                            </span>
                            <span class="fileupload-preview" style="margin-left:5px;"></span> '.(($value && $showval) ? " <A HREF='".$field['path']."/".$value."' TITLE='DOC:".$field['title']."'>������� ����������</A> " . '�������: <input type="checkbox" name="kill_'.$field['name'].'" value = "1">' : '').'
                            <a href="#" class="fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                           </div>';
                if ($value && $showval) {
                    $str .= '<input type ="hidden" name="old_'.$field['name'].'" value = "'.$value.'" >'."\n";
                }
/*				$str .= '<input type="file" name="'.$field['name'].'"';
				$str .= ' class = "';
				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
				$str .= '" ';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= (!empty($field['also'])) ? $field['also'] : '';
				$str .= ">\n"; */
				break;

			// ---------------------------------   PHOTO
			case 'photo':
			global $_REIMG;
				if (!empty($value) && $showval)
				{
					# если есть подтип... (mp3,photo)
					switch ($field['sub_type']) {
						case 'doc':
							$str .= "<A HREF='".$field['path']."/".$value."' TITLE='DOC:".$field['title']."'>�������</A><BR>";
							break;
						case 'mp3':
							$str .= "<A HREF='".$field['path']."/".$value."' TITLE='MP3:".$field['title']."'>�������</A><BR>";
							break;
						case 'photo':
                        if(substr($value, 0, 4) == "http") {
                            $str .= "<IMG width=\"180\" SRC=\"".$value."\" BORDER=0 ><BR>";
                        }else{
                        	$str .= "<a href=\"".$field['path']."/".$value."  \" target=\"_blank\">";
    						if($_REIMG['nginx_mode'] == true){
    							$str .= "<IMG SRC=\"/reimg".$field['path']."/".$field['admwidth']."/".$value."\" BORDER=0 ><BR>";
    						}else{
    							$str .= "<IMG SRC=\"/reimg".$field['path']."/".$value."?".$field['admwidth']."\" BORDER=0 ><BR>";
    						}
    						$str .= "</a>";
                        }
							break;
                        case 'bs-file':
                            break;
						default:
						if($_REIMG['nginx_mode'] == true){
							$str .= "<IMG SRC=\"/reimg".$field['path']."/".$field['admwidth']."/".$value."\" BORDER=0 ><BR>";
						}else{
							$str .= "<IMG SRC=\"/reimg".$field['path']."/".$value."?".$field['admwidth']."\" BORDER=0 ><BR>";
						}
					}
                    if ($field['sub_type'] != 'bs-file') {
    					$str .= '<input type ="hidden" name="old_'.$field['name'].'" value = "'.$value.'" >'."\n";
    					$str .= '�������: <input type="checkbox" name="kill_'.$field['name'].'" value = "1"   ';
    					$str .= (!empty($field['class'])) ? ' class = "'.$field['class'].'" ' : '';
    					$str .= "><BR>�������� \n";
                        }
				}
                if ($field['sub_type'] == 'bs-file') {
                    $str .= '<div class="controls fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                <span class="'.((!empty($field['class'])) ? $field['class'] : '').'">
                                <span class="fileupload-new" '.(($value && $showval) ? 'style="display: none"' : '' ).'><i class="fa fa-paperclip"></i> ������� ����</span>
                                <span class="fileupload-exists"  '.(($value && $showval) ? 'style="display: inline"' : '' ).'><i class="fa fa-undo"></i> ��������</span>
                                    <input type="file" class="default" name="'.$field['name'].'"
                                '.((!empty($field['also'])) ? $field['also'] : '') .'
                                >
                                </span>
                                <span class="fileupload-preview" style="margin-left:5px;"></span> '
                                .(($value && $showval) ? " <A HREF='".$field['path']."/".$value."' TITLE='DOC:".$field['title']."' class='btn ".((!empty($field['class'])) ? $field['class'] : '')."'>�������</A> " 
                                . '<br /> <input type="checkbox" name="kill_'.$field['name'].'" style="width: 20px; float: left;" class="checkbox form-control" value = "1"> <div style="padding: 9px 0 0 25px;">�������</div>' : '')
                                .'<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                               </div>';
                    if ($value && $showval) {
                        $str .= '<input type ="hidden" name="old_'.$field['name'].'" value = "'.$value.'" >'."\n";
                    }
                }elseif ($field['sub_type'] == 'bs-photo') {
                    $str .= '<div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                <div class="fileupload-new thumbnail" '.(($value && $showval) ? 'style="display: none"' : ' style="width: 200px; height: 150px;"' ).'>
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""></div>
                                <i class="fa fa-paperclip"></i> ������� ����</span>
                                <span class="fileupload-preview fileupload-exists thumbnail"  '.(($value && $showval) ? 'style="max-width: 200px; max-height: 150px; line-height: 20px;"' : '' ).'><i class="fa fa-undo"></i> ��������</span>
                                    <input type="file" class="default" name="'.$field['name'].'"
                                '.((!empty($field['also'])) ? $field['also'] : '') .'
                                >
                                </span>
                                <div> '
                                .(($value && $showval) ? " <A HREF='".$field['path']."/".$value."' TITLE='DOC:".$field['title']."' class='btn ".((!empty($field['class'])) ? $field['class'] : '')."'>�������</A> " 
                                . '<br /> <input type="checkbox" name="kill_'.$field['name'].'" style="width: 20px; float: left;" class="checkbox form-control" value = "1"> <div style="padding: 9px 0 0 25px;">�������</div>' : '')
                                .'<a href="#" class="fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                               </div>';
                    if ($value && $showval) {
                        $str .= '<input type ="hidden" name="old_'.$field['name'].'" value = "'.$value.'" >'."\n";
                    }
                } else {
    				$str .= '<input type="file" name="'.$field['name'].'"';
    				$str .= ' class = "';
    				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
    				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
    				$str .= '" ';
    				$str .= ">\n";
                }
				break;
			default:
				if (!empty($field['get_code'])) {
					$str .= $field['get_code'];
					eval('$str .= '.$filed['get_code'].'($value);');
				}
				break;
		} // end switch

		if ((!empty($field['example']) || !empty($field['example_pref'])) && $field['type'] != 'hidden' && $field['title_position']  != 'right') {
			$str .= "<div class=example>" . @$field['example_pref'] . '<b class=inside>' . $field['example'] . '</b>' . @$field['example_suf'] . '</div>';
		}

		return $str;
	}

  	 ///////////////////////////////////////////////////////////   Conv_str ()
	//------------------------------------------------------------------------
	//  Converting String:
	//	1. Strip Tags
	//	2. Strip Slashes
	//	3. Replace ' -> `
	// PRIVATE
	function Conv_str ( $string )
	{
		return ($this->NoTag)?
		str_replace("'","\"",stripslashes(strip_tags($string,$this->EnableTags))):
		str_replace("'","\"",stripslashes($string));
	}

	 ///////////////////////////////////////////////// global_val ()
	// geting the value of variable
	// PRIVATE
	function global_val( $name )
	{
		preg_match("/(.*)\[(.*)\]/",$name, $matches);
		if (!empty($matches[2]))
		{
			global $$matches[1];
			return @${$matches[1]}[$matches[2]];
		}
		else
		{
			global $$name;
			if (empty($$name))	$$name = $_REQUEST[$name];
			if (empty($$name))	$$name = $_FILES[$name]['tmp_name'];
//			echo $name."=".$$name."<br>";
			return $$name;
		}
	}

	 ///////////////////////////////////////////////// chg_val ()
	// geting the value of variable
	// 01.10.02 PRIVATE
	function chg_val( $name, $value )
	{
		preg_match("/(.*)\[(.*)\]/",$name, $matches);
		if (!empty($matches[2]))
		{
			global $$matches[1];
			@${$matches[1]}[$matches[2]] = $value;
		}
		else
		{
			global $$name;
			$$name = $value;
		}
	}
	 ///////////////////////////////////////////////// global_val ()
	// geting the key of array:
	// => some[some_key]
	// <= some_key
	//
	// PRIVATE
	//
	function get_key( $string )
	{
		preg_match("/(.*)\[(.*)\]/",$string, $matches);
		return (!empty($matches[2])) ? $matches[2] : $string;
	}


	 ///////////////////////////////////////////////////////////////////// chk_foto()
	// chk_foto
	// ^^^^^^^^
	//   Функция вывода строки поля формы, на вход:
	//	массив field, с ключом 'name', 'path' быть может 'func newname','maxsize'
	// ----------------------
	//  version 1.2 - OUT:
	// ----------------------
	//
	// 3/22/02
	// PRIVATE
	function chk_photo( &$field )
	{
		global ${'kill_'.$field['name']}, ${'old_'.$field['name']}, ${$field['name']}, ${$field['name']."_type"}, ${$field['name']."_name"};
		$kill_foto	= ${'kill_'.$field['name']};
		$oldfoto	= ${'old_'.$field['name']};
		$foto		= ${$field['name']};
		$foto_type  = ${$field['name']."_type"};
		$foto_name	= ${$field['name']."_name"};
        
//        print_r($_FILES);exit;

		if (empty($foto)) $foto = $_FILES[$field['name']]['tmp_name'];
		if (empty($foto_name)) $foto_name = $_FILES[$field['name']]['name'];
		if (empty($foto_type)) $foto_type = $_FILES[$field['name']]['type'];
		if (empty($kill_foto)) $kill_foto = $_REQUEST['kill_'.$field['name']];
		if (empty($oldfoto)) $oldfoto = $_REQUEST['old_'.$field['name']];

		// If we want Kill Photo
		if (!empty($kill_foto) && !empty($oldfoto))
		{
			$destination = (!isset($field['abspath'])) ? $field['path'] . "/" . $oldfoto :  $field['abspath']. "/" . $oldfoto;
			@unlink( $destination );
			global $_CORE;
			if (@DEBUG) $_CORE->log_message('', 'Kill doc "'.$field['name'].'": '.$destination);
		}

		$this->skipfoto[$field['name']] = (!empty($kill_foto) || !empty($foto_name))?0:1;

//		echo "Foto name ::". $foto_name." (".$this->skipfoto[$field['name']].")<br>";

		if ( !empty($foto_name))
		{
			$may_be	= array(
					'application/zip',
					'application/x-rar-compressed',
					'application/x-zip-compressed',
					"image/jpeg",
					"image/pjpeg",
					"image/gif",
                    "image/png",
					"audio/mpeg",
					"application/rtf",
					"application/msword",
					"application/x-css",
					"application/octet-stream",
			);

			if ( empty($field['nocheck']) ) // in_array($foto_type, $may_be ) || 
			{
				// If We must check Size of a Picture
				if (!empty($field['maxsize']))
				{ 
					$size = sprintf("%u", filesize($foto));
          $dop_size = round(($field['maxsize']/(1024*1024)),2);
					if ( $size > $field['maxsize'] )
					{
		        $mega_size = round(($size/(1024*1024)),2);
            
						$this->Error .= "- �������� ������ �����: ". $mega_size ." ��, ����������� ����������: ".$dop_size." ��.<BR>\n"; // string
					}
          if(empty($size)){
            $this->Error .= "- �������� ������ �����, ����������� ����������: ".$dop_size." ��.<BR>\n"; 
          }
//					echo "here size";
				}
        $image_size = getimagesize( $foto );
        if (!empty($field['maxwidth']))
				{
					if (is_array($image_size) && $image_size[0] > $field['maxwidth'] )
					{
						$this->Error .= "- �������� ���������� �����: ". $image_size[0] ." ��������, ����������� ���������� ������: ".$field['maxwidth']." ��������.<BR>\n"; // string
						
					}
//					echo "here size";
				}
        if (!empty($field['maxheight']))
				{
					if (is_array($image_size) && $image_size[1] > $field['maxheight'] )
					{
						$this->Error .= "- �������� ���������� �����: ". $image_size[1] ." ��������, ����������� ���������� ������: ".$field['maxheight']." ��������.<BR>\n"; // string
						
					}
//					echo "here size";
				}
        if (!empty($this->Error)){
          return false;
        }        
//				else
//				{
					// if update, so must kill old one photo.
					if ( ! empty( $oldfoto ) ) {
						$destination = (!isset($field['abspath'])) ? $field['path'] . "/" . $oldfoto :  $field['abspath']. "/" . $oldfoto;
						@unlink( $destination );
					}
					return true;
//				}// IF
			}
			else
			{
				$this->Error .= "- �������� ��� �����. $foto_type.<BR>\n"; // string
				return false;
			}
		}
		return true;
	} // func

	 ///////////////////////////////////////////////// copy_file()
	// Копируем файл, создав новое имя файла.
	// извне:  ${$field['name']}, ${$field['name']."_name"};
	// field[ 'name', 'newname_func' 'path']
	//
	// 24.03.2002
	// PRIVATE
	function copy_file( & $field )
	{
		/// GLOBALIZE
		global ${$field['name']}, ${$field['name']."_name"},$_CORE;
//		$foto		= ${$field['name']};
//		$foto_name	= ${$field['name']."_name"};


		if (empty($foto)) $foto = $_FILES[$field['name']]['tmp_name'];
		if (empty($foto_name)) $foto_name = $_FILES[$field['name']]['name'];
		// GET NEW NAME
		//$type = strrchr( $foto_name,'.');
		//if( $type == ''  ) $type = strrchr( $foto,'.');
		$ext = explode('.',$foto_name);
		$type = $ext[count($ext)-1];
		$type = '.'.$type;
		//print $type;die();
		if (empty($field['newname_func']))
			$foto_new_name = $field['name'] . $type;
		else
		{	$tmp_r_f = $_REQUEST['form']['alias'];
			$_REQUEST['form']['alias'] = basename( str_replace(".","_",$foto));
			eval("\$foto_new_name = ".$field['newname_func']."; \$foto_new_name .= \$type;");
			$_REQUEST['form']['alias'] = $tmp_r_f;
		}

		// MUST COPY FILE NOW. ALL CHEKS COMPLETE
		$destination = (!isset($field['abspath'])) ? $field['path'] . "/" . $foto_new_name :  $field['abspath']. "/" . $foto_new_name;
//		echo "Copy: $foto => <I>". $destination."-".$foto_new_name."</I><br>";
		if (!is_dir(dirname($destination))) {
			@mkdir(dirname($destination));
		}
		$otnosit_path = substr((!isset($field['abspath']) ? $field['path'] : $field['abspath']),strlen($_SERVER['DOCUMENT_ROOT']));
		@unlink($_CORE->ROOT.'/'.'reimg'.$otnosit_path.'/'.$foto_new_name);
		if (@copy($foto,"$destination"))
		{
			${$field['name']} = $foto_new_name;
//			echo $destination."-".${$field['name']}."<br>";
			if(!empty($field['hardwidth']) || !empty($field['hardheight']))
				KAT::resizefoto($destination, (int)$field['hardwidth'] , (int)$field['hardheight'] );
			
			return true;
		}
		else // ELSE WARNINGS
		{
			$this->Bug_mes = "<p><B>BUG MESSAGE => Trying : copy( \"$foto\", \"$destination\") - false</B></p>";
			$this->Error = "<p class=warning>Couldn't copy files.</p><!--Trying : copy( \"$foto\", \"$destination\") - false -->"; //string
			${$field['name']} = '';
			unset( ${$field['name']} );
			return false;
		}//else
	} // func

/**
 * Load data from external table. Added by Shesternin 13.07.06
 *
 * @param array $field
 * @param array $res_array result
 * @param int $id
 * @param array $op order, desc, offset, limit
 * @param bool $debug
 * @return unknown
 */
	function Load_from_ex_table( &$field, &$res_array, $id=0, $op=array(), $debug=0, $value = '' ){
	    // if single record
		if (!empty($id)) {
			$tmp = SQL::getval( $field['ex_table_field'], $field['ex_table'], $field['id_ex_table']."='".$id."'", $field['ex_table_where'], $debug );
			if (!empty($tmp))	$res_array[$id] = $tmp;
			return true;
		}else{
    		$order = '';
            // Set order
    		if (!empty($op['order'])) {
    			$order = 'ORDER BY '.$op['order'];
    			if (!empty($op['desc'])) $order	.=' DESC';
    		}
    		if (!empty($op['offset'])) {
    			$order .= SQL::limit_sql( $op['offset'], $op['limit']);
    		}
                  
            // get data
    		$res = SQL::sel( $field['ex_table_field'].",".$field['id_ex_table'], $field['ex_table'], $field['ex_table_where'], $order, $debug );
    		if ($res->NumRows > 0) {
    			for ( $i=0; $i<$res->NumRows; $i++ ) {
    				$res->FetchArray($i,1);
                    // if need several fields in line, not only name or smth else
                    if (!empty($field['ex_table_fileds_delimeter'])) {
                        $all = explode (',', $field['ex_table_field']); // another filed for line
                        $t = array();
                        foreach ($all as $f) $t[] = $res->FetchArray[$f];
                        $res_array[$res->FetchArray[$field['id_ex_table']]] = implode ($field['ex_table_fileds_delimeter'], $res->FetchArray); // array_keys($res->FetchArray)
                    } else {
    				    $res_array[$res->FetchArray[$field['id_ex_table']]] = (empty($field['ex_table_field_as']))
                                    ? $res->FetchArray[$field['ex_table_field']]
                                    : $res->FetchArray[$field['ex_table_field_as']];
                    }
                }

                // ��� �� �������� ������ ��������, ���� ������� ��� ����� ���������
                // �������� ���� �� � ������ �������� value � ���� ��� �� ������� � ������� ���
                if($value!= '' && !isset($res_array[$value])){
                    $res2 = SQL::sel( $field['ex_table_field'].",".$field['id_ex_table'], $field['ex_table'], $field['id_ex_table']." = '".$value."'", $order, $debug );
                    if ($res2->NumRows > 0) {
                        for ($i = 0; $i < $res2->NumRows; $i++) {
                            $res2->FetchArray($i, 1);
                            // if need several fields in line, not only name or smth else
                            if (!empty($field['ex_table_fileds_delimeter'])) {
                                $all = explode(',', $field['ex_table_field']); // another filed for line
                                $t = array();
                                foreach ($all as $f) $t[] = $res2->FetchArray[$f];
                                $res_array[$res2->FetchArray[$field['id_ex_table']]] = implode($field['ex_table_fileds_delimeter'], $res2->FetchArray); // array_keys($res->FetchArray)
                            } else {
                                $res_array[$res2->FetchArray[$field['id_ex_table']]] = (empty($field['ex_table_field_as']))
                                    ? '*'.$res2->FetchArray[$field['ex_table_field']]
                                    : '*'.$res2->FetchArray[$field['ex_table_field_as']];
                            }
                        }
                    }
                }
    		}
        }
	}

/**
 * Return id from external table.
 *
 * @param array $field
 * @param array $uniq_data contained data for query
 * @param bool $debug
 * @return int
 */
	function Get_id_from_ex_table( &$field, $uniq_data, $debug=0 ){
		$where = '';
		foreach ($uniq_data as $k=>$v) {
			$where .= $k."='".$v."' AND ";
		}
		$where .= '1';
		return SQL::getval( $field['id_ex_table'], $field['ex_table'], $where, '', DEBUG );
	}

	function Get_pid_fet( &$field ){
		global ${$this->Get_name_parent_id_from_ex_table($field)};
		return !empty(${$this->Get_name_parent_id_from_ex_table($field)})?${$this->Get_name_parent_id_from_ex_table($field)}:0;
	}
	function Get_name_parent_id_from_ex_table( &$field ){
		return $field['field_name']."_".$field['parent_id_ex_table'];
	}
	function Get_name_id_from_ex_table( $table ){
		return Page::LoadCnfArray($table, 'id');
	}

	function parse_form_data ($data=''){
		if (empty($data)) $data = $this->Arr;
		$body_sql = array();
		foreach ($data as $value){
			switch ($value['type']) {
				// hidden
				case 'hidden':
					if (!empty($value['primary_key'])) {
						$body_sql[]= "\t`".$value['field_name']."`\t bigint(20) NOT NULL auto_increment,\n\tPRIMARY KEY  (`".$value['field_name']."`)\n";
					} else {
						$body_sql[]= "\t`".$value['field_name']."`\t varchar(".((!empty($value['maxlen']))?intval($value['maxlen']):255).")\n";
					}
					break;

				// textdate - date
				case 'textdate':
					$body_sql[]= "\t`".$value['field_name']."`\t date default NULL\n";
					break;

				// textbox - date
				case 'textbox':
					if (empty($value['data_type'])) {
						$body_sql[]= "\t`".$value['field_name']."`\t varchar(".((!empty($value['maxlen']))?intval($value['maxlen']):255).")\n";
					} else {
						$body_sql[]= "\t`".$value['field_name']."`\t ".$value['data_type']."\n";
					}
					break;
					
				// select, photo, textbox - varchar
				case 'select': case 'photo': case 'textbox': case 'multiselect_from_table': case'datetime': case 'file':
					$body_sql[]= "\t`".$value['field_name']."`\t varchar (".((!empty($value['maxlen']))?intval($value['maxlen']):255).")\n";
					break;

				case 'select_from_table':  case 'select_popup': case 'select_from_tree':
					$body_sql[]= "\t`".$value['field_name']."`\t bigint(20)\n";
					break;

				// textarea - text
				case "textarea":
					$body_sql[]= "\t`".$value['field_name']."`\t text\n";
					break;

				case "checkbox":
					$body_sql[]= "\t`".$value['field_name']."`\t tinyint\n";
					break;

				case "multcheckbox":
					for ($i = 0; $i < sizeof($value['arr']); $i++) {
						$body_sql[]= "\t`".$value['field_name']."$i`\t tinyint\n";
					}
					break;
				default: if (!empty($value['data_type'])) $body_sql[]= "\t`".$value['field_name']."`\t ".$value['data_type']."\n";
			}
		}
		return implode( ",", $body_sql);
	}

	function addColumn($table, $field='', $debug=0) {
    	if (!empty($field)) {
			$data[$field] = $this->Arr[$field];
            $query = $this->parse_form_data($data);
	        if (!empty($query)) {
	            $query = "ALTER TABLE `".$table."` ADD COLUMN (".$query.")";
	        }
	        return  SQL::query($query, $debug);
        }
	}

	function delColumn($table, $column, $debug=0) {
		if (isset($this->Arr[$column])) {
			$query = "ALTER TABLE `".$table."` DROP COLUMN `".$column."`";
		}
		return 	SQL::query($query, $debug);
	}
	
	function refreshColumn($table, $column, $debug=0) {
    	if (!empty($field)) {
			$data[$field] = $this->Arr[$field];
            $query = $this->parse_form_data($data);
	        if (!empty($query)) {
	            $query = "ALTER TABLE `".$table."` ALTER COLUMN (".$query.")";
	        }
	        return  SQL::query($query, $debug);
        }
	}

    function refreshTable($table, $debug=0) {
//		if (!empty($table)) {
//			$query = "ALTER TABLE `".$table."` (".$this->parse_form_data($this->Arr).")";
//			$res = SQL::query($query, $debug);
//        }
    }
    
    function createTable($table, $debug=0) {
		if (!empty($table)) {
			$query = "CREATE TABLE `".$table."` (".$this->parse_form_data($this->Arr).")";
			$res = SQL::query($query, $debug);
        }
    }

    function dropTable($table, $debug=0) {
		if (!empty($table)) {
			$query = "DROP TABLE `".$table."`";
			$res = SQL::query($query, $debug);
        }
    }

    function describeTable($table, $update=true, $debug=0) {
		if (!empty($table)) {
	        $res = SQL::query(' describe `'.$table.'`; ', $debug);
	        $form_data = Array();
	        for ($i=0; $i<$res->NumRows; $i++) {
				$res->FetchArray($i);
	            $fa = $res->FetchArray;
	            $form_data[$fa['Field']] = Array(
	                'field_name'    => $fa['Field'],
	                'name'          => "form[".$fa['Field']."]",
	                'title'         => $fa['Field'],
	                'must'          => empty( $fa['Null'] )?0:1,
	                'data_type'		=> $fa['Type'],
	            );
	            if (!empty($fa['Key'])&&$fa['Key']=='PRI') {
					$form_data[$fa['Field']]['primary_key'] = true;
					$form_data[$fa['Field']]['type'] = 'hidden';
				}else
	            if( $fa['Type'] == 'varchar(255)' )
	            {   $form_data[$fa['Field']]['size']     = 50;
	                $form_data[$fa['Field']]['maxlen']   = 255;
	                $form_data[$fa['Field']]['type']     = 'textbox';
	            }else
	            if( $fa['Type'] == 'bigint(20)' ){
	                $form_data[$fa['Field']]['maxlen']   = 20;
	                $form_data[$fa['Field']]['type']     = 'textbox';
	            }else
	            if( $fa['Type'] == 'bigint(20) unsigned' ){
	                $form_data[$fa['Field']]['maxlen']   = 20;
	                $form_data[$fa['Field']]['type']     = 'textbox';
	            }else
	            if( $fa['Type'] == 'text' )
	            {
	                $form_data[$fa['Field']]['maxlen']   = 5120;
	                $form_data[$fa['Field']]['type']     = 'textarea';
	                $form_data[$fa['Field']]['rows']     = '8';
	                $form_data[$fa['Field']]['style']    = 'width:100;';
	            }else
	            if( $fa['Type'] == 'int(11)' ){
	                $form_data[$fa['Field']]['maxlen']   = 11;
	                $form_data[$fa['Field']]['type']     = 'textbox';
	            }else
	            if( $fa['Type'] == 'timestamp' ){
	                $form_data[$fa['Field']]['maxlen']   = 20;
	                $form_data[$fa['Field']]['type']     = 'textbox';
	            }else
	            if( $fa['Type'] == 'tinyint(4)' ){
	                $form_data[$fa['Field']]['maxlen']   = 20;
	                $form_data[$fa['Field']]['type']     = 'textbox';
	            }else
	            if( $fa['Type'] == 'int(11) unsigned' ){
	                $form_data[$fa['Field']]['maxlen']   = 20;
	                $form_data[$fa['Field']]['type']     = 'textbox';
	            }else
	            if( $fa['Type'] == 'smallint(6)' ){
	                $form_data[$fa['Field']]['maxlen']   = 20;
	                $form_data[$fa['Field']]['type']     = 'textbox';
	            }else
	            if( $fa['Type'] == 'float' ){
	                $form_data[$fa['Field']]['maxlen']   = 20;
	                $form_data[$fa['Field']]['type']     = 'textbox';
					$form_data[$fa['Field']]['data_type']= 'float';
	            }else
	            if( $fa['Type'] == 'float unsigned' ){
	                $form_data[$fa['Field']]['maxlen']   = 20;
	                $form_data[$fa['Field']]['type']     = 'textbox';
					$form_data[$fa['Field']]['data_type']= 'float unsigned';
	            }else
	            if( $fa['Type'] == 'date' ){
	                $form_data[$fa['Field']]['type']     = 'textdate';
	            }else
	            if( $fa['Type'] == 'datetime' ){
	                $form_data[$fa['Field']]['type']     = 'textbox';
	            }else
	            if( $fa['Type'] == 'tinytext' )
	            {   $form_data[$fa['Field']]['maxlen']   = 5120;
	                $form_data[$fa['Field']]['type']     = 'textarea';
	                $form_data[$fa['Field']]['rows']     = '8';
	                $form_data[$fa['Field']]['style']    = 'width:100;';
	            }
			}
			if ($update) $this->Arr = $form_data;
	        return $form_data;
		}
    }


    public function check_dependence($field, &$vars)
    {
        $data = $this->Arr[$field];
        if(!empty($data['show']) && is_array($data['show']) ){
            $vars['depend'][$field] = $data['show'];
            if(empty( $vars['listen']))  $vars['listen'] = array();
            foreach (array_keys($data['show']) as $key ){
                if(empty($vars['listen'][$key]))$vars['listen'][$key] = array();
                if(!in_array($field,$vars['listen'][$key])) $vars['listen'][$key][] = $field;
            }
        }
    }

}//class

