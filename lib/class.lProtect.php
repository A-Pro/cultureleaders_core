<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : class.lProtect.php
// PROJECT NAME : USEFUL
// DATE         : 09.08.02 LastModify( 13.09.2002 )
// Author       : Andrew Lyadkov
// Discription  : ����� lProtect, ��� ������ ���� �� �������������� ����������
// ...........  : ��������.
// Comment      : Complete ... ������ �������� �� �������������� ����������� �������
// ...........  : GIF, JPG, PNG, BMP
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
/*
Logo:
-----
15.09.2002 - using $HTTP_POST_VARS, $HTTP_GET_VARS, $_SESSION

---------------------
*  EXAMPLE: TEST.php
---------------------

< ?php
// include class

include "class.lProtect.php";

// creating object ( before output )

$wall = new lProtect('http://dbase3.inforis.ru/~land/src');

if (!isset($submit)) 
{
	// show checking form

	? >
	<FORM METHOD=POST ACTION="">
	<TABLE border=1>
	<? $wall->out_form(); ? > 
	<TR><TD colspan=2><INPUT TYPE="submit" name=submit>
	</TABLE>
	</FORM>
	< ?
	echo "<BR>REAL KEY:".$wall->good_key." Image Type:".$wall->type;
}
else // if submit check and show result
	echo ($wall->check()>0) ? "GOOD":"BAD".$wall->err_msg;

? >


================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
*/

/* � ��S ��� ������ ����� � antispam */
//// ���� �������� ��� ��������� ��������
//if (isset($HTTP_POST_VARS['a']) && $HTTP_POST_VARS['a'] == 'img' || isset($HTTP_GET_VARS['a']) && $HTTP_GET_VARS['a'] == 'img') 
//{ 
//	ob_start();
//	$wall = new lProtect( false );
//	if (!$wall->img_generation())
//	{echo $wall->err_msg;}
//	ob_end_flush();
//	exit; 
//}
/* CLASS lProtect

-	function lProtect( $isSessOpened )
-	function out_form()
-	function check()
-	function img_generation()

*/
class lProtect
{
	// ���� ��� SRC ��������.
	var $img_link = "";
	// ����� ������
	var $err_msg = '';
	// �������������� ��� ��������
	var $type='none';
	// ��������� �����������
	var $bg_color = Array( 0xFF, 0xFF, 0xFF); // 
	var $text_color = Array( 0, 0, 0);
	var $height = 17;
	var $weight = 35;
	var $is_error = false;
	var $good_key = 'none';
	var $supported = false;

      //////////////////////////////////////////////////////////////
     /* 	1.�������������:                                      /
    //////////////////////////////////////////////////////////////
	/*
	   --------------
	   ��������� ������, ��������� ����, ���������� ���
	*/
	function lProtect( $img_link = '', $isSessOpened =false)
	{
		// ���� ��� �� �������, ������� ������
		If (!$isSessOpened)
		{
			session_name('sessid');
			session_start();
		}
		$this->img_link = ((isset($img_link))? $img_link.'?a=img':'')."&sessid=".session_id();
		if (function_exists("imagegif")) { $this->type = 'gif';}
		elseif (function_exists("imagepng")) { $this->type = 'png';}
		elseif (function_exists("imagejpeg")) { $this->type = 'jpeg';}
		elseif (function_exists("imagewbmp")) {$this->type = 'bmp';}
		if (!function_exists("ImageCreate")||$this->type=='none') 
		{
			$this->err_msg =  "No ImageCreate: No image support in this PHP server";
			return false;
		}
		$this->supported = true;
		return true;
	}
     //////////////////////////////////////////////////////////////
    /*  2.������� ������ ������� � ��������� � ��������� �����  */
   //////////////////////////////////////////////////////////////
	/* 
	 --------------------------------------
	|  ������� ���� X ��� ����:  | INPUT   |
	 --------------------------------------
	*/
	function out_form()
	{
		// ���� �� ��������������, �� �������.
		if (!$this->supported) { echo "<TR><TD colspan=2>GD not found.";return false;}
		// ����� ...
		// ��������� ����
		mt_srand ((double) microtime() * 1000000000);
		$_SESSION['PROTECT']['good_key'] = mt_rand(0,100000);
		$this->good_key = $_SESSION['PROTECT']['good_key'];
		// ��������� �����
		?>
		<TR<?=($this->is_error)?' class=error':''?>>
		<TD class=tab>������� ��� <span class=mark>*</span></TD>
		<TD class=tab><IMG SRC="<?=$this->img_link?>" BORDER=0 ALT="���" style="float: left"><INPUT TYPE="text" NAME="PROTECT[form_key]" size=10  onkeyup="v()" class="key"></TD>
		</TR>
		<?
	}

     //////////////////////////////////////////////////////
    /*  3. ��������� ������������ ���������� ����       */
   //////////////////////////////////////////////////////
	function check()
	{
		// ���� �� ��������������, �� �������.
		if (!$this->supported) { echo "<TR><TD colspan=2>GD not found.";return false;}
		// ����� ...
		global $ERROR;
		global $_SESSION, $HTTP_POST_VARS, $HTTP_GET_VARS;
		$this->form_key = (isset($HTTP_POST_VARS['PROTECT']['form_key']))?$HTTP_POST_VARS['PROTECT']['form_key']:'';
		$this->form_key = ($this->form_key == '' && isset($HTTP_GET_VARS['PROTECT']['form_key']))?$HTTP_GET_VARS['PROTECT']['form_key']:$this->form_key;
		$this->form_key = ($this->form_key == '' && isset($_GET['PROTECT']['form_key']))?$_GET['PROTECT']['form_key']:$this->form_key;
		$this->form_key = ($this->form_key == '' && isset($_POST['PROTECT']['form_key']))?$_POST['PROTECT']['form_key']:$this->form_key;
		if(!isset($_SESSION['PROTECT']['good_key']))
		{
			$this->err_msg = "��� �� ������";
			$this->is_error = true;
			return -1;
		}
		elseif($this->form_key == '')
		{
			$this->good_key = $_SESSION['PROTECT']['good_key'];
			$this->err_msg = "������� ��� ��������� �� ��������.";
			$this->is_error = true;
			return -1;
		}
		elseif( $_SESSION['PROTECT']['good_key'] != $this->form_key )
		{
			$this->good_key = $_SESSION['PROTECT']['good_key'];
			$this->err_msg = "��������� ��� �� �����";
			$this->is_error = true;
			return -2;
		}
		else
		{
			$this->good_key = $_SESSION['PROTECT']['good_key'];
			$_SESSION['PROTECT']['good_key'] = ''; // ����� ��� ��� � ������ ��� �� ����� (�� �������� �������)
			$this->err_msg = "��� ��!!!";
			$this->is_error = false;
			return 1;
		}
	}
     //////////////////////////////////////////////////////
    /*  4.Creating a picture                            */
   //////////////////////////////////////////////////////
	function img_generation()
	{
		// ���� �� ��������������, �� �������.
		if (!$this->supported) { echo "GD not found.";return false;}
		// ����� ...
		global $_SESSION;
		$im = ImageCreate ($this->weight, $this->height) or die ("Cannot Initialize new GD image stream");
		$background_color = ImageColorAllocate ($im, $this->bg_color[0], $this->bg_color[1], $this->bg_color[2]);
		$text_color = ImageColorAllocate ($im, $this->text_color[0], $this->text_color[1], $this->text_color[2]);
		$black = ImageColorAllocate ($im, 0, 0, 0);
		$white = ImageColorAllocate ($im, 255, 255, 255);
		$text = (isset($_SESSION['PROTECT']['good_key'])) ? $_SESSION['PROTECT']['good_key'] : 'no key';
		// ������� ��������:
		ImageRectangle($im,0,0,$this->weight-1, $this->height-1,$background_color);
		ImageRectangle($im,1,1,$this->weight-2, $this->height-2,$text_color);
		ImageString ($im, 2, 3, 2,  $text, $text_color);
		switch ($this->type) 
		{
			case 'gif':
				Header("Content-type: image/gif");
				ImageGIF($im);
				break;
			case 'png':
				Header("Content-type: image/png");
				ImagePNG($im);
				break;
			case "jpeg":
				Header("Content-type: image/jpeg");
				ImageJPEG($im, "", 0.9);
				break;
			case "bmp":
				Header("Content-type: image/vnd.wap.wbmp");
				ImageWBMP($im);
				break;
		}
		return true;
	}
} //class
?>