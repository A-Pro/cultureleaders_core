<?

global $SQL_DBCONF, $_CORE, $_UNSUB;

$_UNSUB['MODULE_NAME'] = 'unsubscribe';

if (!$_CORE->dll_load('class.DBCQ'))
	die ($_CORE->error_msg());

define("TABLE_UNSUB", DB_TABLE_PREFIX."unsubscribe");

class UNSUB {

	/**
	*
	*/
	function create_table()
	{
		$sql = 'CREATE TABLE  `'.TABLE_UNSUB.'` (
						`email` VARCHAR( 255 ) NOT NULL ,
						`ts` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
						) ENGINE = MYISAM';
		$res = SQL::query($sql, DEBUG);
		return $res->ErrorQuery;
	}
	
	/**
	*
	*/
	function unsubscribe( $e )
	{
		return SQL::ins(TABLE_UNSUB, 'email', "'".$e."'");
	}

	/**
	*
	*/
	function form()
	{
		global $_UNSUB;
		Main::comm_inc('unsub.inc.html', $f, 'unsubscribe');
		if ($f) include "$f";
	}
} 

?>