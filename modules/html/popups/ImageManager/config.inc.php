<?php
// $Id: config.inc.php,v 2.0.2.1 2006/03/29 09:51:46 Andrey Exp $
/**
* HTMLArea3 addon - ImageManager
* @ Package HTMLArea3 XTD
* @ Copyright � 2004, 2005 Bernhard Pfeifer - www.novocaine.de
* @ All rights reserved
* @ Released under HTMLArea3 XTD License: http://www.novocaine.de/license/HTMLArea3_XTD_license.txt
* @version $Revision: 2.0.2.1 $
**/

$BASE_DIR = $_SERVER['DOCUMENT_ROOT'];
$BASE_URL = "/";
$BASE_ROOT = "imglib"; 
$SAFE_MODE = false;
$IMG_ROOT = $BASE_ROOT;

if(strrpos($BASE_DIR, '/')!= strlen($BASE_DIR)-1) 
	$BASE_DIR .= '/';

if(strrpos($BASE_URL, '/')!= strlen($BASE_URL)-1) 
	$BASE_URL .= '/';

function dir_name($dir) 
{
	$lastSlash = intval(strrpos($dir, '/'));
	if($lastSlash == strlen($dir)-1){
		return substr($dir, 0, $lastSlash);
	}
	else
		return dirname($dir);
}