<?php
// $Id: lang-en.php,v 2.0.2.1 2006/03/29 09:51:49 Andrey Exp $
/**
* HTMLArea3 XTD addon - FileManager
* Based on AlRashid's FileManager
* @package HTMLArea3  XTD
* @ Copyright � 2004, 2005 Bernhard Pfeifer - www.novocaine.de
* @ All rights reserved
* @ Released under htmlArea License : http://www.dynarch.com/demos/htmlarea/license.txt
* @version $Revision: 2.0.2.1 $
**/
$MY_MESSAGES = array();
$MY_MESSAGES['extmissing'] = 'Please upload a file with an extensions, e.g. "imagefile.jpg".';
$MY_MESSAGES['loading'] = 'Loading files';
$MY_MESSAGES['uploading'] = 'Uploading...';
$MY_MESSAGES['nopermtodeletefile'] = 'No permission to delete file.';
$MY_MESSAGES['filenotfound'] = 'File not found.';
$MY_MESSAGES['unlinkfailed'] = 'Unlink failed.';
$MY_MESSAGES['rmdirfailed'] = 'Rmdir failed.';
$MY_MESSAGES['nopermtodeletefolder'] = 'No permission to delete folder.';
$MY_MESSAGES['foldernotfound'] = 'Folder not found.';
$MY_MESSAGES['foldernotempty'] = 'Folder is not empty. Please delete all files first.';
$MY_MESSAGES['nopermtocreatefolder'] = 'No permission to create folder.';
$MY_MESSAGES['pathnotfound'] = 'Path not found.';
$MY_MESSAGES['foldernamemissing'] = 'Folder name missing.';
$MY_MESSAGES['folderalreadyexists'] = 'Folder already exists.';
$MY_MESSAGES['mkdirfailed'] = 'Mkdir failed.';
$MY_MESSAGES['nopermtoupload'] = 'No permission to upload.';
$MY_MESSAGES['extnotallowed'] = 'Files with this extension are not allowed.';
$MY_MESSAGES['filesizeexceedlimit'] = 'File exceeds the size limit.';
$MY_MESSAGES['filenotuploaded'] = 'File was not uploaded.';
$MY_MESSAGES['nofiles'] = 'No files...';
$MY_MESSAGES['configproblem'] = 'Configuration problem ';
$MY_MESSAGES['deletefile'] = 'Delete file';
$MY_MESSAGES['deletefolder'] = 'Delete folder';
$MY_MESSAGES['refresh'] = 'Refresh';
$MY_MESSAGES['folder'] = 'Folder';
$MY_MESSAGES['type'] = '';
$MY_MESSAGES['name'] = 'Name';
$MY_MESSAGES['size'] = 'Size';
$MY_MESSAGES['datemodified'] = 'Date Modified';
$MY_MESSAGES['url'] = 'URL';
$MY_MESSAGES['comment'] = 'Comment';
$MY_MESSAGES['caption'] = 'Caption';
$MY_MESSAGES['upload'] = 'Upload';
$MY_MESSAGES['insertfile'] = "Insert File";
$MY_MESSAGES['filemanager'] = "File manager";
$MY_MESSAGES['directory'] = "Directory";
$MY_MESSAGES['enterurl'] = "You must enter the URL";
$MY_MESSAGES['entercaption'] = 'Please enter the caption text';
$MY_MESSAGES['inserticon'] = 'Insert filetype icon';
$MY_MESSAGES['insertsize'] = 'Insert file size';
$MY_MESSAGES['insertdate'] = 'Insert file modification date';
$MY_MESSAGES['newfolder'] = 'New folder name:';
$MY_MESSAGES['cancel'] = 'Cancel';
$MY_MESSAGES['openinnewwin'] = 'Open in new window';


?>