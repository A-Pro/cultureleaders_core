<?php
/**
 * ������� �������� �����
 *
*/
	
	
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! $user_tranzak - �� ��������� !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
	
	
global $_CORE;
$_RBK['TITLE']	    	= ($_CORE->LANG == 'en' )?'RBK - pay system' : 'RBK - ������� ������';
$_RBK['MODULE_TITLE']		= '����������';
$_RBK['MODULE_DESC']		= '������ ����� ��������.';
$_RBK['ERROR']	   	 	= '';
$_RBK['MODULE_NAME']		= 'rbk';
$_RBK['HISTORY_FILE']		= $_CORE->SiteModDir.'/data/history.txt';
$_RBK['HISTORY_LEN']		= 3;

/**
 * ������� �������� ������
 *
 * ������� �������� ������ (���� ����)...
 */	

class RBK {

	function comm_inc($tpl, &$file){ /* ��� ������� ���� ����������� �� ������ DOCTXT */
		global $_CORE, $_RBK;
		$files	=  array( 
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_RBK['MODULE_NAME'].'/'.$tpl,
			$_CORE->SiteModDir.'/'.$tpl,
			$_CORE->CORE_PATHTPLS.'/'.$_RBK['MODULE_NAME'].'/_'.$tpl,
			$_CORE->CoreModDir.'/_'.$tpl,
		);
 		$tmp = FILE::chk_files($files, $file); 
		return $tmp;
	}
	
	
	function result_url($type = 'result'){ 																	/* ������ ������������� �������� ����� ����, ���������� �� ������ (ResultURL) */
		
		global $_CORE, $_RBK; 													/* ���������� ���������� , ���������� ����� �������� */
		$mrh_pass2 	= $_RBK['pass_2']; 												/* ������ ��� ����������� � ������ �������� ��������� */
		$tm 		= getdate(time()+9*3600);												/* ���� */
		$date 		= "$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]"; 	/* ���� 2 */
		$out_summ 	= $_REQUEST["recipientAmount"]; 													/* ����� ������ */
		$inv_id 	= $_REQUEST["paymentId"]; 													/* ���������� ����� �������� */
		$shp_item 	= $_REQUEST["serviceName"]; 												/* ������������ */
		$inv_desc 	= $_REQUEST["userField_3"]; 												/* ����� */
		$crc 		= $_REQUEST["hash"]; 								/* ��� ��� ��������� */
		$rem_addr 	= $_RBK['REMOTE_ADDR']; 											/* ip user */
		/* ������������ ���� */
        $my_crc 	= md5($_REQUEST['eshopId'] 
                         . '::' . $_REQUEST['orderId']
                         . "::" . $_REQUEST['serviceName']
                         . "::" . $_REQUEST['eshopAccount']
                		 . "::" . $_REQUEST["recipientAmount"] 
                		 . '::' . $_REQUEST['recipientCurrency']
                         . '::' . $_REQUEST['paymentStatus'] 
                         . "::" . $_REQUEST['userName']
                         . "::" . $_REQUEST['userEmail']
                         . "::" . $_REQUEST['paymentData']
                 		 . "::" . $mrh_pass2); 

		$culture = $_REQUEST['eshopId']
		 . '::' . $_REQUEST['orderId']
		 . "::" . $_REQUEST['serviceName']
		 . '::' . $_REQUEST['eshopAccount']
		 . "::" . $out_summ 
		 . '::' . $_REQUEST['recipientCurrency'] 
		 . "::" . $_REQUEST['paymentStatus']
		 . "::" . $_REQUEST['userName'] 
		 . "::" . $_REQUEST['userEmail'] 
		 . "::" . $_REQUEST['paymentData'];

		$in_curr	= 0; /* �� ���������� ������, ������� �� ������������ */
		// ��������� ��� �� success (��� result) ��� ������������� (in_curr)
		if ($val = SQL::getval('inv_id', $_RBK['robo_core_table'], "inv_id = '$inv_id' and success_result = '5' and type = '1'", '', DEBUG)) {
			if (Main::comm_inc('rbk.already.html', $file, 'rbk')){
				include_once "$file";
			}		
			return;
		}

		if ($my_crc !=$crc){ 	/* ���� �� ������� */
			$_RBK['result'] = 0;
			$title = "Result Inc is bad\n";
			$template = 'rbk.success_bad.item.html'; // ���� ����� success
		} else {
			$_RBK['result'] = 1;
			$title = "OK$inv_id";															/* ����� ��� ��������� */
			$data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/log/log');
			$template = 'rbk.success_yes.item.html';
		}
		
		if (is_file( $_CORE->SiteModDir.'/rbk.'.$type.'_action.php')) {
			include_once($_CORE->SiteModDir.'/rbk.'.$type.'_action.php'); 				
			/* ���������� ���� ��������� �� ����� �����, ���� ��������� success �� ������ in_curr - ������ ��������� ������ */
		}
        if ($_REQUEST['paymentStatus']==5 && $val = SQL::getval('inv_id', $_RBK['robo_core_table'], "inv_id = '$inv_id' and success_result = '3' ", '', DEBUG)){
            $res = SQL::upd($_RBK['robo_core_table'], 
						"type='$_RBK[result]', success_result='$_REQUEST[paymentStatus]'", 
                        "inv_id = '$inv_id'", DEBUG);
        } else {
			$res = SQL::ins($_RBK['robo_core_table'], 
						"time, 		ip, 		type, summa,      success_result, shp_item, in_curr, culture, inv_id, inv_desc, crc1, crc2, user_tranzak, user_mail", 
						"'".time()."', '$rem_addr', '".$_RBK['result']."', '$out_summ', '$_REQUEST[paymentStatus]', '$shp_item', '$in_curr', '$culture', '$inv_id', '$inv_desc', '$crc', '$my_crc', '$type check rbk', '$_REQUEST[userEmail]'",DEBUG);
        } /* SQL ������ */

		// if it's check of rbk (result) - then just exit, if 'success' url asked then show the answer
		if ($type == 'result') {
			ob_end_clean();
			die($title);

		}else{

			if (Main::comm_inc($template, $file, 'rbk')){
				include_once "$file";
			}
		}

		/*
		OutSum			- ���������� �����. ����� ����� �������� � ��� ������, ������� ���� ������� ��� ����������� ��������. ������ ������������� ����� - ����������� �����.
		InvId			- ����� ����� � ��������
		SignatureValue	- ����������� ����� MD5 - ������ �������������� ����� 32-��������� ����� � 16-������ ����� � ����� �������� (����� 32 ������� 0-9, A-F). ����������� �� ������, ���������� ��������� ���������, ����������� ':', � ����������� sMerchantPass2 - (��������������� ����� ��������� �����������������) �.�. nOutSum:nInvId:sMerchantPass2[:���������������� ���������, � ��������������� �������]
						  � ������� ���� ��� ������������� �������� ���� �������� ���������������� ��������� shpb=xxx � shpa=yyy �� ������� ����������� �� ������ ...:sMerchantPass2:shpa=yyy:shpb=xxx 
		*/
	}
	
	
		/*
		OutSum			- ���������� �����. ����� ����� �������� � ��� ������, ������� ���� ������� ��� ����������� ��������. ������ ������������� ����� - ����������� �����.
		InvId				- ����� ����� � ��������
		SignatureValue	- ����������� ����� MD5 - ������ �������������� ����� 32-��������� ����� � 16-������ ����� � ����� �������� (����� 32 ������� 0-9, A-F). ����������� �� ������, ���������� ��������� ���������, ����������� ':', � ����������� sMerchantPass1 (����������� ��� �����������) �.�. nOutSum:nInvId:sMerchantPass1[:���������������� ���������, � ��������������� �������]. 
						  � ������� ���� ��� ������������� �������� ���� �������� ���������������� ��������� shpb=xxx � shpa=yyy �� ������� ����������� �� ������ ...:sMerchantPass1:shpa=yyy:shpb=xxx
		Culture 		- ���� ������� � ��������, ��������� ��� ������������� ������. ��������: en, ru. 
		*/
//	}
	
	function fail_url(){ 																	/* ������������� ������������ ��� ������ �� ������ (FailURL) */
		global $_CORE, $_RBK; 													/* ���������� ���������� , ���������� ����� �������� */
		$mrh_pass2 	= $_RBK['pass_2']; 												/* ������ ��� ����������� � ������ �������� ��������� */
		$tm 		= getdate(time()+9*3600);												/* ���� */
		$date 		= "$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]"; 	/* ���� 2 */
		$out_summ 	= $_REQUEST["recipientAmount"]; 													/* ����� ������ */
		$inv_id 	= $_REQUEST["paymentId"]; 													/* ���������� ����� �������� */
		$shp_item 	= $_REQUEST["serviceName"]; 												/* ������������ */
		$inv_desc 	= $_REQUEST["userField_3"]; 												/* ����� */
		$crc 		= $_REQUEST["hash"]; 								/* ��� ��� ��������� */
		$rem_addr 	= $_RBK['REMOTE_ADDR']; 
		// $my_crc 	= strtoupper(md5("$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item")); 	
        /* ��� ��� ��������� */
		$my_crc 	= strtoupper(md5($_REQUEST['eshopId']
                		 . "::" . $out_summ 
                		 . '::' . $_REQUEST['recipientCurrency'] 
                		 . "::" . $_REQUEST['paymentStatus']
                         . "::" . $_REQUEST['user_email']
                         . "::" . $_REQUEST['serviceName']
                		 . '::' . $_REQUEST['orderId']
                         . '::' . $_REQUEST['userField_1']
                         . '::' . $_REQUEST['userField_2']
                         . '::' . $_REQUEST['userField_3']
                 		 . "::" . $mrh_pass2)); 	/* ������������ ���� */

		if ( Main::comm_inc('rbk.fail_action.php', $file, 'rbk')) { /* ���������� ���� �� ����-����� ��� ��������� ���������� �������� */
			include_once "$file"; 
		}

		$res = SQL::ins($_RBK['robo_core_table'], 
						"time, 		ip, 		type, summa,      success_result, shp_item, in_curr, culture, inv_id, inv_desc, crc1, crc2, user_tranzak, user_mail", 
						"'".time()."', '$rem_addr', '0', '$out_summ', '0', '$shp_item', '$in_curr', '$culture', '$inv_id', '$inv_desc', '$crc', '$my_crc', 'fail url rbk asked', '$_REQUEST[userEmail]'",DEBUG); /* SQL ������ */

		if (Main::comm_inc('rbk.fail.item.html', $file, 'rbk')){
			include_once "$file";
		}
		/*
		OutSum				- ���������� �����. ����� ����� �������� � ��� ������, ������� ���� ������� ��� ����������� ��������. ������ ������������� ����� - ����������� �����.
		InvId 				- ����� ����� � ��������
		SignatureValue  	- ����������� ����� MD5 - ������ �������������� ����� 32-��������� ����� � 16-������ ����� � ����� �������� (����� 32 ������� 0-9, A-F). ����������� �� ������, ���������� ��������� ���������, ����������� ':', � ����������� sMerchantPass1 (����������� ��� �����������) �.�. nOutSum:nInvId:sMerchantPass1[:���������������� ���������, � ��������������� �������].
							� ������� ���� ��� ������������� �������� ���� �������� ���������������� ��������� shpb=xxx � shpa=yyy �� ������� ����������� �� ������ ...:sMerchantPass1:shpa=yyy:shpb=xxx
		sCulture 			- ���� ������� � ��������, ��������� ��� ������������� ������. ��������: en, ru. 
		
		*/

	}
}

?>