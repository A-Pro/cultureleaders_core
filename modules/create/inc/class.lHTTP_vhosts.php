<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : class.lHTTP_vhosts.php
// PROJECT NAME : Apache Virtual Hosts
// DATE         : 21.05.2003
// Author       : Andrew Lyadkov (land@inforis.ru)
// Discription  : ������ ��� ������ ������������ ������
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////


/*
USAGE:
-----

$FILE = 'd:\hosting\http\conf\vhosts\LapTop_vhosts.conf';

$vh = new lHTTP_vhosts( $FILE );

if (!$vh) {
	die( $vh->error );
}

echo "�������: ".$vh->count()." ����������� ������.\n"; 

echo "������ ������ �����������:\n".$vh->get_content()."\n";

echo "��������� ������ �����������:\n".$vh->get_content($vh->count()-1)."\n";

echo "Dump ������� ����� � ������� (����� �� ���������):\n";
print_r($vh->get_fields_of());

echo "Dump ������� ����� � ������� (����� ServerName):\n";
print_r($vh->get_fields_of(0,array('ServerName')));

echo "Dump ���������� ����� � ������� (����� ���):\n";
print_r($vh->get_fields_of($vh->count()-1,array('*')));

echo "���� �� ����� � ServerName = land.nnov ��� ����� ?\n";
$n = $vh->key2num('SerVerName','land.nnov');
if ($n >= 0) {
	echo "����: $n\n";
	print_r($vh->get_fields_of($n,array('*')));
}else{
	echo "���!\n";
}
echo "----";

�����:

//---------- Interprite ----------
//X-Powered-By: PHP/4.2.3
//Content-type: text/html; charset=win-1251
//
//�������: 5 ����������� ������.
//������ ������ �����������:
//ServerName land.home
//ServerAdmin _land@mail.nnov.ru
//DocumentRoot "/web/virtual/land"
//<Directory "/web/virtual/land">
//Options Includes FollowSymLinks MultiViews
//AllowOverride None
//Order allow,deny
//Allow from all
//</Directory>
//ScriptAlias /cgi-bin/ "/web/virtual/land/cgi-bin/"
//ErrorLog "/hosting/http/logs/land_error.log"
//CustomLog "/hosting/http/logs/land_custom.log" common
//��������� ������ �����������:
//ServerName land.nnov
//ServerAdmin _land@mail.nnov.ru
//DocumentRoot "/hosting/sites/wwland"
//<Directory "/hosting/sites/wwland">
//Options Includes FollowSymLinks MultiViews
//AllowOverride None
//Order allow,deny
//Allow from all
//</Directory>
//ScriptAlias /cgi-bin/ "/hosting/sites/wwland/cgi-bin/"
//ErrorLog "/hosting/http/logs/wwland_error.log"
//CustomLog "/hosting/http/logs/wwland_custom.log" common
//Dump ������� ����� � ������� (����� �� ���������):
//Array
//(
//    [ServerName] => land.home
//    [DocumentRoot] => /web/virtual/land
//)
//Dump ������� ����� � ������� (����� ServerName):
//Array
//(
//    [ServerName] => land.home
//)
//Dump ���������� ����� � ������� (����� ���):
//Array
//(
//    [ServerName] => land.nnov
//    [ServerAdmin] => _land@mail.nnov.ru
//    [DocumentRoot] => /hosting/sites/wwland
//    [<Directory] => /hosting/sites/wwland>
//    [Options] => Includes FollowSymLinks MultiViews
//    [AllowOverride] => None
//    [Order] => allow,deny
//    [Allow] => from all
//    [ScriptAlias] => /cgi-bin/ /hosting/sites/wwland/cgi-bin/
//    [ErrorLog] => /hosting/http/logs/wwland_error.log
//    [CustomLog] => /hosting/http/logs/wwland_custom.log common
//)
//���� �� ����� � ServerName = land.nnov ��� ����� ?
//����: 2
//Array
//(
//    [ServerName] => nnov
//    [ServerAdmin] => _land@mail.nnov.ru
//    [DocumentRoot] => /web/virtual/nnov
//    [<Directory] => /web/virtual/nnov>
//    [Options] => Includes FollowSymLinks MultiViews
//    [AllowOverride] => None
//    [Order] => allow,deny
//    [Allow] => from all
//    [ScriptAlias] => /cgi-bin/ /web/virtual/nnov/cgi-bin/
//    [ErrorLog] => /hosting/http/logs/nnov_error.log
//    [CustomLog] => /hosting/http/logs/nnov_custom.log common
//)
//----Normal Termination
//Output completed (0 sec consumed).

*/

class lHTTP_vhosts {
	// ���� �� ����� ������������
	var $path		= '';
	// "������" ������ �����
	var $conf_rows	= array();
	// ���������� ��������������� �� VirtualHost
	var $vhosts		= array();
	//���������� ������
	var $count	= 0;
	// ����������� ���������, ����� �� ������� ������������ ������ 
	// ������������ �����
	var $keys		= array('ServerName', 'ServerAlias', 'DocumentRoot');

	var $error		= '';

	/**
	*
	*/
	function lHTTP_vhosts( $path2conf='' )
	{
		if ($path2conf=='' || !file_exists($path2conf)) {
			$this->error = "Constructor error: file not found ($path2conf)\n";
			return false;
		}
		// load
		$this->conf_rows = file($path2conf);
		// cleaning
		$this->remove_comments();
		// group by host
		$this->parse_conf($this->vhosts,0);
		$this->count = sizeof($this->vhosts);
	}

	
	/** remove_comments(&$arr)
	*  ������� ������ ���������� (��������� #) � ������ ������
	*/
	function remove_comments()
	{
		$j = 0;
		for ($i=0;$i < sizeof($this->conf_rows);$i++) {
			$this->conf_rows[$i] =stripslashes(trim($this->conf_rows[$i]));
			if (!preg_match("/^s*#.*/",$this->conf_rows[$i]) && !preg_match("/^s*$/",$this->conf_rows[$i])) {
				$this->conf_rows[$j] = $this->conf_rows[$i];
				$j++;
			}
		}	
	}
	
	/** parse_conf( &$ah, $i )
	*  ����������� �������, ��� ����������� �� ����������� ������
	*/
	function parse_conf( &$ah, $i )
	{
		$tmp = array();
		while (isset($this->conf_rows[$i]) && !stristr($this->conf_rows[$i],"<virtualhost")) $i++;
		while (isset($this->conf_rows[++$i]) && !stristr($this->conf_rows[$i],"</virtualhost")) {
			$tmp[] = $this->conf_rows[$i];
		}
		if (sizeof($tmp)>0) $ah[] = $tmp;
		if ($i<sizeof($this->conf_rows)) $this->parse_conf($ah,++$i);
	}

	/** PUBLIC
	*
	*/
	function chkeys( $keys = '')
	{
		if ($keys=='') {
			$this->error = "chkeys error: empty key.\n";
			return false;
		}
		$this->keys = $keys;
	}

	/** PUBLIC
	*
	*/
	function count()
	{
		return $this->count;
	}

	/** PUBLIC get_fileds( [$keys] )
	*   ���������� ������ � �������, ��������� �� ��������� ��� ���������
	*   ��� ����� $num ��� �������
	*/
	function get_fields_of( $num=0, $keys='' )
	{
		$tmp = $this->keys;
		$num = intval($num);
		if ($num >= sizeof($this->vhosts) || $num < 0 ) {
			$this->error = "HTTP_vhosts::get_fileds() error: attribute \$num is not valid.\n";
		}

		if ($keys!='' && !is_array($keys)) {
			$this->error = "HTTP_vhosts::get_fileds() error: attribute \$keys is not array.\n";
			return array();

		}elseif ($keys!='') {
			$this->keys = $keys;
		} 
		// ��� �� ��������� �����?
		$All = (in_array('*',$this->keys));

		$r = array();
		for	($i=0; $i<sizeof($this->vhosts[$num]); $i++){
			if (!$All) {
				// �� ���
				for ($j=0; $j<sizeof($this->keys); $j++) {
					if (preg_match("/".$this->keys[$j]." (.*)/i",$this->vhosts[$num][$i],$m)) {
						$r[$this->keys[$j]] = str_replace('"','',$m[1]);
						continue 1;
					}
				}
			}else {
				// ���
					if (preg_match("/(.*?)[ \t]+(.*)/",$this->vhosts[$num][$i],$m)) {
						$r[$m[1]] = str_replace('"','',$m[2]);
						continue 1;
					}
			}
		}
		$this->keys = $tmp;
		return $r;
	}

	/** PUBLIC
	*
	*  ���������� ���������� ��������� VirtualHost � ������� num ����������� \n
	*/
	function get_content( $num = 0 )
	{
		$num = intval($num);
		if ($num >= sizeof($this->vhosts) || $num < 0 ) {
			$this->error = "HTTP_vhosts::get_content() error: attribute \$num is not valid.\n";
		}
		return join("\n", $this->vhosts[$num]);
	}

	/** PUBLIC  key2num($key='',$val='')
	* ���������� ������ ����� � ������� ������ ������ ���� $key �� ��������� $val
	*/
	function key2num($key='',$val='')
	{
		if ($key == '' || $val == '') {
			$this->error = "HTTP_vhosts::key2num() error: wrong or empty attribute.\n";
			return -1;
		}
		for ($i = 0; $i < $this->count; $i++ ) {
			$data = $this->get_fields_of($i,array($key));
			foreach ($data as $k=>$v) {
				if (preg_match("/".$v."/i",$val)) {
					return $i;
				}
				continue 1;
			}
		}
		return -1;
	}

} 

?>