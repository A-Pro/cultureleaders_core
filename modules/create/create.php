<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : create.php
// PROJECT NAME : CREATE new site by Sited
// BEGIN        : 09.03.2003
// Author       : Andrew Lyadkov (andrey@lyadkov.nnov.ru)
// Discription  : �������� ������ �������
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
/*
USE:
----
=== FILES ===
1. ������� ����� ������� (���������� �����)
2. �������� ���������������� �����
=== DB ===
3. ������� ������������ � ����� ���� ������,������, �������������
=== HTTP ===
4. ������� ����������� ����, ������� �������
=== DNS ===
5. ������� �����

=== LOG ===
5. �������� ����� ����� ����� ��� ������, ������ �� �������� � �.�.
*/
global $_CORE, $_CREATE;
include_once "create.inc.phtml";

switch ($Cmd) {
	case "adm_menu": // 17.11.2005

		global $_PROJECT;
		if (!empty($_CREATE['ADM_MENU']))
			$_PROJECT['ADM_MENU']	= array_merge($_PROJECT['ADM_MENU'], $_CREATE['ADM_MENU']);  // conf.inc.php
		break;

	case 'content/title/short':
	case 'content/title':
		global $_PROJECT;
		$_PROJECT['PATH_2_CONTENT'][0]	= array(
			'name'	=> ($_CORE->LANG != 'en')?'�������� �������':'Project creating' ,
			'path'	=> '/create/'
		);
		echo $_CORE->cmd_exec(array('user','/'.$Cmd));
	case 'content/path':
		echo ' ';
	break;
	case "go": 
		// ��������
		if (CREATE::check($_REQUEST['create'])){
			// === FILES ===
			$new_proj	= $_CREATE['PathToProj'].$_REQUEST['create']['domain']."/";
			mkdir($new_proj);
			ob_start();
			FILE::rn_dir($_CREATE['tpl'][$_REQUEST['create']['conf']]['dir'], $new_proj);
			$err	= ob_get_contents();
			ob_end_clean();
			if (!empty($err)) {
				$_CREATE['Error']	= _CREATE_FILE_COPPING_ERROR;
				rmdir($new_proj);
			} else {
				// === DB ===
				if ( !is_file($_CREATE['tpl'][$_REQUEST['create']['conf']]['sql'])) {
					$_CREATE['Error']	= _CREATE_WRONG_SQL_FILE;
				}else {
					$trans	= array( 
						'__DB_BASE__' => 'auto_'.$_REQUEST['create']['domain'],
						'__DB_USER__' => 'auto_'.$_REQUEST['create']['domain'],
						'__DB_PWD__' => CREATE::create_pwd()
					);
					// CONFIG
					CREATE::conf_file_parse(
						$new_proj.'/'.$_CREATE['tpl'][0]['config'],
						$new_proj.'/'.$_CREATE['tpl'][0]['config'],
						$trans
					);
					// Prepare DB file
					CREATE::conf_file_parse(
						$_CREATE['tpl'][$_REQUEST['create']['conf']]['sql'],
						$_CREATE['tpl'][$_REQUEST['create']['conf']]['sql'].'.'.$_REQUEST['create']['domain'],
						$trans
					);
					// Query DB file
					$f = escapeshellcmd($_CREATE['tpl'][$_REQUEST['create']['conf']]['sql'].'.'.$_REQUEST['create']['domain']);
					$res = exec($_CREATE['PathToMySQL']." -u land -p123 < ".$f );
					if (!empty($res)) {
						$_CREATE['Error']	= _CREATE_WRONG_CREATE_BASE."<br>".$res;
					}else {
						// === HTTP ===
						// Prepare conf
						if (is_file($_CREATE['PathToApacheConfTpl'])) {
							CREATE::conf_file_parse(
								$_CREATE['PathToApacheConfTpl'],
								$_CREATE['PathToApacheConfAuto'].$_REQUEST['create']['domain'].'.conf',
								array('__DOMAIN__' => $_REQUEST['create']['domain'])
							);
						}
						// Restart
						if (is_file($_CREATE['PathToApache'])) {
							$res = exec($_CREATE['PathToApache']." -k restart ");

							// DNS 
							$fp = fopen("c:\windows\system32\drivers\etc\hosts", "a");
							fputs($fp, "127.0.0.1 ".$_REQUEST['create']['domain'].".my \n");
							fclose($fp);

							include_once "well_done.inc.html";
						}else {
							$_CREATE['Error']	= _CREATE_APACHE_NOT_FOUND;
						}
					}
				}
				unset($_REQUEST['create']);
			}
		}else {
//			include_once "form.inc.html";
		}
	break;
//	case "admin":
//		include $land_root."admin/index.php";
//		break;
//	case "admin/users":
//		include $land_root."admin/admin_subscribe.php";
//		break;
	default:
//		include_once "form.inc.html";
}
include_once "form.inc.html";

?>