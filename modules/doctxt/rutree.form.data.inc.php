<?
$FORM_DATA_TREE= array (
  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden'
  ),
  'pos' => 
  array (
    'field_name' => 'pos',
    'name' => 'form[pos]',
    'title' => 'pos',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
    'sub_type' => 'bigint'
  ),
'parent' => 
  array (
    'field_name' => 'parent',
    'name' => 'form[parent]',
    'title' => Main::get_lang_str('parent', 'doctxt'),
    'maxlen' => '1024',
    'must' => '0',
    'type' => 'textarea',
    'rows' => '7'
  ),
  'name' => 
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => Main::get_lang_str('title', 'doctxt'),
    'maxlen' => '255',
    'must' => '1',
    'type' => 'textbox',
    'size' => '80'
  ),
  'path' => 
  array (
    'field_name' => 'path',
    'name' => 'form[path]',
    'title' => Main::get_lang_str('path', 'doctxt'),
    'maxlen' => '1024',
    'must' => '0',
    'type' => 'textarea',
    'rows' => '7'
  ),
  'link' => 
  array (
    'field_name' => 'link',
    'name' => 'form[link]',
    'title' => Main::get_lang_str('link', 'doctxt'),
    'maxlen' => '255',
    'must' => '0',
    'type' => 'textbox',
    'size' => '80',
	'readonly' => 0,
  ),
  'parent_id' => 
  array (
    'field_name' => 'parent_id',
    'name' => 'form[parent_id]',
    'title' => Main::get_lang_str('parent_id', 'doctxt'),
    'maxlen' => '20',
    'must' => '0',
    'type' => 'hidden',
    'sub_type' => 'bigint',
  ),
  'readonly' => 
  array (
    'field_name' => 'readonly',
    'name' => 'form[readonly]',
    'title' => Main::get_lang_str('readonly', 'doctxt'),
    'maxlen' => '10',
    'must' => '0',
    'type' => 'textbox',
    'size' => '10',
  ),
  'nokill' => 
  array (
    'field_name' => 'nokill',
    'name' => 'form[nokill]',
    'title' => Main::get_lang_str('nokill', 'doctxt'),
    'maxlen' => '10',
    'must' => '0',
    'type' => 'textbox',
    'size' => '10',
  ),
  'needreg' => 
  array (
    'field_name' => 'needreg',
    'name' => 'form[needreg]',
    'title' => Main::get_lang_str('needreg', 'doctxt'),
    'maxlen' => '10',
    'must' => '0',
    'type' => 'textbox',
    'size' => '10',
  ),
  'nomenu' => 
  array (
    'field_name' => 'nomenu',
    'name' => 'form[nomenu]',
    'title' => Main::get_lang_str('nomenu', 'doctxt'),
    'maxlen' => '10',
    'must' => '0',
    'type' => 'textbox',
    'size' => '10',
  ),
  'menunolink' => 
  array (
    'field_name' => 'menunolink',
    'name' => 'form[menunolink]',
    'title' => Main::get_lang_str('menunolink', 'doctxt'),
    'maxlen' => '10',
    'must' => '0',
    'type' => 'textbox',
    'size' => '10',
  ),
		'hidden' =>  // dlja standarta
  array (
    'field_name' => 'hidden',
    'name' => 'form[hidden]',
    'title' => Main::get_lang_str('ne_publ', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
		'default' => 0,
  ), 
  'ts'	=>
  array (
    'field_name' => 'ts',
    'name' => 'form[ts]',
    'title' => Main::get_lang_str('data', 'db'),
    'must' => 0,
	'size' => 15,
    'maxlen' => 255,
    'type' => 'textbox',
    'readonly' => 'true',
	'default'	=> date('Y-m-d')
  ),
);
?>