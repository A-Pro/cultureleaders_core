<?
global $_CORE,$_CONF;
Main::load_lang_mod('doctxt');
$_DOC['TITLE']	= Main::get_lang_str('documents', 'doctxt');
$_DOC['MODULE_TITLE']	= Main::get_lang_str('struct_site', 'doctxt');
$_DOC['MODULE_DESC']	= Main::get_lang_str('creation_str_site', 'doctxt');
$_DOC['CntLen']	= '200';
$_DOC['ImgsCount']	= '10';
$_DOC['ERROR']	= '';
$_DOC['MODULE_NAME']	= 'doctxt';
//$_DOC['HISTORY_FILE']	= $_CORE->SiteModDir.'/data/history.txt';
$_DOC['HISTORY_FILE']	= $_CORE->ROOT.'/data/'.$_DOC['MODULE_NAME'].'/history.txt';

//$_DOC['HISTORY_FILE']	= DOCTXT::get_data_path('history.txt', $f, DOC_LOOKIG_DATA_FILE);

$_DOC['HISTORY_LEN']	= 5;
$_DOC['CLIENT_CONF_FILE']	= 'doc.conf.inc.php';

$_DOC['ADM_MENU']	= array(
	"/doctxt/?tree" => array("/ico/a_tree.gif", Main::get_lang_str('soderzhanie', 'doctxt'), $_DOC['MODULE_NAME']),
	);

if (!empty($_CONF['FEATURES_USED']['auth_role']) && !empty($_SESSION['SESS_AUTH']['LOGIN'])) {
	$user_perm = SQL::getrow('*',DB_TABLE_PREFIX.'core_authperm', "name = '".$_SESSION['SESS_AUTH']['LOGIN']."' AND `DOCTXT` = 1");
	if($user_perm !== false){
		$_DOC['permisstions'][$user_perm['name']] = true;
	}
}

if (!defined('DOC_LOOKIG_DATA_FILE')) define ("DOC_LOOKIG_DATA_FILE",'file');
if (!defined('DOC_LOOKIG_DATA_DIR')) define ("DOC_LOOKIG_DATA_DIR",'dir');
if (!defined('DOC_LOOKIG_DATA_SET_OLD')) define ("DOC_LOOKIG_DATA_SET_OLD",'old style');
if (!defined('DOC_LOOKIG_DATA_SET_NEW')) define ("DOC_LOOKIG_DATA_SET_NEW",'new style');


if (is_file( $_CORE->SiteModDir.'/doc.conf.inc.php')) 
	include $_CORE->SiteModDir.'/doc.conf.inc.php';

class DOCTXT {

	function get_cont( $doc_alias, $field = '', $return = false )
	{
		global $_CORE, $_DOC, $FORM_DATA, $_PROJECT;
		$dir	= '';
		if ($field == 'name') $field = 'title';
		if (!empty($field)){ // tol'ko odno pole
			if(!empty($_DOC['conf']['tree_from_db'])){
				if (!$_CORE->dll_load('class.DBCQ'))
					die ($_CORE->error_msg());
				if ($field == 'title') $field = 'name';
				$path = str_replace('//','/',"/doctxt/$doc_alias/");
				$value = SQL::getval($field,(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', "path = '$path'");
				if(!empty($value)){
					if($return) return $value;
					else echo $value;
					
					return;
				}else{
					return;
				}
			}
			if (DOCTXT::is_doc($doc_alias, $dir, $field)){
				if ($return) {
					return implode ('', file($dir.'/'.$field));
				}else
					include ($dir.'/'.$field);
			}else{		

//				if (!is_dir($dir))
//					mkdir($dir);
//				$fp	= fopen($dir.'/'.$field, 'w');
//				fputs($fp, $doc_alias);
//				fclose($fp);
//				echo "default Created ".$doc_alias;
//
//				global $_CORE;
//				$fp	= fopen($dir.'/title', 'w');
//				fputs($fp, $_CORE->cmd_pexec('user/content/title/short'));
//				fclose($fp);
			}
		 }elseif (false !== ($form = DOCTXT::load($doc_alias))) { // && is_file($_CORE->SiteModDir.'/doc.view.item.html')
 			include $_CORE->SiteModDir."/doc.form.data.inc.php"; 
			if ($_DOC['conf']['SEO']) Main::add_seo($FORM_DATA); // If need SEO module
			if ($_DOC['conf']['fb_tags']) Main::add_fb_tags($FORM_DATA);
			$_PROJECT['loaded'][] = array( 'module' => $_DOC['MODULE_NAME'], 'data' => $form );

			// VYVOD DOKUMENTA
			if (DOCTXT::inc_tpl('view.item.html', $file)){
				include "$file"; 
			}
//			include $_CORE->SiteModDir.'/doc.view.item.html';

		 }else {
//				echo "wow";
//		 	echo $_CORE->PATHMODS.$_DOC['MODULE_ALIAS'].'/doc.view.item.html';
		 }
	}

	/**
	*
	*/
	function load( $doc_alias )
	{
		global $_DOC,$_CORE;
		$data	= array();
    if(!empty($_DOC['conf']['tree_from_db'])){
      if (!$_CORE->dll_load('class.DBCQ'))
        die ($_CORE->error_msg());
      if($value = SQL::getall("*",(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', "path = '/doctxt/$doc_alias' or path = '/doctxt/$doc_alias/'","",0)){
        $data = $value[0];
      }else{
        return false;
      }
    } else {
		
  		if (DOCTXT::is_doc($doc_alias, $dir)) {
  			if ($handle = opendir($dir))
  				while (false !== ($file = readdir($handle))){
  					if (is_file($dir.'/'.$file) && !in_array($file, array('.','..'))) {
  						$data[$file]	= implode('',file($dir.'/'.$file));
  					}
  			}
  		}else {
  			return false;
  		}
    }
		global $doc;
		$doc	= @$data['doc'];
		for ($i=0;$i<$_DOC['ImgsCount'];$i++) {
			global ${'doc'.$i};
			${'doc'.$i}	= @$data['doc'.$i];
		}
    
		$_DOC['CURR_DATA'] = $data;
//		$_DOC['CURR_DATA']['_path'] = $_CORE->DIRMODS.$_DOC['MODULE_NAME'].'/data/'.$doc_alias.''; 
//		$_DOC['CURR_DATA']['_path'] = '/data/'.$_DOC['MODULE_NAME'].'/data/'.$doc_alias.''; // $_CORE->DIRMODS.
		
		$_DOC['CURR_DATA']['_path'] = DOCTXT::get_data_link($doc_alias, $dir, DOC_LOOKIG_DATA_DIR);
		return $data;
		
	}

	/**
	*
	*/
	function is_doc( $doc_alias, &$dir, $field = '')
	{
		global $_CORE, $_DOC;
//		$dir	=	$_CORE->SiteModDir.'/data/'.$doc_alias;
//		$dir	=	$_CORE->ROOT.'/data/'.$_DOC['MODULE_NAME'].'/data/'.$doc_alias;
      if ($doc_alias=="/") $doc_alias = "";
//		$dir = DOCTXT::get_data_path($doc_alias, $dir, DOC_LOOKIG_DATA_DIR);
		$dir = (is_dir($_CORE->ROOT.$_CORE->DATA_PATH)) 
				? $_CORE->ROOT.$_CORE->DATA_PATH.$_DOC['MODULE_NAME'].$_CORE->DATA_PATH.$doc_alias
				: $_CORE->SiteModDir.'/data/'.$doc_alias;
				
		if (DEBUG)
			$_CORE->log_message('$dir = '.print_r($dir,1));
			
		$file	=	$dir.'/'.$field;

//		echo $dir . "=!$field=" . $file; 
//		exit;
				
//		return (!empty($field)) ? is_file($file) : is_dir($dir);
		return (!empty($field)) ? is_file($file) : is_dir($dir);
	}

	/**
	*
	*/
	function show_form( $data )
	{
		global $_CORE, $form, $_DOC;		
		$form	= $data;
		$_CORE->dll_load('class.cForm');
		include $_CORE->SiteModDir."/doc.form.data.inc.php";
		if ($_DOC['conf']['SEO']) Main::add_seo($FORM_DATA);  // if need SEO module
		if ($_DOC['conf']['fb_tags']) Main::add_fb_tags($FORM_DATA);
		$_DOC['cForm']	= new cForm( $FORM_DATA );

		// FORMA DOBAVLENIJa/REDAKTIROVANIJa

		if (DOCTXT::inc_tpl('form.inc.phtml', $file))
			include "$file";

//		if (is_file($_CORE->SiteModDir."/doc.form.inc.phtml"))
//			include $_CORE->SiteModDir."/doc.form.inc.phtml";
//		else
//			include $_CORE->CoreModDir."/doc.form.inc.phtml";
	}
//
//	/**
//	*
//	*/
//	function check_form( &$form, $path )
//	{
//		global $_CORE, $_DOC;
//		include "doc.form.data.inc.php";
//		# podgruzka neobhodimyh modulej
//		if (empty($_DOC['cForm'])) {
//			$_CORE->dll_load('class.cForm');
//			$_DOC['cForm']	= new cForm( $FORM_DATA );
//		}
//		#
//		#	pervojetapnaja proverka
//		#
//		if ($_DOC['cForm']->Chk_come()) {										// if BAD
//			$_DOC['ERROR'] = $_DOC['cForm']->Error;
//			return false;
//		}
//		$form['path'] = STR::translit($form['title'], 4);
//	}
	/**
	*
	*/
	function check_form( &$form, $path )
	{
		global $_CORE, $_DOC, $_DOC,$_KAT;
		# podgruzka neobhodimyh modulej
		$form['path'] = $path; // STR::translit($form['title'], 4);
		include $_CORE->SiteModDir."/doc.form.data.inc.php";
		if ($_DOC['conf']['SEO']) Main::add_seo($FORM_DATA);
		if ($_DOC['conf']['fb_tags']) Main::add_fb_tags($FORM_DATA);		
		if (empty($_DOC['cForm'])) {
			$_CORE->dll_load('class.cForm');
			$_DOC['cForm']	= new cForm( $FORM_DATA );
		}
		#
		# Diry 
		if (preg_match("|(.*/)".$_KAT['MODULE_NAME']."/(.+)|",$path,$m) && $_DOC['conf']['tree_from_db'] == true )
			$path = $m[1];
		if (!DOCTXT::is_doc($path, $dir)) {
			if (!is_dir($dir) && !mkdir($dir)){
				if (@DEBUG) $_CORE->log_message('doctxt::save', 'warning', 'Path in dir not found : '.$dir);
				return false;
			}else {
				if (@DEBUG) $_CORE->log_message('doctxt::save', 'good', 'Path present : '.$dir);
			}
		}
		#
		# Dira dlja fotok
		if (!is_dir($dir.'/file') && !mkdir($dir.'/file')) {
			if (@DEBUG) $_CORE->log_message('doctxt::save', 'warning', 'Path for DOC not created : '.$dir.'/doc');
			$_DOC['ERROR']	= 'Path for DOC not created';
			return false;
		}
		#
//		#	pervojetapnaja proverka
		#
		if ($_DOC['cForm']->Chk_come()) {										// if BAD
			$_DOC['ERROR'] .= $_DOC['cForm']->Error;
			return false;
		}
		// Tut dlja kopirovanija fajla i t.p. 
		if (!$_DOC['cForm']->GetSQL('update')){
			$_DOC['ERROR'] .= $_DOC['cForm']->Error;
			return false;
		}

		return true;
	}

	/**
	*
	*/
	function save( &$form, $doc_alias )
	{
		$path = $doc_alias;
		global $_DOC, $_CORE, $doc, $doc_name, $kill_doc, $_KAT;
    if(!empty($_DOC['conf']['tree_from_db'])){
      if (!$_CORE->dll_load('class.DBCQ'))
        die ($_CORE->error_msg());
    }

		if (@DEBUG) $_CORE->log_message('doctxt::save', 'good', "files: $doc-".var_export($_FILES['doc'], true));

		if(!empty($_FILES['doc']['name'])) {
				$doc = true;
		 		$doc_name = $_FILES['doc']['name'];
		}
		if(!empty($_REQUEST['kill_doc'])) {
			$kill_doc = true;
		}
		for ($i=0;$i<$_DOC['ImgsCount'];$i++) {
			if (!empty($_FILES['doc'.$i]['name'])) {
				${'doc'.$i} = $_FILES['doc'.$i];
				${'doc'.$i."_name"} = $_FILES['doc'.$i]['name'];
			}
			if(!empty($_REQUEST['kill_doc'.$i])) {
				${'kill_doc'.$i} = true;
			}
		}
		if (@DEBUG) $_CORE->log_message('doctxt::save', 'good', "Global doc: $doc-".$_DOC['ImgsCount']."=".intval($kill_doc)."+".$doc1);

		// esli demo
		if ($_CORE->IS_DEMO) return false;
		//if( DOCTXT::is_readonly( $_DOC['TREE'], "/".$_DOC['MODULE_NAME']."/".$doc_alias )) return false;
		#
		# vtoroj raz pervyj v check 
		if (preg_match("|(.*/)".$_KAT['MODULE_NAME']."/(.+)|",$doc_alias,$m) && $_DOC['conf']['tree_from_db'] == true )
			$path = $m[1];
		if (!DOCTXT::is_doc($path, $dir)) {
			if (!is_dir($dir) && !mkdir($dir)){
				if (@DEBUG) $_CORE->log_message('doctxt::save', 'warning', 'Path ('.$doc_alias.' :: in dir not found : '.$dir);
				return false;
			}else {
				if (@DEBUG) $_CORE->log_message('doctxt::save', 'good', 'Path present : '.$dir);
			}
		}
		#
		# a esli uzhe est' s takim nazvaniem? 
		if (empty($form['path']) && is_file($dir.'/title')) { // dobavljaem novyj, a takaja dira uzhe est', nehorosho
			if (@DEBUG) $_CORE->log_message('doctxt::save', 'warning', 'New record, but path present : '.$dir);
			$_DOC['ERROR']	.= Main::get_lang_str('doc_already_there', 'doctxt');
			return false;
		}
    $empty = 1;
		foreach ($form as $field => $value) {
		  if(!empty($_DOC['conf']['tree_from_db'])){
		    $value = stripslashes($value);
        if ($field != 'path'){
          $sql_update .= ($empty) ? sprintf(" `%s` = '%s'", $field, $value ) : sprintf(", `%s` = '%s'", $field, $value );
          if($field == 'title') $sql_update .= sprintf(", `%s` = '%s'", 'name', $value );
    			$empty = 0;
        }
      } else {
  			$fp	= fopen($dir.'/'.$field, 'w');
  			if (empty($fp)) {
  				if (@DEBUG) $_CORE->log_message('doctxt::save', 'warning', 'Couldn`t open for save data: '.$dir.'/'.$field);
  				$_DOC['ERROR']	= Main::get_lang_str('�ouldnt_open', 'doctxt');
  				return false;
  			}
  			if (@DEBUG) $_CORE->log_message('doctxt::save', 'good', "Saved : $field");
  //			$tr	= array( '"' => "'", "\r" => "" );
  //			fputs($fp, stripslashes(strtr($value,$tr)));
  			fputs($fp, stripslashes($value));
  //			fputs($fp, $value);
  			fclose($fp);
      }
 			// esli menjalsja ili sohranjalsja kontent, nado sohranit' v histori 
			if ($field == 'cont') {
				// Sohranim v histori udachnyj process.
				DOCTXT::save_history(date('Y-m-d'), $doc_alias); // $_DOC['MODULE_NAME'].'/'.
			} 
		}
    if(!empty($_DOC['conf']['tree_from_db'])){
      $res_upd = SQL::upd((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', $sql_update.", ts='".time()."'", "path = '/doctxt/$doc_alias' or path = '/doctxt/$doc_alias/'");
      if($res_upd->ErrorQuery){
        if (@DEBUG) $_CORE->log_message('doctxt::save_todb', 'warning', 'Couldn`t update for save data: '.$res_upd->ErrorQuery.' / field:'.$field);
				$_DOC['ERROR']	= Main::get_lang_str('�ouldnt_open', 'doctxt');
      }else{
        if (@DEBUG) $_CORE->log_message('doctxt::save_todb', 'good', "Saved : $field");
      }
    }
		// if DOC_FILE
		if (!empty($kill_doc)) {
		  if(!empty($_DOC['conf']['tree_from_db'])){
		    $res_upd = SQL::upd((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', "doc = ''", "path = '/doctxt/$doc_alias' or path = '/doctxt/$doc_alias/'");
		  }
				$fp	= fopen($dir.'/doc', 'w');
				fclose($fp);
		}
		if (!empty($doc) && !empty($doc_name)) {
		  if(!empty($_DOC['conf']['tree_from_db'])){
  			$fp	= fopen($dir.'/doc', 'w');
  			if (empty($fp)) {
  				if (@DEBUG) $_CORE->log_message('doctxt::save', 'warning', 'Couldn`t open for save data: '.$dir.'/doc');
  				$_DOC['ERROR']	= Main::get_lang_str('�ouldnt_open', 'doctxt');
  				return false;
  			}
  			if (@DEBUG) $_CORE->log_message('doctxt::save', 'good', "Saved : '/doc'-$doc");
		    $tr	= array( '"' => "'" );
  			$type = strrchr( $doc_name,'.');
  			
  //			print $doc_name; die();
  			if (!isset($_DOC['cForm']->Arr['doc']['newname_func']))
  				$file_name 	= DOCTXT::get_file_name();
  			else
  				eval('$file_name 	= '.$_DOC['cForm']->Arr['doc']['newname_func'].';');
		    $res_upd = SQL::upd((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', "doc = '".stripslashes(strtr($file_name.$type,$tr))."'", "path = '/doctxt/$doc_alias' or path = '/doctxt/$doc_alias/'");
		    
  			fputs($fp, stripslashes(strtr($file_name.$type,$tr)));
  			fclose($fp);
		  }else {
  			$fp	= fopen($dir.'/doc', 'w');
  			if (empty($fp)) {
  				if (@DEBUG) $_CORE->log_message('doctxt::save', 'warning', 'Couldn`t open for save data: '.$dir.'/doc');
  				$_DOC['ERROR']	= Main::get_lang_str('�ouldnt_open', 'doctxt');
  				return false;
  			}
  			if (@DEBUG) $_CORE->log_message('doctxt::save', 'good', "Saved : '/doc'-$doc");
  		
  			$tr	= array( '"' => "'" );
  			$type = strrchr( $doc_name,'.');
  			
  //			print $doc_name; die();
  			if (!isset($_DOC['cForm']->Arr['doc']['newname_func']))
  				$file_name 	= DOCTXT::get_file_name();
  			else
  				eval('$file_name 	= '.$_DOC['cForm']->Arr['doc']['newname_func'].';');
  			fputs($fp, stripslashes(strtr($file_name.$type,$tr)));
  			fclose($fp);
      }
		}
		for ($i=0; $i < $_DOC['ImgsCount']; $i++) { 
		if (!empty(${'kill_doc'.$i})) {
			if(!empty($_DOC['conf']['tree_from_db'])){
				$res_upd = SQL::upd((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', "doc$i = ''", "path = '/doctxt/$doc_alias' or path = '/doctxt/$doc_alias/'");
			}
			$fp	= fopen($dir.'/doc'.$i, 'w');
			fclose($fp);
			
		}
		}
		for ($i=0; $i < $_DOC['ImgsCount']; $i++) if (!empty(${'doc'.$i})) {
		  if(!empty($_DOC['conf']['tree_from_db'])){
  			$fp	= fopen($dir.'/doc'.$i, 'w');
  			if (empty($fp)) {
  				if (@DEBUG) $_CORE->log_message('doctxt::save', 'warning', 'Couldn`t open for save data: '.$dir.'/doc'.$i);
  				$_DOC['ERROR']	= Main::get_lang_str('�ouldnt_open', 'doctxt');
  				return false;
  			}
		    $tr	= array( '"' => "'" );
  			$type = strrchr( ${"doc".$i."_name"},'.');
  			eval('$nn = '.$_DOC['cForm']->Arr["doc".$i]["newname_func"].";");
		    $res_upd = SQL::upd((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', "doc$i = '".stripslashes(strtr($nn.$type,$tr))."'", "path = '/doctxt/$doc_alias' or path = '/doctxt/$doc_alias/'");
		    if (@DEBUG) $_CORE->log_message('doctxt::save', 'good', "Saved : 'doc$i' Name:".${"doc".$i."_name"}." NewName:$nn Type:$type)" );
  			fputs($fp, stripslashes(strtr($nn.$type,$tr)));
  			fclose($fp);
		  }else {
  			$fp	= fopen($dir.'/doc'.$i, 'w');
  			if (empty($fp)) {
  				if (@DEBUG) $_CORE->log_message('doctxt::save', 'warning', 'Couldn`t open for save data: '.$dir.'/doc'.$i);
  				$_DOC['ERROR']	= Main::get_lang_str('�ouldnt_open', 'doctxt');
  				return false;
  			}
  			$tr	= array( '"' => "'" );
  			$type = strrchr( ${"doc".$i."_name"},'.');
  			eval('$nn = '.$_DOC['cForm']->Arr["doc".$i]["newname_func"].";");
  			if (@DEBUG) $_CORE->log_message('doctxt::save', 'good', "Saved : 'doc$i' Name:".${"doc".$i."_name"}." NewName:$nn Type:$type)" );
  			fputs($fp, stripslashes(strtr($nn.$type,$tr)));
  			fclose($fp);
      }
		}

		// Sinhroniziruem i imja v Dereve !!!
		$path = "/doctxt/".$form['path']; 
		$tmp	= Main::array_multisearch($path, $_DOC['TREE']);
    
    // ������� ��� DB ���������� ��������� �������� � ������ ����� 27.09.2012 Ulyusov
     if($tmp === false){
      $arr = explode("/",$form['path']);
      $add_path =  array_pop($arr);
      if(empty($add_path)){
        $path .= "db/".array_pop($arr)."/";
      }else{
        $path .= "/db/".$add_path."/";
      }
      $tmp	= Main::array_multisearch($path, $_DOC['TREE']);
     }
    ////////////////////////////////////////////////////////////
    
		$link	=& $_DOC['TREE'];
		// Vyshli na uroven'
		if (is_array($tmp)) {
			array_pop($tmp); // ubrali path ostalsja indeks
			for ($i=0;$i<sizeof($tmp)-1;$i++)
				$link	=& $link[$tmp[$i]];
			if (!empty($link[$tmp[$i]])) {
				$link[$tmp[$i]]['name']	= stripslashes($form['title']);
				DOCTXT::save_tree( $_DOC['TREE'] );
			}
		}		
		$_DOC['ERROR']	.= Main::get_lang_str('data_saved', 'doctxt');
		return true;
	}

	/**
	*
	*/
	function del( $doc_alias )
	{
		global $_DOC, $_CORE;
		// esli demo
		if ($_CORE->IS_DEMO) return false;
		
    if( DOCTXT::is_nokill($_DOC['TREE'], "/".$_DOC['MODULE_NAME'].$doc_alias)||DOCTXT::is_readonly($_DOC['TREE'], "/".$_DOC['MODULE_NAME'].$doc_alias) )return false;	
    
		if (preg_match("|^(.+/)db/.*|i",$doc_alias,$matches)){
			$was_doc_alias = $doc_alias;
			$doc_alias = $matches[1];
		}
		
		$del_tree	= true;
		$result	= true;
		if (!DOCTXT::is_doc($doc_alias, $dir)) { // proverjaet fajl
			if (@DEBUG) $_CORE->log_message('doctxt::del', 'notice', 'Document data not found: '.$dir);
			$del_tree = true; // vse ravno udalit' iz dereva, dazhe esli dannyh net 
		}else {
			FILE::deldir($_CORE->ROOT.'/reimg'.$_CORE->DATA_PATH.$_DOC['MODULE_NAME']);	
			if (FILE::rn_dir( $dir, ' ', 'del')){
				if (@DEBUG) $_CORE->log_message('', 'Document data DELETED '.$doc_alias,'doctxt::del');
			}else {
				$del_tree	= false;
				$result	&= false;
				$_DOC['ERROR'] .= 'Document data not DELETED '.$doc_alias;
			}
		}
		
    if(!empty($_DOC['conf']['tree_from_db'])){
		  if (!$_CORE->dll_load('class.DBCQ'))
        die ($_CORE->error_msg());
        if  (!empty($was_doc_alias)) $doc_alias = $was_doc_alias;
      $res_del = SQL::del((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', "path = '/doctxt$doc_alias' or parent='/doctxt$doc_alias' ");
      if (@DEBUG) $_CORE->log_message('', 'Document DELETED '.$doc_alias,'doctxt::del');
      return true;
    } else {
    
		if  (!empty($was_doc_alias)) $doc_alias = $was_doc_alias;

		if ($del_tree) if (DOCTXT::_del_tree_item( $_DOC['TREE'], $doc_alias )) {
			if (@DEBUG) $_CORE->log_message('', 'Document path DELETED '.$doc_alias.".",'doctxt::del');
		}else {
			if (@DEBUG) $_CORE->log_message('doctxt::del', 'notice', "Document path not found in tree: $doc_alias");
//			$_DOC['ERROR']	.= "Document path not found in tree: $doc_alias\n";
		}
		if ($result) $_DOC['ERROR']	.= Main::get_lang_str('doc_del', 'doctxt');
		
		return $result;
    }
	}

	/**
	*
	*/
	function save_tree( &$tree, $lang = 'ru' )
	{
		global $_CORE,$_DOC;
		// esli demo
		if ($_CORE->IS_DEMO) return false;
		$lang	= (in_array($lang, $_CORE->ALL_LANGS)) ? $lang 
				: (in_array($_CORE->LANG, $_CORE->ALL_LANGS)) ? $_CORE->LANG 
				: 'ru';
//		$file = $_CORE->SiteModDir.'/'.$lang.'.tree.inc.php';
//		$fileTo = $_CORE->SiteModDir.'/tmp/'.$lang.'.tree.inc.php';

//		$file = $_CORE->ROOT.'/data/'.$_DOC['MODULE_NAME'].'/'.$lang.'.tree.inc.php';
		$file	=	DOCTXT::get_data_path($lang.'.tree.inc.php', $f, DOC_LOOKIG_DATA_FILE);
				
//		$fileTo = $_CORE->ROOT.'/data/'.$_DOC['MODULE_NAME'].'/tmp/'.$lang.'.tree.inc.php';
		$fileTo = DOCTXT::get_data_path('/tmp/'.$lang.'.tree.inc.php', $f, DOC_LOOKIG_DATA_FILE);
		if (!empty($tree)) {
			copy($file, $fileTo.".".date('d-m-Y-h-i').".tmp"); // DO NOT NEED 
      if(!empty($_DOC['conf']['tree_from_db'])){
        DOCTXT::save_tree_todb($tree,'/',0,true);			
      }else{
        $fp	= fopen($file, 'w');
  			fputs($fp, "<?\n \$_DOC['TREE']	= ".var_export($tree, TRUE)." \n ?>"); // <? 
  			fclose($fp);
      }
		}else {
			$_DOC['ERROR']	.= Main::get_lang_str('tree_is_empty', 'doctxt');
		}
	}
	function save_tree_todb( $tree, $parent="/", $parent_id=0, $clear_table=false, $install=false )
	{
	 global $_CORE;
    if (!$_CORE->dll_load('class.DBCQ'))
      die ($_CORE->error_msg());
		
		// esli demo
		if ($_CORE->IS_DEMO) return false;
    if(!is_array($tree))return false;
/*    if($clear_table){
      $result = SQL::query("TRUNCATE `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree'."`");
      
    }*/
    
    $rootpath = $_SERVER['DOCUMENT_ROOT']."/data/doctxt/data/";   
    
    foreach($tree as $key => $val){
      if($install){
        $was_doc_alias = $val['path'];
        if (preg_match("|^(.+/)db/.*|i",$val['path'],$matches)){
    			$was_doc_alias = $matches[1];
    		}
        $_CORE->log_message($was_doc_alias,'$was_doc_alias=');
        $path = str_replace('/doctxt/','',$was_doc_alias);
        if(is_dir($rootpath.$path)){
            $cur_tree = FILE::read_dir($rootpath.$path);
            if(is_array($cur_tree) && count($cur_tree) ){
              foreach($cur_tree as $v){
                if(!in_array($v,array('file','path', 'name', 'link')) ){
                    if(is_file($rootpath.$path."/".$v)){
                        $pathCur = str_replace('//','/',$rootpath.$path."/".$v);
                      $handle = @fopen($pathCur, "r");
                      if($handle && filesize($pathCur)){ 
                         // echo $pathCur;
                          $src_cont[] = "'".addslashes (fread($handle,filesize($pathCur)))."'";
                          fclose($handle);
                          $src_name[]=$v;
                      }
                    }
                }
              }
                $ts = DOCTXT::get_doc_date($val['path']);
                
              if (count($src_name)){
                $src_name = ",`".implode('`,`',$src_name)."`";
                $src_cont = ",".implode(',',$src_cont);
              }else {
                $src_name =""; $src_cont="";
              }
            }
        }
      }else {
         $src_name =""; $src_cont="";
         $ts = time();
      }
      if($clear_table){
        $res_upd = SQL::upd((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree',"pos='".($key+1)."', parent='$parent', name='{$val['name']}',path='{$val['path']}', hidden='{$val['hidden']}', link='{$val['link']}', parent_id='$parent_id',  readonly='{$val['readonly']}', nokill='{$val['nokill']}', needreg='{$val['needreg']}', nomenu='{$val['nomenu']}', menunolink='{$val['menunolink']}'","path='{$val['path']}'",0);
        echo $res_upd->ErrorQuery;
      } else {
        $res = SQL::ins((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree',"`pos`, `parent`, `name`, `path`, `hidden`, `link`, `parent_id`, `ts`".$src_name,"'".($key+1)."', '$parent', '{$val['name']}', '{$val['path']}', '{$val['hidden']}', '{$val['link']}' , '$parent_id', '{$ts}'".$src_cont);
        echo $res->ErrorQuery;
      }
      unset($src_name, $src_cont);
      $parent_id_lost = $parent_id;
      $parent_lost = $parent;
      if (is_array($val['next']) && count($val['next'])){
        $parent_id = mysql_insert_id();
        if($clear_table) $parent_id = $val['id'];
        self::save_tree_todb( $val['next'], $val['path'], $parent_id, $clear_table, $install );
        $parent_id = $parent_id_lost; $parent = $parent_lost;
      }
	 }
   return true;
}
  function copy_tree_todb($path, $path_to, $add_to_path=""){
	 global $_CORE;
    if (!$_CORE->dll_load('class.DBCQ'))
      die ($_CORE->error_msg());
		
		// esli demo
		if ($_CORE->IS_DEMO) return false;

    $tree = SQL::getrow('*',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', "path = '".$path."'", "",0);
    
    foreach($tree as $key => $val){
      if ($key!='path')$tree[$key] = "'".$val."'"; 
      
      if((int)$key > 0) unset($tree[$key]);       
    }
    $tree['ts'] = "'".time()."'";
    $pos = SQL::getval('count(*)',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree',"parent='".$path_to."'");
    $parent_id =  SQL::getval('id',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree',"path='".$path_to."'");
    $tree['pos'] = "'".($pos+1)."'";
    unset($tree['id'],$tree[0]);
  	$tmp = explode("/",$tree['path']);
		array_pop($tmp);
		$from_last_path = array_pop($tmp);
    $new_path_to = $path_to.$from_last_path.$add_to_path."/";
    $tree['path'] = "'".$new_path_to."'";
    $tree['parent'] = "'".$path_to."'";
    $tree['parent_id'] = "'".$parent_id."'";
    $keys = array_keys($tree);
    $src_name = implode(",",$keys);
    $src_cont = implode(",",$tree);
    SQL::ins((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree',$src_name,$src_cont,1);
    $sub_tree = SQL::getall('path',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', "parent = '".$path."'", "",0);
    if(count($sub_tree)>0){
      foreach($sub_tree as $k=>$v){
        self::copy_tree_todb($v['path'], $new_path_to);
      }
    }
    $_CORE->log_message('$tree = '.print_r($tree,1));
    //$_CORE->log_message('$keys = '.print_r($keys,1));
    /*foreach($tree as $key => $val){
      unset($val['id']);
      $val['ts'] = time();
      $val['path'] = str_replace($val);
      $keys = array_keys($val);
      SQL::ins((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree',$src_name,"'".($key+1)."', '$parent', '{$val['name']}', '{$val['path']}', '{$val['hidden']}', '{$val['link']}' , '$parent_id', '{$val['readonly']}', '{$val['nokill']}', '{$val['needreg']}', '{$ts}'".$src_cont);
    }*/
  }

	function get_tree_fromdb( $parent_id=0 )
	{
	 global $_CORE,$_DOC;
    if (!$_CORE->dll_load('class.DBCQ'))
      die ($_CORE->error_msg());
		
    $fields = 'id,pos,parent,name,path,link,parent_id, parent_id, hidden, readonly, nokill, needreg,nomenu,menunolink, ts';
	
	if (!empty($_DOC['conf']['add_tree_fields']))
		$fields .= ',' . $_DOC['conf']['add_tree_fields'];
	
    if(!empty($_DOC['conf']['dbmodule'])) $fields .= ',dbmodule';
    $tree = SQL::getall($fields,(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', 'parent_id = '.$parent_id, "order by `pos`",0);
    
    if (!is_array($tree)) return false;
    foreach($tree as $key => $val){
      $arr = SQL::getval('count(*)',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', 'parent_id = '.$val['id']);
      if($arr>0){
        $tree[$key]['next'] = self::get_tree_fromdb($val['id']);     
      }
    }
    return $tree;

	}
	
	
	function get_tree_fromdbnew( $parent_id=0 )
	{
	 global $_CORE,$_DOC;
    if (!$_CORE->dll_load('class.DBCQ'))
      die ($_CORE->error_msg());
		
    $fields = 'id,pos,parent,name,path,link,parent_id, parent_id, hidden, readonly, nokill, needreg,nomenu,menunolink, ts';
	
	if (!empty($_DOC['conf']['add_tree_fields']))
		$fields .= ',' . $_DOC['conf']['add_tree_fields'];
	
    if(!empty($_DOC['conf']['dbmodule'])) $fields .= ',dbmodule';
    $arr = SQL::getall($fields,(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', '', "ORDER BY `parent_id`,`pos`",0);
    if (!is_array($arr)) return false;
    $childrens = $tree = array();
    foreach($arr as $key => $val){
		$childrens[$val['parent_id']][$key] = $val;
		if($val['parent_id'] == $parent_id )
		$tree[$key] = $val;
    }
    $tree = self::assembleTree($tree, $childrens);
    return $tree;

	}
	
	
	function assembleTree($arr, $childrens){
		//echo '<pre>'; print_r($arr); echo "</pre>";
		foreach($arr as $key => $val){
			if(!empty($childrens[$val['id']])){
				$arr[$key]['next'] = array_values(self::assembleTree($childrens[$val['id']], $childrens));
			}
		}
		return $arr;
	}

	function save_field($doc_alias, $field, $new_val){
		global $_CORE, $_DOC;

		if ($_CORE->IS_DEMO) return false;
		if(!empty($_DOC['conf']['tree_from_db'])){
      if (!$_CORE->dll_load('class.DBCQ'))
        die ($_CORE->error_msg());
        $tr	= array( '"' => "'", "\r" => "" );
         $new_val = stripslashes(strtr($new_val,$tr));
        if(SQL::upd((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', $field."='".$new_val."', ts='".time()."'","path = '/doctxt/".$doc_alias."'",0)){
          return true;
        }
        return false;
      
    } else {
  		#
  		# vtoroj raz pervyj v check 
//  		echo $doc_alias;
  		if (!DOCTXT::is_doc($doc_alias, $dir)) {
	//			echo $dir;
  			if (!is_dir($dir) && !@mkdir($dir)){
  				if (@DEBUG) $_CORE->log_message('doctxt::save', 'warning', 'Path in dir not found : '.$dir);
  				return false;
  			}else {
  				if (@DEBUG) $_CORE->log_message('doctxt::save', 'good', 'Path present : '.$dir);
  			}
  		}
  		#
  		# a esli uzhe est' s takim nazvaniem? 
  		if (empty($field)) { // dobavljaem novyj, a takaja dira uzhe est', nehorosho
  			if (@DEBUG) $_CORE->log_message('doctxt::save_field', 'warning', 'New record, but path present : '.$dir);
  			$_DOC['ERROR']	.= Main::get_lang_str('no_field', 'doctxt');
  			return false;
  		}
  		$fp	= fopen($dir.'/'.$field, 'w');
  		
  		if (empty($fp)) {
  			if (@DEBUG) $_CORE->log_message('doctxt::save_field', 'warning', 'Couldn`t open for save data: '.$dir.'/'.$field);
  			$_DOC['ERROR']	= Main::get_lang_str('�ouldnt_open', 'doctxt');
  			return false;
  		}
  
  		if (@DEBUG) $_CORE->log_message('doctxt::save_field', 'good', "Saved : $field");
  		$tr	= array( '"' => "'", "\r" => "" );
  		fputs($fp, stripslashes(strtr($new_val,$tr)));
  		fclose($fp);
  
  		return true;
    }

	}

	/**
	*
	*/
	function &subtree( &$tree, $path, $noadd = false )
	{
		global $_DOC;
		
		// parametr vveden pri razrabotke CONNECTS, dlja poiska
		if (!$noadd)
			$path	= '/'.$_DOC['MODULE_NAME'].$path;
		$tmp	= Main::array_multisearch($path, $tree);
		if (!is_array($tmp)) return false; // not found
		else {
			array_pop($tmp);
			$link	=& $tree;
			$bef	= '/';	
			for ($i=0; $i<sizeof($tmp); $i++){
				if ($tmp[$i] === 'next') {
					$bef	= $link['path'];
				}
				unset($link['parent']);
				$link	=& $link[$tmp[$i]];
				$link['parent']	= $bef;
			}
			return $link;
		}
	}

	/**
	*
	*/
	function tree_nodb( $tree )
	{
		global $_DOC;
		if (is_array($tree['next'])){
		  foreach($tree['next'] as $key=>$val){
		    if (is_array($val)){
		      if(strpos($val['path'], '/db/')!==false) {unset($tree['next'][$key]);}
          if(!empty($val['next']) && is_array($val['next'])){ $tree['next'][$key]['next'] = tree_nodb($val['next']);}
		    }
		  }
		} 
    return $tree;
	}

	
	/**
	 * is_present && is_nokill
	 * added by Shesternin (10.05.06)
	 * @param unknown_type $tree
	 * @param unknown_type $doc_alias
	 * @return unknown
	 */
	function is_nokill( &$tree, $doc_alias )
	{	
		return DOCTXT::is_present( $tree, $doc_alias, 'nokill' );
	}
	function is_readonly( &$tree, $doc_alias )
	{
		return DOCTXT::is_present( $tree, $doc_alias, 'readonly' );
	}
	function is_needreg( &$tree, $doc_alias )
	{
		return DOCTXT::is_present( $tree, $doc_alias, 'needreg' );
	}

	function set_nokill( &$tree, $doc_alias )
	{
		DOCTXT::is_present( $tree, $doc_alias, 'nokill', 'set' );
	}
	function set_readonly( &$tree, $doc_alias )
	{
		DOCTXT::is_present( $tree, $doc_alias, 'readonly', 'set' );
	}
	function set_needreg( &$tree, $doc_alias )
	{
		DOCTXT::is_present( $tree, $doc_alias, 'needreg', 'set' );
	}
	// new universal attribute
	function set_attr( &$tree, $doc_alias, $attr = 'nokill')
	{
		DOCTXT::is_present( $tree, $doc_alias, $attr, 'set' );
	}
	function unset_attr( &$tree, $doc_alias, $attr = 'nokill' )
	{
		DOCTXT::is_present( $tree, $doc_alias, $attr, 'unset' );
	}


	function unset_nokill( &$tree, $doc_alias )
	{
		DOCTXT::is_present( $tree, $doc_alias, 'nokill', 'unset' );
	}
	function unset_readonly( &$tree, $doc_alias )
	{
		DOCTXT::is_present( $tree, $doc_alias, 'readonly', 'unset' );
	}
	function unset_needreg( &$tree, $doc_alias )
	{
		DOCTXT::is_present( $tree, $doc_alias, 'needreg', 'unset' );
	}

	function set_order( &$tree, $doc_alias, $order )
	{
		DOCTXT::is_present( $tree, $doc_alias, 'order', 'set', $order );
	}

	function is_present(  &$tree, $doc_alias, $field, $do='is', $value='true' )
	{	
		global $_DOC, $_CORE;
        if(!empty($_DOC['conf']['tree_from_db'])){
          if (!$_CORE->dll_load('class.DBCQ'))
            die ($_CORE->error_msg());
        }
		$tmp	= Main::array_multisearch($doc_alias, $tree);
		if (!is_array($tmp)) return false; // not found
		else {
			array_pop($tmp);
			$link	=& $tree;
			for ($i=0;$i<sizeof($tmp);$i++) $link	=& $link[$tmp[$i]];
			if( $do=='is' )return !empty($link[$field])&&($link[$field]);
			if( $do=='set'){
                $link[$field] = (string)$value; 
                if(!empty($_DOC['conf']['tree_from_db'])){
                    SQL::upd((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', 
                    "`".$field."`='true'","path = '{$link['path']}'",0);
                }else {
                    DOCTXT::save_tree( $tree );
                    $cl_alias = str_replace('/doctxt/', '', str_replace(strstr($doc_alias, '/db/'), '', $doc_alias));
                    DOCTXT::save_field( $cl_alias , $field, 'true');
                }
            }
      if( $do=='unset'){	
        unset($link[$field]);			
        if(!empty($_DOC['conf']['tree_from_db'])){
          SQL::upd((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', $field."=''","path = '{$link['path']}'",0);
        }else {
          DOCTXT::save_tree( $tree );
					$cl_alias = str_replace('/doctxt/', '', str_replace(strstr($doc_alias, '/db/'), '', $doc_alias));
          DOCTXT::save_field( $cl_alias , $field, '');
        }
}
		}
	}
	/**
	 * endof is_present
	 */
	
	/**
	*
	*/
	function _del_tree_item( &$tree, $doc_alias )
	{
		global $_DOC;
		#
		# Sohranim v derevo
		$path	= '/'.$_DOC['MODULE_NAME'].$doc_alias; // $doc_alias with '/'
		$tmp	= Main::array_multisearch($path, $tree);
		if (!is_array($tmp)) return false; // not found
		else {
			array_pop($tmp);
			$link	=& $tree;
			for ($i=0;$i<sizeof($tmp)-1;$i++)
			$link	=& $link[$tmp[$i]];
			unset($link[$tmp[$i]]);			
			unset($link['parent']);			
			DOCTXT::save_tree( $tree );
			return true;
		}
	}

	/**
	*	Pokazat' formu dlja redaktirovanija dereva dokumentov
	*/
	function show_admtree( $tree )
	{
		global $_DOC, $_CORE;


		$ADDISHION = (empty($_DOC['conf']['adm_noadd_first'])) 
			? "<a href='#' OnClick=\"doc_add('/doctxt/');return false;\"><IMG SRC='".DOCTXT::inc_link('a_empty.gif', $f)."' BORDER='0' ALT=".Main::get_lang_str('add', 'doctxt')." TITLE=".Main::get_lang_str('create_doc', 'doctxt')."'align=\"left\">&nbsp;".Main::get_lang_str('add_first_level', 'doctxt')."</a>"
			: "&nbsp;";
			

		// Prefiks tablicy
		if (DOCTXT::inc_tpl('admtree.head.html', $file))
			include "$file";

		$_DOC['COUNTER'] = 0; // dlja spiska dokumentov chto by imet' nomer.
		// Po vsem komponentam
		foreach ($tree as $k => $v) {
			DOCTXT::doctxt_tree($v, $k, 0);
		}

		// Suffiks tablicy
		if (DOCTXT::inc_tpl('admtree.foot.html', $file))
			include "$file";

	}
// added by Shesternin, dlja vyvoda daty poslednego redaktirovanija dokumenta
	function get_doc_date( $doc_alias)
	{
		global $_CORE;
		$time=0;
		list($tmp, $doc_alias) = Main::cmd_parse($doc_alias);
		if (preg_match("|^(.+/)db/.*|i",$doc_alias,$matches)){
		  $was_doc_alias = $doc_alias;
			$doc_alias = $matches[1];
		}
//		$dir	=	$_CORE->SiteModDir.'/data/'.$doc_alias;
		$dir	=	DOCTXT::get_data_path($doc_alias, $dir, DOC_LOOKIG_DATA_DIR);
		if( !empty( $dir )){
				$d = @opendir( $dir );
				while( ($file=readdir($d))!==false )
					{	
						if(( $file=='.' )||( $file==".." )||( $file=="CVS" )||(!is_file($dir."/".$file))) continue;
						$time = ($time<filemtime( $dir."/".$file ))?filemtime( $dir."/".$file ):$time;
					}
				@closedir($dir);
		}

		return $time;
	}

	/**
	 * sortind added by Shesternin (12.05.06)
	 *
	 * @param arrayarray $list
	 * @param string $sort
	 * @param string $ordering ("up/down")
	 * @return array
	 */
	function order_list( &$list, $sort, $ordering )
	{	
	if (is_array($list))
		foreach( $list as $k=>$v )$order[]=$k;
		if( empty($list[$order['0']][$sort])||( $ordering!="up"&&$ordering!="down" ))	return $order;
		
		for( $i=0; $i<sizeof($order)-1; $i++ )
		{	$ii=$i;
			for( $j=$i+1; $j<sizeof($order); $j++ )
			if(($list[$order[$ii]][$sort] <= $list[$order[$j]][$sort])!=($ordering=="up"))  $ii=$j;

			$tmp = $order[$i];
			$order[$i] = $order[$ii];
			$order[$ii] = $tmp;
		}
		return $order;
	}
	/**
	*	Pokazat' formu dlja redaktirovanija dereva dokumentov
	*/
	function show_admlist( $tree, $node = "", $sort, $ordering )
	{
		global $_DOC, $_CORE;


//		$ADDISHION = (empty($_DOC['conf']['adm_noadd_first'])) 
//			? "<a href='#' OnClick=\"doc_add('/doctxt/');return false;\"><IMG SRC='".DOCTXT::inc_link('a_empty.gif', $f)."' BORDER='0' ALT='dobavit'' TITLE='Sozdat' razdel'align=\"left\">&nbsp;Dobavit' pervyj uroven'</a>"
//			: "";

		$ADDISHION .=  (empty($_DOC['conf']['adm_noadd_first'])||($node!=''&&$node!='/')) 
			? "<a href='javascript:;' OnClick=\"ShowPopUp('/clear/doctxt/new_add?$node', 500, 450, 'yes');return false;\"><IMG SRC='".DOCTXT::inc_link('a_empty.gif', $f)."' BORDER='0' ALT=".Main::get_lang_str('add', 'doctxt')." TITLE=".Main::get_lang_str('create_doc', 'doctxt'). "align=\"left\">&nbsp;".Main::get_lang_str('add', 'doctxt')."</a>"
			: '';
			
		// Najti tekuwee mesto
		
		if (!empty($node)&&($node!='/'))
			$tmp = Main::array_multisearch($node, $tree);
			
		if (!empty($tmp)){
			$tmp = Main::array_multisearch($node, $tree);
			$deep = ceil(sizeof($tmp) / 2 );
			if (is_array($tmp)) {
				array_pop($tmp);
				$link	=& $tree;
				for ($i=0;$i<sizeof($tmp);$i++)
					$link	=& $link[$tmp[$i]];
				$link = $link['next'];
			}
		}else{
			$deep = 0;
			$link = $tree;
		}

		// Prefiks tablicy
		if (DOCTXT::inc_tpl('admlist.head.html', $file))
			include "$file";

	if (is_array($link))
		foreach ($link as $key => $item) {
			$item_tmp = &$link[$key];
      if(!empty($_DOC['conf']['tree_from_db'])){
        $item_tmp['date'] = $item['ts'];
      }else{
			 $item_tmp['date'] = DOCTXT::get_doc_date( $item['path'] );
      }
			$item_tmp['hidden'] = empty($item['hidden'])?false:$item['hidden'];
		}
		
		$order = DOCTXT::order_list( $link, $sort, $ordering );
		
		$_DOC['COUNTER'] = 0; // dlja spiska dokumentov chto by imet' nomer.
		for( $i=0; $i<sizeof($order); $i++ )
		{	$item = $link[$order[$i]];
			$item['root'] = $node;
			//$item['date'] = DOCTXT::get_doc_date( $item['path'] );
			DOCTXT::draw_item($item, $deep, $_DOC['COUNTER'], 'list');
			$_DOC['COUNTER']++;
		}

		// Suffiks tablicy
		if (DOCTXT::inc_tpl('admlist.foot.html', $file))
			include "$file";

	}	

	function draw_item( $item, $deep, $key = 0, $type = 'tree')
	{
		global $_CORE, $_DOC;

		$ADDONS = array();

		$node_add = ($type=='list')?"&node=".$_REQUEST['node']:'';

		# dobavit' poddokument
		if ($deep < 3){
			if ($type == 'tree')
				$ADDONS[] = "<a HREF='javascript:;' OnClick=\"doc_add('".$item['path']."');return false;\"><IMG SRC='".DOCTXT::inc_link('a_uban.gif',$f)."' BORDER='0' ALT=".Main::get_lang_str('add', 'doctxt')." TITLE=".Main::get_lang_str('add_next_level', 'doctxt')." align=\"left\">&nbsp;".Main::get_lang_str('add_doc_in_folder', 'doctxt')."</a>";
			elseif ($deep >= 0){
				$ADDONS[] = "<a HREF='javascript:;' OnClick=\"doc_add('".$item['path']."');return false;\"><IMG SRC='".DOCTXT::inc_link('a_uban.gif',$f)."' BORDER='0' ALT=".Main::get_lang_str('add', 'doctxt')." TITLE=".Main::get_lang_str('add_doc', 'doctxt')." align=\"left\">&nbsp;".Main::get_lang_str('add_doc_in_folder', 'doctxt')."</a>";
			}else{
				$ADDONS[] = '';
			}
		}else{
			$ADDONS[] = '';
		}

		# redaktirovat'
		if (empty($item['readonly']))
			$ADDONS[] = "<A HREF='javascript:;'  OnClick=\"if(shift_pressed_on==-1){shift_pressed_on=CurrentMarked;isp=true;}EnterPressed();if(isp)shift_pressed_on=-1;return false;\"><IMG SRC='".DOCTXT::inc_link('a_edit.gif', $f)."' BORDER='0' ALT=".Main::get_lang_str('redact', 'doctxt')." align=\"left\">&nbsp;".Main::get_lang_str('redact', 'doctxt')."</a>";
//			$ADDONS[] = "<A HREF=''  OnClick=\"ShowPopUp('/clear/".$item['path']."?edit', 760, 500, 'yes');return false;\"><IMG SRC='".DOCTXT::inc_link('a_edit.gif', $f)."' BORDER='0' ALT='Redaktirovat'' align=\"left\">&nbsp;Redaktirovat'</a>";
		else
			$ADDONS[] = '<div>&nbsp;</div>';
	
		# delete
		if (($deep > 0 || (empty($_DOC['conf']['adm_nokill_first']) && $deep > -1)) && empty($item['readonly'])&&empty( $item['nokill'] ) ):
			$ADDONS[] =  "<A HREF='javascript:;' OnClick=\"DeletePressed();\"><IMG SRC='".DOCTXT::inc_link('a_delete.gif',$f)."' BORDER='0' ALT='".Main::get_lang_str('del', 'doctxt')."' TITLE=".Main::get_lang_str('del', 'doctxt')." align=\"left\">&nbsp;".Main::get_lang_str('del', 'doctxt')."</A>";		
//			$ADDONS[] =  "<A HREF='' OnClick=\"doc_del('".addslashes(strtr(htmlspecialchars($item['name']),array("'"=>' ', '"' => '')))."','".$item['path']."');return false;\"><IMG SRC='".DOCTXT::inc_link('a_delete.gif',$f)."' BORDER='0' ALT='udalit'' TITLE='Udalit' dokument' align=\"left\">&nbsp;Udalit'</A>";		
		else:
			$ADDONS[] = '<div>&nbsp;</div>';
		endif;	

		# pereimenovat'
		$ADDONS[] = "<A HREF='javascript:;' OnClick=\"doc_rename('".strtr($item['name'],array("'"=>"\'", '"'=>""))." ', '".$item['path']."');return false;\"><IMG SRC='".DOCTXT::inc_link('a_ren.gif', $f)."' BORDER='0' ALT=".Main::get_lang_str('rename', 'doctxt')." align=\"left\">&nbsp;".Main::get_lang_str('rename', 'doctxt')."</a>";

		# podnjat' 
		if ($key > 0 && empty($_DOC['conf']['adm_nomove'])):
			$ADDONS[] =  "<A HREF='/".$_DOC['MODULE_NAME']."/?$type&add=".$item['path']."&up".$node_add."'><IMG SRC='".DOCTXT::inc_link('a_up.gif',$f)."' BORDER='0' ALT=".Main::get_lang_str('up', 'doctxt')." align=\"left\">&nbsp;".Main::get_lang_str('up', 'doctxt')."</A>";
		else:
			$ADDONS[] = '';
		endif;			

		# sprjatat'
		if (empty($item['hidden'])):
			$ADDONS[] =  "<A HREF='/".$_DOC['MODULE_NAME']."/?$type&add=".$item['path']."&hide".$node_add."'><IMG SRC='".DOCTXT::inc_link('a_vis.gif',$f)."' BORDER='0' ALT=".Main::get_lang_str('hide', 'doctxt')." align=\"left\">&nbsp;".Main::get_lang_str('hide', 'doctxt')."</A>";
		else:
			$ADDONS[] =  "<A HREF='/".$_DOC['MODULE_NAME']."/?$type&add=".$item['path']."&show".$node_add."'><IMG SRC='".DOCTXT::inc_link('a_unvis.gif',$f)."' BORDER='0' ALT=".Main::get_lang_str('show', 'doctxt')." align=\"left\">&nbsp;".Main::get_lang_str('show', 'doctxt')."</A>";
		endif;


		// Vyvod stroki tablicy
		if (DOCTXT::inc_tpl( "adm".$type.".item.html", $file))
			include "$file";
	}

	function search ( $tree )
	{
		if (!is_array($tree)) return false;
		_doctxt_tree_search($tree, 0);
	}

	function _scan ( $d, $w )
	{
		global $_DOC;
		$lw		= DOCTXT::ru_strtolower($w);

		
		$cont_st= @strip_tags($d['cont']);
		$cont	= DOCTXT::ru_strtolower($cont_st);

		$delta	= $_DOC['CntLen'];

		// scan content
		$is		= strpos($cont, $lw);
		if ($is !== false) {
			$pref = ($is > $delta) ? '... ' : '';
			$postf = ($is + $delta + strlen($lw) < strlen($cont)) ? ' ...' : '';

			$from	= ($is - $delta < 0) ? 0 : strpos($cont, ' ', $is - $delta);
			$to		= strpos($cont, ' ', min($is + 2*$delta + strlen($lw), strlen($cont) - $is)) - $is;

			$tmp['text']	= strip_tags($tmp['text']);
			$tmp['text']	= preg_replace("/($w)/i",
			"<span class=mark>$1</span>",
			$pref.substr($cont_st, $from, ($to > 0)?$to:strlen($cont)  ).$postf);
		} else {

			$title	= DOCTXT::ru_strtolower($d['title']);
			$is		= strpos($title, $lw);
			if ($is !== false){
				$tmp['text']	= (strlen($d['cont']) > 2*$delta) 
					? substr( $d['cont'], 0, strpos($d['cont'], ' ', 2*$delta ))." ..."
					: $d['cont'];
				$tmp['text']	= strip_tags($tmp['text']);
			}
		}

		if (isset($tmp['text'])) {
			$tmp['title']	= preg_replace("/($w)/i", "<span class=mark>$1</span>", $d['title']);
			$tmp['url']		= "/".$_DOC['MODULE_NAME']."/".$d['path'];
			return $tmp;
		}else
			return false;
	}
	
	/**
	*
	*/
	function ru_strtolower($str)
	{
		return strtolower(strtr($str,Main::get_lang_str('char_big', 'doctxt'),Main::get_lang_str('char_small', 'doctxt')));
	}

	/**
	*
	*/
	function get_file_name( $str = 'def_name', $base_name = true)
	{
		global $_DOCTXT;
		return ($base_name) ? basename($_SERVER['REQUEST_URI']) : $str;
	}

	/**
	*
	*/
	function save_history( $date, $alias )
	{
		global $_DOC;
		if (!is_file($_DOC['HISTORY_FILE'])) touch($_DOC['HISTORY_FILE']);
		$tmp	= file($_DOC['HISTORY_FILE']);
		$ds = $as = array();
		foreach ($tmp as $str) {
			list($ds[],$as[])	= explode("\t",trim($str));
		}
		if (!in_array($alias, $as)) {
			array_unshift($as, $alias);
			array_unshift($ds, $date);
			$fp	= fopen($_DOC['HISTORY_FILE'],'w');
			for ($i = 0; $i < min(sizeof($as),$_DOC['HISTORY_LEN']); $i++) {
				fputs($fp, $ds[$i]."\t".$as[$i]."\n");
			}
			fclose($fp);
		}
	}

	/**
	*
	*/
	function last( $num=0, $field = '')
	{
		global $_DOC;
		if (!is_file($_DOC['HISTORY_FILE'])) {
			$_DOC['ERROR']	= 'No History Data';
			return false;
		}
		// read 
		$aliases_arr = file($_DOC['HISTORY_FILE']);
		if (empty($aliases_arr[$num])) {
			$_DOC['ERROR']	= Main::get_lang_str('no_history_row', 'doctxt');
			return false;
		}
		list($data,$alias)	= explode("\t",trim($aliases_arr[$num]));
		if ($field == 'data') {
			echo $data;
		}else{
			DOCTXT::get_cont( $alias, $field );
		}
	}

	/**
	*
	*/
	function inc_tpl($tpl, &$file)
	{
		global $_CORE, $_DOC;
		$files	=  array( 
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_DOC['MODULE_NAME'].'/'.$tpl,
			$_CORE->CORE_PATHTPLS.'/'.$_DOC['MODULE_NAME'].'/_'.$tpl,
		);
 		$tmp = FILE::chk_files($files, $file); 
		return $tmp;
	}

	/**
	*
	*/
	function inc_link($tpl, &$file)
	{
		global $_CORE, $_DOC;
		$files	=  array( 
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_DOC['MODULE_NAME'].'/'.$tpl,
			$_CORE->CORE_PATHTPLS.'/'.$_DOC['MODULE_NAME'].'/_'.$tpl,
		);
		$tmp = $file = str_replace($_CORE->PATHTPLS, $_CORE->TEMPLATES, FILE::chk_files($files, $file));
		return $tmp;
	}
	
	/**
	 * vosstanavlivaet pravil'nye puti v massive-dereve, zamewaja nepravil'nye prefiksy, na tekuwie
	 *
	 * @param Array $subtree - nachinaja s kornja kotorogo 
	 * @param string $wrong_pref
	 */
	function path_restore(&$subtree, $wrong_pref, $good_pref=''){
		global $_CORE;
		$good_pref = ($good_pref=='')?$subtree['path']:$good_pref;
		
		//$subtree['path'] = preg_replace("|/db/.+|","",str_replace($wrong_pref,$good_pref,$subtree['path']));
    $subtree['path'] = str_replace($wrong_pref,$good_pref,$subtree['path']);
//		echo "$wrong_pref,$good_pref,$subtree[path]";
 
		// teper' esli est' podddoki
		if (!empty($subtree['next']) && is_array($subtree['next']))
			foreach ($subtree['next'] as $k => $point){
					DOCTXT::path_restore($subtree['next'][$k], $wrong_pref,$good_pref);
	//			$subtree['next'][$k]['path'] = str_replace($wrong_pref,$good_pref,$point['path']);
			
		
		// teper' v fajlah
		list($doc,$alias) = $_CORE->cmd_parse($subtree['next'][$k]['path']); 
		DOCTXT::save_field($alias,'path', $subtree['next'][$k]['path']);		
    }	
	}
	/**
	*
	*/
	function doctxt_tree(&$item, $key, $deep)
	{
		DOCTXT::draw_item($item, $deep, $key);
		if (!empty($item['next']) && is_array($item['next'])) {
			foreach ($item['next'] as $k => $v) {
				DOCTXT::doctxt_tree($v, $k, $deep+1);
			}
		}
	}
	
	/**
	 * Razyminovanie aliasa, added by Shesternin 30.05.06
	 * Dobavleno, dlja vyvoda imen v podmenju "Soderzhimoe bufera"
	 * 
	 *
	 * @param string $alias
	 * @return string
	 */
	function get_name_by_alias( $alias )
	{		global $_KAT;
			if( strstr($alias,'/'.$_KAT['MODULE_NAME'].'/')&&( $alias{strlen($alias)-1}!='/' )){
				preg_match("|.*/(".$_KAT['MODULE_NAME']."/.+)|",$alias,$m);			
				$cmd = dirname($m[1]).'/field/'.basename($alias).'/name';
				$res = cmd($cmd,false,true);
			}else
			if (strstr($alias,'/doctxt/')){
			if (preg_match("|(.*/)(".$_KAT['MODULE_NAME']."/.+)|",$alias,$m))
				$alias = $m[1];
				$res = cmd(str_replace("doctxt/", "doctxt/field/", $alias)."title");
			}
			if( empty($res)||( $res =='' )) $res = $alias;
			return $res;
	}

	/**
	 * Lookig for data LINK, need after idea to keep all data separete
	 *
	 * @param string $looking
	 * @param string $found
	 * @param const $look4dir
	 * @return string LINK PATH to the source or empty
	 */
	function get_data_link($looking, &$found, $look4dir = DOC_LOOKIG_DATA_DIR){
		global $_CORE, $_DOC;
		$files	=  array( 
			$_CORE->SiteModDir.'/'.$looking, // Old style
			$_CORE->SiteModDir.$_CORE->DATA_PATH.'/'.$looking, // Old style 
			$_CORE->ROOT.$_CORE->DATA_PATH.$_DOC['MODULE_NAME'].'/'.$looking,
			$_CORE->ROOT.$_CORE->DATA_PATH.$_DOC['MODULE_NAME'].$_CORE->DATA_PATH.$looking,
			$_CORE->SiteModDir.'/_'.$looking,
		);
//		$tmp = $found = str_replace('//','/',str_replace($_CORE->ROOT, '/', ($look4dir == KAT_LOOKIG_DATA_DIR) ? FILE::chk_dirs($files, $looking) : FILE::chk_files($files, $looking)));
		$tmp = $found = str_replace('//','/',str_replace($_CORE->ROOT, '/', ($look4dir == DOC_LOOKIG_DATA_DIR) ? FILE::chk_dirs($files, $looking) : FILE::chk_files($files, $looking)));
		return $tmp;		
	}
	
	/**
	 * Lookig for data PATH, need after idea to keep all data separete
	 *
	 * @param string $looking
	 * @param string $found
	 * @param const $look4dir
	 * @return string FULL PATH to the source of empty
	 */
	function get_data_path($looking, &$found, $look4dir = DOC_LOOKIG_DATA_DIR){
		global $_CORE, $_DOC;
		$files	=  array( 
			$_CORE->SiteModDir.'/'.$looking, // Old style
			$_CORE->SiteModDir.$_CORE->DATA_PATH.'/'.$looking, // Old style
			$_CORE->ROOT.$_CORE->DATA_PATH.$_DOC['MODULE_NAME'].'/'.$looking,
			$_CORE->ROOT.$_CORE->DATA_PATH.$_DOC['MODULE_NAME'].$_CORE->DATA_PATH.$looking,
			$_CORE->SiteModDir.'/_'.$looking,
		);
		$tmp = $found = ($look4dir == DOC_LOOKIG_DATA_DIR) ? FILE::chk_dirs($files, $looking) : FILE::chk_files($files, $looking);
		return $tmp;		
	}
	
	/**
	 * Setting data LINK, need after idea to keep all data separete
	 *
	 * @param string $looking
	 * @param string $found
	 * @param const $look4dir
	 * @return string LINK PATH to the source or empty
	 */
	function set_data_link($looking, &$found, $set_style = DOC_LOOKIG_DATA_SET_NEW){
		global $_CORE, $_DOC, $_KAT;
		if (preg_match("|(.*/)".$_KAT['MODULE_NAME']."/(.+)|",$looking,$m) && $_DOC['conf']['tree_from_db'] == true )
			$looking = $m[1];
		return ($set_style == DOC_LOOKIG_DATA_SET_OLD) 
				? $_CORE->DIRMODS.$_DOC['MODULE_NAME'].'/data/'.$looking
				: $_CORE->DATA_PATH.$_DOC['MODULE_NAME'].$_CORE->DATA_PATH.$looking;
	}
	
	/**
	 * Setting data PATH, need after idea to keep all data separete
	 *
	 * @param string $looking
	 * @param string $found
	 * @param const $look4dir
	 * @return string FULL PATH to the source of empty
	 */
	function set_data_path($looking, &$found, $set_style = DOC_LOOKIG_DATA_SET_NEW){
		global $_CORE, $_DOC, $_KAT;
		if (preg_match("|(.*/)".$_KAT['MODULE_NAME']."/(.+)|",$looking,$m) && $_DOC['conf']['tree_from_db'] == true )
			$looking = $m[1];
		return ($set_style == DOC_LOOKIG_DATA_SET_OLD) 
				? $_CORE->SiteModDir.'/data/'.$looking
				: $_CORE->ROOT.$_CORE->DATA_PATH.$_DOC['MODULE_NAME'].$_CORE->DATA_PATH.$looking;
	}

/**
*
*/
function get_restore_tree($arr="", $path="" ){
  if(!is_array($arr)) $arr = array();
    $rootpath = $_SERVER['DOCUMENT_ROOT']."/data/doctxt/data/";
    $tree = FILE::read_dir($rootpath.$path);

		// Need SQL for define db
		$tlist = SQL::tlist();

    if(is_array($tree) && count($tree) ){
      $i = 0;
			// ���������� �� ����������
      foreach($tree as $val){
         $path_tree = $path.$val."/";
				// ���� �������������
        if(is_dir($rootpath.$path.$val."/") && $val!='file' &&  $val!='.svn'){
//					
//					// ����� ���������
//					if(is_file($rootpath.$path.$val."/title")){
//						$title = implode('', file($rootpath.$path.$val."/title"));
//          }
//					// ������ ��, ���� �� ����������, �� ������
//          if(is_file($rootpath.$path.$val."/hidden")){
//						$hidden = implode('', file($rootpath.$path.$val."/hidden"));
//						if ($hidden == NULL) $hidden = '1';
//          }
					// ����������� ���� 
					$path_tree = @implode('', file($rootpath.$path.$val."/path"));
					if (empty($path_tree)) $path_tree = $path.$val.'/';
						
					// correct path
					if (!strstr($path_tree, '/db/') && in_array(DB_TABLE_PREFIX.$val, $tlist)) {
						$path_tree .= 'db/'.$val.'/';
					}
					$arr[$i]['path'] = '/doctxt/'.$path_tree;//.$val."/";
					$arr[$i]['name'] = 	@implode('', file($rootpath.$path.$val."/title"));

					// other fields for admin
					$arr1 = array( 'hidden', 'needreg', 'nokill', 'nomenu', 'readonly', 'menunolink' );
					foreach ($arr1 as $param) {
	          $arr[$i][$param] = @implode('', file($rootpath.$path.$val."/".$param));

						// �������� ���� �� ������, ���� �� ���������� �� '' ������ NULL, �� ���� ��� hidden �� ������ ���, ����� ����� �������
						if ($arr[$i][$param] === NULL) $arr[$i][$param] = ($param == 'hidden') ? '' : '1';
					}
					
          $arr[$i]['next']=self::get_restore_tree($arr[$i]['next'],$path.$val."/");
          if(count($arr[$i]['next'])==0) $arr[$i]['next']="";
          $i++;
        }
      }
    }
    return $arr;
}
		/**
	*
	*/

}

/**
*
*/
function _doctxt_tree_walk(&$item, $key, $deep)
{
	DOCTXT::draw_item($item, $deep, $key);
	if (!empty($item['next']) && is_array($item['next'])) {
		array_walk($item['next'], '_doctxt_tree_walk', $deep+1);
	}
}

/**
*
*/
function _doctxt_tree_search( $item, $key )
{
	global $_DOC;
	$dir	= '';
	if (!empty($item['hidden'])) return;
	list($tmp, $it_path) = Main::cmd_parse($item['path']);
	# esli dokument est', to proskaniruem ego 
	if(DOCTXT::is_doc($it_path, $dir)) {
		$d	= DOCTXT::load($it_path);
		$res_arr	= DOCTXT::_scan($d, $_DOC['SEARCH_RES'][0]);
	}
	if (!empty($res_arr)) {
		$_DOC['SEARCH_RES'][]	= $res_arr;
	}

	# esli est' podderevo, to dal'she
	if (!empty($item['next']) && is_array($item['next'])) {
		foreach ($item['next'] as $key => $val ) {
			_doctxt_tree_search($val, $key);
		}
	}
	
}


?>
