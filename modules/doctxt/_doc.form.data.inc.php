<?
$FORM_DATA= array (
  'title' => 
  array (
    'field_name' => 'title',
    'name' => 'form[title]',
    'title' => Main::get_lang_str('title', 'doctxt'),
    'maxlen' => '255',
    'must' => '1',
    'type' => 'textbox',
    'size' => '80',
	'readonly' => 0,
  ),
  'anons' => 
  array (
    'field_name' => 'anons',
    'name' => 'form[anons]',
    'title' => Main::get_lang_str('anons', 'doctxt'),
    'maxlen' => '1024',
    'must' => '0',
    'type' => 'textarea',
    'cols' => '80',
    'rows' => '5',
  ),
  'cont' => 
  array (
    'field_name' => 'cont',
    'name' => 'form[cont]',
    'title' => Main::get_lang_str('cont', 'doctxt'),
    'maxlen' => '5124',
    'must' => '1',
    'type' => 'textarea',
    'cols' => '80',
    'class' => '',
    'rows' => '20',
	'wysiwyg'	=> 'htmlarea',
	'wysiwyg_path'	=> '/modules/htmlarea/',
	'wysiwyg_body'	=> 'font-family: Tahoma, sans-serif; font-size: 11px; color: #003466;',
  ),
	'doc' => array(
			'field_name' => 'doc', // dolzhno sovpadat' s 'name'!!!
			'name'	=> 'doc',
			'title' => Main::get_lang_str('file', 'doctxt'),
			'type'	=> 'photo',
			'sub_type'	=> 'photo',
			'newname_func'	=> 'DOCTXT::get_file_name()',
			'path'	=> '/modules/'.$_DOC['MODULE_NAME'].'/data/'.$form['path'].'file',
			'abspath'	=> $_CORE->PATHMODS.$_DOC['MODULE_NAME'].'/data/'.$form['path'].'file/',
	),
		'hidden' =>  // dlja standarta
  array (
    'field_name' => 'hidden',
    'name' => 'form[hidden]',
    'title' => Main::get_lang_str('ne_publ', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
		'default' => 0,
  ), 
);
?>