<?
	$_DOC['TREE'][]	 = array (
			'name'	 =>'Investment potential', 
			'path'	 =>'/doctxt/inve_pote/en/', 
			'next'	 => array (
				 1 	 => array (
				'name'	 =>'Favourable geographical position and natural resources', 
				'path'	 =>'/doctxt/inve_pote/vygo_goeg_polo/en/'
				), 
				 2 	 => array (
				'name'	 =>'Developed transportation and logistics infrastructure and communications', 
				'path'	 =>'/doctxt/inve_pote/razv_tran_infr/en/'				), 
				 3 	 => array (
				'name'	 =>'Institucional and legislative potential', 
				'path'	 =>'/doctxt/inve_pote/inst_i_zako/en/'
				), 
				 4 	 => array (
				'name'	 =>'Labour and consumer potential', 
				'path'	 =>'/doctxt/inve_pote/trud_i_potr/en/'
				), 
				 5 	 => array (
				'name'	 =>'Moderate political and economic risks', 
				'path'	 =>'/doctxt/inve_pote/umer_polit_i/en/'
				), 	
			 	6 	 => array (				
			 	'name'	 =>'Innovative potential, science and education', 				
			 	'path'	 =>'/doctxt/inve_pote/inno_pote_nauk/en/'
				), 				
				7 	 => array (				
				'name'	 =>'Industrial and financial potential', 				
				'path'	 =>'/doctxt/inve_pote/prom_i_fina/en/'				
				), 
				8 	 => array (	
				'name'	 =>'Investments - success stories', 
				'path'	 =>'/doctxt/inve_pote/inve_-_isto/en/'				
				), 				
			), 			
		);
	 $_DOC['TREE'][]	 = array (
			'name'	 =>'Investment projects', 
			'path'	 =>'/doctxt/inve_proe/en/', 
			'next'	 => array (
				 1 	 => array (
				'name'	 =>'Search of projects', 
				'path'	 =>'/doctxt/inve_proe/pois_proe/en/'
				), 
				), 
			);
		 $_DOC['TREE'][]	 = array (
			'name'	 =>'Investment statistics', 
			'path'	 =>'/doctxt/inve_stat/en/', 
			'next'	 => array (
				 1 	 => array (
				'name'	 =>'Foreign investments', 
				'path'	 =>'/doctxt/inve_stat/inos_inve/en/'
				), 
				 2 	 => array (
				'name'	 =>'Investments into a fixed capital', 
				'path'	 =>'/doctxt/inve_stat/inve_v_osno_kapi/en/'
				), 
				), 
			);
		 $_DOC['TREE'][]	 = array (
			'name'	 =>'Page of investor', 
			'path'	 =>'/doctxt/stra_inve/en/', 
			'next'	 => array (
				 1 	 => array (
				'name'	 =>'Project implementation stages', 
				'path'	 =>'/doctxt/stra_inve/etap_real_inve_proe/en/'
				), 
				 2 	 => array (
				'name'	 =>'Governmental support procedure', 
				'path'	 =>'/doctxt/stra_inve/proc_polu_gosu_podd/en/'
				), 	 
				 3 	 => array (
				'name'	 =>'Draft investment agreement', 
				'path'	 =>'/doctxt/stra_inve/make_inve_sogl/en/'
				), 
				 4 	 => array (
				'name'	 =>'Draft business plan', 
				'path'	 =>'/doctxt/stra_inve/make_bizn/en/'
				), 	 
				 5 	 => array (
				'name'	 =>'Local tax system', 
				'path'	 =>'/doctxt/stra_inve/mest_nalo_sist/en/'
				), 	 
				),
			);

		 $_DOC['TREE'][]	 = array (
			'name'	 =>'Legislation', 
			'path'	 =>'/doctxt/zakonodat/en/', 
			'next'	 => array (
				 1 	 => array (
				'name'	 =>'complete set of documents', 
				'path'	 =>'/doctxt/zakonodat/ves_kompl_docu/en/'
				), 
				 2 	 => array (
				'name'	 =>'Federal law dated February, 25, 1999 N 39-�� "About investment activity in Russian Federation which is carried out in form of capital investments"', 
				'path'	 =>'/doctxt/zakonodat/fede_zako_ot_25_fev/en/'
				), 
				 3 	 => array (
				'name'	 =>'Federal law dated July, 9, 1999. N 160-�� "About foreign investments into Russian Federation"', 				
				'path'	 =>'/doctxt/zakonodat/fede_zako_ot_9_iyul/en/'				
				), 				 
				4 	 => array (				
				'name'	 =>'Law dated 22.06.2000 � 116-� "About state support of investment activity in territory of Nizhniy Novgorodarea"', 				
				'path'	 =>'/doctxt/zakonodat/zakon_ot_22_n_116/en/'				
				), 				 
				5 	 => array (				
				'name'	 =>'Decision from 10.06.2002 � 122 "About order of conclusion, registration, conducting account and control over a course of realization of investment agreements"', 				
				'path'	 => '/doctxt/zakonodat/post_ot_10_n_122/en/'				
				), 				
			), 			
			);
		 $_DOC['TREE'][]	 = array (
			'name'	 =>'Helpful information', 
			'path'	 =>'/doctxt/pole_info/en/', 
			'next'	 => array (
				 1 	 => array (
				'name'	 =>'Investment and consulting agencies', 
				'path'	 =>'/doctxt/pole_info/inve_kons_agen/en/'
				), 
				 2 	 => array (
				'name'	 =>'Banks and mortgage companies', 
				'path'	 =>'/doctxt/pole_info/bank_i_ipot_komp/en/'
				), 
				 3 	 => array (
				'name'	 =>'Insurance companies', 
				'path'	 =>'/doctxt/pole_info/strah_komp/en/'
				), 
				 4 	 => array (
				'name'	 =>'Law firms ', 
				'path'	 =>'/doctxt/pole_info/urid_firm/en/'
				), 
				 5 	 => array (
				'name'	 =>'International organizations and centers', 
				'path'	 =>'/doctxt/pole_info/mejd_orga_i_cent/en/'
				), 
				 6 	 => array (
				'name'	 =>'Chambers and stock exchanges', 
				'path'	 =>'/doctxt/pole_info/pala_i_birj/en/'				
				), 				
			), 			
			);
			
		 $_DOC['TREE'][]	 = array (
			'name'	 =>'Innovative policy', 
			'path'	 =>'/doctxt/inno_poli/en/', 
			'next'	 => array (
				 1 	 => array (
				'name'	 =>'Innovative legislation', 
				'path'	 =>'/doctxt/inno_poli/inno_zako/en/'
				), 
				 2 	 => array (
				'name'	 =>'Innovative activity', 
				'path'	 =>'/doctxt/inno_poli/inno_deya/en/'
				), 
				 3 	 => array (
				'name'	 =>'Contacts', 
				'path'	 =>'/doctxt/inno_poli/kont/en/'
				), 
				), 
			);
		 $_DOC['TREE'][]	 = array (
			'name'	 =>'Green and brown fields', 
			'path'	 =>'/doctxt/svob_plos/en/', 
			'next'	 => array (
				 1 	 => array (
				'name'	 =>'Brown fields', 
				'path'	 =>'/doctxt/svob_plos/proizvod/en/'
				), 
				 2 	 => array (
				'name'	 =>'Green fields', 
				'path'	 =>'/doctxt/svob_plos/zelenye/en/'
				), 
				), 
			);
		 $_DOC['TREE'][]	 = array (
			'name'	 =>'Feedback and contacts', 
			'path'	 =>'/doctxt/obra_svya_i_kont/en/', 
			);
?>