<?
 $_DOC['TREE']	= array (
  0 => 
  array (
    'name' => '�������������� ���������',
    'path' => '/doctxt/inve_pote/',
    'next' => 
    array (
      1 => 
      array (
        'name' => '�������� �������������� ��������� � ��������� �������',
        'path' => '/doctxt/inve_pote/vygo_goeg_polo/',
      ),
      2 => 
      array (
        'name' => '�������� �����������-������������� �������������� � �����',
        'path' => '/doctxt/inve_pote/razv_tran_infr/',
      ),
      3 => 
      array (
        'name' => '����������������� � ��������������� ���������',
        'path' => '/doctxt/inve_pote/inst_i_zako/',
      ),
      4 => 
      array (
        'name' => '�������� � ��������������� ���������',
        'path' => '/doctxt/inve_pote/trud_i_potr/',
      ),
      5 => 
      array (
        'name' => '��������� ������������ � ������������� �����',
        'path' => '/doctxt/inve_pote/umer_polit_i/',
      ),
      6 => 
      array (
        'name' => '������������� ���������, ����� � �����������',
        'path' => '/doctxt/inve_pote/inno_pote_nauk/',
      ),
      7 => 
      array (
        'name' => '������������ � ���������� ���������',
        'path' => '/doctxt/inve_pote/prom_i_fina/',
      ),
      8 => 
      array (
        'name' => '���������� - ������� ������',
        'path' => '/doctxt/inve_pote/inve_-_isto/',
      ),
    ),
  ),
  1 => 
  array (
    'name' => '�������������� �������',
    'path' => '/doctxt/inve_proe/',
    'next' => 
    array (
      1 => 
      array (
        'name' => '����� ��������',
        'path' => '/doctxt/inve_proe/pois_proe/',
      ),
    ),
  ),
  2 => 
  array (
    'name' => '�������������� ����������',
    'path' => '/doctxt/inve_stat/',
    'next' => 
    array (
      1 => 
      array (
        'name' => '���������� ��-�� ������',
        'path' => '/doctxt/inve_stat/inos_inve/',
        'next' => 
        array (
          0 => 
          array (
            'name' => '���������� ���������� ��-�� ������ �� ����� ����������',
            'path' => '/doctxt/inve_stat/inos_inve/stat_inve_iz-z_rube_po/',
          ),
          1 => 
          array (
            'name' => '���������� ���������� ��-�� ������ � ������� �����',
            'path' => '/doctxt/inve_stat/inos_inve/stat_inve_iz-z_rube_v/',
          ),
          2 => 
          array (
            'name' => '2004 (3 ��������)',
            'path' => '/doctxt/inve_stat/inos_inve/2004_(3_kvar/',
          ),
        ),
      ),
      2 => 
      array (
        'name' => '���������� � �������� �������',
        'path' => '/doctxt/inve_stat/inve_v_osno_kapi/',
      ),
    ),
  ),
  3 => 
  array (
    'name' => '�������� ���������',
    'path' => '/doctxt/stra_inve/',
    'next' => 
    array (
      1 => 
      array (
        'name' => '����� ���������� ��������������� �������',
        'path' => '/doctxt/stra_inve/etap_real_inve_proe/',
      ),
      2 => 
      array (
        'name' => '��������� ��������� ��������������� ���������',
        'path' => '/doctxt/stra_inve/proc_polu_gosu_podd/',
      ),
      3 => 
      array (
        'name' => '����� ��������������� ����������',
        'path' => '/doctxt/stra_inve/make_inve_sogl/',
      ),
      4 => 
      array (
        'name' => '����� ������-�����',
        'path' => '/doctxt/stra_inve/make_bizn/',
      ),
      5 => 
      array (
        'name' => '������� ��������� �������',
        'path' => '/doctxt/stra_inve/mest_nalo_sist/',
        'next' => 
        array (
        ),
      ),
    ),
  ),
  4 => 
  array (
    'name' => '����������������',
    'path' => '/doctxt/zakonodat/',
    'next' => 
    array (
      1 => 
      array (
        'name' => '���� �������� ����������',
        'path' => '/doctxt/zakonodat/ves_kompl_docu/',
      ),
      2 => 
      array (
        'name' => '����������� ����� �� 25 ������� 1999 �. N 39-�� "�� �������������� ������������ � ���������� ���������, �������������� � ����� ����������� ��������"',
        'path' => '/doctxt/zakonodat/fede_zako_ot_25_fev/',
      ),
      3 => 
      array (
        'name' => '����������� ����� �� 9 ���� 1999 �. N 160-�� "�� ����������� ����������� � ���������� ���������"',
        'path' => '/doctxt/zakonodat/fede_zako_ot_9_iyul/',
      ),
      4 => 
      array (
        'name' => '����� �� 22.06.2000 � 116-� "� ��������������� ��������� �������������� ������������ �� ���������� ������������� �������"',
        'path' => '/doctxt/zakonodat/zakon_ot_22_n_116/',
      ),
      5 => 
      array (
        'name' => '������������� �� 10.06.2002 � 122 "� ������� ����������, �����������, ������� ����� � �������� �� ����� ���������� �������������� ����������"',
        'path' => '/doctxt/zakonodat/post_ot_10_n_122/',
      ),
    ),
  ),
  5 => 
  array (
    'name' => '�������� ����������',
    'path' => '/doctxt/pole_info/',
    'next' => 
    array (
      1 => 
      array (
        'name' => '�������������� � �������������� ���������',
        'path' => '/doctxt/pole_info/inve_kons_agen/',
      ),
      2 => 
      array (
        'name' => '����� � ��������� ��������',
        'path' => '/doctxt/pole_info/bank_i_ipot_komp/',
        'next' => 
        array (
          0 => 
          array (
            'name' => '��� "�������������", ������������� ������',
            'path' => '/doctxt/pole_info/bank_i_ipot_komp/akb_pr_nizh_fili/',
          ),
        ),
      ),
      3 => 
      array (
        'name' => '��������� ��������',
        'path' => '/doctxt/pole_info/strah_komp/',
      ),
      4 => 
      array (
        'name' => '����������� �����',
        'path' => '/doctxt/pole_info/urid_firm/',
      ),
      5 => 
      array (
        'name' => '������������� ����������� � ������',
        'path' => '/doctxt/pole_info/mejd_orga_i_cent/',
      ),
      6 => 
      array (
        'name' => '������ � �����',
        'path' => '/doctxt/pole_info/pala_i_birj/',
      ),
    ),
  ),
  6 => 
  array (
    'name' => '������������� ��������',
    'path' => '/doctxt/inno_poli/',
    'next' => 
    array (
      1 => 
      array (
        'name' => '������������� ����������������',
        'path' => '/doctxt/inno_poli/inno_zako/',
        'next' => 
        array (
          0 => 
          array (
            'name' => '��������� "� ������� ���������� �� ���������� ������� ����� ���������� ������ ..."',
            'path' => '/doctxt/inno_poli/inno_zako/polo_o_pory_vozm_iz/',
          ),
        ),
      ),
      2 => 
      array (
        'name' => '������������� ������������',
        'path' => '/doctxt/inno_poli/inno_deya/',
        'next' => 
        array (
        ),
      ),
      3 => 
      array (
        'name' => '��������',
        'path' => '/doctxt/inno_poli/kont/',
      ),
      4 => 
      array (
        'name' => '����� �� ������-����������� � �������������� �������� ��� ������������� ������������� �������',
        'path' => '/doctxt/inno_poli/sove_po_nauc_i_inno/',
      ),
      5 => 
      array (
        'name' => '���������� ������������',
        'path' => '/doctxt/inno_poli/konk_doku/',
        'next' => 
        array (
          0 => 
          array (
            'name' => '������ �� ������� � �������� �� ������ ������������ ������������� �������� ������������� �������',
            'path' => '/doctxt/inno_poli/konk_doku/zaya_na_ucha_v_konk/',
          ),
          1 => 
          array (
            'name' => '�������� �������������� ������� (�������� ������������� ����������/��������)',
            'path' => '/doctxt/inno_poli/konk_doku/pass_inno_proe_(opi_inno/',
          ),
          2 => 
          array (
            'name' => '������',
            'path' => '/doctxt/inno_poli/konk_doku/anke/',
          ),
        ),
      ),
    ),
  ),
  7 => 
  array (
    'name' => '��������� �������',
    'path' => '/doctxt/svob_plos/',
    'next' => 
    array (
      1 => 
      array (
        'name' => '����������������',
        'path' => '/doctxt/svob_plos/proizvod/',
      ),
      2 => 
      array (
        'name' => '�������',
        'path' => '/doctxt/svob_plos/zelenye/',
      ),
    ),
  ),
  8 => 
  array (
    'name' => '�������� ����� � ��������',
    'path' => '/doctxt/obra_svya_i_kont/',
    'next' => 
    array (
    ),
  ),
) 
 ?>