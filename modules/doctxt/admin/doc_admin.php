<?
/**
 *  @todo v redaktirovanii stat'i dobavit' (chekboks "Opublikovannyj")
 *  @todo DONE nevernyj put' v admine pri perehode v DB (bez path_ tmp)
 *  @todo peremewenie odnotipnyh jelementov, i pereimenovanie aliasov
 * 
 **/
global $form,$_CORE, $_KAT;
$submit	= @$_REQUEST['submit'];
$form	= @$_REQUEST['form'];
$FormTree	= @$_REQUEST['form_tree'];
//Main::load_lang_mod('doctxt');

$_SESSION['DOC']['ADM_VIEWSTYLE'] = (!empty($qtree)) ? 'tree'
	: ((!empty($qlist)) ? 'list'
	: ((!empty($_SESSION['DOC']['ADM_VIEWSTYLE']))? $_SESSION['DOC']['ADM_VIEWSTYLE'] 
	:	'list'));

// Rabota s jelementom dereva na sajte Sohranenie/Redaktirovanie
if (!empty($submit)) {
	if (preg_match("|(.*/)".$_KAT['MODULE_NAME']."/(.+)|",$Cmd,$m) && empty($_DOC['conf']['tree_from_db']) )
		$Cmd = $m[1];
	if(DOCTXT::check_form($form, $Cmd) && DOCTXT::save($form, $Cmd)){
		$message	= Main::get_lang_str('data_changed', 'doctxt');
//		include $_CORE->CoreModDir."/admin/doc_admin.submenu.inc.php";
		$ShowTree = true;
		
		// Posle redaktirovanija
		if (!empty($_REQUEST['apply']))
			DOCTXT::show_form($form);
		elseif (DOCTXT::inc_tpl('after_edit.html', $file))
			include "$file";
//		DOCTXT::get_cont($Cmd);
	}else
		DOCTXT::show_form($form);

// Rabota V DEREVE (+ Spiski)
}elseif(!empty($qtree) || !empty($qlist)){
	
	$_DOC['TITLE']	= Main::get_lang_str('soderzhanie', 'doctxt');
	$_DOC['ERROR']	= '';
	$ShowTree	= true;
	$ShowList	= false;
	
	if($qlist){
		$ShowList	= true;
		$ShowTree	= false;
	}
	
	//
	// Dobavlenie razdela
	//
	if (!empty($_REQUEST['add'])) {
		$dir	= '';
		# vyznaem alias roditelja
		list($doc, $parent_path)	= $_CORE->cmd_parse($_REQUEST['add']);

		# sprjatat' ili pokazat'
		if (isset($_REQUEST['hide']) || isset($_REQUEST['show'])) {
			// najdem
//			$path	= '/'.$_DOC['MODULE_NAME'].$parent_path;
			$path	= $_REQUEST['add'];
			$tmp	= Main::array_multisearch($path, $_DOC['TREE']);
			$link	=& $_DOC['TREE'];
			// Vyshli na uroven'
			if (is_array($tmp)) {
				array_pop($tmp); // ubrali path ostalsja indeks
				for ($i=0;$i<sizeof($tmp)-1;$i++)
					$link	=& $link[$tmp[$i]];
				$link[$tmp[$i]]['hidden'] = (isset($_REQUEST['hide']))?'true':'';
				if(!empty($_DOC['conf']['tree_from_db'])){
					if (!$_CORE->dll_load('class.DBCQ'))
						die ($_CORE->error_msg());
					if($value = SQL::upd((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree', "hidden ='".((isset($_REQUEST['hide']))?'true':'')."'", "path = '$path'")){
					
					}
				} else {
					DOCTXT::save_tree( $_DOC['TREE'] );
				}
			}else {
				$_DOC['ERROR']	= Main::get_lang_str('doc_not_fnd', 'doctxt');
			}

		}
		# podnjat' dokument "vyshe"
		elseif (isset($_REQUEST['up'])) {
			// najdem
//			$path	= '/'.$_DOC['MODULE_NAME'].$parent_path;
			$path	= $_REQUEST['add'];
			$tmp	= Main::array_multisearch($path, $_DOC['TREE']);
			$link	=& $_DOC['TREE'];
			// Vyshli na uroven'
			if (is_array($tmp)) {
				array_pop($tmp); // ubrali path ostalsja indeks
				for ($i=0;$i<sizeof($tmp)-1;$i++)
					$link	=& $link[$tmp[$i]];
				if ($tmp[$i] > 0 && !empty($link[$tmp[$i]-1])) {
					$tmp_rec		= $link[$tmp[$i]];
					$link[$tmp[$i]]	= $link[$tmp[$i]-1];
					$link[$tmp[$i]-1] = $tmp_rec;

  				  DOCTXT::save_tree( $_DOC['TREE'] );
          
				}else {
					$_DOC['ERROR']	= Main::get_lang_str('rec_already_in_start', 'doctxt');
				}
			}else {
				$_DOC['ERROR']	= Main::get_lang_str('doc_not_fnd', 'doctxt');
			}
		}
		# pereimenovat'
		elseif (!empty($_REQUEST['rename'])) {
			// najdem
//			$path	= '/'.$_DOC['MODULE_NAME'].$parent_path;
			$path	= $_REQUEST['add'];
			if(!empty($_DOC['conf']['tree_from_db'])){
				DOCTXT::save_field( str_replace("/doctxt/", "", $path), 'name', stripslashes($_REQUEST['rename']) );
				DOCTXT::save_field( str_replace("/doctxt/", "", $path), 'title', stripslashes($_REQUEST['rename']) );
			} else {
  			$tmp	= Main::array_multisearch($path, $_DOC['TREE']);
  			$link	=& $_DOC['TREE'];
  			// Vyshli na uroven'
  			if (is_array($tmp)) {
  				array_pop($tmp); // ubrali path ostalsja indeks
  				for ($i=0;$i<sizeof($tmp)-1;$i++)
  					$link	=& $link[$tmp[$i]];
  				if (!empty($link[$tmp[$i]])) {
  					$link[$tmp[$i]]['name']	= stripslashes($_REQUEST['rename']);
  					DOCTXT::save_tree( $_DOC['TREE'] );
  					DOCTXT::save_field( str_replace("/doctxt/", "", $link[$tmp[$i]]['path']), 'title', $link[$tmp[$i]]['name'] );
  				}else {
  					$_DOC['ERROR']	= Main::get_lang_str('doc_not_fnd', 'doctxt');
  				}
  			}else {
  				$_DOC['ERROR']	= Main::get_lang_str('doc_not_fnd', 'doctxt').'..'.$tmp;
  				print_r($tmp);
  			}
      }
		}
		# est' direktorija razdela
		elseif (!DOCTXT::is_doc($parent_path, $dir)) {
			$_DOC['ERROR']	= Main::get_lang_str('doc_par_nfnd1', 'doctxt').$parent_path. Main::get_lang_str('doc_par_nfnd2', 'doctxt');
		}
		
		# est' nazvanie dokumenta, dobavljaem jelement
		elseif(!empty($_REQUEST['sub_name'])){
			$form['title']	= stripslashes($_REQUEST['sub_name']);
			#
			# otnositel'nyj put', alias bez imeni modulja.
			$otn_path		= $parent_path.STR::translit($_REQUEST['sub_name'], 50, 50, true).'/';

			if (DOCTXT::save($form, $otn_path)){
				#
				# put', alias, dlja obrabotki i zapisi v derevo.
				$form['path']	= '/'.$_DOC['MODULE_NAME'].$otn_path;
				#
				# Sohranim v derevo
				$path	= '/'.$_DOC['MODULE_NAME'].$parent_path;
				$tmp	= Main::array_multisearch($path, $_DOC['TREE']);
																				// esli jeto poduroven'
				if (is_array($tmp)) {
					array_pop($tmp);
					$link	=& $_DOC['TREE'];
					for ($i=0;$i<sizeof($tmp);$i++)
						$link	=& $link[$tmp[$i]];
					$link['next'][]	= array(
						'name'	=> $form['title'],
						'path'	=> $form['path'],
						'hidden'	=> 'true',
						'link'	=> (!empty($_REQUEST['sub_url']))?$_REQUEST['sub_url']:'',
					);
           $parent = $path;
           $parent_id = SQL::getval("id",(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree',"path = '$path'");
				}else {															// pervyj uroven'
					$_DOC['TREE'][]	= array(
						'name'	=> $form['title'],
						'path'	=> $form['path'],
						'hidden'	=> 'true',
						'link'	=> (!empty($_REQUEST['sub_url']))?$_REQUEST['sub_url']:'',
						);
            $parent = "/";
            $parent_id = 0;
				}
        if(!empty($_DOC['conf']['tree_from_db'])){
          if (!$_CORE->dll_load('class.DBCQ'))
            die ($_CORE->error_msg());
            $pos = SQL::getval("count(*)",(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree',"parent = '$parent'");
            SQL::ins((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree',"pos, parent, name, title, path, hidden, link, parent_id, ts","'".($pos+1)."', '$parent', '{$form['title']}', '{$form['title']}', '{$form['path']}', 'true', '".((!empty($_REQUEST['sub_url']))?$_REQUEST['sub_url']:'')."' , '$parent_id', '".time()."'");
        } else {
				  DOCTXT::save_tree( $_DOC['TREE'] );
        }
				if (empty($_REQUEST['node'])) $_REQUEST['node'] = '/';
				
//				19.12.2010 zakomentareno, chto by obnovljal stranicu i perehodil na after_add
//				ob_end_flush();
//				header("Location: /".$_DOC['MODULE_NAME'].'/?'.$_SESSION['DOC']['ADM_VIEWSTYLE'].'&node='.$_REQUEST['node']);
//				exit;

//				DOCTXT::save_tree( $_DOC['TREE'], 'ru' );
//				DOCTXT::save_tree( $_DOC['TREE'], 'en' );
				$_DOC['ERROR'] = Main::get_lang_str('data_saved', 'doctxt');
			}else  
				$_DOC['ERROR'] = Main::get_lang_str('doc_already_there', 'doctxt').$_DOC['MODULE_NAME'].$otn_path;
			
		}else {
			$_DOC['ERROR']	= Main::get_lang_str('not_put_doc_title', 'doctxt') .$_REQUEST['sub_name'];
		}

//echo $_DOC['ERROR'];
//		print_r($_REQUEST);	
//		exit;
			
		// t.e. posle dobavlenija, ne nado nichego delat', togda prosto peregruzhaetsja opener
		if (!empty($_REQUEST['inc'])){
			if (DOCTXT::inc_tpl('after_add.html', $file))
				include "$file";
		}else{
				ob_end_flush();	
				header("Location: /".$_DOC['MODULE_NAME'].'/?'.$_SESSION['DOC']['ADM_VIEWSTYLE'].'&node='.$_REQUEST['node']);
				exit;
		}
		
	}elseif (!empty($_REQUEST['del']) && in_array($_REQUEST['del'],array("doctxt","/doctxt","/doctxt/","//doctxt/","//","/"))===false ) {
    
		# vyznaem alias
		list($doc, $alias)	= $_CORE->cmd_parse($_REQUEST['del']);
		
		// na sluchaj esli jeto byla baza dannyh
		$subtree = DOCTXT::subtree($_DOC['TREE'], $_REQUEST['del'], true );
		unset($subtree['parent']);
		if (preg_match("|.*(/".$_KAT['MODULE_NAME']."/.+)|",$subtree['path'],$m)){
			echo  $m[1];
			KAT::remove($m[1], true);
		}
		
		DOCTXT::del($alias);
		
		DOCTXT::save_tree( $_DOC['TREE'] );
//		DOCTXT::save_tree( $_DOC['TREE'], 'ru' );
//		DOCTXT::save_tree( $_DOC['TREE'], 'en' );
		
		// Posle udalenija
		if (DOCTXT::inc_tpl('after_del.html', $file))
			include "$file";
			
	}elseif(!empty($_REQUEST[ITEM_CUT])||!empty($_REQUEST[ITEM_COPY])){
		$item_tmp = (!empty($_REQUEST[ITEM_CUT]))?$_REQUEST[ITEM_CUT]:$_REQUEST[ITEM_COPY];
		if( !in_array(	$item_tmp, $_SESSION['DOC']['CUTTED_ITEM_ALIAS'])){
				$_SESSION['DOC']['CUTTED_ITEM_ALIAS'][] = $item_tmp;
				$_SESSION['DOC']['CUTTED_TYPE'][] = (!empty($_REQUEST[ITEM_CUT]))?ITEM_CUT:ITEM_COPY;
		}
		/*
		vyzyvaetsja v bekgraunde iz JS po Ctrl+X posemu
		*/
		/**
		 * Also output
		 */
		echo "<SCRIPT>parent.CUTTED_DOC='".(!empty($_REQUEST[ITEM_CUT]))?$_REQUEST[ITEM_CUT]:$_REQUEST[ITEM_COPY]."'; document.location.reload();</SCRIPT>";
		exit;

	}elseif(isset($_REQUEST['paste'])){
    
		// esli zapomnili, i est' tekuwaja direktorija
    global $_USER;
    
		if (!empty($_SESSION['DOC']['CUTTED_ITEM_ALIAS']) && $_REQUEST['node'] && $_USER['ADM_BUFFER'] ){
		  // ���� ���������� $_REQUEST['clear_cutted'] ������� ����� � ������������� ��������     
  		if(isset($_REQUEST['clear_cutted'])){
       		ob_clean();
      		$_SESSION['DOC']['CUTTED_ITEM_ALIAS'] = '';
      		$_SESSION['DOC']['CUTTED_TYPE'] = '';
      		echo "<script>parent.location.reload();</script>";
      		exit;
      }
			# vyznaem alias
		foreach($_SESSION['DOC']['CUTTED_ITEM_ALIAS'] as $k=>$cutted_item_alias ){
			$cutted_type = $_SESSION['DOC']['CUTTED_TYPE'][$k];
			
			list($doc, $alias_from)	= $_CORE->cmd_parse($cutted_item_alias);
			list($doc2, $alias_to)	= $_CORE->cmd_parse($_REQUEST['node']);
			//$_CORE->log_message('$doc = '.$doc,'alias_to='.$alias_to ,' -> alias_from= '.$alias_from.' doctxt::paste');
			// esli kopiruem v papku, kotoraja podpapka kopiruemoj (anticikl)
			if (strpos($alias_to,$alias_from)>0){
				ob_clean();
				echo "<script>alert('".Main::get_lang_str('couldnt_copy', 'doctxt'). "$alias_to,$alias_from'); </script>";
				exit;
			}
			
			// vzjat' podderevo
			$subt_from = DOCTXT::subtree($_DOC['TREE'], $cutted_item_alias, 'noadd' ); 
      
      // ����� & ����� DOCTXT
      if (empty($subt_from)){
				ob_clean();
        $_SESSION['DOC']['CUTTED_ITEM_ALIAS'] = '';
        $_SESSION['DOC']['CUTTED_TYPE'] = '';
        $_CORE->log_message('DOCTXT doc_admin','$subt_from = empty');
				echo "<script>alert('".Main::get_lang_str('copy_err_empty', 'doctxt'). "'); parent.location.reload();</script>";
				exit;
			}
			$parent_from = $subt_from['parent'];
      //$_CORE->log_message('$subt_from = '.print_r($subt_from,1));
			if ($parent_from == '/') {
				$parent_from  = '/doctxt/';
			}
			
			$tmp = explode("/",$alias_from);
			array_pop($tmp);
			$from_last_path = array_pop($tmp);
			
			unset($subt_from['parent']);
      
      if(strpos($subt_from['path'], '/db/')!==false ){
        $alias_from = preg_replace("|/db/.+|","",$alias_from)."/";
        $doc = "db";
      }
			//$_CORE->log_message('$doc = '.$doc,'alias_to='.$alias_to ,' -> alias_from= '.$alias_from.' doctxt::paste');
      if ($_REQUEST['node']=="/") { 
        $subt_to['path'] = '/'; 
      }else{
        $subt_to =& DOCTXT::subtree($_DOC['TREE'], $_REQUEST['node'], 'noadd' );
      }
      

			unset($subt_to['parent']);
			// Esli v tu zhe direktoriju ili v direktorii uzhe est takaya, to dobavki
			if ($parent_from == $subt_to['path'] || DOCTXT::is_doc($alias_to.$from_last_path."/", $dir) || ($doc == "db" && $cutted_type == ITEM_COPY)){
        if ($doc == "db"){
          if (!$_CORE->dll_load('class.DBCQ'))
            die ($_CORE->error_msg());
          $tables = SQL::tlist();
        } else {
          $tables = array();
        }
          
				for ($i=1;1;$i++){
					if(!DOCTXT::is_doc($alias_to.$from_last_path."_".$i."/", $dir) && !in_array((( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') .$from_last_path."_".$i , $tables)) break;
				}
        //$_CORE->log_message('$alias_to.$from_last_path."_".$i."/" = '.print_r($alias_to.$from_last_path."_".$i."/",1));
        
				// DObavok k puti
				$add_to_path = "_$i";
				// DObavok k imeni
				$subt_from['name']	.= "($i)";
			}else {
				$add_to_path = '';
			}
			// skopirovat' fajlovuju strukturu
			if (DOCTXT::is_doc($alias_from, $dir_from) && DOCTXT::is_doc($alias_to, $dir_to) ) {
				$dir_to .= $from_last_path.$add_to_path."/";
//				exit;
				if (FILE::rn_dir($dir_from, $dir_to, false)){
					if (@DEBUG) $_CORE->log_message('', Main::get_lang_str('data_copied', 'doctxt').$dir_from,' -> '.$dir_to.' doctxt::paste');
//					echo 'Document data Copied '.$dir_from,' -> '.$dir_to.' doctxt::paste';
				}else {
					$_DOC['ERROR'] .= Main::get_lang_str('doc_not_del', 'doctxt').$doc_alias;
				}
				
        /*if($cutted_type == ITEM_COPY){
          $subt_from = DOCTXT::tree_nodb($subt_from);
        }
        $_CORE->log_message('$subt_from_nodb = '.print_r($subt_from,1)); 
        */
				// skopirovat' podmassiv
        
				if ($subt_to['path'] == '/') {
					$_DOC['TREE'][] = $subt_from;
					end($_DOC['TREE']);
				} else {
					$subt_to['next'][] = $subt_from;
					end($subt_to['next']);
				}

				if ($doc == "db" && !empty($add_to_path)){
				  
          $query = "CREATE TABLE ".(( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') .$from_last_path.$add_to_path." SELECT * FROM ".(( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') .$from_last_path.";";

        		$res = SQL::query($query,0);
        		if (!$res->Result)
        			echo $res->ErrorQuery;
           //	 sozdadim conf dlja DB
           $alias = $from_last_path.$add_to_path;
        	$onpage = (!empty($_KAT['ONPAGE_DEF'])) ? $_KAT['ONPAGE_DEF'] : 20;
        	$str = "
        	\$_KAT['onpage_def']['$alias']	= $onpage;
        	\$_KAT['TABLES']['$alias']	= '$alias';
        	\$_KAT['TITLES']['$alias']	= '".str_replace(array('"',"'"),"&quot;",$subt_from['name'])."';
        	\$_KAT['SRC']['$alias']	= '{$_KAT[SRC][$from_last_path]}';
        	\$_KAT['FILES']['$alias']	= '".$_KAT['FILES'][$_KAT['SRC'][$from_last_path]]."';
        	\$_KAT['$alias']['AFTER_ADD']	= '$_KAT[MODULE_NAME]/$alias/'; 
        	\$_KAT['$alias']['hidden']	= 'TRUE'; // ��� db/list";
          if(!empty($_KAT['DOC_PATH'][$from_last_path])){
          $str .= "
          \$_KAT['DOC_PATH']['$alias']	= '".$alias_to.$from_last_path.$add_to_path."$_KAT[MODULE_NAME]/$alias//inside'; ";
            
          }
          if(!empty($_KAT[$from_last_path]['matrix_table'])){
            if(is_array($_KAT[$from_last_path]['matrix_table'])){
              foreach($_KAT[$from_last_path]['matrix_table'] as $key=>$val){
          $str .= "
          \$_KAT['$alias']['matrix_table']['$key']	= '".$val."'; ";
              }
            }
          }
        	
        	// est' li fajlo, prochitaem, obnovim, sohranim
        	KAT::load_users_conf($str_was);
        	$str = implode('',$str_was).$str;
        	KAT::save_users_conf($str);
		      
     			// skopirovat' fajlovuju strukturu
          $dir_db = $_CORE->ROOT.$_CORE->DATA_PATH."db/";
// ��� ���� ����� ������������ �������� ���������� �������� � ���� ����������� /
  				if (FILE::rn_dir($dir_db.'f_'.$from_last_path, $dir_db.'f_'.$alias, false)){
  					if (@DEBUG) $_CORE->log_message('', Main::get_lang_str('data_copied', 'doctxt').$dir_db.'f_'.$from_last_path."/",' -> '.$dir_db.'f_'.$alias."/".' doctxt::paste');
  				}else{
  				  if (@DEBUG) $_CORE->log_message('', Main::get_lang_str('data_not_copied', 'doctxt').$dir_db.'f_'.$from_last_path."/",' -> '.$dir_db.'f_'.$alias."/".' doctxt::paste');
  				}

          
        }
				// pochinit' puti v poddereve i v poljah
				$wrong_path = $parent_from.$from_last_path."/";
				$good_path = $_REQUEST['node'].$from_last_path.$add_to_path."/";
        
        if ($doc == "db"){
  				// pochinit' puti v poddereve i v poljah
  				$wrong_path .= $doc."/".$from_last_path;
  				$good_path .= $doc."/".$from_last_path.$add_to_path;				  
        }
				
				if ($subt_to['path'] == '/') { 
				  //$_CORE->log_message('$wrong_path = '.print_r($wrong_path,1));
          //$_CORE->log_message('$good_path = '.print_r($good_path,1));
					DOCTXT::path_restore($_DOC['TREE'][key($_DOC['TREE'])], $wrong_path, '/doctxt'.$good_path);
				} else {
		      //$_CORE->log_message('$wrong_path = '.print_r($wrong_path,1));
          //$_CORE->log_message('$good_path = '.print_r($good_path,1));
					DOCTXT::path_restore($subt_to['next'][key($subt_to['next'])], $wrong_path, $good_path);
				}	
				
				// udalit' poddirektoriju
				// udalit' podmassiv
				if ($cutted_type == ITEM_CUT){

          if ($doc == "db" ){
            $alias_from .= $doc."/".$from_last_path."/";
            if(!empty($add_to_path)) KAT::remove($from_last_path, true);
          } 
					DOCTXT::del($alias_from);
        }
				
				// sohranit' derevo
        if(empty($_DOC['conf']['tree_from_db'])){
				  DOCTXT::save_tree( $_DOC['TREE'] );
        }else {
          DOCTXT::copy_tree_todb($cutted_item_alias, $_REQUEST['node'], $add_to_path);
/*          $treeDB[] = ($subt_to['path'] == '/')?$_DOC['TREE'][key($_DOC['TREE'])]:$subt_to['next'][key($subt_to['next'])];
          $_CORE->log_message('$treeDB = '.print_r($treeDB,1));
          DOCTXT::save_tree_todb($treeDB,$subt_to['path'],$subt_to['id']);
          $_DOC['TREE']=DOCTXT::get_tree_fromdb();*/
        }
				/*
				a esli byl Cut, a ne kopi, to nado udalit'
				*/
				/*
				vyzyvaetsja v bekgraunde iz JS po Ctrl+V posemu
				*/
      }
    }
    ob_clean();
    $_SESSION['DOC']['CUTTED_ITEM_ALIAS'] = '';
    $_SESSION['DOC']['CUTTED_TYPE'] = '';
    echo "<script>parent.location.reload()</script>";
    exit;
				
		}
		exit; // vot kak suda popadaet psle udachnogo kopirovanija neponjatno
	}

// novoe dobavlenie, i svjazka s DB	
}elseif(!empty($_REQUEST['new_add'])){
  
	
	if (empty($_REQUEST['name'])){
		$_DOC['ERROR'] = Main::get_lang_str('not_name_sect', 'doctxt');
		return;
	}
	if (empty($_REQUEST['node'])||($_DOC['conf']['adm_noadd_first'] && $_REQUEST['node']=='/')){
		$_DOC['ERROR'] = Main::get_lang_str('not_root_sect', 'doctxt');
		return;
	}
	
	global $_KAT;
	if (strlen($_REQUEST['name'])>60){
		$_DOC['ERROR'] = Main::get_lang_str('tabl_numletter_max', 'doctxt');
		return;
	}	
	$alias		= STR::translit(str_replace(array("'",'"'),"",$_REQUEST['name']), 50, 50, true);
	// ne dolzhno byt' tol'ko chislovyh, nel'zja takie tablicy
	if (is_numeric($alias)){
		$alias = 't'.$alias;
	}
	if (!empty($_KAT['TABLES'][$alias])){
		
		    for ($i=1;1;$i++){
					if(empty($_KAT['TABLES'][$alias."_".$i])) break;
				}
        $alias = $alias."_".$i;
	}
 // $_REQUEST['name'] = htmlspecialchars(str_replace("\"",'"',$_REQUEST['name']));
	if ($_REQUEST['node'] == '/') $_REQUEST['node'] = "/".$_DOC['MODULE_NAME']."/";

	$doc_path	= $_REQUEST['node'].$alias."/".$_KAT['MODULE_NAME']."/".$alias.'/';
	
	/**
	 *  Izmenenija v DB
	 *  1. zapisat' novuju shemu v conf.user.inc.php (ne zabudet pro prava na zapis')
	 *  2. i ustanosit'(sozdat' bazu dannyh)
	 *  
	 */
	
/*	if (!empty($_KAT['TABLES'][$alias])){
		$_DOC['ERROR'] = Main::get_lang_str('tabl_alr_there', 'doctxt');
		return;
	}*/
//	 sozdadim conf dlja DB
	$onpage = (!empty($_KAT['ONPAGE_DEF'])) ? $_KAT['ONPAGE_DEF'] : 20;
	$str .= "
	\$_KAT['onpage_def']['$alias']	= $onpage;
	\$_KAT['TABLES']['$alias']	= '$alias';
	\$_KAT['TITLES']['$alias']	= '".stripslashes(htmlspecialchars($_REQUEST['name']))."';
	\$_KAT['SRC']['$alias']	= '$_REQUEST[data_type]';
	\$_KAT['DOC_PATH']['$alias'] = '$doc_path/inside';
	\$_KAT['FILES']['$alias']	= '".$_KAT['FILES'][$_REQUEST['data_type']]."';
	\$_KAT['$alias']['AFTER_ADD']	= '$_KAT[MODULE_NAME]/$alias/'; // start without '/' or become '//' after add
	\$_KAT['$alias']['hidden']	= 'TRUE'; // ��� db/list
	";
	
	// est' li fajlo, prochitaem, obnovim, sohranim
	KAT::load_users_conf($str_was);
	$str = implode('',$str_was).$str;
	KAT::save_users_conf($str);
		
//	$fp	= fopen($file, 'w');
//	fputs($fp,"<?\n $str \n? >" );
//	fclose($fp);
//	
	cmd('/'.$_KAT['MODULE_NAME'].'/install');
	
//	
	// Na siim zakonchili
	//------------------------------------------------------------------------------
	
	/**
	 * Teper' v derevo izmenenija
	 * 1. Dobavim v derevo
	 */
	
	#
	# Sohranim v derevo
	$path	= $_REQUEST['node'];
//	echo $path."<br>";
	$tmp	= Main::array_multisearch($path, $_DOC['TREE']);
																	// esli jeto poduroven'
	if (is_array($tmp)) {
		array_pop($tmp);
		$link	=& $_DOC['TREE'];
		for ($i=0;$i<sizeof($tmp);$i++)
			$link	=& $link[$tmp[$i]];
		$link['next'][]	= array(
			'path'	=> $doc_path, // .'/'.$_KAT['MODULE_NAME']."/".$alias.'/'
			'name'	=> stripslashes(htmlspecialchars($_REQUEST['name'])),
//			'readonly'	=> 'true',
//			'path_ tmp'	=> "/".$_KAT['MODULE_NAME']."/".$alias.'/',
			);
    if(!empty($_DOC['conf']['tree_from_db'])){
      $parent = $path;
      $parent_id = SQL::getval("id",(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree',"path = '$path'");
    }
	}else {															// pervyj uroven'
		$_DOC['TREE'][]	= array(
			'path'	=> $doc_path,
			'name'	=> stripslashes(htmlspecialchars($_REQUEST['name'])),
//			'readonly'	=> 'true',
//			'path_ tmp'	=> "/".$_KAT['MODULE_NAME']."/".$alias.'/',
			);
      if(!empty($_DOC['conf']['tree_from_db'])){
        $parent = "/";
        $parent_id = 0;
      }
	}
  $form['title'] = stripslashes(htmlspecialchars($_REQUEST['name']));
  if(!empty($_DOC['conf']['tree_from_db'])){
    if (!$_CORE->dll_load('class.DBCQ'))
      die ($_CORE->error_msg());
      $pos = SQL::getval("count(*)",(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree',"parent = '$parent'");
      SQL::ins((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'rutree',"pos, parent, name, title, path, hidden, link, parent_id, ts","'".($pos+1)."', '$parent', '{$form['title']}', '{$form['title']}', '{$doc_path}', 'true', '' , '$parent_id', '".time()."'");
  } else {
	  DOCTXT::save_tree( $_DOC['TREE'] );
  }
	
	// i sohranim dannye
//	echo $_REQUEST['node'].$alias.'/'.$_KAT['MODULE_NAME']."/".$alias.'/';
	
//	print_r($_REQUEST);
	$doc_alias = str_replace("/".$_DOC['MODULE_NAME']."/", "", $_REQUEST['node'].$alias);
//	echo $doc_alias;
//	DOCTXT::save_field($doc_alias, 'cont', "<?=cmd('db/$alias')? >");
	DOCTXT::save_field($doc_alias, 'cont', " ");
	DOCTXT::save_field($doc_alias, 'path', str_replace("/".$_DOC['MODULE_NAME']."/","",$doc_path));
	DOCTXT::save_field($doc_alias, 'title', $_REQUEST['name']);

	$_DOC['ERROR'] = Main::get_lang_str('dir_created', 'doctxt');
	if (DOCTXT::inc_tpl('after_add.html', $file))
		include "$file";
		
	/**
	 * @done path_ tmp v jetoj tehnologii ne ispol'zuetsja, i ne objazatel'no, tol'ko dlja perehodov v adminke
	 * 
	 * @todo DONE  3. v shablone pri prosmotre konkretnoj zapisi, zamenit' ssylku, dlja raboty puti po saju
	 * @todo DONE 4. v obwem sluchae ne otrabatyvaet php i v razdele ne pokazyvajutsja materialy
	 * @todo DONE 5. ogranichit' dostupnye shablony "po DOC_PATH"
	 * 
	 */
	
}else{
	$_DOC['ERROR'] = '';
  if(empty($_DOC['conf']['tree_from_db'])){
  	global $_KAT;
  	if (preg_match("|(.*/)".$_KAT['MODULE_NAME']."/(.+)|",$Cmd,$m))
  		$Cmd = $m[1];
  }
	$data	= DOCTXT::load($Cmd); // $doc_alias from doc.php
	DOCTXT::show_form($data);
}
//
////
//// Vyvod dereva dokumentov
////
//if ($ShowTree) {
//	if (!empty($_REQUEST['subm_tree'])) {
//		if (DOCTXT::check_tree($FormTree) && DOCTXT::save_tree($FormTree)) {
//			include $_SERVER['DOCUMENT_ROOT'].'/'.$_DOC['MODULE_NAME']."/tree.inc.php";
//			USER::gettree();		// REDO ON INT !!!!! When base !!!
//		}
//	}
//
//	DOCTXT::show_admtree( $_DOC['TREE'] );
//
////	if (is_file($_CORE->SiteModDir."/doc.admtree.inc.phtml"))
////		include $_CORE->SiteModDir."/doc.admtree.inc.phtml";
////	else
////		include $_CORE->CoreModDir."/doc.admtree.inc.phtml";
//}
//
//if ($ShowList){
//	DOCTXT::show_admlist( $_DOC['TREE'], $qnode );
//}

switch ($_SESSION['DOC']['ADM_VIEWSTYLE']) {
	case 'tree':
		if (!$ShowTree) break;
		if (!empty($_REQUEST['subm_tree'])) {
			if (DOCTXT::check_tree($FormTree) && DOCTXT::save_tree($FormTree)) {
				include $_SERVER['DOCUMENT_ROOT'].'/'.$_DOC['MODULE_NAME']."/tree.inc.php";
				USER::gettree();		// REDO ON INT !!!!! When base !!!
			}
		}
	
		DOCTXT::show_admtree( $_DOC['TREE'] );
		break;
	case 'list':
		if (!$ShowList) break;
		//if( !empty($_GET['desc'])&&$_GET['desc']=='site' ){ $_GET['orderby']='order';  $_GET['desc']='up';}
		DOCTXT::show_admlist( $_DOC['TREE'], $qnode, empty($_GET['orderby'])?"":$_GET['orderby'], empty($_GET['desc'])?"":$_GET['desc'] );
		break;
	
	default:
		DOCTXT::show_admlist( $_DOC['TREE'], $qnode );
		$_SESSION['DOC']['ADM_VIEWSTYLE'] = 'list';
		
}
/**
 * Also output
 */


?>