<?
/*

	Poisk po derevu dokumentov v fajlah cont, anons, title - v takom porjadke prioriteta
	0.1 - prostaja, poisk podstroki

*/
global $_DOC;
//include_once "tree.inc.php"; // na vsjakij, esli uzhe bylo to ne povtorit'sja
$string		= $_REQUEST['go'];
$tree 		= (empty($_DOC['SEARCH_SUBTREE']) || $_DOC['SEARCH_SUBTREE'] == '//') ? array('next' => $_DOC['TREE']) : DOCTXT::subtree($_DOC['TREE'], $_DOC['SEARCH_SUBTREE']);
$show_res 	= (empty($_DOC['SEARCH_NOSHOW']));

if (!empty($string)) {
	if (empty($tree)) {
		$_DOC['ERROR']	= Main::get_lang_str('in_site_doc_nfnd', 'doctxt');
	}else{
		$_DOC['SEARCH_RES']	= array();// title, url, cont
		$_DOC['SEARCH_RES'][0]	= $string;
		DOCTXT::search($tree);

		if ($show_res) if (sizeof($_DOC['SEARCH_RES']) > 1) {

			
				if(DOCTXT::inc_tpl('search.head.html', $file))
					include $file;
				else{
					echo "<br>".Main::get_lang_str('search_phrases1', 'doctxt')."\"<b>$string</b>\"".Main::get_lang_str('search_phrases2', 'doctxt');
					echo "<br>".Main::get_lang_str('found', 'doctxt').STR::fins_prich(sizeof($_DOC['SEARCH_RES'])-1)." <B>".(sizeof($_DOC['SEARCH_RES'])-1)."</B>".Main::get_lang_str('doc', 'doctxt').STR::fins_sush(sizeof($_DOC['SEARCH_RES'])-1).".<br>";
				}
						
			foreach($_DOC['SEARCH_RES'] as $num => $data)
				if ($num > 0 && DOCTXT::inc_tpl('search.item.html', $file))
						include "$file";
			
			include DOCTXT::inc_tpl('search.foot.html', $file);


		}elseif(empty($_DOC['ERROR_NOSHOW'])) {
			if (DOCTXT::inc_tpl('search.nothing.html', $file))
					include "$file";
		}
	}
}


?>