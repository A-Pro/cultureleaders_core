<?

//define ("TO","andruha@programist.ru");
//define ("TO","online@new-technologies.ru");

$_OBSV['ERROR']	= '';

class OBSV {

	/**
	*
	*/
	function check( &$data )
	{
		global $_OBSV;
		if ( empty($_OBSV['NO_EMAIL_CHECK']) && !OBSV::is_email($data['email'])) {
			$_OBSV['ERROR']	.= OBSV_ERR_WRONG_EMAIL."\n";
		}
		$data['name'] = strip_tags($data['name']);
		if (empty($data['name'])) {
			$_OBSV['ERROR']	.= OBSV_ERR_EMPTY_NAME."\n";
		}
		$data['body'] = strip_tags($data['body']);
		if (empty($data['body'])) {
			$_OBSV['ERROR']	.= OBSV_ERR_EMPTY_BODY."\n";
		}elseif (strlen($data['body']) > 2048) {
			$_OBSV['ERROR']	.= OBSV_ERR_LARGE_BODY."\n";
		}
		return (empty($_OBSV['ERROR']));
	}

	/**
	*
	*/
	function send( &$data, $no_convert = false )
	{
		if($_CORE->IS_ADMIN && $_CORE->CURR_TPL	== 'admin' && !empty($data['tema'])){
			$message = '����: '.$data['tema'];
		}
		if ( $no_convert ) {
			$name 		  = $data['name'];
			$message      = (!empty($data['tema'])?'����: '.$data['tema']."\n":"").$data['body'];
			$subject      = '������ � ����� ' . $_SERVER['HTTP_HOST'];
			$charset      = $no_convert;
		}
		else {
			$name = convert_cyr_string ($data['name'], "w", "k");
			$message      = convert_cyr_string ((!empty($data['tema'])?'����: '.$data['tema']."\n":"").$data['body'], "w", "k");
			$subject      = convert_cyr_string ('������ � ����� '.$_SERVER['HTTP_HOST'], "w", "k");
			$charset      = 'koi8-r';
		}
		//------- �������� ��������� --------

                if ($data['type']=="html"){
					$headers="MIME-Version: 1.0\nContent-Type: text/html; charset={$charset};\nContent-transfer-Encoding: 8bit\nX-Mailer: PHP.";
                }
				else {
					$headers="MIME-Version: 1.0\nContent-Type: text/plain; charset={$charset};\nContent-transfer-Encoding: 8bit\nX-Mailer: PHP.";
                }

//                $test_str = preg_replace("/(.)/e", "sprintf('=%02x',ord('$1'))", $data['name']);
//                $data['name'] = '=?KOI8-R?Q?'.$test_str.'?=';

				$from   = "From: ".$name."<".$data['email'].">\n".$headers;
				
				global $_CORE;
				if($_CORE->IS_ADMIN && $_CORE->CURR_TPL	== 'admin'){
					@mail(ADMINTO, $subject, $message, $from);
				}else{
				    $to = TO;
				    if(!empty($data['to'])) $to = $data['to'];
					@mail($to, $subject, $message, $from);
				}
                return true;

	}
	
	/**
	*
	*/
	function is_email( $email )
	{
		if ( empty( $email ) || ! strchr( $email, '@' )) return 0;
		list($first, $domain) = explode("@", $email);
		$point = strpos($domain, ".");
		if ( strstr($email, " ") || $point == 0 || $point == strlen( $domain ) || strlen($first) == 0 ) return 0;
		else return 1;
	}
	
	/* �������� ����� � ������������� ������

	$file_name - ���� � �����, ������� ���� ���������� � ������
	(��� ����� ���� ��� �����, ���������� � ���� <input name="file_name" type="file">)
	*/
	function sendMail( &$data, $file_name ) {
		
		$message   = str_replace("\n","<br />", $data['body']);
		$message   = convert_cyr_string ($message, "w", "k");
		$from_name = convert_cyr_string ( $data['name'], "w", "k" );
		$from      = $data['email'];
		$subject   = (!empty($data['subject']))?$data['subject']:'������ � ����� '.$_SERVER['HTTP_HOST'];
		$subject   = convert_cyr_string ( $subject, "w", "k" );
		
		$boundary  = md5(uniqid(time()));
		$headers[] = "MIME-Version: 1.0";
		$headers[] = "Content-Type: multipart/mixed;boundary={$boundary};";
		$headers[] = "From: "       . $from;
		$headers[] = "Reply-To: "   . $from;
		$headers[] = "Return-Path: ". $from;
		//$headers[] ="X-Mailer: PHP/" ;
		
		$multipart[]= "--".$boundary;
		$multipart[] ="MIME-Version: 1.0";
		$multipart[]= "Content-Type: text/html; charset=koi8-r;";
		$multipart[]= "Content-Transfer-Encoding: Quot-Printed";
		$multipart[]= ""; // ������ ����� ����������� � ����� html-�����
		$multipart[]= $message;
		$multipart[]= "";

		$file        = fopen($file_name,"rb");
		$filecontent = fread($file,filesize($file_name));

		$multipart[]="--".$boundary;
		$multipart[] ="MIME-Version: 1.0";
		$multipart[]= "Content-Type: application/octet-stream; name=\"".convert_cyr_string ( basename($data['doc']), "w", "k" )."\"";
		$multipart[]= "Content-Transfer-Encoding: base64";
		$multipart[]= "Content-Disposition: attachment; filename=\"".convert_cyr_string ( basename($data['doc']), "w", "k" )."\"";
		$multipart[]= "";
		$multipart[]= chunk_split(base64_encode($filecontent));
		$multipart[]= "{$boundary}--";

		$headers   = implode("\n", $headers);
		$multipart = implode("\n", $multipart);

		$to = TO;
		return mail($to, $subject, $multipart, $headers);
	}
	
	
	
	function send_mime_mail($name_from, // ��� �����������
							$email_from, // email �����������
							$name_to, // ��� ����������
							$email_to, // email ����������
							$data_charset, // ��������� ���������� ������
							$send_charset = 'koi8-r', // ��������� ������
							$subject, // ���� ������
							$body, // ����� ������
							$type = 'text'
	) {
	    $bcc = array();
	    if(strpos($email_to,";")){
	       $bcc = explode(';',$email_to);
           $email_to = array_shift($bcc);
	    }
		$to = OBSV::mime_header_encode($name_to, $data_charset, $send_charset)
		. ' <' . $email_to . '>';
		$subject = OBSV::mime_header_encode($subject, $data_charset, $send_charset);
		$from =  OBSV::mime_header_encode($name_from, $data_charset, $send_charset)
		.' <' . $email_from . '>';
		if($data_charset != $send_charset) {
			$body = iconv($data_charset, $send_charset, $body);
		}
		$headers = "From: $from\r\n";
        if(count($bcc)){
            foreach($bcc as $emailBcc){
                $headers .= "Bcc: ".$emailBcc."\r\n";
            }
        }
		
		if ($type == "html"){
			$headers.="MIME-Version: 1.0\r\nContent-Type: text/html; charset={$send_charset};\r\nContent-transfer-Encoding: 8bit\r\nX-Mailer: PHP.";
		} else {
			$headers.="MIME-Version: 1.0\r\nContent-Type: text/plain; charset={$send_charset};\r\nContent-transfer-Encoding: 8bit\r\nX-Mailer: PHP.";
		}
		return @mail($to, $subject, $body, $headers);
	}
	
	function mime_header_encode($str, $data_charset, $send_charset) {
		if($data_charset != $send_charset) {
			$str = iconv($data_charset, $send_charset, $str);
		}
		return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
	}
}

?>