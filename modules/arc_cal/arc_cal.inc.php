<?


class ArcCal {

	///////////////////////////////////////////////////////////  _show_calendar
	//------------------------------------------------------------------------
	//  ������� ������� � ����������, 
	//  
	//  ������� ���������: 
	//  $month - �� ����� ����� �������� ��������� (���� ����������� ��� ����, �� �� ������� �����)
	//	$link - 
	//			������ ��� ������� ���. ���� �� ������ �� � ����� 
	//			����� ������: HREF="$link$a.$month.$year" - ��� $a - ���� DD, $mounth - ����� MM, $year - ��� YY
	//			�����: HREF="#"
	//	$OnClick_func, $submit - 
	//			�������� JS ������� ���� �� ������ �� ����������� �������: 		
	//			�����:  OnClick=\"$OnClick_func( '$a-$month-$year,' ); return $submit;\"
	//   
	//	$day, $year - ���������������. � �� ������������
	//----------
	// TESTED 3/27/02 by Land
	// 
	function show_calendar( &$data_arr, $month=0, $year = 0, $link = '', $OnClick_func='', $submit=false, $day=0, $year=0 )
	{
		if (empty($month)) $month = date("n",time());
		$year = (empty($year)) ? date("Y",time()) : $year;
		$padding = date("m",time());
		$day = (empty($day)) ? date("d",time()) : $day;
	
		//	Get info for Calendar generation
		$daysmonth = date("t",mktime(0,0,0,$month,$day,$year));
		$firstday = date("w",mktime(0,0,0,$month,1,$year));
		$padmonth = date("m",mktime(0,0,0,$month,$day,$year));
		$padday = date("d",mktime(0,0,0,$month,$day,$year));
	
		$currdate = date("d",time());
		$currmonth = date("n",time());
	
		//	Customize according to $calendar_format
				if ($firstday == 0) 
					$firstday = 7;
				$date = "$year-$padmonth-$padday";
				$dayletter = array(1 => "��", 2 => "��", 3 => "��", 4 => "��", 5 => "��", 6 => "��", 7 => "��");
				$monthlet = array(1 => "������", '01' => "������",2 => "�������", '02' => "�������", '03' => "����", 3 => "����", 4 => "������", '04' => "������", 5 => "���", '05' => "���", 6 => "����", '06' => "����", 7 => "����", '07' => "����", 8 => "������", '08' => "������", 9 => "��������", '09' => "��������", 10 => "�������", 11 => "������", 12 => "������");
				$daymod = 0;
	
			if ($month == 1) {
				$pyear = $year - 1;
				$pmonth = 12;
			} else {
				$pyear = $year;
				$pmonth = $month - 1;
			}
	
			if ($month == 12) {
				$nyear = $year + 1;
				$nmonth = 1;
			} else {
				$nyear = $year;
				$nmonth = $month + 1;
			}
			
			if (strlen($month) == 1)
				$month = "0$month";
	
		//	Print the dayletters
		echo '<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 class="schedule" width=100%>';
		echo "<TR><TD colspan=7 class='month'>$monthlet[$month]</TD></tr><tr>";
		for($daynumber = 1; $daynumber < 8; $daynumber++) {
			echo "<th>$dayletter[$daynumber]</th>";
		}
		echo "</tr><tr>";
		//	Print the calendar
		global $_KAT;
	
		for ($i = 1; $i < $daysmonth + $firstday + $daymod; $i++) 
		{
			$a = substr("0".($i - $firstday + 1 - $daymod), -2);
			$day = $i - $firstday + 1 - $daymod;
			//	Empty images to pad the calendar
			if (($i < $firstday) || ($a == "00")) 
				echo "<td>&nbsp;</td>";
			else 
			{
				if ($_KAT['article']["SHOW_NUMBER"] == "$year-$month-$a"){
					$key = array_search("$year-$month-$a",$data_arr);
//					echo "<td bgcolor='#e9f2d8'>$day</td>";
					echo "<td bgcolor='#e9f2d8'><a ";
					echo (empty($OnClick_func)) ? '' : " OnClick=\"$OnClick_func( '$a-$month-$year,' ); return $submit;\" ";
					echo (empty($link)) ? "  HREF='#' " : " HREF=\"$link$key\" ";
					echo ">$day</a></td>";
				}elseif	(false !== ($key = array_search("$year-$month-$a",$data_arr))){
					echo "<td><a ";
					echo (empty($OnClick_func)) ? '' : " OnClick=\"$OnClick_func( '$a-$month-$year,' ); return $submit;\" ";
					echo (empty($link)) ? "  HREF='#' " : " HREF=\"$link$key\" ";
					echo ">$day</a></td>";
				}else{
					echo "<td>$day</td>";
				}
//				if (($currdate == $a) && ($currmonth == $month))
//				{
//					echo "<td><a ";
//					echo (empty($OnClick_func)) ? '' : " OnClick=\"$OnClick_func( '$a-$month-$year,' ); return $submit;\" ";
//					echo (empty($link)) ? "  HREF='#' " : " HREF=\"$link$a.$month.$year\" ";
//					echo "><div class=small><FONT color=red>$a</FONT></div></a></td>";
//				}
//				else
//				{
//					echo "<td align=\"center\"><a  ";
//					echo (empty($OnClick_func)) ? '' : " OnClick=\"$OnClick_func( '$a-$month-$year,' ); return $submit;\" ";
//					echo (empty($link)) ? "  HREF='#' " : " HREF=\"$link$a.$month.$year\" ";
//					echo "><div class=small>$a</a></div></td>";
//				} // else ($curdate)
			} // else ($i < $firstday
			if (($i%7) == 0) 
				echo "</tr><tr>\n";
		}
		if (($i%7) != 1) 
			echo "</tr>\n";
		echo "</TABLE>";
	
//		echo "<PRE>";
//		print_r($data_arr);
//		echo "</PRE>";

// 		FROM UP TO DOWN
		
//		echo '<TABLE width="100%"  border="0" cellspacing="0" cellpadding="0">';
//		for ($daynumber = 0; $daynumber < 7; $daynumber++ ) {
//			// ���� ������ 
//			echo '<tr  align="center" valign="middle"><td width="18" height="18" align="left" valign="middle" class="text-01">'.$dayletter[$daynumber+1].'</td>';
//			echo '<TD align="center"><IMG src="/images/dots_01.gif" width="65" height="12"></TD>';
//			for ($i=$daynumber+1; $i < 36; $i=$i+7 ) {
//				$day = $i  - $firstday + 1;
//				$a = (strlen($day) == 1) ? "0$day" : $day;
//				if ($day > 0 && $day <= $daysmonth) {
//					$bg = ($currdate == $day) ? 'class="greyBg_2"': '';
//					if (!empty($data_arr["$a.$month.$year"])){
//						echo "<TD width=\"18\" height=\"18\" align=\"center\" $bg ><a  ";
//						echo (empty($OnClick_func)) ? '' : " OnClick=\"$OnClick_func( '$a-$month-$year,' ); return $submit;\" ";
//						echo (empty($link)) ? "  HREF='#' " : " HREF=\"$link$a.$month.$year/\" ";
//						echo ">$day</td>";
//					}else {
//						echo "<TD width=\"18\" height=\"18\" align=\"center\" $bg >$day</td>";
//					}
//				}else {
//					echo "<TD width=\"18\" height=\"18\" align=\"center\" $bg >&nbsp;</td>";
//				}
//			}
//			echo "</TR>";
//		}
//		echo "</TABLE>";
	}
	
	function show_form (){
		
		global $_CORE;
		
		$submit = $_REQUEST['Submit'];
		
		switch ($_REQUEST['ch']){
			case "y": $_REQUEST['month'] = '';$_REQUEST['day']='';
			case "m": $_REQUEST['day']='';
		}
		
		
		global $_KAT;
		if (!isset($_KAT['articles']['CUR_NUMBER'])) cmd('/db/articles/nothing');
		
		$where = " hidden != '1' AND ts != '".$_KAT['article']['CUR_NUMBER']."' ";
		
		$where_y = (!empty($_REQUEST['year'])) ? " AND DATE_FORMAT(ts, '%Y')  = '".$_REQUEST['year']."'" : '';
		$where_m = (!empty($_REQUEST['month']) && $_REQUEST['ch'] != 'y') ? " AND  DATE_FORMAT(ts, '%m') = '".$_REQUEST['month']."'" : '';
		$where_d = (!empty($_REQUEST['day'])) ? " AND DATE_FORMAT(ts, '%d') = '".$_REQUEST['day']."'" : '';
		
		// ������� �� ����
		SQL::col2arr($years, 'numbers',' DISTINCT ( DATE_FORMAT(ts, "%Y") ) as y', '', $where." ORDER BY y DESC");
		if (empty($_REQUEST['year'])) $_REQUEST['year'] = $years[0];
		$where_y = (!empty($_REQUEST['year'])) ? " AND DATE_FORMAT(ts, '%Y')  = '".$_REQUEST['year']."'" : '';
		
		// ������� �� ������
		SQL::col2arr($months, 'numbers',' DISTINCT ( DATE_FORMAT(ts, "%m") ) as m', '', $where." ". $where_y. " ORDER BY m DESC");
		if (empty($_REQUEST['month'])) $_REQUEST['month'] = $months[0];
		$where_m = (!empty($_REQUEST['month']) && $_REQUEST['ch'] != 'y') ? " AND  DATE_FORMAT(ts, '%m') = '".$_REQUEST['month']."'" : '';
		
		// ������� �� ���
		SQL::col2arr($days, 'numbers',' DISTINCT ( DATE_FORMAT(ts, "%d") ) as d', '', $where." ". $where_y. $where_m." ORDER BY d DESC");
		if (empty($_REQUEST['day'])) $_REQUEST['day'] = $days[0];
		
		$one_day = (!empty($_REQUEST['day']))
					? $_REQUEST['day']
		//			:'';
					:((sizeof($days)>=1)
						?$days[0]
						:'');
						
						
		$NumName =  (!empty($one_day)) 
			? SQL::getval('name','numbers', "DATE_FORMAT(ts, '%Y-%m-%d') = '$_REQUEST[year]-$_REQUEST[month]-$one_day'",'',0)
			: '';
		
		
		?>
		
				<FORM method="POST" name="tform">
				<table border="0" cellpadding="0" cellspacing="0" class="archive"  id="noprint">
						<input type="hidden" name="ch" value="no">
						<tr>
							<td class="tdleft">
								<select name="year" onchange="document.tform.ch.value='y';document.tform.submit()">
									<?foreach ($years as $y){?>
									<option value="<?=$y?>" <?=($_REQUEST['year']==$y)?'selected':''?>><?=$y?> ���</option>
									<?}?>
								</select>
							</td>
							<td>
								<select name="month" onchange="document.tform.ch.value='m';document.tform.submit()">
									<?foreach ($months as $m){?>
									<option value="<?=$m?>" <?=($_REQUEST['month']==$m)?'selected':''?>><?=$_CORE->CONF['month'][$m]?></option>
									<?}?>
								</select>
							</td>
							<td>
								<select name="day" onchange="document.tform.ch.value='d';document.tform.submit()">
									<?foreach ($days as $d){?>
									<option value="<?=$d?>" <?=($_REQUEST['day']==$d)?'selected':''?>><?=STR::week_day("$y-$m-$d")?>,  <?=(int)$d?></option>
									<?}?>
								</select>
							</td>
							<td><?=$NumName?></td>
							<td><input type="submit" name="Submit" value="�������" <?=(empty($NumName))?'DISABLED':''?> onClick="document.location='/archive/issue.html';"></td>
						</tr>
				</table>
				</FORM>
		
		<?
		if (!empty($_REQUEST['Submit']) && $_REQUEST['ch'] == 'no' && !empty($NumName)){
			ob_end_clean();
		//	global $_DOC;
		//	$_SERVER['REQUEST_URI'] = '/doctxt/arhiv/inside'; // ��� �� ���� ����������
		//	$_DOC['RE_TITLE'] = (int)$one_day." ".$_CORE->CONF['months'][$_REQUEST['month']]." $_REQUEST[year] �., ".STR::week_day("$y-$m-$d").", $NumName";
		//	$_REQUEST['form']['number_id'] = SQL::getval('id','numbers', "DATE_FORMAT(ts, '%Y-%m-%d') = '$_REQUEST[year]-$_REQUEST[month]-$one_day'",'',0);	
		//	echo cmd('/db/articles/');
			echo "<SCRIPT>top.location.href='/db/articles/?form[number_id]=".SQL::getval('id','numbers', "DATE_FORMAT(ts, '%Y-%m-%d') = '$_REQUEST[year]-$_REQUEST[month]-$one_day'",'',0)."';</SCRIPT>";
			exit;
		
		}
	}
}


?>