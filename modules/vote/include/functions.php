<?

/********************************************************************
*/
function vote_answer( $aid )
{
	global $_VOTE;
	if (isset($_VOTE['conf']['period'])){
		$period = " and data > UNIX_TIMESTAMP()-".$_VOTE['conf']['period'];
	} else {
		$period = "";
	}
	$is_voted = SQL::getval('remote_addr', DB_TABLE_PREFIX.'vote_answers a, '.DB_TABLE_PREFIX.'vote_remotes r', "a.id = '$aid' and a.quest_id = r.quest_id and r.remote_addr = '".$_SERVER['REMOTE_ADDR']."'".$period);
	if ($is_voted != $_SERVER['REMOTE_ADDR']) {
	    $update	= SQL::upd( DB_TABLE_PREFIX.'vote_answers', "votes = votes + 1", "id = '$aid'", 0);
		if ($_SERVER['REMOTE_ADDR'] != '') {
			$id = SQL::getval('quest_id', DB_TABLE_PREFIX.'vote_answers', "id = '$aid'");
			$insert = SQL::ins( DB_TABLE_PREFIX.'vote_remotes', " quest_id, remote_addr, data", "'$id', '".$_SERVER['REMOTE_ADDR']."', '".time()."'");
		}
	}
} // end func

/********************************************************************
*/
function vote_load( $id )
{
	global $_VOTE;
    $all = SQL::sel('q.title as title, a.title as ans, q.akt as akt, q.id as id, a.id as aid, comment, votes, views, q.data', DB_TABLE_PREFIX."vote_questions q, ".DB_TABLE_PREFIX."vote_answers a", "q.id = a.quest_id AND q.id='$id'", 'ORDER BY aid');
	if ($all->NumRows == 0) {
	    $_VOTE['ERROR_MESS']	= "������ $id �� ������";
		return false;
	} else {
		for( $i = 0; $i < $all->NumRows; $i++ ) {
		    $all->FetchArray($i);
			$_VOTE['vote']['ans'][] = $all->FetchArray['ans'];
			$_VOTE['vote']['ans_votes'][] = $all->FetchArray['votes'];
			$_VOTE['vote']['ans_id'][] = $all->FetchArray['aid'];
		}
		$_VOTE['vote']['data'] = $all->FetchArray['data'];
		$_VOTE['vote']['ts']   = strtotime($all->FetchArray['data']);
		$_VOTE['vote']['title'] = $all->FetchArray['title'];
		$_VOTE['vote']['comment'] = $all->FetchArray['comment'];
		$_VOTE['vote']['akt'] = $all->FetchArray['akt'];
		$_VOTE['vote']['id'] = $all->FetchArray['id'];
		$_VOTE['vote']['views'] = $all->FetchArray['views'];
	}
	return true;
} // end func


/********************************************************************
*/
function view_result( $empty = false)
{
    global $_VOTE, $_CORE;
	$all_count	= array_sum($_VOTE['vote']['ans_votes']);
	$draw = ($all_count != 0);
//	print_r($_VOTE['loaded']);
	//include $_CORE->SiteModDir."/result.inc.html";
	include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."result.inc.html";
} // end func



?>