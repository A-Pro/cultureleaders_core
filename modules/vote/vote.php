<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : vote.php
// PROJECT NAME : VOTINGS
// DATE         : 27.05.2004
// Author       : Andrew Lyadkov (andrey@lyadkov.nnov.ru)
// Discription  : 
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
global $_CORE, $_VOTE, $_PROJECT; 
include_once $_CORE->SiteModDir."/settings.inc.php";
	if ($_SERVER['REMOTE_ADDR']==$_SERVER['SERVER_ADDR'] && !empty($_SERVER['HTTP_X_REAL_IP']) ){
		$_SERVER['REMOTE_ADDR']=$_SERVER['HTTP_X_REAL_IP'];
	}
switch ($Cmd) {
	case "adm_menu": // 17.11.2005
				if (!empty($_VOTE['ADM_MENU']))
								$_PROJECT['ADM_MENU'] = array_merge($_PROJECT['ADM_MENU'], $_VOTE['ADM_MENU']);  // conf.inc.php
				break;

	case 'content/title/short':
	case 'content/title':
		global $_PROJECT;
		$_PROJECT['PATH_2_CONTENT'][0]	= array(
			'name'	=> ($_CORE->LANG != 'en')?'�����':'Voting' ,
			'path'	=> '/vote/'
		);
		echo $_CORE->cmd_exec(array('user','/'.$Cmd));
	case 'content/seo_title':
	break;
	case 'content/path':
		echo '';
	break;
	case 'module/title':
		echo @$_VOTE['MODULE_TITLE'];
		break;
	case 'module/desc':
		echo @$_VOTE['MODULE_DESC'];
		break;

	case 'install': 

		$sql = "

CREATE TABLE IF NOT EXISTS `".DB_TABLE_PREFIX."vote_answers` (
  `id` int(11) NOT NULL auto_increment,
  `quest_id` int(10) unsigned default NULL,
  `title` varchar(255) default NULL,
  `votes` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=31 ;
";
		SQL::query($sql, DEBUG );

		$sql = "

CREATE TABLE IF NOT EXISTS `".DB_TABLE_PREFIX."vote_questions` (
  `id` int(11) NOT NULL default '0',
  `title` varchar(255) default NULL,
  `comment` text,
  `akt` char(1) NOT NULL default '1',
  `views` int(10) unsigned default '0',
  `data` date NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

";
		SQL::query($sql, DEBUG );

		$sql = "
CREATE TABLE IF NOT EXISTS `".DB_TABLE_PREFIX."vote_remotes` (
  `id` int(11) NOT NULL auto_increment,
  `quest_id` int(10) unsigned default NULL,
  `remote_addr` varchar(255) default NULL,
  `data` int(20) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=49 ;

";
		SQL::query($sql, DEBUG );

		break;
	case "admin":
		if ($_CORE->IS_ADMIN && $_CORE->CURR_TPL=='admin'){
			include $_CORE->CoreModDir."/admin/vote_admin.php";
			include $_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'.'kat_admin.submenu.inc.php';
			break;
		}
	case "all":
			include $_CORE->CoreModDir."/results.all.php";
			break;
	case "any":
		unset($_REQUEST['id']);
		$_VOTE['vote'] = array();
	case "ask":
		$_VOTE['id'] = $_REQUEST['id'];
		if (empty($_REQUEST['id']) || !vote_load($_REQUEST['id'])) {
			$all	= SQL::sel('id', DB_TABLE_PREFIX.'vote_questions', "akt = 1");
			for ($i = 0; $i < $all->NumRows; $i++ ) {
				$all->FetchArray($i);
				$all_arr[]	= $all->FetchArray['id'];
			} 
			if (is_array($all_arr)) {
				$num	= array_rand($all_arr);
				vote_load($all_arr[$num]);
				$_VOTE['id'] = $all_arr[$num];
				$all_count	= array_sum($_VOTE['vote']['ans_votes']);
			}else {
				return " ";
			}
		}
		//include "ask.inc.html";
		$questId='';
		include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."ask.inc.html";
		break;
	case "lost":
		$ids= SQL::sel('id', DB_TABLE_PREFIX.'vote_questions', "akt = 1", " ORDER BY data DESC LIMIT 1");
		if($ids->NumRows > 0) {
			$ids->FetchArray($i);
			$vote_id = $ids->FetchArray['id'];
 			vote_load($vote_id);
			$_VOTE['id'] = $vote_id;
			$all_count	= array_sum($_VOTE['vote']['ans_votes']);
		}else {
			return " ";
		}
		$questId='';
		if (isset($_VOTE['conf']['period'])){
			$period = " and data > UNIX_TIMESTAMP()-".$_VOTE['conf']['period'];
		} else {
			$period = "";
		}
		$is_voted = SQL::getval('remote_addr', DB_TABLE_PREFIX.'vote_remotes', "quest_id = {$_VOTE['id']} and remote_addr = '".$_SERVER['REMOTE_ADDR']."'".$period);
		if ($is_voted != $_SERVER['REMOTE_ADDR']) {
			include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."ask.inc.html";
		} else {
			$draw = ($all_count != 0);
			include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."result.inc.html";
		}
		break;
	case "next":
		if (isset($_REQUEST['prev']) && (int)$_REQUEST['prev']){
			$prev = (int)$_REQUEST['prev'];
			$all	= SQL::sel('id', DB_TABLE_PREFIX.'vote_questions', "akt = 1", " ORDER BY data DESC");
			$count = $all->NumRows;
			$prev_num = -1;
			for ($i = 0; $i < $count; $i++ ) {
				$all->FetchArray($i);
				$all_arr[$i]	= $all->FetchArray['id'];
				if ($all_arr[$i] == $prev){
					$prev_num = $i;
				}
			} 
			if (is_array($all_arr)) {
				if ($prev_num!=-1 && $prev_num!=($count-1) ){
					$num = $prev_num+1;
				} else {
					$num = 0;
				}
				vote_load($all_arr[$num]);
				$_VOTE['id'] = $all_arr[$num];
				$all_count	= array_sum($_VOTE['vote']['ans_votes']);
			}else {
				return " ";
			}
		} else {
			return " ";
		}
		//include "ask.inc.html";
		$questId='';

		if (isset($_VOTE['conf']['period'])){
			$period = " and data > UNIX_TIMESTAMP()-".$_VOTE['conf']['period'];
		} else {
			$period = "";
		}
		$is_voted = SQL::getval('remote_addr', DB_TABLE_PREFIX.'vote_remotes', "quest_id = {$_VOTE['id']} and remote_addr = '".$_SERVER['REMOTE_ADDR']."'".$period);
		if ($is_voted != $_SERVER['REMOTE_ADDR']) {
			include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."ask.inc.html";
		} else {
			$draw = ($all_count != 0);
			include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."result.inc.html";
		}
		break;
	case "":
	default:
		if (empty($_REQUEST['aid']) || !SQL::getval('quest_id', DB_TABLE_PREFIX.'vote_answers', "id = '".$_REQUEST['aid']."'")) {
			echo "<HR><H1 align=center>�� ������ �����!</H1><HR>";
			return;
		}
		vote_answer( $_REQUEST['aid'] );
		include $_CORE->CoreModDir."/results.inc.php";
}



?> 