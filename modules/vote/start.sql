-- phpMyAdmin SQL Dump
-- version 3.2.3
-- http://www.phpmyadmin.net
--
-- ����: localhost
-- ����� ��������: ��� 28 2012 �., 12:21
-- ������ �������: 5.0.45
-- ������ PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- ���� ������: `kamenki`
--

-- --------------------------------------------------------

--
-- ��������� ������� `kamenki__vote_answers`
--

CREATE TABLE IF NOT EXISTS `kamenki__vote_answers` (
  `id` int(11) NOT NULL auto_increment,
  `quest_id` int(10) unsigned default NULL,
  `title` varchar(255) default NULL,
  `votes` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

--
-- ��������� ������� `kamenki__vote_questions`
--

CREATE TABLE IF NOT EXISTS `kamenki__vote_questions` (
  `id` int(11) NOT NULL default '0',
  `title` varchar(255) default NULL,
  `comment` text,
  `akt` char(1) NOT NULL default '1',
  `views` int(10) unsigned default '0',
  `data` date NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `kamenki__vote_remotes`
--

CREATE TABLE IF NOT EXISTS `kamenki__vote_remotes` (
  `id` int(11) NOT NULL auto_increment,
  `quest_id` int(10) unsigned default NULL,
  `remote_addr` varchar(255) default NULL,
  `data` int(20) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=49 ;
