<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : vote.php
// PROJECT NAME : VOTINGS
// DATE         : 27.05.2004
// Author       : Andrew Lyadkov (andrey@lyadkov.nnov.ru)
// Discription  : 
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////

$dbyear = SQL::sel('DATE_FORMAT(data, "%Y") as year', DB_TABLE_PREFIX.'vote_questions', 'akt = 1', ' group by year order by data  ');
if ($dbyear->NumRows > 0) {
	for ($i = 0; $i < $dbyear->NumRows; $i++ ) {
		$dbyear->FetchArray($i);
		$year[]	= $dbyear->FetchArray['year'];
	}

}
if (isset($_REQUEST['year']) && (int)$_REQUEST['year']){
	$seach = " and DATE_FORMAT(data, '%Y') = ".$_REQUEST['year'];
} else{
	$seach = " and DATE_FORMAT(data, '%Y') = ".$year[(count($year)-1)];
	$_REQUEST['year'] = $year[(count($year)-1)];
}
include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."head.all.inc.html";
$all	= SQL::sel( 'id', DB_TABLE_PREFIX.'vote_questions', 'akt = 1'.$seach ,' ORDER BY data DESC ');
if ($all->NumRows > 0) {
	for ($i = 0; $i < $all->NumRows; $i++ ) {
		$all->FetchArray($i);
		$id		= $all->FetchArray['id'];
		if (empty($id)) {
			echo "<H1>�� ������ ������.</H1>";
		}
		vote_load( $id );
	if (isset($_VOTE['conf']['period'])){
		$period = " and data > UNIX_TIMESTAMP()-".$_VOTE['conf']['period'];
	} else {
		$period = "";
	}	
	$is_voted = SQL::getval('remote_addr', DB_TABLE_PREFIX.'vote_remotes', "quest_id = $id and remote_addr = '".$_SERVER['REMOTE_ADDR']."'".$period);
	if ($is_voted == $_SERVER['REMOTE_ADDR']) {
		include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."item.result.inc.phtml";
	} else {
		$all_count	= array_sum($_VOTE['vote']['ans_votes']);
		include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."item.ask.inc.phtml";

	}
		$_VOTE['vote'] = array();
	}
} else {
		echo "����������� �� �������.";
}
include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."foot.all.inc.html";
?> 
