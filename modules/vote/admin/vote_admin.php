<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : vote_admin.php
// PROJECT NAME : VOTINGS
// DATE         : 27.05.2004
// Author       : Andrew Lyadkov (andrey@lyadkov.nnov.ru)
// Discription  : 
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////

/*

global $SQL_DBLINK;

if (empty($SQL_DBLINK))	
	if (!SQL::connect())
		return false;
print_r($SQL_DBLINK);

//CREATIONS:
//----------

$SQL[]	= "CREATE TABLE vote_questions (
id		integer PRIMARY KEY DEFAULT 0,
title	varchar(255),
comment	text,
views	integer UNSIGNED
)
";
$SQL[]	= "CREATE TABLE vote_answers (
id		integer PRIMARY KEY DEFAULT 0 auto_increment,
quest_id	integer UNSIGNED,
title	varchar(255),
votes	integer UNSIGNED
)
";
$SQL[]	= "CREATE TABLE vote_remotes (
id		integer PRIMARY KEY DEFAULT 0 auto_increment,
quest_id	integer UNSIGNED,
remote_addr	varchar(255),
)
";
for ($i = 0; $i < sizeof($SQL); $i++ ) {
	$create	= new Query( $SQL_DBLINK, $SQL[$i]);
	if (!$create->Result) {
		echo $create->ErrorQuery."<br>";
	}
}
*/
global $_VOTE,$_CORE, $SQL_DBLINK;

if (empty($SQL_DBLINK))	
	if (!SQL::connect())
		return false;


$_VOTE['vote'] = $_REQUEST['vote'];
$_VOTE['id'] = $_REQUEST['id'];

$_VOTE['LOADED'] = (!empty($_REQUEST['loaded']));

/* actions */

if ($_VOTE['LOADED'] && !empty($_REQUEST['id']) && !isset($_REQUEST['go']) ) {
    vote_load( $_REQUEST['id'] );
}

if (!$_CORE->IS_DEMO) {
	if (isset($_REQUEST['go']) && !empty($_VOTE['vote'])) {				// ��������
		if (vote_check()) {
			vote_update();
	//		echo "<PRE>";
			$cmd = get_sql();
	//		print_r( $cmd );
			if (vote_doit( $cmd)){
				include $_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME']."/after_edit.html";
            }
		}
	}
}

if (isset($_REQUEST['del']) && !empty($_REQUEST['id'])) {
	if (vote_delete($_REQUEST['id'])){
		include $_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME']."/after_edit.html";
	}
}


# HTML 
if (!empty($_VOTE['LOADED']) || isset($_REQUEST['add'])){
	vote_form();
} else {
vote_header();
if (!empty($_VOTE['ERROR_MESS'])) vote_error($_VOTE['ERROR_MESS']);
if (!empty($_VOTE['MESSAGE'])) vote_message($_VOTE['MESSAGE']);
if (!empty($_VOTE['REPORT_OLD'])) vote_message($_VOTE['REPORT_OLD']);
	vote_list();
}
vote_footer();



/********************************************************************
*/
function vote_delete( $id )
{
	global $_VOTE;
	$res	= SQL::del(DB_TABLE_PREFIX.'vote_questions',"id = '$id'");    
	if (!$res->Result) {
	    $_VOTE['ERROR_MESS']	= "�� ������� ������� ������ $id. ���������� � ��������������.";
		return false;
	}
	$res1	= SQL::del(DB_TABLE_PREFIX.'vote_answers',"quest_id = '$id'");    
	if (!$res1->Result) {
	    $_VOTE['ERROR_MESS']	= "�� ������� ������� ������ $id. ���������� � ��������������.";
		return false;
	}
	$_VOTE['MESSAGE'] =	"������ � ������� $id ������!";
	return true;
} // end func
/********************************************************************
*/
function vote_report( $vote )
{
	$tmp = $vote['title']."<BR>&nbsp;-&nbsp;";
	$tmp .= implode("<BR>&nbsp;-&nbsp;", $vote['ans'])."<BR>--<BR>";
	return $tmp;
} // end func


/********************************************************************
*/
function vote_doit( &$cmd)
{
	global $_VOTE, $id, $SQL_DBLINK;

	foreach ($cmd as $sql) {
		$res	= new Query( $SQL_DBLINK, $sql );
		if (!$res->Result) {
			$_VOTE['MESSAGE']	= '';
		    $_VOTE['ERROR_MESS']	= $res->ErrorQuery;
			return false;
		}
		$_VOTE['MESSAGE']	= '��������� ���������!';
	}
	$_VOTE['vote']		= array();
	$_VOTE['id']		= '';
	$_VOTE['LOADED']	= false;
	return true;
} // end func
/********************************************************************
*/
function get_sql()
{
	global $_VOTE, $REPORT_OLD;
	$vote = $_VOTE['vote'];
//	print_r($vote);
	if (empty($_VOTE['LOADED'])) {						// => add
		$cmd[]	= "INSERT INTO ".DB_TABLE_PREFIX."vote_questions (id, title, comment, akt, data) VALUES ( '".$_VOTE['id']."', '$vote[title]', '$vote[comment]', '$vote[akt]', '$vote[data]') ";
		foreach ($vote['ans'] as $ans) {
			$cmd[]	= "INSERT INTO ".DB_TABLE_PREFIX."vote_answers (quest_id, title) VALUES ( '".$_VOTE['id']."', '$ans' ) ";
		}
	} else {									// => edit
		$cmd[]	= "UPDATE ".DB_TABLE_PREFIX."vote_questions SET title = '$vote[title]', comment= '$vote[comment]', akt = '$vote[akt]', data = '$vote[data]' WHERE id = '".$_VOTE['id']."'";
//		$cmd[]	= "DELETE FROM vote_answers WHERE quest_id = '".$_VOTE['id']."'";
		foreach ($vote['ans'] as $key => $ans) {
			if (!empty($vote['ans_id'][$key])) {
				if (!empty($ans))
					$cmd[]	= "UPDATE ".DB_TABLE_PREFIX."vote_answers SET title = '$ans' WHERE id = '".$vote['ans_id'][$key]."' ";
//	���� �� �������:	else
//					$cmd[]	= "DELETE FROM vote_answers WHERE id = '".$vote['ans_id'][$key]."'";
				unset($vote['ans_id'][$key]);
			}else
				$cmd[]	= "INSERT INTO ".DB_TABLE_PREFIX."vote_answers (quest_id, title) VALUES ( '".$_VOTE['id']."', '$ans') ";
		}
		// ������ ������ 
		if (!empty($vote['ans_id'])) {
			foreach ($vote['ans_id'] as $val) {
					$cmd[]	= "DELETE FROM ".DB_TABLE_PREFIX."vote_answers WHERE id = '".$val."'";
			}
		}
	}
	return $cmd;
} // end func

/********************************************************************
*/
function vote_update()
{
	global $_VOTE;
    $_VOTE['title'] = htmlspecialchars(trim($_VOTE['title']));
    $_VOTE['comment'] = htmlspecialchars(trim($_VOTE['comment']));
    $_VOTE['akt'] = intval(isset($_VOTE['akt']));
	if ($_VOTE['id'] == 0 || $_VOTE['id'] > time()) $_VOTE['id'] = time();
} // end func

/********************************************************************
*/
function vote_check()
{
	global $_VOTE, $id;
	$_VOTE['ERROR']	=	array();
	$id			=	@intval($id);
	if (!isset($_VOTE['vote']['title']) || trim($_VOTE['vote']['title']) == '') {
		$_VOTE['ERROR']['title'] = true;
		$_VOTE['ERROR_MESS']	.= "������ ���������.\n";
	}elseif (!@is_array($_VOTE['vote']['ans'])) {
		$_VOTE['ERROR']['ans'][0] = true;
		$_VOTE['ERROR_MESS']	.= "�� ������� �������� ������.\n";
	}else {
		foreach ($_VOTE['vote']['ans'] as $key => $ans) {
			$_VOTE['vote']['ans'][$key] = trim($ans);
			if ($_VOTE['vote']['ans'][$key] == '')
				unset($_VOTE['vote']['ans'][$key]);
		}
		if (sizeof($_VOTE['vote']['ans']) < 2 ) {
			$_VOTE['ERROR']['ans'][sizeof($vote['ans'])] = true;
			$_VOTE['ERROR_MESS']	.= "������� ������ 2-�.\n";
		}
	}
	if (!empty($_VOTE['ERROR'])) {
	    $_VOTE['ERROR_MESS']	.= "������ �� ������ ��������.";
		return false;
	}
	return true;
} // end func



/********************************************************************
*/
function vote_list()
{
	global $_VOTE;
	if (!empty($_VOTE['vote'])){
		$vote = $_VOTE['vote'];
		$id = $_VOTE['id'];
	}
    $all = SQL::sel('id', DB_TABLE_PREFIX."vote_questions", '', "ORDER BY id ",0);
	for ($i = 0; $i < $all->NumRows; $i++) {
		$all->FetchArray($i);
		$_VOTE['vote'] = array();
		vote_load( $all->FetchArray['id']);
		global $_CORE;
		//include $_CORE->SiteModDir."/admin/vote_admin.list.item.html";
		//include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."vote_admin.list.item.html";
		include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."search.item.html";
	}
	$_VOTE['vote']	= $vote;
	$_VOTE['id']	= $id;
} // end func


/********************************************************************
*/
function vote_form()
{
	global $_VOTE;
	$id = (empty($_VOTE['id'])) ?  time() : $_VOTE['id'];
	$vote = $_VOTE['vote'];
	if (empty($_VOTE['id'])) $vote['akt'] = 1;
	global $_CORE;
	//include $_CORE->SiteModDir."/admin/vote_admin.form.html";
	include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."vote_admin.form.html";
} // end func

/********************************************************************
*/
function vote_header()
{
	global $_CORE,$_VOTE;
	//include $_CORE->SiteModDir."/admin/vote_admin.head.html";
	//include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."vote_admin.head.html";
	include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."search.head.html";
} // end func


/********************************************************************
*/
function vote_footer()
{
	global $_CORE,$_VOTE;
	//include $_CORE->SiteModDir."/admin/vote_admin.foot.html";
	include	$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_VOTE['MODULE_NAME'].'/'."vote_admin.foot.html";
} // end func


/********************************************************************
*/
function vote_error( $str )
{
    echo "<DIV ALIGN=CENTER class=error><B>������!</B> ".nl2br($str)."</DIV>";
} // end func
function vote_message( $str )
{
    echo "<DIV ALIGN=CENTER class=message>".nl2br($str)."</DIV>";
} // end func



?> 