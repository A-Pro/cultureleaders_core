<?
global $_CORE, $_OBSV;
if (!$_CORE->dll_load('class.cForm'))
	die ($_CORE->error_msg());
	
include_once $_CORE->CoreModDir."/class.out.php";
include_once $_CORE->CoreModDir."/class.tools.php";
include_once $_CORE->CoreModDir."/conf/config.php";
$GLOBALS['tools'] = new Tools("filter", "order", "search_obsv");


#
# SWITCHER
#
include_once $_CORE->CoreModDir."/".$_CORE->LANG.".lang.php";
include_once $_CORE->CoreModDir."/obsv.inc.php";
$DEBUG		= 0;

/**
		SWITCHER
**/
switch ($Cmd) {
	case "adm_menu": // 29.05.2006
		global $_PROJECT;
		if (!empty($_OBSV['ADM_MENU']))
			$_PROJECT['ADM_MENU']	= array_merge($_PROJECT['ADM_MENU'], $_OBSV['ADM_MENU']); // doc.inc.php
		break;
  case "install":
     case "install/":
               include_once SRC."/".$_OBSV['main_table'].".cnf.php";
               include_once SRC."/".$_OBSV['forms_table'].".cnf.php";
               include_once SRC."/".$_OBSV['emails_table'].".cnf.php";
               include_once SRC."/".$_OBSV['details_table'].".cnf.php";
							 var_dump($GLOBALS[$_OBSV['main_table']]);
							 $cForm = new cForm($GLOBALS[$_OBSV['main_table']]['form_data']);
               $cForm->createTable($_OBSV['main_table'],1);

							 $cForm = new cForm($GLOBALS[$_OBSV['forms_table']]['form_data']);
               $cForm->createTable($_OBSV['forms_table'],1);

							 $cForm = new cForm($GLOBALS[$_OBSV['emails_table']]['form_data']);
               $cForm->createTable($_OBSV['emails_table'],1);

							 $cForm = new cForm($GLOBALS[$_OBSV['details_table']]['form_data']);
               $cForm->createTable($_OBSV['details_table'],1);
							 SQL::ins($_OBSV['main_table'], '','',1);
          break;

	case 'content/title/short':
	case 'content/title':
		global $_PROJECT;
		$_PROJECT['PATH_2_CONTENT'][0]	= array(
			'name'	=> $_OBSV['MODULE_TITLE'] ,
			'path'	=> '/'.$_OBSV['MODULE_NAME'].'/'
		);
		echo $_CORE->cmd_exec(array('user','/'.$Cmd));
	case 'content/path':
		echo ' ';
		return;
	break;
	case "module/title":
		echo $_OBSV['MODULE_TITLE'];
	break;
	
	case "/emails/add":
	case "emails/add":
		if ($_CORE->IS_ADMIN) {
			$GLOBALS['part'] = "emails";
			$GLOBALS['table_alias'] = $_OBSV['emails_table'];
			$_OBSV['form_name'] = "emails";
			include_once SRC."/".$GLOBALS['table_alias'].".cnf.php";
			include_once SRC_PATH."/add.inc.php";
			if (Main::comm_inc("add.form.html", $file, $_OBSV['MODULE_NAME'])) include $file;
		}
	break;

	case "emails/edit":
	case "/emails/edit":
		if ($_CORE->IS_ADMIN&&isset($_REQUEST['id'])) {
			$GLOBALS['part'] = "emails";
			$_OBSV['form_name'] = "emails";
			$GLOBALS['table_alias'] = $_OBSV['emails_table'];
			include_once SRC."/".$GLOBALS['table_alias'].".cnf.php";
			include_once SRC_PATH."/edit.inc.php";
			if (Main::comm_inc("edit.form.html", $file, $_OBSV['MODULE_NAME'])) include $file;
		}
	break;
	
	case "/emails/":
	case "emails/list":
	case "emails":
		if ($_CORE->IS_ADMIN) {
			$GLOBALS['part'] = "emails";
			$GLOBALS['table_alias'] = $_OBSV['emails_table'];
			include_once SRC."/".$GLOBALS['table_alias'].".cnf.php";
			if (!empty($_REQUEST['delete_id'])) {
				SQL::del($GLOBALS['table_alias'], $GLOBALS[$GLOBALS['table_alias']]['id']." = '".$_REQUEST['delete_id']."' LIMIT 1 ");
			}
			include_once SRC_PATH."/list.inc.php";
			if (Main::comm_inc("list.html", $file, $_OBSV['MODULE_NAME'])) include $file;
		}
	break;

	case "/details/add":
	case "details/add":
		if ($_CORE->IS_ADMIN) {
			$GLOBALS['part'] = "details";
			$GLOBALS['table_alias'] = $_OBSV['details_table'];
			$_OBSV['form_name'] = "Contact Details";
			include_once SRC."/".$GLOBALS['table_alias'].".cnf.php";
			include_once SRC_PATH."/add.inc.php";
			if (Main::comm_inc("add.form.html", $file, $_OBSV['MODULE_NAME'])) include $file;
		}
	break;

	case "details/edit":
	case "/details/edit":
		if ($_CORE->IS_ADMIN&&isset($_REQUEST['id'])) {
			$GLOBALS['part'] = "details";
			$GLOBALS['table_alias'] = $_OBSV['details_table'];
			$_OBSV['form_name'] = "Contact Details";
			include_once SRC."/".$GLOBALS['table_alias'].".cnf.php";
			include_once SRC_PATH."/edit.inc.php";
			if (Main::comm_inc("edit.form.html", $file, $_OBSV['MODULE_NAME'])) include $file;
		}
	break;
	
	case "/details/":
	case "details/list":
	case "details/view":
	case "details":
		if ($_CORE->IS_ADMIN) {
			$GLOBALS['part'] = "details";
			$GLOBALS['table_alias'] = $_OBSV['details_table'];
			include_once SRC."/".$GLOBALS['table_alias'].".cnf.php";
			if (!empty($_REQUEST['delete_id'])) {
				SQL::del($GLOBALS['table_alias'], $GLOBALS[$GLOBALS['table_alias']]['id']." = '".$_REQUEST['delete_id']."' LIMIT 1 ");
			}
			include_once SRC_PATH."/list.inc.php";
			if ($Cmd=="details/view") {
				if (Main::comm_inc("view_list.html", $file, $_OBSV['MODULE_NAME'])) include $file;
			} else {
				if (Main::comm_inc("list.html", $file, $_OBSV['MODULE_NAME'])) include $file;
			}
		}
	break;
	
	case "/main":
	case "main":
		if ($_CORE->IS_ADMIN) {
			$GLOBALS['part'] = "main";
			$GLOBALS['table_alias'] = $_OBSV['main_table'];
			include_once SRC."/".$GLOBALS['table_alias'].".cnf.php";
			$_REQUEST['id'] = 1;
			include_once SRC_PATH."/edit.inc.php";
			if (Main::comm_inc("main.form.html", $file, $_OBSV['MODULE_NAME'])) include $file;
		}
		break;
		
	case "forms/":
	case "forms":
		if ($_CORE->IS_ADMIN) {
			$GLOBALS['part'] = "main";
			$GLOBALS['table_alias'] = $_OBSV['forms_table'];
			if (!empty($_REQUEST['delete_id'])) {
				$res = SQL::sel("`table`", $GLOBALS['table_alias'], "form_id = '".$_REQUEST['delete_id']."'");
				if ($res->NumRows==1) {
					$res->FetchArray(0);
					if (file_exists(SRC_CONF."/".$res->FetchArray['table'].".cnf.php")) {
						unlink(SRC_CONF."/".$res->FetchArray['table'].".cnf.php");
						cForm::dropTable($res->FetchArray['table']);
					}
					SQL::del($GLOBALS['table_alias'], "form_id = '".$_REQUEST['delete_id']."' LIMIT 1 ");
				}
			}
			include_once SRC."/".$_OBSV['forms_table'].".cnf.php";
			include_once SRC_PATH."/list.inc.php";
			if (Main::comm_inc("forms_list.html", $file, $_OBSV['MODULE_NAME'])) include $file;
		}
		break;

	case "configurator":
	case "configurator/":
		if ($_CORE->IS_ADMIN&&!empty($_REQUEST['form_id'])) {
			include_once "class.cnf.php";
			$GLOBALS['part'] = "configurator";
			$res = SQL::sel("`table`, `name`", "_obsv_forms", "`form_id`='".$_REQUEST['form_id']."'");
			$res->FetchArray(0);
			$_OBSV['table_alias']	= $res->FetchArray['table'];
			$_OBSV['form_name']		= $res->FetchArray['name'];
			if (file_exists(SRC_CONF."/".$_OBSV['table_alias'].".cnf.php")) {
				include_once SRC_CONF."/".$_OBSV['table_alias'].".cnf.php";
			} else unset($_SESSION['obsv']);
			include_once SRC_PATH."/configurator.inc.php";
			if (Main::comm_inc("configurator.html", $file, $_OBSV['MODULE_NAME'])) include $file;
		}
	break;

	case "configurator/view":
	case "configurator/view/":
		if ($_CORE->IS_ADMIN&&!empty($_REQUEST['form_id'])) {
			include_once "class.cnf.php";
			$GLOBALS['part'] = "configurator";
			$res = SQL::sel("`table`, `name`", "_obsv_forms", "`form_id`='".$_REQUEST['form_id']."'");
			$res->FetchArray(0);
			$_OBSV['csv'] = 'csv';
			$_OBSV['table_alias']	= $res->FetchArray['table'];
			$GLOBALS['table_alias']	= $_OBSV['table_alias'];
			$_OBSV['form_name']		= $res->FetchArray['name'];
			if (file_exists(SRC_CONF."/".$_OBSV['table_alias'].".cnf.php")) {
				include_once SRC_CONF."/".$_OBSV['table_alias'].".cnf.php";
			} else unset($_SESSION['obsv']);
			include_once SRC_PATH."/list.inc.php";
			if (Main::comm_inc("view_list.html", $file, $_OBSV['MODULE_NAME'])) include $file;
		}
	break;

	case "forms/add":
	case "forms/add/":
		if ($_CORE->IS_ADMIN) {
			$GLOBALS['table_alias'] = $_OBSV['forms_table'];
			$_OBSV['form_name'] 	= "new form";
			include_once SRC."/".$GLOBALS['table_alias'].".cnf.php";
			if (!empty($_POST['form']['name']))
				{	$GLOBALS['form']['table'] = "_obsv__".str_replace(" ", "_", $_POST['form']['name']);
					$_POST['form']['table'] = "_obsv__".str_replace(" ", "_", $_POST['form']['name']);
			} else $GLOBALS[$_OBSV['forms_table']]['form_data']['table']['type'] = 'hidden';
			include_once SRC_PATH."/add.inc.php";
			if (Main::comm_inc("add.form.html", $file, $_OBSV['MODULE_NAME'])) include $file;
		}
		break;

	case "configurator/add":
	case "configurator/add/":
		if ($_CORE->IS_ADMIN&&!empty($_REQUEST['form_id'])) {
			include_once "class.cnf.php";
			$GLOBALS['part'] = "configurator";
			$res = SQL::sel("`table`, `name`", "_obsv_forms", "`form_id`='".$_REQUEST['form_id']."'");
			$res->FetchArray(0);
			$_OBSV['table_alias']	= $res->FetchArray['table'];
			$_OBSV['form_name']		= $res->FetchArray['name'];
			include_once SRC_PATH."/conf_add.inc.php";
			if (Main::comm_inc("conf_add.form.html", $file, $_OBSV['MODULE_NAME'])) include $file;
		}
	break;

	case "preview":
	case "preview/":
		if ($_CORE->IS_ADMIN&&!empty($_REQUEST['form_id'])) {
			$res = SQL::sel("`contactus`, `details`, `position`", $_OBSV['main_table'], "id='1'");
			$res->FetchArray(0);
			if ($res->FetchArray['contactus']=='1') {
				$_OBSV['show_det']	= $res->FetchArray['details'];
				$_OBSV['det_pos']	= $res->FetchArray['position'];
				$res = SQL::sel("`table`, `name`", "_obsv_forms", "`form_id`='".$_REQUEST['form_id']."'");
				$res->FetchArray(0);
				$_OBSV['table_alias']	= $res->FetchArray['table'];
				$_OBSV['form_name']		= $res->FetchArray['name'];
				if ($Cmd=='preview'||$Cmd=='preview/') $_OBSV['preview']	= 'preview';
				if (file_exists(SRC_CONF."/".$_OBSV['table_alias'].".cnf.php")) {
					include_once SRC_CONF."/".$_OBSV['table_alias'].".cnf.php";
				}
				include_once SRC_PATH."/preview.inc.php";
				if (Main::comm_inc("view.form.html", $file, $_OBSV['MODULE_NAME'])) include $file;
			}
		}
	break;

	case "sending":
			echo "mail send";
		break;
	
	case "capture":
	case "capture/":
			$res = SQL::sel("`form_id`, `capture`", $_OBSV['main_table'], "id='1'");
			$tmp = $res->FetchArray(0);
			if ($res->FetchArray['capture']=='1') {
				$_REQUEST['form_id'] = !empty($_REQUEST['form_id'])?$_REQUEST['form_id']:$res->FetchArray['form_id'];
				$res = SQL::sel("`table`, `name`", $_OBSV['forms_table'], "`form_id`='".$_REQUEST['form_id']."'");
				$res->FetchArray(0);
				$_OBSV['table_alias']	= $res->FetchArray['table'];
				$GLOBALS['table_alias'] = $_OBSV['table_alias'];
				$_OBSV['form_name']		= $res->FetchArray['name'];
				if (file_exists(SRC_CONF."/".$_OBSV['table_alias'].".cnf.php")) {
					include_once SRC_CONF."/".$_OBSV['table_alias'].".cnf.php";
					include_once SRC_PATH."/csv.inc.php";
				}
			}
	break;
		
	case "form":
	case "":
	case "/":
			$res = SQL::sel("`contactus`, `details`, `position`, `form_id`", $_OBSV['main_table'], "id='1'");
			$res->FetchArray(0);
			if ($res->FetchArray['contactus']=='1') {
				$_OBSV['show_det']	= $res->FetchArray['details'];
				$_OBSV['det_pos']	= $res->FetchArray['position'];
				$_REQUEST['form_id'] = $res->FetchArray['form_id'];
				$res = SQL::sel("`table`, `name`", "_obsv_forms", "`form_id`='".$res->FetchArray['form_id']."'");
				$res->FetchArray(0);
				$_OBSV['table_alias']	= $res->FetchArray['table'];
				$_OBSV['form_name']		= $res->FetchArray['name'];
				if (file_exists(SRC_CONF."/".$_OBSV['table_alias'].".cnf.php")) {
					include_once SRC_CONF."/".$_OBSV['table_alias'].".cnf.php";
					require_once SRC_PATH."/send.inc.php";
					include_once SRC_PATH."/preview.inc.php";
					if (Main::comm_inc("view.form.html", $file, $_OBSV['MODULE_NAME'])) include $file;
				}
			}
	break;
		
}


?>