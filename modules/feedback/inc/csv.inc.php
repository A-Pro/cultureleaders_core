<?php
	// Returns array $out
//	ob_end_clean();
	global $_CORE;
	Tools::getStdOut($out, SQL::sel('*', $_OBSV['table_alias']));
	$path = $_CORE->SiteModDir."/tmp";
	$uniq = md5(rand(100,999)*time());
	$filename = $_OBSV['form_name'].'_'.date('Y-m-d_h-i');
	$tmp_filename = $filename.'_'.$uniq.'.csv';

	$fp = fopen($path.'/'.$tmp_filename, "w");
	// Export $out to CSV file
	$string = "";
	$delimiter = ";";
	$enclosure = "";

	
	foreach($GLOBALS[$_OBSV['table_alias']]['form_data'] as $k=>$field) if ($k!='id') {
		$k = str_replace("\"", "\"\"", $k);
		$string .= $enclosure.$k.$enclosure.$delimiter;
	}
   $string .= "\n";
   
	// for each array element, which represents a line in the csv file...
	if (is_array($out)) {
		foreach($out as $line){
		
			foreach($line as $k=>$field) if (isset($GLOBALS[$_OBSV['table_alias']]['form_data'][$k])&&$k!='id'){
			   $field = str_replace("\"", "\"\"", $field);
			   if ($GLOBALS[$_OBSV['table_alias']]['form_data'][$k]['type']=="select") $field = $GLOBALS[$_OBSV['table_alias']]['form_data'][$k]['arr'][$field];
			   $string .= $enclosure.$field.$enclosure.$delimiter;
			}
		   // Append new line
		   $string .= "\n";
		}
	} else $string = "empty";
	fwrite($fp, $string);
	fclose($fp);

	if (file_exists($path.'/'.$tmp_filename)) {
		header("Content-Disposition: attachment; filename=".$filename.".csv");
		header("Content-Type: application/force-download");
		header("Content-Transfer-Encoding: binary");
		if (readfile($path.'/'.$tmp_filename)) {
			unlink($path.'/'.$tmp_filename);
		}
		exit;
	}
		
?>