<? 
$field_alias 			 = @$_GET['f'];


if (!empty($_POST['fields']))	$_SESSION['obsv']['table_info']['form_data'][$field_alias] = $_POST['fields'];

$_SESSION['field_params'] = $_SESSION['obsv']['table_info']['form_data'][$field_alias];
$_SESSION['field_params']['type'] = (isset($_POST['fields']['type']))?$_POST['fields']['type']:$_SESSION['field_params']['type'];

// Save data
if (isset($_POST['save'])&&($_POST['save']=='SAVE')) {
	
	if (!empty($field_alias)&&!empty($_POST['fields']['field_name'])) {
		
		$_POST['fields']['name'] = "form[".$_POST['fields']['field_name']."]";
		if (empty($_POST['fields']['title']))	$_POST['fields']['title'] = $_POST['fields']['field_name'];
		unset($_SESSION['configurator']['table_info']['form_data'][$field_alias]);
		
		$_SESSION['configurator']['table_info']['form_data'][$_POST['fields']['field_name']] = $_POST['fields'];

		if (Cnf::getFormDataParams($_SESSION['obsv']['table_info']['form_data'][$_POST['fields']['field_name']],$_SESSION['field_params']['type'])) {	
			Cnf::editField($_SESSION['configurator']['current_table'], $field_alias, $_POST['fields']['field_name']);
			echo "<script>window.opener.location.href=window.opener.location.href; self.close();</script>";
			exit;
		}
	}	
}

$vars['types_list'] = Cnf::getFieldType($_OBSV['types_array'], $_SESSION['field_params']['type']);
$vars['type_params'] = Cnf::getTypeParams($field_alias, $_SESSION['field_params']['type']);



?>