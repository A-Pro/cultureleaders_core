<?
global $cform_send;
$cform_send = new cForm($GLOBALS[$_OBSV['table_alias']]['form_data'] );

if (isset($_POST['form'])&&!$cform_send -> Chk_come()) {
	$tmp = $cform_send->GetSQL();
	SQL::ins($_OBSV['table_alias'],$tmp[0],$tmp[1]);
	$data['body'] = OBSV::getData($_POST['form'], $GLOBALS[$_OBSV['table_alias']]);
	$res = SQL::sel("`recipient`, `email`", $_OBSV['emails_table']);
	for ($i=0; $i<$res->NumRows; $i++) {
		$res->FetchArray($i);
		$data['name']	= $res->FetchArray['recipient'];
		$data['email']	= $res->FetchArray['email'];
		OBSV::send_new($data);
	}
	ob_end_clean();
	echo "<script>location.href='/obsv/sending';</script>";
	exit;
} else 
if (!empty($cform_send->Error))
	echo "<hr>".$cform_send->Error."<hr>";
?>