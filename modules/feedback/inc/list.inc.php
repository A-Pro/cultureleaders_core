<?
global $tools;

$table_alias = $GLOBALS['table_alias'];
$table_sql_alias = $table_alias;

$from = $table_alias.' ';
if (is_array($GLOBALS[$GLOBALS['table_alias']]['form_data'])) {
	foreach ($GLOBALS[$GLOBALS['table_alias']]['form_data'] as $field_name => $field) {
		if (in_array($field['type'],array('select_popup', 'select_from_tree', 'select_from_table'))) {
			$tables[] = 'LEFT JOIN '.$field['ex_table'].' ON '.$table_sql_alias.'.'.$field['field_name'].' = '.$field['ex_table'].'.'.$field['id_ex_table'];
			$fields[] = $field['ex_table'].'.'.$field['ex_table_field'].' as '.$field['field_name'];
		} else
			$fields[] = '`'.$field['field_name'].'`';
	}

$from .= !empty($tables)?implode(' ', $tables):'';
$res_fields = !empty($fields)?implode(', ',$fields):'';


// Order. Generating "filter-tail".
$vars['key_filter'] = $tools->getFilterURL();

if ($_POST['search']) $tools -> clearSearchSession();
$where = $tools->getFSQuery($GLOBALS[$GLOBALS['table_alias']]['form_data'],$table_sql_alias);

$order = $tools->getOrderQuery($GLOBALS[$GLOBALS['table_alias']]['form_data'], $table_sql_alias);
if (empty($order)) $order = "ORDER BY ".$table_sql_alias.".".$GLOBALS[$GLOBALS['table_alias']]['id'];
$of_li = $tools->getOfLi();

$GLOBALS['pline'] = $tools->getPline($table_alias, $where);
// Endoff.

$tools->getStdOut($out, SQL::sel($res_fields, $from, $where, $order.' '.$of_li));

if (is_array($out)) {
	foreach ($out as $k=>$v) {
		foreach ($GLOBALS[$GLOBALS['table_alias']]['form_data'] as $field_name => $field) {
			if ($field['type']=='select'&&isset($out[$k][$field_name])) {
				$out[$k][$field_name] = $field['arr'][$out[$k][$field_name]];
			}
		}
	}
} else echo "empty";
}
?>