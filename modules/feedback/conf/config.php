<?
global $_CORE, $_OBSV;

define('INC', 'inc');

define(SRC, $_CORE->CoreModDir.'/conf/');
define(SRC_PATH, "inc");
define(SRC_CONF, $_CORE->SiteModDir."/conf");

$_OBSV['MODULE_NAME']	= "obsv";
$_OBSV['main_table']	= "_obsv_main";
$_OBSV['forms_table']	= "_obsv_forms";
$_OBSV['emails_table']	= "_obsv_emails";
$_OBSV['details_table']	= "_obsv_contact_details";


//Configurator add
$_OBSV['types_array'] = array('    ', 'TextBox', 'OptionBox', 'CheckBox');
$_OBSV['see'] = array('title', 'Type', 'extra_size', 'Section', 'Content Type');
$_OBSV['TextBox']['Content Type'] = array("Email", "Numbers Only", "Free Text");


//Configurator
$_OBSV['aliases'] = array(
	'title'	=> 'Name',
	'Type'	=> 'Type',
	'extra_size'	=> 'Size',
	'Section'		=> 'Section',
	'Content Type'	=> 'Content Type',
);
?>