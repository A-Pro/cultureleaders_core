<?php
$GLOBALS['_obsv_emails'] = array (
	"form_data" => array(
	  'email_id' => 
	  array (
	    'field_name' => 'email_id',
	    'name' => 'form[email_id]',
	    'title' => 'email_id',
	    'must' => 0,
	    'data_type' => 'bigint(20) unsigned',
	    'primary_key' => true,
	    'type' => 'hidden',
	  ),
	  'recipient' => 
	  array (
	    'field_name' => 'recipient',
	    'name' => 'form[recipient]',
	    'title' => 'recipient',
	    'must' => 1,
	    'data_type' => 'varchar(255)',
	    'size' => 50,
	    'maxlen' => 255,
	    'type' => 'textbox',
	  ),
	  'email' => 
	  array (
	    'field_name' => 'email',
	    'name' => 'form[email]',
	    'title' => 'email',
	    'must' => 1,
	    'data_type' => 'varchar(255)',
	    'size' => 50,
	    'maxlen' => 255,
	    'type' => 'textbox',
	  ),
	  'order' => 
	  array (
	    'field_name' => 'order',
	    'name' => 'form[order]',
	    'title' => 'order',
	    'must' => 0,
	    'type' => 'hidden',
	    'in_list' => false,
	    'data_type' => 'int(10) unsigned',
	  ),
	),
	
	"id"		=> "email_id",
	'onpage'	=> 20,
);
//save by  Configurator on 2006-08-14 15:39:58 ?>
