<?php
$GLOBALS['_obsv_main'] = array (
"form_data" => array(
  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'data_type' => 'bigint(20) unsigned',
    'primary_key' => true,
    'type' => 'hidden',
  ),
  'form_id' => 
  array (
    'field_name' => 'form_id',
    'name' => 'form[form_id]',
    'title' => 'SelectForm',
    'must' => 0,
    'data_type' => 'bigint(20) unsigned',
    'maxlen' => 20,
    'type' => 'select_from_table',
    'ex_table' => '_obsv_forms',
    'id_ex_table' => 'form_id',
    'ex_table_field' => 'name',
  ),
  'details' => 
  array (
    'field_name' => 'details',
    'name' => 'form[details]',
    'title' => 'Display Contact Details',
    'must' => 0,
    'type' => 'checkbox',
    'data_type' => 'tinyint(3) unsigned',
  ),
  'position' => 
  array (
    'field_name' => 'position',
    'name' => 'form[position]',
    'title' => 'Above/Below',
    'must' => 0,
    'type' => 'checkbox',
    'data_type' => 'tinyint(3) unsigned',
  ),
  'capture' => 
  array (
    'field_name' => 'capture',
    'name' => 'form[capture]',
    'title' => 'Capture Data Online',
    'must' => 0,
    'type' => 'checkbox',
    'data_type' => 'tinyint(3) unsigned',
  ),
  'contactus' => 
  array (
    'field_name' => 'contactus',
    'name' => 'form[contactus]',
    'title' => 'Enable Contact Us Form',
    'must' => 0,
    'type' => 'checkbox',
  ),
),
"id" => "id",

);
//save by  Configurator on 2006-08-14 18:53:00 ?>
