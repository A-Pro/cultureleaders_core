<?php
include_once "class.out.php";
/**
 * Standart class, for debuging other classes, and projects.
 *
 */
class Error{
	var $error;
	
	function Error(){ $error = ''; }
	
	/**
	 * Add $_error to error-string. Delimeter - "\n".
	 *
	 * @param string $_error
	 */
	function setError( $_error )
	{
			$this->error .= $_error."\n";
	}

	/**
	 * Return error-string. Clear error-string.
	 *
	 * @return unknown
	 */
	function getError()
	{
			$tmp = $this->error; 
			$this->error = '';
			return $tmp;
	}

	/**
	 * Dumping $var and die().
	 *
	 * @param mixed $var
	 * @param string $mess
	 */
	function bp(&$var,$mess='')
	{ 
		?><font style="font: normal 14px Tahoma, Verdana, sans-serif; color: red"><xmp><?
	var_dump($var);
	?></xmp></font><? 
	die($mess);
	}

  
	/**
	 * Return alert-message to user.
	 *
	 * @param string $mess
	 * @param string $isdie
	 * @param string $extra
	 */
	function alert($mess='', $isdie = true, $extra = '')
	{ 
		echo $extra;
		if ($isdie) die($mess);
		else echo $mess;		
	}
	
	/**
	 * Return error-string to user and die.
	 *
	 */
	function getErrorOut()
	  {
	  	?><xmp><?
	    var_dump($this->error);
	    ?></xmp><? 
	    die( 'Debug' );
	  }

	/**
	 * Dumping $var.
	 *
	 * @param mixed $var
	 */
	function vb(&$var)
	{ 
		?><font style="font: normal 14px Tahoma, Verdana, sans-serif; color: green"><xmp><?
	var_dump($var);
	?></xmp></font><? 
	}
};

class Tools extends Error {
	var $f, $o, $s, $ss;
	
	/**
	 * Construstor
	 *
	 * @param string $_f
	 * @param string $_o
	 * @param string $_s
	 * @return Tools
	 */
	function Tools($_f='filter', $_o='order', $_s='search') { 
		$this->f = $_f; 
		$this->o = $_o; 
		$this->s = $_s;
		$this->ss= $_s."_session";
	}
	
	//////////////////////////
	// Filtration functions //
	//////////////////////////
	
	/**
	 * Returns TRUE if the field with $field['field_name'] is filtered field, FALSE if not
	 *
	 * @param array $field
	 * @return boolean
	 */
	function isFiltered(&$field) {
		return isset($_REQUEST[$this->f][$field['field_name']]);
	}

	/**
	 * Returns a value of a filtering variable if the field is filtered, empty string otherwise
	 *
	 * @param array $field
	 * @return string
	 */
	function getFilter(&$field) {
		return ($this->isFiltered($field))?($_REQUEST[$this->f][$field['field_name']]):'';
	}
	/**
	 * This function is similar to isFiltered with the difference that it has string parameter
	 * Returns TRUE if the field is filtered, FALSE if not. 
	 *
	 * @param string $field_name
	 * @return boolean
	 */
	function isFilteredByName($field_name) {
		return isset($_REQUEST[$this->f][$field_name]);
	}
	
	/**
	 * This function is similar to getFilter with the difference that it has string parameter	
	 * Returns a value of a filtering variable if the field is filtered, NULL if not. 
	 *
	 * @param string $field_name
	 * @return string
	 */
	function getFilterByName($field_name) {
		return @$_REQUEST[$this->f][$field_name];
	}
	
	/**
	 * Unset filtering variable
	 *
	 * @param string $field_name
	 */
	function deleteFilter($field_name) {
		unset($_REQUEST[$this->f][$field_name]);
	}
	
	/**
	 * Prepare and returns filtered "WHERE" clause for SQL query
	 * The optional parameter $ta allow to set table alias for SQL query
	 * @param array $fd
	 * @param string $ta
	 * @return string
	 */
	function getFilterQuery(&$fd, $ta='') {
		$where = '';
		if (!empty($ta)) { 
			$ta.=".";
		}
		foreach ($fd as $field_name => $field) {
			if ($this->isFiltered($field)) {
				$where[] = "(".$ta.$field_name."='".$this->getFilter($field)."')";
			}
		}
		return !empty($where)?implode(' AND ',$where):'';
	}
	
	/**
	 * Returns "filter-tail" string
	 *
	 * @return string
	 */
	function getFilterURL() {
		$res = '';
		if (is_array($_REQUEST[$this->f])) {
			foreach( $_REQUEST[$this->f] as $k=>$v) if (!empty($v)) {
				$res .= "&".$this->f."[".$k."]=".$v;
			}
		}
		return $res;
	}



	///////////////////////
	// Sorting functions //
	///////////////////////
	
	/**
	 * Returns TRUE if the field with $field_name is sorting field, FALSE if not
	 *
	 * @param string $field_name
	 * @return boolean
	 */
	function isOrdered($field_name) {
		return $_REQUEST[$this->o]['field']==$field_name;
	}
	
	/**
	 * Returns TRUE if the sorting order is DESC, FALSE if not
	 *
	 * @return boolean
	 */
	function isDesced() {
		return isset($_REQUEST[$this->o]['desc'])&&($_REQUEST[$this->o]['desc']=='desc');
	}
	
	function getOrder() {
		return @$_REQUEST[$this->o]['field'];
	}
	/**
	 * Prepare and returns "ORDER BY" clause for SQL query.
	 * The optional parameter $ta allow to set table alias for SQL query
	 * The optional parameter $no_requsted allow to skip some check-up
	 * @param array $fd
	 * @param string $ta
	 * @param int $nb
	 * @return string
	 */
	function getOrderQuery (&$fd, $ta='', $no_requsted=0) {
	    $order = '';
	    if (!empty($ta)) { 
	    	$ta .= ".";
	    }
		if (isset($_REQUEST[$this->o]['field'])) {
			if ($no_requsted) {	
				$order = 'ORDER BY '.$ta.$_REQUEST[$this->o]['field'];
				$_SERVER['REQUEST_URI']	= str_replace("&".$this->o."[field]=".$_REQUEST[$this->o]['field'], "", $_SERVER['REQUEST_URI']);
				$_SERVER['REQUEST_URI']	= str_replace("&".$this->o."[desc]=desc", "", $_SERVER['REQUEST_URI']);
				if ($this->isDesced()) {
					$order .=' DESC';
				}
				return $order;
			}
			foreach ($fd as $field) {
				if ($this->isOrdered($field['field_name']))	{
					$order = 'ORDER BY '.$ta.$field['field_name'];
					$_SERVER['REQUEST_URI']	= str_replace( "&".$this->o."[field]=".$field['field_name'], "", $_SERVER['REQUEST_URI']);
					$_SERVER['REQUEST_URI']	= str_replace( "&".$this->o."[desc]=desc", "", $_SERVER['REQUEST_URI']);
					if ($this->isDesced()) { 
						$order .=' DESC'; 
					}
					break;
				}
			}
		}	
		return $order;
	}

	/**
	 * Generate "order-tail" string.
	 * The optional parameter $r allow to change request URI
	 * 
	 * @param string $field_name
	 * @param string_type $r
	 * @return string
	 */
	function getOrderURL ($field_name, $r='') {
		if (empty($r)) {
			$r = $_SERVER['REQUEST_URI'];
		}
		$sym = (strpos($_SERVER['REQUEST_URI'], "?")>0)?"&":"?";
		$r = $r.$sym.$this->o."[field]=".$field_name;
		if ($this->isOrdered($field_name) && !isset($_REQUEST[$this->o]['desc'])) { 
			$r .= "&order[desc]=desc";		
		}
		return $r;
	}


	///////////////////////////////
	// Page navigation functions //
	///////////////////////////////
	/**
	 * On default returns a first page number
	 *
	 * @param int $def_first
	 * @return string
	 */
	function getFirstPage ($def_first=1) {
		return !empty($_REQUEST['p'])?$_REQUEST['p']:$def_first;	
	}
	
	/**
	 * Get record count on page
	 *
	 * @return string
	 */
	function getOnpage() {
		return !empty($_REQUEST['onpage'])?$_REQUEST['onpage']:(!empty($GLOBALS[$GLOBALS['table_alias']]['onpage'])?$GLOBALS[$GLOBALS['table_alias']]['onpage']:20);
	}
	
	/**
	 * Returns html-code of "page line"
	 * The optional parameter $what allow to set custom clause for SQL query
	 * @param string $from
	 * @param string $where
	 * @param string $what
	 * @return string
	 */
	function getPline($from, $where, $what='count(*)') {
		$all = SQL::getval($what, $from,	$where, '');
		return Out::pline($all, $this->getFirstPage(), $this->getOnpage());
	}
	
	/**
	 * Set range for "ORDER BY" clause for SQL query
	 *
	 * @return string
	 */
	function getOfLi() {
		return SQL::of_li(($this->getFirstPage()-1)*$this->getOnpage(), $this->getOnpage());
	}

	
	/////////////////////////
	// Searching functions //
	/////////////////////////

	/**
	 * Returns TRUE if the field with $field['field_name'] is searching field, FALSE if not
	 *
	 * @param array $field
	 * @return boolean
	 */
	function isSearched(&$field) {
		if (isset($_REQUEST[$this->s][$field['field_name']])&&!empty($_REQUEST[$this->s][$field['field_name']])) {
			$_SESSION[$this->ss][$field['field_name']] = $_REQUEST[$this->s][$field['field_name']];
			return true;
		} elseif (isset($_SESSION[$this->ss][$field['field_name']])&&!empty($_SESSION[$this->ss][$field['field_name']])) {
			return true;
		}
		return false;
	}
	
	/**
	 * This function returns a string with all occurences of "*" in search query 
	 * replaced with replace "%"
	 *
	 * @param array $field
	 * @return string
	 */
	function getSearch(&$field) {
		if ($this->isSearched($field)) {
			return !empty($_REQUEST[$this->s][$field['field_name']])?$_REQUEST[$this->s][$field['field_name']]:$_SESSION[$this->ss][$field['field_name']];
		}
		return false;
	}
	
	/**
	 * Returns TRUE if the field with $field_name is searching field, FALSE if not
	 *
	 * @param string $field_name
	 * @return boolean
	 */
	function isSearchedByName($field_name) {
		if (isset($_REQUEST[$this->s][$field_name])&&!empty($_REQUEST[$this->s][$field_name])) {
			$_SESSION[$this->ss][$field_name] = $_REQUEST[$this->s][$field_name];
			return true;
		} elseif (isset($_SESSION[$this->ss][$field_name])&&!empty($_SESSION[$this->ss][$field_name])) {
			return true;
		}
		return false;
	}
	
	/**
	 * This function is similar to isFiltered with the difference that it has string parameter.
	 * Returns a string with all occurences of "*" in search query 
	 * replaced with replace "%"
	 *
	 * @param string $field_name
	 * @return string
	 */
	function getSearchByName($field_name) {
		if ($this->isSearchedByName($field_name)) {
			return !empty($_REQUEST[$this->s][$field_name])?$_REQUEST[$this->s][$field_name]:$_SESSION[$this->ss][$field_name];
		}
		return false;
	}
	
	/**
	 * Returns "WHERE" clause for SQL query.
	 * The optional parameter $ta allow to set table alias
	 * 
	 * @param array $fd
	 * @param string $ta
	 * @return string
	 */
	function getSearchQuery(&$fd, $ta='') {
		$where = '';
		if (!empty($ta)) { 
			$ta .= ".";
		}
		foreach ($fd as $field_name => $field) {
			if ($this->isSearched($field)) {
					$where[] = "(".$ta.$field_name." LIKE '".str_replace("*", "%", $this->getSearch($field))."%')";
			}
		}
		return !empty($where)?implode(' AND ',$where):'';
	}

	/**
	 * 
	 */
	function clearSearchSession() {
		unset($_SESSION[$this->ss]);
	}
	
	/**
	 * Returns "search-tail" string
	 *
	 * @return string
	 */
	function getSearchURL() {
		$res = '';
		if (is_array($_REQUEST[$this->s])) {
			foreach( $_REQUEST[$this->s] as $k=>$v) if (!empty($v)) {
				$res .= "&".$this->s."[".$k."]=".$v;
			}
		}
		return $res;
	}

	/**
	 * Returns specific "WHERE" clause for SQL query.
	 * The optional parameter $ta allow to set table alias
	 *
	 * @param array $fd
	 * @param string $ta
	 * @return string
	 */
	function getFSQuery(&$fd, $ta='') {
		$fw = $this->getFilterQuery($fd, $ta);
		$sw = $this->getSearchQuery($fd, $ta);
		return !empty($fw)&&!empty($sw)?$fw." AND ".$sw:$fw.$sw;
	}

	/**
	 * 
	 * The optional parameter $ta allow to set table alias
	 *
	 * @param array $fd
	 * @param string $ta
	 * @return string
	 */
	function getFSURL() {
		$fw = $this->getFilterURL();
		$sw = $this->getSearchURL();
		return $fw.$sw;
	}

	/**
	 * Returns "standart" output array. 
	 * The optional parameter $out_var allow to create subarray like $out[$$out_var]
	 *
	 * @param array $out
	 * @param array $res
	 * @param string $out_var
	 */
	function getStdOut(&$out, &$res, $out_var='') {
		if ($res->NumRows > 0) {
			for ($i = 0; $i < $res->NumRows; $i++ ){
				$res->FetchArray($i,1);
				if (!empty($out_var)) {
					$out[$out_var][$i] = $res->FetchArray;
				} else {
					$out[$i] = $res->FetchArray;
				}
			}
		}
	}
	
	
	function changeStockStatus($stock_id, $status, $category) {
		global $_ALIASES;
		if (!empty($status)&&!empty($category)&&!empty($stock_id)) {
			$category_id = SQL::getval("category_id", 'status_categories', "name='".$category."'", '', 1 );
			if (!empty($status)&&!empty($category_id)) {
				$status_id = SQL::getval("status_id", 'statuses', "name='".$status."'", '', 1);
				if (!empty($status_id)) {
					SQL::upd( 'stock', "category_id='".$category_id."', status_id='".$status_id."'", "stock_id = ".$stock_id, 1);
				}
			}
		}
	}
}

?>