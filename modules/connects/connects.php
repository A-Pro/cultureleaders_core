<?
/*
�������:

������������ � �������:
 - �������� ��� ������ � DOCTXT,DB 
 - ���������� ������� ������ KAT::  ?
 - ������ � ���� ( path, link )
 - ���������� �������� ������, �� ��������� php-����: links.inc.phtml 

1. �������� ����� � �������� (�� ���� path �� ����� html-list)
2. �������������� ����� � �������� (�� ����� path)


�������: 

seealso/_path_ 				- ��������, � ����������� �� ������� ����� ����
�������: /templates/def/connects/seealso.head.html
�������: /templates/def/connects/seealso.item.html
�������: /templates/def/connects/seealso.foot.html

edit/_path_					- ���� �������������� ������ � ������ ��������������
�������: /templates/admin/connects/seealso.head.html
�������: /templates/admin/connects/seealso.item.html
�������: /templates/admin/connects/seealso.foot.html
�������: /templates/admin/connects/seealso.footer.html

del 						- �������� �����
������� GET ���������� ��� ���������: $path, $link

trunc						- �������� ���� ������

install						- �������� ��� ���������� ������ CONNECTS

*/

global $_CORE, $_CONNECTS;
include_once $_CORE->CoreModDir.'/connects.inc.php';

//echo $Cmd."<br>";
$node = empty($_GET['node'])?"/":$_GET['node'];
$path = empty($_GET['path'])?"":$_GET['path'];
$action = empty($_POST['action'])?"none":$_POST['action'];
if( strpos( $Cmd, '/' ) )
{	$tmppath = substr( $Cmd, strpos( $Cmd, '/' )+1 );
	$Cmd	 = substr( $Cmd, 0, strpos( $Cmd, '/' ) );	
}

if($Cmd == 'seealso' || $Cmd == 'edit' || $Cmd == 'seealso_all') $path = str_replace(":", "/", $tmppath);
switch ($Cmd){
		
	case 'del':
	if( $_CORE->IS_ADMIN ){
		$link = empty($_GET['link'])?"":$_GET['link'];	
		CONNECTS::edit_link( $path , $link, 'del');
		ob_end_clean();
		header('Location: http://'.$_SERVER['SERVER_NAME'].CONNECTS::from_where($path));
		exit;
	}	break;

		
	case 'process':
	if( $_CORE->IS_ADMIN ){
		if( $action == 'reset' ){ $add=''; $del =''; }
		if(( $action =='close' )||( $action=='apply' ))
		{	$add = empty($_POST['add'])?"":$_POST['add'];
			$del = empty($_POST['del'])?"":$_POST['del'];
			$order = empty($_POST['order'])?"":$_POST['order'];
			$del_array = explode( ";", $del);
			array_pop($del_array);
			$add_array = explode( ";", $add);
			array_pop($add_array);
			$order_array = explode( ";", $order);
			array_pop($order_array);
			
			if ( is_array( $add_array ) ){
					foreach( $add_array as $link)
						CONNECTS::edit_link( $path, $link,  'add');
			}		
			if ( is_array( $del_array ) ){
					foreach( $del_array as $link)
						CONNECTS::edit_link( $path, $link,  'del');
			}		
			if ( is_array( $order_array ) ){
				$i=0;
					foreach( $order_array as $link)
						CONNECTS::set_prior( $path, $link, $i++);
			}		
			$add = '';
			$del = '';
			$order = '';
		}
		if (Main::comm_inc('links.inc.phtml', $f, $_CONNECTS['MODULE_NAME']))include "$f";
	}	break;
		
	case "seeall":
		if( $_CORE->IS_ADMIN )	CONNECTS::see_all();
	break;

	case "concerned":
		global $_KAT;
		if( !empty($_KAT['KUR_ALIAS']) ) $tmp_kur_alias = $_KAT['KUR_ALIAS'];
		CONNECTS::load_paths($path, $_CONNECTS['links']);
        
		if( !empty( $_CONNECTS['links'] ) ){
			// �����
			if (Main::comm_inc('seealso.head.html', $f, $_CONNECTS['MODULE_NAME']))
				include "$f";
			// �� ���������
			if( is_array($_CONNECTS['links']) ){
				foreach ($_CONNECTS['links'] as $k=>$w){
					$v['path'] = $k;
					$v['name'] = $w['name'];
					$v['doc']	 = $w['doc'];
					if (Main::comm_inc('seealso.item.html', $f, $_CONNECTS['MODULE_NAME']))include "$f";
				}
			}
			// �����
			if (Main::comm_inc('seealso.foot.html', $f, $_CONNECTS['MODULE_NAME']))include "$f";
		unset( $_CONNECTS['links'] );
		}				
	if( !empty( $tmp_kur_alias ) )  $_KAT['KUR_ALIAS'] = $tmp_kur_alias;
	break;

	
	case "edit":
		global $_KAT, $_DOC;
		// Add switch off
		if (!empty($_DOC['conf']['no_connects'])) break;
		
		if( !empty($_KAT['KUR_ALIAS']) ) $tmp_kur_alias = $_KAT['KUR_ALIAS'];
		CONNECTS::load_paths($path, $_CONNECTS['links']);
		if( !empty( $_CONNECTS['links'] ) ){
			$_CONNECTS['TITLE'] = "� ������������ �:";
			$_CONNECTS['PASSIVE'] = true;
			$_CONNECTS['is_del'] = 'notdelete';
			if (Main::comm_inc('seealso.head.html', $f, $_CONNECTS['MODULE_NAME']))
				include "$f";
			// �� ���������
			if( is_array($_CONNECTS['links']) ){
				foreach ($_CONNECTS['links'] as $k=>$w){
					$v['path'] = $k;
					$v['name'] = $w['name'];
					$v['doc']	 = $w['doc'];
					if (Main::comm_inc('seealso.item.html', $f, $_CONNECTS['MODULE_NAME']))include "$f";
				}
			}
			// �����
			if (Main::comm_inc('seealso.foot.html', $f, $_CONNECTS['MODULE_NAME']))include "$f";
		unset( $_CONNECTS['links'] );
		unset( $_CONNECTS['is_del'] );
		}				
		if( !empty( $tmp_kur_alias ) )  $_KAT['KUR_ALIAS'] = $tmp_kur_alias;

	case "seealso":
		global $_KAT;
		if( !empty($_KAT['KUR_ALIAS']) ) $tmp_kur_alias = $_KAT['KUR_ALIAS'];
		CONNECTS::load_links($path, $_CONNECTS['links']);
		if( !empty( $_CONNECTS['links'] ) ){
			$_CONNECTS['TITLE'] = "�����������:";
			$_CONNECTS['PASSIVE'] = false;
			// �����
			if (Main::comm_inc('seealso.head.html', $f, $_CONNECTS['MODULE_NAME']))
				include "$f";
			// �� ���������
			if( is_array($_CONNECTS['links']) ){
				foreach ($_CONNECTS['links'] as $k=>$w){
					$v['path'] = $k;
					$v['name'] = $w['name'];
					$v['doc']	 = $w['doc'];
					if (Main::comm_inc('seealso.item.html', $f, $_CONNECTS['MODULE_NAME']))include "$f";
				}
			}
			// �����
			if (Main::comm_inc('seealso.foot.html', $f, $_CONNECTS['MODULE_NAME']))include "$f";
		unset( $_CONNECTS['links'] );
		}				
	if (Main::comm_inc('seealso.footer.html', $f, $_CONNECTS['MODULE_NAME']))include "$f";
	if( !empty( $tmp_kur_alias ) )  $_KAT['KUR_ALIAS'] = $tmp_kur_alias;
	break;
    
	case "seealso_all":
		global $_KAT;
		if( !empty($_KAT['KUR_ALIAS']) ) $tmp_kur_alias = $_KAT['KUR_ALIAS'];
        
		CONNECTS::load_links($path, $_CONNECTS['links'], true);
        CONNECTS::load_paths($path, $_CONNECTS['links'], true);
        
		if( !empty( $_CONNECTS['links'] ) ){
			$_CONNECTS['TITLE'] = "�����������:";
			$_CONNECTS['PASSIVE'] = false;
			// �����
			if (Main::comm_inc('seealso.head.html', $f, $_CONNECTS['MODULE_NAME']))
				include "$f";
			// �� ���������
			if( is_array($_CONNECTS['links']) ){
				foreach ($_CONNECTS['links'] as $k=>$w){
					$v['path'] = $k;
					$v['name'] = $w['name'];
					$v['doc']	 = $w['doc'];
					if (Main::comm_inc('seealso.item.html', $f, $_CONNECTS['MODULE_NAME']))include "$f";
				}
			}
			// �����
			if (Main::comm_inc('seealso.foot.html', $f, $_CONNECTS['MODULE_NAME']))include "$f";
		unset( $_CONNECTS['links'] );
		}				
	if (Main::comm_inc('seealso.footer.html', $f, $_CONNECTS['MODULE_NAME']))include "$f";
	if( !empty( $tmp_kur_alias ) )  $_KAT['KUR_ALIAS'] = $tmp_kur_alias;
	break;

	case "content":
		echo "������ ������";
	break;
	
	case 'trunc':
	if( $_CORE->IS_ADMIN ){
		CONNECTS::trunc();
		echo "��� ����� ����� ���� �������!!!";  
	}	break;
	
	case 'install':
		CONNECTS::create_table();
		echo "������� ��������� ������� ������ CONNECTS";
	break;

	case 'delete':
	if( $_CORE->IS_ADMIN ){
		CONNECTS::delete_table();
		echo "������� ��������� ������� ������ CONNECTS";
	}break;

	default:
		  //$_CONNECTS['ERROR'] = ' �������� ������� ������ '.$_CONNECTS['MODULE_NAME'];
	break;
}


//echo @$_CONNECTS['ERROR'];

?>