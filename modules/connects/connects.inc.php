<?
if (!$_CORE->dll_load('class.DBCQ'))
	die ($_CORE->error_msg());
	
$_CONNECTS['MODULE_NAME']   = 'connects';
$_CONNECTS['DEBUG']         = 1;
$_CONNECTS['TABLE']         = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'connects';
$_CONNECTS['BLACK_LIST']    = array('aliases', 'numbers');
$_CONNECTS['on_page']       = 10000;
$_CONNECTS['SEEALSO_COUNT'] =30;

class CONNECTS {
	
	/**
	 * �������� ������ ������ � ���� arr(path,name,is_parent)
	 *
	 * @param string $node - ������� ����� ����� ��������
	 * @return array[](path,name)
	 */
	function get_list($node = '/'){
		global $_DOC, $_CORE, $_PROJECT, $_CONNECTS, $_KAT;

		if (!isset($_KAT)){
			@include $_CORE->ROOT.$_CORE->DIRMODS.'db/conf.inc.php';
			@include $_CORE->ROOT.$_CORE->DIRMODS.'db/conf.user.inc.php';
			@include $_CORE->ROOT.'/data/db/conf.user.inc.php';
		}
		if (preg_match("|.*/".$_KAT['MODULE_NAME']."/(.+)/|",$node,$m)){
			$tmp_node = $m[1];
			if( empty( $_KAT['TABLES'][$tmp_node] )||( in_array($tmp_node, $_CONNECTS['BLACK_LIST']) )){ echo'���������� �� ������� /db/'.$tmp_node.'/'; return; }
			$i=0;	
			$res = SQL::sel( ' name, alias', (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . $_KAT['TABLES'][$tmp_node], '', SQL::of_li(0, $_CONNECTS['on_page']),$_CONNECTS['DEBUG']);
			if ($res->NumRows > 0) {
				for ($i = 1; $i <= $res->NumRows; $i++ ) {
					$res->FetchArray($i-1);
					list($arr[$i]['name'], $arr[$i]['path'], $arr[$i]['is_parent'])	= array($res->FetchArray['0'], $node.$res->FetchArray['1'], false);
			}}
				return $arr;
		}

		if (!isset($_DOC))
			include $_CORE->PATHMODS.'doctxt/doc.inc.php';

		// �������� ��������� �� DOCTXT
		// ��-�� �������
		if (!empty($node) && $node != '/'){
			$tmp = DOCTXT::subtree($_PROJECT['TREE'], $node, 'no_add_doctxt');
		}else {
			$i = -1;
		}
		if (empty($tmp)) {
			$tmp['next'] = $_PROJECT['TREE'];
		}
		if (is_array($tmp['next'])) foreach ($tmp['next'] as $point){
			$path_res = $point['path'];
			$is_parent = empty($point['next'])?false:true;

			if (preg_match("|.*/".$_KAT['MODULE_NAME']."/(.+)/.*|",$point['path'],$m)){
				$is_parent=true;
				if( in_array( $m[1], $_CONNECTS['BLACK_LIST'])  ) $is_parent = false;
			}
			list($arr[++$i]['name'],$arr[$i]['path'], $arr[$i]['is_parent'])	= array($point['name'], $path_res, $is_parent);
		}
		elseif(empty($node) || $node =='/')
			$_CONNECTS['ERROR'] = "���������� �� ������� ($node)";
			
		return $arr;
	}
	
	/**
	 * �������������� ����� � ����� $path � ��������� $link
	 * ��������� �������� $action add/del
	 *
	 * @param string $path
	 * @param string $link
	 * @param string $action
	 */
	function edit_link( $path, $link, $action='add'){
		global $_CONNECTS;
		if (empty($path)) return;
			switch ($action){
				case "add":
					if( substr($path,0,1) != "/") $path = "/".$path ;
					$res = SQL::ins($_CONNECTS['TABLE'], "path,link", "'$path', '$link'",$_CONNECTS['DEBUG']); // @todo update '/' requitred 
					break;
				case "del":
					
					$res = SQL::del($_CONNECTS['TABLE'], "(path like '%".$path."')AND(link like '%".$link."')", $_CONNECTS['DEBUG']);
					
					break;
			}
			if ((!empty($res))&&(!$res->Result))
				$_CONNECTS['ERROR'] .= $res->ErrorQuery;
	}
	
	function set_prior( $path, $link, $prior){
		global $_CONNECTS;
		SQL::upd($_CONNECTS['TABLE'], "prior = ".$prior."", "(path like '%".$path."')AND(link like '%".$link."')",$_CONNECTS['DEBUG']);
	}
	/**
	 * ������� ��������� ������
	 * �� ���� �������� ������
	 *
	 */
	function trunc()
	{	global $_CONNECTS, $SQL_DBLINK;
		$query = "TRUNCATE TABLE ".$_CONNECTS['TABLE'];
		$res = SQL::query( $query );

	}
	/**
	 * �������� ��������� ������
	 *
	 */
	function delete_table()
	{	global $_CONNECTS, $SQL_DBLINK;
		$query = "DROP TABLE ".$_CONNECTS['TABLE'];
		$res = SQL::query( $query );
	}
	/**
	 * �������� ��������� �������
	 *
	 */
	function create_table()
	{	global $_CONNECTS, $SQL_DBLINK;
		$query = "CREATE TABLE ".$_CONNECTS['TABLE']." (id INT UNSIGNED, path  TINYTEXT, link TINYTEXT, prior INT UNSIGNED)";
		$res = SQL::query( $query );
	}

	/**
	 * ����������� �������� ����������� ����
	 *
	 */
	function see_all()
	{   global $_CONNECTS;
		$res = SQL::sel( '*', $_CONNECTS['TABLE'], '', '');
		for( $i=0; $i<$res->NumRows; $i++)
		{	$res -> FetchArray($i);
			foreach( $res->FetchArray  as $k=>$v ) echo "$k=>[$v]";
			echo  "<BR><BR>";
		}
	}
	
	/**
	 * ������������� ������
	 *
	 * @param string $alias
	 * @return string
	 */
	function get_name_by_alias( $alias )
	{		global $_KAT,$_PROJECT;

			if( (strstr($alias, '/'.$_KAT['MODULE_NAME'].'/') || strpos($alias, $_KAT['MODULE_NAME']) == 0)&&( $alias{strlen($alias)-1}!='/' )){
				preg_match("|.*/?(".$_KAT['MODULE_NAME']."/.+)|",$alias,$m);			
				$cmd = dirname($m[1]).'/field/'.basename($alias).'/name';
				
				$res = cmd($cmd);
			}else{
				if (strstr($alias,'/doctxt/')){
					if (preg_match("|(.*/)(".$_KAT['MODULE_NAME']."/.+)|",$alias,$m))
						$alias = $m[1];

					$res = cmd(str_replace("doctxt/", "doctxt/field/", $alias)."title");
				// if '/db/kat_name'
				}else{
					$try = Main::array_multisearch( $alias, $_PROJECT['TREE'], "and end");
					if (!empty($try)) {
						$res = $_PROJECT['TREE'][$try[0]]['name'];
					}
				}
			}
			if( empty($res)||( $res =='' )) $res = $alias;
			return $res;
	}

	/**
	 * ������������� ������
	 *
	 * @param string $alias
	 * @return string
	 */
	function get_field_by_alias( $alias, $field = 'name' )
	{		global $_KAT,$_PROJECT;
			$field = stripslashes($field);
			
			// for DB
			if( (strstr($alias, '/'.$_KAT['MODULE_NAME'].'/') || strpos($alias, $_KAT['MODULE_NAME']) == 0)&&( $alias{strlen($alias)-1}!='/' )){
			     preg_match("|.*/?(".$_KAT['MODULE_NAME']."/.+)|",$alias,$m);
				if ($field == 'path') {
				    
                    $alias = $m[1];
				    $alias_arr = explode("/",$alias);
                    if($alias_arr[0] == "db"){
                        $alias = $alias_arr[1];
                       $try = Main::array_multisearch( $alias, $_PROJECT['TREE'], "and end");

    					if (!empty($try)) {
    						$res = $_PROJECT['TREE'][$try[2]]['alias'];
                            $curr_tree = $_PROJECT['TREE'];
                            foreach($try as $k=>$v){
                               //echo $_PROJECT['TREE'][$try[$k]];
                              
                                $curr_tree = $curr_tree[$v];

                            }
                            $res = $curr_tree.$alias_arr[2];
    					} else {
    					   $alias = '/'.implode('/',$alias_arr);
    					}
                    }
				}else{
    				$cmd = dirname($m[1]).'/field/'.basename($alias).'/'.$field;
    				
    				$res = cmd($cmd);
    
    				// if need doc for DB (doctxt not done)
    				if ($field == 'doc') {
    					$res = '/data/'.$_KAT['MODULE_NAME'].'/f_'.str_replace($_KAT['MODULE_NAME'].'/', '',dirname($m[1])).'/'.$res;
    				}
				}
			}else{
			// for doctxt
				if (strstr($alias,'/doctxt/')){
					if (preg_match("|(.*/)(".$_KAT['MODULE_NAME']."/.+)|",$alias,$m))
						$alias = $m[1];
					
					$res = cmd(str_replace("doctxt/", "doctxt/field/", $alias).($field == 'name' ? 'title' : $field) );
				// if '/db/kat_name'
				}else{
					$try = Main::array_multisearch( $alias, $_PROJECT['TREE'], "and end");
					if (!empty($try)) {
						$res = $_PROJECT['TREE'][$try[0]][$field];
					}
				}
			}
			if( empty($res)||( $res =='' )) $res = $alias;
			return $res;
	}
	
	/**
	 * �������� � ������ $links �������� � ����� $path
	 *
	 * @param string $path
	 * @param array $links
	 */
	function load_links( $path, &$links, $path_restor=false){
		global $_CONNECTS;
		$res = SQL::sel( 'link',$_CONNECTS['TABLE'], "path like '%".$path."'", 'ORDER BY prior ASC');
		for( $i=0; $i<$res->NumRows; $i++ ){
			$res -> FetchArray($i);
			$v = $res->FetchArray['link'];
             if($path_restor){
                $path = CONNECTS::get_field_by_alias($v, 'path');
    			$links[$path]['name'] = CONNECTS::get_name_by_alias($v);
    			$links[$path]['doc'] = CONNECTS::get_field_by_alias($v, 'doc');               
            }else{
    			$links[$v]['name'] = CONNECTS::get_name_by_alias($v);
    			$links[$v]['doc'] = CONNECTS::get_field_by_alias($v, 'doc');
            }
		}
	}
	
	function load_paths( $path, &$links, $path_restor=false){
		global $_CONNECTS;
		//$path = basename($path);
		$res = SQL::sel( 'path',$_CONNECTS['TABLE'], "link like '%".$path."'", 'ORDER BY prior ASC');
		for( $i=0; $i<$res->NumRows; $i++ ){
			$res -> FetchArray($i);
			$v = $res->FetchArray['path'];
            if($path_restor){
                $path = CONNECTS::get_field_by_alias($v, 'path');
    			$links[$path]['name'] = CONNECTS::get_name_by_alias($v);
    			$links[$path]['doc'] = CONNECTS::get_field_by_alias($v, 'doc');               
            }else{
    			$links[$v]['name'] = CONNECTS::get_name_by_alias($v);
    			$links[$v]['doc'] = CONNECTS::get_field_by_alias($v, 'doc');
            }
		}
	}
    
	/**
	 * ����������� ������� ��� ���� $link
	 *
	 * @param string $link
	 * @return string
	 */
	function from_where( $link )
	{	global $_CORE, $_KAT;
//		if (!isset($_KAT))			include $_CORE->ROOT.$_CORE->DIRMODS.'db/conf.inc.php';
//		if (!isset($_DOC))			include $_CORE->PATHMODS.'doctxt/doc.inc.php';
		if( substr($link,0,1) != "/") $link = "/".$link ;
		if( strstr($link,'/'.$_KAT['MODULE_NAME'].'/')&&( $link{strlen($link)-1}!='/' )){
			preg_match("|.*(/".$_KAT['MODULE_NAME']."/.+/)(.*)|",$link,$m);			
			return $m[1]."?mark=".$m[1].$m[2];
		}
		if (strstr($link,'/doctxt/'))
		if (preg_match("|(.*/)(".$_KAT['MODULE_NAME']."/.+/)|",$link,$m)){
				preg_match("|(.+)(/".$_KAT['MODULE_NAME']."/.+/)|",$link,$m);
				return '/doctxt/?list&node='.dirname($m[1]).'/&mark='.$link;
		}else{
				return '/doctxt/?list&node='.dirname($link).'/&mark='.$link;				
		}		
		return '/';
	}
}
?>