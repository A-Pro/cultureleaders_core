<?php
class ADMIN {
	
	public function __construct() {
		
	}
	
	
	
	public function get_history_doctxt(){
		global $_DOC;
		if (!is_file($_DOC['HISTORY_FILE'])) {
			$_DOC['ERROR']	= 'No History Data';
			return false;
		}
		// read 
		$arr = file($_DOC['HISTORY_FILE']); 
		$history = array();
		if(is_array($arr))
		foreach($arr as $i => $item){
			list($data,$path) = explode("\t",trim($item));
			$history[$i]['data'] = $data;
			$history[$i]['path'] = $path;
			$history[$i]['title'] = DOCTXT::get_cont($history[$i]['path'],'title',true);
			$history[$i]['anons'] = DOCTXT::get_cont($history[$i]['path'],'anons',true);
			if(empty($history[$i]['title'])) unset($history[$i]);
			
		}
		
		return $history;
	}
	
	
	
	public function get_shortcuts(){
		$shortcuts = SQL::getall('*',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."core_shortcuts",'1 AND (hidden != 1 OR hidden IS NULL)','ORDER BY prior asc, id asc ');
		return $shortcuts;
	}
	
	
	
	public function add_maincontent(){
		global $_CONF;
		$query = "ALTER TABLE `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."settings` ADD `admin_title` VARCHAR( 255 ) NOT NULL;";
		$res = SQL::query( $query );
		
		$query = "ALTER TABLE `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."settings` ADD `admin_cont` text NOT NULL;";
		$res = SQL::query( $query );
		$_CONF['settings']['admin_title']= $_CONF['settings']['admin_cont'] = '';
		
		return;
	}
}
?>