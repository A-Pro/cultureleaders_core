<?
/* to save basket into base, on this option in DB and /db/install turn on */

global $_CONF, $_BASKET;

if ($_CONF['FEATURES_USED']['basketsave']) {
	$_CORE->dll_load('class.cForm');
	global $form;
	$form = $data;
	$form['alias'] = time();
	$form['ts'] = date("Y-m-d");
	$form['hidden'] = 1;
	include $_CORE->CORE_ROOT."/modules/db/_coredb_basket.data.inc.php";
	$_BASKET['cForm']	= new cForm($FORM_DATA);
	list($var, $val) = $_BASKET['cForm']->GetSQL('insert');
	$res	= SQL::ins( $_BASKET['TABLE'], $var, $val, DEBUG);
	if($_BASKET['save_purchasing']){
		// сохраняем данные о покупках в отдельную таблицу
		BASKET::save_purchasing($form['alias']);
	}
	
	// add user for request
	if ($_CONF['FEATURES_USED']['basketauth']) {
		include "user.add.inc.php";
	}
}
?>