<?
global $_CORE, $_BASKET;
#
#	BASKET MODULE
#
define ("BASKET_ERR_WRONG_EMAIL","�������� e-mail �����.");
define ("BASKET_ERR_EMPTY_BODY","������ �������.");
define ("BASKET_ERR_EMPTY_DELIVERY","�� ������ ������ ��������.");
define ("BASKET_ERR_EMPTY_NAME","������� ���� ���.");
define ("BASKET_ERR_EMPTY_PHONE","������� ��� ���������� ����� ��������.");

define ("BASKET_ERR_NOT_SEND",'��� ����� �� ���� ���� ��������� ���������� �������.');
define ("BASKET_ERR_SEND",'��� ����� ���������.');

$module = $_BASKET['MODULE_NAME']	  = 'basket';

Main::load_lang_mod($module);

$_BASKET['MODULE_TITLE']  = Main::get_lang_str('dir', 'basket');
$_BASKET['BASKET_SUMM']   = Main::get_lang_str('basket_summ', 'basket');

$_BASKET['PLUGIN'] = 'plugin'; // call after check_contact
$_BASKET['TABLE']	= DB_TABLE_PREFIX . 'basket';
$_BASKET['TABLE_ITEMS']	= DB_TABLE_PREFIX . 'basket2items';


class BASKET {
	/**
	*
	*/
	function check( )
	{
		global $_CORE, $_BASKET;
		# ��������� ����������� �������
		if (empty($_BASKET['cForm'])) {
			global $FORM_DATA_ITEM;
			$_CORE->dll_load('class.cForm');
			$_BASKET['cForm']	= new cForm( $FORM_DATA_ITEM );
		}
		#
		#	������������ ��������
		#
		if ($_BASKET['cForm']->Chk_come()) {										// if BAD
			$_BASKET['ERROR'] = $_BASKET['cForm']->Error;
			return false;
		}
		return true;
	}

	function put( &$form )
	{
		if (!isset($_SESSION['_BASKET_CONT'])) {
			session_start();
		}

		if (!is_array($_SESSION['_BASKET_CONT'])) {
			$_SESSION['_BASKET_CONT'] = array();
		}

		if ($form['count'] > 0){
			if (!empty($_SESSION['_BASKET_CONT'][STR::translit($form['subj'])][$form['link']])) {
				$was_size  = $_SESSION['_BASKET_CONT'][STR::translit($form['subj'])][$form['link']]['size'];
				$was_count = $_SESSION['_BASKET_CONT'][STR::translit($form['subj'])][$form['link']]['count'];
			}
			if ( isset($form['size']) && !empty($form['size']) ){
				if (strstr($form['size'], $was_size)) {
					$form['size'] = $was_size;
				}else
					$form['size'] .= ','.$was_size;
			}

			if ($was_count > 0) {
				$form['count'] += $was_count;
			}
/*echo $_SESSION['_BASKET_CONT'] . '<br>';
echo $_SESSION['_BASKET_CONT'][STR::translit($form['subj'])] . '<br>';
echo $_SESSION['_BASKET_CONT'][STR::translit($form['subj'])][$form['link']] . '<br>';*/
			$_SESSION['_BASKET_CONT'][STR::translit($form['subj'])][$form['link']]	= $form;
		} 
		else {
			unset($_SESSION['_BASKET_CONT'][STR::translit($form['subj'])][$form['link']]);
		}

		//echo "<pre>";
		//print_r ($_SESSION);


		BASKET::get_cont('empty');
	}

	function del( $del )
	{
		if (!isset($_SESSION['_BASKET_CONT']))
			return true;
		if (isset($_SESSION['_BASKET_CONT'][$del[0]][$del[1]])) {
			unset($_SESSION['_BASKET_CONT'][$del[0]][$del[1]]);
		}
		return true;
	}

	function change( $del )
	{
		if (!isset($_SESSION['_BASKET_CONT']))
			return true;
		if (isset($_SESSION['_BASKET_CONT'][$del[0]][$del[1]])) {

			if ($_POST['BASKET']['count'] == 0)
				unset($_SESSION['_BASKET_CONT'][$del[0]][$del[1]]);
			else{
				$_SESSION['_BASKET_CONT'][$del[0]][$del[1]]['count']	= $_POST['BASKET']['count'];
				$_SESSION['_BASKET_CONT'][$del[0]][$del[1]]['size']	= $_POST['BASKET']['size'];
			}

			return true;
		} else {
			$_BASKET['ERROR'] = 'No item';
			return false;
		}
	}
	/**
	* ����� empty ��� ��������� �������
	*/
	function get_cont($is_empty = '',$tpl = '')
	{
		global $_CORE, $_BASKET;
//		if (empty($tpl)) $tpl = $_CORE->SiteModDir.'/';

		if (!$is_empty) 
			if (BASKET::inc_tpl($tpl.'head.html', $file)){
				include "$file";
			}

		if (!empty($_SESSION['_BASKET_CONT'])) {
			$Summ = $Items = $Names = 0;
			$i	= 0;
			foreach ($_SESSION['_BASKET_CONT'] as $subjs => $vals) {
				foreach ($vals as $id => $data) {
					if (!$is_empty) {
						if (BASKET::inc_tpl($tpl.'item.html', $file)){
							include "$file";
						}
					}
					$Items	+= $data['count'];
					$data['price'] = str_replace(" ", '',$data['price']);
					$Summ	+= $data['count']*round(str_replace(",", '.',$data['price']));
					$i++;
					$Names++;
				}
			}
			$Summ = $Summ;
			$_SESSION['_BASKET_SUMM'] = $Summ;
			$_SESSION['_BASKET_ITEMS'] = $Items;
			$_SESSION['_BASKET_NAMES'] = $Names;
		}

		if (!$is_empty) {
			if (BASKET::inc_tpl($tpl.'foot.html', $file)){
				include "$file";
			}
		}
	}

	/**
	*
	*/
	function make_report()
	{
		ob_start();
		BASKET::get_cont(false, 'mail.');
		if (BASKET::inc_tpl('mail.contact.html', $file)){
			include "$file";
		}
		$res	= ob_get_contents();
		ob_end_clean();
		return $res;
	}

	/**
	*
	*/
	function chck_contacts()
	{
		global $_BASKET;
		if($_BASKET['delivery'] && empty($_POST['BASKET_DELIVERY']['delivery_type'])){
			$_BASKET['ERROR']	= BASKET_ERR_EMPTY_DELIVERY;
			return false;
		}
		
		if (!empty($_SESSION['BASKET_CONTACTS'])) {
			$_POST['BASKET_CONTACTS'] = $_SESSION['BASKET_CONTACTS'];
		}
		if (empty($_SESSION['_BASKET_ITEMS'])) {
			$_BASKET['ERROR']	= BASKET_ERR_EMPTY_BODY;
			return false;
		}
		if (empty($_POST['BASKET_CONTACTS']['name'])) {
			$_BASKET['ERROR']	= BASKET_ERR_EMPTY_NAME;
			return false;
		}
		$_SESSION['_BASKET_NAME'] = $_POST['BASKET_CONTACTS']['name'];
		if (empty($_POST['BASKET_CONTACTS']['phone'])) {
			$_BASKET['ERROR']	= BASKET_ERR_EMPTY_PHONE;
			return false;
		}
		$_SESSION['_BASKET_PHONE'] = $_POST['BASKET_CONTACTS']['phone'];
/*		if (!lMail::is_email($_POST['BASKET_CONTACTS']['email'])) {
			$_BASKET['ERROR']	= BASKET_ERR_WRONG_EMAIL;
			return false;
		}*/
		$_SESSION['_BASKET_EMAIL'] = $_POST['BASKET_CONTACTS']['email'];
		return true;
	}

/**
 * Proverka na suwestvovanie fajla-shablona s imenem $tpl
 *
 * Pri proverke suwestvuet prioritet fajlov:
 * <code>
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BASKET['MODULE_NAME'].'/'.$tpl,
 *$_CORE->CORE_PATHTPLS.'/'.$_BASKET['MODULE_NAME'].'/_'.$tpl,
 * </code>
 * U pervogo iz suwestvujuwih fajlov-shablonov, ego imja i put' do nego sohranjaetsja v $file. A v rezul'tate funkcii vozvrawaetsja true, inache false.
 * Primer ispol'zovanija (modul' db, komanda /list/ ):
 * <code>
 *  if (BASKET::inc_tpl('BASKET.list.foot.html', $file))
 *            include "$file";
 * </code>
 * 
 * @uses FILE::chk_files()
 * @uses $_CORE->PATHTPLS
 * @uses $_CORE->CURR_TPL
 * @uses $_CORE->CORE_PATHTPLS
 * $_BASKET['MODULE_NAME']
 * @param string $tpl
 * @param string $file
 * @return string
 */
	function inc_tpl($tpl, &$file)
	{
		global $_CORE, $_BASKET;

		$files	=  array( 
	
		//  na sajte v tekuwem shablone, direktorija modulja, prosto fajl
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BASKET['MODULE_NAME'].'/'.$tpl, 
			
		// v jadre, direktorija modulja, alias s "_" i nazvanie
			$_CORE->CORE_PATHTPLS.'/'.$_BASKET['MODULE_NAME'].'/_'.$tpl, 
			
			// defoltovyj shablon
		// na sajte, shablon po umolchaniju vsego modulja
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BASKET['MODULE_NAME'].'/'.$_BASKET['MODULE_NAME'].'/'.$tpl, 
			
		// v jadre, shablon po umolchaniju vsego modulja
			$_CORE->CORE_PATHTPLS.'/'.$_BASKET['MODULE_NAME'].'/'.$_BASKET['MODULE_NAME'].'/_'.$tpl, 
		);

		$tmp = FILE::chk_files($files, $file);
		return $tmp;
	}
	/**
	* ���������� ������ � ������� ������ �����/�����
	*/
	function save_purchasing($order_alias)
	{
		global $_CORE, $_BASKET;
		
		if (!empty($_SESSION['_BASKET_CONT'])) {
			
			foreach ($_SESSION['_BASKET_CONT'] as $subjs => $vals) {
				foreach ($vals as $id => $data) {
					if( substr($data['link'],-1) == '/' ) $data['link'] = substr($data['link'],strlen($data['link'])-1);
					if( substr($data['link'],0,1) == '/' ) $data['link'] = substr($data['link'],1);
					$arr_link = explode('/',$data['link']);
					$arr = array();
					$arr['order_alias'] = $order_alias;
					$arr['item_alias'] = array_pop($arr_link);
					$arr['db_alias'] = array_pop($arr_link);
					$res = SQL::ins( $_BASKET['TABLE_ITEMS'], implode(",",array_keys($arr)), "'".implode("','",array_values($arr))."'", DEBUG);
				}
			}
		}
		return;
	}
	
	
	
	function getpath($p='')
	{
		global $_PROJECT, $_BASKET;
		if ($p == '') 
			if (empty($_REQUEST['node'])) {
				$p = $_SERVER['REQUEST_URI'];
			}else{
				$p = $_REQUEST['node'];
			}
		if (!empty($_PROJECT['PATH_2_CONTENT']))
			$_PROJECT['PATH_2_CONTENT'] = array();

		// For /doctxt/..../db/some/ITEM  - construction patch
		$p	= (substr($p,-1,1)!='/') ? $p.'/' : $p;
		$parts = explode('/',$p);
		$LOOK	= (substr($p,0,6) == 'admin/') ? substr($p, 6) : $p;
		$path	= (substr($p,-1,1)!='/') ? $p.'/' : $p;
		
		if (!in_array($p, array( '', '/'))) {
			$PATH_2_CONTENT_tmp	=& Main::array_multisearch( $path, $_BASKET['TREE'], "and end");
			$up	= $last	= $prelast = $_BASKET['TREE'];
			for ($i=0;$i<sizeof($PATH_2_CONTENT_tmp)-1; $i++ ) {
				if (!empty($up[$PATH_2_CONTENT_tmp[$i]]['name']) ) {
					$_PROJECT['PATH_2_CONTENT'][]	= array(
						'name'	=> $up[$PATH_2_CONTENT_tmp[$i]]['name'],
						'path'	=> $up[$PATH_2_CONTENT_tmp[$i]]['path']
						);
				}
				$up = $up[$PATH_2_CONTENT_tmp[$i]];
			}
		}
	}
	
	
	
	public function install_delivery(){
		$alias = 'basket_delivery';
		$alias2 = 'basket_delivery2payment';
		$file = KAT::get_data_path('conf.user.inc.php', $f, KAT_LOOKIG_DATA_FILE);
		include $file;
		$str = '';
		if(empty($_KAT['TABLES'][$alias])){
			$str .= "
			\$_KAT['onpage_def']['$alias']	= 20;
			\$_KAT['TABLES']['$alias']	= '$alias';
			\$_KAT['TITLES']['$alias']	= '������� ��������';
			\$_KAT['SRC']['$alias']	= 'coredb_delivery';
			\$_KAT['DOC_PATH']['$alias'] = '/tmp/inside';
			\$_KAT['FILES']['$alias']	= 'coredb_delivery.data.inc.php';
			\$_KAT['$alias']['AFTER_ADD']	= 'db/$alias/'; 
			\$_KAT['$alias']['hidden']	= 'TRUE'; // ��� db/list
			";
		}
		if(empty($_KAT['TABLES'][$alias2])){
			$str .= "
			\$_KAT['onpage_def']['$alias2']	= 20;
			\$_KAT['TABLES']['$alias2']	= '$alias2';
			\$_KAT['TITLES']['$alias2']	= '��������� ��������';
			\$_KAT['SRC']['$alias2']	= 'coredb_delivery2payment';
			\$_KAT['DOC_PATH']['$alias2'] = '/tmp/inside';
			\$_KAT['FILES']['$alias2']	= 'coredb_delivery2payment.data.inc.php';
			\$_KAT['$alias2']['AFTER_ADD']	= 'db/$alias2/'; 
			\$_KAT['$alias2']['hidden']	= 'TRUE'; // ��� db/list
			\$_KAT['$alias2']['adm_fields'] = array('type_id','condition','value','cost');
			";
		}
		// est' li fajlo, prochitaem, obnovim, sohranim
		KAT::load_users_conf($str_was);
		$str = implode('',$str_was).$str;
		KAT::save_users_conf($str);
		
		
		cmd('/db/install');
		
	}
	
	
	
	public function get_deliverypay($summ, $delivery_type = ''){
		global $_BASKET;
		if(!$_BASKET['delivery']) return false;
		$where = '1';
		if( !empty($delivery_type)) 
			$where .= " AND `type_id` = '".$delivery_type."'";
		else
			$where .= " AND `type_id` = '���'";
			
		$where .= " AND (";
		$where .= " (`condition` = 1  AND value >= $summ )";
		$where .= " OR (`condition` = 2  AND value <= $summ )";
		$where .= " OR `condition` = 3 ";
		
		$where .= " )";
		
		$res = SQL::getrow('*', DB_TABLE_PREFIX . 'basket_delivery2payment', $where ,'order by cost ASC limit 1');
		if($res!==false)
			return $res;
		else 
			return false;
	}
} 
?>