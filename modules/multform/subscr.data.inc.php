<?
$_MULTFORM['TITLE'] = '���������� ��������';

$FORM_DATA = array(
	0	=> array(
		'name'	=> '����������&nbsp;����������',
		'js'	=> 'm=[0,1,2];',
		'next' 	=> array(
				  'fam' => 
				  array (
				    'field_name' => 'fam',
				    'name' => 'form[fam]',
				    'title' => '�������',
				    'maxlen' => '255',
				    'must'=>'1','also'=>'onkeyup="v()"',
					'style' => 'width:100%',
				    'type' => 'textbox',
					'readonly' => 0,
				  ),
				  'name' => 
				  array (
				    'field_name' => 'name',
				    'name' => 'form[name]',
				    'title' => '���',
				    'maxlen' => '255',
				    'must'=>'1','also'=>'onkeyup="v()"',
					'style' => 'width:100%',
				    'type' => 'textbox',
					'readonly' => 0,
				  ),			  
				  'midname' => 
				  array (
				    'field_name' => 'midname',
				    'name' => 'form[midname]',
				    'title' => '����c���',
				    'maxlen' => '255',
				    'must'=>'1','also'=>'onkeyup="v()"',
					'style' => 'width:100%',
				    'type' => 'textbox',
					'readonly' => 0,
				  ),			  
//				  'phone' => 
//				  array (
//				    'field_name' => 'phone',
//				    'name' => 'form[phone]',
//				    'title' => '�������',
//				    'maxlen' => '255',
//				    'must'=>'1','also'=>'onkeyup="v()"',
//					'style' => 'width:100%',
//				    'type' => 'textbox',
//					'readonly' => 0,
//				  ),			  
//				  'email' => 
//				  array (
//				    'field_name' => 'email',
//				    'name' => 'form[email]',
//				    'title' => '��. �����',
//				    'maxlen' => '255',
//				    'must'=>'1','also'=>'onkeyup="v()"',
//					'style' => 'width:100%',
//				    'type' => 'textbox',
//					'readonly' => 0,
//				  ),			  
			)// first
	),
	1 => array(
		'name'	=> '�����',
		'js'	=> 'm=[0,1,2,3,4,6]',
		'next' 	=> array(
				  'index' => 
				  array (
				    'field_name' => 'index',
				    'name' => 'form[index]',
				    'title' => '������',
				    'maxlen' => '255',
				    'must'=>'1','also'=>'onkeyup="v()"',
					'style' => 'width:100%',
				    'type' => 'textbox',
					'readonly' => 0,
				  ),
				  'obl' => 
				  array (
				    'field_name' => 'obl',
				    'name' => 'form[obl]',
				    'title' => '�������',
				    'maxlen' => '255',
				    'must'=>'1','also'=>'onkeyup="v()"',
					'style' => 'width:100%',
				    'type' => 'textbox',
					'readonly' => 0,
				  ),			  
				  'city' => 
				  array (
				    'field_name' => 'city',
				    'name' => 'form[city]',
				    'title' => '�����, ����',
				    'maxlen' => '255',
				    'must'=>'1','also'=>'onkeyup="v()"',
					'style' => 'width:100%',
				    'type' => 'textbox',
					'readonly' => 0,
				  ),			  
				  'street' => 
				  array (
				    'field_name' => 'street',
				    'name' => 'form[street]',
				    'title' => '�����',
				    'maxlen' => '255',
				    'must'=>'1','also'=>'onkeyup="v()"',
					'style' => 'width:100%',
				    'type' => 'textbox',
					'readonly' => 0,
				  ),			  
				  'house' => 
				  array (
				    'field_name' => 'house',
				    'name' => 'form[house]',
				    'title' => '���',
				    'maxlen' => '255',
				    'must'=>'1','also'=>'onkeyup="v()"',
					'style' => 'width:100%',
				    'type' => 'textbox',
					'readonly' => 0,
				  ),			  
				  'korp' => 
				  array (
				    'field_name' => 'korp',
				    'name' => 'form[korp]',
				    'title' => '������',
				    'maxlen' => '255',
				    'must' => '0',
					'style' => 'width:100%',
				    'type' => 'textbox',
					'readonly' => 0,
				  ),			  
				  'flat' => 
				  array (
				    'field_name' => 'flat',
				    'name' => 'form[flat]',
				    'title' => '��������',
				    'maxlen' => '255',
				    'must'=>'1','also'=>'onkeyup="v()"',
					'style' => 'width:100%',
				    'type' => 'textbox',
					'readonly' => 0,
				  ),			  
			),// second
		),
		
	2 => array(
		'name'	=> '����������&nbsp;�&nbsp;������',
		'js'	=> 'm=[0,1];mo=[2,3,4,5,6,7,8,9,10,11,12,13]', // ��� ������� ��������� ����: m - ������������, mo - ���� �� ����
		'next' 	=> array(
				  'count' => 
				  array (
				    'field_name' => 'count',
				    'name' => 'form[count]',
				    'title' => '����������',
				    'maxlen' => '255',
				    'must'=>'1','also'=>'onkeyup="v()"',
					'style' => 'width:100%',
				    'type' => 'textbox',
					'readonly' => 0,
				  ),
				  'type' => 
				  array (
				    'field_name' => 'type',
				    'name' => 'form[type]',
				    'title' => '�������',
				    'maxlen' => '255',
				    'must'=>'1',
				    'also'=>'onkeyup="v()"',
					'style' => 'width:100%',
				    'type' => 'select',
					'readonly' => 0,
					'arr'	=> array('',
					'�� ���������, ���������, ��������',
					'�� ��������� � �������������� � ������������������ �������������� �����'
       				),
				  ),				  
				  'months' => 
				  array (
				    'field_name' => 'months',
				    'name' => 'form[months]',
				    'title' => '������',
				    'maxlen' => '255',
				    'must'=>'1','also'=>'onchange="v()"',
					'style' => 'width:100%',
				    'type' => 'multcheckbox',
					'readonly' => 0,
					'in_row' => 3,
					'arr'	=> array_values($this->CONF['month']),
				  ),			  
			)
	),// third
	3 => array(
		'name' => '�����'
	)
);
?>