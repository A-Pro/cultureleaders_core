<?

class GALLERY {
	/**
	*
	*/
	function del( $url )
	{
		global $_GALLERY;
		if (is_file($_SERVER['DOCUMENT_ROOT'].'/'.$url)) {
			if (!unlink($_SERVER['DOCUMENT_ROOT'].'/'.$url)) {
				$_GALLERY['ERROR'] = "No permissions.";
			}
			if (!unlink($_SERVER['DOCUMENT_ROOT'].'/'.dirname($url).'/tmbs/'.basename($url)) ) {
				$_GALLERY['ERROR'] = "No permissions to del small.";
			}
		}
	}
	/**
	*
	*/
	function get_big( $url )
	{
		global $_GALLERY, $_CORE;
		$data['url']	= $url;
		$data['size'] = @getimagesize ($_SERVER['DOCUMENT_ROOT'].'/'.$url );
		if (empty($data['size'])) {
			echo "<SCRIPT>self.close()</SCRIPT>";
			return;
		}
		if ($data['size'][0] > 1024) {
			$data['size'][0] = 1024;
			$data['size'][1] = 768;
		}
		include (is_file($_CORE->SiteModDir."/"."gallery.item.html"))?
				$_CORE->SiteModDir."/"."gallery.item.html" :
				$_CORE->CoreModDir."/"."_gallery.item.html";
	}

	/**
	*
	*/
	function get_list($path = 'foto1')
	{
		global $_GALLERY, $_CORE;
		$page = (!empty($_REQUEST['p'])) ?$_REQUEST['p']:1;

		$arr	= array();
		if (!empty($path) && is_dir($_GALLERY['PATH_ROOT'].$path))
			if ($handle = opendir($_GALLERY['PATH_ROOT'].$path)) {
				while (false !== ($file = readdir($handle)))
					if ( is_dir($_GALLERY['PATH_ROOT'].$path.'/'.$file) || in_array($file, array('.','..','about')) ) continue;
					else $arr[] = $file;
				closedir($handle); 
			}
			if (sizeof($arr) > 0 ) {
				$pline	= GALLERY::pline(sizeof($arr), $page, $_GALLERY['ONPAGE']);

				include (is_file($_CORE->SiteModDir."/"."gallery.head.html"))?
					$_CORE->SiteModDir."/"."gallery.head.html" :
					$_CORE->CoreModDir."/"."_gallery.head.html";

				$from = ($page - 1)*$_GALLERY['ONPAGE'];
				$to = min($from + $_GALLERY['ONPAGE'], sizeof($arr));
//				echo $from."-".$to;
				for ($i=$from; $i < $to;$i++) {
					$data['url'] = $path;
					$data['file'] = $arr[$i];
					$size = @getimagesize ($_GALLERY['PATH_ROOT'].$path.'/tmbs/'.$arr[$i]);
					$data['sm_size'] = $size[3];
//					$size = getimagesize ($_GALLERY['PATH_ROOT'].'/'.$path.'/'.$arr[$i]);
					$data['size'] = @getimagesize ($_GALLERY['PATH_ROOT'].$path.'/'.$arr[$i]);
					if ($data['size'][0] > 1024) {
						$data['size'][0] = 1024 + 6;
						$data['size'][1] = 768 + 6;
					}
					include (is_file($_CORE->SiteModDir."/"."gallery.list.item.html"))?
							$_CORE->SiteModDir."/"."gallery.list.item.html" :
							$_CORE->CoreModDir."/"."_gallery.list.item.html";
//					include "gallery.list.item.html";
				}
				include (is_file($_CORE->SiteModDir."/"."gallery.foot.html"))?
						$_CORE->SiteModDir."/"."gallery.foot.html" :
						$_CORE->CoreModDir."/"."_gallery.foot.html";
//				include "gallery.foot.html";
			}
		else {
			$_GALLERY['ERROR'] = 'Wrong path to files! ('.$path.")";
		}
	}

	function pline( $all, $p, $onpage)
	{
			global $th, $mount, $year;
			if ($all < $onpage) 
				return '';
			if (intval($onpage) == 0) return '';
			$QS		= $_SERVER['QUERY_STRING'];
			$string = str_replace("&p=$p","",$QS);
			if ($string != '') $string .= '&';									// ��� ���������� ����� /?&p - �� ��������
			$uri	 = $_SERVER['REQUEST_URI'];
			$tmp	= '
				<TABLE border="0" cellpadding="0" cellspacing="0">
				<TR bgcolor="F0F0F0">
					<TD height="18" width="28"></TD>
					<TD></TD>
					<TD valign="middle">';
			$tmp	.= ($_CORE->LANG == 'en') ? 'Pages :' : '��������: ';
			$pages = ceil($all / $onpage);
			$inline = min(10, $pages); // ������� ������ �� ������ �������� � �����
			if ($p < $inline) {
					$from = 1; $to = $from + $inline -1;
			}elseif( $p > $pages - $inline) {
					$from = max($pages - $inline, 1); $to = $pages;
			}else {
					$from   = max(1, $p - floor($inline / 2)); $to  = min($pages, $p + floor($inline / 2))+1;
			}

			if ($from > 1) $tmp .= "<a href='$uri?".$string."p=".($from-1)."'>...</a>&nbsp;|&nbsp;";
			for( $i = $from; $i <= $to; $i++ ) {
					$tmp .= ($i == $p) ? "<span class='nlink'>".($i)."</span>" : "<a href='$uri?".$string."p=$i'>".($i)."</a>";
					$tmp .= "&nbsp;|&nbsp;";
			}
			if ($pages-1 > $to) $tmp .= "<a href='$uri?".$string."p=$i' >...</a>&nbsp;&nbsp;";
			$tmp	.= '</TD>
					<TD width="28">&nbsp;</TD>
				</TR>
				</TABLE>
			';
			return $tmp;
	}

	/**
	*
	*/
	function get_rand_img($big = false)
	{
		global $_GALLERY;
		$arr	= GALLERY::read_galleries();

		if (empty($arr)|| !is_array($arr)) return false;

		srand ((float)microtime()*1000000);
		$r1 = rand(0,sizeof($arr)-1);
//		shuffle ($arr);
		$imgs = FILE::read_dir($_GALLERY['PATH_ROOT'].$arr[$r1].((!$big)?'/tmbs/':''));

		if (empty($imgs) || !is_array($imgs)) return false;

//		print_r($imgs);
		srand ((float)microtime()*1000000);
		$r = rand(0,sizeof($imgs)-1);
//		shuffle ($imgs);
//		print_r($imgs);

		return $arr[$r1].((!$big)?'/tmbs':'')."/".$imgs[$r];
	}

	/**
	*
	*/
	function read_galleries()
	{
		global $_GALLERY, $_CORE;
		$to_imgs	= $_GALLERY['PATH_ROOT'].$_GALLERY['PATH_LINK'];
		$gals		= FILE::read_dir($to_imgs);

		if (empty($gals)) return false;
		
		$good = array();
		
		for ($i=sizeof($gals);$i>=0;$i--)
			if (is_dir($to_imgs.$gals[$i].'/tmbs'))
				$good[] = $_GALLERY['PATH_LINK'].$gals[$i];

		if (!empty($good)) return $good;
		else return false;
	}

	/**
	*
	*/
	function stat_calc()
	{
		global $_GALLERY;
		if (is_file($_GALLERY[ "SKIP_FILES" ])) {
			$Others = file( $_GALLERY[ "SKIP_FILES" ] ); //������������ ������, �� �����������
		}else {
			$Others = array();
		}
		$Others[] = ".";
		$Others[] = "..";

		$IDdir = opendir( $_GALLERY[ "PATH_DATA" ] );
		$IDstatFile = fopen( $_GALLERY[ "STAT_FILE" ], "w");
		$p=0;
		$files=0;
		while( ($CurFile=readdir($IDdir))!==false ) 
		{  $CurDir = $_GALLERY[ "PATH_DATA" ]."/$CurFile/tmbs";
		   if( !in_array($CurFile,array(".", "..")) && is_dir($CurDir) )
			 {  $IDcurDir = opendir( $CurDir );
				$i = 0;
				while( ($f=readdir($IDcurDir))!==false )
					 if(  !in_array( $f, $Others )&&(  !is_dir( "$CurDir/$f" ) ))        
						 $i++;
				closedir( $IDcurDir );
				$IDaboutFile = fopen( $_GALLERY[ "PATH_DATA" ]."/$CurFile/".$_GALLERY[ "ABOUT" ], "r" );
				$coment = rtrim( fgets( $IDaboutFile ));
				fclose( $IDaboutFile );
				fputs( $IDstatFile, "$CurFile\n$coment\n$i\n" );
				$p++;
				$files+=$i;
			 }
		}
		closedir( $IDdir );
		fclose( $IDstatFile );
	}

	/**
	*
	*/
	function stat_show()
	{
		global $_GALLERY;
		if (!is_file($_GALLERY['STAT_FILE'])) GALLERY::stat_calc();
		if (!is_file($_GALLERY['STAT_FILE'])) {
			$_GALLERY['ERROR'] = 'Couldn`t create STAT file.';
			return false;
		}

		if (is_file($_CORE->SiteModDir."/counter.head.html"))
			include $_CORE->SiteModDir."/counter.head.html";
		else 
			include $_CORE->CoreModDir."/_counter.head.html";
		$IDfile = fopen( $_GALLERY['STAT_FILE'], "r" );
		$p = $files = 0;
		while( !feof( $IDfile ) ){ 
			$path	= fgets( $IDfile );
			$coment	= fgets( $IDfile );
			$i		= fgets( $IDfile );
			$files += $i;
			$p++;
			if ($i > 0)
			if (is_file($_CORE->SiteModDir."/counter.row.html"))
				include $_CORE->SiteModDir."/counter.row.html";
			else 
				include $_CORE->CoreModDir."/_counter.row.html";
		 } 
		fclose($IDfile); 
		if (is_file($_CORE->SiteModDir."/counter.foot.html"))
			include $_CORE->SiteModDir."/counter.foot.html";
		else 
			include $_CORE->CoreModDir."/_counter.foot.html";
	}
} 


?>