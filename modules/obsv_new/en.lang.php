<?
#
# LANG RU
#

define ("OBSV_ERR_WRONG_EMAIL","Wrong E-mail.");
define ("OBSV_ERR_EMPTY_BODY","Empty message.");
define ("OBSV_ERR_LARGE_BODY","Message too long.");
define ("OBSV_ERR_EMPTY_NAME","Enter your name.");

define ("OBSV_ERR_NOT_SEND",'Your question couldn`t be send, please try to do it later.');
define ("OBSV_ERR_SEND",'Message sent.');

?>