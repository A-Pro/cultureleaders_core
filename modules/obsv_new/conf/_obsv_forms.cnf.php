<?php
$GLOBALS['_obsv_forms'] = array (
	'form_data'=>array(
	  'form_id' => 
	  array (
	    'field_name' => 'form_id',
	    'name' => 'form[form_id]',
	    'title' => 'form_id',
	    'must' => 0,
	    'data_type' => 'bigint(20) unsigned',
	    'primary_key' => true,
	    'type' => 'hidden',
	  ),
	  'name' => 
	  array (
	    'field_name' => 'name',
	    'name' => 'form[name]',
	    'title' => 'name',
	    'must' => 1,
	    'data_type' => 'varchar(255)',
	    'size' => 50,
	    'maxlen' => 255,
	    'type' => 'textbox',
	  ),
	  'table' => 
	  array (
	    'field_name' => 'table',
	    'name' => 'form[table]',
	    'title' => 'table',
	    'must' => 0,
	    'data_type' => 'varchar(255)',
	    'size' => 50,
	    'maxlen' => 255,
	    'type' => 'textbox',
	  ),
	  'extra' => 
	  array (
	    'field_name' => 'extra',
	    'name' => 'form[extra]',
	    'title' => 'extra',
	    'data_type' => 'varchar(255)',
	    'size' => 50,
	    'maxlen' => 255,
	    'type' => 'hidden',
	  ),
	 ),
	 'id'=>'form_id',
);
//save by  Configurator on 2006-08-14 15:38:52 ?>
