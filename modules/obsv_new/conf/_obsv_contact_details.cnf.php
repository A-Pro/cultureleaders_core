<?php
$GLOBALS['_obsv_contact_details'] = array (
"form_data" => array(
  'contact_id' => 
  array (
    'field_name' => 'contact_id',
    'name' => 'form[contact_id]',
    'title' => 'contact_id',
    'must' => 0,
    'data_type' => 'bigint(20) unsigned',
    'primary_key' => true,
    'type' => 'hidden',
  ),
  'department' => 
  array (
    'field_name' => 'department',
    'name' => 'form[department]',
    'title' => 'department',
    'must' => 1,
    'data_type' => 'varchar(255)',
    'size' => 50,
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  'name' => 
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => 'name',
    'must' => 0,
    'data_type' => 'varchar(255)',
    'size' => 50,
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  'address1' => 
  array (
    'field_name' => 'address1',
    'name' => 'form[address1]',
    'title' => 'address1',
    'must' => 0,
    'data_type' => 'varchar(255)',
    'size' => 50,
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  'address2' => 
  array (
    'field_name' => 'address2',
    'name' => 'form[address2]',
    'title' => 'address2',
    'must' => 0,
    'data_type' => 'varchar(255)',
    'size' => 50,
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  'town' => 
  array (
    'field_name' => 'town',
    'name' => 'form[town]',
    'title' => 'town',
    'must' => 0,
    'data_type' => 'varchar(255)',
    'size' => 50,
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  'state' => 
  array (
    'field_name' => 'state',
    'name' => 'form[state]',
    'title' => 'state',
    'must' => 0,
    'data_type' => 'varchar(255)',
    'size' => 50,
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  'country' => 
  array (
    'field_name' => 'country',
    'name' => 'form[country]',
    'title' => 'country',
    'must' => 0,
    'data_type' => 'varchar(255)',
    'size' => 50,
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  'telefone' => 
  array (
    'field_name' => 'telefone',
    'name' => 'form[telefone]',
    'title' => 'telefone',
    'must' => 0,
    'data_type' => 'varchar(255)',
    'size' => 50,
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  'fax' => 
  array (
    'field_name' => 'fax',
    'name' => 'form[fax]',
    'title' => 'fax',
    'must' => 0,
    'data_type' => 'varchar(255)',
    'size' => 50,
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  'email' => 
  array (
    'field_name' => 'email',
    'name' => 'form[email]',
    'title' => 'email',
    'must' => 0,
    'data_type' => 'varchar(255)',
    'size' => 50,
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  'extra' => 
  array (
    'field_name' => 'extra',
    'name' => 'form[extra]',
    'title' => 'extra',
    'must' => 0,
    'data_type' => 'varchar(255)',
    'size' => 50,
    'maxlen' => 255,
    'type' => 'hidden',
    'in_list' => false,
  ),
),
"id" => "contact_id",
"onpage" => 20,
);
//save by  Configurator on 2006-08-14 18:39:42 ?>
