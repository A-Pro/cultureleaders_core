<?php
$current_table_alias 	= !empty($_OBSV['table_alias'])?$_OBSV['table_alias']:((isset($_POST['table_alias']))?$_POST['table_alias']:$_SESSION['obsv']['current_table']);

$field_alias 			= $_GET['f'];
$delete_field_flag		= ($_GET['v']=='delete' AND isset($field_alias))?TRUE:FALSE;


if (!empty($current_table_alias)) {
	$_SESSION['obsv']['current_table'] = $current_table_alias;
	
	if (isset($_POST['save'])) Cnf::saveFormData($current_table_alias, $_SESSION['obsv']['table_info']);
	
	if ($delete_field_flag==TRUE&&!empty($field_alias)) {
		Cnf::deleteField($current_table_alias, $field_alias);
	}

	if (isset($current_table_alias)) {
		$_SESSION['obsv']['table_info'] = Cnf::loadTableInfo($current_table_alias);
	}
	
} else {
	unset($_SESSION['obsv']['table_info']);
}

function emptyParametr() {
	return '<font color="red"><b>none</b></font>';
}
?>