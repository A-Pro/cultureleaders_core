<?php
/**
 * Contains functions for work in templates
 *
 */
class Out {
	


	/**
	 * Returns TRUE if the page navigation template with $tpl_name exists and is a regular file.
	 *
	 * @param string $tpl_name
	 * @param string $file
	 * @return boolean
	 */
	function inc_tpl( $tpl_name, &$file )
	{   $file = $_SERVER['DOCUMENT_ROOT']."/".TPL."/pline/".$tpl_name;
		return is_file( $file );
	}
	
	/**
	 * Take out page naviagtion html-code
	 * 
	 * <code>
	 * pline.head.html
	 * pline.dot.html
	 * pline.delimeter.html
	 * pline.current.html
	 * pline.link.html
	 * pline.foot.html
	 * </code>
	 *
	 * @uses Out::inc_tpl()
	 * @param int $all
	 * @param int $p
	 * @param int $onpage
	 * @return string
	 */
     function pline($all, $p, $onpage)
     {
               if (intval($onpage) == 0) return '';

               $string = str_replace("&p=$p", "", $_SERVER['QUERY_STRING']);

               if ($string != '') $string .= '&';
               $uri      = $_SERVER['REQUEST_URI'];
               ob_start();

               if (Out::inc_tpl('pline.head.html', $file))
                    include "$file";

               $pages =  ceil($all / $onpage);

               if ($pages <= 1){
                    ob_end_clean();
                    return '';
               }
               $inline = min(10, $pages);
               if ($p < $inline) {
                         $from = 1; $to = $from + $inline -1;
               }elseif( $p > $pages - $inline) {
                         $from = max($pages - $inline, 1); $to = $pages;
               }else {
                         $from   = max(1, $p - floor($inline / 2)); $to  = min($pages, $p + floor($inline / 2))+1;
               }

               if ($from > 1) {
                    $dot_mean = $from-1;
                    if (Out::inc_tpl('pline.dot.html', $file))
                         include "$file";
 					else echo "<a href='$uri?".$string."p=".$dot_mean."'>...</a>";


                    if (Out::inc_tpl('pline.delimeter.html', $file))
                         include "$file";
                    else echo "&nbsp;|&nbsp;";

               }

               for( $i = $from; $i <= $to; $i++ ) {
                         if ($i == $p) {
                      if (Out::inc_tpl('pline.current.html', $file))           include "$file";
                      else 							                           echo $i;
                         }else{
                              if (Out::inc_tpl('pline.link.html', $file))
                                   include "$file";
                              else 
                                   echo "<a href='$uri?".$string."p=$i'>".($i)."</a>";

                         }
                         if ($i < $to || $pages-1 > $to) {
                              if (Out::inc_tpl('pline.delimeter.html', $file))
                                   include "$file";
                              else 
                                   echo "&nbsp;|&nbsp;";
                         }
               }
               if ($pages-1 > $to) {
                    $dot_mean = $i;
                    if (Out::inc_tpl('pline.dot.html', $file))
                         include "$file";
                    else 
                         echo "<a href='$uri?".$string."p=".$dot_mean."'>...</a>";

               }

                    if (Out::inc_tpl('pline.foot.html', $file))
                         include "$file";

               $tmp = ob_get_contents();
               ob_end_clean();
               return 'Pages: '.$tmp;
     }	

	/**
	 * Take out list of links with filter string to dependent tables
	 * 
	 * @param array $dependences
	 * @param int $record_id
	 */
	function dependencesFilter($dependences, $record_id)
	{
		if (is_array($dependences)) {
			foreach ($dependences as $dependence => $table) { ?>
				<div><a href="/?t=<?=$dependence?>&a=list&filter[<?=$GLOBALS[$GLOBALS['table_alias']]['id']?>]=<?=$record_id?>" onclick="window.opener.location.href=this.href;self.close();">View <?=$dependence?></a></div><?
			}
		}
	}	
	
	/**
	 * Return page caption
	 *
	 * @param string $action
	 * @return string
	 */
	function pageCaption($action='') {
		if ($action == 'list') {
			return $GLOBALS[$GLOBALS['table_alias']]['caption'];
		} elseif(isset($action)) {
			return '<a href="index.php?t='.$GLOBALS['table_alias'].'&a=list">'.$GLOBALS[$GLOBALS['table_alias']]['caption'].'</a>';
		}
	}

	/**
	 * Return page caption
	 *
	 * @param string $action
	 * @return string
	 */
	function actionCaption($action='') {
		switch ($action) {
			case 'view_add_form':
				return 'Add';				
			break;
			case 'view_edit_form':
				return 'Edit';				
			break;
			case 'view_csv_form':
				return 'CSV exchange';				
			break;
		}
	}
	
	/**
	 * Take out tree path panel html-code
	 *
	 * @uses Tools::getFilterByName()
	 * @param string $table
	 * @param int $parent_id
	 * @param string $name
	 * @param int $tree_id
	 * @return string
	 */
	function newTreePathPanel($table, $action='list', $parent_id='parent_id', $name='name', $tree_id='tree_id') { // Added by Shesternin
		if (empty($table)) $table = $GLOBALS['table_alias'];
		global $tools;
		if ($tools->getFilterByName($parent_id)==0){
			$path = $GLOBALS[$table]['section_caption'];
		} else {
			$res = SQL::sel( "$name, $parent_id", $table, "$tree_id='".$tools->getFilterByName($parent_id)."'"  );
			if ($res->NumRows==1) {
				$res->FetchArray(1,1);
				$pid = $res->FetchArray[$parent_id];
				$name_f = $res->FetchArray[$name];
				$path = " / ".$name_f;
			}
			while ($pid!=0) {
				$res = SQL::sel( "$name, $parent_id", $table, "$tree_id='".$pid."'"  );
				if ($res->NumRows==1) {
					$res->FetchArray(1,1);
					$name_f = $res->FetchArray[$name];
					$path = " / <a href=\"/index.php?t=".$table."&a=".$action."&filter[parent_id]=".$pid."\">".$name_f."</a>".$path;
					$pid = $res->FetchArray[$parent_id];
				}
			}
			$path = "<a href=\"/index.php?t=".$table."&a=".$action."\">".$GLOBALS[$table]['section_caption']."</a>"." ".$path;
		}
		return $path;
	}

	/**
	 * Finishes script work
	 *
	 */
	function page404() {
		die('404');
	}

	/**
	 * Get field title
	 *
	 * @uses Page::LoadCnfArray()
	 * @param string $field_alias
	 * @param string $second_table
	 * @param string $first_table
	 * @return string
	 */
	function getFieldTitle($field_alias,  $first_table='') {
		if (empty($first_table)) {
			$first_table = $GLOBALS['table_alias'];
		}
		if (!empty($GLOBALS[$first_table]['form_data'][$field_alias]['title'])) {
			$tmp = $GLOBALS[$first_table]['form_data'][$field_alias]['title'];
		} 
		return !empty($tmp)?$tmp:$field_alias;
	}

	/**
	 * Check-up the field visibility (based on the field description)
	 *
	 * @uses Page::LoadCnfArray()
	 * @param string $field_alias
	 * @param string $second_table
	 * @param string $first_table
	 * @return boolean
	 */
	function checkFieldVisibility($field_alias, $first_table='') {
		if (empty($first_table)) {
			$first_table = $GLOBALS['table_alias'];
		}
		if (empty($first_table)||!isset($GLOBALS[$first_table]['form_data'][$field_alias])) {
			return false;
		}
		if (isset($GLOBALS[$first_table]['form_data'][$field_alias]['in_list'])||$GLOBALS[$first_table]['id'] == $field_alias) {
			return FALSE;
		} 
		return TRUE;
	}

	/**
	 * Check-up hidden field (based on the field description)
	 *
	 * @uses Page::LoadCnfArray()
	 * @param string $field_alias
	 * @param string $first_table
	 * @return boolean
	 */
	function checkHiddenField($field_alias, $first_table='') {
		if (empty($first_table)) {
			$first_table = $GLOBALS['table_alias'];
		} else Page::LoadCnfArray($first_table); 
		if (!empty($GLOBALS[$first_table]['form_data'][$field_alias])) {
			if ($GLOBALS[$first_table]['form_data'][$field_alias]['type'] == 'hidden') {
				return FALSE;
			}
		}
		return TRUE;
	}
	
	/**
	 * Check-up search by the field ability (based on the field description)
	 *
	 * @param string $field_alias
	 * @return boolean
	 */
	function checkSearchField($field_alias) {
		$exceptions_arr = array('select_from_table', 'select_from_tree', 'select_popup');
		if (!in_array($GLOBALS[$GLOBALS['table_alias']]['form_data'][$field_alias]['type'], $exceptions_arr)) {
			return TRUE;
		}
		return FALSE;
	}
	
	/**
	 * Attempt to create the HTML-copy of page and to convert this HTML-copy to PDF file
	 *
	 * @param string $buffer
	 * @param string $request_uri
	 */
	function viewInPDF($buffer, $request_uri) {
		$path = TMP;
		if (file_exists($path)) {
			$url = parse_url($request_uri);
			parse_str($url['query'], $vars);
			$uniq = ($_COOKIE['PHPSESSID'])?$_COOKIE['PHPSESSID']:md5(rand(100,999)*time());
			$filename = $vars['t'].'_'.$vars['a'].'_'.$uniq;
			$fp = fopen($path.'/'.$filename.'.html', 'w');
			fwrite($fp, $buffer);
			fclose($fp); 
			ob_clean();
						
			// will be replaced!
			system('htmldoc -t pdf --webpage -f '.$path.'/'.$filename.'.pdf '.$path.'/'.$filename.'.html');
			
			header("Content-type: application/pdf");
			header("Content-Disposition: attachment; filename=".$vars['t'].'_'.$vars['a'].".pdf");
			if (file_exists($path.'/'.$filename.'.pdf') AND file_exists($path.'/'.$filename.'.html')) {
				if (readfile($path.'/'.$filename.'.pdf')) {
					unlink($path.'/'.$filename.'.pdf');
					unlink($path.'/'.$filename.'.html');
				}
				return;
			} 
		}
		return;
	}
} 
?>