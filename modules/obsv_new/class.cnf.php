<? class Cnf {
	

	
	
	function deleteField($current_table_alias, $field_alias) {
		$cnf = Cnf::loadTableInfo($current_table_alias);
		//del from mysql table
		if (!empty($cnf['form_data'][$field_alias])) {
			$cform = new cForm($cnf['form_data']);
			$cform->delColumn($current_table_alias, $field_alias);
			unset($_SESSION['obsv']['table_info']['form_data'][$field_alias]);
			unset($cnf['form_data'][$field_alias]);
			Cnf::saveTableInfo($current_table_alias, $cnf);
			return true;
		}
		return false;
	}

	function autoDesribeTable($current_table_alias) {
		$tmp = array();
		$cform = new cForm($tmp);
		return $cform->describeTable($current_table_alias, false);;
	}	
	
	function addField($current_table_alias, $field_alias) {
		$cnf = Cnf::loadTableInfo($current_table_alias);
		if (isset($_SESSION['obsv']['table_info']['form_data'][$field_alias])) {
			$cnf['form_data'][$field_alias] = $_SESSION['obsv']['table_info']['form_data'][$field_alias];
			$cform = new cForm($cnf['form_data']);
			$cform->addColumn($current_table_alias, $field_alias);
			Cnf::saveTableInfo($current_table_alias, $cnf);
			return true;
		}
		return false;		
	}
	
	function editField($current_table_alias, $old_field, $new_field) {
		$cnf = Cnf::loadTableInfo($current_table_alias);
		if (isset($_SESSION['obsv']['table_info']['form_data'][$new_field])) {
			unset($cnf['form_data'][$old_field]);
			unset($_SESSION['obsv']['table_info']['form_data'][$old_field]);
		
			$cnf['form_data'][$new_field] = $_SESSION['obsv']['table_info']['form_data'][$new_field];
			$cform = new cForm($cnf['form_data']);
			$cform->delColumn($current_table_alias, $old_field);
			$cform->addColumn($current_table_alias, $new_field);
			Cnf::saveTableInfo($current_table_alias, $cnf);
			return true;
		}
		return false;
	}
	
	function editSimpleField($current_table_alias, $field_alias) {
		$cnf = Cnf::loadTableInfo($current_table_alias);
		if (isset($_SESSION['obsv']['table_info'][$field_alias])) {
			$cnf[$field_alias] = $_SESSION['obsv']['table_info'][$field_alias];
			Cnf::saveTableInfo($current_table_alias, $cnf);
			return true;
		}	
		return false;
	}
	
	function saveTableInfo($current_table_alias, &$cnf) {
		if (empty($current_table_alias)) die("empty alias!");
		if (!empty($cnf['id'])&&isset($cnf['form_data'][$cnf['id']])) {
			foreach ($cnf['form_data'] as $fn=>$fv)
				if (isset($fv['primary_key'])) unset($cnf['form_data'][$fn]['primary_key']);
			$cnf['form_data'][$cnf['id']]['primary_key'] = true;
		}
		if (!file_exists(SRC_CONF.'/'.$current_table_alias.'.cnf.php')) {
			$cnf['id']		= 'id';
			$cnf['onpage']	= 20;
			$cnf['form_data'][$cnf['id']] =     array (
													'field_name' => 'id',
													'name' => 'form[id]',
													'title' => 'id',
													'must' => 0,
													'data_type' => 'bigint(20) unsigned',
													'type' => 'hidden',
													'primary_key' => true,
												);
			$cformSave = new cForm($cnf['form_data']);
			$cformSave->createTable($current_table_alias);
		}
		$file = fopen(SRC_CONF.'/'.$current_table_alias.'.cnf.php', "w");
		fputs($file, "<?php\n");
		fputs($file, "\$GLOBALS['".$current_table_alias."'] = ".var_export($cnf, true).";\n");
		fputs($file, "//save by  Configurator on ".date("Y-m-d H:i:s")." ?>\n");
		fclose($file);
	}	
	
	
	
	
	
	function loadTableInfo($current_table_alias) {
		global $_ALIASES;
		if (file_exists(SRC_CONF.'/'.$current_table_alias.'.cnf.php')) {
			include SRC_CONF.'/'.$current_table_alias.'.cnf.php';
		}
		return $GLOBALS[$current_table_alias];
	}		
	
	
	
	function getFieldType($types_arr, $field_type) {
		$types_list = '';
		foreach($types_arr as $key => $type_alias) {
			$is_selected = ($field_type == $type_alias)?'selected':'';
			$types_list .= '<option '.$is_selected.' value="'.$type_alias.'">'.$type_alias.'</option>';
			$is_selected = ($field_type == $type_alias)?'':'';
		}
		return $types_list;
	}	
	
	function getFormDataParams(&$field, $type_alias) {
		switch ($type_alias) {
			case 'TextBox':
				switch ($_REQUEST['fields']['Size']) {
					case "0":  // SMALL
						$field['type'] = "textbox";
						$field['maxlen'] = 255;
						$field['extra_type'] = "TextBox";
						$field['extra_size'] = "SMALL";
					break;						
					case "1":  // MEDIUM
						$field['type'] = "textarea";
						$field['maxlen'] = 2060;
						$field['rows'] = 3;
						$field['extra_type'] = "TextBox";
						$field['extra_size'] = "MEDIUM";
					break;						
					case "2":  // BIG
						$field['type'] = "textarea";
						$field['maxlen'] = 5120;
						$field['rows'] = 5;
						$field['extra_type'] = "TextBox";
						$field['extra_size'] = "BIG";
					break;						
				}
			break;
			case 'OptionBox':
				$field['type'] = "select";
				$field['arr'] = array();
				foreach($_REQUEST['fields']['Section'] as $v)
					if (!empty($v)) $field['arr'][]=$v;
			break;
			case 'CheckBox':
				return true;
			break;
			default: return false;
			
		}
		$field['Type'] = $type_alias;
		return true;
	}
	
	
	function getTypeParams($field_alias, $type_alias) {
		global $_OBSV;
		switch ($type_alias) {
			case 'TextBox':
				$type_params['Size'] = "<select name=\"fields[Size]\"><option value=0>SMALL<option value=1>MEDIUM<option value=2>BIG</select>";
				$type_params['Content Type'] = "<select name=\"fields[Content Type]\">";
				foreach ($_OBSV['TextBox']['Content Type'] as $k=>$v)
					$type_params['Content Type'] .= "<option value=$v>".$v;
				$type_params['Content Type'] .= "</select>";
			break;
			case 'OptionBox':
				$type_params['Number of options avaliable'] = "<select name=\"fields[Number of options avaliable]\" onChange=\"form.submit();\">";
				for($i=1; $i<101; $i++) {
					$type_params['Number of options avaliable'] .= "<option value=$i ".(@$_REQUEST['fields']['Number of options avaliable']==$i?"selected":"").">$i";
				}
				$type_params['Number of options avaliable'] .= "</select>";
				$_POST['fields']['Number of options avaliable'] = empty($_POST['fields']['Number of options avaliable'])?1:$_POST['fields']['Number of options avaliable'];
				for($i=1; $i<1+$_POST['fields']['Number of options avaliable']; $i++) {
					$type_params["Section $i"] = "<input name=\"fields[Section][$i]\" value=".(empty($_REQUEST['fields']["Section"][$i])?"":$_REQUEST['fields']["Section"][$i]).">";
				}
			break;
			case 'CheckBox':
				$type_params['Number of options avaliable'] = "<select name=\"fields[Number of options avaliable]\" onChange=\"form.submit();\">";
				for($i=1; $i<101; $i++) {
					$type_params['Number of options avaliable'] .= "<option value=$i ".(@$_REQUEST['fields']['Number of options avaliable']==$i?"selected":"").">$i";
				}
				$type_params['Number of options avaliable'] .= "</select>";
				$_POST['fields']['Number of options avaliable'] = empty($_POST['fields']['Number of options avaliable'])?1:$_POST['fields']['Number of options avaliable'];
				for($i=1; $i<1+$_POST['fields']['Number of options avaliable']; $i++) {
					$type_params["Section $i"] = "<input name=\"fields[Section][$i]\" value=".(empty($_REQUEST['fields']["Section"][$i])?"":$_REQUEST['fields']["Section"][$i]).">";
				}
			break;
			
		}
		return $type_params;
	}
	

	
	function getTablesList($files_list, $current_table_alias = '', $exclusions = '') {
		$exclusions_arr = ($exclusions==1)?array($_SESSION['obsv']['current_table'], 'home'):array();
		$tables_list = '';
		foreach($files_list as $key => $filename) {
			$filename = explode('.', $filename);
			$alias = $filename[0];
			if (!in_array($alias, $exclusions_arr)) {
				if (!empty($alias)) {
					$is_selected = ($current_table_alias == $alias)?'selected':'';
					$tables_list .= '<option '.$is_selected.' value="'.$alias.'">'.$alias.'</option>';
					$is_selected = ($current_table_alias == $alias)?'':'';
				}
			}
		}
		return $tables_list;
	}
	
	
	
	
	function scandir2($dir = './', $sort = 0) {
		$dir_open = @opendir($dir);
		if (!$dir_open) return false; 
		while (($dir_content = readdir($dir_open)) !== false) $files[] = $dir_content;
		if ($sort == 1) rsort($files, SORT_STRING); 
		else sort($files, SORT_STRING);
		return $files;
	}
}
?>