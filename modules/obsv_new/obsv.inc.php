<?

//define ("TO","andruha@programist.ru");
//define ("TO","online@new-technologies.ru");

$_OBSV['ERROR']	= '';

$_OBSV['MODULE_NAME']		= "obsv";
$_OBSV['MODULE_DESC']		= ($_CORE->LANG != 'en')?"������ �������� �����.":'';
$_OBSV['MODULE_TITLE']	= ($_CORE->LANG != 'en')?"�������� �����":"Contact us";

$_OBSV['ADM_MENU']	= array(
	"/obsv/forms/" => array("/ico/a_tree.gif", $_OBSV['MODULE_TITLE'], $_OBSV['MODULE_NAME']),
	);


class OBSV {

	/**
	*
	*/
	function check( &$data )
	{
		global $_OBSV;
		if (!OBSV::is_email($data['email'])) {
			$_OBSV['ERROR']	.= OBSV_ERR_WRONG_EMAIL."\n";
		}
		$data['name'] = strip_tags($data['name']);
		if (empty($data['name'])) {
			$_OBSV['ERROR']	.= OBSV_ERR_EMPTY_NAME."\n";
		}
		$data['body'] = strip_tags($data['body']);
		if (empty($data['body'])) {
			$_OBSV['ERROR']	.= OBSV_ERR_EMPTY_BODY."\n";
		}elseif (strlen($data['body']) > 2048) {
			$_OBSV['ERROR']	.= OBSV_ERR_LARGE_BODY."\n";
		}
		return (empty($_OBSV['ERROR']));
	}

	/**
	*
	*/
	function send( &$data )
	{
		$message = convert_cyr_string ($data['body'], "w", "k");
		$data['name'] = convert_cyr_string ($data['name'], "w", "k");
		$subject = convert_cyr_string ('������ � ����� '.$_SERVER['HTTP_HOST'], "w", "k");
		//------- �������� ��������� --------
		if ($type=="html"){
		 $headers="MIME-Version: 1.0\r\nContent-Type: text/html; charset=koi8-r;\r\nContent-transfer-Encoding: 8bit\r\nX-Mailer: PHP.Lands form Goernment./";
		}else{
		  $headers="MIME-Version: 1.0\r\nContent-Type: text/plain; charset=koi8-r\r\nContent-transfer-Encoding: 8bit\r\nX-Mailer: PHP.Lands form Goernment./";
		}

		$test_str = preg_replace("/(.)/e", "sprintf('=%02x',ord('$1'))", $data['name']);
		$data['name'] = '=?KOI8-R?Q?'.$test_str.'?=';

		$from	= "From: ".$data['name']."<".$data['email'].">\r\n".$headers;


		mail(TO, $subject, $message, $from);
	}
	
	function getData (&$postdata, &$cnf) {
		if (!empty($postdata)&&!empty($cnf)) {
			$str = "";
			foreach ($cnf['form_data'] as $alias=>$field) 
				if ((!empty($postdata[$alias])||$field['type']=='checkbox')&&$field['type']!='hidden') {
					if ($field['type']=='select') {
						$str .= $field['title'].": ".$field['arr'][$postdata[$alias]]."\n";
					} elseif ($field['type']=='checkbox') {
						$str .= $field['title'].": ".(empty($postdata[$alias])?"nocheked":"checked")."\n";
					} else $str .= $field['title'].": ".$postdata[$alias]."\n";
				}
			
			return $str;
		}
		return false;
	}
	
	function send_new( &$data )
	{
		$message = convert_cyr_string ($data['body'], "w", "k");
//		$data['name'] = convert_cyr_string ($data['name'], "w", "k");
		$subject = convert_cyr_string ('Message from site: '.$_SERVER['HTTP_HOST'], "w", "k");
		//------- �������� ��������� --------
		if ($type=="html"){
		 $headers="MIME-Version: 1.0\r\nContent-Type: text/html; charset=koi8-r;\r\nContent-transfer-Encoding: 8bit\r\nX-Mailer: PHP./";
		}else{
		  $headers="MIME-Version: 1.0\r\nContent-Type: text/plain; charset=koi8-r\r\nContent-transfer-Encoding: 8bit\r\nX-Mailer: PHP./";
		}

//		$test_str = preg_replace("/(.)/e", "sprintf('=%02x',ord('$1'))", $data['name']);
//		$data['name'] = '=?KOI8-R?Q?'.$test_str.'?=';

//		$from	= "From: ".$data['name']."<".$data['email'].">\r\n".$headers;

		if (OBSV::is_email($data['email'])) {
			//echo("To: ".$data['name']."<".$data['email']."><hr>$subject<hr>$message<hr>$headers<hr><hr>");
			mail($data['email'], $subject, "To ".$data['name']."\n".$message, $headers);
			echo "sendto: ".$data['name']."<br>";
		}			
	}
	/**
	*
	*/
	function is_email( $email )
	{
		if ( empty( $email ) || ! strchr( $email, '@' )) return 0;
		list($first, $domain) = explode("@", $email);
		$point = strpos($domain, ".");
		if ( strstr($email, " ") || $point == 0 || $point == strlen( $domain ) || strlen($first) == 0 ) return 0;
		else return 1;
	}
} 

?>