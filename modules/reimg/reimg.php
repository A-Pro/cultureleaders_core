<?php
set_time_limit(600);
global $_CORE, $_REIMG;
# colour- & textvalues
$picBG = "66,66,66"; # RGB-value !
$picFG = "104,104,104"; # RGB-value !
//$copyright = "(c) land"; 
$font = 1;
$xSpace = 9;
$ySpace = 3;

switch ($Cmd) {
		case "":
		return;
		break;
		case "clearhash":

            function clearDir( $dir, $level = 0 ) {
            	if ($objs = glob($dir."/*")) {
            		foreach($objs as $obj) {
            			if ( is_dir($obj) ) { 
            			    clearDir($obj, $level+1);
            			}
            			else {
            				if ($level>0) {
                                if( !isset($_GET['name_file']) || $_GET['name_file'] == basename($obj)){
                                    unlink($obj);
                                    if( !isset($_GET['noprint']) ) echo 'Delete ' . $obj . '<br>';
                                }
            				}
            			}
            		}
            	}
            }
            
            clearDir('reimg/data');
            if( !isset($_GET['noprint']) )
                echo "Очистка кеша reimg завершена"; 
            // die();
			break;
            default:
$image 	= $Cmd;
if (empty($image) && !empty($_SERVER['QUERY_STRING'])) {
	list($image,$_SERVER['QUERY_STRING']) = explode('?',$_SERVER['QUERY_STRING']);
}
list($size,$stretch) = explode('&',$_SERVER['QUERY_STRING']);
if(!empty($stretch) && $stretch == 'nostretch'){ $_REIMG['stretch'] = ''; }
 
$_SERVER['QUERY_STRING'] = $size;
list($maxX,$maxY) = explode('x',$_SERVER['QUERY_STRING']);
@include_once $_CORE->SiteModDir."/reimg.conf.php";

$nopicurl  = ($_REIMG['default_nopic']) ? $_REIMG['default_nopic'] : "/images/pic_none.jpg"; # starting in $imagepath!!!
$nofileurl = ($_REIMG['default_nopic']) ? $_REIMG['default_nopic'] : "/images/pic_none.jpg"; # starting in $imagepath!!!


/**
 * For external images
 */
if (substr($image,0,7) == 'http://') {

	if (!empty($_REIMG['allow_external']) && is_dir($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'reimg')) {

		$file_name = DIRECTORY_SEPARATOR.'reimg'.DIRECTORY_SEPARATOR.basename($image);
		$file_path = $_SERVER['DOCUMENT_ROOT'];
		// if not present
		if (is_file($file_path.$file_name) || copy($image, $file_path.$file_name)) {
			$image = '/reimg/'.basename($image);
		}
	}else{ 
		header("Location: $image");
		exit;
	}
}

/**
 * Deller 22.09.2011
 * Функционал создания директории для сохранения кэша файлов
 * START CREATE DIRS 
 */
if (empty($_REIMG['no_save_to_cache'])) {
  $pathes = explode('/', trim($image, '/'));
  $path   = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'reimg';
  $file   = array_pop($pathes);
  if (empty($_REIMG['nginx_mode'])) {
  	$hw = $_SERVER['QUERY_STRING']; 
  } else {
  	$hw    = array_pop($pathes);
  	$image = implode('/', $pathes) . '/' . $file;
  }
  list($maxX,$maxY) = explode('x', $hw);
  
  if(!empty($hw)) { // Если мы знаем как ресайзить
  	if( !substr_count( $hw, '..') ) {
  		$pathes[] = $hw;
  	}
  } else { // Иначе отдаем картинку без изменений
  	header("Location: " . str_replace('//', '/', "/{$image}"));
  	exit;
  }
  
  $c = count($pathes);
  for ( $i=-1;$i<$c;$i++ ) {
  	if($pathes[$i] != '..') {
  		$path = $path.DIRECTORY_SEPARATOR.$pathes[$i];
  		// echo $path. "<br>";
  		if(!file_exists($path)) {
  			mkdir($path,0777);
  			if($i == -1) {
  				@file_put_contents($path.'.htaccess',"RewriteEngine On\nRewriteBase /reimg\nRewriteCond %{REQUEST_FILENAME}	!-f\nRewriteRule ^(.*)/([^/]*\.(jpg|png|gif))$ $1/%{QUERY_STRING}/$2 [NC]\nRewriteCond %{SCRIPT_FILENAME} !-f\nRewriteRule ^(.*)$ ../core_st.php [NC]\nphp_flag engine 0");
  			}
  		}
  	}
  }
}
/**
 * END CREATE DIRS
 */

# standard height & weight if not given
//if(empty($maxX)) $maxX = 100;
//if(empty($maxY)) $maxY = 75;

# minimal & maximum zoom
$minZoom = 1; # per cent related on orginal (!=0)
$maxZoom = 500; # per cent related on orginal (!=0)
# paths
$imgpath = $_SERVER['DOCUMENT_ROOT']."/"; # ending with "/" !

if(empty($image))
	$imageurl = $imgpath . $nopicurl;

elseif(! file_exists($imgpath . trim($image)))
	$imageurl = $imgpath . $nofileurl;

else
	$imageurl = $imgpath . trim($image);
$images = $image;
# reading image
$image = getImageSize($imageurl, $info); # $info, only to handle problems with earlier php versions...

switch($image[2]) {
  case 1:
    # GIF image
    //$timg = imageCreateFromGIF($imageurl);
//    echo trim($images).'!!!';
//    die();
	header('Location: /'.trim($images));
	die();
    break;
case 2:
    # JPEG image
    $timg = imageCreateFromJPEG($imageurl);
    break;
case 3:
    # PNG image
    $timg = imageCreateFromPNG($imageurl);
    break;

}


# reading image sizes
$imgX = $image[0];
$imgY = $image[1];

if (empty($maxX)&&empty($maxY)) {
	$maxX = $imgX;
	$maxY = $imgY;
}

if ($maxX > $imgX && empty($maxY) && empty($_REIMG['stretch'])){
	$maxX = $imgX;
	$maxY = $imgY;
}

if ($maxX > $imgX && $maxY > $imgY && empty($_REIMG['stretch'])){
	$maxX = $imgX;
	$maxY = $imgY;
}

if (empty($maxX)){
	# calculation zoom factor 
	$_Y = $imgY/$maxY;
	$maxX = ceil($imgX / $_Y);
}

if (empty($maxY)){
	# calculation zoom factor 
	$_X = $imgX/$maxX;
	$maxY = ceil($imgY / $_X);
}

# calculation zoom factor
$_X = $imgX/$maxX * 100;
$_Y = $imgY/$maxY * 100;

# selecting correct zoom factor, so that the image always keeps in the given format
# no matter if it is more higher than wider or the other way around isset($_REQUEST['fill'])
if( ( $_X < $_Y && isset($_REQUEST['fill'])) || ( $_X > $_Y && !isset($_REQUEST['fill'])) ) $_K = $_X;
else $_K = $_Y;

# zoom check to the original
if($_K > 10000/$minZoom) $_K = 10000/$minZoom;
if($_K < 10000/$maxZoom) $_K = 10000/$maxZoom;

# calculate new image sizes
$newX = $imgX/$_K * 100;
$newY = $imgY/$_K * 100;

# set start positoin of the image
# always centered 
$posX = ($maxX-$newX) / 2;
$posY = ($maxY-$newY) / 2;

# creating new image with given sizes
$imgh = imageCreateTrueColor($newX, $newY); // maxX maxY
imagealphablending($imgh, FALSE);

# setting colours
$cols = explode(",", $picBG);
$bgcol = imageColorallocate($imgh, trim($cols[0]), trim($cols[1]), trim($cols[2]));
$cols = explode(",", $picFG);
$fgcol = imageColorallocate($imgh, trim($cols[0]), trim($cols[1]), trim($cols[2]));
# fill background
//imageFill($imgh, 0, 0, $bgcol);

# create small copy of the image
$width=$image[0];
$height=$image[1];

$new_width = $newX;
$new_height = $newY;

if ($new_width > 0)               $x = $new_width / $width;
if ($new_height > 0)              $y = $new_height / $height;
if (($x > 0 && $y > $x) || $x==0) $x = $y;
$width_big = $width * $x;
$height_big = $height * $x;
//$dst_img = imagecreatetruecolor($new_width,$new_height);
//$tmp_img = imagecreatetruecolor($width_big,$height_big);
$copy_res = imagecopyresampled($imgh,$timg,0,0,0,0,$width_big,$height_big,imagesx($timg),imagesy($timg)); // $width_big,$height_big
if(!$copy_res) echo 'bad';
//              	  imagecopy($imgh,$timg,0,0,0,0,$new_width,$new_height);

// imageCopyResampled($imgh, $timg, $posX, $posY, 0, 0, $newX, $newY, $image[0], $image[1]);
# writing copyright note
imageStringUp($imgh, $font, $newX-$xSpace, $newY-$ySpace, $copyright, $fgcol);
# output
switch($image[2]) {
  case 1:
    # GIF image
    header("Content-type: image/gif");
//   @imageGIF($imgh,$path.DIRECTORY_SEPARATOR.$file);
    imageGIF($imgh);
    break;
case 2:
    # JPEG image   
    header("Content-type: image/jpeg");
	if (empty($_REIMG['no_save_to_cache'])) {
		//save to cache
		$res = imageJPEG($imgh,$path.DIRECTORY_SEPARATOR.$file,100);
		$pathfile2 = str_replace($_SERVER['DOCUMENT_ROOT'],'',$path.DIRECTORY_SEPARATOR.$file);
	}else{
    imageJPEG($imgh,'',100);
    die();
  }
 

  break;
case 3:
    # PNG image
    header("Content-type: image/png");
    imagesavealpha($imgh, TRUE);
	if (empty($_REIMG['no_save_to_cache'])) {
	
		//save to cache
		imagePNG($imgh,$path.DIRECTORY_SEPARATOR.$file);
    //echo '1234123452345234 --- ' . $_SERVER['DOCUMENT_ROOT'],'-=--',$path.DIRECTORY_SEPARATOR.$file; exit;
    $pathfile2 = str_replace($_SERVER['DOCUMENT_ROOT'],'',$path.DIRECTORY_SEPARATOR.$file);
	} else {
        imagePNG($imgh);
    
    die();
  }
  break;
}

# cleaning cache
imageDestroy($timg);
imageDestroy($imgh);
  //$_CORE->log_message('$pathfile2 = '.$pathfile2);
  header("Location: ". str_replace(array('\\','//'), '/', $pathfile2 ));
  unset($pathfile2);

exit;
break;
}