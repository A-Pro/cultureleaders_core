<?
$_FLOWGALLERY['MODULE_NAME'] = 'flowgallery'; 
$SWF_FILE	= 'FlowGallery.swf';

if (Main::comm_link($SWF_FILE, $f, 'flowgallery'))
    $SWF = $f;
else
	$SWF = '/modules/'.$_FLOWGALLERY['MODULE_NAME'] . '/' .$SWF_FILE;

$_FLOWGALLERY['DB_DATA_PATH'] = '/data/db/f_'; 
$_FLOWGALLERY['SWF_PATH']	= $SWF; 

$_FLOWGALLERY['BGCOLOR']	= '#ffffff';
$_FLOWGALLERY['WIDTH']		= '480';
$_FLOWGALLERY['HEIGHT']		= '200';

$_FLOWGALLERY['IMAGE_ANGILE']	= '30';
$_FLOWGALLERY['USE_SCROLL']	= 'true';
$_FLOWGALLERY['SLIDESHOW']	= 'false';
$_FLOWGALLERY['SLIDESHOW_DELAY']	= '2';
$_FLOWGALLERY['COLOR_SCHEME']	= 'white';
$_FLOWGALLERY['START_POSITION']	= 'center';
$_FLOWGALLERY['USE_HIGHLIGHT']	= 'true';
$_FLOWGALLERY['REFLECTION_ALPHA']	= '80';

if (file_exists($_CORE->SiteModDir . '/flowgallery.conf.php'))
	include $_CORE->SiteModDir . '/flowgallery.conf.php';

/**
*
*/
function show_swf( $path )
{
	global $_FLOWGALLERY;
	?>
	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="<?=$_FLOWGALLERY['WIDTH']?>" height="<?=$_FLOWGALLERY['HEIGHT']?>" id="gal" align="middle">
	<param name="allowScriptAccess" value="sameDomain" />
	<param name="allowFullScreen" value="false" />
	<param name="flashVars" value="XMLFile=/<?=$_FLOWGALLERY['MODULE_NAME'] . '/xml/' . $path?>">
	<param name="movie" value="<?=$_FLOWGALLERY['SWF_PATH']?>" />
	<param name="quality" value="high" />
	<param name="bgcolor" value="<?=$_FLOWGALLERY['BGCOLOR']?>" />	
	<embed src="<?=$_FLOWGALLERY['SWF_PATH']?>" quality="high" bgcolor="#ffffff" width="<?=$_FLOWGALLERY['WIDTH']?>" height="<?=$_FLOWGALLERY['HEIGHT']?>" name="gal" flashvars="XMLFile=/clear/<?=$_FLOWGALLERY['MODULE_NAME'] . '/xml/' . $path?>" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
	</object>
	<?
}

/**
*
*/
function show_xml( $path )
{
	global $_CORE, $_FLOWGALLERY;
	?>
<gallery>
<settings>
	<imagesFolder><?=$_FLOWGALLERY['DB_DATA_PATH'] . $path?>/</imagesFolder>
	<imageAngle><?=$_FLOWGALLERY['IMAGE_ANGILE']?></imageAngle>
	<useScrollBar><?=$_FLOWGALLERY['USE_SCROLL']?></useScrollBar>
	<slideShow><?=$_FLOWGALLERY['SLIDESHOW']?></slideShow>
	<slideShowDelay><?=$_FLOWGALLERY['SLIDESHOW_DELAY']?></slideShowDelay>
	<flipDuration>1</flipDuration>
	<flipSound></flipSound>
	<colorScheme><?=$_FLOWGALLERY['COLOR_SCHEME']?></colorScheme>
	<startPosition><?=$_FLOWGALLERY['START_POSITION']?></startPosition>	
	<reflectionAlpha><?=$_FLOWGALLERY['REFLECTION_ALPHA']?></reflectionAlpha>
	<useHighlight><?=$_FLOWGALLERY['USE_HIGHLIGHT']?></useHighlight>		
	
</settings>
<items>
	<?
	$arr = FILE::read_dir( $_CORE->ROOT . '/data/db/f_'.$path );
	foreach ($arr as $file) {
		if ($file == '.svn' || !in_array(strtolower(substr($file, -4)), array('.jpg', '.gif', 'jpeg', '.png'))) continue;
		echo "<item><source>$file</source></item>";
	}
	?>
</items>
</gallery>
<?
}

?>