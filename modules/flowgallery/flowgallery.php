<?
/* usage:

echo cmd('/flowgallery/swf/'.$_KAT['KUR_ALIAS'])

may USE project own flowgallery.inc.php
need to put FlowGallery.swf into /template/def/flowgallery/ directory

*/

global $_CORE, $_FLOWGALLERY;

if (is_file($_CORE->SiteModDir . "/flowgallery.inc.php")) {
	include_once $_CORE->SiteModDir . "/flowgallery.inc.php";
}elseif (is_file($_CORE->CoreModDir . "/flowgallery.inc.php")) {
	include_once $_CORE->CoreModDir . "/flowgallery.inc.php";
}else{
	echo "Flow Gallery ERROR -> Conf File not found";
}

list ($Cmd1, $path) = explode('/', $Cmd);

switch ($Cmd1) {
	case "xml":
		ob_end_clean();
		show_xml( $path );
		exit;
	break;
	case "swf":
		show_swf( $path );
	break;
}

?>