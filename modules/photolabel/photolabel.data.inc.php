<?
/*

    [left] => 81
    [top] => 363
    [width] => 140
    [height] => 67
    [img_width] => 570
    [img_height] => 455
    [item_title] => 123 123
    [item_url] => 
    [item_id] => 7
    [creator_id] => -1
    [leftTopX] => 0.14211
    [leftTopY] => 0.79780
    [rightBottomX] => 0.38772
    [rightBottomY] => 0.94505

*/

global $FORM_FIELD4ALIAS;
$FORM_FIELD4ALIAS = 'alias';

$FORM_ORDER	= '	ORDER BY ts ASC';

if (!$_CORE->IS_ADMIN)
	$FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL)";

$FORM_DATA = array(
  'id' => 
  array	(
	'field_name' =>	'id',
	'name' => 'form[id]',
	'title'	=> 'id',
	'must' => 0,
	'maxlen' =>	20,
	'type' => 'hidden',
  ),
  'alias' => 
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => 'alias',
    'must' => 0,
    'maxlen' => 255,
    'type' => 'hidden',
    'default' => $_SESSION['SESS_AUTH']['ID']."_".microtime(1),
  ),
//  'left'	=>
//  array	(
//	'field_name' =>	'left',
//	'name' => 'form[left]',
//	'title'	=> "left",
//	'must' => 0,
//	'maxlen' =>	'400',
//	'type' => 'textbox',
//	'style'	=> 'width:100%',
//  ),
   'hidden' => 
  array (
    'field_name' => 'hidden',
    'name' => 'form[hidden]',
    'title' => '�� ����������',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'checkbox',
  ),
  'ts' => 
  array (
    'field_name' => 'ts',
    'name' => 'form[ts]',
    'title' => 'time �������',
    'must' => '1',
    'maxlen' => '255',
    'type' => 'hidden',
		'default' => date("Y-m-d H:i:s"),
		'style' => 'width:100%',
  ),
	'from_auth'	=>
	array	(
		'field_name' =>	'from_auth',
		'name' =>	'form[from_auth]',
		'title'	=> '�� ����',
		'must' =>	1,
		'size' =>	50,
		'maxlen' =>	255,
		'type' =>	'hidden',
		'sub_type' =>	'bigint',	// ����� ������	�� ������	�	�������	UNIQUE
		'default'	=> $_SESSION['SESS_AUTH']['ID'],
  ),
);

// ������� ��������� ����

$fields_arr = array(
'target_alias',
'target_id',
'left',
'top',
'width',
'height',
'albomAlias',
'imageID',
'img_width',
'img_height',
'item_title',
'item_url',
'item_id',
'creator_id',
'leftTopX',
'leftTopY',
'rightBottomX',
'rightBottomY'
);

foreach ($fields_arr as $f) {
	$FORM_DATA[$f] = array(
	'field_name' =>	$f,
	'name' => 'form['.$f.']',
	'title'	=> $f,
	'must' => 0,
	'maxlen' =>	'400',
	'type' => 'textbox',
	'style'	=> 'width:100%',
  );
}

?>