<?php

if (!$_CORE->dll_load('class.DBCQ'))
	die ($_CORE->error_msg());
global $_RATING;	
$_RATING['MODULE_NAME']   = 'rating';
$_RATING['TABLE']         = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'rating'; 	
class RATING {
	function get_key($node) {
		return preg_replace('{[/\'"#@!:._]+}isU','',$node);
	}

	function get_rating($node = ''){
		if ($node == '') {
			return false;
		}	
		global $_RATING;
		$res = SQL::getval( 'rating', $_RATING['TABLE'], "path ='{$node}'" );
		if (Main::comm_link('rating.form.html', $f, 'rating')) {
			$path = $_SERVER['DOCUMENT_ROOT']."$f";
			if (is_file($f)) {
				include $f;
			}
			elseif(is_file($path)){
				include $path;
			}			
		}
		return $res;
	}
	
	function up_rating($node = ''){
		if ($node == '' || self::is_already_vote($node)) {
			return false;
		}
		self::set_cookie($node);
		global $_RATING;		
		$res = SQL::getval( 'rating', $_RATING['TABLE'], "path ='{$node}'" );
		if($res == NULL){
			SQL::ins( $_RATING['TABLE'], "path, rating", "'{$node}', 1");
		}
		else {
			SQL::upd( $_RATING['TABLE'], "rating = rating+1", "path = '{$node}'");	
		}	
		return $res;
	}

		
	function down_rating($node = ''){
		if ($node == '' || self::is_already_vote($node)) {
			return false;
		}	
		self::set_cookie($node);	
		global $_RATING;	
		$res = SQL::getval( 'rating', $_RATING['TABLE'], "path ='{$node}'" );
		if($res == NULL){
			SQL::ins( $_RATING['TABLE'],"path, rating", "'{$node}', 1");
		}
		else{		
			SQL::upd( $_RATING['TABLE'],"rating = rating-1", "path = '{$node}'");	
		}	
		return $res;
	}

	function is_already_vote($node){
		$str = self::get_key($node);		
		if ( isset($_COOKIE[$str]) ) {
			return true; 
		} 
		return false;
	}
	
	function create_table() {	
		global $_RATING, $SQL_DBLINK;
		$query = "CREATE TABLE ".$_RATING['TABLE']." ( id bigint(20) AUTO_INCREMENT, path    varchar(64), rating bigint(20),PRIMARY KEY (id))";
		$res = SQL::query( $query );
	}
	
	function delete_table()	{
		global $_RATING, $SQL_DBLINK;
		$query = "DROP TABLE ".$_RATING['TABLE'];
		$res = SQL::query( $query );
	}
	
	function set_cookie($node)	{
		$cook = self::get_key($node);
		setcookie ($cook, "vote", time()+3600*7*24,'/','.cnc.ru');
		$_COOKIE[$cook] = 'vote';
	}
}
?>