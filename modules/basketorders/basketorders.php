<?

global $_CORE, $_PROJECT, $_BASKET,$_CONF;

define ("BASKET_ERR_WRONG_EMAIL","�������� e-mail �����.");
define ("BASKET_ERR_EMPTY_BODY","������ �������.");
define ("BASKET_ERR_EMPTY_DELIVERY","�� ������ ������ ��������.");
define ("BASKET_ERR_EMPTY_NAME","������� ���� ���.");
define ("BASKET_ERR_EMPTY_PHONE","������� ��� ���������� ����� ��������.");

define ("BASKET_ERR_NOT_SEND",'��� ����� �� ������� ���������, ���������� �������.');
define ("BASKET_ERR_SEND",'��� ����� ���������.');
/*
* 
*/
if ( ! isset( $_CORE->_modules['basketorders'] ) )
{
	echo '������ basketorders �� ����������';
}
else
{
	$moduleTree = array();
	if ( is_file( $_CORE->SiteModDir . '/tree.inc.php' ) )
	{
		include $_CORE->SiteModDir . '/tree.inc.php';
	}
	elseif ( is_file( $_CORE->CoreModDir . '/_tree.inc.php' ) )
	{
		include $_CORE->CoreModDir . '/_tree.inc.php';
	}
	include_once ( $_CORE->CoreModDir . '/basketorders.inc.php' );
	$basketOrders = new BasketOrders(array('moduleTree'=>$moduleTree));
	$_BASKET['ADM_MENU']["/basketorders/orders"] = array("/ico/a_news.gif", '������', '/basketorders/orders');
	if ($basketOrders->config['delivery'])
		$_BASKET['ADM_MENU']["/db/basket_delivery/"] = array("/ico/a_news.gif", '������� ��������', 'db/basket_delivery/');
	if ($basketOrders->config['delivery2payment'])
		$_BASKET['ADM_MENU']["/db/basket_delivery2payment/"] = array("/ico/a_news.gif", '��������� ��������', 'db/basket_delivery2payment/');
	if ($basketOrders->config['deliverybyself'])
		$_BASKET['ADM_MENU']["/db/basket_deliverybyself/"] = array("/ico/a_news.gif", '������ ��� ����������', 'db/basket_deliverybyself/');
	if ($basketOrders->config['pickpoint'])
		$_BASKET['ADM_MENU']["/db/basket_pickpoint/"] = array("/ico/a_news.gif", '����� PickPoint', 'db/basket_pickpoint/');
	if ($basketOrders->config['expressru'])
		$_BASKET['ADM_MENU']["/db/basket_expressru/"] = array("/ico/a_news.gif", '����� Express.ru', 'db/basket_expressru/');
	
	list($command,$alias) = $_CORE->cmd_parse($Cmd);
	$path = ''; 
	if($command == 'myorders' && !empty($alias) && $alias != '/'){ 
		$Cmd = 'myorders/item';
		$alias = substr($alias, 1);
	}else{
		$alias = '';
	}
	if(strpos($_SERVER['REQUEST_URI'],'myorders/'))$path = '/basketorders/myorders/item';
	$accessCmd = array('adm_menu','orders', 'order');
	if (!empty($_CONF['FEATURES_USED']['auth_role']) && !empty($_SESSION['SESS_AUTH']['LOGIN'])) {
	$user_perm = SQL::getrow('*',DB_TABLE_PREFIX.'core_authperm', "name = '".$_SESSION['SESS_AUTH']['LOGIN']."' AND (`BASKETORDERS` = 1 OR `BASKETORDERS_MINI` = 1) ");
	
}
	if(in_array($Cmd,$accessCmd) ){
		if(	$_CORE->IS_ADMIN == false || 
			(
				!in_array($_SESSION['SESS_AUTH']['LOGIN'],array('admin','developer')) && 
				empty($user_perm)
			)
		){
			$Cmd = 'ErrorAccess';
		}
	}
	
	switch ( $Cmd )
	{
		case "ErrorAccess":
			echo "<h1>������������ ���� �� ���������� ������ �������</h1>";
		break;
		case "adm_menu":
			if ( ! empty( $_BASKET['ADM_MENU'] ) ) $_PROJECT['ADM_MENU'] = array_merge( $_PROJECT['ADM_MENU'],
					$_BASKET['ADM_MENU'] );
		break;
		case 'content/title/short':
		case 'content/seo_title':
		case 'content/seo_keywords':
		case 'content/seo_desc':
		case 'content/title':
		case 'content/path':
			$basketOrders->getpath($path);
			echo $_CORE->cmd_exec( array( 'user', '/' . $Cmd ) );
			if($Cmd == 'content/title' && !empty($alias)) echo ' �'.$alias;
		break;
		case 'module/title':
			echo 'BasketOrders';
		break;
		case 'module/desc':
			echo '������ ��� ������������, ��������� � �������������� �������';
		break;
		case "put":
			$product = $_POST['BASKET'];
			$result = $basketOrders->put($product);
			die($result);
		break;
		case "change":
			if (!empty($_POST['BASKET']['link']) && isset($_POST['BASKET']['count'])) { 
				if ($result = $basketOrders->changeCount($_POST['BASKET'])){
					die($result);
				}
			}
		break;
		case "del":
			if (!empty($_POST['link']) ) { 
				if ($result = $basketOrders->delProduct($_POST['link'])){
					die($result);
				}
			}
		break;
		case "show":
			if ($_CORE->comm_inc('basket.tmp.html', $file,'basketorders')){
				include "$file";
			}
		break;
		case "deliverypay":
			$summ = intval(@$_POST['summ']);
			$delivery_type = intval(@$_POST['delivery_type']);
			$result = $basketOrders->get_deliverypay($summ,$delivery_type,$_POST);
			echo $result;
			die();
		break;
		case "checkout": // ���������� ������
			if(!empty($_SESSION['_BASKET']['counts'])){
				if(!empty($_POST['BASKET_CONTACTS']) && is_array($_POST['BASKET_CONTACTS'])){
					$_SESSION['_BASKET']['contacts'] = $_POST['BASKET_CONTACTS'];
					if($basketOrders->config['delivery'])
						header('Location: //'.$_SERVER['SERVER_NAME'].'/basketorders/delivery');
					else
						header('Location: //'.$_SERVER['SERVER_NAME'].'/basketorders/go');
				}
				if ($_CORE->comm_inc('form.contact.html', $file,'basketorders')){
					include "$file";
				}
			}else{
				header('location: //'.$_SERVER['SERVER_NAME']); die;
			}
			break;
		case "install/plugins":
			if($_CORE->IS_DEV ){
				$basketOrders->install_plugins();
			}else{
				echo '�� ���������� ����.';
			}
		break;
		case 'expressru_cities':
			if( $basketOrders->config['expressru'] ){
				include 'expressru.cities.php';
			}
			if( $basketOrders->config['from_citi'] && is_array($cities[$basketOrders->config['from_citi']]) ){
				echo '<select name="BASKET_DELIVERY[expressru_zone]" id="expressru_zone">';
				echo '<option value=""></option>';
				foreach($cities[$basketOrders->config['from_citi']] as $citi=>$data){
					echo '<option value="'.$data['zona'].'">'.$citi.'</option>';
				}
				echo '</select>';
			}
		break;
		case "delivery":
			if(!empty($_SESSION['_BASKET']) && $basketOrders->config['delivery']){
				if ($_CORE->comm_inc('delivery.html', $file,'basketorders')){
					include "$file";
				}
			}else{
				header('location: //'.$_SERVER['SERVER_NAME']); die;
			}
			break;
		case 'orders':
			if( $_CORE->comm_inc('coredb_basket.data.inc.php',$file,'db'))
				include "$file";
            $search = '';
            if(!empty($_REQUEST['search'])){
                $search = mysql_real_escape_string($_REQUEST['search']);
            }
			$orders = $basketOrders->getOrders('all',$search);
			
			if(is_array($orders) && count($orders)){
				if( $_CORE->comm_inc('search.head.html',$file,'basketorders'))
					include "$file";
				foreach($orders as $data){
					if( $_CORE->comm_inc('search.item.html',$file,'basketorders'))
					include "$file";
				}
				if( $_CORE->comm_inc('search.foot.html',$file,'basketorders'))
					include "$file";
			}
			break;
		case 'order':
			if(!empty($_GET['alias'])){
				global $form;
				if( $_CORE->comm_inc('coredb_basket.data.inc.php',$file,'db'))
					include "$file";
				if(is_array($_POST) && count($_POST)){
                    if ($user_perm['BASKETORDERS_MINI'] == 1 ){
                        $miniPostData = $basketOrders->getOrder($_POST['form']['alias']);
                        $accessFields = array('alias','hidden','status','payed','counts');

                        if(!empty($_POST['form'])){
                            foreach ($accessFields as $field) {
                                $miniPostData[$field] = $_POST['form'][$field];
                            }
                            $res = $basketOrders->changeOrder($miniPostData);
                        }
                    } else {
                        if (!empty($_POST['form'])) {
                            $res = $basketOrders->changeOrder($_POST['form']);
                        }
                        if (!empty($_POST['purchasing'])) {
                            $res = $basketOrders->changePurchasing($_POST['purchasing']);
                        }
                        if (!empty($_POST['add_alias'])) {
                            $addArray = array(
                                'alias' => $_POST['add_alias'],
                                'count' => 1,
                                'tab' => $basketOrders->config['add2basket_db_alias']
                            );
                            $_SESSION['_BASKET']['items'][] = $addArray;
                            $res = $basketOrders->savePurchasing($_GET['alias']);
                        }
                    }
					if($res === false ){
						$_BASKET['ERROR'] = "������ ��������� �� �������";
					}else{
						$_BASKET['ERROR'] = "������ ������� ���������";
					}
					unset($_POST);
					$order = $basketOrders->getOrder($_GET['alias']);
					if( $_CORE->comm_inc('after_edit.inc.php',$file,'basketorders'))
						include "$file";
				}else{
					$order = $basketOrders->getOrder($_GET['alias']);
				}
				if(empty($order['cont'])){
					unset($FORM_DATA['cont']);
				}
				$form = $order;
				$cForm	= new cForm($FORM_DATA);
				if( $_CORE->comm_inc('view.head.html',$file,'basketorders'))
					include "$file";
				if( is_array($order['items']) && count($order['items']) && empty($order['cont']) ){
					echo $basketOrders->getCont($order);
				}else{
					echo '<div id="tabs-2" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
		<p>'.$order['cont'].'</p></div>';
					
				}
				if( $_CORE->comm_inc('view.foot.html',$file,'basketorders'))
					include "$file";
				
			}
			break;
		case 'myorders':
			if( $_CORE->comm_inc('coredb_basket.data.inc.php',$file,'db'))
				include "$file";
			$orders = $basketOrders->getOrders($_SESSION['SESS_AUTH']['ID']);
			
			//echo "<pre>"; print_r($orders); echo "</pre>";
			if(is_array($orders) && count($orders)){
				if( $_CORE->comm_inc('search.head.html',$file,'basketorders'))
					include "$file";
				foreach($orders as $order){
					if( $_CORE->comm_inc('search.item.html',$file,'basketorders'))
					include "$file";
				}
				if( $_CORE->comm_inc('search.foot.html',$file,'basketorders'))
					include "$file";
			}else{
				echo '������� �� �������';
			}
			break;
		case 'myorders/item':
			if(!empty($alias)){
				$order = $basketOrders->getOrder($alias);
				if( $_CORE->comm_inc('view.head.html',$file,'basketorders'))
					include "$file";
				if( is_array($order['items']) && count($order['items']) && empty($order['cont']) ){
					echo $basketOrders->getCont($order);
				}else{
					echo $order['cont'];
				}
				if( $_CORE->comm_inc('view.foot.html',$file,'basketorders'))
						include "$file";
				
				/*if( $_CORE->comm_inc('view.item.html',$file,'basketorders'))
					include "$file";*/
			}
			break;
		case "go":
		
			if ($basketOrders->checkContacts()) {
				// �������� �����
				$data = $basketOrders->saveOrder(); 
				//print_r($_POST);print_r($_GET);exit;
				if (!empty($data['alias'])){ 
					// �������� ������ ������� � �� ����������������
					$basketOrders->savePurchasing($data['alias']);
					// ���������� ������ ��� ����������� ����� ���������� ������
					if( $_CORE->comm_inc('plugin.inc.php',$file,'basketorders'))
						include "$file";
					// �������� �� ����� ���������� � ������
					$basketOrders->mailNotify($data,$attach);
					$_BASKET['ERROR']	= BASKET_ERR_SEND;
				}
				
			}
			
			unset($_SESSION['_BASKET']);
		
			// if new then must be loggined, else go there
			$after = ($_SESSION['SESS_AUTH']['LOGIN']) ? '/basketorders/myorders' : '/auth/login';
			
			if($basketOrders->config['confirm']){
				if( $_CORE->comm_inc('confirm.inc.php',$file,'basketorders'))
					include "$file";
			}else{
				if(!empty($basketOrders->config['after']) ) $after = $basketOrders->config['after'];
				
				//die ("<HTML><HEAD><META http-equiv='Content-Type' content='text/html; charset=windows-1251'></HEAD><SCRIPT> alert('".$_BASKET['ERROR']."'); document.location = '$after';</SCRIPT>");
			}

			break;
		default:
			echo $basketOrders->getCont($_SESSION['_BASKET'],'basket.');
			break;
	}
}
