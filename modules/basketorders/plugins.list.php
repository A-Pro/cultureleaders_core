<?php
	$pluginsList = array(
		'delivery'=>array(
			'db' => array(
				'alias' => 'basket_delivery',
				'TITLES' => '������� ��������',
				'SRC' => 'coredb_delivery',
				'FILES' => 'coredb_delivery.data.inc.php',
				'adm_fields' => '',
			),
		),
		'delivery2payment' => array(
			'db' => array(
				'alias' => 'basket_delivery2payment',
				'TITLES' => '��������� ��������',
				'SRC' => 'coredb_delivery2payment',
				'FILES' => 'coredb_delivery2payment.data.inc.php',
				'adm_fields' => "'type_id','condition','value','cost'",
			),
		),
		'deliverybyself' => array(
			'db' => array(
				'alias' => 'basket_deliverybyself',
				'TITLES' => '������ ��� ����������',
				'SRC' => 'coredb_deliverybyself',
				'FILES' => 'coredb_deliverybyself.data.inc.php',
				'adm_fields' => '',
			),
		),
		'pickpoint' => array(
			'db' => array(
				'alias' => 'basket_pickpoint',
				'TITLES' => '����� PickPoint',
				'SRC' => 'coredb_pickpoint',
				'FILES' => 'coredb_pickpoint.data.inc.php',
				'adm_fields' => '',
			),
		),
		'expressru' => array(
			'db' => array(
				'alias' => 'basket_expressru',
				'TITLES' => '����� Express.ru',
				'SRC' => 'coredb_expressru',
				'FILES' => 'coredb_expressru.data.inc.php',
				'adm_fields' => '',
			),
		),
	);
?>