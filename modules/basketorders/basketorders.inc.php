<?php
global $_CORE, $_CONF, $_KAT;
cmd('db/nothing',false,true); // ��������� ������ $_KAT ����� ����� ������ � ���������� ���������
include_once $_CONF['CorePath'].'/lib/class.Modules.php';

/**
 * �lass BasketOrders 
 *
 */
 
class BasketOrders extends Modules {
	
	public $tabOrders; // ������� �������
	public $tabOrderItems; // ������� ������� � ������
	/**
	 * ����������� ������
	 * 
	 */
	public function __construct($params = array())
	{
		$this->tabOrders = DB_TABLE_PREFIX.'basket';
		$this->tabOrderItems = DB_TABLE_PREFIX.'basket2items';
		if(is_array($params) && count($params)){
			foreach($params as $key=>$val){
				$this->$key = $val;
			}
		}
		parent::__construct('basketorders');
	}
	
	
	
	public function install()
	{
		return parent::install();
	}
	
	
	public function getOrders($user, $search = '')
	{
		$where = "1 ";
		if($user!= 'all') $where .= "AND from_auth = ".intval($user);
        if(!empty($search)) $where .= "AND (id = '".$search."' OR alias = '".$search."')";
		$result = $this->_sql->getall('*',$this->tabOrders,$where,"ORDER BY `ts` DESC");
		return $result;
	}
	
	
	public function loadOrder($alias){
		$order = $this->_sql->getrow('*',$this->tabOrders,"alias = '".$alias."'");
		if($order !== false ){
			$where = "order_alias = '".$alias."'";
			$order['items'] = $this->_sql->getall('*',$this->tabOrderItems,$where);
		}
		return $order;
	}
	
	
	
	public function getOrder($alias)
	{
		if(empty($_SESSION['SESS_AUTH']['ID']) && empty($_GET['hash'])){ 
			// TODO: �������� � ������ �������
			echo '������ �������';
			return false;
		}
		if($this->_core->IS_ADMIN){ 
			$where = "1";
		}else{
			if(!empty($_GET['hash'])){
				$where = "printversion = '".mysql_real_escape_string($_GET['hash'])."'";
			}else{
				$where = "from_auth = ".intval($_SESSION['SESS_AUTH']['ID']);
			}
		}
		$where .= " AND alias = '".$alias."'";
		
		
		$order = $this->_sql->getrow('*',$this->tabOrders,$where);
		if($order !== false ){
			$where = "order_alias = '".$alias."'";
			$order['items'] = $this->_sql->getall('*',$this->tabOrderItems,$where);
		}
		return $order;
	}
	
	
	public function getpath($p=''){
		global $_PROJECT;
		parent::getpath($p);
		if(strpos($_SERVER['REQUEST_URI'],'myorders')){
			$Cmd = str_replace('/basketorders/','',$_SERVER['REQUEST_URI']);
			list($command,$alias) = $this->_core->cmd_parse($Cmd);
			if(!empty($alias) && $alias!='/'){
				$alias = substr($alias, 1);
				array_pop($_PROJECT['PATH_2_CONTENT']);
				array_push($_PROJECT['PATH_2_CONTENT'],array('name'=>'����� �'.$alias));
			}
		}
	}
	
	
	/**
	* ����� empty ��� ��������� �������
	*/
	public function getCont($data, $tpl = '',$is_empty = false)
	{
		global $_KAT,$_CONF;
		if (!$is_empty) 
			if ($this->_core->comm_inc($tpl.'head.html',$file,'basketorders')){
				include "$file"; 
			}
		$summ = $counts = $names = 0;
		if (is_array($data) && count($data)) {
			$i	= 0;
			if (is_array($data['items']) && count($data['items'])) {
				foreach ($data['items'] as $link => $item) {
					if( $tpl == 'basket.' ){
						$itemData = $this->_sql->getrow('*',DB_TABLE_PREFIX.$_KAT['TABLES'][$item['tab']], 'alias='.$item['alias']);
						$item = array_merge($itemData,$item);
					}else{
						$itemData = STR::json_decode_cyr($item['item_data'],true);
						$itemData = STR::utf_to_win($itemData,true);
						$item = array_merge($itemData,$item);
					}
					$item['full_price'] = str_replace(array(" ",","), array('','.'),$item['full_price']);
                    if(!empty($_CONF['regions_addprice']) && !$_COOKIE['GEO_HOME']){
                        $item['full_price'] = ceil($item['full_price']*(1+$_CONF['regions_addprice']/100));
                    }
					if (!$is_empty) {
						if ($this->_core->comm_inc($tpl.'item.html', $file,'basketorders')){
							include "$file";
						}
					}
					$counts	+= $item['count'];

					if(!empty($item['discount'])){
						$summ	+= $item['count']*round($item['full_price']*(1-$item['discount']/100));
					}else{
						$summ	+= $item['count']*round($item['full_price']);
					}
					$i++;
					$names++;
				}
			}
		}
		$data['counts'] = $counts;
		$data['summ'] = $summ;
		$data['names'] = $names;
		if (!$is_empty) {
			if ($this->_core->comm_inc($tpl.'foot.html', $file,'basketorders')){
				include "$file";
			}
		}else{
			return $data;
		}
		
	}
	
	
	public function put( $form )
	{
		if (!isset($_SESSION['_BASKET'])) {
			session_start();
		}

		if (!is_array($_SESSION['_BASKET'])) {
			$_SESSION['_BASKET'] = array();
			$_SESSION['_BASKET']['items'] = array();
		}

		if ($form['count'] > 0){
			if (!empty($_SESSION['_BASKET']['items'][$form['link']]) && !($this->config['delivery']) ) {
				$_SESSION['_BASKET']['items'][$form['link']]['count'] += $form['count'];
			}else{
				$_SESSION['_BASKET']['items'][$form['link']] = $form;
			}
		}
		$_SESSION['_BASKET'] = $this->getCont($_SESSION['_BASKET'],'basket.',true);
		return json_encode($_SESSION['_BASKET']);
	}
	
	
	public function delProduct( $link )
	{
		if (!empty($_SESSION['_BASKET']['items'][$link])) {
			unset($_SESSION['_BASKET']['items'][$link]);
		}
		$_SESSION['_BASKET'] = $this->getCont($_SESSION['_BASKET'],'basket.',true);
		return json_encode($_SESSION['_BASKET']);
	}
	
	
	public function changeCount( $data )
	{
		if ( !isset($_SESSION['_BASKET']) )
			return true;
		if ( !empty($_SESSION['_BASKET']['items'][$data['link']]['count']) ) {
			
			if ($data['count'] == 0)
				unset($_SESSION['_BASKET']['items'][$data['link']]);
			else{
				$_SESSION['_BASKET']['items'][$data['link']]['count'] = $data['count'];
			}
			$_SESSION['_BASKET'] = $this->getCont($_SESSION['_BASKET'],'basket.',true);
			return json_encode($_SESSION['_BASKET']);
		} else {
			// �� ������ ����� � �������
			return false;
		}
	}
	
	
	function checkContacts()
	{
		if($this->config['delivery']){
			if( empty($_POST['BASKET_DELIVERY']['delivery_type'])){
				// $_BASKET['ERROR']	= BASKET_ERR_EMPTY_DELIVERY; // ������
				return false;
			}
			$_SESSION['_BASKET'] = @array_merge( $_SESSION['_BASKET'], $_POST['BASKET_DELIVERY']);
		}
		if (!empty($_SESSION['_BASKET']['contacts']) && !empty($_POST['BASKET_CONTACTS'])) {
			$_SESSION['_BASKET']['contacts'] = array_merge( $_SESSION['_BASKET']['contacts'], 
															$_POST['BASKET_CONTACTS']
															);
		}
		
		if (!empty($_SESSION['_BASKET']['contacts'])) {
			$_POST['BASKET_CONTACTS'] = $_SESSION['_BASKET']['contacts'];
		}
		if (empty($_SESSION['_BASKET']['items'])) {
			$_BASKET['ERROR']	= BASKET_ERR_EMPTY_BODY;
			return false;
		}
		if (empty($_POST['BASKET_CONTACTS']['name'])) {
			// $_BASKET['ERROR']	= BASKET_ERR_EMPTY_NAME; // ������
			return false;
		}
		$_SESSION['_BASKET']['name'] = $_POST['BASKET_CONTACTS']['name'];
		if (empty($_POST['BASKET_CONTACTS']['phone'])) {
			$_BASKET['ERROR']	= BASKET_ERR_EMPTY_PHONE;
			return false;
		}
		$_SESSION['_BASKET']['phone'] = $_POST['BASKET_CONTACTS']['phone'];
		$_SESSION['_BASKET']['email'] = $_POST['BASKET_CONTACTS']['email'];
		return true;
	}
	
	
	public function saveOrder($dataIn = '')
	{
		global $form;
		// ���������� ��������� ������� �������� �������
		if( $this->_core->comm_inc('coredb_basket.data.inc.php',$file,'db'))
			include "$file";
		
		$dataIn = $_SESSION['_BASKET']; // ��������� ������
		if($this->config['delivery']){
			$dataIn['delivery_summ'] = $this->get_deliverypay($dataIn['summ'],$dataIn['delivery_id'],$dataIn);
		}
		$form = array();
		foreach($dataIn as $field => $value){
			if(isset($FORM_DATA[$field]) && !is_array($value)){
				$form[$field] = mysql_real_escape_string($value);
			}
			if($field == 'contacts'){
				foreach($value as $cField => $cValue ){
					if(isset($FORM_DATA[$cField]) ){
						$form[$cField] = mysql_real_escape_string($cValue);
					}
				}
			}
		}
				
		$form['cnt'] = $dataIn['counts'];
		$form['user_name'] = $form['name'];
		$form['alias'] = $form['name'] = uniqid();
		$form['ts'] = date("Y-m-d H:i:s");
		$form['hidden'] = 1;
		
		if($this->config['printversion'])
			$form['printversion'] 	= md5(time().$form['email']);
		$cForm	= new cForm($FORM_DATA);
		list($var, $val) = $cForm->GetSQL('insert');
		$res = $this->_sql->ins( $this->tabOrders, $var, $val, DEBUG);
		if($res->Result)
			return $this->loadOrder($form['alias']);
		else 
			return false;
	}
	
	
	public function changeOrder($dataIn)
	{
		if(empty($dataIn['alias'])){
			return false;
		}
		global $form;
		// ���������� ��������� ������� �������� �������
		if( $this->_core->comm_inc('coredb_basket.data.inc.php',$file,'db'))
			include "$file";
		
		$form = array();
		foreach($dataIn as $field => $value){
			if(isset($FORM_DATA[$field]) && !is_array($value)){
				$form[$field] = mysql_real_escape_string($value);
			}
		}
		$form['cnt'] = $dataIn['counts'];
		$cForm	= new cForm($FORM_DATA);
		$set = $cForm->GetSQL('update');
		$res = $this->_sql->upd( $this->tabOrders, $set, "alias = '".$dataIn['alias']."'", 0);
		if($res->Result)
			return $form;
		else 
			return false;
	}
	
	/**
	* ���������� ������ � ������� ������ �����/�����
	*/		
	public function savePurchasing($order_alias)
	{
		global $_KAT;
		if (is_array($_SESSION['_BASKET']['items']) && count($_SESSION['_BASKET']['items'])) {
			foreach ($_SESSION['_BASKET']['items'] as $item) {
				$itemData = $this->_sql->getrow('*',DB_TABLE_PREFIX.$_KAT['TABLES'][$item['tab']], "alias=".$item['alias']);
				if(!empty($item['doc']))
					$itemData['doc'] = $item['doc'];
				elseif(!empty($itemData['doc']))
					$itemData['doc'] = '/data/db/f_'.$item['tab'].'/'.$itemData['doc'];
				$arr = array();
				$arr['order_alias'] = $order_alias;
				$arr['item_alias'] 	= $item['alias'];
				$arr['count'] 		= $item['count'];
				$arr['db_alias'] 	= $item['tab'];
				$arr['item_data'] 	= STR::win_to_utf($itemData,true);
				$arr['item_data'] 	= STR::json_encode_cyr($arr['item_data']);
				$res = $this->_sql->ins( $this->tabOrderItems, 
									implode(",",array_keys($arr)), 
									"'".implode("','",array_values($arr))."'", 
									0);
			}
		}
		return;
	}
	
	
	public function changePurchasing($dataIn)
	{
		if( $dataIn['count'] == 0 ){
			$res = $this->_sql->del($this->tabOrderItems,"id = '".$dataIn['id']."' AND order_alias = '".$dataIn['order_alias']."'", 0);
		}else{
			if(is_array($dataIn['item_data']) && count($dataIn['item_data'])){
				$itemData = $this->_sql->getval('item_data',$this->tabOrderItems, "id = '".$dataIn['id']."' AND order_alias = '".$dataIn['order_alias']."'");
				$itemData = STR::json_decode_cyr($itemData,true);
				$itemData = STR::utf_to_win($itemData,true);
				$itemData = array_merge($itemData,$dataIn['item_data']);
				$itemData = STR::win_to_utf($itemData,true);
				$itemData = STR::json_encode_cyr($itemData);
			}
			$res = $this->_sql->upd($this->tabOrderItems,"count = '".$dataIn['count']."', item_data = '".$itemData."'","id = '".$dataIn['id']."' AND order_alias = '".$dataIn['order_alias']."'", 0);
		}
		if($res->Result){
			$order = $this->loadOrder($dataIn['order_alias']);
			$data = $this->getCont($order,'',true);
			if(!empty($data) && is_array($data)){
				$this->changeOrder($data);
			}
			return true;
		}else 
			return false;
	}
	
	
	public function mailNotify($order,$attach = array())
	{
		global $_CONF;
		$data = $this->loadOrder($order['alias']);
		ob_start();
		$this->getCont($data,"mail.");
		if ($this->_core->comm_inc('mail.contact.html', $file, 'basketorders')){
			include "$file";
		}
		$data['cont'] = ob_get_contents();
		ob_end_clean(); 
		$data['to']		= TO;
		$data['name']	= $data['user_name'];
		$data['subj']	= 'Order from '.$_SERVER['HTTP_HOST'];
		$data['type']	= 'html';
		if ($this->_core->comm_inc('mail_admin.tmpl.html', $file, 'basketorders')){
			ob_start();
			include "$file";
			$data['body'] = ob_get_contents();
			ob_end_clean(); 
		}
		if (!$this->_lmail->sendhtml($data,$attach)){
			$_BASKET['ERROR']	= BASKET_ERR_NOT_SEND;
		}else {
			$data['mailto'] = $data['email'];
			$data['email']  = TO;
			$data['subj']	= 'Your order from '.$_SERVER['HTTP_HOST'];
			$data['name'] = (!empty($this->config['name_for_mail']))?$this->config['name_for_mail']:$_SERVER['HTTP_HOST'];
			if ($this->_core->comm_inc('mail_user.head.html', $file, 'basketorders')){
				ob_start();
				include "$file";
				$data['cont'] = ob_get_contents().$data['cont'];
				ob_end_clean(); 
			}
			if ($this->_core->comm_inc('mail_user.tmpl.html', $file, 'basketorders')){
				ob_start();
				include "$file";
				$data['body'] = ob_get_contents();
				ob_end_clean(); 
			}
				
			$this->_lmail->sendhtml($data,$attach);
			
			if (!empty($_CONF['FEATURES_USED']['partners'])){
				if ($this->_core->comm_inc('partner.inc.php', $file, 'basketorders')){
					@include_once "$file";
				}
			}
		
		}
	}
	
	
	public function get_deliverypay($summ, $delivery_type = '',$params=array()){
		if(!$this->config['delivery2payment'] || !$this->config['delivery']) return false;
		$res1 = 0;
		if(!empty($delivery_type)){
			$delivery = $this->_sql->getrow('*',DB_TABLE_PREFIX.'basket_delivery',"id='".$delivery_type."'");
			if(!empty($delivery['plugin'])){
				$method = 'get_'.$delivery['plugin'].'pay'; // ��� ������� ��������� �������� ��������
				$res1 = $this->$method($params);
				//return $res1;
			}
		}
		$where = '1';
		if( !empty($delivery_type)) 
			$where .= " AND (`type_id` = '".$delivery_type."' OR `type_id` = '���')";
		else
			$where .= " AND `type_id` = '���'";
			
		$where .= " AND (";
		$where .= " (`condition` = 1  AND `value` <= $summ )";
		$where .= " OR (`condition` = 2  AND `value` > $summ )";
		$where .= " OR `condition` = 3 ";
		
		$where .= " )";
		// echo $where;
		$res2 = $this->_sql->getval('cost', DB_TABLE_PREFIX . 'basket_delivery2payment', $where ,'order by cost ASC ');
		//echo "<pre>"; print_r($res1); echo "</pre>";
		if($res2!==false ){
			if(!empty($res1) && $res1 < $res2) return $res1;
			return intval($res2);
		}elseif(!empty($res1)){
			return $res1;
		}else
			return 0;
	}
	
	
	// ������������ $params[zone]
	public function get_pickpointpay($params=array()){
		if(!$this->config['pickpoint']) return false;
		$where = 'zone = '.$params['pickpoint_zone'];
		
		$res = SQL::getval('cost', DB_TABLE_PREFIX . 'basket_pickpoint', $where);
		//echo "<pre>"; print_r($res); echo "</pre>";
		if($res!==false )
			return intval($res);
		else 
			return false;
	}
	
	
	// ������������ $params[zone]
	public function get_expressrupay($params=array()){
		if(!$this->config['expressru']) return false;
		$where = "zone = '".$params['expressru_zone']."'";
		
		$res = SQL::getval('cost', DB_TABLE_PREFIX . 'basket_expressru', $where);
		//echo "<pre>"; print_r($res); echo "</pre>";
		if($res!==false )
			return intval($res);
		else 
			return false;
	}
	
	
	// ��������� ����������� ������ ��� ��������
	public function install_plugins(){
		include 'plugins.list.php';
		$file = KAT::get_data_path('conf.user.inc.php', $f, KAT_LOOKIG_DATA_FILE);
		include $file;
		$str = '';
		foreach($pluginsList as $key=>$plugin){
			if($this->config[$key] && !empty($plugin['db'])){
				$alias = $plugin['db']['alias'];
				if(empty($_KAT['TABLES'][$alias])){
				$str .= "
					\$_KAT['onpage_def']['$alias']	= 20;
					\$_KAT['TABLES']['$alias']	= '$alias';
					\$_KAT['TITLES']['$alias']	= '".$plugin['db']['TITLES']."';
					\$_KAT['SRC']['$alias']	= '".$plugin['db']['SRC']."';
					\$_KAT['DOC_PATH']['$alias'] = '/tmp/inside';
					\$_KAT['FILES']['$alias']	= '".$plugin['db']['FILES']."';
					\$_KAT['$alias']['AFTER_ADD']	= 'db/$alias/'; 
					\$_KAT['$alias']['hidden']	= 'TRUE'; // ��� db/list
					\$_KAT['$alias']['adm_fields'] = array(".$plugin['db']['adm_fields'].");
					";
				}
			}
		}
		// est' li fajlo, prochitaem, obnovim, sohranim
		KAT::load_users_conf($str_was);
		$str = implode('',$str_was).$str;
		KAT::save_users_conf($str);
		
		
		cmd('/db/install');
		
	}
}
?>