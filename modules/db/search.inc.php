<?php
/*

	Poisk po derevu dokumentov v fajlah cont, anons, title - v takom porjadke prioriteta
	0.1 - prostaja, poisk podstroki

*/
global $_KAT, $_CORE, $FORM_DATA, $FORM_ORDER;
$search	= @$_REQUEST['form'];
if(!empty($_REQUEST['ajax'])){
    foreach($search as $key=>$val){
        if(!is_array($val)){
            $search[$key]=iconv("UTF-8","windows-1251",$val);
        }
    }
}
//echo '<pre> FORM = '.print_r($_REQUEST['form'],1).'</pre>';
//echo '<pre> DB_SEARCH = '.print_r($_SESSION['DB_SEARCH'][$_KAT['KUR_ALIAS']],1).'</pre>';
if(empty($search) && $_KAT[$_KAT['KUR_ALIAS']]['AJAX'] == true) {
    // ���� ��� �������� ������� ����� AJAX � ������ ������� ������
    // �� ���� ���� ����������� ������ ������� ���������� ��
    if(!empty($_SESSION['DB_SEARCH'][$_KAT['KUR_ALIAS']])){
        $search = $_SESSION['DB_SEARCH'][$_KAT['KUR_ALIAS']];
    }
}
$_CORE->dll_load('class.cForm');
$ScForm	= new cForm( $FORM_DATA);

if(!empty($_KAT[$_KAT['KUR_ALIAS']]['AJAX']) && $_KAT[$_KAT['KUR_ALIAS']]['AJAX'] == true){
    $_SESSION['DB_SEARCH'][$_KAT['KUR_ALIAS']] = $search;
    echo '<section id="db_'.$_KAT['KUR_ALIAS'].'_form" >'."\n\r";
}
/**
 * Podkljuchenie fajla search.form.html
 * v jetom fajle osuwestvljaetsja nastrojka parametrov pered vyvodom. Naprimer $search = 'searching';
 */
if (KAT::inc_tpl('search.form.html', $file))
	include "$file";
if(!empty($_KAT[$_KAT['KUR_ALIAS']]['AJAX']) && $_KAT[$_KAT['KUR_ALIAS']]['AJAX'] == true){
    echo '</section>'."\n\r";
    if (KAT::inc_tpl('search.scripts.html', $file))
        include "$file";
}
// vybrannyj seo update //////////////
	if (isset($_REQUEST['seo_update']) && is_array($_REQUEST['seo_update']) && count($_REQUEST['seo_update'])&&$_CORE->IS_ADMIN) {
    	$seocheck = $_REQUEST['seo_check'];
		if (is_array($seocheck) && count($seocheck))
		{
			$where  = "(";
			foreach ($seocheck as $val) {
				$val = addslashes($val);
				$where  .= " '$val',";
			}
			$where = "name IN " . substr($where , 0, strlen($where)-1).")";
			
			$mass_update_array = array('seo_title', 'seo_keywords', 'seo_desc');
			foreach ($_REQUEST['seo_update'] as $k=>$v) {
				if ( !in_array($k, $mass_update_array) ) {
					unset($_REQUEST['seo_update'][$k]);
				}
			}
		
			if ( KAT::mass_update($_REQUEST['seo_update'], $where) ) {
				echo 'Seo update complete!';
			}
		}
	}
/////////////////////////////////////
	

if (!empty($search)) {
	$_KAT['SEARCH_RES']	= array();
	if(!empty($_KAT['ANY'])) {
		$limit = (!empty($_KAT[$_KAT['KUR_ALIAS']]['ANY']))?$_KAT[$_KAT['KUR_ALIAS']]['ANY']:3;
		$any = " ORDER BY RAND() LIMIT  ".$limit;
		if(!empty($_KAT['CUR'])){
			global $FORM_WHERE;
			$FORM_WHERE .= "AND ( id != ".$_KAT['CUR']." )"; 
		}
		KAT::search($search,'','',-1,false,$any);
	}else{
	   //print_r($search);
		KAT::search($search, $FORM_ORDER);
	}
	
	if (isset($_REQUEST['mass_update']) && is_array($_REQUEST['mass_update']) && count($_REQUEST['mass_update'])) {

		$where = KAT::search($search, $FORM_ORDER, '', -1, true);
		$mass_update_array = array( 'seo_keywords', 'seo_desc'); // 'seo_title',
		foreach ($_REQUEST['mass_update'] as $k=>$v) {
			if ( !in_array($k, $mass_update_array) ) {
				unset($_REQUEST['mass_update'][$k]);
			}
		}
		if (KAT::mass_update($_REQUEST['mass_update'], $where)) {
			echo 'Seo update complete!';
		}
	}

    if(!empty($_KAT[$_KAT['KUR_ALIAS']]['AJAX']) && $_KAT[$_KAT['KUR_ALIAS']]['AJAX'] == true){
        echo '<section id="db_'.$_KAT['KUR_ALIAS'].'_cont" >'."\n\r";
    }
    if(isset($_REQUEST['ajax'])&& $_KAT[$_KAT['KUR_ALIAS']]['AJAX'] == true) {
        ob_end_clean();
        ob_start();
    }
//    echo '<pre> FORM = '.print_r($_REQUEST['form'],1).'</pre>';
//    echo '<pre> DB_SEARCH = '.print_r($_SESSION['DB_SEARCH'][$_KAT['KUR_ALIAS']],1).'</pre>';
	if (sizeof($_KAT['SEARCH_RES']) > 0) {

//		if (empty($_KAT['NO_PLINE'])) 
//			echo $_KAT['SEARCH_PLINE'];

		// inc head


/**
 * Podkljuchenie zagolovochnogo fajla search.head.html
 * v jetom fajle osuwestvljaetsja vyvod zagolovka dlja posledujuwego vyvoda dannyh. 
 * Naprimer zagolovka tablicy.
 */
		if(empty($_KAT['CUR']))	if (KAT::inc_tpl('search.head.html', $file))
					include "$file";

		// wich file for item
		$file	=  $_CORE->SiteModDir.'/'.$_KAT['KUR_ALIAS'].'.search.item.html';
		$inc	=  (is_file($file)) ? $file : 'search.item.html';

/**
 * Podkljuchenie ciklicheski povtorjaemogo fajla search.item.html
 * v jetom fajle osuwestvljaetsja osnovnoj vyvod dannyh. 
 * Naprimer polja tablicy. Dannye v shablone berutsja iz massiva $data.
 */
		KAT::inc_tpl('search.item.html', $inc);
		
		foreach($_KAT['SEARCH_RES'] as $num => $data)
				include $inc;

/**
 * Podkljuchenie zavershajuwego fajla search.foot.html
 * v jetom fajle osuwestvljaetsja vyvod zagolovka dlja posledujuwego vyvoda dannyh. 
 * Naprimer okonchanija tablicy.
 */


	if(empty($_KAT['CUR']))	if (KAT::inc_tpl('search.foot.html', $file)){
			include "$file";
		}

/**
 * Podkljuchenie, esli razresheno, stroki listinga.
 */

		if (empty($_KAT['NO_PLINE']) && empty($_KAT[$_KAT['KUR_ALIAS']]['NO_PLINE'])) 
			echo $_KAT['SEARCH_PLINE'];
	
	}else {

/**
 * Vyvod na sluchaj, esli nichego ne najdeno
 */
		if(empty($_KAT['CUR']))if (KAT::inc_tpl('search.head.html', $file))
			include "$file";

		if (!isset($_KAT[$_KAT['KUR_ALIAS']]['NOTHING'])){
		if (KAT_ERR_NOT_FOUND == "KAT_ERR_NOT_FOUND"){
				echo "<div class=mark>".KAT_ERR_NOT_FOUND."<BR></div>";
			}
			}
		else 
			echo $_KAT[$_KAT['KUR_ALIAS']]['NOTHING'];

		// inc foot
		if(empty($_KAT['CUR']))if (KAT::inc_tpl('search.foot.html', $file))
			include "$file";

	}

    if(isset($_REQUEST['ajax']) && $_KAT[$_KAT['KUR_ALIAS']]['AJAX'] == true) {
        ob_end_flush();
       return;
    }
    if(!empty($_KAT[$_KAT['KUR_ALIAS']]['AJAX']) && $_KAT[$_KAT['KUR_ALIAS']]['AJAX'] == true){
        echo '</section>'."\n\r";
    }
}
