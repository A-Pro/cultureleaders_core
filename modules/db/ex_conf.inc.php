<?
/**
 * V jetom fajle sozdajutsja i nastraivajutsja osnovnye peremennye, ispol'zuemye v module db
Peremennye, smysl:

    1. $FORM_
    
    Pravila formirovanija $FORM_DATA            na vsjakij:  http://land.nnov.ru/src/maker_array/create_arr.php - pravda staraja jeto, versija no kljuchevye jelementy tam te zhe.

	osnovnoe pravilo po postroeniju imen
            [field_name] => 123  --- jeto schitaetsja polem v baze dannyh
            [name] => some[123]  --- jeto peremennaja v forme
            
	tak vot dlja [type] = photo, [name] dozhno byt' == [field_name],
(jeto     svjazano     s    rabotoj    slassa    cForm   i   ispol'zovaniem   tam
peremennyh  $[field_name]_type, $[field_name]_name) u drugih tipov polej pravilo takoe,  chto  kljuchem  dlja  massiva  [name]  dolzhno  byt'  [field_name], t.e. esli field_name = 123, to name = _any_[123].

+ v ramkah odnogo FORM_DATA, _any_ dlja korrektnoj raboty dolzhno byt' odinakovym.

       $FORM_DATA*         - universal'no formiruet strukturu tablicy suwnosti, ispol'zuetsja... .. da vezde pochti
         Primer formirovanija:
          $FORM_DATA= array (
            'id' =>
              array (
                  'field_name' => 'id',
                  'name' => 'form[id]',
                  'title' => 'id',
              ),
            'prior' =>
              array (
                  'field_name' => 'prior',
                  'name' => 'form[prior]',
                  'title' => 'Porjadok',
             )}
             
	    Obwij vid polja, nekotorye podpolja mogut otsutstvovat'
	    '<imja>' =>
	     array (
	         'field_name'     => '<imja>',        //esli <imja>===doc, to dolzhno sovpadat' s 'name'
	         'name'           => 'form[<imja>]',
	         'title'          => 'Zagolovok',
	         'maxlen'         => '255',
	         'must'           => '1',            // 1-NOT NULL
	         'type'           => 'textbox/textarea/photo/select',
	         'size'           => '80',
	         'readonly'       => 0,
	         'cols'           => '80',
	         'rows'           => '5',
	         'class'          => '',
	
	         'wysiwyg'        => 'htmlarea',     //what you see is what you get
	         'wysiwyg_path'   => '/modules/htmlarea/',
	         'wysiwyg_body'   => 'font-family: Tahoma, sans-serif; font-size: 11px; color: #003466;',

	         'maxwidth'       => 1500,
	         'maxheight'      => 1500,
	         'sub_type'       => 'doc/photo',    // sposob otobrazhenija esli foto - to pokazyvaet kartinku:)
	         'newname_func'   => 'DOCTXT::get_file_name()',
	         'path'           => '/modules/'.$_DOC['MODULE_NAME'].'/data/'.$form['path'].'file',
	         'abspath'        => $_CORE->PATHMODS.$_DOC['MODULE_NAME'].'/data/'.$form['path'].'file/',
	         'arr'            => array(<varianty dlja selectbox>),
	           )             

       $FORM_ORDER         - chast' SQL-zaprosa, otvechajuwaja za sortirovku
       $FORM_SELECT        - dopolnitel'nye uslovija na vyborku, esli nado vybrat' ne tol'ko *
       $FORM_SELECT_BEFORE - dopolnitel'nye uslovija, kotorye stavjatsja pered zaprsom
       $FORM_FROM          - Dopolnitel'nye tablicy dlja vyborki
       $FORM_WHERE         - dop.uslovija na poisk
	   $FORM_FIELD4ALIAS   - esli unikal'nym i pokazatel'nym polem javljaetsja ne "name", ispol'zuetsja v KAT::save()
	   $FORM_FIELDLONG     - skol'ko slov ispol'zovat' ot kazhdoj frazy dlja sostavlenija alias, isp v KAT::save()
	   
	   

      Esli ljubaja iz perechislennyh peremennyh ispol'zuetsja, pri sozdanii kakogolibo kataloga,
      to pri sozdanii drugih katalogov ih tozhe nuzhno sozdavat' i obnuljat'. Vo izbezhanie.
         Primer. Fail /db/search.inc.php. Metod void KAT::search( $search, $order='', $limit = '', $page = -1  );
         Osnovnoj zapros pri poiske.

         SQL::sel($add_sel_bef(na osnove $FORM_SELECT_BEFORE).' * '.$add_sel(na osnove $FORM_SELECT),  $_KAT['KUR_TABLE'].$FORM_FROM, "($add ".$sel(a na osnove $search i $FORM_WHERE).")", $order(f pri pomowi $_KAT['onpage']).$tail(f pri pomowi $_KAT['onpage']), 0);

         Takzhe ispol'zuetsja v int KAT::get_pages();



    2. $_KAT*              - glavnyj nastroechnyj massiv modulja db. Formiruetsja v faile conf.inc.php
       <name> - zdes' i dalee alias kataloga

       $_KAT['TABLES']['<name>']        = '<name>';                     -imja tablicy, dlja kataloga <name> dolzhno sovpadat' s <name>!!!
       $_KAT['TITLES']['<name>']        = "katalog";                    - nazvanie kataloga (otobrazhaetsja na sajte)
       $_KAT['FILES']['<name>']         = '<name>.data.inc.php';
       $_KAT['<name>']['hidden']        = 'true'; // dlja db/list        - parametr otvechajuwij za otkrytuju ublikaciju na sajte
       $_KAT['MODULE_NAME']             = 'db';                         - imja modulja

         Primer ispol'zovanija. Fail /db/db.php (vetka list).
                foreach ($_KAT['TABLES'] as $alias => $table){
                        if (!empty($_KAT[$alias]['hidden']) || $alias == '') continue;
                        $data['path'] = "/".$_KAT['MODULE_NAME']."/".$alias;
                        $data['name'] = $_KAT['TITLES'][$alias];
                        if (KAT::inc_tpl('db.list.item.html', $file))   include "$file";
                        else
                               echo "<p>".$_KAT['TITLES'][$alias]. " (".$alias.")";
                }


       $_KAT['onpage_def']['<name>']    = 100;                          - kolvo informacii na odnoj stranice;
       $_KAT['<name>']['NOTHING']       = 'Materilov ne najdeno!';      - zaglushka na neudachnyj poisk
       $_KAT['<name>']['SEEALSO']       = <p class='popup'><a href=\"Javascript:window.open('/clear/obsv/vlasti','', 'width=500,height=500,scrollbars=yes,resizable=yes');void(0)\">Zadat' vlasti vopros</a></p>;
       $_KAT['<name>']['AFTER_ADD']     = '/db/numbers/';
       $_KAT['ERROR']                   = '';
       $_KAT['onpage']                  = 7;                            - na stranice
       $_KAT['onpage_def']['def']       = 7;                            - vyvod informacii na stranice po umolchaniju
       $_KAT['DEF_ALIAS']               = 'news';                       - alias po umolchaniju
       $_KAT['TABLES']['']      = $_KAT['TABLES'][$_KAT['DEF_ALIAS']];
       $_KAT['FILES']['']       = $_KAT['FILES'][$_KAT['DEF_ALIAS']];
       $_KAT['TITLES']['']      = $_KAT['TITLES'][$_KAT['DEF_ALIAS']];
       $_KAT['KUR_TABLE']       = $_KAT['TABLES'][''];                  - tekuwaja tablica
       $_KAT['NO_PLINE']        = false;                                - parametr otvechajuwij za vyvod stroki listinga
       $_KAT['ADM_MENU']        = array(
                                    //        "/doctxt/?tree" => array("/ico/a_tree.gif", "Derevo"),
        );

       $_KAT['SUB_TITLE'] - imja konkretnogo razdela, ispol'zuetsja pri vyvode puti po sajtu (komanda /content/path/)
       
       $_KAT['RE_TITLE'] - stroka, sozdaetsja po neodhodimosti, dlja forsirovanija zagolovkov stranic
       
       $_KAT['_name_alias_']['SEEALSO'] - libo  stroka,  libo  massiv ispol'zuetsja tol'ko pri komande 'seealso' modulja DB.
       
       $_PROJECT['SEEALSO'] - rezul'tat  raboty  komand(y)  seealso  global'no  v  proekte.


       


       
       $_KAT[ADM_MENU]
        


      V principe v $_KAT mozhno zapihivat', ne tol'ko perechislennoe,
      no i razlichnye peremennye, kotorye mogut ponadobitsja pri rabote s opredelenym katalogom.
*/


$_KAT['ADM_MENU']        = array(
        "/".$_KAT['MODULE_NAME']."/numbers/" => array("/ico/a_news.gif", Main::get_lang_str('numbers', 'db'), $_KAT['MODULE_NAME'].'/numbers'),
        "/".$_KAT['MODULE_NAME']."/list" => array("/ico/a_news.gif", Main::get_lang_str('inf_razd', 'db'), $_KAT['MODULE_NAME'].'/list'),
//        "/".$_KAT['MODULE_NAME']."/news/" => array("/ico/a_news.gif", "Novosti", $_KAT['MODULE_NAME'].'/news'),
        "/".$_KAT['MODULE_NAME']."/aliases/" => array("/ico/a_news.gif", Main::get_lang_str('aliases', 'db'), $_KAT['MODULE_NAME'].'/aliases'),
//        "/".$_KAT['MODULE_NAME']."/today/" => array("/ico/a_news.gif", "Daty", $_KAT['MODULE_NAME'].'/today'),
//        "/".$_KAT['MODULE_NAME']."/norm_akts/" => array("/ico/a_news.gif", "Normativnye akty", $_KAT['MODULE_NAME'].'/norm_akts'),
//        "/".$_KAT['MODULE_NAME']."/konkursy/" => array("/ico/a_news.gif", "Konkurs i ob#javlenija", $_KAT['MODULE_NAME'].'/konkursy'),
//        "/".$_KAT['MODULE_NAME']."/selhoz/" => array("/ico/a_news.gif", "Vopros vlasti :: Sel'skoe hozjajstvo", $_KAT['MODULE_NAME'].'/selhoz'),
//        "/".$_KAT['MODULE_NAME']."/jil_ref/" => array("/ico/a_news.gif", "Vopros vlasti :: Zhiliwnaja reforma", $_KAT['MODULE_NAME'].'/jil_ref'),
//        "/".$_KAT['MODULE_NAME']."/monetiz/" => array("/ico/a_news.gif", "Vopros vlasti :: Monetizacija", $_KAT['MODULE_NAME'].'/monetiz'),
        );


$_CORE->MONTHS		= Array('', Main::get_lang_str('mes1', 'db'),Main::get_lang_str('mes2', 'db'),Main::get_lang_str('mes3', 'db'),Main::get_lang_str('mes4', 'db'),Main::get_lang_str('mes5', 'db'),Main::get_lang_str('mes6', 'db'),Main::get_lang_str('mes7', 'db'),Main::get_lang_str('mes8', 'db'),Main::get_lang_str('mes9', 'db'),Main::get_lang_str('mes10', 'db'),Main::get_lang_str('mes11', 'db'),Main::get_lang_str('mes12', 'db'));

//$_KAT['TITLE']        = 'Katalog';
$_KAT['MODULE_NAME']        = 'db';
$_KAT['ERROR']        = '';

 $_KAT['onpage']                = 10;
 $_KAT['onpage_def']['def']                = 10;

// $_KAT['onpage_def']['news']        = 20;
// $_KAT['TABLES']['news']        = 'news';
// $_KAT['TITLES']['news']        = 'Novosti';
// $_KAT['FILES']['news']        = 'news.data.inc.php';
// $_KAT['news']['AFTER_ADD']        = '/db/news/';
// $_KAT['news']['hidden']        = 'true'; // dlya db/list

 $_KAT['onpage_def']['aliases']        = 20;
 $_KAT['TABLES']['aliases']        = 'aliases';
 $_KAT['TITLES']['aliases']        = Main::get_lang_str('aliases', 'db');
 $_KAT['FILES']['aliases']        = 'aliases.data.inc.php';
 $_KAT['aliases']['AFTER_ADD']        = '/db/aliases/';
 $_KAT['aliases']['hidden']        = 'true'; // dlya db/list

 $_KAT['onpage_def']['numbers']        = 20;
 $_KAT['TABLES']['numbers']        = 'numbers';
 $_KAT['TITLES']['numbers']        = Main::get_lang_str('numbers', 'db');
 $_KAT['FILES']['numbers']        = 'numbers.data.inc.php';
 $_KAT['numers']['AFTER_ADD']        = '/db/numbers/';
 $_KAT['numbers']['hidden']        = 'true'; // dlya db/list

 $_KAT['onpage_def']['articles']        = 100;
 $_KAT['TABLES']['articles']        = 'articles';
 $_KAT['TITLES']['articles']        = Main::get_lang_str('arhiv', 'db');
 $_KAT['FILES']['articles']                = 'articles.data.inc.php';
 $_KAT['articles']['NOTHING']        = Main::get_lang_str('arhiv_not_fnd', 'db');
 $_KAT['articles']['hidden']         = 'true'; // dlya db/list
 $_KAT['articles']['SEEALSO']        = array("arc_cal", "/shedule");

 $_KAT['onpage_def']['today']        = 20;
 $_KAT['TABLES']['today']        = 'today';
 $_KAT['TITLES']['today']        = Main::get_lang_str('today', 'db');
 $_KAT['FILES']['today']        = 'today.data.inc.php';
 $_KAT['today']['NOTHING']        = ' ';
 $_KAT['today']['AFTER_ADD']        = '/db/today/';

 $_KAT['onpage_def']['norm_akts']        = 20;
 $_KAT['TABLES']['norm_akts']        = 'norm_akts';
 $_KAT['TITLES']['norm_akts']        = Main::get_lang_str('norm_akts', 'db');
 $_KAT['FILES']['norm_akts']        = 'norm_konk.data.inc.php';
 $_KAT['norm_akts']['NOTHING']        = Main::get_lang_str('norm_akts_not_fnd', 'db');
 $_KAT['norm_akts']['AFTER_ADD']        = '/db/norm_akts/';
 $_KAT['norm_akts']['SEEALSO']        = "<p class='popup'><a href=\"Javascript:window.open('/clear/obsv/vlasti','', 'width=500,height=500,scrollbars=yes,resizable=yes');void(0)\">"Main::get_lang_str('ask_questions', 'db')"</a></p>";

 $_KAT['onpage_def']['konkursy']        = 20;
 $_KAT['TABLES']['konkursy']        = 'konkursy';
 $_KAT['TITLES']['konkursy']        = Main::get_lang_str('konkursy', 'db');
 $_KAT['FILES']['konkursy']        = 'norm_konk.data.inc.php';
 $_KAT['konkursy']['NOTHING']        = Main::get_lang_str('konkursy_not_fnd', 'db');
 $_KAT['konkursy']['AFTER_ADD']        = '/db/konkursy/';

 $_KAT['onpage_def']['questions']        = 20;
 $_KAT['TABLES']['questions']        = 'questions';
 $_KAT['TITLES']['questions']        = Main::get_lang_str('questions', 'db');
 $_KAT['FILES']['questions']        = 'common.data.inc.php';
 $_KAT['questions']['NOTHING']        = ' ';
 $_KAT['questions']['AFTER_ADD']        = '/db/questions/';
 $_KAT['questions']['SEEALSO']        = "<p class='popup'><a href=\"Javascript:window.open('/clear/obsv/vlasti','', 'width=500,height=500,scrollbars=yes,resizable=yes');void(0)\">"Main::get_lang_str('ask_questions', 'db')"</a></p>";

 $_KAT['onpage_def']['prj_zdravohranenie']        = 20;
 $_KAT['TABLES']['prj_zdravohranenie']        = 'prj_zdravohranenie';
 $_KAT['TITLES']['prj_zdravohranenie']        = Main::get_lang_str('prj_zdravohranenie', 'db');
 $_KAT['FILES']['prj_zdravohranenie']        = 'projects.data.inc.php';
 $_KAT['prj_zdravohranenie']['NOTHING']        = ' ';
 $_KAT['prj_zdravohranenie']['AFTER_ADD']        = '/db/prj_zdravohranenie/';
// $_KAT['prj_zdravohranenie']['SEEALSO']        = "<p class='popup'><a href=\"Javascript:window.open('/clear/obsv/vlasti','', 'width=500,height=500,scrollbars=yes,resizable=yes');void(0)\">Zadat' vlasti vopros</a></p>";

 $_KAT['onpage_def']['prj_obrazovanie']        = 20;
 $_KAT['TABLES']['prj_obrazovanie']        = 'prj_obrazovanie';
 $_KAT['TITLES']['prj_obrazovanie']        = Main::get_lang_str('prj_obrazovanie', 'db');
 $_KAT['FILES']['prj_obrazovanie']        = 'projects.data.inc.php';
 $_KAT['prj_obrazovanie']['NOTHING']        = ' ';
 $_KAT['prj_obrazovanie']['AFTER_ADD']        = '/db/prj_obrazovanie/';
// $_KAT['prj_zdravohranenie']['SEEALSO']        = "<p class='popup'><a href=\"Javascript:window.open('/clear/obsv/vlasti','', 'width=500,height=500,scrollbars=yes,resizable=yes');void(0)\">Zadat' vlasti vopros</a></p>";

 $_KAT['onpage_def']['prj_hozyaystvo']        = 20;
 $_KAT['TABLES']['prj_hozyaystvo']        = 'prj_hozyaystvo';
 $_KAT['TITLES']['prj_hozyaystvo']        = Main::get_lang_str('prj_hozyaystvo', 'db');
 $_KAT['FILES']['prj_hozyaystvo']        = 'projects.data.inc.php';
 $_KAT['prj_hozyaystvo']['NOTHING']        = ' ';
 $_KAT['prj_hozyaystvo']['AFTER_ADD']        = '/db/prj_hozyaystvo/';

 $_KAT['onpage_def']['prj_live']        = 20;
 $_KAT['TABLES']['prj_live']        = 'prj_live';
 $_KAT['TITLES']['prj_live']        = Main::get_lang_str('prj_live', 'db');
 $_KAT['FILES']['prj_live']                = 'projects.data.inc.php';
 $_KAT['prj_live']['NOTHING']        = ' ';
 $_KAT['prj_live']['AFTER_ADD']        = '/db/prj_live/';

// $_KAT['onpage_def']['selhoz']        = 100;
// $_KAT['TABLES']['selhoz']        = 'selhoz';
// $_KAT['TITLES']['selhoz']        = 'Sel'skoe hozjajstvo';
// $_KAT['FILES']['selhoz']        = 'common.data.inc.php';
// $_KAT['selhoz']['NOTHING']        = 'Vosprosov vlasti po sel'skomu hozjajstvu ne najdeno!';
// $_KAT['selhoz']['AFTER_ADD']        = '/db/selhoz/';
// $_KAT['selhoz']['SEEALSO']        = "<p class='popup'><a href=\"Javascript:window.open('/clear/obsv/vlasti','', 'width=500,height=500,scrollbars=yes,resizable=yes');void(0)\">Zadat' vlasti vopros</a></p>";
//
// $_KAT['onpage_def']['jil_ref']        = 20;
// $_KAT['TABLES']['jil_ref']        = 'jil_ref';
// $_KAT['TITLES']['jil_ref']        = 'Zhiliwnaja reforma';
// $_KAT['FILES']['jil_ref']        = 'common.data.inc.php';
// $_KAT['jil_ref']['NOTHING']        = 'Vosprosov vlasti po zhiliwnoj reforme ne najdeno!';
// $_KAT['jil_ref']['AFTER_ADD']        = '/db/jil_ref/';
// $_KAT['jil_ref']['SEEALSO']        = "<p class='popup'><a href=\"Javascript:window.open('/clear/obsv/vlasti','', 'width=500,height=500,scrollbars=yes,resizable=yes');void(0)\">Zadat' vlasti vopros</a></p>";
//
// $_KAT['onpage_def']['monetiz']        = 20;
// $_KAT['TABLES']['monetiz']        = 'monetiz';
// $_KAT['TITLES']['monetiz']        = 'Monetizacija';
// $_KAT['FILES']['monetiz']        = 'common.data.inc.php';
// $_KAT['monetiz']['NOTHING']        = 'Vosprosov vlasti po monetizacii ne najdeno!';
// $_KAT['monetiz']['AFTER_ADD']        = '/db/monetiz/';
// $_KAT['monetiz']['SEEALSO']        = "<p class='popup'><a href=\"Javascript:window.open('/clear/obsv/vlasti','', 'width=500,height=500,scrollbars=yes,resizable=yes');void(0)\">Zadat' vlasti vopros</a></p>";

 $_KAT['onpage_def']['job']        = 20;
 $_KAT['TABLES']['job']        = 'job';
 $_KAT['TITLES']['job']        = Main::get_lang_str('vacancies', 'db');
 $_KAT['FILES']['job']        = 'job.data.inc.php';
 $_KAT['job']['NOTHING']        = '';
 $_KAT['job']['AFTER_ADD']        = '/db/job/';
 $_KAT['job']['SEEALSO']        = "<p class='popup'><a href=\"Javascript:window.open('/clear/multform/resume/','', 'width=500,height=500,scrollbars=yes,resizable=yes');void(0)\">".Main::get_lang_str('otprav_rezume', 'db')."</a></p>";  //Objazatel'no s / v konce !!!


 $_KAT['NO_PLINE'] = false;

# default
 $_KAT['KUR_TABLE']                = $_KAT['TABLES'][''];


# default
 $_KAT['DEF_ALIAS']                = 'news';
 $_KAT['TABLES']['']        = $_KAT['TABLES'][$_KAT['DEF_ALIAS']];
 $_KAT['FILES']['']                = $_KAT['FILES'][$_KAT['DEF_ALIAS']];
 $_KAT['TITLES']['']        = $_KAT['TITLES'][$_KAT['DEF_ALIAS']];
 $_KAT['KUR_TABLE']                = $_KAT['TABLES'][''];

define ("KAT_ERR_NOT_FOUND",Main::get_lang_str('not_fnd', 'db'));

?>