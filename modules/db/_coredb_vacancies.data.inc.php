<?php
global $_CONF;
$FORM_ORDER	= ' ORDER BY ts DESC ';
$FORM_IMPORT = array('name', 'id');
if(isset($_REQUEST['city']) && !empty($_REQUEST['search'])) {
	$search = addslashes($_REQUEST['search']);
	$FORM_WHERE = " AND city='".(int)$_REQUEST['city']."' AND (name LIKE '%".$search."%' OR description LIKE '%".$search."%') ";
}
$FORM_DATA= array (
  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'alias' => 
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => Main::get_lang_str('alias', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
 'name'	=>
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => Main::get_lang_str('title', 'db'),
    'must' => 1,
	'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'textbox',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
  'ts'	=>
  array (
    'field_name' => 'ts',
    'name' => 'form[ts]',
    'title' => Main::get_lang_str('data', 'db'),
    'must' => 1,
	'size' => 15,
    'maxlen' => 255,
    'type' => 'textdate',
    'readonly' => 'true',
	'default'	=> date('Y-m-d')
  ),
  'city' =>
  array(
  	'field_name'	=>	'city',
  	'name'			=>	'form[city]',
  	'title'			=>	Main::get_lang_str('city', 'db'),
  	'must'			=> 	0,
  	'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'select',
  	'arr'	=> $_CONF['cities'],
//		'logic' => 'OR',
//		'search' => " LIKE '%%%s%%'",
  ),
  'level' =>
  array(
  	'field_name'	=>	'level',
  	'name'			=>	'form[level]',
  	'title'			=>	Main::get_lang_str('level', 'db'),
  	'must'			=> 	0,
  	'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'select',
  	'arr'	=> array('','������','�������','��������','���-��������'),
  ),
  'industry' =>
  array(
  	'field_name'	=>	'industry',
  	'name'			=>	'form[industry]',
  	'title'			=>	Main::get_lang_str('industry', 'db'),
  	'must'			=> 	0,
  	'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'select',
  	'arr'	=> array(
  		'������������� ������',
'���������������� ��������',
'�����/����������/������',
'������������',
'�����������/�������������� ����/������� �����������',
'������ ����������',
'��������������� ������/�������������� �����������',
'������ �����',
'�������� ��������',
'�������',
'����������� � ������',
'�������������� ����������, ��������, �������',
'���������/�����������/����-�����',
'����������������',
'���������/�������/PR',
'��������/������������',
'�����/�����������',
'������ �������/��������',
'�������',
'������������',
'������� ��������',
'���������� �����/������/������ �������',
'�����������',
'�������������/������������',
'���������/���������',
'������/���������/���������',
'���������� ����������/��������',
'������'
  	),
  ),
    'description' => 
  array (
    'field_name' => 'description',
    'name' => 'form[description]',
    'title' => Main::get_lang_str('about', 'db'),
    'must' => '0',
    'maxlen' => '65535',
    'type' => 'textarea',
	'style' => 'width:100%',
    'rows' => '20',
	'wysiwyg'	=> 'tinymce',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
	),
	'requirements' =>
	array(
		'field_name'	=> 'requirements',
		'name'			=> 'form[requirements]',
		'title'			=> Main::get_lang_str('requirements', 'db'),
		'must'			=> 0,
		'maxlen'		=> '65536',
		'type'			=> 'textarea',
		'style' => 'width:100%',
    	'rows' => '20',
		'wysiwyg'	=> 'tinymce',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
	),
	'compens' =>
	array(
		'field_name'	=> 'compens',
		'name'			=> 'form[compens]',
		'title'			=> Main::get_lang_str('compens', 'db'),
		'must'			=> 0,
		'maxlen'		=> '65536',
		'type'			=> 'textarea',
		'style' => 'width:100%',
    	'rows' => '20',
		'wysiwyg'	=> 'tinymce',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
	),
)
?>
