<?
//$FORM_SEARCH	=	array( 
//'city', 'far', 'square', 'vodo', 'energy', 'otopl', 'kanal', 'gaz', 'arenda', 'prodaja'
//);

// if admin, then all of them
if (!isset($_REQUEST['form']['tid']) && !$_CORE->IS_ADMIN) {
	$FORM_SELECT	= ', count(alias) as cnt, min(date) as md'; // dlja formatirovannogo vyvoda daty v poiskah i dannyh.
	$FORM_ORDER	= "OR pid != '0' GROUP BY tid ORDER BY md DESC"; // OR pid != '0' chto by perebit' pid=0 kotoryj ustanavlivaetsja v *.form.* dlja listinga jeto polezano
}else {
	$FORM_SELECT	= ''; // dlja formatirovannogo vyvoda daty v poiskah i dannyh.
	$FORM_ORDER	= 'ORDER BY date ASC';
}

$_KAT['INDEX'][$_KAT['KUR_ALIAS']]	= 'id'; // ispol'zovat' indeks ne id inache pri redaktirovanii/dobavlenii proishodit sboj.
$FORM_FROM	= '';
$FORM_BEFORE	= '';
$FORM_WHERE	= 'AND (hidden != 1 OR hidden IS NULL)'; // objazatel'no esli gde-libo ispol'zuetsja

$FORM_DATA= array (
  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
		'default' => time(),
  ),
  'pid' => 
  array (
    'field_name' => 'pid',
    'name' => 'form[pid]',
    'title' => 'pid',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
	'uid' => 
  array (
    'field_name' => 'uid',
    'name' => 'form[uid]',
    'title' => 'uid',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
	'alias' =>  // dlja standarta
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => Main::get_lang_str('alias', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
		'default' => time(),
  ),
	'hidden' =>  // dlja standarta
  array (
    'field_name' => 'hidden',
    'name' => 'form[hidden]',
    'title' => Main::get_lang_str('ne_publ', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
		'default' => 0,
  ),
  'tid' => 
  array (
    'field_name' => 'tid',
    'name' => 'form[tid]',
    'title' => 'tid',
    'must' => 0,
    'maxlen' => 20,
	'default' => time(),
    'type' => 'hidden',
  ),
  'ln' => 
  array (
    'field_name' => 'ln',
    'name' => 'form[ln]',
    'title' => Main::get_lang_str('from', 'db'),
    'must' => 0,
	'size' => 50,
    'maxlen' => 256,
	'default' => '',
    'type' => 'textbox',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
  'date'	=>
  array (
    'field_name' => 'date',
    'name' => 'form[date]',
    'title' => Main::get_lang_str('time', 'db'),
    'must' => 0,
		'size' => 15,
    'maxlen' => 255,
    'type' => 'hidden',
    'readonly' => 'true',
	'default'	=> time()
  ),
  'subj'	=>
  array (
    'field_name' => 'subj',
    'name' => 'form[subj]',
    'title' => Main::get_lang_str('tema', 'db'),
    'style'	=> 'width:100%',
    'must' => 1,
	'size' => 50,
    'maxlen' => 255,
    'type' => 'hidden',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
  'name'	=>
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => Main::get_lang_str('your_name', 'db'),
    'style'	=> 'width:100%',
    'must' => 1,
	'size' => 50,
    'maxlen' => 255,
    'type' => 'textbox',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
  'email'	=>
  array (
    'field_name' => 'email',
    'name' => 'form[email]',
    'style'	=> 'width:100%',
    'title' => Main::get_lang_str('email', 'db'),
    'must' => 0,
		'size' => 50,
    'maxlen' => 255,
    'type' => 'hidden',
  ),
  'about' => 
  array (
    'field_name' => 'about',
    'name' => 'form[about]',
    'title' => Main::get_lang_str('mess', 'db'),
    'style'	=> 'width:100%',
    'must' => 1,
    'maxlen' => '5120',
    'type' => 'textarea',
    'cols' => '40',
    'rows' => '5',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
)
?>