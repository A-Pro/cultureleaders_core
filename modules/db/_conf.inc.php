<?global $_CONF;
if(empty($_KAT['MODULE_NAME'])) $_KAT['MODULE_NAME'] = 'db';
// Peremennye zapolnjajutsja v conf.phtml
if ($_CONF['FEATURES_USED']['news']) 
$_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/news/"] = array("/ico/a_news.gif", Main::get_lang_str('news', 'db'), $_KAT['MODULE_NAME'].'/news');

if ($_CONF['FEATURES_USED']['alias']) 
$_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/aliases/"] = array("/ico/a_alias.gif", Main::get_lang_str('aliases', 'db'), $_KAT['MODULE_NAME'].'/aliases');

if ($_CONF['FEATURES_USED']['obsv']) 
$_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/obsv/"] = array("/ico/a_obsv.gif", Main::get_lang_str('obsv', 'db'), $_KAT['MODULE_NAME'].'/obsv');

if ($_CONF['FEATURES_USED']['forum']) 
	$_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/forum/"] = array("/ico/a_forum.gif", Main::get_lang_str('forums', 'db'), $_KAT['MODULE_NAME'].'/forum');

if ($_CONF['FEATURES_USED']['dialog'] ) 
	$_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/dialog/"] = array("/ico/a_forum.gif", Main::get_lang_str('dialog', 'db'), $_KAT['MODULE_NAME'].'/dialog');

if ($_CONF['FEATURES_USED']['blogs']) 
	$_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/blogs/"] = array("/ico/a_forum.gif", Main::get_lang_str('blogs', 'db'), $_KAT['MODULE_NAME'].'/blogs');

if (!empty($_CONF['FEATURES_USED']['settings'])) {
	$_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/settings/"] = array("/ico/a_forum.gif", Main::get_lang_str('settings', 'db'), $_KAT['MODULE_NAME'].'/settings');
}
if (!empty($_CONF['FEATURES_USED']['youtubeapi'])) {
	$_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/channel2users/"] = array("/ico/a_forum.gif", Main::get_lang_str('channel2users', 'db'), $_KAT['MODULE_NAME'].'/channel2users'); 
}
if (in_array('rbk', $_CONF['MENUED_MODULES_ADMIN']) ){    
    $_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/rbk_billing/"] 
    	= array("/ico/a_news.gif", "RBKmoney", $_KAT['MODULE_NAME'].'/rbk_billing');
}
	
// Po umolchaniju SEO parametry otkljucheny
$_KAT['conf']['SEO'] = false;

//��������� �� ����� � �������������� ���������
$_KAT['conf']['NOTIF'] = false;

$_CORE->MONTHS		= Array('', Main::get_lang_str('mes1', 'db'),Main::get_lang_str('mes2', 'db'),Main::get_lang_str('mes3', 'db'),Main::get_lang_str('mes4', 'db'),Main::get_lang_str('mes5', 'db'),Main::get_lang_str('mes6', 'db'),Main::get_lang_str('mes7', 'db'),Main::get_lang_str('mes8', 'db'),Main::get_lang_str('mes9', 'db'),Main::get_lang_str('mes10', 'db'),Main::get_lang_str('mes11', 'db'),Main::get_lang_str('mes12', 'db'));
$_KAT['TITLE']	= '';
$_KAT['MODULE_NAME']	= 'db';
$_KAT['ERROR']	= '';

 $_KAT['onpage']		= 10;
 $_KAT['onpage_def']['def']		= 10;

if ($_CONF['FEATURES_USED']['settings']) {
// Tipovye konfiguracii
if ($_CORE->IS_ADMIN){
	$_KAT['onpage_def']['settings']	= 20;
	
	$_KAT['TABLES']['admin_content']	= 'admin_content';
	$_KAT['SRC']['admin_content']		= 'admin_content';
	$_KAT['TITLES']['admin_content']	= 'admin main page content';
	$_KAT['FILES']['admin_content']		= 'coredb_admin_content.data.inc.php';
	$_KAT['admin_content']['hidden']	= 'false'; // ��� db/list
	$_KAT['DOC_PATH']['admin_content']	= '/tmp/inside';
}  else {  
    $_KAT['onpage_def']['settings']	= 1;
}
 $_KAT['TABLES']['settings']	= 'settings';
 $_KAT['SRC']['settings']		= 'settings';
 $_KAT['TITLES']['settings']	= Main::get_lang_str('settings', 'db');
 $_KAT['FILES']['settings']	    = 'coredb_settings.data.inc.php';
 $_KAT['settings']['hidden']	= 'false'; // ��� db/list
 $_KAT['DOC_PATH']['settings']	= '/tmp/inside';
 $_KAT['settings']['AFTER_ADD_CMD']	= '/css/restyle';

 }

 $_KAT['onpage_def']['coredb_news']	= 20;
 $_KAT['TABLES']['coredb_news']	= 'coredb_news';
 $_KAT['SRC']['coredb_news']		= 'coredb_news';
 $_KAT['TITLES']['coredb_news']	= Main::get_lang_str('news', 'db');
 $_KAT['FILES']['coredb_news']	= 'coredb_news.data.inc.php';
// $_KAT['coredb_news']['AFTER_ADD']	= '/db/news/';
 $_KAT['coredb_news']['hidden']	= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['coredb_news']	= '/tmp/inside';
 
 $_KAT['onpage_def']['coredb_graph']	= 20;
 $_KAT['TABLES']['coredb_graph']	= 'coredb_graph';
 $_KAT['SRC']['coredb_graph']		= 'coredb_graph';
 $_KAT['TITLES']['coredb_graph']	= Main::get_lang_str('graph', 'db');
 $_KAT['FILES']['coredb_graph']	= 'coredb_graph.data.inc.php';
 $_KAT['coredb_graph']['hidden']	= 'true';
 $_KAT['DOC_PATH']['coredb_graph']	= '/tmp/inside';

 $_KAT['coredb_news']['hidden']	= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['coredb_news']	= '/tmp/inside';

 $_KAT['onpage_def']['coredb_blog']	= 20;
 $_KAT['TABLES']['coredb_blog']			= 'coredb_blog';
 $_KAT['SRC']['coredb_blog']				= 'coredb_blog';
 $_KAT['TITLES']['coredb_blog']			= Main::get_lang_str('blog', 'db');
 $_KAT['FILES']['coredb_blog']			= 'coredb_blog.data.inc.php';
// $_KAT['coredb_blog']['AFTER_ADD']	= '/db/coredb_blog/';
 $_KAT['coredb_blog']['hidden']			= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['coredb_blog']		= '/tmp/inside';

 $_KAT['onpage_def']['coredb_people']	= 20;
 $_KAT['TABLES']['coredb_people']			= 'coredb_people';
 $_KAT['SRC']['coredb_people']				= 'coredb_people';
 $_KAT['TITLES']['coredb_people']			= Main::get_lang_str('people', 'db');
 $_KAT['FILES']['coredb_people']			= 'coredb_people.data.inc.php';
// $_KAT['coredb_people']['AFTER_ADD']	= '/db/coredb_people/';
 $_KAT['coredb_people']['hidden']			= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['coredb_people']		= '/tmp/inside';
  
 $_KAT['onpage_def']['coredb_images']	= 20;
 $_KAT['TABLES']['coredb_images']			= 'coredb_images';
 $_KAT['SRC']['coredb_images']				= 'coredb_images';
 $_KAT['TITLES']['coredb_images']			= Main::get_lang_str('pictures', 'db');
 $_KAT['FILES']['coredb_images']			= 'coredb_images.data.inc.php';
// $_KAT['coredb_images']['AFTER_ADD']	= '/db/coredb_images/';
 $_KAT['coredb_images']['hidden']			= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['coredb_images']		= '/tmp/inside';

 $_KAT['onpage_def']['coredb_images_comment']	= 20;
 $_KAT['TABLES']['coredb_images_comment']			= 'coredb_images_comment';
 $_KAT['SRC']['coredb_images_comment']				= 'coredb_images_comment';
 $_KAT['TITLES']['coredb_images_comment']			= Main::get_lang_str('pictures2', 'db');
 $_KAT['FILES']['coredb_images_comment']			= 'coredb_images_comment.data.inc.php';
// $_KAT['coredb_images_comment']['AFTER_ADD']	= '/db/coredb_images_comment/';
 $_KAT['coredb_images_comment']['hidden']			= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['coredb_images_comment']		= '/tmp/inside';
 
 $_KAT['MASS_UPLOADER']['doc']		= 'doc';  
 $_KAT['MASS_UPLOADER']['description']		= 'name';

 $_KAT['onpage_def']['coredb_files']	= 20;
 $_KAT['TABLES']['coredb_files']			= 'coredb_files';
 $_KAT['SRC']['coredb_files']					= 'coredb_files';
 $_KAT['TITLES']['coredb_files']			= Main::get_lang_str('files', 'db');
 $_KAT['FILES']['coredb_files']= 'coredb_files.data.inc.php';
// $_KAT['coredb_files']['AFTER_ADD']		= '/db/coredb_files/';
 $_KAT['coredb_files']['hidden']			= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['coredb_files']		= '/tmp/inside';

 $_KAT['onpage_def']['coredb_pdf']	= 20;
 $_KAT['TABLES']['coredb_pdf']			= 'coredb_pdf';
 $_KAT['SRC']['coredb_pdf']					= 'coredb_pdf';
 $_KAT['TITLES']['coredb_pdf']			= Main::get_lang_str('pdf', 'db');
 $_KAT['FILES']['coredb_pdf']				= 'coredb_pdf.data.inc.php';
// $_KAT['coredb_pdf']['AFTER_ADD']		= '/db/coredb_pdf/';
 $_KAT['coredb_pdf']['hidden']			= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['coredb_pdf']		= '/tmp/inside';

 $_KAT['onpage_def']['coredb_youtube']	= 20;
 $_KAT['TABLES']['coredb_youtube']		= 'coredb_youtube';
 $_KAT['SRC']['coredb_youtube']			= 'coredb_youtube';
 $_KAT['TITLES']['coredb_youtube']		= 'Youtube';
 $_KAT['FILES']['coredb_youtube']		= 'coredb_youtube.data.inc.php';
// $_KAT['coredb_youtube']['AFTER_ADD']	= '/db/coredb_youtube/';
 $_KAT['coredb_youtube']['hidden']		= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['coredb_youtube']	= '/tmp/inside';
 
if (!empty($_CONF['FEATURES_USED']['youtubeapi'])) {
 $_KAT['onpage_def']['youtubeapi']	= 20;
 $_KAT['TABLES']['youtubeapi']		= 'youtubeapi_video';
 $_KAT['SRC']['youtubeapi']			= 'youtubeapi';
 $_KAT['TITLES']['youtubeapi']		= 'YoutubeApi';
 $_KAT['FILES']['youtubeapi']		= 'coredb_youtubeapi.data.inc.php';
// $_KAT['youtubeapi']['AFTER_ADD']	= '/db/coredb_youtubeapi/';
 $_KAT['youtubeapi']['hidden']		= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['youtubeapi']	= '/tmp/inside';
 

 $_KAT['onpage_def']['channel2users']	= 20;
 $_KAT['TABLES']['channel2users']		= 'youtubeapi_channel2users';
 $_KAT['SRC']['channel2users']			= 'channel2users';
 $_KAT['TITLES']['channel2users']		= Main::get_lang_str('channel2users', 'db');
 $_KAT['FILES']['channel2users']		= 'channel2users.data.inc.php';
 $_KAT['channel2users']['hidden']		= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['channel2users']	= '/tmp/inside';
 }
// 30.08.2011 from addwiise

  $_KAT['onpage_def']['coredb_vacancies']	= 20;
  $_KAT['TABLES']['coredb_vacancies']		= 'coredb_vacancies';
  $_KAT['SRC']['coredb_vacancies']			= 'coredb_vacancies';
  $_KAT['TITLES']['coredb_vacancies']		= Main::get_lang_str('vacancies', 'db');
  $_KAT['FILES']['coredb_vacancies']			= 'coredb_vacancies.data.inc.php';
  $_KAT['coredb_vacancies']['hidden']		= 'true'; // ��� db/list
//  $_KAT['coredb_vacancies']['AFTER_ADD']		= '/db/coredb_vacancies/';
  $_KAT['DOC_PATH']['coredb_vacancies']		= '/db/coredb_vacancies';

 $_KAT['onpage_def']['coredb_shop']	= 20;
 $_KAT['TABLES']['coredb_shop']			= 'coredb_shop';
 $_KAT['SRC']['coredb_shop']				= 'coredb_shop';
 $_KAT['TITLES']['coredb_shop']			= Main::get_lang_str('shop', 'db');
 $_KAT['FILES']['coredb_shop']			= 'coredb_shop.data.inc.php';
// $_KAT['coredb_shop']['AFTER_ADD']	= '/db/coredb_shop/';
 $_KAT['coredb_shop']['hidden']			= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['coredb_shop']		= '/tmp/inside';

// 22.01.2012 from dtp

 $_KAT['onpage_def']['coredb_htmlfiles']	= 300;
 $_KAT['TABLES']['coredb_htmlfiles']			= 'coredb_htmlfiles';
 $_KAT['SRC']['coredb_htmlfiles']				= 'coredb_htmlfiles';
 $_KAT['TITLES']['coredb_htmlfiles']			= Main::get_lang_str('html_file', 'db');
 $_KAT['FILES']['coredb_htmlfiles']			= 'coredb_htmlfiles.data.inc.php';
// $_KAT['coredb_htmlfiles']['AFTER_ADD']	= '/db/coredb_htmlfiles/';
 $_KAT['coredb_htmlfiles']['hidden']			= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['coredb_htmlfiles']		= '/tmp/inside';

// Dostup uzhe k gotovym podmoduljam

 $_KAT['onpage_def']['aliases']	= 20;
 $_KAT['TABLES']['aliases']	= 'aliases';
 $_KAT['TITLES']['aliases']	= Main::get_lang_str('aliases', 'db').'(URL)';
 $_KAT['FILES']['aliases']	= 'coredb_aliases.data.inc.php';
// $_KAT['aliases']['AFTER_ADD']	= '/db/aliases/';
 $_KAT['aliases']['hidden']	= 'true'; // ��� db/list

 $_KAT['onpage_def']['obsv']	= 20;
 $_KAT['TABLES']['obsv']	= 'obsv';
 $_KAT['TITLES']['obsv']	= Main::get_lang_str('obsv', 'db');
 $_KAT['FILES']['obsv']	= 'coredb_obsv.data.inc.php';
// $_KAT['obsv']['AFTER_ADD']	= '/db/obsv/';
 $_KAT['obsv']['hidden']	= 'true'; // ��� db/list

 $_KAT['onpage_def']['coredb_spravochnik']	= 20;
 $_KAT['TABLES']['coredb_spravochnik']			= 'coredb_spravochnik';
 $_KAT['SRC']['coredb_spravochnik']					= 'coredb_spravochnik';
 $_KAT['TITLES']['coredb_spravochnik']			= Main::get_lang_str('spravochnik', 'db');
 $_KAT['FILES']['coredb_spravochnik']				= 'coredb_spravochnik.data.inc.php';
 $_KAT['coredb_spravochnik']['hidden']			= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['coredb_spravochnik']		= '/tmp/inside';

// author logs
	if (!empty($_CONF['FEATURES_USED']['auth_logs'])) {
		$_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/coredb_auth_logs/"] = array("/ico/a_forum.gif", Main::get_lang_str('auth_logs', 'db'), $_KAT['MODULE_NAME'].'/coredb_auth_logs');
	}
 $_KAT['onpage_def']['coredb_auth_logs']	= 100;
 $_KAT['TABLES']['coredb_auth_logs']			= 'coredb_auth_logs';
 $_KAT['SRC']['coredb_auth_logs']					= 'coredb_auth_logs';
 $_KAT['TITLES']['coredb_auth_logs']			= Main::get_lang_str('auth_logs', 'db');
 $_KAT['FILES']['coredb_auth_logs']				= 'coredb_auth_logs.data.inc.php';
 $_KAT['coredb_auth_logs']['hidden']			= 'true'; // ��� db/list
 $_KAT['DOC_PATH']['coredb_auth_logs']		= '/tmp/inside';
if ($_CONF['FEATURES_USED']['auth_logs']) 
 $_KAT['auth_logs']['create']			= true;
	$_KAT['coredb_auth_logs']['PERM']['ua']	= '0'; // dobavlenie
	$_KAT['coredb_auth_logs']['PERM']['ue']	= '0'; // redaktirovanie
	$_KAT['coredb_auth_logs']['PERM']['ud']	= '0'; // udalenie
	$_KAT['coredb_auth_logs']['PERM']['ui']	= '0'; // import
	$_KAT['coredb_auth_logs']['PERM']['uf']	= '0'; // pokaz formy
	$_KAT['coredb_auth_logs']['PERM']['uem']	= '0'; // ochistka tablicy

# default
 $_KAT['KUR_TABLE']		= $_KAT['TABLES'][''];


# default
 $_KAT['TABLES']['']	= $_KAT['TABLES'][$_KAT['DEF_ALIAS']];
 $_KAT['FILES']['']		= $_KAT['FILES'][$_KAT['DEF_ALIAS']];
 $_KAT['TITLES']['']	= $_KAT['TITLES'][$_KAT['DEF_ALIAS']];
 $_KAT['KUR_TABLE']		= $_KAT['TABLES'][''];

if (in_array('forum', $_CONF['USED_MODULES']) || in_array('blog', $_CONF['USED_MODULES'])) {
/* FORUM / DISCUSSIONS */

//if (!$_CORE->IS_ADMIN) {
//
//	$_KAT['forum']['PERM']['ua']	= '0'; // dobavlenie
//	$_KAT['forum']['PERM']['ue']	= '1'; // redaktirovanie
//	$_KAT['forum']['PERM']['ud']	= '1'; // udalenie
//	$_KAT['forum']['PERM']['ui']	= '1'; // import
//	$_KAT['forum']['PERM']['uf']	= '1'; // pokaz formy
//	$_KAT['forum']['PERM']['uem']	= '1'; // ochistka tablicy
//
//}

$_KAT['forum']['reg_only'] = true; // kommentiroat' razresheno tol'ko avtorizovannym 

if (!$_CORE->IS_ADMIN && (!$_KAT['forum']['reg_only'] || $_KAT['forum']['reg_only'] && $_SESSION['SESS_AUTH']['ID'])) {
	$_KAT['forum']['PERM']['ua']	= '0'; // dobavlenie
	$_KAT['forum']['PERM']['ue']	= '1'; // redaktirovanie
	$_KAT['forum']['PERM']['ud']	= '1'; // udalenie
	$_KAT['forum']['PERM']['ui']	= '1'; // import
	$_KAT['forum']['PERM']['uf']	= '1'; // pokaz formy
	$_KAT['forum']['PERM']['uem']	= '1'; // ochistka tablicy
}elseif(!$_CORE->IS_ADMIN){
	$_KAT['forum']['PERM']['ua']	= '1'; // dobavlenie
	$_KAT['forum']['PERM']['ue']	= '1'; // redaktirovanie
	$_KAT['forum']['PERM']['ud']	= '1'; // udalenie
	$_KAT['forum']['PERM']['ui']	= '1'; // import
	$_KAT['forum']['PERM']['uf']	= '1'; // pokaz formy
	$_KAT['forum']['PERM']['uem']	= '1'; // ochistka tablicy
}

$tid = (empty($_REQUEST['form']['tid']))?0:$_REQUEST['form']['tid'];
$_KAT['forum']['AFTER_ADD']	= 'db/forum/?form[tid]='.$tid;

 $_KAT['onpage_def']['forum']		= 20;
 $_KAT['TABLES']['forum']	= 'forum';
 $_KAT['TITLES']['forum']	= Main::get_lang_str('forum', 'db');
 $_KAT['FILES']['forum']	= 'forum.data.inc.php';
 $_KAT['forum']['hidden']	= 'true'; // dlya db/list

 $_KAT['onpage_def']['forumadmin']		= 20;
 $_KAT['TABLES']['forumadmin']	= 'forum';
 $_KAT['TITLES']['forumadmin']	= Main::get_lang_str('forum', 'db').'(adm)';
 $_KAT['FILES']['forumadmin']	= 'forumadmin.data.inc.php';
 $_KAT['forumadmin']['hidden']	= 'true'; // dlya db/list
}
/* ������� (����) */
if ($_CONF['FEATURES_USED']['dialog'] && $_SESSION['SESS_AUTH']['ID']) {

if (!$_CORE->IS_ADMIN ) {
	$_KAT['dialog']['PERM']['ua']	= '0'; // dobavlenie
	$_KAT['dialog']['PERM']['ue']	= '0'; // redaktirovanie
	$_KAT['dialog']['PERM']['ud']	= '1'; // udalenie
	$_KAT['dialog']['PERM']['ui']	= '1'; // import
	$_KAT['dialog']['PERM']['uf']	= '0'; // pokaz formy
	$_KAT['dialog']['PERM']['uem']	= '1'; // ochistka tablicy
}
    
 $_KAT['dialog']['AFTER_ADD']	= 'db/dialog/';
 $_KAT['onpage_def']['dialog']		= 20;
 $_KAT['TABLES']['dialog']	= 'dialog';
 $_KAT['TITLES']['dialog']	= Main::get_lang_str('dialog', 'db');
 $_KAT['FILES']['dialog']	= 'dialog.data.inc.php';
 $_KAT['dialog']['hidden']	= 'true'; // dlya db/list

}
/* ����� ������������� */
if ($_CONF['FEATURES_USED']['blogs']){
$_KAT['blogs']['reg_only'] = true; // kommentiroat' razresheno tol'ko avtorizovannym 

if (!$_CORE->IS_ADMIN && (!$_KAT['blogs']['reg_only'] || $_KAT['blogs']['reg_only'] && $_SESSION['SESS_AUTH']['ID'] && $_SESSION['SESS_AUTH']['BLOG'])) {
	$_KAT['blogs']['PERM']['ua']	= '0'; // dobavlenie
	$_KAT['blogs']['PERM']['ue']	= '0'; // redaktirovanie
	$_KAT['blogs']['PERM']['ud']	= '1'; // udalenie
	$_KAT['blogs']['PERM']['ui']	= '1'; // import
	$_KAT['blogs']['PERM']['uf']	= '1'; // pokaz formy
	$_KAT['blogs']['PERM']['uem']	= '1'; // ochistka tablicy
}elseif(!$_CORE->IS_ADMIN){
	$_KAT['blogs']['PERM']['ua']	= '1'; // dobavlenie
	$_KAT['blogs']['PERM']['ue']	= '1'; // redaktirovanie
	$_KAT['blogs']['PERM']['ud']	= '1'; // udalenie
	$_KAT['blogs']['PERM']['ui']	= '1'; // import
	$_KAT['blogs']['PERM']['uf']	= '1'; // pokaz formy
	$_KAT['blogs']['PERM']['uem']	= '1'; // ochistka tablicy
}

$tid = (empty($_REQUEST['form']['tid']))?0:$_REQUEST['form']['tid'];
$_KAT['blogs']['AFTER_ADD']	= 'db/blogsuser/';

 $_KAT['onpage_def']['blogs']		= 20;
 $_KAT['TABLES']['blogs']	= 'blogs';
 $_KAT['TITLES']['blogs']	= Main::get_lang_str('blogs', 'db');
 $_KAT['FILES']['blogs']	= 'blogs.data.inc.php';
 $_KAT['blogs']['hidden']	= 'true'; // dlya db/list
 

 
if($_SESSION['SESS_AUTH']['BLOG']){
 $_KAT['onpage_def']['blogsuser']		= 20;
 $_KAT['TABLES']['blogsuser']	= 'blogs';
 $_KAT['TITLES']['blogsuser']	= Main::get_lang_str('blogsuser', 'db');
 $_KAT['FILES']['blogsuser']	= 'blogsuser.data.inc.php';
 $_KAT['blogsuser']['hidden']	= 'true'; // dlya db/list
}

if($_CORE->IS_ADMIN){
 $_KAT['onpage_def']['blogsadmin']		= 20;
 $_KAT['TABLES']['blogsadmin']	= 'blogs';
 $_KAT['TITLES']['blogsadmin']	= Main::get_lang_str('blogs', 'db').'(adm)';
 $_KAT['FILES']['blogsadmin']	= 'blogsadmin.data.inc.php';
 $_KAT['blogsadmin']['hidden']	= 'true'; // dlya db/list
}
/*����� ������ �������������*/
}

 if (in_array('forum', $_CONF['USED_MODULES']) || in_array('blog', $_CONF['USED_MODULES'])) {
 	$_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/forumadmin/"] = array("/ico/a_news.gif", Main::get_lang_str('forums', 'db'), $_KAT['MODULE_NAME'].'/forumadmin');
 }
 
  if (in_array('blogs', $_CONF['USED_MODULES']) ) {
 	$_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/blogsadmin/"] = array("/ico/a_news.gif", Main::get_lang_str('blogs', 'db'), $_KAT['MODULE_NAME'].'/blogsadmin');
 }

/* Users */
 $_KAT['onpage_def']['auth']		= 20;
 $_KAT['TABLES']['auth']	= 'auth_pers';
 $_KAT['TITLES']['auth']	= Main::get_lang_str('users', 'db');
 $_KAT['FILES']['auth']	= 'coredb_auth.data.inc.php';
 $_KAT['auth']['hidden']	= 'true'; // dlya db/list

 if(!$_CONF['MENU_ADMIN']['lk'] )
if (in_array('auth', $_CONF['MENUED_MODULES_ADMIN'])) {
	$_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/auth/"] = array("/ico/a_news.gif", Main::get_lang_str('users', 'db'), $_KAT['MODULE_NAME'].'/auth');
}
if ($_CORE->IS_ADMIN){

	// ������ ������ � �������
	 $_KAT['onpage_def']['faqtoadmin']	= 20;
	 $_KAT['TABLES']['faqtoadmin']			= 'faqtoadmin';
	 $_KAT['SRC']['faqtoadmin']					= 'faqtoadmin';
	 $_KAT['TITLES']['faqtoadmin']			= Main::get_lang_str('faqtoadmin', 'db');
	 $_KAT['FILES']['faqtoadmin']				= 'faqtoadmin.data.inc.php';
	 $_KAT['faqtoadmin']['hidden']			= 'true'; // ��� db/list
	 $_KAT['DOC_PATH']['faqtoadmin']		= '/tmp/inside';
	 $_KAT['faqtoadmin']['adm_fields'][0]	= 'question';
	 $_KAT['faqtoadmin']['AFTER_ADD_INC'] = $_CORE->PATHTPLS."/admin/db/faqtoadmin/after_add_inc.html";


	$_KAT['onpage_def']['admin_faq']	= 10;
	$_KAT['TABLES']['admin_faq']	= 'admin_faq';
	$_KAT['TITLES']['admin_faq']	= 'faq';
	$_KAT['DOC_PATH']['admin_faq'] 	= '/doctxt/admin_faq/db/admin_faq/';
	$_KAT['FILES']['admin_faq']		= 'admin_faq.data.inc.php';
//	$_KAT['admin_faq']['AFTER_ADD']	= '/db/admin_faq/'; 
	$_KAT['admin_faq']['hidden']	= 'TRUE'; // ��� db/list
	$_KAT['admin_faq']['IMPORT']   = array('ts', 'name', 'cont', 'id', 'alias', 'hidden');
	

	$_KAT['onpage_def']['admin_faq_youtube']	= 20;
	$_KAT['TABLES']['admin_faq_youtube']		= 'admin_faq_youtube';
	$_KAT['TITLES']['admin_faq_youtube']		= 'faq_youtube';
	$_KAT['SRC']['admin_faq_youtube']			= 'admin_faq_youtube';
	$_KAT['DOC_PATH']['admin_faq_youtube'] 		= '/doctxt/admin_faq_youtube/db/admin_faq_youtube/';
	$_KAT['FILES']['admin_faq_youtube']			= 'admin_youtube.data.inc.php';
//	$_KAT['admin_faq_youtube']['AFTER_ADD']		= '/db/admin_faq_youtube/'; 
	$_KAT['admin_faq_youtube']['hidden']		= 'TRUE'; // ��� db/list
	$_KAT['admin_faq_youtube']['IMPORT']   		= array('ts', 'name', 'code', 'id', 'alias', 'hidden');
	
/*	$_KAT['onpage_def']['admin_market']	= 10;
	$_KAT['TABLES']['admin_market']		= 'admin_market';
	$_KAT['TITLES']['admin_market']		= 'market';
	$_KAT['DOC_PATH']['admin_market'] 	= '/doctxt/admin_market/db/admin_market/';
	$_KAT['FILES']['admin_market']		= 'admin_market.data.inc.php';
//	$_KAT['admin_market']['AFTER_ADD']	= '/db/admin_market/'; 
	$_KAT['admin_market']['hidden']		= 'TRUE'; // ��� db/list
	$_KAT['admin_market']['IMPORT']   	= array( 'ts', 'name', 'cont', 'id', 'alias', 'hidden', 'code', 'doc', 'doc1' );*/
    
   if (in_array('rbk', $_CONF['MENUED_MODULES_ADMIN']) && $_CORE->CURR_TPL	== 'admin' ){
    	$_KAT['onpage_def']['rbk_billing']	= '20';
    	$_KAT['TABLES']['rbk_billing']	= 'rbk';
    	$_KAT['TITLES']['rbk_billing']	= 'RBKmoney';
    	$_KAT['SRC']['rbk_billing']	    = 'rbk_billing';
    	$_KAT['FILES']['rbk_billing']	= 'coredb_rbk.data.inc.php';
    	$_KAT['rbk_billing']['hidden']	= 'TRUE';
   }
   
	if( $_CORE->CURR_TPL	== 'admin' ){
		$_KAT['onpage_def']['core_shortcuts'] 	= 1000;
		$_KAT['TABLES']['core_shortcuts'] 		= 'core_shortcuts';
		$_KAT['TITLES']['core_shortcuts'] 		= '������� ������';
		$_KAT['SRC']['core_shortcuts'] 			= 'core_shortcuts';
		$_KAT['FILES']['core_shortcuts'] 		= 'core_shortcuts.data.inc.php';
		$_KAT['core_shortcuts']['hidden'] 		= 'true'; // ��� db/list
		$_KAT['DOC_PATH']['core_shortcuts'] 	= '/tmp/inside';
	}
}

/* basket zakaz  addition for module basket */
if($_CONF['FEATURES_USED']['basketsave']) {

	$_KAT['ADM_MENU']["/".$_KAT['MODULE_NAME']."/basket/"] = array("/ico/a_forum.gif", Main::get_lang_str('basket', 'db'), $_KAT['MODULE_NAME'].'/basket');

	$_KAT['coredb_basket']['hidden']	= false; // ��� db/list
	$_KAT['coredb_basket']['create']	= true; // ��� �������� ������� ��� install

	$_KAT['basket']['AFTER_ADD']	= 'db/basket/';
	$_KAT['onpage_def']['basket']		= 20;
	$_KAT['TABLES']['basket']	= 'basket';
	$_KAT['TITLES']['basket']	= Main::get_lang_str('basket', 'db');
	$_KAT['FILES']['basket']	= 'coredb_basket.data.inc.php';
	$_KAT['basket']['hidden']	= 'true'; // dlya db/list
	$_KAT['basket']['NOTHING'] = Main::get_lang_str('empty_basket', 'db');
}

/* If personal pages needed */
if($_CONF['FEATURES_USED']['userspage']) {

	$_KAT['coredb_users']['hidden']	= false; // ��� db/list
	$_KAT['coredb_users']['create']	= false; // ��� �������� ������� ��� install

	$_KAT['users']['AFTER_ADD']	= 'db/users/';
	$_KAT['onpage_def']['users']		= 20;
	$_KAT['TABLES']['users']	= 'auth_pers';
	$_KAT['TITLES']['users']	= Main::get_lang_str('users', 'db');
	$_KAT['FILES']['users']	= 'coredb_users.data.inc.php';
	$_KAT['users']['hidden']	= 'true'; // dlya db/list
}
 
	$_KAT['onpage_def']['robokassa']	= 100;
	$_KAT['TABLES']['robokassa']			= 'robokassa_view';
	$_KAT['TITLES']['robokassa']			= 'Robokassa';
	$_KAT['FILES']['robokassa']			= 'coredb_payments.data.inc.php';
	$_KAT['robokassa']['hidden']				= 'true'; // ��� db/list

/* ������� ������ */
if($_CONF['FEATURES_USED']['scheta']) {
	if (!$_CORE->IS_ADMIN) {
		$_KAT['coredb_scheta']['PERM']['ua']		= '0'; // ����������
		$_KAT['coredb_scheta']['PERM']['ue']		= '0'; // ��������������
		$_KAT['coredb_scheta']['PERM']['ud']		= '0'; // ��������
		$_KAT['coredb_scheta']['PERM']['ui']		= '1'; // ������
		$_KAT['coredb_scheta']['PERM']['uf']		= '0'; // ����� �����
		$_KAT['coredb_scheta']['PERM']['uem']	= '1'; // ������� �������
	}
	$_KAT['coredb_scheta']['hidden']	= false; // ��� db/list

	$_KAT['coredb_scheta']['AFTER_ADD']	= 'db/scheta/';
	$_KAT['onpage_def']['coredb_scheta']		= 20;
	$_KAT['TABLES']['coredb_scheta']	= 'scheta';
	$_KAT['TITLES']['coredb_scheta']	= Main::get_lang_str('scheta', 'db');
	$_KAT['FILES']['coredb_scheta']	= 'coredb_scheta.data.inc.php';
	 $_KAT['DOC_PATH']['coredb_scheta']	= '/tmp/inside'; // to be addded in ins

}

/* ������� ������ */
if($_CONF['FEATURES_USED']['photolabel']) {
	// ������� �� ����
  $_KAT['onpage_def']['photolabel']	= 20;
  $_KAT['TABLES']['photolabel']		= 'photolabel';
  $_KAT['TITLES']['photolabel']		= 'PhotoLabels';
  $_KAT['FILES']['photolabel']		= '_coredb_photolabel.data.inc.php';
  $_KAT['photolabel']['hidden']		= ''; // ��� db/list
  $_KAT['DOC_PATH']['photolabel']		= '/tmp/inside';
}
if ($_CORE->IS_DEV){
	
	$_KAT['onpage_def']['core_menu'] 	= 1000;
	$_KAT['TABLES']['core_menu'] 		= 'core_menu';
	$_KAT['TITLES']['core_menu'] 		= '���� �������';
	$_KAT['SRC']['core_menu'] 			= 'core_menu';
	$_KAT['FILES']['core_menu'] 		= 'core_menu.data.inc.php';
	$_KAT['core_menu']['hidden'] 		= 'true'; // ��� db/list
	$_KAT['DOC_PATH']['core_menu'] 	= '/tmp/inside';
	
}
if (!empty($_CONF['FEATURES_USED']['auth_role']) ) {
	$_KAT['onpage_def']['core_authperm'] 	= 20;
	$_KAT['TABLES']['core_authperm'] 		= 'core_authperm';
	$_KAT['TITLES']['core_authperm'] 		= '������� �������������';
	$_KAT['SRC']['core_authperm'] 			= 'core_authperm';
	$_KAT['FILES']['core_authperm'] 		= 'core_authperm.data.inc.php';
	$_KAT['core_authperm']['hidden'] 		= 'true'; // ��� db/list
	$_KAT['DOC_PATH']['core_authperm'] 	= '/tmp/inside';
	$_KAT['core_authperm']['AFTER_ADD_INC'] 	= $_CORE->CORE_PATHTPLS.'/db/core_authperm/after_add.inc.php';
}
if (!empty($_CONF['FEATURES_USED']['auth_role']) && !empty($_SESSION['SESS_AUTH']['LOGIN'])) {
	$user_perm = SQL::getrow('*',DB_TABLE_PREFIX.'core_authperm', "name = '".$_SESSION['SESS_AUTH']['LOGIN']."'");
	if($user_perm !== false){
		$permissions = explode(',',$user_perm['DB']);
		
		if(is_array($permissions) && count($permissions)){
			$_KAT['permissions'][$user_perm['name']] = array();
			foreach($permissions as $permission){
		
				$_KAT['permissions'][$user_perm['name']][] = $permission;
				$_KAT[$permission]['PERM'] = array(
					'ua'	=> 0, // ����������
					'ue'	=> 0, // ��������������
					'ud'	=> 0, // ��������
					'ui'	=> 0, // ������
					'uf'	=> 0, // ����� �����
					'uem'	=> 0, // ������� �������
				);
			}
		}
	}
	
}
//
// 19.12.2018
// need history (origain vmcrm project)
// /db/install ����� ��������� ��������� ������� ������� *_history, � ������ �������� ����� ���������� ������
// $_KAT[_alias_]['acc_history'] = true;
// ����� ������� �� �����, ��� ������ ���������: /db/acc_history?type=__alias__ 
// ��� ���� ��� ����������� �������� ���������� select_table, FORM_DATA ������ ������ � ��������� _alias_.sheme.ind.php �����, ��� �� ��� ����������� ���� ����������
//
	$_KAT['onpage_def']['acc_history'] 	= 20;
	$_KAT['TABLES']['acc_history'] 		= '__no_metter____not_empty___';
	$_KAT['TITLES']['acc_history'] 		= '������� ���������';
	$_KAT['SRC']['acc_history'] 		= 'acc_history';
	$_KAT['FILES']['acc_history'] 		= 'coredb_acc_history.data.inc.php';
	$_KAT['acc_history']['hidden'] 		= 'true'; // ��� db/list


?>