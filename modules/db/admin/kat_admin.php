<?
//include_once "odc_admin.inc.php";
global $form, $_KAT;
$submit	= @$_REQUEST['submit'];
$submit_x	= @$_REQUEST['submit_x'];
$form	= @$_REQUEST['form'];
if(!empty($_REQUEST['ajax'])){
  foreach($form as $key=>$val){
    if(!is_array($val)){
      $form[$key]=iconv("UTF-8","windows-1251",$val);
    }
  }
}

//$FormTree	= @$_REQUEST['form_tree'];
//
// DOBAVLENIE ILI RELDAKTIROVANIE
//

if ((!empty($submit) || !empty($submit_x)) && (empty($_KAT[$_KAT['KUR_ALIAS']]['PERM']['ua']) || empty($_KAT[$_KAT['KUR_ALIAS']]['PERM']['ue'])) && !isset($_GET['del'])  && !isset($_GET['action'])) { // izmenenie zapisi
	if(KAT::check_form($form, $Cmd) && KAT::save($form) && empty($_REQUEST['apply'])){
		$message	= Main::get_lang_str('data_edit', 'db');

		if (KAT::inc_tpl('kat_admin.submenu.inc.php', $f))
			include $f;

		if ($_KAT[$_KAT['KUR_ALIAS']]['AFTER_ADD_INC']) {
			include $_KAT[$_KAT['KUR_ALIAS']]['AFTER_ADD_INC'];
		}
    if(!empty($_REQUEST['ajax'])) die('ok');
		if (KAT::inc_tpl('after_edit.html', $file))
			include "$file";
			
		if ($_KAT[$_KAT['KUR_ALIAS']]['AFTER_ADD_INC']) {
			include $_KAT[$_KAT['KUR_ALIAS']]['AFTER_ADD_INC'];
		}elseif ($_KAT[$_KAT['KUR_ALIAS']]['AFTER_ADD_CMD']) {

			echo cmd($_KAT[$_KAT['KUR_ALIAS']]['AFTER_ADD_CMD']);

		}elseif ($_KAT[$_KAT['KUR_ALIAS']]['AFTER_ADD']) {
			$add = (empty($_CORE->CLEAR_VERSION)) ? '' : '/clear';
			header("Location: $add/".$_KAT[$_KAT['KUR_ALIAS']]['AFTER_ADD']);
			die();
		}else {
			KAT::get_cont($form['alias']);
		}

	}else {
		if ($_KAT[$_KAT['KUR_ALIAS']]['AFTER_ADD_INC']) {

			include $_KAT[$_KAT['KUR_ALIAS']]['AFTER_ADD_INC'];

		}
    if(!empty($_REQUEST['ajax'])) die('no');
		KAT::show_form($form);
	}

//
// �������� ��� ������
//
}elseif ((isset($_GET['show']) || isset($_GET['hide']) || isset($_GET['udel'])) && empty($_KAT[$_KAT['KUR_ALIAS']]['PERM']['ue']) ) {													// udalenie zapisi
	$act = (isset($_GET['show']))?'0':'1';
	$field = (isset($_GET['field']))?mysql_real_escape_string($_GET['field']):'hidden';
	if(KAT::show_hide($Cmd, $act, $field)){
		$message = Main::get_lang_str('data_edit', 'db');
	}else{
		$message = Main::get_lang_str('data_not_edit', 'db');
  }
		if ($_KAT[$_KAT['KUR_ALIAS']]['AFTER_ADD_INC']) {

			include $_KAT[$_KAT['KUR_ALIAS']]['AFTER_ADD_INC'];

		}
    $path = (!empty($_SERVER['HTTP_REFERER']))?$_SERVER['HTTP_REFERER']:$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS']."/";
    if(!empty($_REQUEST['ajax'])) $path = '/empty/'.$path;
  header("Location: ".$path);
//
// ������� 
//
}elseif (isset($_GET['del']) && empty($_KAT[$_KAT['KUR_ALIAS']]['PERM']['ud']) ) {
	// udalenie zapisi
	if(KAT::del($Cmd)){
		$message = Main::get_lang_str('rec_del', 'db');
		if ($_KAT[$_KAT['KUR_ALIAS']]['AFTER_DEL_INC']) {
			include $_KAT[$_KAT['KUR_ALIAS']]['AFTER_DEL_INC'];
		}
		header("Location: /".$_KAT['MODULE_NAME']."/".$_KAT['KUR_ALIAS']."/?".str_replace("&del",'',$_SERVER['QUERY_STRING']));
	}else
		$message = Main::get_lang_str('rec_not_del', 'db');
	echo $_CORE->cmd_pexec($_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS'].'/search');

//
// IMPORT CSV
//
}elseif (isset($_REQUEST['import']) && empty($_KAT[$_KAT['KUR_ALIAS']]['PERM']['ui'])) { 												// forma importirovanie dannyh
	if (isset($_POST['import_go'])) {											// importirovanie dannyh
		if(false !== ($count = KAT::import($_REQUEST['type']))){
			$_KAT['ERROR'] .= Main::get_lang_str('import_tabl', 'db').$_KAT['KUR_TABLE'].Main::get_lang_str('tabl_add', 'db').intval($count[0]).Main::get_lang_str('tabl_upd', 'db').$count[1].")<BR>";
		}else
			$_KAT['ERROR'] .= Main::get_lang_str('obrab_vhodnyh_dannyh', 'db');
	}

	if (KAT::inc_tpl('import.form.inc.html', $f))
	include $f;

//	include_once (is_file($_CORE->SiteModDir."/import.form.inc.html"))
//		? $_CORE->SiteModDir."/import.form.inc.html"
//		: $_CORE->CoreModDir."/import.form.inc.html";

//
// JeKSPORT CSV
//
}elseif (isset($_GET['export'])) { 												// jeksport VSEH dannyh

	if(false !== ($count = KAT::export(true))){
		return;
	}else
		echo Main::get_lang_str('vyvod_vhodnyh_dannyh', 'db');

//
// Ochistka vsej bazy
//
}elseif (isset($_GET['empty'])&& empty($_KAT[$_KAT['KUR_ALIAS']]['PERM']['uem'])) {													// udalenie zapisi
	if(false !== ($rows = KAT::truncate()))
		echo Main::get_lang_str('tabl_clear', 'db');
	else
		echo Main::get_lang_str('tabl_not_clear', 'db').$_KAT['ERROR'];
	header("Location: /".$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS'].'/search');
	die();

//
// Massovoe redaktirovanie
//
}elseif (isset($_REQUEST['mass_edit_form']) && empty($_KAT[$_KAT['KUR_ALIAS']]['PERM']['uf'])) {
//
// VSTAVIT'' (paste)
//
}elseif (isset($_REQUEST['action'])) {

	//  pometit', dlja peremewenija ili kopirovanija
	if (!empty($_REQUEST[ITEM_CUT])||!empty($_REQUEST[ITEM_COPY])){
		$item_cut = (!empty($_REQUEST[ITEM_CUT]))?$_REQUEST[ITEM_CUT]:$_REQUEST[ITEM_COPY];
		error_log($item_cut." - ".var_export($_SESSION['KAT'], 'true')."\n");
		if( !in_array($item_cut, @$_SESSION['KAT']['CUTTED_ITEM_ALIAS']) ){
			$_SESSION['KAT']['CUTTED_ITEM_ALIAS'][] = $item_cut;
			$_SESSION['KAT']['CUTTED_TYPE'][] = (!empty($_REQUEST[ITEM_CUT]))?ITEM_CUT:ITEM_COPY;
		}

		/*
		vyzyvaetsja v bekgraunde iz JS po Ctrl+X posemu
		*/
		echo "<SCRIPT>parent.CUTTED_KAT='".$item_cut."';</SCRIPT>";
		exit;

	// Vstavka
	}elseif(isset($_REQUEST['paste']) && !empty($_SESSION['KAT']['CUTTED_ITEM_ALIAS']) && is_array($_SESSION['KAT']['CUTTED_ITEM_ALIAS'])){
		if(isset($_REQUEST['form'])){ $casheForm = $_REQUEST['form']; unset($_REQUEST['form']);}

		KAT::paste($_SESSION['KAT']['CUTTED_ITEM_ALIAS'], $_SESSION['KAT']['CUTTED_TYPE']);

		if(isset($casheForm)){ $_REQUEST['form'] = $casheForm; unset($casheForm);}
//		ob_clean();
//		echo "here";
		$_SESSION['KAT']['CUTTED_ITEM_ALIAS'] = '';
		$_SESSION['KAT']['CUTTED_TYPE'] = '';

		echo "<script>parent.location.reload()</script>";
		exit;
	}
//echo "action: ".$_SESSION['KAT']['CUTTED_ITEM_ALIAS'];
	exit;
//
// POKAZAT'' FORMU
//
}elseif (empty($_KAT[$_KAT['KUR_ALIAS']]['PERM']['uf'])) {

//	echo "<PRE>\nCommand:".$Cmd."\n";
	$_KAT['ERROR'] = '';
	$data	= KAT::load($Cmd); // alias
	KAT::show_form($data);

//
// INAChE PEREJTI KUDA-LIBO
//
} else {
	header("Location: /".$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS'].'/search');
	die();
}


if( is_array($_SESSION['KAT']['CUTTED_ITEM_ALIAS']) )
	foreach( @$_SESSION['KAT']['CUTTED_ITEM_ALIAS'] as $k=>$v )
	echo $_SESSION['KAT']['CUTTED_TYPE'][$k]."=>".$v."<br>";


?>

