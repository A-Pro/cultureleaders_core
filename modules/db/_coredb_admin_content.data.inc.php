<?
 if ($_CORE->IS_ADMIN){
	$FORM_DATA= array (
		'id' => 
		array (
			'field_name' => 'id',
			'name' => 'form[id]',
			'title' => 'id',
			'must' => 0,
			'maxlen' => 20,
			'type' => 'hidden'
		),
		'alias' => 
		array (
			'field_name' => 'alias',
			'name' => 'form[alias]',
			'title' => Main::get_lang_str('alias', 'db'),
			'must' => 0,
			'maxlen' => 255,
			'type' => 'hidden'
		),
		'name' => 
		array (
			'field_name' => 'name',
			'name' => 'form[name]',
			'title' => Main::get_lang_str('name', 'db'),
			'must' => 0,
			'maxlen' => 255,
			'type' => 'hidden'
		),
		'admin_title'	=>
		array (
			'field_name' => 'admin_title',
			'name' => 'form[admin_title]',
			'title' => 'Заголовок',
			'must' => 0,
			'style' => 'width:100%',
			'maxlen' => 255,
			'type' => 'textbox'
		),
		'admin_cont' => 
		array (
			'field_name' => 'admin_cont',
			'name' => 'form[admin_cont]',
			'title' => 'Контент',
			'must' => '0',
			'maxlen' => '65535',
			'type' => 'textarea',
			'style' => 'width:100%',
			'rows' => '20',
			'wysiwyg'	=> 'tinymce'
		)
	);
}
?>