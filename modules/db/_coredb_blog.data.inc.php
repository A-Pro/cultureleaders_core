<?

$FORM_ORDER	=	'	ORDER	BY ts	DESC, id desc ';
if (!$_CORE->IS_ADMIN)
	$FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL)";
else 
    $FORM_WHERE	= '';    


$FORM_DATA=	array	(
	'id' =>	
	array	(
		'field_name' =>	'id',
		'name' =>	'form[id]',
		'title'	=> 'id',
		'must' =>	0,
		'maxlen' =>	20,
		'type' =>	'hidden',
	),
	'alias'	=> 
	array	(
		'field_name' =>	'alias',
		'name' =>	'form[alias]',
		'title'	=> Main::get_lang_str('alias', 'db'),
		'must' =>	0,
		'maxlen' =>	20,
		'type' =>	'hidden',
	),
 'name'	=>
	array	(
		'field_name' =>	'name',
		'name' =>	'form[name]',
		'title'	=> Main::get_lang_str('title', 'db'),
		'must' =>	1,
	'style'	=> 'width:100%',
		'maxlen' =>	255,
		'type' =>	'textbox',
		'logic'	=> 'OR',
		'search' =>	"	LIKE '%%%s%%'",
	),
	'tags'	=>
	array	(
		'field_name' =>	'tags',
		'name' =>	'form[tags]',
		'title'	=> Main::get_lang_str('teg', 'db'),
		'must' =>	0,
		'style'	=> 'width:100%',
		'maxlen' =>	255,
		'type' =>	'textbox',
		'logic'	=> 'OR',
		'search' =>	"	LIKE '%%%s%%'",
	),
	'ts'	=>
	array	(
		'field_name' =>	'ts',
		'name' =>	'form[ts]',
		'title'	=> Main::get_lang_str('data', 'db'),
		'must' =>	1,
	'size' =>	15,
		'maxlen' =>	255,
		'type' =>	'textdate',
		'readonly' =>	'true',
	'default'	=> date('Y-m-d')
	),
	'cont' =>	
	array	(
		'field_name' =>	'cont',
		'name' =>	'form[cont]',
		'title'	=> Main::get_lang_str('about', 'db'),
		'must' =>	'0',
		'maxlen' =>	'65535',
		'type' =>	'textarea',
		'style'	=> 'width:100%',
		'rows' =>	'20',
		'wysiwyg'	=> 'tinymce',
//		'wysiwyg_path'	=> '/modules/html/',
		'logic'	=> 'OR',
		'search' =>	"	LIKE '%%%s%%'",
	),
	'doc1' =>	array(
			'field_name' =>	'doc1',	//	dolzhno sovpadat' s 'name'!!!
			'name'	=> 'doc1',
			'title'	=> Main::get_lang_str('foto', 'db'),
			'admwidth' =>	500,
			'type'	=> 'photo',
			'sub_type'	=> 'photo',
			'newname_func'	=> 'get_file_name("ph")',
			'path'	=> KAT::get_data_link( '/f_'.$_KAT['KUR_ALIAS'], $dir, KAT_LOOKIG_DATA_DIR ),
			'abspath'	=> KAT::get_data_path('/f_'.$_KAT['KUR_ALIAS'],	$dir,	KAT_LOOKIG_DATA_DIR),
	),

	'doc'	=> array(
			'field_name' =>	'doc', //	dolzhno sovpadat' s 'name'!!!
			'name'	=> 'doc',
			'title'	=> Main::get_lang_str('add_file', 'db'),
			'type'	=> 'photo',
			'sub_type'	=> 'doc',
			'newname_func'	=> 'get_file_name()',
			'path'	=> KAT::get_data_link( '/f_'.$_KAT['KUR_ALIAS'], $dir, KAT_LOOKIG_DATA_DIR ),
			'abspath'	=> KAT::get_data_path('/f_'.$_KAT['KUR_ALIAS'],	$dir,	KAT_LOOKIG_DATA_DIR),
	),
	'hidden'	=>
	array	(
		'field_name' =>	'hidden',
		'name' =>	'form[hidden]',
		'title'	=> Main::get_lang_str('ne_publ', 'db'),
		'must' =>	0,
		'maxlen' =>	1,
		'type' =>	'checkbox',
	),
)
?>