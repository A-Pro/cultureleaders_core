<?
$_KAT['INDEX'][$_KAT['KUR_ALIAS']]	= 'id';
global $FORM_FIELD4ALIAS;
$FORM_FIELD4ALIAS = 'alias';
$FORM_ORDER	= 'ORDER BY prior,ts DESC ';
$FORM_WHERE = '';


$FORM_DATA = array(
	'id' => array(
		'field_name' 	=> 'id',
		'name' 			=> 'form[id]',
		'title'			=> 'id',
		'must' 			=> 0,
		'maxlen' 		=> 20,
		'type' 			=> 'hidden',
	),
	'alias' => array(
		'field_name' 	=> 'alias',
		'name' 			=> 'form[alias]',
		'title' 		=> 'alias',
		'must' 			=> 0,
		'maxlen' 		=> 20,
		'type' 			=> 'hidden',
		'default' 		=> time()
	),
	'type_id' => array	(
		'field_name' 	=> 'type_id',
		'name' 			=> 'form[type_id]',
		'title'			=> '�������� ������� ��������',
		'must' 			=> 1,
		'style'			=> 'width:100%',
		'maxlen' 		=> 255,
		'type' 			=> 'select_from_table',
		'ex_table' 		=> DB_TABLE_PREFIX.'basket_delivery',
		'ex_table_field' => 'name',
		'id_ex_table' 	=> 'id',
		'prompt' 		=> '���'
	),
	'condition' => array	(
		'field_name' 	=> 'condition',
		'name' 			=> 'form[condition]',
		'title'			=> '��� �������',
		'must' 			=> 1,
		'type' 			=> 'select',
		'sub_type' 		=> 'int',
		'arr' 			=> array('','��','��','�������������','�������')
	),
	'value' => array	(
		'field_name' 	=> 'value',
		'name' 			=> 'form[value]',
		'title'			=> '��������',
		'must' 			=> 0,
		'style'			=> 'width:100%',
		'maxlen' 		=> 20,
		'type' 			=> 'textbox',
		'sub_type' 		=> 'bigint',
		/*'logic'	=> 'OR',
		'search' =>	" LIKE '%%%s%%'",*/
	),
	'cost' => array	(
		'field_name' 	=> 'cost',
		'name' 			=> 'form[cost]',
		'title'			=> '���������',
		'must' 			=> 0,
		'style'			=> 'width:100%',
		'maxlen' 		=> 20,
		'type' 			=> 'textbox',
		'sub_type' 		=> 'bigint',
		/*'logic'	=> 'OR',
		'search' =>	" LIKE '%%%s%%'",*/
	),
	
	'hidden' => array(
		'field_name' 	=> 'hidden',
		'name' 			=> 'form[hidden]',
		'title' 		=> '�� ����������',
		'must' 			=> 0,
		'maxlen' 		=> 1,
		'type' 			=> 'checkbox',
	),
	'ts' => array(
		'field_name' 	=> 'ts',
		'name' 			=> 'form[ts]',
		'title' 		=> '����',
		'must' 			=> 0,
		'size' 			=> 20,
		'maxlen' 		=> 255,
		'type' 			=> 'hidden',
		'readonly' 		=> 'true',
		'default'		=> date('Y-m-d H:i:s')
	),
	'prior' => array (
		'field_name' 	=> 'prior',
		'name' 			=> 'form[prior]',
		'title'			=> '����������',
		'must' 			=> 0,
		'maxlen' 		=> 20,
		'type' 			=>'hidden',
		'sub_type' 		=> 'int'
	)
);
?>