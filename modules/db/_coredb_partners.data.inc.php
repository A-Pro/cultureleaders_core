<?

$FORM_ORDER	=	'	ORDER BY  name DESC';
if (!$_CORE->IS_ADMIN)
	$FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL)";


$FORM_DATA=	array	(
	'id' =>	
	array	(
		'field_name' =>	'id',
		'name' =>	'form[id]',
		'title'	=> 'id',
		'must' =>	0,
		'maxlen' =>	20,
		'type' =>	'hidden',
	),
	'alias'	=> 
	array	(
		'field_name' =>	'alias',
		'name' =>	'form[alias]',
		'title'	=> '�����',
		'must' =>	0,
		'maxlen' =>	20,
		'type' =>	'hidden',
	),
 'name'	=>
	array	(
		'field_name' =>	'name',
		'name' =>	'form[name]',
		'title'	=> '��������',
		'must' =>	1,
		'style'	=> 'width:100%',
		'maxlen' =>	255,
		'type' =>	'textbox',
		'logic'	=> 'OR',
		'search' =>	"	LIKE '%%%s%%'",
	),
 'about' =>	
	array	(
		'field_name' =>	'about',
		'name' =>	'form[about]',
		'title'	=> '��������',
		'must' =>	'0',
		'maxlen' =>	'65535',
		'type' =>	'textarea',
		'style'	=> 'width:100%',
		'rows' =>	'20',
		'wysiwyg'	=> 'tinymce',
		'logic'	=> 'OR',
		'search' =>	"	LIKE '%%%s%%'",
	),
	'doc' =>	array(
			'field_name' =>	'doc',	//	dolzhno sovpadat' s 'name'!!!
			'name'	=> 'doc',
			'title'	=> '�������',
			'admwidth' =>	500,
			'type'	=> 'photo',
			'sub_type'	=> 'photo',
			'newname_func'	=> 'get_file_name("ph")',
			'path'	=> KAT::get_data_link( '/f_'.$_KAT['KUR_ALIAS'], $dir, KAT_LOOKIG_DATA_DIR ),
			'abspath'	=> KAT::get_data_path('/f_'.$_KAT['KUR_ALIAS'],	$dir,	KAT_LOOKIG_DATA_DIR),
	),
	'hidden'	=>
	array	(
		'field_name' =>	'hidden',
		'name' =>	'form[hidden]',
		'title'	=> '�� �����������',
		'must' =>	0,
		'maxlen' =>	1,
		'type' =>	'checkbox',
	),
)
?>