<?
$_KAT['INDEX'][$_KAT['KUR_ALIAS']]	= 'id';

$FORM_ORDER	= 'ORDER BY prior,ts DESC ';
$FORM_WHERE = '';
if (!$_CORE->IS_ADMIN)
	$FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL)";

$FORM_DATA = array(
	'id' => array(
		'field_name' 	=> 'id',
		'name' 			=> 'form[id]',
		'title'			=> 'id',
		'must' 			=> 0,
		'maxlen' 		=> 20,
		'type' 			=> 'hidden',
	),
	'alias' => array(
		'field_name' 	=> 'alias',
		'name' 			=> 'form[alias]',
		'title' 		=> 'alias',
		'must' 			=> 0,
		'maxlen' 		=> 20,
		'type' 			=> 'hidden'
	),
	'name' => array	(
		'field_name' 	=> 'name',
		'name' 			=> 'form[name]',
		'title'			=> '����� ����� ������',
		'must' 			=> 1,
		'style'			=> 'width:100%',
		'maxlen' 		=> 255,
		'type' 			=> 'textbox',
		/*'logic'	=> 'OR',
		'search' =>	" LIKE '%%%s%%'",*/
	),
	'worktime' => array	(
		'field_name' 	=> 'worktime',
		'name' 			=> 'form[worktime]',
		'title'			=> '����� ������ �(���) ��������',
		'must' 			=> 0,
		'style'			=> 'width:100%',
		'maxlen' 		=> 3000,
		'type' 			=> 'textarea',
	),
	'cont' => array(
		'field_name' 	=>	'cont',
		'name' 			=> 'form[cont]',
		'title'			=> '��� ����� �������',
		'must' 			=> 0,
		'maxlen' 		=> '65535',
		'type' 			=> 'textarea',
		'style'			=> 'width:100%',
		'rows' 			=> '20',
	),
	'doc' => array(
		'field_name' 	=> 'doc',
		'name'			=> 'form[doc]',
		'title' 		=> '�������� �������',
		'maxwidth' 		=> 1600,
		'maxheight' 	=> 1200,
		'admwidth' 		=> 300,
		'type' 			=> 'photo',
		'sub_type' 		=> 'photo',
	),
	
	'hidden' => array(
		'field_name' 	=> 'hidden',
		'name' 			=> 'form[hidden]',
		'title' 		=> '�� ����������',
		'must' 			=> 0,
		'maxlen' 		=> 1,
		'type' 			=> 'checkbox',
	),
	'ts' => array(
		'field_name' 	=> 'ts',
		'name' 			=> 'form[ts]',
		'title' 		=> '����',
		'must' 			=> 0,
		'size' 			=> 20,
		'maxlen' 		=> 255,
		'type' 			=> 'hidden',
		'readonly' 		=> 'true',
		'default'		=> date('Y-m-d H:i:s')
	),
	'prior' => array (
		'field_name' 	=> 'prior',
		'name' 			=> 'form[prior]',
		'title'			=> '����������',
		'must' 			=> 0,
		'maxlen' 		=> 20,
		'type' 			=>'hidden',
		'sub_type' 		=> 'int'
	)
);
?>