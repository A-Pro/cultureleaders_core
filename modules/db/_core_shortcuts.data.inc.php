<?
$FORM_WHERE = '';
$FORM_ORDER	=	' ORDER BY prior asc, id asc ';
if (!$_CORE->IS_DEV){
	$FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL)";
}
$_KAT['core_shortcuts']['SEO'] = false;
$FORM_DATA=	array	(
	'id' =>	
	array	(
		'field_name' =>	'id',
		'name' =>	'form[id]',
		'title'	=> 'id',
		'must' =>	0,
		'maxlen' =>	20,
		'type' =>	'hidden',
	),
	'alias'	=> 
	array	(
		'field_name' =>	'alias',
		'name' =>	'form[alias]',
		'title'	=> 'alias',
		'must' =>	0,
		'maxlen' =>	20,
		'type' =>	'hidden',
	),
	'name'	=>
	array	(
		'field_name' =>	'name',
		'name' =>	'form[name]',
		'title'	=> '������',
		'must' =>	1,
		'style'	=> 'width:100%',
		'maxlen' =>	255,
		'type' =>	'textbox',
		'logic'	=> 'OR',
		'search' =>	"	LIKE '%%%s%%'",
	),
	'icon' => 
	array (
		'field_name' => 'icon',
		'name' => 'form[icon]',
		'title' => '������',
		'must' => '0',
		'maxlen' => '255',
		'type' => 'radiobtn',
		'inrow' => 4,
		'arr'	=> array( ''=> '��� ������', 'icon-list-alt'=>'<i class="icon-list-alt"></i>', 'icon-bookmark'=>'<i class="icon-bookmark"></i>', 'icon-signal'=>'<i class="icon-signal"></i>', 'icon-comment'=>'<i class="icon-comment"></i>', 'icon-user'=>'<i class="icon-user"></i>', 'icon-file'=>'<i class="icon-file"></i>', 'icon-picture'=>'<i class="icon-picture"></i>', 'icon-tag'=>'<i class="icon-tag"></i>' ),
		'style' => 'width:100%'
	),
	'title'	=>
	array	(
		'field_name' =>	'title',
		'name' =>	'form[title]',
		'title'	=> '�������� ������',
		'must' =>	0,
		'style'	=> 'width:100%',
		'maxlen' =>	255,
		'type' =>	'textbox'
	),
	'prior'	=>
	array	(
		'field_name' =>	'prior',
		'name' =>	'form[prior]',
		'title'	=> '������� ����������',
		'must' =>	0,
		'maxlen' =>	255,
		'type' =>	'hidden'
	),
	'hidden'	=>
	array	(
		'field_name' =>	'hidden',
		'name' =>	'form[hidden]',
		'title'	=> Main::get_lang_str('ne_publ', 'db'),
		'must' =>	0,
		'maxlen' =>	1,
		'type' =>	'checkbox',
	),
)
?>