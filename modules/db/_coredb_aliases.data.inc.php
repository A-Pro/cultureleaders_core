<?
//$FORM_SEARCH	=	array( 
//'city', 'far', 'square', 'vodo', 'energy', 'otopl', 'kanal', 'gaz', 'arenda', 'prodaja'
//);

$FORM_ORDER	= ' ORDER BY url DESC ';
$FORM_WHERE		= '';
$FORM_SELECT	= '';
$FORM_FROM		= '';


$FORM_DATA= array (
  'name'	=>
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => Main::get_lang_str('adress', 'db'),
    'must' => 1,
	'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  
  'url'	=>
  array (
    'field_name' => 'url',
    'name' => 'form[url]',
    'title' => Main::get_lang_str('alias', 'db'),
    'must' => 1,
	'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'textbox',
  ),

  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'alias' => 
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => '',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
)
?>