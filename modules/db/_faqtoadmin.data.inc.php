<?
if ($_CORE->IS_ADMIN){
	$FORM_ORDER	= '	ORDER BY id DESC';
	$FORM_WHERE = '';
	$FORM_DATA = array(
	  'id' => 
	  array	(
		'field_name' =>	'id',
		'name' => 'form[id]',
		'title'	=> 'id',
		'must' => 0,
		'maxlen' =>	20,
		'type' => 'hidden',
	  ),
	  'ts'	=>
	  array (
	    'field_name' => 'ts',
	    'name' => 'form[ts]',
	    'title' => Main::get_lang_str('data', 'db'),
	    'must' => 0,
		'size' => 15,
	    'maxlen' => 255,
	    'type' => 'hidden',
	    'readonly' => 'true',
		'default'	=> date('Y-m-d')
	  ),
	  'alias' => 
	  array (
	    'field_name' => 'alias',
	    'name' => 'form[alias]',
	    'title' => Main::get_lang_str('alias', 'db'),
	    'must' => 0,
	    'maxlen' => 20,
	    'type' => 'hidden',
	  ),
	  'name'	=>
	  array	(
		'field_name' =>	'name',
		'name' => 'form[name]',
		'title'	=> "name",
		'must' => 0,
		'maxlen' =>	'400',
		'type' => 'hidden',
		'default'	=> time()
	  ),
	  'question'	=>
	  array	(
		'field_name' =>	'question',
		'name' => 'form[question]',
		'title'	=> "������",
		'must' => 0,
		'maxlen' =>	'65535',
		'type' => 'textarea',
		'style'	=> 'width:100%',
		'rows' => '10',
	  ),
	 'cont'	=>
	  array	(
		'field_name' =>	'cont',
		'name' => 'form[cont]',
		'title'	=> "�����",
		'must' => 0,
		'maxlen' =>	'65535',
		'type' => 'textarea',
		'style'	=> 'width:100%',
		'rows' => '10',
		'wysiwyg'	=> 'tinymce',
		'logic'	=> 'OR',
		'search' =>	" LIKE '%%%s%%'",
	  ),
	   'hidden' => 
	  array (
	    'field_name' => 'hidden',
	    'name' => 'form[hidden]',
	    'title' => '�� ����������',
	    'must' => 0,
	    'maxlen' => 20,
	    'type' => 'checkbox',
	  )
	);
}
?>