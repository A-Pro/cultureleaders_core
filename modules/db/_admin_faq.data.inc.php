<?
if ($_CORE->IS_ADMIN){

	$FORM_ORDER	= '  ';
	
	
	$FORM_DATA= array (
	  'ts'	=>
	  array (
		'field_name' => 'ts',
		'name' => 'form[ts]',
		'title' => Main::get_lang_str('data', 'db'),
		'must' => 1,
		'size' => 15,
		'maxlen' => 255,
		'type' => 'hidden',
	//    'readonly' => 'true',
			'default'	=> date('Y-m-d'),
			'sub_type' => 'varchar'
	  ),
	  'name'	=>
	  array (
		'field_name' => 'name',
		'name' => 'form[name]',
		'title' => Main::get_lang_str('name', 'db'),
		'must' => 1,
		'style'	=> 'width:100%',
		'type' => 'textbox',
			'logic' => 'OR',
			'search' => " LIKE '%%%s%%'",
	  ),
	  'cont' =>	
	  array	(
		  'field_name' =>	'cont',
		  'name' =>	'form[cont]',
		  'title'	=> Main::get_lang_str('about', 'db'),
		  'must' =>	'0',
		  'maxlen' =>	'65535',
		  'type' =>	'textarea',
		  'style'	=> 'width:100%',
		  'rows' =>	'20',
		  'wysiwyg'	=> 'tinymce',
		  'logic'	=> 'OR',
		  'search' =>	"	LIKE '%%%s%%'",
	  ),
	  'id' => 
	  array (
		'field_name' => 'id',
		'name' => 'form[id]',
		'title' => 'id',
		'must' => 0,
		'maxlen' => 20,
		'type' => 'hidden',
	  ),
	  'alias' => 
	  array (
		'field_name' => 'alias',
		'name' => 'form[alias]',
		'title' => Main::get_lang_str('alias', 'db'),
		'must' => 0,
		'maxlen' => 20,
		'type' => 'hidden',
	  ),
	  'hidden'	=>
	  array (
		'field_name' => 'hidden',
		'name' => 'form[hidden]',
		'title' => Main::get_lang_str('ne_publ', 'db'),
		'must' => 0,
		'maxlen' => 1,
		'type' => 'checkbox',
			'sub_type' => 'varchar'
	  ),
	);
}
?>