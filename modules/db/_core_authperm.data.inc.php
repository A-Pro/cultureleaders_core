<?
$FORM_ORDER	= 'ORDER BY id desc';
$FORM_FROM	= '';
$FORM_BEFORE	= '';
$FORM_WHERE	= 'AND (hidden != 1 OR hidden IS NULL)'; // objazatel'no esli gde-libo ispol'zuetsja
// ������� ������ ������������� ��������� 
$catalogs = array();
$usetables = SQL::tlist();
if(!empty($_KAT['core_authperm']['DB']) && count($_KAT['core_authperm']['DB']))
    $authpermTables = $_KAT['core_authperm']['DB'];
else
    $authpermTables = array_keys($_KAT['TABLES']);
foreach ($authpermTables as $usekey){
    $usetab = $_KAT['TABLES'][$usekey];
    if(!empty($usekey)){
    	if (in_array((( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . $usetab, $usetables) && !empty($_KAT['TITLES'][$usekey])){
    		$catalogs[$usekey] = $_KAT['TITLES'][$usekey];
    	}
    }
}
$FORM_DATA= array (
  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'alias' =>  // dlja standarta
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => Main::get_lang_str('alias', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'hidden' =>  // dlja standarta
  array (
    'field_name' => 'hidden',
    'name' => 'form[hidden]',
    'title' => Main::get_lang_str('ne_publ', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
    'default' => 0,
  ),
  'name' => 
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => '�����',
    'type' => 'textbox',
    'must' => '1',
    'size' => '20',
    'maxlen' => '255',
  ),
  'password' => 
  array (
    'field_name' => 'password',
    'name' => 'form[password]',
    'title' => '������',
    'maxlen' => '25',
    'must' => '1',
    'type' => 'textbox',
    'size' => '20',
  ),
  'DOCTXT' => 
  array (
    'field_name' => 'DOCTXT',
    'name' => 'form[DOCTXT]',
    'title' => '������ � ����������� ���������(������ DOCTXT)',
    'must' => 0,
    'maxlen' => 1,
    'type' => 'checkbox',
    'sub_type' => 'int'
  ),
  
  'BASKETORDERS' => 
  array (
    'field_name' => 'BASKETORDERS',
    'name' => 'form[BASKETORDERS]',
    'title' => '������ ������ � ������ ������(BASKETORDERS)',
    'must' => 0,
    'maxlen' => 1,
    'type' => 'checkbox',
    'sub_type' => 'int'
  ),
    'BASKETORDERS_MINI' =>
        array (
            'field_name' => 'BASKETORDERS_MINI',
            'name' => 'form[BASKETORDERS_MINI]',
            'title' => '������ � ������� �������(BASKETORDERS)',
            'must' => 0,
            'maxlen' => 1,
            'type' => 'checkbox',
            'sub_type' => 'int'
        ),
  'DB' => 
  array(
    'field_name' => 'DB',
    'name' => 'form[DB]',
    'title' => '������ � ���������',
    'style' => 'width:100%',
    'must' => 0,
    'maxlen' => 100,
    'type' => 'multiselect_from_table',
    'subtype' => 'checkbox',
    'arr' => $catalogs
  ),
)
?>