<?
$_KAT['INDEX'][$_KAT['KUR_ALIAS']]	= 'id';

$FORM_ORDER	= 'ORDER BY id ASC ';
$FORM_WHERE = '';
if (!$_CORE->IS_ADMIN)
	$FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL)";

$FORM_DATA = array(
	'id' => array(
		'field_name' 	=> 'id',
		'name' 			=> 'form[id]',
		'title'			=> 'id',
		'must' 			=> 0,
		'maxlen' 		=> 20,
		'type' 			=> 'hidden',
	),
	'alias' => array(
		'field_name' 	=> 'alias',
		'name' 			=> 'form[alias]',
		'title' 		=> 'alias',
		'must' 			=> 0,
		'maxlen' 		=> 20,
		'type' 			=> 'hidden'
	),
	'name' => array	(
		'field_name' 	=> 'name',
		'name' 			=> 'form[name]',
		'title'			=> '�������� ����',
		'must' 			=> 1,
		'style'			=> 'width:100%',
		'maxlen' 		=> 255,
		'type' 			=> 'textbox',
		/*'logic'	=> 'OR',
		'search' =>	" LIKE '%%%s%%'",*/
	),
	'zone' => array (
		'field_name' => 'zone',
		'name' => 'form[zone]',
		'title' => '����� ����',
		'must' => 0,
		'maxlen' => 3,
		'type' => 'textbox',
	),
	'weight' => 
	array (
		'field_name' => 'weight',
		'name' => 'form[weight]',
		'title' => '���',
		'must' => '0',
		'maxlen' => '255',
		'type' => 'select',
		'arr'	=> array( '0.3' => '�� 0,3', '0.5' =>'�� 0,5', '1' => '�� 1','20' => '20','30' => '30' ),
		'style' => 'width:100%'
	),
	'cost' => array(
		'field_name' 	=>	'cost',
		'name' 			=> 'form[cost]',
		'title'			=> '��������� ��������',
		'must' 			=> 0,
		'maxlen' 		=> 50,
		'type' 			=> 'textbox',
		'style'			=> 'width:100%',
	),
	'hidden' => array(
		'field_name' 	=> 'hidden',
		'name' 			=> 'form[hidden]',
		'title' 		=> '�� ����������',
		'must' 			=> 0,
		'maxlen' 		=> 1,
		'type' 			=> 'checkbox',
	),
	'ts' => array(
		'field_name' 	=> 'ts',
		'name' 			=> 'form[ts]',
		'title' 		=> '����',
		'must' 			=> 0,
		'size' 			=> 20,
		'maxlen' 		=> 255,
		'type' 			=> 'hidden',
		'readonly' 		=> 'true',
		'default'		=> date('Y-m-d H:i:s')
	),
);
?>