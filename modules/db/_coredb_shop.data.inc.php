<?
// ������� ������� 
$FORM_ORDER	= ' ORDER BY id ASC ';
$FORM_ORDER_FIELD = 'id';
$FORM_SEARCH	=	array( 
	'size'
//'city', 'far', 'square', 'vodo', 'energy', 'otopl', 'kanal', 'gaz', 'arenda', 'prodaja'
);

$FORM_DATA= array (
  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'alias' => 
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => 'alias',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'hidden'	=>
  array	(
	  'field_name' =>	'hidden',
	  'name' =>	'form[hidden]',
	  'title'	=> Main::get_lang_str('ne_publ', 'db'),
	  'must' =>	0,
	  'maxlen' =>	1,
	  'type' =>	'checkbox',
  ),
  'ts'	=>
  array	(
	  'field_name' =>	'ts',
	  'name' =>	'form[ts]',
	  'title'	=> Main::get_lang_str('data', 'db'),
	  'must' =>	1,
  'size' =>	15,
	  'maxlen' =>	255,
	  'type' =>	'textdate',
	  'readonly' =>	'true',
  'default'	=> date('Y-m-d')
  ),
  'name'	=>
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => '��������',
    'must' => 1,
    'maxlen' => 128,
    'type' => 'textbox',
	'class' => 'textField-03',
	  ),
  'price' => 
  array (
    'field_name' => 'price',
    'name' => 'form[price]',
    'title' => '����',
    'must' => 1,
    'maxlen' => 6,
    'type' => 'textbox',
  ),
  'anons' => 
  array (
    'field_name' => 'anons',
    'name' => 'form[anons]',
    'title' => '������� ��������',
    'must' => '0',
    'maxlen' => '300',
    'type' => 'textarea',
    'cols' => '50',
    'rows' => '5',
  ),
  'cont' => 
  array (
    'field_name' => 'cont',
    'name' => 'form[cont]',
    'title' => '������ �������',
    'must' => '0',
    'maxlen' => '65535',
    'type' => 'textarea',
    'cols' => '50',
    'rows' => '20',
	'wysiwyg'	=> 'tinymce',
	'logic'	=> 'OR',
	'search' =>	"	LIKE '%%%s%%'",
  ),
  'category' => 
  array (
    'field_name' => 'category',
    'name' => 'form[category]',
    'title' => '������ ������',
    'must' => '0',
    'maxlen' => 6,
	'class'	=> 'hide',
    'type' => 'select_from_table',
	'ex_table' => ( (defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') .'category',
	'id_ex_table' => 'name',
	'ex_table_field' => 'name',
  ),
  'doc'	=> array(
	'field_name' =>	'doc', 
	'name'	=> 'doc',
	'title'	=> Main::get_lang_str('add_file', 'db'),
	'type'	=> 'photo',
	'sub_type'	=> 'doc',
	'newname_func'	=> 'get_file_name()',
	'path'	=> KAT::get_data_link( '/f_'.$_KAT['KUR_ALIAS'], $dir, KAT_LOOKIG_DATA_DIR ),
	'abspath'	=> KAT::get_data_path('/f_'.$_KAT['KUR_ALIAS'],	$dir,	KAT_LOOKIG_DATA_DIR),
  ),
)
?>