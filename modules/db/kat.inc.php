<?php
/**
 * Some testx here 
 * 
 * And some new one
 * 
 *  @todo some some some
 * @var $_KAT
 *  @desc Description ?
 */
global $SQL_DBCONF, $_CORE, $_KAT, $_LANGUAGE;


if (!$_CORE->dll_load('class.DBCQ'))
	die ($_CORE->error_msg());

if (!Main::load_lang_mod('db')){
};
$_KAT['MODULE_NAME']	= 'db';
$_KAT['MODULE_TITLE']	= Main::get_lang_str('dir', 'db');
$_KAT['MODULE_DESC']	= Main::get_lang_str('module_tabl', 'db');
$_KAT['NO_PLINE']		= 0;


if (!defined('KAT_FILE_NOT_FOUND')) define ("KAT_FILE_NOT_FOUND",Main::get_lang_str('file_not_fnd', 'db'));
if (!defined('KAT_IMPORT_WRONG_DELIMETER')) define ("KAT_IMPORT_WRONG_DELIMETER",Main::get_lang_str('wrong_del', 'db'));
if (!defined('KAT_IMPORT_WRONG_SETTINGS')) define ("KAT_IMPORT_WRONG_SETTINGS",Main::get_lang_str('not_set_mechanism', 'db'));
if (!defined('KAT_IMPORT_WRONG_DIR')) define ("KAT_IMPORT_WRONG_DIR",Main::get_lang_str('wrong_kat', 'db'));
if (!defined('KAT_SEARCH_INSIDE')) define ("KAT_SEARCH_INSIDE","inside");
if (!defined('KAT_LOOKIG_DATA_FILE')) define ("KAT_LOOKIG_DATA_FILE",'file');
if (!defined('KAT_LOOKIG_DATA_DIR')) define ("KAT_LOOKIG_DATA_DIR",'dir');

/**
 * Klass KAT soderzhit osnovnoj funkcional modulja db
 *
 */
class KAT {

/**
 * Vyvod ob'ekta (ili polja ob'ekta) po peredanomu aliasu i polju
 * 
 * Ispol'zuet klass cForm, podkljuchaemyj pri pomowi instrukcii $_CORE->dll_load(). 
 * 
 * Shablon: view.item.html, podkljuchaetsja pri pomowi KAT::inc_tpl().
 * 
 * Array $form zapolnjaetsja pri pomowi KAT::load(). Array $form globaliziruetsja i mozhet byt' ispol'zovan v shablone, 
 * takzhe globalizirujutsja $doc, $doc1,...,$doc10 i $big_doc, $big_doc1,...,$big_doc10 (rezul'tat raboty metoda KAT::load()).
 * 
 * Esli shablon ne najden, to vyvod array $form osuwestvljaetsja pri pomowi cForm::Print_come().
 * 
 * @global Array osnovnoj massiv modulja db 
 * @global Main jadro
 * @global Array globaliziruemyj rezul'tat
 * @global Array nastroechnyj massiv 
 * @uses $_CORE
 * @uses $FORM_DATA
 * @uses KAT::load()
 * @uses KAT::inc_tpl() 
 * @uses $_KAT['cForm']->Print_come()
 * @uses $_CORE->dll_load()
 * @uses KAT_FILE_NOT_FOUND
 * @param string $alias
 * @param string $field
 * @return array $form
 */
	function get_cont( $alias, $field = '')
	{
		// Ubran $field = 'cont' po umolchaniju, t.k. byla ispravlena funcija load, ran'she rabotala ne verno.
		global $_KAT, $_CORE, $form, $FORM_DATA, $_PROJECT, $_CONF;
		$form	= KAT::load( $alias, $field);

		$_PROJECT['loaded'][] = array( 'parent' => "/".$_KAT['MODULE_NAME']."/".$_KAT['KUR_ALIAS']."/", 'data' => $form );
		$_PROJECT['loaded']['last'] = array( 'parent' => "/".$_KAT['MODULE_NAME']."/".$_KAT['KUR_ALIAS']."/", 'data' => $form );

		if (is_array($form) && count($form)) { 
			if (KAT::inc_tpl('view.item.html', $file)) {
				include "$file";  
//	09.08.2012 land		if ( KAT::inc_tpl('db_dialog.form.html', $file) && $_CONF['FEATURES_USED']['dialog'] && ( $_CONF['DIALOG']['db1']===$_KAT['KUR_ALIAS'] || $_CONF['DIALOG']['db2']===$_KAT['KUR_ALIAS'] )) {
//					include "$file"; 
//				}
			}else {
				if (empty($_KAT['cForm'])) {
					$_CORE->dll_load('class.cForm');
					$_KAT['cForm']	= new cForm($FORM_DATA);
				}
				unset($_KAT['cForm']->Arr['alias']);
				unset($_KAT['cForm']->Arr['id']);
				unset($_KAT['cForm']->Arr['anons']);
				echo $_KAT['cForm']->Print_come();
			}
			return $form;
		}
	}

/**
 * Vozvrawaet nabor dannyh ob'ekta po peredanomu aliasu i polju, esli pole pustoe, to vozvrawaetsja ves' ob'ekt.
 * 
 * Vyborka osuwestvljaetsja metodom <code>SQL::sel(' * '.$add_sel,  $_KAT['KUR_TABLE'].$FORM_FROM, $where.$add_where, $FORM_ORDER, 0);</code>
 * $add_sel formiruetsja na osnove $FORM_SELECT, $add_where formiruetsja na osnove $FORM_WHERE.
 * Sozdaet global'nye peremennye $doc, $doc1,...,$doc10 i $big_doc, $big_doc1,..., $big_doc10. Otvechajuwie za prikreplennye fajly.
 * 
 * c 14/03/2006 ubral uslovija WHERE, dlja chtenija konkretnoj zapisi.
 * 
 * @global Array osnovnoj nastroechnyj massiv
 * @uses $FORM_FROM
 * @uses $FORM_ORDER
 * @uses $FORM_SELECT
 * @uses $FORM_WHERE
 * @uses $_KAT['KUR_TABLE']
 * @uses SQL::sel();
 * @param string $alias 
 * @param string $field
 * @return array $data
 */
	function load( $alias = '', $field = '' )
	{
		global $_KAT, $FORM_SELECT, $FORM_WHERE, $FORM_FROM, $FORM_ORDER; // add $FORM_ORDER for articles
		$add_sel = (!empty($FORM_SELECT)) ? $FORM_SELECT : '';
		// dopolnitel'no dlja perekrestnyh vyborok (vpervye v EFES)
		$add_where	= (!empty($FORM_WHERE)) ? $FORM_WHERE : '';
		$data	= array();
		if (empty($alias))
			return false;
    $pkey	= (empty($_KAT['ALIAS'][$_KAT['KUR_ALIAS']])) ? 'alias' : $_KAT['ALIAS'][$_KAT['KUR_ALIAS']];
		$where	= " ".$_KAT['KUR_TABLE'].".$pkey = '$alias' ";
		$res	= SQL::sel(' * '.$add_sel,  $_KAT['KUR_TABLE'].$FORM_FROM, $where, $FORM_ORDER, DEBUG); // .$add_where, $FORM_ORDER
		if ($res->NumRows > 0) {
			$res->FetchArray(0);
			$data	= $res->FetchArray;
			$res->Destroy();
		}
		/*
		* dlja prikreplennyh fajlov 
		*/
		if (!empty($field)) {
			echo $data[$field];
			return $data[$field];
		}

		global $doc;
		global $big_doc;
		$doc	= @$data['doc'];
		$big_doc	= @$data['big_doc'];
		for ($i=0;$i<10;$i++) {
			global ${'doc'.$i},${'big_doc'.$i};
			${'doc'.$i}	= @$data['doc'.$i];
			${'big_doc'.$i}	= @$data['big_doc'.$i];
		}

		return $data;
	}

/**
 * Udaljaet ob'ekt s aliasom $alias, vozvrawaet rezul'tat udalenija(true/false)
 * 
 * Esli vkljuchen demo-rezhim ($_CORE->IS_DEMO==true)
 * ili suwestvuet $_KAT[$_KAT['KUR_ALIAS']]['PERM']['ud'], to udalenija ne proishodit. 
 * 
 * @uses $_KAT['KUR_ALIAS']
 * @uses $_KAT[$_KAT['KUR_ALIAS']]['PERM']['ud']
 * @uses SQL::del()
 * @param string $alias
 * @return bool
 */
	function del( $alias)
	{
		global $_KAT, $_CORE, $FORM_DATA;
		// if demo
		if ($_CORE->IS_DEMO || !empty($_KAT[$_KAT['KUR_ALIAS']]['PERM']['ud'])) return true;

		$data	= array();
		if (empty($alias))
			return false;
    $sel = SQL::getrow('*',$_KAT['KUR_TABLE']," alias = '$alias' ");
    $d = KAT::get_data_path('/f_'.$_KAT['KUR_ALIAS'], $dir);
    if(isset($sel['doc']) && !empty($sel['doc'])){
      $dir = (!empty($FORM_DATA['doc']['abspath']))?$FORM_DATA['doc']['abspath']:$dir;
      unlink($dir.'/'.$sel['doc']);
    }
    for($i=1; $i<=10; $i++){
      if(isset($sel['doc'.$i]) && !empty($sel['doc'.$i])){
     	$dir = (!empty($FORM_DATA['doc'.$i]['abspath']))?$FORM_DATA['doc'.$i]['abspath']:$dir;
        unlink($dir.'/'.$sel['doc'.$i]);
      }      
    }
		$where	= " alias = '$alias' ".$_KAT[$_KAT['KUR_ALIAS']]['WHERE_DEL']; // for users del
		$res	= SQL::del( $_KAT['KUR_TABLE'], $where, 0);
		if ($res->Result) {
		  
            // if history
            if (!empty($_KAT[$_KAT['KUR_ALIAS']]['acc_history']))     {
                $change_saved = KAT::save_history( $_KAT['KUR_TABLE'], array('id' => $id), array('id' => 'delete'), false );
		      }
                        
			return true;
		}else {
			return false;
		}
	}
/**
 * Udaljaet ob'ekt s aliasom $alias, vozvrawaet rezul'tat udalenija(true/false)
 * 
 * Esli vkljuchen demo-rezhim ($_CORE->IS_DEMO==true)
 * ili suwestvuet $_KAT[$_KAT['KUR_ALIAS']]['PERM']['ud'], to udalenija ne proishodit. 
 * 
 * @uses $_KAT['KUR_ALIAS']
 * @uses $_KAT[$_KAT['KUR_ALIAS']]['PERM']['ed']
 * @uses SQL::show_hide()
 * @param string $alias
 * @param int $act
 * @return bool
 */
	function show_hide( $alias, $act, $field = 'hidden')
	{
		global $_KAT, $_CORE;
		// if demo
		if ($_CORE->IS_DEMO || !empty($_KAT[$_KAT['KUR_ALIAS']]['PERM']['ed'])) return true;

		$data	= array();
		if (empty($alias))
			return false;
        
        $act = (int)$act;
		$where	= " alias = '$alias' "; // for users del
        
        
		$res	= SQL::upd( $_KAT['KUR_TABLE'],"`{$field}`='".$act."'", $where, 0);
		if ($res->Result) {

            // if history
            if (!empty($_KAT[$_KAT['KUR_ALIAS']]['acc_history']))     {
                $id = SQL::getval('id', $_KAT['KUR_TABLE'], $where);       
                $change_saved = KAT::save_history( $_KAT['KUR_TABLE'], array($field =>$act, 'id' => $id), array('id' => $id), false );
		      }
              
			return true;
		}else {
			return false;
		}
	}
/**
 * Vyvod peredanogo ob'ekta $data
 * 
 * Ispol'zuet shablony: form.inc.phtml, podkljuchaemyj pri pomowi KAT::inc_tpl().
 * Globaliziruetsja array $form i mozhet byt' ispol'zovan v shablone.
 * 
 * 
 * @uses $_CORE
 * @uses $FORM_DATA
 * @uses KAT::inc_tpl()
 * @uses $_CORE->dll_load()
 * @param array $data
 */
	function show_form( $data )
	{
		global $_CORE, $form, $_KAT;		
		$form	= $data;

		$_CORE->dll_load('class.cForm');
		global $FORM_DATA;
		$_KAT['cForm']	= new cForm( $FORM_DATA );

		/* 
		*
		*   Dlja ispol'zovanija zawitnogo koda ($_KAT['alias']['wall'] nado ustanovit' ne nulevoj)
		*
		*/
		if (!empty($_KAT[$_KAT['KUR_ALIAS']]['wall'])) {
			if (!$_CORE->dll_load('class.lProtect'))
				die ($_CORE->error_msg());
			$_KAT[$_KAT['KUR_ALIAS']]['wall_obj'] = new lProtect('/antispam/show');
		}

//		include $_CORE->SiteModDir."/kat.form.inc.php";
		//  Teper' shablon
		if (KAT::inc_tpl('form.inc.phtml', $file)) {
            if(!empty($_KAT[$_KAT['KUR_ALIAS']]['DRAFT'])){
            	echo '<div class="'.$_KAT['KUR_ALIAS'].'_draft">';
                include "$file";
                echo "</div>";
			}else{
                include "$file";
			}
			// ��������� ���� ��� �������� �������� ��� ����� ���������� � ��������������
            if (KAT::inc_tpl('form.scripts.html', $file))
                include "$file";
		}else {
			Echo "<B>No file:</B> form.inc.phtml";
		}
	}

/**
 * Proverka na korestnost' (sootvetstvie $FORM_DATA) dannyh $form
 *  
 * Vozvrawaet true, esli dannye korektny, inache false
 * Esli proverka proshla udachno, dobavljaet $form['path']  = $path
 * Ispol'zuet klass cForm, podkljuchaemyj pri pomowi instrukcii $_CORE->dll_load('class.cForm');
 * 
 * 
 * @uses $FORM_DATA
 * @uses $_KAT['cForm']->Chk_come()
 * @param array $form
 * @param string $path
 * @return bool
 */
	function check_form( &$form, $path='' )
	{
		global $_CORE, $_KAT;
		# podgruzka neobhodimyh modulej
		if (empty($_KAT['cForm'])) {
			global $FORM_DATA;
			$_CORE->dll_load('class.cForm');
			$_KAT['cForm']	= new cForm( $FORM_DATA );
		}

		/* 
		*
		*   Dlja ispol'zovanija zawitnogo koda ($_KAT['alias']['wall'] nado ustanovit' ne nulevoj)
		*
		*/
		if (!empty($_KAT[$_KAT['KUR_ALIAS']]['wall'])) {
			if (!$_CORE->dll_load('class.lProtect'))
				die ($_CORE->error_msg());
			$_KAT[$_KAT['KUR_ALIAS']]['wall_obj'] = new lProtect('/antispam/show');
		}

		#
		#	pervojetapnaja proverka
		#
		// Snachala "Stena" kod, esli ustanovleno
		if (empty($_KAT[$_KAT['KUR_ALIAS']]['wall']) || $_KAT[$_KAT['KUR_ALIAS']]['wall_obj']->check()>0) {
			// dal'she somu formu
			if ($_KAT['cForm']->Chk_come()) {								// if BAD
				$_KAT['ERROR'] = $_KAT['cForm']->Error;
				
				if (DEBUG)
					Main::log_message ( $_KAT['cForm']->Error, 'db', $_KAT['KUR_ALIAS'] );

				ob_clean();
//				echo "<script>window.alert('".addslashes($set)."');</script>";
//				exit();
				for ($i=0;$i<10;$i++) {
					global ${'doc'.$i};
					${'doc'.$i}	= '';
				}
				return false;
			}
		} else {
			$_KAT['ERROR'] = $_KAT[$_KAT['KUR_ALIAS']]['wall_obj']->err_msg;
			return false;
		}
		if (DEBUG)
			Main::log_message ( $path . ' check_form() true' , 'db', $_KAT['KUR_ALIAS'] );

		$form['path'] = $path; // STR::translit($form['title'], 4);
		return true;
	}

/**
 * Cohranenie dannyh v baze ob'ekta $form
 * 
 * Vozvrawaet true, esli sohranienie udalos', inache false. 
 * Esli vkljuchen demo-rezhim ($_CORE->IS_DEMO == true), ne sohranjaet v baze.
 * 
 *
 * @uses $FORM_FIELD4ALIAS
 * @uses $FORM_FIELDLONG
 * @uses $_KAT['INDEX']
 * @uses $_KAT['KUR_ALIAS']
 * @uses $_KAT[$_KAT['KUR_ALIAS']]['PERM']['ue']
 * @uses $_KAT['cForm']->GetSql()
 * @uses STR::translit()
 * @uses SQL::upd()
 * @uses SQL::ins()
 * @uses SQL::getval()
 * @param array $form
 * @return bool
 */
	function save( &$form )
	{

		global $_KAT, $_CORE,
			$FORM_FIELD4ALIAS,  // esli unikal'nym i pokazatel'nym polem javljaetsja ne "name"
			$FORM_FIELDLONG;    // skol'ko slov ispol'zovat' ot kazhdoj frazy dlja sostavlenija alias
		// esli demo
		if ($_CORE->IS_DEMO) return true;

//		global $doc0;
//		echo "Save: ".$doc0;
		$long = (empty($FORM_FIELDLONG)) ? 100 : $FORM_FIELDLONG;

		$insert	= '';
		foreach ($form as $key => $value) {
			if(is_array($value)){
				$value = implode(',',$value);
			}
			$tr	= array( "'" => '"' );
			$form[$key]	= stripslashes(strtr($value,$tr));
		}
//		$form['alias']	= strtr(STR::translit($form['name'], 4),$tr);
//			? preg_replace('/[^a-z0-9_]/','', STR::translit($form['name'], 5))

		$pkey	= (empty($_KAT['INDEX'][$_KAT['KUR_ALIAS']])) ? 'id' : $_KAT['INDEX'][$_KAT['KUR_ALIAS']];
		
//print_r($form);

		if ($pkey != 'alias') {
            $form['alias']	= (empty($FORM_FIELD4ALIAS)) 
				? STR::translit($form['name'], $long , $long , true)
				: STR::translit($form[$FORM_FIELD4ALIAS], $FORM_FIELDLONG,$long,true);
		}

		// for adding from GET string
		if (empty($form['alias']) && !empty($FORM_FIELD4ALIAS)) {
			$form[$FORM_FIELD4ALIAS] = $form['alias'] = $_KAT['cForm']->Arr[$FORM_FIELD4ALIAS]['default'];
		}

		// snachala apdejtim
		if (!empty($form[$pkey]) && empty($_KAT[$_KAT['KUR_ALIAS']]['PERM']['ue'])) {
            if(!isset($_KAT['conf']['alias_rand'])) {
                 
                 $get_current =  SQL::getrow( "*", $_KAT['KUR_TABLE'], "alias = '".$form['alias']."'");
                 $val_id = $get_current['id']; // SQL::getval( "id", $_KAT['KUR_TABLE'], "alias = '".$form['alias']."'");
                
                // check doubles 
  				if (isset($form['id']) && $val_id != $form['id'])
                    if($count_alias = SQL::getval( "count(*)", $_KAT['KUR_TABLE'], "name LIKE '".$form['name']."%%'")){
					   $form['alias'] .="_".($count_alias + 1);
                    }
			}
			$set	= $_KAT['cForm']->GetSql('update');
			echo $_KAT['cForm']->Error;
            
            // LOGS
			if (DEBUG)
				Main::log_message ( $_KAT['cForm']->Error . ' save() SET : ' . $set , 'db', $_KAT['KUR_ALIAS'] );

            // TRY UPDATE
			if (!is_array($set) && SQL::getval( $pkey, $_KAT['KUR_TABLE'], "$pkey = '".$form[$pkey]."'".$_KAT[$_KAT['KUR_ALIAS']]['WHERE_UPD'])){
				$res	= SQL::upd( $_KAT['KUR_TABLE'], $set, "$pkey = '".$form[$pkey]."'".$_KAT[$_KAT['KUR_ALIAS']]['WHERE_UPD'], DEBUG);
				 Main::log_message ( "Update Tuples: ".$res->Tuples.": Error: " . $res ->ErrorQuery );
			}
//			$res	= SQL::upd( $_KAT['KUR_TABLE'], $set, "$pkey = '".$form[$pkey]."'", DEBUG);
		}else {
			unset($form['id']);
		}
		#
		# esli ne proapdatilos', znachit vstavim
		if (!empty($form['alias'])&&(empty($res) || !@$res->Result || $res->ErrorQuery) && empty($_KAT[$_KAT['KUR_ALIAS']]['PERM']['ua'])) {
            
            if(!isset($_KAT['conf']['alias_rand'])) {
				if ( $count_alias = SQL::getval( "count(*)", $_KAT['KUR_TABLE'], "alias LIKE '".$form['alias']."%%'"))
					$form['alias'] .="_".($count_alias+1);
			}		  
          
			$_KAT['ERROR']	.= Main::get_lang_str('adding', 'db');
			list($var, $val) = $_KAT['cForm']->GetSql('insert');

			$res	= SQL::ins( $_KAT['KUR_TABLE'], $var, $val, DEBUG);
            $inserted = true;
            
            // LOG
			if (DEBUG)
				Main::log_message ( $_KAT['cForm']->Error . ' save() INS : ' . $var ." VALS: $val" , 'db', $_KAT['KUR_ALIAS'] );


		}else
			$_KAT['ERROR']	.= Main::get_lang_str('v_INS_ne_proshel', 'db'). ". pkey: $pkey = " . $form[$pkey] ."  form[alias] = ".$form['alias'].' name: '.$form['name'];
	
		if ($res->Result) {
			$_KAT['ERROR']	= Main::get_lang_str('data_saved', 'db');
            
            // if history
            if (!empty($_KAT[$_KAT['KUR_ALIAS']]['acc_history']))            
                $change_saved = KAT::save_history( $_KAT['KUR_TABLE'], $form, $get_current, $inserted );
            
			// for apply
            if (empty($_REQUEST['apply'])) { // �������, ���� �� apply, ��� �������, ����� ��������� ���� �� ��������, � ����� ���� �� ������ �������� ������ 
                $alias = $form['alias']; // fix it ... ����� �������� ��� ����� � ����� �������� ����� id � ������ �����
                $_KAT[$_KAT['KUR_ALIAS']]['last_form'] = $form; // save it for future use or afteredit funtions
                $_REQUEST['form'] = $form  = array(); // 
            }
			$form['id'] = SQL::getval("id", $_KAT['KUR_TABLE'], "alias = '$alias'","", DEBUG );
            $form['alias'] = $alias;
			if (DEBUG) Main::log_message( $_KAT['ERROR'] . $form['id'] );
            

			return true;
		} else {
			if (DEBUG) Main::log_message($_KAT['ERROR'] . $res->ErrorQuery);
			$_KAT['ERROR']	= $res->ErrorQuery;
			return false;
		}
	}

/**
 * Cohranenie dannyh v baze ob'ekta � ������� $dbname
 * 
 * Vozvrawaet true, esli sohranienie udalos', inache false. 
 * 
 * 
 *
 */
	function save_matrix( $arr, $dbname, $id )
	{
		global $_KAT, $_CORE;
        SQL::del( DB_TABLE_PREFIX.$dbname, " item_id = ".$id, DEBUG);
		foreach ($arr as $keyX => $val) {
            $keyX = (int)$keyX;
            if (!is_array($val)){
                $_KAT['ERROR'] = "save_matrix: input array ������ ���� ��������� ";
                return false;
            }
            foreach ($val as $keyY => $value){
                $keyY = (int)$keyY;
                if (!empty($value)){
                    $value = mysql_real_escape_string($value);
                    $var = "item_id, id_db1, id_db2, value";
                    $val = $id.",".$keyX.",".$keyY.",'".$value."'";
                    $res = SQL::ins(  DB_TABLE_PREFIX.$dbname, $var, $val, DEBUG);
                }
            }
		}
        return true;
	}
/********************************************************************/
/********************************************************************/
/********************************************************************/
/********************************************************************/

/**
 * Poisk v baze dannyh, rezul'tat stranicy $page skladyvaetsja v $_KAT['SEARCH_RES'], nichego javno ne vozvrawaet.
 * 
 * $order - parametr dlja  sortirovki, $limit - maksimal'noe kolvo. Oba parametra string, tak kak dobavljajutsja v SQL-zapros.
 * Esli v vyborke bol'she chem vsego i iskali bol'she odnoj, to nuzhna stroka listinga.
 *  Ona skladyvaetsja v $_KAT['SEARCH_PLINE']. 
 * Primer ispol'zovanija (modul' DB, komanda /getdata/ ): 
 * <code>
 *              KAT::search('', $FORM_ORDER);
 * </code>
 * 
 *
 * @uses $FORM_WHERE
 * @uses $FORM_SELECT
 * @uses $FORM_SELECT_BEFORE
 * @uses $FORM_FROM
 * @uses $FORM_DATA
 * @uses $_KAT['onpage']
 * @uses $_KAT['KUR_TABLE']
 * @uses $_KAT['SEARCH_RES']
 * @uses SQL::getval()
 * @uses SQL::sel()
 * @uses KAT::pline()
 * @param string $search
 * @param string $order
 * @param string $limit
 * @param int $page
 * @return void
 */
	function search ( $search, $order='', $limit = '', $page = -1, $return_only_where = false, $any = '')
	{
		
    	global $_KAT, $FORM_SELECT, $FORM_WHERE, $FORM_SELECT_BEFORE, $FORM_FROM, $FORM_DATA;
		$add_sel = (!empty($FORM_SELECT)) ? $FORM_SELECT : '';
		$add_sel_bef = (!empty($FORM_SELECT_BEFORE)) ? $FORM_SELECT_BEFORE : '';

		$page	= ($page < 0 && !empty($_REQUEST['p']) && empty($_KAT[$_KAT["KUR_ALIAS"]]['page_one'])) ? intval($_REQUEST['p']) : max($page,1);
		if (empty($limit))	$limit	 = $_KAT['onpage_def'][$_KAT["KUR_ALIAS"]];
		else $_KAT['onpage'] = $limit;
		$sel	= ''; 
		if (is_array($search)) foreach ($search as $f => $v) {
			$f_key = str_replace(preg_replace('/\w+/i','',$f), '', $f); 

			if ($FORM_DATA[$f_key]['type'] != 'multcheckbox'){ 
				$f_key = $f; // if not multcheckbox then get back key
			}	else {
			 $name = (empty($FORM_DATA[$f]['field_name'])) ? $this->get_key( $FORM_DATA[$f]['name'] ) : $FORM_DATA[$f]['field_name'];
            # thought all arr 
						for ($i = 0; $i < sizeof($FORM_DATA[$f]['arr']); $i++) {
							$f_key = $name.$i;
							$v = $search[$f_key];
                            if (isset($v) && !empty($v)){ 
                				$rel = (empty($FORM_DATA[$f]['search'])) ? "='$v'" : sprintf($FORM_DATA[$f]['search'], $v);
                //				if ($sel != '')
                				$sel .= (!empty($FORM_DATA[$f]['logic']))?$FORM_DATA[$f]['logic']:' AND ';
                				$tmp = " "."$f_key".$rel." "; // $_KAT['TABLES'][$_KAT['KUR_ALIAS']].
                				$sel .= $tmp; 
                			}
						}
			}
          	if (isset($FORM_DATA[$f_key])
				&& isset($v)  
				&& (!empty($v) || empty($v) && empty($FORM_DATA[$f_key]['no_empty']))
				
			){ 
			 //print_r($FORM_DATA);
				if (!is_array($v)) {
					$rel = (empty($FORM_DATA[$f_key]['search'])) ? "='$v'" : sprintf($FORM_DATA[$f_key]['search'], $v);
					$sel .= (!empty($FORM_DATA[$f_key]['logic']))?$FORM_DATA[$f_key]['logic']:' AND ';
					$tmp = " "."$f".$rel." "; // $_KAT['TABLES'][$_KAT['KUR_ALIAS']].
					$sel .= $tmp; 
				} else { // 22.11.2013 - ��� �������� OR

					$sel_arr .= (!empty($FORM_DATA[$f_key]['logic']))?$FORM_DATA[$f_key]['logic']:' AND ';
					$sel_arr .= ' ( ';
                    $tmp = array();
					foreach ($v as $vs){
						echo $FORM_DATA[$f_key]['search'];
                        $rel = (empty($FORM_DATA[$f_key]['search'])) 
                            ? (($FORM_DATA[$f_key]['type'] != 'multiselect_from_table') ? "='$vs'" : " LIKE '$vs,%' OR $f LIKE '%,$vs,%'  OR $f LIKE '%,$vs' OR $f = '$vs'") 
                            : sprintf($FORM_DATA[$f_key]['search'], $vs);
                        
						$tmp[] = " ("."$f".$rel.") "; // $_KAT['TABLES'][$_KAT['KUR_ALIAS']].
					}
					$sel_arr .= implode (' OR ', $tmp) . ' ) ';
					$sel .= $sel_arr;
				}
			}
		}
		$tail	= ($limit >= $_KAT['onpage']) 
				? SQL::of_li( $_KAT['onpage'] * ($page - 1), $limit) 
				: '';

		if(!empty($any)) {
		$tail = $any;
		}
		// dopolnitel'no dlja perekrestnyh vyborok (vpervye v EFES)
		$sel 	.=  (!empty($FORM_WHERE)) ? $FORM_WHERE : '';
		$add 	= 1;

		if (!empty($sel)){
			$add = (preg_match("/^(\s*)or(\s+).*/i", $sel)) ?0:1;
		}
		if ($return_only_where) {
		    return "($add " . $sel . ")";
		}
		$res	= SQL::sel($add_sel_bef.' * '.$add_sel,  $_KAT['KUR_TABLE'].$FORM_FROM, "($add ".$sel.")", $order.$tail, DEBUG);
		if ($res->NumRows > 0) {
			for ($i = 0; $i < $res->NumRows; $i++ ) {
				$res->FetchArray($i);
				$_KAT['SEARCH_RES'][]	= $res->FetchArray;
			}
			#
			# esli v vyborke bol'she chem vsego i iskali bol'she odnoj, to nuzhna stroka.
			$_KAT['SEARCH_PLINE']	 = '';
			if ($limit >= $_KAT['onpage']) {
				$All	= SQL::getval(' DISTINCT count(*) ', $_KAT['KUR_TABLE'].$FORM_FROM, "($add ".$sel.")"); //$add_sel_bef. //, $order.$tail,0);
				
				$_KAT['all'] = $All;
				if ($All > $res->NumRows)
					$_KAT['SEARCH_PLINE']	.= KAT::pline($All, $page, $_KAT['onpage']);
			}
		}
	}

	
/**
 * Poluchit' kolichestvo stranic radela (vpervye Titanium)
 *
 * $_KAT['onpage']
 * @uses $FORM_WHERE
 * @uses $FORM_SELECT
 * @uses $FORM_SELECT_BEFORE
 * @uses $FORM_FROM
 * @uses $FORM_DATA
 * @uses SQL::getval()
 * @return int
 */
	function get_pages()
	{
		global $_KAT, $FORM_SELECT, $FORM_WHERE, $FORM_SELECT_BEFORE, $FORM_FROM, $FORM_DATA;
		
		$add_sel_bef = (!empty($FORM_SELECT_BEFORE)) ? $FORM_SELECT_BEFORE : '';

		$sel	= '';
		if (is_array($search)) foreach ($search as $f => $v) {
			if (isset($v)){
				$rel = (empty($FORM_DATA[$f]['search'])) ? "='$v'" : sprintf($FORM_DATA[$f]['search'], $v);
				$sel .= " AND $f".$rel." ";
			}
		}

		// dopolnitel'no dlja perekrestnyh vyborok (vpervye v EFES)
		$add_where	= (!empty($FORM_WHERE)) ? $FORM_WHERE : '';
		$All = SQL::getval($add_sel_bef.' count(*) ', $_KAT['KUR_TABLE'].$FORM_FROM, '1 '.$sel." ".$add_where,'',0);

		return ceil($All / $_KAT['onpage']);
	}

/**
 * Beret jekzempljar zapisi v sootvetstvii s peredannymi parametrami, udovletvorjajuwij parametram funkcii.
 * 
 * Rezul'tat skladyvaet v $_KAT['SEARCH_LAST'].
 * 
 * 
 * @uses $_KAT['LAST']['form']
 * @uses KAT::search()
 * @param string $order
 * @param int $limit
 * @param int $offset
 */
	function last( $order ='', $limit = 1, $offset = 1)
	{
		global $_KAT;
		if (empty($_KAT['SEARCH_LAST'])) {
			$_KAT['SEARCH_RES']	= array();
			KAT::search($_KAT['LAST']['form'], $order, $limit, $offset); // vsegda 1 stranicu pri posldnih, inache on hvataet iz QS i esli my na N stranice kataloga on N stranicu novosti budet iskat'.
			if (sizeof($_KAT['SEARCH_RES']) > 0) {
				$_KAT['SEARCH_LAST']	= $_KAT['SEARCH_RES'][0];
			}
		}
	}

/**
 * Obnuljaet rezul'tat predyduwego poiska, osuwestvljaemogo funkcijami KAT::search() i KAT::last().
 * 
 * Po suti obnuljaet $_KAT['SEARCH_LAST']  i $_KAT['SEARCH_RES']
 * 
 * 
 * @uses $_KAT['SEARCH_LAST']
 * @uses $_KAT['SEARCH_RES']
 */
    function last_reset()
	{
		global $_KAT;
		$_KAT['SEARCH_LAST'] = '';
		$_KAT['SEARCH_RES'] = '';
	}
	
	

/**
 * Formiruet stroku listinga
 * 
 * $all - skoka vsego zapisej vyvoditsja. $p - otkrytaja stranica. $onpage - skol'ko zapisej na stranice.
 * 
 * Ispol'zuemye shablony:
 * <code>pline.head.html
 * pline.dot.html
 * pline.delimeter.html - shablon razdelitelja
 * pline.current.html
 * pline.link.html
 * pline.foot.html
 * </code>
 * 
 * Esli shablony otsutstvujut, to ispol'zuetsja standartnyj vyvod.
 *
 * @uses KAT::inc_tpl()
 * @param int $all
 * @param int $p
 * @param int $onpage
 * @return string
 */
	function pline( $all, $p, $onpage)
	{
			global $th, $mount, $year, $_CORE;
			if (intval($onpage) == 0) return '';
			$QS		= $_SERVER['QUERY_STRING'];
			$string = preg_replace("/^p=$p/","",$QS);
			$string = str_replace(array("&p=$p",$_SERVER['REDIRECT_QUERY_STRING']),"",$string);
        	// dlja validatora inache /?&p - ne ponimaet
			if ($string != '') $string .= '&';
			$uri	 = $_SERVER['REQUEST_URI'];
			ob_start();

			// shapka  (v otdel'nuju funkciju nel'zja, ibo znachenija peremennyh terjajutsja)
			//  Teper' shablon
			if (KAT::inc_tpl('pline.head.html', $file))
				include "$file";

			$tmp	.= ($_CORE->LANG == 'en') ? 'Pages :' : Main::get_lang_str('pages', 'db');
			$pages = ceil($all / $onpage);

			if ($pages <= 1){
				ob_end_clean();
				return '';
			}
			$inline = min(10, $pages); // skol'ko ssylok na raznye stranicy v linii
			if ($p < $inline) {
					$from = 1; $to = $from + $inline -1;
			}elseif( $p > $pages - $inline) {
					$from = max($pages - $inline, 1); $to = $pages;
			}else {
					$from   = max(1, $p - floor($inline / 2)); $to  = min($pages, $p + floor($inline / 2))+1;
			}

			if ($from > 1) {
				$dot_mean = $from-1;
				// razdelitel'
				//  Teper' shablon
				if (KAT::inc_tpl('pline.dot.html', $file))
					include "$file";
				else 
					echo "<a href='$uri?".$string."p=".$dot_mean."'>...</a>";


				// razdelitel'
				//  Teper' shablon
				if (KAT::inc_tpl('pline.delimeter.html', $file))
					include "$file";
				else 
					echo "&nbsp;|&nbsp;";

			}

			for( $i = $from; $i <= $to; $i++ ) {
					if ($i == $p) {

						// tekuwaja  
						if (KAT::inc_tpl('pline.current.html', $file))
							include "$file";
						else 
							echo $i;

					}else{

						// ssylka
						//  Teper' shablon
						if (KAT::inc_tpl('pline.link.html', $file))
							include "$file";
						else 
							echo "<a href='$uri?".$string."p=$i'>".($i)."</a>";

					}
					if ($i < $to || $pages-1 > $to) {
				// razdelitel'
				//  Teper' shablon
						if (KAT::inc_tpl('pline.delimeter.html', $file))
							include "$file";
						else 
							echo "&nbsp;|&nbsp;";
					}
			}
			if ($pages-1 > $to) {
				$dot_mean = $i;
				//  Teper' shablon
				if (KAT::inc_tpl('pline.dot.html', $file))
					include "$file";
				else 
					echo "<a href='$uri?".$string."p=".$dot_mean."'>...</a>";

			}

			// finish
				//  Teper' shablon
				if (KAT::inc_tpl('pline.foot.html', $file))
					include "$file";

			$tmp = ob_get_contents();
			ob_end_clean();
			return $tmp;
	}



	function mass_update($fields, $where) {
		if (empty($where)) return false;

		global $_KAT, $_CORE, $FORM_DATA, $FORM_IMPORT, $FORM_IMPORT_TYPE, $FORM_FIELD4ALIAS, $FORM_FIELDLONG;

        $upds = array();
		if (is_array($fields) && count($fields)) {
		    foreach ($fields as $f => $v) {
		    	$v = addslashes($v);
                if ( isset($FORM_DATA[$f]) && !empty($v) ){
                    $upds[] = " $f = '$v' ";
                }
            }
        }
        if (count($upds)) {
		    SQL::upd($_KAT['KUR_TABLE'], implode(',', $upds), $where, 0);
		    return true;
        }
        return false;
	}


/**
 * Import dannyh iz fajla. Naprimer iz tablicy Excel.
 * 
 * Fajl dlja importa: $_FILES['import_data']['tmp_name'], pravda gde globaliziruetsja $_FILES neizvestna.
 * Dannye v fajle dolzhny byt' razdeleny standartnymi razdeliteljami ('',',',';','\t') i sootvetstvovat' formatu $FORM_IMPORT.
 * Tip importa: insert/update prioritetnee to, chto peredano v funkciju ($type) , inache dannye iz *.data.inc.php ($FORM_IMPORT_TYPE).
 *
 * @uses $FORM_DATA
 * @uses $FORM_IMPORT_TYPE
 * @uses $FORM_IMPORT
 * @uses $FORM_FIELDLONG
 * @uses $FORM_FIELD4ALIAS
 * @uses $_FILES['import_data']['tmp_name']
 * @uses $_FILES['import_data']['error']
 * @uses $_KAT['ERROR']
 * @uses $_KAT['INDEX']
 * @uses $_KAT['KUR_ALIAS']
 * @uses SQL::ins()
 * @uses SQL::upd()
 * @param string $type
 * @return Array
 */
	function import($type='')
	{
		global $_KAT,$_CORE, $FORM_DATA, $FORM_IMPORT, $FORM_IMPORT_TYPE, $FORM_FIELD4ALIAS, $FORM_FIELDLONG;
		set_time_limit(1200);
		if (!empty($_KAT[$_KAT['KUR_ALIAS']]['IMPORT'])) {
			$FORM_IMPORT = $_KAT[$_KAT['KUR_ALIAS']]['IMPORT'];
		}

		// tip importa: insert/update prioritetnee to, chto peredano v funkciju, inache dannye iz *.data.inc.php
		if (empty($type) && !empty($FORM_IMPORT_TYPE)) {
			$type = @$FORM_IMPORT_TYPE;
		}
		
		if (empty($FORM_IMPORT)) {
			$_KAT['ERROR'] = KAT_IMPORT_WRONG_SETTINGS;
			return false;
		}

		// prishel li fajl
		if (!is_file($_FILES['import_data']['tmp_name']) || !empty($_FILES['import_data']['error'])) {
			$_KAT['ERROR'] = KAT_FILE_NOT_FOUND;
			return false;
		}

		// kuda ego poka polozhit' dlja arhiva
//		$new_file	= $_CORE->SiteModDir.'/f_'.$_KAT['KUR_ALIAS'].'/im_'.date('d.m.Y.H.i.s').".csv";
		$d = KAT::get_data_path('/f_'.$_KAT['KUR_ALIAS'], $dir);

		if (empty($dir)) {
			$_KAT['ERROR'] = KAT_IMPORT_WRONG_DIR . "($dir)";
			return false;
		}

		$new_file	= $dir.'/im_'.date('d.m.Y.H.i.s').".csv";

		copy($_FILES['import_data']['tmp_name'], $new_file);

		// opredelenie razdelitelja
		$darr	= array('',',',';','\t');
		if (!empty($darr[$_POST['import_delimeter']])) {
			$delimeter = $darr[$_POST['import_delimeter']];
		}else {
			$_KAT['ERROR'] = KAT_IMPORT_WRONG_DELIMETER;
			return false;
		}
		// baza ...

//		global $SQL_DBCONF;
//		$db	= new Connection('', $SQL_DBCONF);
//
//		// ************** Zakachka dannyh
//		$SQL	= "	LOAD DATA LOCAL INFILE '".$new_file."' 
//					INTO TABLE ".$_KAT['KUR_TABLE']." 
//					FIELDS TERMINATED BY '".$delimeter."' ESCAPED BY '' LINES TERMINATED BY '\\n'
//					( ".$_KAT['IMPORT'][$_KAT['KUR_ALIAS']]." )";
//		$load	= new Query( $db->Connect, $SQL);
//		$good	= $load->Tuples;														// dobavleno zapisej
//		if (!$load->Result) {
//			$_KAT['ERROR']	= $load->ErrorQuery. "(".$load->ErrorQueryNum.")\n";
//			return false;
//		}
//		2.

		$lines	= file($new_file);
		$good	= $bad	= 0;

		// teper' po kazhdoj stroke
		for ($i = 0; $i < sizeof($lines); $i++ ) if (!empty($lines[$i])) {
			if ($i == 0 && $_KAT[$_KAT['KUR_ALIAS']]['IMPORT_WITH_TITLES']) continue;
				
			$data	= explode($delimeter, trim(str_replace(" \ n ", "\n", $lines[$i])));

			// esli ploho, to razdelim standartnym ;
			if (sizeof($data) == 1) {
				Main::log_message ( "Import '$type', size 0 after explode by $delimeter, trying ';'" );
				$data = explode(';', trim(str_replace(" \ n ", "\n", $lines[$i])));
			}

			// esli tam byli opciony (dannye kotorye ne po real'nomu znacheniju a po indeksu v massive, t.e. tipa select)
			// po kazhdomu polju zagruzhaemyh dannyh
			if (is_array($data)) foreach ($data as $n => $value) {
                switch($FORM_DATA[$FORM_IMPORT[$n]]['type']){
                    case 'select':
                        // najdem indeks
    					$new_val	= array_search(trim($value,"� \t"), $FORM_DATA[$FORM_IMPORT[$n]]['arr']);
    					// zamenim na indeks
    					$data[$n]	= ($new_val !== false) ? $new_val : $data[$n];
                    break;
                    case 'select_rel':
                        // najdem nuzhnyj massiv
    					$rel_field = rtrim(str_replace('form[', '', $FORM_DATA[$FORM_IMPORT[$n]]['rel_from']), ']'); 
    					$rel_field_val = $data[array_search( $rel_field, $FORM_IMPORT)];
    					// najdem indeks
    					$new_val	= @array_search(trim($value,"� \t"), $FORM_DATA[$FORM_IMPORT[$n]]['arr'][$rel_field_val]);
    					// zamenim na indeks
    					$data[$n]	= ($new_val != '') ? $new_val : $data[$n];
                    break;
                    case 'select_from_table':
    					$new_val	= SQL::getval($FORM_DATA[$FORM_IMPORT[$n]]['id_ex_table'],$FORM_DATA[$FORM_IMPORT[$n]]['ex_table'],$FORM_DATA[$FORM_IMPORT[$n]]['ex_table_field']." LIKE '".trim($value,"� \t")."'");
    					// zamenim na indeks
    					if($new_val !== false) $data[$n] = $new_val;
                    break;
                    default:
                    break;
                }

			}
			// dopishem massiv dannyh ili srezhem lishnee, 
			// chto by byli odinakovogo razmera. rovno stol'ko, skol'ko v massive FORM_IMPORT
			if (sizeof($data) < sizeof($FORM_IMPORT)) {
				$data	= array_pad($data, sizeof($FORM_IMPORT), '');
			}
			if (sizeof($data) > sizeof($FORM_IMPORT)) {
				$data = array_slice($data, 0, sizeof($FORM_IMPORT));				
			}

			// dlja sozdanija aliasa, opredelim dlinnu vyrezaemyh simvolov, i kolichestvo slov.
			$long = (empty($FORM_FIELDLONG)) ? 60 : $FORM_FIELDLONG;

			if (empty($type) || $type == 'insert' || $type == 'try') {
				// sozdali dannye dlja dobavlenieja INSERT
				$val	= "'".implode("','", $data)."'";
				$var	= "`".implode("`,`", $FORM_IMPORT)."`";
			}

			if ($type == 'update' || $type == 'try') {
				// sozdali dannye dlja obnovlenija UPDATE
				$set = '';
				for ($j=sizeof($FORM_IMPORT)-1; $j >= 0; $j --) {
					$set .= "`".$FORM_IMPORT[$j]."` = '".$data[$j]."',";
				}
				$set = rtrim($set, ",");
			}
			
			// esli avtomatom aliasy ne stavjatsja, to uznaem po kakomu polju i sdelaem alias.
			if (empty($_KAT['INDEX']['autoinc_'.$_KAT['KUR_ALIAS']])) {
				$pkey	= (empty($_KAT['INDEX'][$_KAT['KUR_ALIAS']])) ? $FORM_IMPORT[0] : $_KAT['INDEX'][$_KAT['KUR_ALIAS']];
//				if ($pkey != 'alias') {
					$pkey_val	= (empty($FORM_FIELD4ALIAS)) ? array_search($pkey, $FORM_IMPORT) : array_search($FORM_FIELD4ALIAS, $FORM_IMPORT);
					$data_add	= STR::translit($data[$pkey_val],$long, $long, true);
           if(empty($data_add)) continue;
//				}else{
//					$pkey_val	= (empty($FORM_FIELD4ALIAS)) ? array_search($pkey, $FORM_IMPORT) : array_search($FORM_FIELD4ALIAS, $FORM_IMPORT);
//					$data_add	= STR::translit($data[$pkey_val],$long, $long, true);
//				}
				$var .= ", `alias`";
				$val .= ", '".$data_add."'";
				$where = " `$pkey` = '".$data_add."'"; // $data_add
			}elseif(!empty($_KAT[$_KAT['KUR_ALIAS']]['ALIAS_CONCAT']) && is_array($_KAT[$_KAT['KUR_ALIAS']]['ALIAS_CONCAT'])){
				$data_add = '';
				foreach($_KAT[$_KAT['KUR_ALIAS']]['ALIAS_CONCAT'] as $alias_field){
					$pkey_val	= array_search($alias_field, $FORM_IMPORT);
					$data_add .= STR::translit($data[$pkey_val],$long, $long, true);
				}
				$var .= ", `alias`, `name`";
				$val .= ", '".$data_add."', '".$data_add."'";
				$where = " `alias` = '".$data_add."'"; 
			}
			
			if (empty($type) || $type == 'insert') {
				$res	= SQL::ins($_KAT['KUR_TABLE'], $var, $val, DEBUG);
			}elseif ($type == 'update') {
				$res	= SQL::upd($_KAT['KUR_TABLE'], $set, $where, DEBUG);
			}elseif ($type = 'try') {
				// poprobuem dobavit'
				$importResultType = 'insert';
				$res	= SQL::ins($_KAT['KUR_TABLE'], $var, $val, DEBUG);
				// ili vstavit'
				if ($res->ErrorQuery){
					$res	= SQL::upd($_KAT['KUR_TABLE'], $set, $where, DEBUG);
					$importResultType = 'update';
				}
				
				// pluging after each
				if (KAT::inc_tpl('import_after.item.php', $file))
					include "$file";
			}

			if ($res->Tuples > 0) {
				$good+=$res->Tuples;
			}else {
				$_KAT['ERROR'] .= $res->ErrorQuery;
				$bad++;
			}
		} // for
		
		// pluging after full import
		if (KAT::inc_tpl('import.all.php', $file))
			include "$file";
		
		unlink($new_file);
		return array($good, $bad);
	}

/**
 * Jeksport dannyh v fajl. 
 *
 * Prichem esli parametr $die == true, to proishodit sohranie v fajl, inache prosto vyvod na jekran.
 * 
 * @uses $FORM_IMPORT
 * @uses $_KAT['KUR_TABLE']
 * @uses SQL::sel()
 * @param bool $die
 */
	function export($die = true)
	{
		global $_KAT, $FORM_IMPORT, $FORM_DATA, $FORM_ORDER;

		if (!empty($_KAT[$_KAT['KUR_ALIAS']]['IMPORT'])) {
			$FORM_IMPORT = $_KAT[$_KAT['KUR_ALIAS']]['IMPORT'];
		}
 		if (empty($FORM_IMPORT) && !empty($_KAT[$_KAT['KUR_ALIAS']]['EXPORT'])) {
			$FORM_IMPORT = $_KAT[$_KAT['KUR_ALIAS']]['EXPORT'];
		} 

		$var	= implode(", ", $FORM_IMPORT);
		$res	= SQL::sel( $var, $_KAT['KUR_TABLE'], '', $FORM_ORDER, DEBUG );
		$cols_showed = false;
    
		if (!empty($_KAT[$_KAT['KUR_ALIAS']]['EXPORT_AS'])) {
			$FORM_IMPORT = $_KAT[$_KAT['KUR_ALIAS']]['EXPORT_AS'];
		}

		if ($res->NumRows > 0){
			for ($i=0;$i < $res->NumRows; $i++ ) {
				if (!$cols_showed && $_KAT[$_KAT['KUR_ALIAS']]['IMPORT_WITH_TITLES']) {
					foreach ($FORM_IMPORT as $k => $title) {
						$str .= $FORM_DATA[$title]['title'].';';
					}	
					$str .= "\n";
					$cols_showed = true;
				}
				$res->FetchRow($i);
				$data = $res->FetchRow;

				foreach ($data as $n => $value) {
					if ($FORM_DATA[$FORM_IMPORT[$n]]['type'] == 'select') {
						// najdem indeks
//						$new_val	= array_search(trim($value,"� \t\n\r"), $FORM_DATA[$FORM_IMPORT[$n]]['arr']);
						// zamenim na ��������
						$data[$n]	= $FORM_DATA[$FORM_IMPORT[$n]]['arr'][$value]; //($new_val != '') ? $new_val : $data[$n];
					}
				}
				
				if (!empty($data['1']) && $data['1'] == 'admin') 
						continue;
					$str .= str_replace("\r", '', str_replace("\n", ' \ n ', implode(";",$data)))."\n";
				}
		}

		if ($die) {
			ob_end_clean();
			header("Content-Type: application/ms-excel");
			header('Content-Disposition: attachment; filename="example.csv"');
			die($str);
		} else {
			echo $str;
		}
	}

	
	function export2csv($delim = ',', $filename = '', $die = true)
	{
		global $_KAT, $FORM_DATA;
		if ( empty($_KAT[$_KAT['KUR_ALIAS']]['EXPORT2CSV']) ) {
			die('');
		}
		
		$filename = $filename ? $filename : $_KAT['KUR_ALIAS'] . ".csv";
		
		$res	= SQL::sel( '*', $_KAT['KUR_TABLE'], '', '', DEBUG );

		// send response headers to the browser
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename='.$filename);
		$fp = fopen('php://output', 'w');
       
       
		
		$cols_showed = false;
		if ($res->NumRows > 0){
			for ($i=0;$i < $res->NumRows; $i++ ) {
				$res->FetchArray($i);
				$fields = array();
				$cols   = array();
				foreach ($res->FetchArray as $k=>$v) {
					if ( isset($FORM_DATA[$k]['export2csv']) ) {
						if ($FORM_DATA[$k]['export2csv'] == 'url') {
							$v = "http://{$_SERVER['SERVER_NAME']}/db/{$_KAT['KUR_ALIAS']}/{$v}";
						} 
						elseif ( $FORM_DATA[$k]['type'] == 'photo' && !empty($v) ) {
							$v = "http://{$_SERVER['SERVER_NAME']}{$FORM_DATA[$k]['path']}/{$v}";
						}
						$cols  [] = $FORM_DATA[$k]['export2csv'];
						$fields[] = iconv('windows-1251', 'UTF-8', $v);
					}
				}
				if (is_array($_KAT[$_KAT['KUR_ALIAS']]['EXPORT2CSV']['MANDATORY'])) {
					foreach ( $_KAT[$_KAT['KUR_ALIAS']]['EXPORT2CSV']['MANDATORY'] as $k=>$v) {
						$fields[] = iconv('windows-1251', 'UTF-8', $v);
						$cols  [] = $k;
					}
				}
				
				if ( !$cols_showed ) {
					fputcsv($fp, $cols, $delim);
					$cols_showed = true;
				}

                fputcsv($fp, $fields, $delim);

			}
		}

        fclose($fp);
		die();
	}
	
/**
 * Ochistka tekuwej tablicy ($_KAT['KUR_TABLE']).
 *
 * Vozvrawaet true, esli vse poluchilos'. Inache false i soobwenie ob oshibke ($_KAT['ERROR']).
 * @uses $SQL_DBCONF
 * @uses $_KAT['ERROR']  
 * @uses $_KAT['KUR_TABLE']
 * @return bool
 */
	function truncate()
	{
		global $SQL_DBCONF, $_KAT, $FORM_DATA;
		$db	= new Connection('', $SQL_DBCONF);
		// ************** Poluchim vse dannye 
		$sel = SQL::getall('*',$_KAT['KUR_TABLE']);
		// ************** Udalenie dannyh iz table
		$SQL	= "TRUNCATE TABLE ".$_KAT['KUR_TABLE'];
		$trun	= new Query( $db->Connect, $SQL);
		$good	= $trun->Tuples;														// dobavleno zapisej
		if (!$trun->Result) {
			$_KAT['ERROR']	= $trun->ErrorQuery. "(".$trun->ErrorQueryNum.")\n";
			return false;
		}else {
			$d = KAT::get_data_path('/f_'.$_KAT['KUR_ALIAS'], $dir);
			foreach($sel as $item){				
				if(isset($item['doc']) && !empty($item['doc'])){
					$dir = (!empty($FORM_DATA['doc']['abspath']))?$FORM_DATA['doc']['abspath']:$dir;
					unlink($dir.'/'.$item['doc']);
				}
				for($i=1; $i<=10; $i++){
					if(isset($item['doc'.$i]) && !empty($item['doc'.$i])){
					$dir = (!empty($FORM_DATA['doc'.$i]['abspath']))?$FORM_DATA['doc'.$i]['abspath']:$dir;
					unlink($dir.'/'.$item['doc'.$i]);
					}
				}
			}
/*			$d = KAT::get_data_path('/f_'.$_KAT['KUR_ALIAS'], $dir);
			$files = glob($dir."/*");
			$c = count($files);
			if (count($files) > 0) {
				foreach ($files as $file) {      
					if (file_exists($file)) {
						unlink($file);
					}
				}
			} */
			return true;
		}
	}

/**
 * Proverka na suwestvovanie fajla-shablona s imenem $tpl
 *
 * Pri proverke suwestvuet prioritet fajlov:
 * <code>
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS']."/$tpl",
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS'].'.'.$tpl,
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$tpl,
 *$_CORE->CORE_PATHTPLS.'/'.$_KAT['MODULE_NAME'].'/_'.$tpl,
 * </code>
 * U pervogo iz suwestvujuwih fajlov-shablonov, ego imja i put' do nego sohranjaetsja v $file. A v rezul'tate funkcii vozvrawaetsja true, inache false.
 * Primer ispol'zovanija (modul' db, komanda /list/ ):
 * <code>
 *  if (KAT::inc_tpl('db.list.foot.html', $file))
 *            include "$file";
 * </code>
 * 
 * @uses FILE::chk_files()
 * @uses $_CORE->PATHTPLS
 * @uses $_CORE->CURR_TPL
 * @uses $_CORE->CORE_PATHTPLS
 * $_KAT['MODULE_NAME']
 * $_KAT['KUR_ALIAS']
 * @param string $tpl
 * @param string $file
 * @return string
 */
	function inc_tpl($tpl, &$file)
	{
		global $_CORE, $_KAT;
		/**
		 *  Dlja rss potoka modulja db
		 */
		if(isset($_GET['rss']) && $_KAT['rss'][$_KAT['KUR_ALIAS']])
		{
			$_CORE->EMPTY_VERSION = true;
			$files = array(
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS']."/rss_$tpl", // na sajte v tekuwem shablone, direktorija modulja, direktorija aliasa
				$_CORE->PATHTPLS.$_CORE->CURR_TPL.DIRECTORY_SEPARATOR.$_KAT['MODULE_NAME'].DIRECTORY_SEPARATOR.'rss'.DIRECTORY_SEPARATOR.$tpl,
				$_CORE->CORE_PATHTPLS.$_KAT['MODULE_NAME'].DIRECTORY_SEPARATOR.'rss'.DIRECTORY_SEPARATOR.'_'.$tpl,
			);
		}elseif(isset($_GET['rssturbo']) && $_KAT['rssturbo'][$_KAT['KUR_ALIAS']])
		{
			$_CORE->EMPTY_VERSION = true;
			$files = array(
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS']."/rssturbo_$tpl", // na sajte v tekuwem shablone, direktorija modulja, direktorija aliasa
				$_CORE->PATHTPLS.$_CORE->CURR_TPL.DIRECTORY_SEPARATOR.$_KAT['MODULE_NAME'].DIRECTORY_SEPARATOR.'rssrssturbo'.DIRECTORY_SEPARATOR.$tpl,
				$_CORE->CORE_PATHTPLS.$_KAT['MODULE_NAME'].DIRECTORY_SEPARATOR.'rssturbo'.DIRECTORY_SEPARATOR.'_'.$tpl,
			);
		}
		else{
			$files	=  array( 
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS']."/$tpl", // na sajte v tekuwem shablone, direktorija modulja, direktorija aliasa
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS'].'.'.$tpl, // na sajte v tekuwem shablone, direktorija modulja, fajly aliasa
				$_CORE->CORE_PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS'].'/_'.$tpl, // v jadre, direktorija modulja shablona, direktorija aliasa s "_"
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$tpl, // 23.08.2011 (.slk strochkoj vyshe) na sajte v tekuwem shablone, direktorija modulja, prosto fajl
				$_CORE->CORE_PATHTPLS.'/'.$_KAT['MODULE_NAME'].'/_'.$tpl, // v jadre, direktorija modulja, alias s "_" i nazvanie
				// shablon u roditelja
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['SRC'][$_KAT['KUR_ALIAS']].'/'.$tpl, // roditel'skij shablon na sajte
				$_CORE->CORE_PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['SRC'][$_KAT['KUR_ALIAS']].'/'.$tpl, // roditel'skij shablon v jadre
                $_CORE->CORE_PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['MODULE_NAME'].'/'.$tpl, // roditel'skij shablon v jadre
				
				// defoltovyj shablon
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['MODULE_NAME'].'/'.$tpl, // na sajte, shablon po umolchaniju vsego modulja
				$_CORE->CORE_PATHTPLS.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['MODULE_NAME'].'/_'.$tpl, // v jadre, shablon po umolchaniju vsego modulja
			);
		}
		$tmp = FILE::chk_files($files, $file);
        //print_r($files);
		return $tmp;
	}

/**
 * Proverka na suwestvovanie fajla-dannyh s imenem $tpl
 *
 * Pri proverke suwestvuet prioritet fajlov:
 * <code>
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS']."/$tpl",
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS'].'.'.$tpl,
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$tpl,
 *$_CORE->CORE_PATHTPLS.'/'.$_KAT['MODULE_NAME'].'/_'.$tpl,
 * </code>
 * U pervogo iz suwestvujuwih fajlov-shablonov, ego imja i put' do nego sohranjaetsja v $file. A v rezul'tate funkcii vozvrawaetsja true, inache false.
 * Primer ispol'zovanija (modul' db, komanda /list/ ):
 * <code>
 *  if (KAT::inc_tpl('db.list.foot.html', $file))
 *            include "$file";
 * </code>
 * 
 * @uses FILE::chk_files()
 * @uses $_CORE->PATHTPLS
 * @uses $_CORE->CURR_TPL
 * @uses $_CORE->CORE_PATHTPLS
 * $_KAT['MODULE_NAME']
 * $_KAT['KUR_ALIAS']
 * @param string $tpl
 * @param string $file
 * @return string
 */
	function inc_data($tpl, &$file)
	{
		global $_CORE, $_KAT;
		$files	=  array( 
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS']."/$tpl", // na sajte v tekuwem shablone, direktorija modulja, direktorija aliasa
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS'].'.'.$tpl, // na sajte v tekuwem shablone, direktorija modulja, fajly aliasa
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$tpl, // na sajte v tekuwem shablone, direktorija modulja, prosto fajl
			$_CORE->CORE_PATHTPLS.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS'].'/_'.$tpl, // v jadre, direktorija modulja, direktorija aliasa s "_"
			$_CORE->CORE_PATHTPLS.'/'.$_KAT['MODULE_NAME'].'/_'.$tpl, // v jadre, direktorija modulja, alias s "_" i nazvanie
			// shablon u roditelja
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['SRC'][$_KAT['KUR_ALIAS']].'/'.$tpl, // roditel'skij shablon na sajte
			$_CORE->CORE_PATHTPLS.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['SRC'][$_KAT['KUR_ALIAS']].'/'.$tpl, // roditel'skij shablon v jadre
			
			// defoltovyj shablon
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['MODULE_NAME'].'/'.$tpl, // na sajte, shablon po umolchaniju vsego modulja
			$_CORE->CORE_PATHTPLS.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['MODULE_NAME'].'/_'.$tpl, // v jadre, shablon po umolchaniju vsego modulja
			);
		$tmp = FILE::chk_files($files, $file);
		return $tmp;
	}

/**
 * Proverka na suwestvovanie fajla-shablona $tpl.
 * 
 * Pri proverke suwestvuet prioritet shablonov:
 * <code>
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS']."/$tpl",
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS'].'.'.$tpl,
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$tpl,
 * </code>
 * U pervogo iz suwestvujuwih fajlov, ego imja i put' sohranjaetsja v $file, i pri jetom v puti $_CORE->PATHTPLS menjaetsja na $_CORE->TEMPLATES.
 * A v rezul'tate funkcii vozvrawaetsja $file.
 * 
 * @uses FILE::chk_files()
 * @uses $_CORE->PATHTPLS
 * @uses $_CORE->CURR_TPL
 * @uses $_CORE->TEMPLATES
 * $_KAT['MODULE_NAME']
 * $_KAT['KUR_ALIAS']
 * @param string $tpl
 * @param string $file
 * @return string
 */
	function inc_link($tpl, &$file)
	{
		global $_CORE, $_KAT;
		$files	=  array( 
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS']."/$tpl",
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS'].'.'.$tpl,
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_KAT['MODULE_NAME'].'/'.$tpl,
			$_CORE->PATHTPLS.'/admin/'.$_KAT['MODULE_NAME'].'/'.$tpl,
		);
		$tmp = $file = str_replace($_CORE->PATHTPLS, $_CORE->TEMPLATES, FILE::chk_files($files, $file));
		return $tmp;
	}

	/**
	 * SQL commant for CREATE TABLE
	 *
	 * @param string $tab
	 * @param FORM_DATA(array) $data
	 * @return string SQL COMMAND "CREATE"
	 */
	function create_sql_table( $tab, &$data){

		if (empty($tab))  $tab = 'table_default';
		
		$body_sql = '';
		
		KAT::parse_form_data( $body_sql, $data );

//		if (sizeof($data)) 
//			$body_sql = substr($body_sql,0,-2);
			
		$OUT_BUF = "
	CREATE TABLE $tab (
$body_sql 
		PRIMARY KEY (`id`)
	);";
		return $OUT_BUF;
	}
	
	/**
	 * parsing FORM_DATA (array)
	 *
	 * @param string_empty $body_sql
	 * @param FORM_DATA(array) $data
	 * @return void
	 */
	function parse_form_data( &$body_sql, &$data ){
		foreach ($data as $value){ 
			
		$key = (!empty($value['key'])) ? $value['key'] : '';

		switch ($value['type']) 
		{
			// hidden - diff
			case 'hidden': 
				if (empty($value['sub_type'])){
					switch ($value['field_name']){
						case 'ts':
							$body_sql .= "\t`".$value['field_name']."`\t timestamp ON UPDATE CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,\n\t";
							break;
						case 'id':
							$body_sql .= "\t`".$value['field_name']."`\t bigint(20) NOT NULL auto_increment,\n\t";
							break;
						case 'alias':
							$body_sql .= "\t`".$value['field_name']."`\t varchar(255) NOT NULL default '',\n\tUNIQUE KEY `".$value['field_name']."` (`".$value['field_name']."`) ,\n";
							break;
						default:
							$body_sql .= "\t`".$value['field_name']."`\t varchar (".((!empty($value['maxlen']))?intval($value['maxlen']):255)."), $key \n";
							break;
					}
				} else {
					switch ($value['sub_type']){
						case 'bigint':
							$body_sql .= "\t`".$value['field_name']."`\t bigint(20) NOT NULL , $key\n";
							break;
						case 'int':
							$body_sql .= "\t`".$value['field_name']."`\t int(".((!empty($value['maxlen']))?intval($value['maxlen']):10).") NOT NULL , $key\n";
							break;
						case 'varchar':
						default:
							$body_sql .= "\t`".$value['field_name']."`\t varchar (".((!empty($value['maxlen']))?intval($value['maxlen']):255)."), $key\n";
							break;	
					}					
				}
				break;
				
			// textdate - date
			case 'textdate':
				$body_sql .= "\t`".$value['field_name']."`\t date default NULL, $key\n";
				break;
				
			// select, photo, textbox - varchar
			case 'select': case 'select_rel': case 'photo': case 'file': case 'textbox':case 'checkbox': case 'select_from_table':  case'datetime': case 'radiobtn':
				if (empty($value['sub_type'])){
					$body_sql .= "\t`".$value['field_name']."`\t varchar (".((!empty($value['maxlen']))?intval($value['maxlen']):255)."), $key\n";
				}else{
					switch ($value['sub_type']){
						case 'bigint':
							$body_sql .= "\t`".$value['field_name']."`\t bigint(20) NOT NULL , $key\n";
							break;
						case 'int':
							$body_sql .= "\t`".$value['field_name']."`\t int(".((!empty($value['maxlen']))?intval($value['maxlen']):10).") NOT NULL , $key\n";
							break;
						case 'varchar':
						default:
							$body_sql .= "\t`".$value['field_name']."`\t varchar (".((!empty($value['maxlen']))?intval($value['maxlen']):255)."), $key\n";
							break;	
					}
				}
				break;
				
			// textarea - text
			case "textarea":  case "multiselect_from_table":
				$body_sql .= "\t`".$value['field_name']."`\t text, \n";
				break;
			case "multcheckbox":
				for ($i = 0; $i < sizeof($value['arr']); $i++) {
					$body_sql .= "\t`".$value['field_name']."$i`\t tinyint, $key\n";
				}
				break;			
			// unknown
			default:
				$ERROR_MSG .= Main::get_lang_str('not_type_field', 'db');
				// return false;
		}}
	}
	
	function remove($url, $remove_dir = false){
		
		$alias = (strstr($url, "/db/"))?str_replace('/db/','',$url):$url;
		$alias = str_replace('/','',$alias);
		
		
		// udalit' iz fajlov konfiguracij
		KAT::load_users_conf($arr);
		eval(implode("",$arr));

//		$str = var_export($_KAT);

		$tab = $_KAT['TABLES'][$alias];
//
//		if ($_KAT['onpage_def'][$alias])
//			unset($_KAT['onpage_def'][$alias]);
//		if ($_KAT['TABLES'][$alias])
//			unset($_KAT['TABLES'][$alias]);
//		if ($_KAT['TITLES'][$alias])
//			unset($_KAT['TITLES'][$alias]);
//		if ($_KAT['FILES'][$alias])
//			unset($_KAT['FILES'][$alias]);
//		if ($_KAT['SRC'][$alias])
//			unset($_KAT['SRC'][$alias]);
//		if ($_KAT[$alias]['AFTER_ADD'])
//			unset($_KAT[$alias]['AFTER_ADD']);
//		if ($_KAT[$alias]['hidden'])
//			unset($_KAT[$alias]['hidden']);
			
		$str = '';
		
		foreach ($_KAT['TABLES'] as $al){
			if ($alias != $al){
			$str .= "
			\$_KAT['onpage_def']['$al']	= '".$_KAT['onpage_def'][$al]."';
			\$_KAT['TABLES']['$al']	= '".$_KAT['TABLES'][$al]."';
			\$_KAT['TITLES']['$al']	= '".$_KAT['TITLES'][$al]."';
			\$_KAT['SRC']['$al']	= '".$_KAT['SRC'][$al]."';
			\$_KAT['FILES']['$al']	= '".$_KAT['FILES'][$al]."';
			\$_KAT['$al']['AFTER_ADD']	= '".$_KAT[$al]['AFTER_ADD']."';
			\$_KAT['$al']['hidden']	= '".$_KAT[$al]['hidden']."';
			";
      if(!empty($_KAT['DOC_PATH'][$al])){
      $str .= "
      \$_KAT['DOC_PATH']['$al']= '".$_KAT['DOC_PATH'][$al]."';
      ";
      if(is_array($_KAT[$al]['matrix_table'])){
        foreach($_KAT[$al]['matrix_table'] as $key =>$val){
      $str .= "
      \$_KAT['$al']['matrix_table']['$key'] = '".$val."';
      ";
        }
      }
  
      }
      }
		}
		
		
		
//		$str = "\$_KAT = ".var_export($_KAT,true).";";
//		echo "str = ".$str;

		KAT::save_users_conf($str);
		
		// udalit' tablicu
		$tables = SQL::tlist();
		
		print_r($tables);
		
		global $_KAT, $_CORE;
		
		$tab = $alias;
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . $alias;
		
		
		if (in_array($table, $tables)) {
			$query = "DROP TABLE $table";
			echo $query;
			
			$res = SQL::query($query,DEBUG);
			
			if (!$res->Result)
				echo $res->ErrorQuery;
				
			// esli nado idiry tozhe udaljat'
			elseif ($remove_dir) {
				// shablony pol'zovatelja
				$def_dir = $_CORE->PATHTPLS.'/def/'.$_KAT['MODULE_NAME']."/".$tab.'/';
				echo "<HR>$def_dir<HR>";
				if (FILE::rn_dir( $def_dir, ' ', 'del')){
					if (@DEBUG) $_CORE->log_message('', 'Document data DELETED '.$def_dir,'doctxt::del');
				}else {
					$del_tree	= false;
					$_KAT['ERROR'] .= 'Document data not DELETED '.$def_dir;
				}
				
				// shablony administratora
				$def_dir = $_CORE->PATHTPLS.'/admin/'.$_KAT['MODULE_NAME']."/".$tab.'/';
				echo "<HR>$def_dir<HR>";
				if (FILE::rn_dir( $def_dir, ' ', 'del')){
					if (@DEBUG) $_CORE->log_message('', 'Document data DELETED '.$def_dir,'doctxt::del');
				}else {
					$del_tree	= false;
					$_KAT['ERROR'] .= 'Data not DELETED '.$def_dir;
				}
				// fajly s izobrazhenijami i dokumentami
				$def_dir = KAT::get_data_path('f_'.$tab, $dir, KAT_LOOKIG_DATA_DIR);
				echo "<HR>$def_dir<HR>";
				if (FILE::rn_dir( $def_dir, ' ', 'del')){
					if (@DEBUG) $_CORE->log_message('', 'Document data DELETED '.$def_dir,'doctxt::del');
				}else {
					$del_tree	= false;
					$_KAT['ERROR'] .= 'Data not DELETED '.$def_dir;
				}
			}
				
		}
	}
	
	/**
	 * Zagruzhaem vmassiv strok pol'zovatel'skij fajl conf.user.inc.php, ibrezav pervyj <? i poslednij ? >
	 *
	 * @param string $str empty, filling in function
	 * @return string $filepath 
	 * @uses $_CORE
	 * @uses $_KAT
	 */
	function load_users_conf(&$arr){
		
		global $_CORE, $_KAT;
		$arr = array();
//		$file = $_CORE->ROOT.$_CORE->DIRMODS.$_KAT['MODULE_NAME'].'/conf.user.inc.php';
		$file = KAT::get_data_path('conf.user.inc.php', $f, KAT_LOOKIG_DATA_FILE);

		if (is_file($file)){
			$arr = file($file);
			array_shift($arr);
			array_pop($arr);
		}
		return $file;
	}
	
	/**
	 * sohranjaem stroku v pol'zovatel'skij fajl conf.user.inc.php, vstaviv pervyj <? i poslednij ? >
	 *
	 * @param string $str empty, filling in function
	 * @return string $filepath 
	 * @uses $_CORE
	 * @uses $_KAT
	 */
	function save_users_conf($str){
		
		global $_CORE, $_KAT;
//		$file = $_CORE->ROOT.$_CORE->DIRMODS.$_KAT['MODULE_NAME'].'/conf.user.inc.php';
		$file = KAT::get_data_path('conf.user.inc.php', $f, KAT_LOOKIG_DATA_FILE);
		if (is_file($file)){
			$fp	= fopen($file, 'w');
			fputs($fp,"<?\n $str \n?>" ); // <?
			fclose($fp);
		}
		return $file;
	}
	
/**
 * Avtomaticheski sozdaet imja dlja vremennogo ili postojannogo faila.
 * 
 * Pri sostavlenii ispol'zuet parametr $str, kak chast' novogo imeni.
 * Takzhe ispol'zuet parametr $_REQUEST['form']['alias'] i basename( $_SERVER['REQUEST_URI'] ).
 * <code>
 * $add = (!empty($_REQUEST['form']['alias']))? $_REQUEST['form']['alias']:basename($_SERVER['REQUEST_URI']);  
 * </code>
 *
 * @uses $_KAT['KUR_ALIAS']
 * @param string $str part of new name
 * @return string(new name)
 */
	function get_file_name($str='')
	{
		global $_KAT;
		$add = (!empty($_REQUEST['form']['alias']))? $_REQUEST['form']['alias']:basename($_SERVER['REQUEST_URI']);
		return $_KAT['KUR_ALIAS'].'_'.STR::translit($_REQUEST['form']['name'])."_".$str."_".$add."_".time();
	}	
	
	
	

	/**
	 * Vstavka zapisi iz drugoj ili jetoj zhe tablicy
	 *
	 * @param array of alias $cmd
	 */
	function paste($cutted_items, $cutted_types){
		global $FORM_DATA, $form, $_KAT;
		foreach( $cutted_items as $k=>$cmd ){
			list($tmp,$_module, $_tab_alias, $_doc_alias) = explode("/",$cmd);
			
			$need_fields = array_keys($FORM_DATA);
			foreach ($need_fields as $v => $k){
				if( substr( $k, 0, 3)=="doc"  ){
					$new_abs_path = $FORM_DATA[$k]['abspath'];break;
				}
			}

			
			$tmp = array();
			$kur_alias = $_KAT['KUR_ALIAS'];
			
			foreach ($need_fields as $v => $k){
				$tmp[$k] = cmd("/$_module/$_tab_alias/field/$_doc_alias/$k");
				if( !empty($tmp[$k]) && substr( $k, 0, 3)== "doc"  ){
					$tmp_docname[$k] = $tmp[$k];
					$tmp[$k] = $FORM_DATA[$k]['abspath']."/".$tmp[$k]; //KAT::copy_files( $new_abs_path, $_tab_alias, $tmp[$k], $k);
					$tmp_doc[$k] = $tmp[$k];
				}
			}
			
//			 dlja vosstanovlenija
			cmd("/".$_KAT['MODULE_NAME']."/$kur_alias/nothing");
			$form = $tmp;
			if (is_array($tmp_doc)) foreach( $tmp_doc as $k=>$v )
			{
				global $$k;				$$k = $tmp_doc[$k];
				global ${"kill_".$k};	${"kill_".$k} = true;
				$_FILES[$k]['tmp_name'] = $tmp_doc[$k];
				$_FILES[$k]['name'] = $tmp_docname[$k];
			}
			
			$pkey = (empty($_KAT['INDEX'][$_KAT['KUR_ALIAS']])) ? 'id' : $_KAT['INDEX'][$_KAT['KUR_ALIAS']];

			// �������� ���� �� ��� � ����� ������, ���� ���� �� �������� ����� 
			if (SQL::getval("name",$_KAT['KUR_TABLE'],"alias = '".$form['alias']."'",'', DEBUG)){
				for ($i=2;1;$i++)
					if (!SQL::getval("name",$_KAT['KUR_TABLE'],"name = '".$form['name'].'('.$i.")'",'',DEBUG))
						break;
						
				$form['name'] = $form['name']."(".$i.")";
				// alias sdelaetsja potom po name
			}
			
			unset($form[$pkey]);
			
			if (KAT::check_form($form))
			{
				KAT::save($form);		
				for ($i=0;$i<10;$i++) {
					global ${'doc'.$i};
					${'doc'.$i}	= '';
				}
				global $doc;$doc = '';
				$_FILES=array();
//ob_clean();
//foreach( $form as $k=>$v)
//echo "<script>window.alert('".$k."=>".$v."');</script>";
//exit();				
			}else{
				$error .= "<br>".$_KAT['ERROR'];
			}
		}
		error_log($error);
		if( !empty($error)) echo $error;
	}
	
	/**
	 * Lookig for data LINK, need after idea to keep all data separete
	 *
	 * @param string $looking
	 * @param string $found
	 * @param const $look4dir
	 * @return string LINK PATH to the source or empty
	 */
	static function get_data_link($looking, &$found, $look4dir = true){
		global $_CORE, $_KAT;
		$files	=  array( 
			$_CORE->SiteModDir.'/'.$looking,
			$_CORE->ROOT.$_CORE->DATA_PATH.$_KAT['MODULE_NAME'].'/'.$looking,
			$_CORE->ROOT.$_CORE->DATA_PATH.$_KAT['MODULE_NAME'].'/f_'.$_KAT['SRC'][$_KAT['KUR_ALIAS']],
			$_CORE->SiteModDir.'/_'.$looking,
		);
//		$tmp = $found = str_replace($_CORE->PATHTPLS, $_CORE->TEMPLATES, ($look4dir == KAT_LOOKIG_DATA_DIR) ? FILE::chk_dirs($files, $looking) : FILE::chk_files($files, $looking));
		$tmp = $found = str_replace('//','/',str_replace($_CORE->ROOT, '/', ($look4dir == KAT_LOOKIG_DATA_DIR) ? FILE::chk_dirs($files, $looking) : FILE::chk_files($files, $looking)));
		return $tmp;		
	}
	
	/**
	 * Lookig for data PATH, need after idea to keep all data separete
	 *
	 * @param string $looking
	 * @param string $found
	 * @param const $look4dir
	 * @return string FULL PATH to the source of empty
	 */
	static function get_data_path($looking, &$found, $look4dir = KAT_LOOKIG_DATA_DIR){
		global $_CORE, $_KAT;
		$files	=  array( 
			$_CORE->SiteModDir.'/'.$looking,
			$_CORE->ROOT.$_CORE->DATA_PATH.$_KAT['MODULE_NAME'].'/'.$looking,
			$_CORE->ROOT.$_CORE->DATA_PATH.$_KAT['MODULE_NAME'].'/f_'.$_KAT['SRC'][$_KAT['KUR_ALIAS']],
			$_CORE->SiteModDir.'/_'.$looking,
		);
		
		$tmp = $found = ($look4dir == KAT_LOOKIG_DATA_DIR) ? FILE::chk_dirs($files, $looking) : FILE::chk_files($files, $looking);
		return $tmp;		
	}

	/**
	*
	*/
	function allow($alias)
	{
		global $_KAT;
		return ($_SESSION['SESS_AUTH']['LOGIN'] == 'admin' || $_SESSION['SESS_AUTH']['LOGIN'] == 'developer'
			|| (is_array($_KAT['permissions'][$_SESSION['SESS_AUTH']['LOGIN']]) && in_array($alias, $_KAT['permissions'][$_SESSION['SESS_AUTH']['LOGIN']] ))
			);
	}
  
/**
*
*/
function get_restore_confuser($path="", $src="" ){
  if($path=="" || $src=="") return false;
  global $_KAT;
 		KAT::load_users_conf($arr_kat);

    if(!is_array($arr_kat)) $arr_kat = array();
    if(!is_array($arr)) $arr = array();
    $rootpath = $_SERVER['DOCUMENT_ROOT']."/data/doctxt/data";
    $tree = FILE::read_dir($rootpath.$path);

		// Need SQL for define db
		$tlist = SQL::tlist();

    if(is_array($tree) && count($tree) ){
      $i = 0;
			// ���������� �� ����������
      foreach($tree as $val){
         $path_tree = $path.$val."/";
				// ���� �������������
        if(is_dir($rootpath.$path.$val."/") && $val!='file' &&  $val!='.svn'){

					// ����������� ���� 
					$path_tree = @implode('', file($rootpath.$path.$val."/path"));
					if (empty($path_tree)) $path_tree = $path.$val.'/';
						
					// correct path
					if (!strstr($path_tree, '/db/') && in_array(DB_TABLE_PREFIX.$val, $tlist)) {
						$path_tree .= 'db/'.$val.'/';
					}
					$arr[$i]['path'] = '/doctxt/'.$path_tree;//.$val."/";
          $arr[$i]['name'] = 	@implode('', file($rootpath.$path.$val."/title"));
          if(in_array(DB_TABLE_PREFIX.$val, $tlist) && !in_array($val,$_KAT['TABLES'])){
      			$arr_kat[] = "	\$_KAT['onpage_def']['$val']	= 20;\n";
      			$arr_kat[] = "	\$_KAT['TABLES']['$val']	= '".$val."';\n";
      			$arr_kat[] = "	\$_KAT['TITLES']['$val']	= '".str_replace(array('"',"'"),"&quot;",$arr[$i]['name'])."';\n";
      			$arr_kat[] = "	\$_KAT['SRC']['$val']     = '".$_KAT['SRC'][$src]."';\n";
            $arr_kat[] = "	\$_KAT['DOC_PATH']['$val']	= '".$arr[$i]['path']."';\n";
      			$arr_kat[] = "	\$_KAT['FILES']['$val']	= '".$_KAT['FILES'][$src]."';\n";
      			$arr_kat[] = "	\$_KAT['$val']['AFTER_ADD']	= 'db/".$val."/';\n";
      			$arr_kat[] = "	\$_KAT['$val']['hidden']	= 'TRUE';\n";
            $arr_kat[] = "\n";
          }

          $i++;
        }
      }
      $str = trim(implode("",$arr_kat));
     KAT::save_users_conf($str);
    }
    return $str;
}

// �������� �������� � �������� �� �����
	function resizefoto($absolutePath, $maxX , $maxY ){
		# minimal & maximum zoom
		$minZoom = 1; # per cent related on orginal (!=0)
		$maxZoom = 500; # per cent related on orginal (!=0)
		$image = getImageSize($absolutePath, $info); # $info, only to handle problems with earlier php versions...
		switch($image[2]) {
			case 1:
			# GIF image
			break;
			case 2:
				# JPEG image
				$timg = imageCreateFromJPEG($absolutePath);
			break;
			case 3:
				# PNG image
				$timg = imageCreateFromPNG($absolutePath);
			break;
		}
		
		# reading image sizes
		$imgX = $image[0];
		$imgY = $image[1];
		
		if (empty($maxX) && empty($maxY)) {
			$maxX = $imgX;
			$maxY = $imgY;
		}
		
		if ($maxX > $imgX && empty($maxY) ){
			$maxX = $imgX;
			$maxY = $imgY;
		}
		
		if ($maxX > $imgX && $maxY > $imgY ){
			$maxX = $imgX;
			$maxY = $imgY;
		}
		
		if (empty($maxX)){
			# calculation zoom factor 
			$_Y = $imgY/$maxY;
			$maxX = ceil($imgX / $_Y);
		}
		
		if (empty($maxY)){
			# calculation zoom factor 
			$_X = $imgX/$maxX;
			$maxY = ceil($imgY / $_X);
		}
		
		# calculation zoom factor 
		$_X = $imgX/$maxX * 100;
		$_Y = $imgY/$maxY * 100;
		# calculation zoom factor 
		$_X = $imgX/$maxX * 100;
		$_Y = $imgY/$maxY * 100;
		
		# selecting correct zoom factor, so that the image always keeps in the given format
		# no matter if it is more higher than wider or the other way around
		if((100-$_X) < (100-$_Y)) $_K = $_X;
		else $_K = $_Y;
		
		# zoom check to the original
		if($_K > 10000/$minZoom) $_K = 10000/$minZoom;
		if($_K < 10000/$maxZoom) $_K = 10000/$maxZoom;
		
		# calculate new image sizes
		$newX = $imgX/$_K * 100;
		$newY = $imgY/$_K * 100;
		
		# set start positoin of the image
		# always centered 
		$posX = ($maxX-$newX) / 2;
		$posY = ($maxY-$newY) / 2;
		
		# creating new image with given sizes
		$imgh = imageCreateTrueColor($newX, $newY); // maxX maxY
		imagealphablending($imgh, FALSE);
		
		# setting colours
		$cols = explode(",", $picBG);
		$bgcol = imageColorallocate($imgh, trim($cols[0]), trim($cols[1]), trim($cols[2]));
		$cols = explode(",", $picFG);
		$fgcol = imageColorallocate($imgh, trim($cols[0]), trim($cols[1]), trim($cols[2]));

		# create small copy of the image
		$width 		= $image[0];
		$height 	= $image[1];
		
		$new_width 	= $newX;
		$new_height = $newY;
		
		if ($new_width > 0)					$x = $new_width / $width;
		if ($new_height > 0)				$y = $new_height / $height;
		if (($x > 0 && $y > $x) || $x==0)	$x = $y;
		
		$width_big = $width * $x; 
		$height_big = $height * $x;
		
		imagecopyresampled($imgh,$timg,0,0,0,0,$width_big,$height_big,imagesx($timg),imagesy($timg)); 
		
		unlink($absolutePath);
		# output
		switch($image[2]) {
			case 1:
				# GIF image
			break;
			case 2:
				# JPEG image   
				imageJPEG($imgh,$absolutePath,100);
			break;
			case 3:
				# PNG image
				imagesavealpha($imgh, TRUE);
				@imagePNG($imgh,$absolutePath.$file);
			break;
		}
		
	}
    
    function save_history( $tab, $data = '', $old = '', $insert = true) {
        
        $skip_fields = array('path', 'author_password');
            
        if (!empty($old)) { // ����� ��� ������ ������
            // ������ �������
            foreach ($data as $f => $v) 
                if (!in_array($f, $skip_fields)){
                    if ($data[$f] != $old[$f] ) {
                        if ($old[$f] == 0 && empty($data[$f])) continue;
                        $change[$f] = $v;
                    }
                }
                
        }else { // ����� �� �������
            unset($data['password']); 
            $change = $data;
        }
        
        // ���� �������� ���� 
        if (!empty($change)) {
            $change_arr = array(
                'from_auth' => $_SESSION['SESS_AUTH']['ID'],
                'rec_id'    => ($data['id']) ? $data['id'] : $old['id'],
                'changes'   => serialize($change),
            );       
            
            $result = SQL::ins_set(  $tab . '_history', SQL::get_set($change_arr), DEBUG);     
        }
    }
  
} // KAT

/**
 * Avtomaticheski sozdaet imja dlja vremennogo ili postojannogo faila.
 * 
 * Pri sostavlenii ispol'zuet parametr $str, kak chast' novogo imeni.
 *
 * @uses $_KAT['KUR_ALIAS']
 * @param string $str part of new name
 * @return string(new name)
*/
function get_file_name($str='')
{
	global $_KAT;
	$sek = time();
	return $_KAT['KUR_ALIAS'].'_'.$str."_".basename($_SERVER['REQUEST_URI'])."_".$sek;
	while( date("is")==$sek ) true;
	
}

/**
 * Avtomaticheski sozdaet KOROTKOE imja dlja vremennogo ili postojannogo faila.
 * 
 * Pri sost ( avl ) enii ispol'zuet parametr $str, kak chast' novogo imeni.
 *
 * @uses $_KAT['KUR_ALIAS']
 * @param string $str part of new name
 * @return string (new name)
*/
function get_small_file_name()
{
	global $_KAT;
	return $_KAT['KUR_ALIAS'].'_'.basename($_SERVER['REQUEST_URI']).'_s';
}




?>
