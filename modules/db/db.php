<?
/**
 *
 * Dokumentacija k modulju db
 *
 * Obwij vid komandy: _sajt_/db/_komanda_.
 * Vozmozhnye komandy:
 * 1.Komanda /list
 *
 *       Vnimanie: rabotaet tol'ko dlja administratorov!
 *       Ispol'zuet $_KAT[], opisyvaemyj v fajle conf.inc.php.
 *       V chastnosti: $_KAT['TABLES']           - spisok tablic
 *                    $_KAT['MODULE_NAME']      - imja modulja (db)
 *                    $_KAT['TITLES']           - spisok naimenovanie katalogov
 *
 *       Ispol'zuet shablony (podkljuchaemye pri pomowi konstrukcii KAT::inc_tpl()):
 *                             db.list.head.html - zagolovok
 *                             db.list.item.html - ciklicheski povtorjajuwajasja chast'
 *                                Parametry peredavaemye shablonu:
 *                                     $data['path']="/".$_KAT['MODULE_NAME']."/".$alias; -put' do tablicy,
 *                                     $data['name']=$_KAT['TITLES'][$alias]; - psevdonim tablicy dannyh.
 *                             db.list.foot.html - okonchanie
 *
 *       Rezul'tatom raboty javljaetsja HTML-kod shablonov ispol'zujuwij dannye iz katalogov,
 *       u kotoryh net polja $_KAT['<alias_name>']['hidden']
 *
 *
 * @uses KAT::inc_tpl();
 * @uses $_KAT['MODULE_NAME']
 * @uses $_KAT['TABLES']
 * @uses $_KAT['TITLES']
 *
 *
 * 2.Komanda /seealso
 *
 * Vyvodit kod stroki SEEALSO.
 *
 * Prichem esli ne suwestvuet $_PROJECT['SEEALSO'], to stroka formiruetsja na osnove $_KAT[$_KAT['KUR_ALIAS']]['SEEALSO']
 * <code>
 *    $_PROJECT['SEEALSO'] .= (is_array($_KAT[$_KAT['KUR_ALIAS']]['SEEALSO']))
 *    ? $_CORE->cmd_exec($_KAT[$_KAT['KUR_ALIAS']]['SEEALSO'])
 *    : $_KAT[$_KAT['KUR_ALIAS']]['SEEALSO'];
 * </code>
 *
 * @uses $_PROJECT['SEEALSO']
 * @uses $_KAT['KUR_ALIAS']
 * @uses $_KAT[$_KAT['KUR_ALIAS']]['SEEALSO']
 * @uses $_CORE->cmd_exec();
 *
 *
 * 3.Komanda /pages
 *
 * Vydaet rezul'tat raboty metoda KAT::get_pages();
 *
 * @uses KAT::get_pages()
 *
 *
 * 4.Komanda /h
 *
 * Help
 * Vydaet vozmozhnye komandy modulja db, sejchas ne vse
 *
 * @uses $_KAT['MODULE_NAME']
 *
 *
 * 5.Komanda /content/title
 *
 * Rezul'tatom raboty sluzhit zagolovok stranicy.
 * Esli ne pusto $_KAT['RE_TITLE'], to beretsja $_KAT['RE_TITLE'].
 * Inache, esli on ne opredeljaetsja v module User, te ne otklikaetsja na komandu $_CORE->cmd_exec(array('user','/'.$Cmd)),
 * to zagolovok beretsja iz $_KAT['TITLE'].
 *
 * @uses $_KAT['RE_TITLE']
 * @uses $_KAT['TITLE']
 * @uses $_CORE->cmd_exec()
 *
 *
 * 6.Komanda /content/path/
 *
 * Vozvrawaet rezul'tat raboty $_CORE->cmd_exec(array('user','/'.$Cmd));
 * Prichem esli ne pusto $_KAT['SUB_TITLE'], to ewe zapolnjaetsja $_PROJECT['PATH_2_CONTENT']
 * <code>
 *   $_PROJECT['PATH_2_CONTENT'][]        = array(
 *      'name'        => $_KAT['SUB_TITLE'],
 *      'path'        => '/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS']
 *    );
 * </code>
 *
 * @uses $_KAT['KUR_ALIAS']
 * @uses $_KAT['MODULE_NAME']
 * @uses $_KAT['SUB_TITLE']
 * @uses $_PROJECT['PATH_2_CONTENT']
 * @uses $_CORE->cmd_exec()
 *
 *
 * 7.Komanda /content/title/short
 *
 * Esli suwestvuet $_KAT['SUB_TITLE'], to v rezul'tate vydaetsja ono.
 * Esli ne pust rezul'tat $_CORE->cmd_exec(array('user','/'.$Cmd)), to on.
 * Inache vydaetsja $_KAT['TITLE'].
 *
 * @uses $_KAT['SUB_TITLE']
 * @uses $_KAT['TITLE']
 * @uses $_CORE->cmd_exec()
 *
 *
 * 8.Komanda /adm/menu
 *
 * Esli ne pusto $_KAT['ADM_MENU'], to na ego osnove izmenjaet $_PROJECT['ADM_MENU'].
 *
 * @uses $_KAT['ADM_MENU']
 * @uses $_PROJECT['ADM_MENU']
 *
 *
 * 9.Komanda /install
 *
 * Instaljacija modulja Katalog
 *
 * @uses $_KAT['TITLE']
 * @uses $_CORE->CoreModDir
 *
 *
 * 10.Komanda /noline
 *
 * Otkljuchaet polosu listinga
 *
 *
 * 11.Komanda /getdata
 *
 * Ispolnjaet KAT::search('', $FORM_ORDER);
 *
 * @uses KAT::search()
 * @uses $FORM_ORDER
 *
 *
 * 12.Komanda / (pusto)
 *
 * sm. Komanda /search
 *
 *
 * 13.Komanda /search
 *
 * Vyvod rezul'tatov poiska v baze dannyh, esli poisk bez parametrov, to vydaet vse.
 * Vyvod proishodit, pri pomowi podkljuchaemogo fajla: search.inc.php.
 * <code>include($_CORE->CoreModDir.'/search.inc.php');</code>
 *
 * Esli vkljuchen rezhim adminestrirovanija, to ewe dobavljaetsja menju admina.
 *
 * @uses $_CORE->IS_ADMIN
 * @uses $_CORE->CoreModDir
 * @uses KAT::inc_tpl()
 * @uses $_KAT['ERROR']
 *
 *
 * 14.Komanda /last/name
 *
 * Vozvrawaet poslednij (pervyj) name rezul'tata poiska, v sootvetstvie s parametrom $FORM_ORDER
 *
 * @uses $FORM_ORDER
 * @uses $_KAT['SEARCH_LAST']['alias']
 * @uses KAT::last()
 *
 *
 * 15.Komanda /last/ts
 *
 * Vozvrawaet poslednij (pervyj) ts rezul'tata poiska, v sootvetstvie s parametrom $FORM_ORDER
 *
 * @uses $FORM_ORDER
 * @uses $_KAT['SEARCH_LAST']['alias']
 * @uses KAT::last()
 *
 *
 * 16.Komanda /last/anons
 *
 * Vozvrawaet poslednij (pervyj) anons rezul'tata poiska, v sootvetstvie s parametrom $FORM_ORDER
 *
 * @uses $FORM_ORDER
 * @uses $_KAT['SEARCH_LAST']['alias']
 * @uses KAT::last()
 *
 *
 * 17.Komanda /last/alias
 *
 * Vozvrawaet poslednij (pervyj) alias rezul'tata poiska, v sootvetstvie s parametrom $FORM_ORDER
 *
 * @uses $FORM_ORDER
 * @uses $_KAT['SEARCH_LAST']['alias']
 * @uses KAT::last()
 *
 *
 * 18.Komanda /last/reset
 *
 * Sbros rezul'tatov poslednego poiska
 *
 * @uses KAT::last_reset()
 *
 *
 * 19.Komanda /field/_alias_/_filed_
 *
 * Vypolnjaet KAT::get_cont(substr(dirname($SubCmd),1), basename($SubCmd) );
 *
 * @uses KAT::get_cont();
 *
 *
 * 20.Komanda /last/_kolvo_
 *
 * Vypolnjaet KAT::last($FORM_ORDER, $num, 0);
 *
 * @uses KAT::last()
 * @uses KAT::last_reset()
 *
 *
 * 21.Komanda /last/_num_/_field_
 *
 * Vyvodit jelement zapisi _field_ s nomerom _num_.
 *
 * @uses KAT::last();
 *
 *
 * 22.Komanda /path
 *
 * Vyvodit put' do fajlovogo hraniliwa dannogo kataloga
 *
 * @uses $_CORE->DIRMODS
 * @uses $_KAT['KUR_ALIAS']
 *
 *
 * 23.curr/_choto_
 *
 * Vyvodit $_KAT[_choto_]
 *
 *
         case "site_order/":
        case "site_order":  //added 01.03.06 by Shesternin
				if( !empty( $FORM_DATA['prior'] ) ){
					$template = ( $_CORE->CLEAR_VERSION )?"/clear":"";
					$wC = empty( $_POST['closeWindow'] )?false:$_POST['closeWindow'];
					if( !empty( $_POST['data_ordering'] ))
					  {  $data = $_POST['data_ordering'];
					     $aliases = explode( ";", $data );
					     $i=0;
					     foreach( $aliases as $al )
						     {  $i++;
						        SQL::upd( $_KAT['KUR_TABLE'], "prior = '$i'", "( alias = '$al' )" );
						     }
					  }
	        	    if (KAT::inc_tpl('order.inc.phtml', $file))
	                    	include "$file";
				}
        	    break;
*
 * 24.site_order
 *
 * Vozvrawaet okno uporjadochivanija vyvoda informacii na sajte. Prichem okno mozhet byt' vsplyvajuwim ili net.
 *
 * Ispol'zuet shablon order.inc.phtml. Komanda menjat pole prior (prioritet) u dannyh tekuwego kataloga.
 *
 * @uses $CORE->CLEAR_VERSION
 * @uses $FORM_DATA
 * @uses KAT::inc_tpl()
 * @uses $FORM_ORDER
 */
#
# SWITCHER
#
global $_CORE, $_KAT, $_PROJECT;
Main::load_lang_mod('db');
//if (is_file($_CORE->SiteModDir.'/conf.user.inc.php'))
//	include $_CORE->SiteModDir.'/conf.user.inc.php';


/**
 * opredelenie vseh peremennyh opisyvajuwih tablicy i razdely
 */
/**
 *
 */
include_once($_CORE->CoreModDir.'/kat.inc.php');



// Load user conf
if (KAT::get_data_path('conf.user.inc.php', $f, KAT_LOOKIG_DATA_FILE))
	include $f;

// Load default settings for db configurations
include $_CORE->CoreModDir.'/_conf.inc.php';
include $_CORE->SiteModDir.'/conf.inc.php';


/**
 *
 * opredelenie vseh peremennyh opisyvajuwih tablicy i razdely
 * dobavlennye pol'zovatelem (dinamicheskaja)
 *
 */

/*  popytka zanulit' global'nye peremennye prikazhdom zapuske DB,
		reshaem problemu kogda na stranice est' forum naprimer i tam est' FORM_SELECT, potom zabiraem seealso, i dlja poluchenija imeni srabatyvaet FORM_SELECT
	*/
global $FORM_SELECT;
$FORM_SELECT = '';

list($tab, $command)        = $_CORE->cmd_parse($Cmd);
if (!empty($_KAT['TABLES'][(string)$tab])) {
        $_KAT['KUR_ALIAS']        = (empty($tab)) ? $_KAT['DEF_ALIAS'] : (string)$tab;
        $_KAT['KUR_TABLE']        = ( ($_KAT[$tab]['TABLE_PREFIX']?$_KAT[$tab]['TABLE_PREFIX']:(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')) .
        							$_KAT['TABLES'][(string)$tab]);
        $_KAT['onpage']           = (!empty($_KAT['onpage_def'][(string)$tab]))?$_KAT['onpage_def'][(string)$tab]:$_KAT['onpage_def']['def'];
        $_KAT['TITLE']            = (empty($_KAT['TITLE'])||$_KAT['TITLE']=$_KAT['TITLE']['alias']) ? $_KAT['TITLES'][(string)$tab] : $_KAT['TITLE'];
		$_KAT['MASS_UPLOADER']['doc']		  = empty($_KAT['MASS_UPLOADER']['doc'])        ? 'doc' : $_KAT['MASS_UPLOADER']['doc'];
		$_KAT['MASS_UPLOADER']['description'] = empty($_KAT['MASS_UPLOADER']['description'])? 'name': $_KAT['MASS_UPLOADER']['description'];
        global $FORM_DATA, $FORM_SELECT, $FORM_SELECT_BEFORE, $FORM_ORDER, $FORM_LIMIT, $FORM_WHERE, $FORM_FROM, $FORM_FIELDLONG;
        global $FORM_IMPORT;

        /*
				* Try to load user configurations, else try to get from core (also )
				* elso inluded in install.php
				*/
				if (is_file($_CORE->SiteModDir.'/'.$_KAT['FILES'][(string)$tab])) {
					include $_CORE->SiteModDir.'/'.$_KAT['FILES'][(string)$tab];
				}elseif($_CORE->CoreModDir.'/_'.$_KAT['FILES'][(string)$tab]){
					include $_CORE->CoreModDir.'/_'.$_KAT['FILES'][(string)$tab];
				}
        
				if ($_KAT['conf']['SEO'] && $_KAT['KUR_ALIAS']!='auth' && empty($_KAT[$_KAT['KUR_ALIAS']]['NOSEO'])) Main::add_seo($FORM_DATA); // If need SEO module 26.04.2011
				if ($_KAT['conf']['fb_tags'] && $_KAT['KUR_ALIAS']!='auth') Main::add_fb_tags($FORM_DATA); // if need fb tags
//        echo "here1".$FORM_ORDER;
        $Cmd        = substr($command, 1);
}

// if empty ... goto index, nothing to do
if (empty($tab)) {
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: /");	
}

// Prava na blok
if ($_CORE->IS_ADMIN && !KAT::allow($_KAT['KUR_ALIAS']) && $Cmd != 'adm_menu'){
	echo Main::get_lang_str('dostup_zakryt', 'db');
	return;
}

//if (empty($_KAT['KUR_ALIAS'])&&$_CORE->IS_ADMIN){
//	$Cmd = 'list';
//}

/**
                IF ADMIN
**/
//print_r($is_admin);
//echo "=".$Cmd;
//if ($is_admin[0] == 'admin') {
//        $admin        = array_shift($is_admin);
//        $Cmd        = implode('/',$is_admin);
//}
//echo ">".$Cmd;
/**
                SWITCHER
**/

switch ($Cmd) {
		case "add_comment":
			if (empty($_KAT['TABLES']['forum'])) {
				return Main::get_lang_str('modul_ne_nastroen', 'db');
			}

			if (Main::comm_inc('add_comment.inc.php', $f, 	$_KAT['MODULE_NAME'], $_KAT['KUR_ALIAS']))
				include $f;

			break;
		case "add_userblog":
			if (empty($_KAT['TABLES']['blogs'])) {
				return Main::get_lang_str('modul_ne_nastroen', 'db');
			}

			if (Main::comm_inc('add_userblog.inc.php', $f, 	$_KAT['MODULE_NAME']))
				include $f;

			break;

        case 'list': // nizhpravda
                if (!$_CORE->IS_ADMIN) break;

                if (KAT::inc_tpl('db.list.head.html', $file))
                        include "$file";
								
                foreach ($_KAT['TABLES'] as $alias => $table){

                        if (!empty($_KAT[$alias]['hidden']) || $alias == '') continue;

                        $data['path'] = "/".$_KAT['MODULE_NAME']."/".$alias;
                        $data['name'] = $_KAT['TITLES'][$alias];
                        if (KAT::inc_tpl('db.list.item.html', $file))

                                include "$file";
                        else
                                echo "<p>".$_KAT['TITLES'][$alias]. " (".$alias.")";

                }
				$_KAT['TITLE'] = Main::get_lang_str('spisok', 'db');
                if (KAT::inc_tpl('db.list.foot.html', $file))
                        include "$file";

                break;

        case 'seealso': // nizhpravda
                if (empty($_PROJECT['SEEALSO']))
                        $_PROJECT['SEEALSO'] .= (is_array($_KAT[$_KAT['KUR_ALIAS']]['SEEALSO']))
                                ? $_CORE->cmd_exec($_KAT[$_KAT['KUR_ALIAS']]['SEEALSO'])
                                : $_KAT[$_KAT['KUR_ALIAS']]['SEEALSO'];
                echo $_PROJECT['SEEALSO'];
                break;
        case "pages":
                echo KAT::get_pages();
                break;
        case "h":
                echo "/".$_KAT['MODULE_NAME']."/_kat_name_/content/title<BR>";
                echo "/".$_KAT['MODULE_NAME']."/_kat_name_/content/title/short<BR>";
                echo "/".$_KAT['MODULE_NAME']."/_kat_name_/content/path<BR>";
                echo "/".$_KAT['MODULE_NAME']."/_kat_name_/field/_field_name_<BR>";
                echo "/".$_KAT['MODULE_NAME']."/_kat_name_/last/_num_<BR>";
                echo "/".$_KAT['MODULE_NAME']."/_kat_name_/last/_num_/_field_name_<BR>";
                echo "/".$_KAT['MODULE_NAME']."/_kat_name_/_alias_<BR>";
                break;                
                
        case 'content/seo_keywords':
						echo ($_PROJECT['loaded']['last']['data']['seo_keywords']) ? $_PROJECT['loaded']['last']['data']['seo_keywords'] : $_KAT['SEO_KEYWORDS'];
						break;
        case 'content/seo_desc':
						echo ($_PROJECT['loaded']['last']['data']['seo_desc']) ? $_PROJECT['loaded']['last']['data']['seo_desc'] : $_KAT['SEO_DESC'];
						break;
		case "seo_pages":
						if (!empty($_REQUEST['p']) && $_KAT['conf']['SEO_PAGES']) 
							echo " - ���.".$_REQUEST['p'];
					break;
        case 'content/seo_title':
						$seo_title =  $_PROJECT['loaded']['last']['data']['seo_title'];
						if ($seo_title && !empty($_REQUEST['p']) && $_KAT['conf']['SEO_PAGES']) {
							echo $seo_title." - ���.".$_REQUEST['p'];
							break;
						}
						if (!empty($seo_title)) {
							echo $seo_title;
							break;	
						}
        case 'content/title':
                $title        = $_CORE->cmd_exec(array('user','/'.$Cmd));
                $res_title        = $pref. $title; // $pref.
                if (empty($res_title))
                        $res_title        = $_KAT['TITLE'];
                if (!empty($_KAT['RE_TITLE']))
                        $res_title        = $_KAT['RE_TITLE'];

//                echo @substr($res_title,strrpos($res_title,'\\'));   zakomentil, v BSL nado sleshi v tajtle
                echo $res_title;
                break;
		case 'content/fb_tags': 
		//echo '<pre>';
		//var_dump($_PROJECT);die;
				global $_USER,$_KAT, $_CONF;
                $add_part_link="";

                if(!empty($_CONF['FEATURES_USED']['partners']) && !empty($_SESSION['SESS_AUTH']['ID'])) $add_part_link = "?prtn=".$_SESSION['SESS_AUTH']['ID'];	
            				echo 	"<meta property='og:title' content='".$_PROJECT['loaded']['last']['data']['seo_title']."'/>";
            				echo 	"<meta property='og:description' content='".$_PROJECT['loaded']['last']['data']['seo_desc']."'/>";
            				echo	"<meta property='og:type' content='website'/>\n";

                if(!empty($_SERVER['REDIRECT_URL'])){
            		echo	"<meta property='og:url' content='//".$_SERVER['HTTP_HOST'].$_SERVER['REDIRECT_URL'].$add_part_link."'/>\n";
                } else {
                    echo	"<meta property='og:url' content='//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].$add_part_link."'/>\n";
                }
                
        		if(is_file($_SERVER['DOCUMENT_ROOT'].'/data/db/f_'.$_KAT['KUR_ALIAS'].'/'.$_PROJECT['loaded']['last']['data']['doc'])) 
        			echo	"<meta property='og:image' content='//".$_SERVER['HTTP_HOST'].'/data/db/f_'.$_KAT['KUR_ALIAS'].'/'.$_PROJECT['loaded']['last']['data']['doc']."'/>\n";
                elseif(is_file($_SERVER['DOCUMENT_ROOT'].'/data/db/f_'.$_KAT['KUR_ALIAS'].'/'.$_PROJECT['loaded']['last']['data']['doc1'])) 
				    echo	"<meta property='og:image' content='//".$_SERVER['HTTP_HOST'].'/data/db/f_'.$_KAT['KUR_ALIAS'].'/'.$_PROJECT['loaded']['last']['data']['doc1']."'/>\n";
                elseif (is_file($_SERVER['DOCUMENT_ROOT'].'/'.$_USER['og:image']))			
				    echo	"<meta property='og:image' content='//".$_SERVER['HTTP_HOST'].$_USER['og:image']."'/>\n";
                
                echo	"<meta property='og:site_name' content='".$_USER['START_TITLE']."'/>\n";
				if($_KAT['conf']['fb_tags']){ 				
					if (!empty($_PROJECT['loaded']['last']['data']['fb_description']))	echo	"<meta property='og:description' content='".($_PROJECT['loaded']['last']['data']['fb_description'])."'/>\n";
					elseif(($_KAT['conf']['SEO']) && $_PROJECT['loaded']['last']['data']['seo_desc'] && empty($_KAT[$_KAT['KUR_ALIAS']]['NOSEO'])) echo	"<meta property='og:description' content='".($_PROJECT['loaded']['last']['data']['seo_desc'])."'/>\n";
					else echo	"<meta property='og:description' content='".html_entity_decode(substr(str_replace("'", '"', strip_tags($_PROJECT['loaded']['last']['data']['cont'])), 0, 400))."'/>\n";
				}
				break; 
         case 'content/path':

//                        break;

                global $_PROJECT;
                if (empty($_KAT['KUR_ALIAS'])) break;

                $last        = (is_array($_PROJECT['PATH_2_CONTENT'])) ? end($_PROJECT['PATH_2_CONTENT']) : ''; // may be empty
//                echo $last['name'];
//                Ubral, delaja put' v ADMINE, (byli Aliasy)
//                if (!is_array($last) || $last['name'] != $_KAT['TITLE'])
//                        $_PROJECT['PATH_2_CONTENT'][]        = array(
//                                'name'        => $_KAT['TITLE'],
//                                'path'        => '/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS']
//                        );
                if (!empty($_KAT['SUB_TITLE']))
                        $_PROJECT['PATH_2_CONTENT'][]        = array(
                                'name'        => $_KAT['SUB_TITLE'],
                                'path'        => '/'.$_KAT['MODULE_NAME'].'/'.$_KAT['KUR_ALIAS']
                        );

                $title        = $_CORE->cmd_exec(array('user','/'.$Cmd));

               	if (sizeof($_PROJECT['PATH_2_CONTENT'])==0){
               		$name = $_KAT['TITLE'];
					if (Main::comm_inc("path.curr.html",$f,'user')) {
						include $f;
					}
					else{
						echo "".$name."";
					}
               	} else
	               	echo $title;
                break;
        case 'content/title/short':
//                print_r($_PROJECT['PATH_2_CONTENT']);
                $title        = $_CORE->cmd_exec(array('user','/'.$Cmd));
//                echo $pref.(empty($title)) ? $_KAT['TITLE'] : $title;
                $res_title        = $pref. $title; // $pref.
//                echo $title;
                if (!empty($_KAT['SUB_TITLE']))
                        echo $_KAT['SUB_TITLE'];

                else if (empty($res_title))
                        echo $_KAT['TITLE'];
                else
                        echo $res_title;
//                echo (strstr($title, $_KAT['SUB_TITLE']) == $_KAT['SUB_TITLE']) ? '' : $_KAT['SUB_TITLE'];
                break;
        case 'module/title':
        	echo @$_KAT['MODULE_TITLE'];
					if (!empty($_KAT['SRC'][$_KAT['KUR_ALIAS']])) {
						echo " <span> / ".$_KAT['TITLES'][$_KAT['SRC'][$_KAT['KUR_ALIAS']]]."</span>";
					}
			break;
        case 'module/desc':
        	echo @$_KAT['MODULE_DESC'];
			break;
        case "adm_menu": // 17.11.2005

                global $_PROJECT;
                if (!empty($_KAT['ADM_MENU']))
                        $_PROJECT['ADM_MENU']        = array_merge($_PROJECT['ADM_MENU'], $_KAT['ADM_MENU']);  // conf.inc.php
                break;

        case "install":
               if ($_CORE->IS_ADMIN ){
        	    $_KAT['TITLE']        = Main::get_lang_str('install_module_kat', 'db');
                include $_CORE->CoreModDir."/install.php";
               }
                break;
        case "restore_usercat":
               if ($_CORE->IS_ADMIN ){
                  $path=$_REQUEST['path'];
                  $src=$_REQUEST['src'];
                  $str = KAT::get_restore_confuser($path,$src);
                  echo $str;
               }
                break;

        case "add_prefix":
				if ($_CORE->IS_ADMIN && defined('DB_TABLE_PREFIX')){
					$tables = SQL::tlist();
					echo "<pre>";
					print_r($tables);
					foreach ($tables as $table){
						if (empty($table) || strstr($table, DB_TABLE_PREFIX)) continue;
						echo "Renaming <b>$table</b> into ".DB_TABLE_PREFIX.$table."</b><br>";
						SQL::query('ALTER TABLE `'.$table.'` RENAME `'.DB_TABLE_PREFIX.$table.'` ;', DEBUG);
					}
				}
                break;
        case "del_prefix":
				if ($_CORE->IS_ADMIN && defined('DB_TABLE_PREFIX')){
					$tables = SQL::tlist();
					echo "<pre>";
					print_r($tables);
					foreach ($tables as $table){
						if (empty($table)) continue;
						$new_table = preg_replace("/^".DB_TABLE_PREFIX."/", '', $table);
						echo "Renaming <b>$table</b> into ".$new_table."</b><br>";
						SQL::query('ALTER TABLE `'.$table.'` RENAME `'.$new_table.'` ;', DEBUG);
					}
				}
                break;

        case "ren_prefix":

				if ($_CORE->IS_ADMIN && defined('DB_TABLE_PREFIX')){
        			$from = @$_REQUEST['from'];
        			$to = @$_REQUEST['to'];
        			echo $from . " = > ". $to;
					$tables = SQL::tlist();
					echo "<pre>";
					print_r($tables);
					foreach ($tables as $table){
						if (empty($table) || !strstr($table, $from."_")) continue;
						$new_table = preg_replace("/^".$from."_/", $to."_", $table);
						echo "Renaming <b>$table</b> into ".$new_table."</b><br>";
						SQL::query('ALTER TABLE `'.$table.'` RENAME `'.$new_table.'` ;', DEBUG);
					}
				}
                break;

        case "noline":
                $_KAT['NO_PLINE'] = true;

        case "getdata":
                KAT::search('', $FORM_ORDER);
                break;

        case "action":
               include $_CORE->CoreModDir."/admin/kat_admin.php";
               break;

        case "":
        case "search":
               if ($_CORE->IS_ADMIN ){
                       if (KAT::inc_tpl('kat_admin.submenu.inc.php', $f))
                                include $f;
                }
                include ($_CORE->CoreModDir.'/search.inc.php'); // 16.08.2005 _once, chto by neskol'ko zaprosov na odnoj stranice mozhno
                if (!empty($_KAT['ERROR'])) {
                        echo "<P>".$_KAT['ERROR']."</P>";
                }
                break;
        case "any":
				$_KAT['ANY'] = 1;
                include ($_CORE->CoreModDir.'/search.inc.php'); // 16.08.2005 _once, chto by neskol'ko zaprosov na odnoj stranice mozhno
                $_KAT['ANY'] = 0;
				break;
        case 'last/name':
        case 'last/ts':
        case 'last/anons':
        case 'last/alias':
                $what        = basename($Cmd);
                KAT::last($FORM_ORDER, 1);
                echo $_KAT['SEARCH_LAST'][$what];
                break;
        case 'last/reset':
                KAT::last_reset();
                break;
        case 'last':
                KAT::last($FORM_ORDER, 1);
				if(empty($_KAT['SEARCH_LAST']))
					if (KAT::inc_tpl('view.nothing.html', $file)) {
					include "$file";
					}
				$form=$_KAT['SEARCH_LAST'];
				if (KAT::inc_tpl('view.item.html', $file)) {
					include "$file";
				}
				unset($_KAT['SEARCH_LAST']);
                break;
        case "site_order/":
        case "site_order":  //added 01.03.06 by Shesternin
				if( !empty( $FORM_DATA['prior'] ) ){
					$template = ( $_CORE->CLEAR_VERSION )?"/clear":"";
					$wC = empty( $_POST['closeWindow'] )?false:$_POST['closeWindow'];
					if( !empty( $_POST['data_ordering'] ))
					  {  $data = $_POST['data_ordering'];
					     $aliases = explode( ";", $data );
					     $i=0;
					     foreach( $aliases as $al ){
						     {  $i++;
						        SQL::upd( $_KAT['KUR_TABLE'], "prior = '$i'", "( alias = '$al' )" );
						     }
						 }
						 if(isset($_POST['ajax'])){
					     	die("Updated {$i} ������� ");
						 }
					  }
	        	    if (KAT::inc_tpl('order.inc.phtml', $file))
	                    	include "$file";
				}
        	    break;
		case 'index_back': // back			
				if (!isset($FORM_ORDER_FIELD)){
					$FORM_ORDER_FIELD="ts";	
				}	
				$back_db_element = SQL::getrow("*", $_KAT['KUR_TABLE'], "{$FORM_ORDER_FIELD} > '{$_REQUEST['id_last']}' {$FORM_WHERE}", "ORDER	BY {$FORM_ORDER_FIELD} ASC LIMIT 1");	
				unset($_REQUEST['id_last']);
				if(empty($back_db_element))
					if(KAT::inc_tpl('view.nothing.html', $file)) {
					include "$file";	
				}					
				$form = $back_db_element;   
				if (KAT::inc_tpl('view.item.html', $file)) {
					include "$file";
				}
			break;
				
		case 'index_next': // next	
				if (!isset($FORM_ORDER_FIELD)){				
					$FORM_ORDER_FIELD="ts";					
				} 
				$next_db_element = SQL::getrow("*", $_KAT['KUR_TABLE'], "{$FORM_ORDER_FIELD} < '{$_REQUEST['id_next']}' {$FORM_WHERE}", "ORDER	BY {$FORM_ORDER_FIELD} DESC LIMIT 1"); 

				$form = $next_db_element; 

				if(empty($next_db_element)) {
					KAT::last($FORM_ORDER, 1);
					$form=$_KAT['SEARCH_LAST'];
				}

				if (KAT::inc_tpl('view.item.html', $file)) {
					include "$file";
				}
				unset( $next_db_element);
				unset( $form);
			break;


		case 'export2csv': // back			
			KAT::export2csv($_KAT[$_KAT['KUR_ALIAS']]['EXPORT2CSV']['DELIMETER']);
			break;

			
		case 'alias_back': // back	
			if (!isset($FORM_ORDER_FIELD)){				
				$FORM_ORDER_FIELD="ts";					
			} 
			$back_db_element = SQL::getval("alias", $_KAT['KUR_TABLE'], "{$FORM_ORDER_FIELD} > '{$_REQUEST['id_last']}' {$FORM_WHERE}", "ORDER	BY {$FORM_ORDER_FIELD} ASC LIMIT 1"); 
			unset($_REQUEST['id_last']);
			if(!empty($back_db_element)) {
				echo $back_db_element;
			}
		break;		
		
		case 'alias_next': // next	
			if (!isset($FORM_ORDER_FIELD)){				
				$FORM_ORDER_FIELD="ts";					
			} 
			$next_db_element = SQL::getval("alias", $_KAT['KUR_TABLE'], "{$FORM_ORDER_FIELD} < '{$_REQUEST['id_next']}' {$FORM_WHERE}", "ORDER	BY {$FORM_ORDER_FIELD} DESC LIMIT 1"); 
			unset($_REQUEST['id_next']);
			if(!empty($next_db_element)) {
				echo $next_db_element;
			}
		break;
	case "any":
		$data = BAN::get_cont( $alias, '', 'any' );
		BAN::inc_data( $data['alias'], 'view' );
	break;
    case "draft":
        if(!empty($_KAT[$_KAT['KUR_ALIAS']]['DRAFT'])){
			echo "��������� � ������ �������� ��� {$_KAT['KUR_ALIAS']}<br>";
            $form	= @$_REQUEST['form'];
            if( !empty($form) )
            	$_SESSION['DB_DRAFT'][$_KAT['KUR_ALIAS']] = $form;
            if(isset($_REQUEST['clear']))
            	unset($_SESSION['DB_DRAFT'][$_KAT['KUR_ALIAS']]);
        }
    	 echo "<pre>";print_r($_SESSION['DB_DRAFT']);echo "</pre>";
        break;
case "tmp":

if (!$_CORE->IS_ADMIN) die('wrong-wrong-wrong');

// vremennaja zatychka, dlja dobavlenija polja, opisanogo v $FORM_DATA, vo vse tablicy bazy
foreach ($_KAT['TABLES'] as $key=>$tab)if(!empty($key)){
	echo "<P><u>$tab</u>";
    $tab .= '_history';
	// DB
		$FORM_DATA= array (
  'from_group'	=>
  array (
    'field_name' => 'from_group',
    'name' => 'form[from_group]',
    'title' => Main::get_lang_str('data', 'db'),
    'must' => 1,
		'size' => 15,
    'maxlen' => 255,
    'type' => 'textbox',
    'sub_type'  => 'bigint',
    'readonly' => 'true',
		'default'	=> ''
  ),

	);


		// Poluchaem stroku CREATE
		echo "<BR><b>Add to table: </b>";

		$query = "";
		KAT::parse_form_data( $query, $FORM_DATA );
        // ALTER TABLE `kmd__blog` CHANGE `from_group` `from_group` BIGINT NULL DEFAULT NULL;

		$query = substr($query,0,-2);
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . $tab;

		// $query = "ALTER TABLE ".$table." ADD COLUMN (".trim($query, ',').")";
        $query = "ALTER TABLE ".$table."  ADD COLUMN  ".trim($query, ',')."";
		echo $query. " <br>";;

		$res = SQL::query($query,0);
		if (!$res->Result)
			echo $res->ErrorQuery;
	}
/*
foreach ($_KAT['TABLES'] as $key=>$tab)if(!empty($key)){
	echo "<P><u>$tab</u>";
	// DB
		$FORM_DATA= array (
  'hidden'	=>
  array (
    'field_name' => 'hidden',
    'name' => 'form[hidden]',
    'title' => Main::get_lang_str('ne_publ', 'db'),
    'must' => 0,
    'maxlen' => 1,

    'type'=>'hidden',
    'sub_type' => 'varchar',
  )
);


		// Poluchaem stroku CREATE
		echo "<BR><b>Add to table: </b>";

		$query = "";
		KAT::parse_form_data( $query, $FORM_DATA );

		$query = substr($query,0,-2);
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . $tab;

		$query = "ALTER TABLE ".$table." ADD COLUMN (".trim($query, ',').")";
		echo $query. " <br>";;

		$res = SQL::query($query,0);
		if (!$res->Result)
			echo $res->ErrorQuery;
	}

foreach ($_KAT['TABLES'] as $key=>$tab)if(!empty($key)){
	echo "<P><u>$tab</u>";
	// DB
		$FORM_DATA= array (
  'alias'	=>
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => 'alias',
    'must' => 0,
    'maxlen' => 128,

    'type'=>'hidden',
    'sub_type' => 'varchar',
  )
);


		// Poluchaem stroku CREATE
		echo "<BR><b>Add to table: </b>";

		$query = "";
		KAT::parse_form_data( $query, $FORM_DATA );

		$query = substr($query,0,-2);
		$table = (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . $tab;

		$query = "ALTER TABLE ".$table." ADD COLUMN (".trim($query, ',').")";
		echo $query. " <br>";;

		$res = SQL::query($query,0);
		if (!$res->Result)
			echo $res->ErrorQuery;
	}
*/
        	break;
		case "nothing":
			// Nizhchego prosto podkljuchit' modul'
			break; 
            
    	case "matrix_edit":
			if ( $_CORE->IS_ADMIN && !empty($_REQUEST['id']) && !empty($_REQUEST['dbname'])) {
				
				if (isset($_POST['form']) && is_array($_POST['form'])){
                    KAT::save_matrix($_POST['form'], $_REQUEST['dbname'], $_REQUEST['id']);
                    echo '<script language="javascript" type="text/javascript">
                    	if( window.opener != null ){	
                			self.close();
                		}
                    </script>';
					die();
				}
                if (KAT::inc_tpl('head.matrix_form.inc.html', $f))
                    include $f;
                $res	= SQL::sel(' * ', DB_TABLE_PREFIX.$_REQUEST['dbname'], " item_id = ".$_REQUEST['id'], '', DEBUG);
                $data = array();
        		if ($res->NumRows > 0) {
        			for ($i = 0; $i < $res->NumRows; $i++ ) {
        				$res->FetchArray($i);
        				$arr = $res->FetchArray;
                        $data[$arr['id_db1']][$arr['id_db2']] = $arr['value'];
        			}
        		}
                
                $resX	= SQL::sel(' * ', DB_TABLE_PREFIX.$_REQUEST['dbnamex'], '', 'order by name', DEBUG);
                $dataX = array();
        		if ($resX->NumRows > 0) {
        			for ($i = 0; $i < $resX->NumRows; $i++ ) {
        				$resX->FetchArray($i);
        				$dataX[]	= $resX->FetchArray;
        			}
        		}
                $resY	= SQL::sel(' * ', DB_TABLE_PREFIX.$_REQUEST['dbnamey'], '', 'order by name', DEBUG);
                $dataY = array();
        		if ($resY->NumRows > 0) {
        			for ($i = 0; $i < $resY->NumRows; $i++ ) {
        				$resY->FetchArray($i);
        				$dataY[]	= $resY->FetchArray;
        			}
        		}
                if (KAT::inc_tpl('form.matrix_form.inc.html', $f))
                    include $f;
			}
			break;
			
    	case "mass_add":
			if ( $_CORE->IS_ADMIN ) {
                if (KAT::inc_tpl('head.mass_form.inc.html', $f))
                    include $f;
                else
                	include $_CORE->CoreModDir."/head.mass_form.inc.html";
				if (isset($_POST['doc']) && is_array($_POST['doc']) && isset($_POST['name']) && is_array($_POST['name'])) {
                    $add_upd = '';
                    if (isset($_POST['form']) && is_array($_POST['form'])) {
                        foreach ($_POST['form'] as $key => $value){
                            $key = mysql_escape_string($key);
                            $value = mysql_escape_string($value);
                            $add_upd .= "`".$key ."`" . "='" . $value . "', ";
                        }
                    }
					// sozdali dannye dlja obnovlenija UPDATE
					foreach ($_POST['doc'] as $k=>$v){
						$name = addslashes($_POST['name'][$k]);
						$v = addslashes($v);
						if (!empty($name))
							$upd = $_KAT['MASS_UPLOADER']['description'] . " = '{$name}', ".$add_upd;
						SQL::upd($_KAT['KUR_TABLE'], $upd . " new = '0'", $_KAT['MASS_UPLOADER']['doc']." = '{$v}'");
					}
					if (KAT::inc_tpl('after_edit.html', $file)) {
						include "$file";
					}
					$add = (empty($_CORE->CLEAR_VERSION)) ? '' : '/clear';
					header("Location: $add/".$_KAT[$_KAT['KUR_ALIAS']]['AFTER_ADD']);
					die();
				}

				if (KAT::inc_tpl('mass_form.phtml', $f))
                    include $f;
                else
					include $_CORE->CoreModDir."/admin/mass_form.phtml";
			}
			break;
			
		case "mass/uploadify":
		
			if(isset($_REQUEST['SID']) && !empty($_REQUEST['SID'])) {session_id($_REQUEST['SID']); session_start();}
			if ( !empty($_FILES) && $_CORE->IS_ADMIN ) {
				$tempFile 		= $_FILES['Filedata']['tmp_name'];
				$targetPath 	= $_SERVER['DOCUMENT_ROOT'].KAT::get_data_link( '/f_'.$_KAT['KUR_ALIAS'], $dir, KAT_LOOKIG_DATA_DIR ). '/';
				$alias 			= gmmktime().rand(1000,9999);
				$newName 		= $alias . "." . pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);
				$date 			= date('Y-m-d');
				$targetFile 	= str_replace('//','/',$targetPath) . $newName;
				
				$field = $FORM_DATA[$_KAT['MASS_UPLOADER']['doc']];
				if(!empty($field['hardwidth']) || !empty($field['hardheight'])){
					KAT::resizefoto($tempFile, $field['hardwidth'], $field['hardheight']);
				}
				
				if ( move_uploaded_file($tempFile,$targetFile) ){
					$res = SQL::ins($_KAT['KUR_TABLE'], $_KAT['MASS_UPLOADER']['description'].", ".$_KAT['MASS_UPLOADER']['doc'].", alias, new, ts",
												 "'".pathinfo($_FILES['Filedata']['name'], PATHINFO_FILENAME )."', '{$newName}', '{$alias}', '1' , '{$date}'", DEBUG);
					if ($res->Result) echo "inserted";
				}
			}
		break;

		case "mass/getnewfoto";
			if ( $_CORE->IS_ADMIN ) {
                if (is_file($_CORE->SiteModDir.'/'.$_KAT['FILES'][(string)$tab])) {
                    include $_CORE->SiteModDir.'/admin/'. (string)$tab . '.newfoto.phtml';
                }else{
					include $_CORE->CoreModDir."/admin/show_newfoto.phtml";
                }
			}
		break;

    	case "mass/delfoto":
			if ( $_CORE->IS_ADMIN && !empty($_POST['doc']) ) {
				SQL::del( $_KAT['KUR_TABLE'], $_KAT['MASS_UPLOADER']['doc']." = '{$_POST['doc']}' " );
				$directory = $_SERVER['DOCUMENT_ROOT'].KAT::get_data_link( '/f_'.$_KAT['KUR_ALIAS'], $dir, KAT_LOOKIG_DATA_DIR );
				if ( is_file("$directory/$doc") ) {
					unlink("$directory/$doc");
				}
			}
			die();
			break;
				
    	case 'rss_link':
    			$rss_link ='';
    			foreach($_KAT['rss'] as $k => $v)
    			{
    			
    				if($v){
    					$rss_link = '<link rel="alternate" type="application/rss+xml" title="'.$_KAT['TITLES'][$k].' RSS Feed" href="//'.$_SERVER['HTTP_HOST'].'/db/'.$k.'/?rss" />'."\n";
    					echo $rss_link;
    				}
    			}
    		break;
        case 'none': break;
        case "add":
        default:
		list($Sub, $SubCmd)        = $_CORE->cmd_parse($Cmd);
                // Esli nado odno pole, no ne poslednego
                if ($Sub == 'field') {

                	$_KAT['searching'] = true;

									if (is_file($_CORE->SiteModDir.'/'.$_KAT['FILES'][(string)$tab])) {
										include $_CORE->SiteModDir.'/'.$_KAT['FILES'][(string)$tab];
									}elseif($_CORE->CoreModDir.'/_'.$_KAT['FILES'][(string)$tab]){
										include $_CORE->CoreModDir.'/_'.$_KAT['FILES'][(string)$tab];
									}
									KAT::get_cont(substr(dirname($SubCmd),1), basename($SubCmd) );
                	$_KAT['searching'] = false;
                // Esli nado poslednee
                }elseif($Sub == 'last'){
                    
                        list($num, $field)        = $_CORE->cmd_parse($SubCmd);
                        if (empty($field)||$field == '/') {// togda prosto v dannye zagruzit' poslednih $num zapisej
                                KAT::last_reset(); // pohozhe nado obnuljat'
                                
                                KAT::last($FORM_ORDER, $num, 0);
                        }else {
                            
                                KAT::last($FORM_ORDER, 1, $num+1); // +1 dlja shodstva s doctxt
                                echo $_KAT['SEARCH_LAST'][basename($field)];
                        }
                // put' do fajlov
                }elseif($Sub == 'path') {
                        echo $_CORE->DIRMODS."f_".$_KAT['KUR_ALIAS'];

                // put' do fajlov
                }elseif($Sub == 'curr') {
                        echo @$_KAT[$SubCmd];
                        // Esli ves' dokument
				}else{
			
                         //esli net takogo modulja, to v aut
                        if (empty($_KAT['TABLES'][(string)$tab])) { 
						/*	echo "������������� � ������� ������������� ����� ����������� �� ���� ������. <br />
									� ��� ����� ������ �� �� ���������.";*/
							if( !$_CORE->IS_ADMIN && $_KAT['conf']['NOTIF'] ){
								if (!$_CORE->dll_load('class.lMail'))
									die ($_CORE->error_msg());
								$mail_err = array(
									'subject' => '���������� ������ �� ����� '.$_SERVER['HTTP_HOST'],
									'name' => 'robot',
									'email' => 'robot@'.$_SERVER['HTTP_HOST'],
									'body'  => '���������� ������ �� ����� '.$_SERVER['HTTP_HOST'].':'."\n\r".'�� ������� ������� - '.$tab."\n\r ������ ��������� �� �������� ".$_SERVER['HTTP_REFERER']."\n\r ������� ".$_SERVER['REQUEST_URI'],
									'mailto' => 'denis.ulyusov.startupmilk@gmail.com;service@startupmilk.com'
								);
								
								lMail::send( $mail_err );
							}
							// ��� ���������� ��������, �������� �����, ���� ��� ������ - ������ 404 � �������, ����� �������� 24.12.2012 (������ exit �� return)
							include $_SERVER['DOCUMENT_ROOT']."/404.html";
//							exit;
							return;
                        }
                        if ($_CORE->IS_ADMIN || !empty($_KAT[$_KAT['KUR_ALIAS']]['PERM']) ){ // admin ili dopolnitel'nye prava
                                if (isset($_GET['edit'])
                                        || isset($_GET['del'])
                                        || isset($_GET['import'])
                                        || isset($_GET['export'])
                                        || isset($_GET['empty'])
                                        || isset($_GET['show'])
                                        || isset($_GET['hide'])
                                        || isset($_GET['udel'])
                                        ) {
                                        include $_CORE->CoreModDir."/admin/kat_admin.php";
                                        break;
                                }elseif (false)
                                        if (KAT::inc_tpl('kat_admin.submenu.inc.php', $f))
                                                include $f;
                        }

                        $data        = KAT::get_cont($Cmd);
        //                $_KAT['TITLE']        = (!empty($_KAT['TITLE'])) ? $_KAT['TITLE'] : Main::short($data['name'], TITLE_LEN, 3, ' ');
                        $_KAT['SUB_TITLE']        = Main::short(is_string($data['name'])?$data['name']:'', TITLE_LEN, 3, ' ');
               }
}
?>
