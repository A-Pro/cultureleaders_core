<?

$FORM_ORDER	= 'ORDER BY alias DESC';

$FORM_WHERE	= 'AND (hidden != 1 OR hidden IS NULL)';
 
if (!$_CORE->IS_ADMIN && $_SESSION['SESS_AUTH']['ID']){
    $FORM_WHERE	= "AND (from_auth = '".$_SESSION['SESS_AUTH']['ID']."' OR to_auth = '".$_SESSION['SESS_AUTH']['ID']."')";
}

if (isset($_REQUEST['form']) && is_array($_REQUEST['form'])){
    $_REQUEST['form']['ts'] = date('Y-m-d');
    $_REQUEST['form']['from_auth'] = $_SESSION['SESS_AUTH']['ID'];
    $_REQUEST['form']['name'] = time();
}



$FORM_DATA= array (
  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
		'default' => time()
  ),
  'alias' => 
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => 'alias',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
		'default' => time()
  ),
  'from_auth' => 
  array (
    'field_name' => 'from_auth',
    'name' => 'form[from_auth]',
    'title' => '�� ����',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
    'default' => $_SESSION['SESS_AUTH']['ID']
  ),
  'to_auth' => 
  array (
    'field_name' => 'to_auth',
    'name' => 'form[to_auth]',
    'title' => '����',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'db1' => 
  array (
    'field_name' => 'db1',
    'name' => 'form[db1]',
    'title' => '����� ������� 1',
    'must' => 0,
    'maxlen' => 256,
    'type' => 'hidden',
    'default' => $_CONF['DIALOG']['db1']
  ),
  'db2' => 
  array (
    'field_name' => 'db2',
    'name' => 'form[db2]',
    'title' => '����� ������� 2',
    'must' => 0,
    'maxlen' => 256,
    'type' => 'hidden',
    'default' => $_CONF['DIALOG']['db2']
  ),
  'db1_id'	=>
  array (
    'field_name' => 'db1_id',
    'name' => 'form[db1_id]',
    'title' => ''.$_KAT['TITLES'][$_CONF['DIALOG']['db1']],
	  'must' => 1,
	  'style'	=> 'width:100%',
	  'maxlen' =>	20,
		'default' => $_REQUEST['form']['db1_id'],
		'type' => 'select_from_table',
		'ex_table' => DB_TABLE_PREFIX.$_KAT['TABLES'][$_CONF['DIALOG']['db1']],
		'id_ex_table' => 'id',
		'ex_table_field' => 'name',
		'ex_table_where' => ' `from_auth`='.$_SESSION['SESS_AUTH']['ID'].' ',
  ),
  'db2_id'	=>
  array (
    'field_name' => 'db2_id',
    'name' => 'form[db2_id]',
    'title' => ''.$_KAT['TITLES'][$_CONF['DIALOG']['db2']],
	  'must' => 1,
	  'style'	=> 'width:100%',
	  'maxlen' =>	20,
		'default' => $_REQUEST['form']['db2_id'],
		'type' => 'select_from_table',
		'ex_table' => DB_TABLE_PREFIX.$_KAT['TABLES'][$_CONF['DIALOG']['db2']],
		'id_ex_table' => 'id',
		'ex_table_field' => 'name',
		'ex_table_where' => ' `from_auth`='.$_SESSION['SESS_AUTH']['ID'].' ',
  ),
  'name'	=>
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => '��������',
    'style'	=> 'width:100%',
    'must' => 1,
	'size' => 50,
    'maxlen' => 255,
    'type' => 'hidden',
		'default' => time()
  ),
  'price'	=>
  array (
    'field_name' => 'price',
    'name' => 'form[price]',
    'style'	=> 'width:100%',
    'title' => '����',
    'must' => 0,
		'size' => 50,
    'maxlen' => 255,
    'type' => 'textbox'
  ),
  'comment' => 
  array (
    'field_name' => 'comment',
    'name' => 'form[comment]',
    'title' => Main::get_lang_str('mess', 'db'),
    'style'	=> 'width:100%',
    'must' => 0,
    'maxlen' => '5120',
    'type' => 'textarea',
    'cols' => '40',
    'rows' => '5',
		'logic' => 'OR',
		'search' => " LIKE '%%s%%'"
  ),
  'ts'	=>
  array (
    'field_name' => 'ts',
    'name' => 'form[ts]',
    'title' => Main::get_lang_str('data', 'db'),
    'must' => 1,
	'size' => 15,
    'maxlen' => 255,
    'type' => 'hidden',
    'readonly' => 'true',
	'default'	=> date('Y-m-d')
  ),
	'agree' =>  // dlja standarta
  array (
    'field_name' => 'agree',
    'name' => 'form[agree]',
    'title' => '������������',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'checkbox',
		'default' => 0
  ),
	'alias' =>  // dlja standarta
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => Main::get_lang_str('alias', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
		'default' => time()
  ),
	'hidden' =>  // dlja standarta
  array (
    'field_name' => 'hidden',
    'name' => 'form[hidden]',
    'title' => Main::get_lang_str('ne_publ', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
		'default' => 0
  )
);
if(DB_TABLE_PREFIX.$_REQUEST['form']['kur_alias'] === $FORM_DATA['db2_id']['ex_table'] ){
	$FORM_DATA['db2_id']['type'] = 'hidden'; 
//	$_REQUEST['form']['db2_id'] = $_REQUEST['form']['kur_id'];
}
if(DB_TABLE_PREFIX.$_REQUEST['form']['kur_alias'] === $FORM_DATA['db1_id']['ex_table'] ){
    $FORM_DATA['db1_id']['type'] = 'hidden'; 
//    $_REQUEST['form']['db1_id'] = $_REQUEST['form']['kur_id'];
}
if($_REQUEST['form']['kur_alias'] === $_KAT['KUR_ALIAS']){
    $FORM_DATA['db1_id']['readonly'] = 'true'; 
		if (!empty($_REQUEST['form']['db1_id'])) $FORM_DATA['db1_id']['type'] = 'hidden'; 
    $FORM_DATA['db2_id']['readonly'] = 'true'; 
		if (!empty($_REQUEST['form']['db2_id'])) $FORM_DATA['db2_id']['type'] = 'hidden'; 
}
?>