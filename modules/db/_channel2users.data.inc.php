<?
$prefix = defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '';
$FORM_ORDER	= '  ';
$FORM_WHERE = '';

$FORM_DATA= array ( 
  'name'	=>
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => Main::get_lang_str('name', 'db'),
    'must' => 1,
	'style'	=> 'width:100%',
    'type' => 'textbox',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'alias' => 
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => Main::get_lang_str('alias', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'ts' => 
  array (
    'field_name' => 'ts',
    'name' => 'form[ts]',
    'title' => Main::get_lang_str('ts', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'link'	=>
  array (
    'field_name' => 'link',
    'name' => 'form[link]',
    'title' => '������ �� �����',
    'must' => 1,
	'style'	=> 'width:100%',
    'type' => 'textbox',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
  'channel'	=>
  array (
    'field_name' => 'channel',
    'name' => 'form[channel]',
    'title' => '�����',
    'must' => 1,
	'style'	=> 'width:100%',
    'type' => 'textbox',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
  'user_id'	=>
  array (
    'field_name' => 'user_id',
    'name' => 'form[user_id]',
    'title' => '������������',
    'must' => 0,
	'style'	=> 'width:100%',
    'type' => 'hidden',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
  'hidden'	=>
  array (
    'field_name' => 'hidden',
    'name' => 'form[hidden]',
    'title' => '��������� ����� ����',
    'must' => 0,
    'maxlen' => 1,
    'type' => 'checkbox',
		'sub_type' => 'varchar'
  ),
  'show_main'	=>
  array (
    'field_name' => 'show_main',
    'name' => 'form[show_main]',
    'title' => "�������� �� �������",
    'must' => 0,
    'maxlen' => 1,
    'type' => 'checkbox',
		'sub_type' => 'varchar'
  ),
	'cont' => 
	array (
		'field_name' => 'cont',
		'name' => 'form[cont]',
		'title' => '������ ��������',
		'must' => '0',
		'maxlen' => '65535',
		'type' => 'textarea',
		'style' => 'width:100%',
		'rows' => '30',
		'wysiwyg'	=> 'tinymce',
	),
)
?>