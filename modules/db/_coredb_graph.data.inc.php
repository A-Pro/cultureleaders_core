<?
$FORM_ORDER	= '  ';
if (!$_CORE->IS_ADMIN)
	$FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL)";


$FORM_DATA= array (
  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'ts'	=>
  array (
    'field_name' => 'ts',
    'name' => 'form[ts]',
    'title' => Main::get_lang_str('data', 'db'),
    'must' => 1,
  	'size' => 15,
    'maxlen' => 255,
    'type' => 'hidden',
  	'default'	=> date('Y-m-d')
  ),
  'alias' => 
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => Main::get_lang_str('alias', 'db'),
    'must' => 0,
    'maxlen' => 255,
    'type' => 'hidden',
  ),
  'name'	=>
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => Main::get_lang_str('name', 'db'),
    'must' => 1,
    'style'	=> 'width:100%',
    'maxlen' => 255,
    'type' => 'textbox'
  ),
   'coord_x' =>	
	array	(
		'field_name' =>	'coord_x',
		'name' =>	'form[coord_x]',
		'title'	=> '���������� �',
    'must' => 1,
  	'size' => 15,
    'type' => 'date',
    'readonly' => 'true',
  	'default'	=> date('Y-m-d'),
    'graph' =>  'coord-x' 
	),
  'coord_y'	=>
  array (
    'field_name' => 'coord_y',
    'name' => 'form[coord_y]',
    'title' => '���������� Y',
    'must' => 1,
    'style'	=> 'width:100%',
    'maxlen' => 255,
    'type' => 'textbox',
    'graph' => 'coord-y'
  ),
  'hidden'	=>
  array (
    'field_name' => 'hidden',
    'name' => 'form[hidden]',
    'title' => Main::get_lang_str('ne_publ', 'db'),
    'must' => 0,
    'maxlen' => 1,
    'type' => 'checkbox',
  ),
  
)
?>