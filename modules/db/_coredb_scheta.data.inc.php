<?
if (empty($_SESSION['SESS_AUTH']['ID'])) {
	header("Location: /");
	exit;
}

$FORM_ORDER	=	'	ORDER	BY ts desc ';

if (!$_CORE->IS_ADMIN) {
	$FORM_WHERE = "AND (hidden != 1 OR hidden IS NULL) AND from_auth = '".$_SESSION['SESS_AUTH']['ID']."'";
}

$FORM_DATA=	array	(
	'id' =>	
	array	(
		'field_name' =>	'id',
		'name' =>	'form[id]',
		'title'	=> 'id',
		'must' =>	0,
		'maxlen' =>	20,
		'type' =>	'hidden',
		'default' => md5(time()),
	),
	'alias'	=> 
	array	(
		'field_name' =>	'alias',
		'name' =>	'form[alias]',
		'title'	=> 'alias',
		'must' =>	0,
		'maxlen' =>	20,
		'type' =>	'hidden',
	),
    'name'	=>
    array	(
    	'field_name' =>	'name',
    	'name' =>	'form[name]',
    	'title'	=> '�����',
    	'must' =>	1,
			'style'	=> 'width:100%',
    	'maxlen' =>	255,
    	'type' =>	'textbox',
			'default' => date("dmYi"),
    	'logic'	=> 'OR',
    	'search' =>	"	LIKE '%%%s%%'",
    ),
    'who'	=>
    array	(
    	'field_name' =>	'who',
    	'name' =>	'form[who]',
    	'title'	=> '����������',
    	'must' =>	1,
    	'style'	=> 'width:100%',
    	'maxlen' =>	255,
    	'type' =>	'textbox',
    	'logic'	=> 'OR',
    	'search' =>	"	LIKE '%%%s%%'",
        'default'   => $_REQUEST['who'],
    ),
    'what'	=>
    array	(
    	'field_name' =>	'what',
    	'name' =>	'form[what]',
    	'title'	=> '�� ���',
    	'must' =>	1,
    	'style'	=> 'width:100%',
    	'maxlen' =>	255,
    	'type' =>	'textbox',
    	'logic'	=> 'OR',
    	'search' =>	"	LIKE '%%%s%%'",
        'default'   => $_REQUEST['what'],
    ),
    'money'	=>
    array	(
    	'field_name' =>	'money',
    	'name' =>	'form[money]',
    	'title'	=> '����� (�.)',
    	'must' =>	1,
    	'style'	=> 'width:100%',
    	'maxlen' =>	255,
    	'type' =>	'textbox',
    	'logic'	=> 'OR',
    	'search' =>	"	LIKE '%%%s%%'",
        'default'   => $_REQUEST['money'],
    ),
	'ts'	=>
  array (
    'field_name' => 'ts',
    'name' => 'form[ts]',
    'title' => '����',
    'must' => 0,
		'size' => 15,
    'maxlen' => 255,
    'type' => 'hidden',
    'readonly' => 'true',
		'default'	=> date('Y-m-d H:i:s')
  ),
	'hidden'	=>
	array	(
		'field_name' =>	'hidden',
		'name' =>	'form[hidden]',
		'title'	=> Main::get_lang_str('ne_publ', 'db'),
		'must' =>	0,
		'maxlen' =>	1,
		'type' =>	'hidden',
	),
	'from_auth'	=>
	array	(
		'field_name' =>	'from_auth',
		'name' =>	'form[from_auth]',
		'title'	=> '�� ����',
		'must' =>	1,
		'size' =>	50,
		'maxlen' =>	255,
		'type' =>	'hidden',
		'sub_type' =>	'bigint',	// ����� ������	�� ������	�	�������	UNIQUE
		'default'	=> $_SESSION['SESS_AUTH']['ID'],
  )
)
?>