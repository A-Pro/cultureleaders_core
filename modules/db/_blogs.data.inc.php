<?
$FORM_ORDER	= 'ORDER BY id desc';
$FORM_FROM	= '';
$FORM_BEFORE	= '';
$FORM_WHERE	= 'AND (hidden != 1 OR hidden IS NULL)'; // objazatel'no esli gde-libo ispol'zuetsja

$FORM_DATA= array (
  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'pid' => 
  array (
    'field_name' => 'pid',
    'name' => 'form[pid]',
    'title' => 'pid',
    'must' => 1,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
	'alias' =>  // dlja standarta
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => Main::get_lang_str('alias', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
	'hidden' =>  // dlja standarta
  array (
    'field_name' => 'hidden',
    'name' => 'form[hidden]',
    'title' => Main::get_lang_str('ne_publ', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
		'default' => 0,
  ),
  'name'	=>
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => Main::get_lang_str('title', 'db'),
    'style'	=> 'width:100%',
    'must' => 1,
	'size' => 50,
    'maxlen' => 255,
    'type' => 'textbox',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
  'cont' => 
  array (
    'field_name' => 'cont',
    'name' => 'form[cont]',
    'title' => Main::get_lang_str('about', 'db'),
    'style'	=> 'width:100%',
    'must' => 1,
    'maxlen' => '5120',
    'type' => 'textarea',
    'cols' => '40',
    'rows' => '5',
	'wysiwyg'	=> 'tinymce',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
 'tags'	=>
	array	(
		'field_name' =>	'tags',
		'name' =>	'form[tags]',
		'title'	=> Main::get_lang_str('teg', 'db'),
		'must' =>	0,
		'style'	=> 'width:100%',
		'maxlen' =>	255,
		'type' =>	'textbox',
		'logic'	=> 'OR',
		'search' =>	"	LIKE '%%%s%%'",
	),

)
?>