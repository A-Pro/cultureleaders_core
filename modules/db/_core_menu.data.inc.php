<?
$FORM_WHERE = '';
$FORM_ORDER	=	' ORDER BY prior asc, id asc ';
if (!$_CORE->IS_DEV){
	$FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL)";
}
$_KAT['core_menu']['NOSEO'] = true;
$FORM_DATA=	array	(
	'id' =>	
	array	(
		'field_name' =>	'id',
		'name' =>	'form[id]',
		'title'	=> 'id',
		'must' =>	0,
		'maxlen' =>	20,
		'type' =>	'hidden',
	),
	'alias'	=> 
	array	(
		'field_name' =>	'alias',
		'name' =>	'form[alias]',
		'title'	=> 'alias',
		'must' =>	0,
		'maxlen' =>	20,
		'type' =>	'hidden',
	),
	'parent_id' => 
	array (
		'field_name' => 'parent_id',
		'name' => 'form[parent_id]',
		'title' => '������������ ����',
		'must' => 0,
		'maxlen' => 6,
		'type' => 'select_from_table',
		'ex_table' => DB_TABLE_PREFIX.'core_menu',
		'id_ex_table' => 'id',
		'ex_table_field' => 'name'
	),
	'name'	=>
	array	(
		'field_name' =>	'name',
		'name' =>	'form[name]',
		'title'	=> '��������',
		'must' =>	1,
		'style'	=> 'width:100%',
		'maxlen' =>	255,
		'type' =>	'textbox',
		'logic'	=> 'OR',
		'search' =>	"	LIKE '%%%s%%'",
	),
	'icon' => 
	array (
		'field_name' => 'icon',
		'name' => 'form[icon]',
		'title' => '������',
		'must' => '0',
		'maxlen' => '255',
		'type' => 'radiobtn',
		'arr'	=> array( ''=> '��� ������', 'icon-th'=>'<i class="icon-th"></i>', 'icon-copy'=>'<i class="icon-copy"></i>', 'icon-external-link'=>'<i class="icon-external-link"></i>' ),
		'style' => 'width:100%'
	),
	'link'	=>
	array	(
		'field_name' =>	'link',
		'name' =>	'form[link]',
		'title'	=> '������',
		'must' =>	0,
		'style'	=> 'width:100%',
		'maxlen' =>	255,
		'type' =>	'textbox'
	),
	'access' => 
	array (
		'field_name' => 'access',
		'name' => 'form[access]',
		'title' => '������',
		'must' => '0',
		'maxlen' => '255',
		'type' => 'select',
		'arr'	=> array( 0 => 'Admin', 1=>'Developer'),
		'style' => 'width:100%'
	),
	'prior'	=>
	array	(
		'field_name' =>	'prior',
		'name' =>	'form[prior]',
		'title'	=> '������� ����������',
		'must' =>	0,
		'maxlen' =>	255,
		'type' =>	'hidden'
	),
	'hidden'	=>
	array	(
		'field_name' =>	'hidden',
		'name' =>	'form[hidden]',
		'title'	=> Main::get_lang_str('ne_publ', 'db'),
		'must' =>	0,
		'maxlen' =>	1,
		'type' =>	'checkbox',
	),
)
?>