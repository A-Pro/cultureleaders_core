<?

$FORM_ORDER	= '	ORDER BY name desc';

$FORM_DATA=	array (
  'id' => 
  array	(
	'field_name' =>	'id',
	'name' => 'form[id]',
	'title'	=> 'id',
	'must' => 0,
	'maxlen' =>	20,
	'type' => 'hidden',
  ),
  'alias' => 
  array	(
	'field_name' =>	'alias',
	'name' => 'form[alias]',
	'title'	=> Main::get_lang_str('alias', 'db'),
	'must' => 0,
	'maxlen' =>	20,
	'type' => 'hidden',
  ),
 'name'	=>
  array	(
	'field_name' =>	'name',
	'name' => 'form[name]',
	'title'	=> Main::get_lang_str('name', 'db'),
	'must' => 1,
	'style'	=> 'width:100%',
	'maxlen' =>	255,
	'type' => 'textbox',
	'logic'	=> 'OR',
	'search' =>	" LIKE '%%%s%%'",
  ),
 'ts'	=>
  array	(
	'field_name' =>	'ts',
	'name' => 'form[ts]',
	'title'	=> Main::get_lang_str('data', 'db'),
	'must' => 1,
	'size' => 15,
	'maxlen' =>	255,
	'type' => 'hidden',
	'readonly' => 'true',
	'default'	=> date('Y-m-d')
  ),
'doc1' => array(
	'field_name' =>	'doc1', // dolzhno sovpadat' s 'name'!!!
	'name'	=> 'doc1',
	'title'	=> Main::get_lang_str('avatar', 'db'),
	'maxwidth' => 200,
	'must' => 0,
	'type'	=> 'photo',
	'sub_type'	=> 'photo',
	'newname_func'	=> 'get_file_name("ava")',
//			'path'	=> '/modules/'.$_KAT['MODULE_NAME'].'/f_'.$_KAT['KUR_ALIAS'],
//			'abspath'	=> $_CORE->SiteModDir.'/f_'.$_KAT['KUR_ALIAS'],
	'path'	=> KAT::get_data_link( '/f_'.$_KAT['KUR_ALIAS'], $dir, KAT_LOOKIG_DATA_DIR ),
	'abspath'	=> KAT::get_data_path('/f_'.$_KAT['KUR_ALIAS'],	$dir, KAT_LOOKIG_DATA_DIR),
	),
'doc' => array(
	'field_name' =>	'doc', // dolzhno sovpadat' s 'name'!!!
	'name'	=> 'doc',
	'title'	=> Main::get_lang_str('pdf', 'db'),
	'must' => 0,
	'type'	=> 'photo',
	'sub_type'	=> 'doc',
	'newname_func'	=> 'get_file_name()',
//			'path'	=> '/modules/'.$_KAT['MODULE_NAME'].'/f_'.$_KAT['KUR_ALIAS'],
//			'abspath'	=> $_CORE->SiteModDir.'/f_'.$_KAT['KUR_ALIAS'],
	'path'	=> KAT::get_data_link( '/f_'.$_KAT['KUR_ALIAS'], $dir, KAT_LOOKIG_DATA_DIR ),
	'abspath'	=> KAT::get_data_path('/f_'.$_KAT['KUR_ALIAS'],	$dir, KAT_LOOKIG_DATA_DIR),
	),
 'hidden'	=>
  array	(
	'field_name' =>	'hidden',
	'name' => 'form[hidden]',
	'title'	=> Main::get_lang_str('ne_publ', 'db'),
	'must' => 0,
	'maxlen' =>	1,
	'type' => 'checkbox',
  ),
)
?>