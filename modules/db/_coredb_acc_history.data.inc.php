<?php
if (empty($_SESSION['SESS_AUTH']['ID'] )) { header ("Location: /auth/login"); exit; }
/**
 * Компании от которых вдеется деятельность, используется в финансовом блоке плюс договорные отношения с Контрагентами 
 * 
 */
global $FORM_FIELD4ALIAS,$_ACCESS, $_KAT, $FORM_FROM;
$FORM_WHERE = '';
$FORM_ORDER	= '';
$FORM_FIELD4ALIAS = 'id';


// переопределим
$_KAT['TABLES']['acc_history']   = mysql_real_escape_string($_REQUEST['type']);
$_KAT['KUR_TABLE']  = DB_TABLE_PREFIX.$_KAT['TABLES']['acc_history'];
$FORM_FROM   = '_history';

if(!$_CORE->IS_ADMIN)
    $FORM_ORDER = " ORDER BY ts desc ";

if (!$_CORE->IS_ADMIN)
	$FORM_WHERE	= $_ACCESS->get_where( $_KAT['TABLES']['acc_history'] , false); //" AND (from_auth = '".$_SESSION['SESS_AUTH']['ID']."' " . $FROM_WHERE_ACCESS . ") AND (hidden != 1 OR hidden IS NULL)";

// для таблиц админа
// $_KAT[$_KAT['KUR_ALIAS']]['admin_search']	= array( 'add_qualif', 'fedsubject_id', 'from_auth' ); // для db/list
// $_KAT[$_KAT['KUR_ALIAS']]['admin_fields']	= array( 'Доп.' => 'add_qualif', "Суб.Фед." => 'fedsubject_id', 'Польз' => 'from_auth' ); 
/////////////////

$FORM_DATA= array (
    'id' => array (
        'field_name' => 'id',
        'name' => 'form[id]',
        'title' => 'id',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
        'default' => time(),
    ),
    'rec_id' => array (
        'field_name' => 'rec_id',
        'name' => 'form[rec_id]',
        'title' => 'rec_id',
        'must' => 0,
        'maxlen' => 255,
        'type' => 'hidden',
        //'default' => md5('some'.time()),
    ),
    'changes' =>     array (
        'field_name' => 'changes',
        'name' => 'form[changes]',
        'title' => 'Описание',
        'must' => '0',
        'maxlen' => '65535',
        'type' => 'textarea',
    	'style' => 'width:100%',
        'rows' => '10',
        'logic' => 'OR',
        'placeholder'   => 'Описание',
        'search' => " LIKE '%%%s%%' ",
    ), 
  
    'ts' => array	(
        'field_name' =>	'ts',
        'name' =>	'form[ts]',
        'title'	=> 'Дата',
        'must' =>	0,
        'size' =>	15,
        'maxlen' =>	255,
        'type' =>	'hidden',
        'readonly' =>	'true',
        'default'	=> date('Y-m-d H:i')
    ),


    'from_auth' => array (
        'field_name' => 'from_auth',
        'name' => 'form[from_auth]',
        'title' => 'Автор',
        'must' => '0',
        'maxlen' => '255',
        'disabeled' => 'true',
        'subtype'   => 'bigint',
		'placeholder' => 'ID Автора',
		'default'	=> $_SESSION['SESS_AUTH']['ID'],
        'type' => 'hidden',
    ),

    
 );


?>