<?
//$FORM_SEARCH	=	array( 
//'city', 'far', 'square', 'vodo', 'energy', 'otopl', 'kanal', 'gaz', 'arenda', 'prodaja'
//);

$FORM_ORDER	= ' ORDER BY time DESC ';
$FORM_WHERE		= '';
$FORM_SELECT	= '';
$FORM_FROM		= '';

$FORM_DATA= array (
  'ip'	=>
  array (
    'field_name' => 'ip',
    'name' => 'form[ip]',
    'title' => 'IP',
    'must' => 0,
		'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  
  'time'	=>
  array (
    'field_name' => 'time',
    'name' => 'form[time]',
    'title' => 'Time',
		'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'textbox',
  ),

  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'summs'	=>
  array (
    'field_name' => 'summs',
    'name' => 'form[summs]',
    'title' => '$',
		'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  'success_result'	=>
  array (
    'field_name' => 'success_result',
    'name' => 'form[success_result]',
    'title' => 'Result',
		'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  'inv_id'	=>
  array (
    'field_name' => 'inv_id',
    'name' => 'form[inv_id]',
    'title' => 'Inv-ID',
		'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'textbox',
  ),
  'user_tranzak'	=>
  array (
    'field_name' => 'user_tranzak',
    'name' => 'form[user_tranzak]',
    'title' => 'Tranzak',
		'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'textbox',
  ),


)
?>