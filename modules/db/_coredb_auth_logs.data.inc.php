<?

$FORM_ORDER	= ' ORDER BY ts DESC ';
if (!$_CORE->IS_ADMIN)
	$FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL)";

if (!defined('AUTH_LOGLEVEL_ADMIN')) {
	define ("AUTH_LOGLEVEL_ADMIN", 1);
}
if (!defined('AUTH_LOGLEVEL_USER')) {
	define ("AUTH_LOGLEVEL_USER", 2);
}

	$_KAT['INDEX'][$_KAT['KUR_ALIAS']] = 'id';

$FORM_DATA = array (
  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'alias' => 
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => Main::get_lang_str('alias', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
 'name'	=>
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => Main::get_lang_str('title', 'db'),
    'must' => 1,
	'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'textbox',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),

 'url'	=>
  array (
		'field_name' => 'url',
		'name' => 'form[url]',
		'title' => 'URL',
		'must' => 1,
		'style' => 'width:100%',
		'maxlen' => 255,
		'type' => 'textbox',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),

	'ts'	=>
  array (
    'field_name' => 'ts',
    'name' => 'form[ts]',
    'title' => Main::get_lang_str('data', 'db'),
    'must' => 1,
	'size' => 15,
    'maxlen' => 255,
    'type' => 'hidden',
    'readonly' => 'true',
  ),
 
  'hidden'	=>
  array (
    'field_name' => 'hidden',
    'name' => 'form[hidden]',
    'title' => Main::get_lang_str('ne_publ', 'db'),
    'must' => 0,
    'maxlen' => 1,
    'type' => 'checkbox',
  ),


)
?>