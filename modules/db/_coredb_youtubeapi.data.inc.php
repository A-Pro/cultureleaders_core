<?
$prefix = defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '';
$FORM_ORDER	= '  ';
$FORM_WHERE = '';

$FORM_DATA= array ( 
  'name'	=>
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => Main::get_lang_str('name', 'db'),
    'must' => 1,
	'style'	=> 'width:100%',
    'type' => 'textbox',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'alias' => 
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => Main::get_lang_str('alias', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'code'	=>
  array (
    'field_name' => 'code',
    'name' => 'form[code]',
    'title' => Main::get_lang_str('code', 'db'),
    'must' => 1,
	'style'	=> 'width:100%',
    'type' => 'textbox',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
  'updated'	=>
  array (
    'field_name' => 'updated',
    'name' => 'form[updated]',
    'title' => Main::get_lang_str('updated', 'db'),
    'must' => 1,
	'style'	=> 'width:100%',
    'type' => 'textbox',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),  
  'cont'	=>
  array (
    'field_name' => 'cont',
    'name' => 'form[cont]',
    'title' => Main::get_lang_str('cont', 'db'),
    'must' => 1,
	'style'	=> 'width:100%',
    'type' => 'textarea',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),    
  'hidden'	=>
  array (
    'field_name' => 'hidden',
    'name' => 'form[hidden]',
    'title' => Main::get_lang_str('ne_publ', 'db'),
    'must' => 0,
    'maxlen' => 1,
    'type' => 'checkbox',
		'sub_type' => 'varchar'
  ),
)
?>