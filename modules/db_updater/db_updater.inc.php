<?
if (!$_CORE->dll_load('class.DBCQ'))
	die ($_CORE->error_msg());
global $_KAT;	
$_DB_UPD['MODULE_NAME'] = 'db_updater';
$_DB_UPD['DEBUG'] = 1;
$_DB_UPD['VERSION'] = 'version.txt';
$_DB_UPD['DIR'] = 'data/sql/';
$_DB_UPD['TABLES']['admin_faq']      	= $_KAT['TABLES']['admin_faq'];
$_DB_UPD['TABLES']['admin_faq_youtube'] = $_KAT['TABLES']['admin_faq_youtube'];
//$_DB_UPD['TABLES']['admin_market']      = $_KAT['TABLES']['admin_market'];
$_DB_UPD['SRC'] = 'startupmilk.com'; // ���� � �������� ����� ��������������� $_DB_UPD['TABLES']



class DB_UPD {
	
	/**
	 * �������� ������ ������ � ���� arr(path,name,is_parent)
	 *
	 * @param string $node - ������� ����� ����� ��������
	 * @return array[](path,name)
	 */
	function update($link){
		global $_CORE, $_DB_UPD;
			if ($cmd = file_get_contents($link)){
				$arr = explode(';',$cmd);
				array_pop($arr);
				foreach($arr as $k => $val){
					$search = array ("'--.*?([\r\n])[\s]+'si",
					                 "'([\r\n])[\s]+'");
					$val = preg_replace($search, "", $val);
					$res[$k] = SQL::query( $val, $_DB_UPD['DEBUG'] );
					$res[$k]->Query = $val;
					if ( $res[$k]->ErrorQuery ){
						$h = fopen($_DB_UPD['DIR']."error.txt",'a+');
						fwrite($h,$val.";\n \n");
						fclose($h);
					}
				}
				return $res;
			}
		return false;
	}
	// �������� ������� first()
	function first(){
		global $_CORE, $_DB_UPD;
		$h = @fopen($_DB_UPD['DIR'].$_DB_UPD['VERSION'],'w');
		foreach (glob($_DB_UPD['DIR']."*.sql") as $filename) {
			fwrite($h,basename($filename)."\n");
		}
		@fclose($h);
		return file_exists($_DB_UPD['DIR'].$_DB_UPD['VERSION']);
	}
/**
 * Jeksport dannyh v fajl. 
 *
 * Prichem esli parametr $die == true, to proishodit sohranie v fajl, inache prosto vyvod na jekran.
 * 
 * @uses $FORM_IMPORT
 * @uses $_KAT['KUR_TABLE']
 * @uses SQL::sel()
 * @param bool $die
 */
	function export($name)
	{
		global $_KAT, $_DB_UPD, $_CORE ;

 		 $table        = ( ($_KAT[$name]['TABLE_PREFIX']?$_KAT[$name]['TABLE_PREFIX']:(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')) . (string)$name);
		if (!empty($_KAT["$name"]['IMPORT'])) {
			$FORM_IMPORT = $_KAT["$name"]['IMPORT'];
		}

		$var	= "`".implode("`,`", $FORM_IMPORT)."`";
		$res	= SQL::sel( $var , $table, '', '', DEBUG );

		if ($res->NumRows > 0){
			for ($i=0;$i < $res->NumRows; $i++ ) {
				$res->FetchRow($i);
				$str .= implode(";",$res->FetchRow)."\n";
			}
			
		}

		if ( !empty($str) ){
			$f = fopen($_DB_UPD['DIR'].$name.'.csv','w'); 
			fwrite($f, $str);
			fclose($f);
		}
	}

	function import($name)
	{
		global $_KAT, $_DB_UPD, $_CORE ;
		 $type = 'insert';
 		 $table        = ( ($_KAT[$name]['TABLE_PREFIX']?$_KAT[$name]['TABLE_PREFIX']:(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')) . (string)$name);
		 
		 $res = SQL::query("Show tables like '".$table."'", DEBUG);
		 if ($res->NumRows > 0){
			 SQL::query("TRUNCATE `".$table."`;", DEBUG);
		 } else {
			 
			 cmd('db/install');
		 }
			
		 
		if (!empty($_KAT["$name"]['IMPORT'])) {
			$FORM_IMPORT = $_KAT["$name"]['IMPORT'];
		}

		$new_file	= $_DB_UPD['DIR'].$name.'.tmp';

		$lines	= file($new_file);

		$good	= $bad	= 0;
		
		$delimeter = ',';
		// teper' po kazhdoj stroke
		for ($i = 0; $i < sizeof($lines); $i++ ) if (!empty($lines[$i])) {
			$data	= explode($delimeter, trim($lines[$i]));

			// esli ploho, to razdelim standartnym ;
			if (sizeof($data == 1)) {
				Main::log_message ( "Import '$type', size 0 after explode by $delimeter, trying ';'" );
				$data = explode(';', trim($lines[$i]));
			}

			// dopishem massiv dannyh ili srezhem lishnee, 
			// chto by byli odinakovogo razmera. rovno stol'ko, skol'ko v massive FORM_IMPORT
			if (sizeof($data) < sizeof($FORM_IMPORT)) {
				$data	= array_pad($data, sizeof($FORM_IMPORT), '');
			}
			if (sizeof($data) > sizeof($FORM_IMPORT)) {
				$data = array_slice($data, 0, sizeof($FORM_IMPORT));				
			}

			if (empty($type) || $type == 'insert' || $type == 'try') {
				// sozdali dannye dlja dobavlenieja INSERT
				$val	= "'".implode("','", $data)."'";
				$var	= "`".implode("`,`", $FORM_IMPORT)."`";
			}

			if (empty($type) || $type == 'insert') {
				$res	= SQL::ins($table, $var, $val, DEBUG);
			}elseif ($type == 'update') {
				$res	= SQL::upd($table, $set, $where, DEBUG);
			}elseif ($type = 'try') {
				// poprobuem dobavit'
				$res	= SQL::ins($table, $var, $val, DEBUG);
				// ili vstavit'
				if ($res->ErrorQuery)
					$res	= SQL::upd($table, $set, $where, DEBUG);
			}

			if ($res->Tuples > 0) {
				$good+=$res->Tuples;
			}else {
				$_KAT['ERROR'] .= $res->ErrorQuery;
				$bad++;
			}
		}
		return array($good, $bad);
	}

}
?>