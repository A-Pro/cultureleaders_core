<?php
global $_CORE, $_CONF;

if (!$_CORE->dll_load('class.DBCQ'))
	die ($_CORE->error_msg());
//include_once $_CONF['CorePath']."/lib/JSON.php";
	
/**
 * �lass DeveloperApi 
 *
 */
 

class DeveloperApi {
	
	
	public $Error = array();
	public $modulesList; // ������ ������ �������
	public $moduleTree = array(); // ������ ������ ��������� ������ ��� ������� ������
	/**
	 * ����������� ������
	 * 
	 */
	public function __construct( $params = array()){
		if(is_array($params) && count($params)){
			foreach($params as $key=>$val){
				$this->$key = $val;
			}
		}
	}
	
	
	
	public function install(){
		$sql = "
			CREATE TABLE IF NOT EXISTS `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."core_modules` (
			  `id` bigint(20) NOT NULL AUTO_INCREMENT,
			  `alias` varchar(255) NOT NULL,
			  `name` varchar(255) NOT NULL,
			  `active` int(1) NOT NULL DEFAULT '0',
			  `beforepl_plg` varchar(255) NOT NULL,
			  `config` TEXT NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM
		";
		$res = SQL::query($sql, DEBUG);
		SQL::query("ALTER TABLE  `grand__core_modules` ADD  `config` TEXT NOT NULL",DEBUG);
		if (!$res->Result)
			echo $res->ErrorQuery."<br />";
		else
			echo "����������� ������� `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."core_modules`<br />";
		
		// �������� ���� �� ������������ developer � ���� ��� �� �������
		if( SQL::getval('count(*)',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."auth_pers","author_login='developer'") == 0 ){
			$max_id = SQL::getval( "MAX(author_id) as max", (defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."auth_pers");
			$next_id_lost = intval($max_id)+1;
			$res = SQL::ins( (defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."auth_pers", " author_login,  author_passwd, author_comment, author_id, created, modify", " 'developer','developer123','developer', '$next_id_lost', ".time().", ".time()."" );
			if (!$res->Result)
				echo $res->ErrorQuery."<br />";
			else
				echo "�������� ������������ `developer`<br />";
		}
		return;
	}
	
	
	
	public function install_menu(){
		$sql = "
			CREATE TABLE IF NOT EXISTS `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."core_menu` (
			  `id` bigint(20) NOT NULL AUTO_INCREMENT,
			  `parent_id` bigint(20) NOT NULL,
			  `icon` varchar(255) NOT NULL,
			  `alias` varchar(255) NOT NULL,
			  `name` varchar(255) NOT NULL,
			  `link` varchar(255) NOT NULL,
			  `hidden` int(1) NOT NULL,
			  `access` int(1) NOT NULL COMMENT '0 - admin, 1 - developer',
			  `prior` int(3) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM  ;
		";
		$res = SQL::query($sql, DEBUG);
		if (!$res->Result)
			echo $res->ErrorQuery."<br />";
		else
			echo "����������� ������� `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."core_menu`<br />";
		return;
	}
	
	
	
	public function get_module_config($mod){
		// �������� ���� �� ����� ������
		global $_CORE;
		if(!isset($_CORE->_modules[$mod]) ) return false;
		
		if(empty($_CORE->_modules[$mod]['config'])) return '';
		$form = STR::json_decode_cyr($_CORE->_modules[$mod]['config'],true);
		$form = STR::utf_to_win($form);
		return $form;
	}
	
	
	
	public function set_module_config($mod,$form){
		// �������� ���� �� ����� ������
		global $_CORE;
		if(!isset($_CORE->_modules[$mod]) ) return false;
		
		if(!isset($_CORE->_modules[$mod]['config'])) return false;
		$form = STR::win_to_utf($form,true);
		$config = STR::json_encode_cyr($form);
		SQL::upd("`".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."core_modules`", "config = '".$config."'","alias = '".$mod."'");
		return true;
	}
	
	
	
	public function install_mod($mod){
		// �������� ���� �� ����� ������
		global $_CORE;
		if(isset($_CORE->_modules[$mod]) ) {
			array_push($this->Error, "������, ������ ��� ����������");
			return false;
		}
		$file = $_CORE->PATHMODS.'/'.$mod.'/'.$mod.'.inc.php';
		if( is_file($file) )
			include "$file";
		else {
			array_push($this->Error, "������, ���� $file � ������� �� ������");
			return false; // ������, ���� � ������� �� ������
		}
		$className = ucfirst(strtolower($mod));
		$modClass = new $className();
		$modClass->install();
		if(!empty($modClass->Error)){ 
			array_push($this->Error, $modClass->Error);
			return false;
		}
		return true;
	}
	
	
	function getpath($p=''){
		global $_PROJECT;
		if ($p == '') 
			if (empty($_REQUEST['node'])) {
				$p = $_SERVER['REQUEST_URI'];
			}else{
				$p = $_REQUEST['node'];
			}
		if (!empty($_PROJECT['PATH_2_CONTENT']))
			$_PROJECT['PATH_2_CONTENT'] = array();
		$p	= (substr($p,-1,1)!='/') ? $p.'/' : $p;
		$parts = explode('/',$p);
		$LOOK	= (substr($p,0,6) == 'admin/') ? substr($p, 6) : $p;
		$path	= (substr($p,-1,1)!='/') ? $p.'/' : $p;
		if (!in_array($p, array( '', '/'))) {
			$PATH_2_CONTENT_tmp	=& Main::array_multisearch( $path, $this->moduleTree, "and end");
			$up	= $last	= $prelast = $this->moduleTree;
			for ($i=0;$i<sizeof($PATH_2_CONTENT_tmp)-1; $i++ ) {
				if (!empty($up[$PATH_2_CONTENT_tmp[$i]]['name']) ) {
					$_PROJECT['PATH_2_CONTENT'][]	= array(
						'name'	=> $up[$PATH_2_CONTENT_tmp[$i]]['name'],
						'path'	=> $up[$PATH_2_CONTENT_tmp[$i]]['path']
						);
				}
				$up = $up[$PATH_2_CONTENT_tmp[$i]];
			}
		}
	}
}
