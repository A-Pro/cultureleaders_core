<?php
$FORM_DATA=	array(
	'delimiter1' => array(
		'type' 		=> 'delimeter_line', 
		'subtype' 	=> 'tabs',
		'title' 	=> '����� ���������'
	),
	'oneitemput'	=>
	array	(
		'field_name' =>	'oneitemput',
		'name' =>	'form[oneitemput]',
		'title'	=> '������ ����� � ����� ����������',
		'must' =>	0,
		'maxlen' =>	1,
		'type' =>	'checkbox'
	),
	'printversion'	=>
	array	(
		'field_name' =>	'printversion',
		'name' =>	'form[printversion]',
		'title'	=> '����������� ������ ��� ������ ������ �� ������',
		'must' =>	0,
		'maxlen' =>	1,
		'type' =>	'checkbox'
	),
	'name_for_mail'	=>
	array	(
		'field_name' =>	'name_for_mail',
		'name' =>	'form[name_for_mail]',
		'title'	=> '��� �� ���� ���������� ������ ���������',
		'must' =>	0,
		'style'	=> 'width:100%',
		'maxlen' =>	255,
		'type' =>	'textbox'
	),
	'after'	=>
	array	(
		'field_name' =>	'after',
		'name' =>	'form[after]',
		'title'	=> '������ �� ������� �������������<br>����� ���������� ������ ������',
		'must' =>	0,
		'style'	=> 'width:100%',
		'maxlen' =>	255,
		'type' =>	'textbox'
	),
	'add2basket_db_alias'=>
	array	(
		'field_name' =>	'add2basket_db_alias',
		'name' =>	'form[add2basket_db_alias]',
		'title'	=> '����� �������� �� �������� ����������� �����',
		'must' =>	0,
		'style'	=> 'width:100%',
		'maxlen' =>	255,
		'type' =>	'textbox'
	),
	'from_citi'=>
	array	(
		'field_name' =>	'from_citi',
		'name' =>	'form[from_citi]',
		'title'	=> '����� �� �������� �������������� �������� �����',
		'must' =>	0,
		'style'	=> 'width:100%',
		'maxlen' =>	255,
		'type' =>	'textbox'
	),
	'delimiter2' => array(
		'type' 		=> 'delimeter_line', 
		'subtype' 	=> 'tabs',
		'title' 	=> '�������'
	),
	'confirm'	=>
	array	(
		'field_name' =>	'confirm',
		'name' =>	'form[confirm]',
		'title'	=> '�������� ���������� ������� confirm.inc.php<br>����� ���������� ������ ������',
		'must' =>	0,
		'maxlen' =>	1,
		'type' =>	'checkbox'
	),
	'delivery'	=>
	array	(
		'field_name' =>	'delivery',
		'name' =>	'form[delivery]',
		'title'	=> '������� ��������',
		'must' =>	0,
		'maxlen' =>	1,
		'type' =>	'checkbox'
	),
	'delivery2payment' => array (
		'field_name' =>	'delivery2payment',
		'name' =>	'form[delivery2payment]',
		'title'	=> '��������� ��������',
		'must' =>	0,
		'maxlen' =>	1,
		'type' =>	'checkbox'
	),
	'deliverybyself' => array (
		'field_name' =>	'deliverybyself',
		'name' =>	'form[deliverybyself]',
		'title'	=> '����������� ����������',
		'must' =>	0,
		'maxlen' =>	1,
		'type' =>	'checkbox'
	),
	'pickpoint' => array (
		'field_name' =>	'pickpoint',
		'name' =>	'form[pickpoint]',
		'title'	=> '�������� ���������� pickpoint',
		'must' =>	0,
		'maxlen' =>	1,
		'type' =>	'checkbox'
	),
	'expressru' => array (
		'field_name' =>	'expressru',
		'name' =>	'form[expressru]',
		'title'	=> '�������� ���������� Express.ru',
		'must' =>	0,
		'maxlen' =>	1,
		'type' =>	'checkbox'
	),
	
);
?>