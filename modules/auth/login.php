<?php
 //////////////////////////////////// About	 this file ////////////////
//
// FILE			: login.php	
// PROJECT NAME	: NNOV / LK
// DATE			: 05.11.2002
// Author		: Andrew Lyadkov
// Discription	: 
// ...........	: ���� �� ������
// Comment		:
// ...........	:
// Coding		: CP-1251
//
//////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////
////////   SETTINGS	  /////////////////////////////////
///////////////////////////////////////////////////////
$LOGIN_RESULT =	false;
$LOGIN_RESULT_STR =	'';

$submit	= $_POST['submit'];
$login	= $_POST['login'];
$passwd	= $_POST['passwd'];
global $auth,$_COOKIE, $_SMS, $_CONF;

if (!empty($login)&&!empty($passwd)) {

	///////////////////////////////////////////////////////
	////////   CONTROL	  /////////////////////////////////
	///////////////////////////////////////////////////////

	include_once "lib_includer.php"; //	+ $db =	new

	_load_land_arr($DATA, $_CONF['LandDatPath'].'/auth_pers.dat');

	$pwd	=  ($_CONF['PWDCRYPTED']) ? crypt($passwd, SALT) : $passwd;
	#
	# ���� ������
	#
	// ��� ������ ���� ����� ��� ����� �������� 
	if($_AUTH['phone_login'] && $phone = _is_phone($login)){ // ���� ����������� ����� ������� �������� � ���������� � ��������� ������ ��� ����� ��������
		$add_where = "author_phone = '".$phone."'"; // accept_phone
	}else{
		$add_where = "author_login	= '".$login."'";
	}
	
	$search	= select_sql( '*', TAB_AUTH_PERS, $add_where." AND author_passwd = '$pwd'", '', 0);
	if ($search->Result	&& $search->NumRows>0 || $_CORE->check_root($login,$pwd)) {
		$search->FetchArray(0);
		$auth =	$search->FetchArray;
		$login = $auth['author_login'];
		// for core 
		global $_CORE;

		if ($_AUTH['ban_enabled'] && $auth['hidden']) {

			$LOGIN_RESULT = false;
			$LOGIN_RESULT_STR		= _get_error_str("������������ ������������");
		
		} else {

			if (empty($auth['author_part'])) {
				$_CORE->set_admin($login, $auth['author_role']);
			}
			# Zapominaem ego dannye
			if(($auth['accept'] == 2)||(empty($_CONF['MAIL_ACT']))){

				if( empty($_AUTH['phone_login']) || (!empty($phone) && $auth['accept_phone'] == 2) || empty($phone) ){
					session_start();
					$_SESSION['SESS_AUTH']				= array();
					$_SESSION['SESS_AUTH']['ID']		= $auth['author_id'];
					$_SESSION['SESS_AUTH']['LOGIN']		= $auth['author_login'];
			//		$_SESSION['SESS_AUTH']['PASSWD']	= $auth['author_passwd'];
					$_SESSION['SESS_AUTH']['HASH']		= md5($auth['author_passwd'].$auth['author_login']);
					$_SESSION['SESS_AUTH']['BLOG']		= $auth['author_blog'];
					$_SESSION['SESS_AUTH']['PART'] 		= $auth['author_part'];
					$_SESSION['SESS_AUTH']['ADDITION']	= $auth['author_role'];
					$_SESSION['SESS_AUTH']['NAME']		= isset($auth['author_name']) ? $auth['author_name'] : $auth['author_comment'];
					if(empty($_SESSION['SESS_AUTH']['NAME'])) $_SESSION['SESS_AUTH']['NAME']		= $auth['author_company'];
							$_SESSION['ADMIN']['TPL']           = 'admin'; // for login after admin's switcher Design
							$_SESSION['SESS_AUTH']['PART']		= $auth['author_part'];
					$_SESSION['SESS_AUTH']['PERSON']	= (!empty($_SESSION['SESS_AUTH']['NAME']))?$_SESSION['SESS_AUTH']['NAME']:$_SESSION['SESS_AUTH']['LOGIN'];
					$_SESSION['SESS_AUTH']['AVATAR']	= $DATA['doc']['path'].$auth['doc'];
					$_SESSION['SESS_AUTH']['ALL']	= $auth;
					unset($_SESSION['SESS_AUTH']['ALL']['2']);
					unset($_SESSION['SESS_AUTH']['ALL']['author_passwd']);
	
					$LOGIN_RESULT			= true;
					$LOGIN_RESULT_STR		= _get_error_str("�����	����������,	".$_SESSION['SESS_AUTH']['PERSON']."!");
				}
			 	// ���� ����� �������� � ������� ������, �� �� �����������
				if( !empty($_AUTH['phone_login']) && !empty($phone) && $auth['accept_phone'] < 2 ){
					$code = rand(10000,99999);
					$res = SQL::upd( DB_TABLE_PREFIX.'auth_pers', "code_phone = '".$code."', accept_phone = 1", "author_id = '".$auth['author_id']."'");
					// ���������� ���������� �� sms

					$text = "��� ������������� �������� ".$code;
					$text_sms = iconv("windows-1251","UTF-8",$text);
					$phone = substr(preg_replace('/\D/', '', $phone),-10);
					$res = $_SMS->sendSMS ( $phone , $text_sms , $_CONF['settings']['from'] );
				}
			}
		}


	} else {

		$LOGIN_RESULT_STR		= _get_error_str ("������� ������� ������");
	}
	
}
#
# �����
#


 /// echo $LOGIN_RESULT_STR; 

if (!$LOGIN_RESULT && empty($_SESSION['SESS_AUTH']['ID'])){
	if(!empty($code)){
		if (Main::comm_inc("accept_phone.html", $file, 'auth'))
			include $file;
	}else{
		if (Main::comm_inc("auth_login.html", $file, 'auth'))
			include $file;
	}
}else{
		if(!empty($_AUTH['AUTORISE'])) {
    		SQL::upd(TAB_AUTH_PERS,"AUTORISE='".time()."'", "author_id = ".$auth['author_id']);
		}  
		
		setcookie('login', $login, time()+3600*7*24,'/',".".$_SERVER['HTTP_HOST']);
		$_COOKIE['login'] = $login;
		if($_POST['remember'] == 1){
		setcookie('passwd', $passwd, time()+3600*7*24,'/',".".$_SERVER['HTTP_HOST']);
		$_COOKIE['passwd'] = $passwd;
		}
  
	// If admin is comming
	if ($_CORE->IS_ADMIN && $_CORE->CURR_TPL == 'admin') {
	    $_SESSION['ADMIN']['TPL']           = 'admin'; // for login after admin's switcher Design
		header("Location: /doctxt/?list");
		exit;

	// if just user logged in
	}else{
		if (!empty($_AUTH['AFTER_LOGIN_INC'])) {
			include $_AUTH['AFTER_LOGIN_INC'];
		}
		if(empty($_REQUEST['norefresh'])) {
    		header("Location: /".$_AUTH['after_login_url']);
			exit;
		}
		else {
			if (Main::comm_inc("auth_login_ok.html", $file, 'auth'))
				include $file;
		}
	}
}

?>