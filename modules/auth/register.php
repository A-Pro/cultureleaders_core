<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : edit.php 
// PROJECT NAME : TEMPLATE
// DATE         : 29.10.2002
// Author       : Andrew Lyadkov
// Discription  : 
// ...........  : ��� ������ � ��������, ���������� �������������� ...
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////
////////   SETTINGS   /////////////////////////////////
///////////////////////////////////////////////////////

// FOR CORE
global $_CORE, $CONF,$ERROR_MSG;
if ($_CORE->NeedUsers != $_CONF['AddUsersTypes'][2])
	if ($_CORE->NeedUsers==$_CONF['AddUsersTypes'][1] && !$_CORE->IS_ADMIN)
		return;

//LAND
include_once "lib_includer.php"; // + $db = new
//$FILE_FORM	= 'auth_reg.form.html';

//$FILE_FORM	= (is_file($_CORE->SiteModDir.'/auth_reg.form.html')) ? 
//				$_CORE->SiteModDir.'/auth_reg.form.html' : 
//				$_CORE->CoreModDir.'/auth_reg.form.html';

global $REG_RESULT, $REG_RESULT_STR, $ERROR_MSG, $auth, $DATA;
$auth	= $_POST['auth'];
$submit	= $_POST['submit'];

Main::comm_inc("auth_reg.form.html", $FILE_FORM, 'auth');

_load_land_arr($DATA, $_CONF['LandDatPath'].'/auth_reg.dat');

if (!empty($_CONF['FEATURES_USED']['partners'])){
$DATA['prtn'] = 
  array (
    'field_name' => 'prtn',
    'name' => 'auth[prtn]',
    'title' => '�������',
    'type' => ((empty($_SESSION['SESS_AUTH']['PRTN']) || $_CONF['FEATURES_USED']['partners'] === 'opened') ?'textbox':'hidden'),
    'must' => '0',
    'size' => '20',
    'maxlen' => '20',
		'readonly' => (empty($_SESSION['SESS_AUTH']['PRTN'])?"":'true'),
    'default' =>  (empty($_SESSION['SESS_AUTH']['PRTN'])?"":$_SESSION['SESS_AUTH']['PRTN'])
  );
}
$cform		= new cForm($DATA);


if( !isset($ERROR_MSG) ) {
	$ERROR_MSG	= '';
}

$AUTH_ID	= '';
$PASSWD		= '';
$code = '';
$next_id = 0;

if (!isset($action)) $action = 'add';

///////////////////////////////////////////////////////
////////   CONTROL    /////////////////////////////////
///////////////////////////////////////////////////////

$auth	= $_POST['auth'];
$submit	= $_POST['submit'];
if(!empty($_POST['auth']['author_company'])) $auth['author_company'] = iconv("UTF-8", "Windows-1251" ,$_POST['auth']['author_company']);

global $REG_RESULT, $REG_RESULT_STR, $ERROR_MSG;

$REG_RESULT = false; 
$REG_RESULT_STR = '';

/* ��� ����������� �������� ����� ����� ���������������� �������������
*  ��������� �������� ��� ���� � ������� auth_pers 
*       code varchar 255
*       accept int 1 
*  � ����� �������� ���������� ���������� $_CONF['MAIL_ACT'] = true
*/

// �������� 
if (!empty($submit)){ 		// ��������� ��������� ������
	unset($submit);
	if (check_form( $auth )){
		if(!empty($_CONF['MAIL_ACT'])){
				//��������� ����
			$am=mt_rand(5,7);
			$eng='qwertyuiopasdfghjklzxcvbnm';
			$num='1234567890';
			$full_text=$eng.$num;
			$e=strlen($full_text)-1;
			$p='';
			for($n=0;$n!=$am;$n++){
				$p.=substr($full_text,mt_rand(0,$e),1);
			} 
			$code	= $p;
		}
		
		if ( make_insert_add( $auth,$code ) ) {
			
			$REG_RESULT = true;
			$REG_RESULT_STR = _get_error_str("�� ����������������!");
      if(!empty($code)) {$REG_RESULT_STR = _get_error_str("�� ����������������!<br />�� �����, ������� �� ������� ��� �����������, ���� ������� ������ ��� ���������"); }
		if(!empty($_REQUEST['reg_text']))	$next_id = getval_sql( "MAX(author_id) as max", TAB_AUTH_PERS); 
		}
	} else {
		$REG_RESULT_STR = $ERROR_MSG;
	}
}

# ����� 
if(!defined('MOBILE_FACE')) {
//	echo ($REG_RESULT_STR) ? "<p class='alert alert-warning'>".$REG_RESULT_STR."</p>" : '';
}

global $_CONF;
$_CONF['ModuleTitle'] = "�����������";

if (!$REG_RESULT) { 
	include "$FILE_FORM";
}else{
	$auth['token'] = md5(SALT.$auth['author_login']);

	Main::comm_inc("after_register.html", $file, 'auth');
	if ($file) include "$file";
	 	
	// Admin notify
	//if (!MAIL_LOGO2ADMIN(var_export($auth, true)))	
	//	echo "<!-- adm: no sent -->";
				$str = "������������! ����������� �� ����� ".$_SERVER["HTTP_HOST"]." ������ �������.<br> ��� ����� - ".$auth['author_login']."<br> ��� ������ - ".$auth['author_passwd'];
				if(!empty($_CONF['MAIL_ACT'])){ 
				$next_id = getval_sql( "MAX(author_id) as max", TAB_AUTH_PERS);
				$str .='<br> ����� ����������� �����������, �������� �� ���� ������ <a href="http://'.$_SERVER["HTTP_HOST"].'/auth/accept?code='.$code.'&nom='.$next_id.'" target=_blank>'.$_SERVER["HTTP_HOST"].'/auth/accept?code='.$code.'&nom='.$next_id.'</a>';
				}
				$text1	= iconv("windows-1251", "utf-8", $str);
				$subj = iconv("windows-1251", "utf-8", "����������� �� ����� ".$_SERVER["HTTP_HOST"]);
				$headers="MIME-Version: 1.0\nContent-Type: text/html; charset=utf-8;";	
				$from   = "From: no-reply@".$_SERVER["HTTP_HOST"]."<no-reply@".$_SERVER["HTTP_HOST"].">\n".$headers;
                
                // land add for silent mail mode
				if (empty($_CONF['MAIL_SILENCE'])) @mail($auth['author_login'],$subj, $text1, $from);
				// ������
				@mail(TO,iconv("windows-1251", "utf-8",'����������� ������� �� '.$_SERVER['HTTP_HOST']),iconv("windows-1251", "utf-8","����� ������������: $auth[author_login] - $auth[author_comment]"),"MIME-Version: 1.0\nContent-Type: text/html; charset=utf-8;\nContent-transfer-Encoding: 8bit\nX-Mailer: PHP.");
	// Reg message
	if(!empty($regMessage))
	{
		foreach($auth as $k=>$v)
		{
			$regMessage = str_replace('{'.$k.'}',$v,$regMessage);
		}
		// ������������
		mail($auth['author_login'],STR::win2utf('����������� �������'),$regMessage,"MIME-Version: 1.0\nContent-Type: text/html; charset=cp1251;\nContent-transfer-Encoding: 8bit\nX-Mailer: PHP.");
		// ������
		mail(TO,STR::win2utf('����������� ������� �� '.$_SERVER['HTTP_HOST']),"����� ������������: $auth[author_login] - $auth[author_comment]","MIME-Version: 1.0\nContent-Type: text/html; charset=cp1251;\nContent-transfer-Encoding: 8bit\nX-Mailer: PHP.");
	}

	// need mail
	Main::comm_inc("after_register_mail.html", $file, 'auth');

	if ($file) {
		ob_start();
		include "$file";
		$body = ob_get_contents();
		ob_end_clean();
		if (MAIL_LOGO2USER($auth['author_login'], $auth['author_comment'], $body))
			echo "<!-- user not send (( -->";
	}
}

///////////////////////////////////////////////////////
////////   FUNCTIONS  /////////////////////////////////
///////////////////////////////////////////////////////

 //////////////////////////////////////////////////////	 check_form( &$arr )
//
function check_form( &$arr )
{
	global $ERROR_MSG, $DATA, $_CONF, $db, $_AUTH;
	_load_land_arr($DATA, $_CONF['LandDatPath'].'/auth_reg.dat');
  if (!empty($_CONF['FEATURES_USED']['partners'])){
  $DATA['prtn'] = 
    array (
      'field_name' => 'prtn',
      'name' => 'auth[prtn]',
      'title' => '�������',
      'type' => (empty($_SESSION['SESS_AUTH']['PRTN'])?'textbox':'hidden'),
      'must' => '0',
      'maxlen' => '255',
      'default' =>  (empty($_SESSION['SESS_AUTH']['PRTN'])?"":$_SESSION['SESS_AUTH']['PRTN'])
    );
  }	
	for(reset($DATA);list($k,$v)=each($DATA);){
		# ������������ 
		if ($v['must']==1&&(!isset($arr[$k]) || $arr[$k]=='') && $v['type']!='multcheckbox') {
			$ERROR_MSG .= _get_error_str('���� "'.$v['title'].'" �����������.');
		}
			// if multcheckbox
			if ($v['type'] == 'multcheckbox' && !empty($v['must'])){
				$tmp_bad = true;
				for($i = 0; $i < sizeof($v['arr']); $i++ ){
					$value = $arr[ $v['field_name'].$i ];
					$tmp_bad = $tmp_bad && empty($value);
				}
				if ($tmp_bad) {
					$ERROR_MSG .= _get_error_str('���� "'.$v['title'].'" �����������.');					
				}
			}
		# ������ 
		if (strlen($arr[$k])>$v['maxlen']) {
			$ERROR_MSG .= _get_error_str('� ���� "'.$v['title'].'" ������� ����� ��������.');
		}
	}

	if (empty($arr['author_passwd']) || $arr['author_passwd']!=$arr['author_passwd2']) {
		$ERROR_MSG .= _get_error_str('������ �� ���������.');
	}

	if ($_AUTH['login_email']) {
		if (!empty($arr['author_login']) && !_is_email($arr['author_login'])) {
			$ERROR_MSG .= _get_error_str('������� ������ E-MAIL.');
		}
	}
	// esli ne admin, to logi - e-mail
	if (empty($arr['author_email']) && $arr['auth_login'] != 'admin' && !_is_email($arr['author_login']) && !$_CORE->IS_ADMIN) {
		$ERROR_MSG .= _get_error_str('������� ������ E-mail.');
  }
	# check login (sql search)

	$login = $arr['author_login'];
	if (getval_sql( "COUNT(*) as count", TAB_AUTH_PERS, " author_login = '$login' ", '', $DEBUG)>0) {
		$ERROR_MSG .= _get_error_str('����� ����� ��� ���������������.');
		$_REQUEST['reg_text'] = 1;
	}
//	if (getval_sql( "COUNT(*) as count", TAB_AUTH_PERS, " author_email = '$arr[author_email]' ", '', $DEBUG)>0) {
//		$ERROR_MSG .= _get_error_str('����� � ����� Email`�� ��� ���������� :( .');
//	}
	# check data
	for(reset($arr);list($n,$v)=each($arr);) {
		if (strchr($v,"'")) {
			$arr[$n] = stripslashes($v);
			$ERROR_MSG .= _get_error_str('������������ ������ � ����: '.$DATA[$n]['title']);
		} else {
			$arr[$n] = clear_data($v);
		}
	}
	return ($ERROR_MSG == '');
}

 //////////////////////////////////////////////////////	 make_insert_add( &$arr )
//
function make_insert_add( &$arr, $code)
{

	global $DATA, $auth, $ERROR_MSG, $_CONF,$regMessage, $_AUTH;

	$auth = $arr; // ������� 24.08.2011 ((

	$auth['author_passwd']	= ($_CONF['PWDCRYPTED']) ? crypt($auth['author_passwd'],SALT) : $auth['author_passwd'];

	unset($auth['author_passwd2']);


	$form		= new cForm($DATA);
	$now_time	= time();
	list($vars,$vals) = $form->GetSQL('insert');
	$max_id = getval_sql( "MAX(author_id) as max", TAB_AUTH_PERS);
	$next_id_lost = intval($max_id)+1;
    
	if ($_AUTH['token']) {
		$auth['token'] = md5(SALT.$auth['author_login']);
		$var_add = ',token';
		$val_add = ",'".$auth['token']."'";
	}
	
	if(!empty($code))	{
	$acc = 1; $ins = insert_sql( TAB_AUTH_PERS, "$vars, code, accept, author_id, created, modify".$var_add, " $vals, '$code',$acc,'$next_id_lost', $now_time, $now_time".$val_add );
	}	else $ins = insert_sql( TAB_AUTH_PERS, "$vars, author_id, created, modify".$var_add, "$vals, '$next_id_lost', $now_time, $now_time".$val_add );
	$next_id = mysql_insert_id();
  $auth['author_id'] = ((empty($next_id))?$next_id_lost:$next_id);
/*	echo TAB_AUTH_PERS . " author_id, $vars, created, modify " . " '$next_id', $vals, $now_time, $now_time";
	die();*/
	// add alias for db 
	if (in_array('auth', $_CONF['USED_MODULES'])) {
		SQL::upd( TAB_AUTH_PERS, ' alias = author_id ', '' , '', DEBUG );
	}
	
	if (!$ins->Result){
		$ERROR_MSG 	= $inc->ErrorQuery;
		return false;
	}

	# ���������� ��� ������
	if(empty($_REQUEST['reg_text'])){
	session_start();

	if (!$_AUTH['no_auto_login']) {
		$_SESSION['SESS_AUTH'] 				= array();
		$_SESSION['SESS_AUTH']['ID'] 		= ((empty($next_id))?$next_id_lost:$next_id);
		$_SESSION['SESS_AUTH']['LOGIN'] 	= $auth['author_login'];
		$_SESSION['SESS_AUTH']['PART'] 		= $auth['author_part'];
	//	$_SESSION['SESS_AUTH']['PASSWD'] 	= $auth['author_passwd'];
		$_SESSION['SESS_AUTH']['HASH']		= md5($auth['author_passwd'].$auth['author_login']);
		$_SESSION['SESS_AUTH']['NAME']		= isset($auth['author_name']) ? $auth['author_name'] : $auth['author_comment'];
		if(empty($_SESSION['SESS_AUTH']['NAME'])) $_SESSION['SESS_AUTH']['NAME'] = $auth['author_company'];
		$_SESSION['SESS_AUTH']['PERSON'] = (!empty($_SESSION['SESS_AUTH']['NAME']))?$_SESSION['SESS_AUTH']['NAME']:$_SESSION['SESS_AUTH']['LOGIN'];
    $auth	= SQL::getrow( '*',DB_TABLE_PREFIX.'auth_pers', "author_id	= '".$_SESSION['SESS_AUTH']['ID']."' AND author_login = '".$_SESSION['SESS_AUTH']['LOGIN']."'" );
				$_SESSION['SESS_AUTH']['ALL']	= $auth;
        unset($_SESSION['SESS_AUTH']['ALL']['0']);
        unset($_SESSION['SESS_AUTH']['ALL']['1']);
        unset($_SESSION['SESS_AUTH']['ALL']['2']);
        unset($_SESSION['SESS_AUTH']['ALL']['3']);
				unset($_SESSION['SESS_AUTH']['ALL']['4']);
				unset($_SESSION['SESS_AUTH']['ALL']['author_passwd']);
	}
	}
	
	return true;
}


/*

DATA:
----

auth = Array
(
	�����.

    [author_login] => auth_pers
    [author_passwd] => auth_pers
    [author_name] => auth_pers
	[author_male] => auth_pers
    [author_email] => auth_pers

	�������.
    [author_passwd2] => auth_pers

)


*/
?>