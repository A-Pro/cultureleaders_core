<?php
/**
 * ������ Loginza ��� ����������� ����� ���������� ���� � OpenID �����������.
 * 
 * ������ ������ ���������� �� ������ ������� Loginza.API (http://loginza.ru/api-overview).
 * 
 * @package loginza
 * @version 1.0
 * @author Denis Ulyusov
 *
 * ��� ������������ ����� ����������� ���� � ������� �������������
 * ALTER TABLE `grand__auth_pers` ADD `author_fam` VARCHAR( 255 ) NOT NULL ,
ADD `author_nik` VARCHAR( 255 ) NOT NULL ,
ADD `author_bd` VARCHAR( 255 ) NOT NULL ,
ADD `author_sex` VARCHAR( 255 ) NOT NULL ,
ADD `doc` VARCHAR( 255 ) NOT NULL ,
ADD `accept` VARCHAR( 255 ) NOT NULL ,
ADD `created` VARCHAR( 255 ) NOT NULL ,
ADD `MODIFY` VARCHAR( 255 ) NOT NULL ,
ADD `AUTORISE` VARCHAR( 255 ) NOT NULL ,

ADD `loginza_identity` VARCHAR( 255 ) NOT NULL ,
ADD `loginza_provider` VARCHAR( 255 ) NOT NULL ;
 * 
*/

require_once 'lib/LoginzaAPI.class.php';
require_once 'lib/LoginzaUserProfile.class.php';

define('USERS_TABLE', 	(( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'auth_pers');
/**
* loginza_register
* loginza registration
* @package loginza
*/
class loginza_register
{
	var $u_action;
  var $profile;
	function main()
	{
		global $_CONF, $_CORE;
		
		$LoginzaAPI = new LoginzaAPI();
		
		// ������ ������� ��������������� ������������
		$profile = $LoginzaAPI->getAuthInfo($_POST['token']);
    $this->profile = $profile;
		// �������� �� ������
		if (is_object($profile) && empty($profile->error_type)) {
			// ����� ������������ � ��
			if ( !($user_id = $this->findUserByIdentity($profile->identity)) ) {
			     
				$user_id = $this->regUser($profile);
                
			}
			
		}else{
		  return false;
		}
    if($user_id){
  		// ����������� �����
  		$result = $this->load_user($user_id);
  		
  		// ���� ������ ������� �������
  		if ($result === true) {
        Main::comm_inc("after_register.html", $file, 'auth');
        if ($file) include "$file";
        header("Location: /auth/update");
	       exit;
  		}
    }
    return false;
	}
	
	/**
	 * ����� ������������� ������������ �� ��� identity
	 *
	 * @param string $identity
	 * @return array
	 */
	function findUserByIdentity ($identity) {
		global $_CONF, $_CORE;
		if (!$_CORE->dll_load('class.DBCQ'))
        	die ($_CORE->error_msg());
    $add_where = '';
    if(isset($this->profile->email))  $add_where = "OR `author_login`= '".$this->profile->email."'";      
		$result = SQL::getrow('author_id', USERS_TABLE, "`loginza_identity` = '".mysql_real_escape_string($identity)."'  ".$add_where);

		return @$result['author_id'];
	}
    
  function load_user($uid){
    global $_CORE;
    if(!empty($uid))
    $auth	= SQL::getrow( '*',DB_TABLE_PREFIX.'auth_pers', "author_id = '".$uid."'" );
    if(!empty($auth)){
			if (empty($auth['author_part'])) {
				$_CORE->set_admin($auth['author_login'], $auth['author_role']);
			}
      $_SESSION['SESS_AUTH']['ID']=$auth['author_id'];
      $_SESSION['SESS_AUTH']['LOGIN']=$auth['author_login'];
      $_SESSION['SESS_AUTH']['HASH']= md5($auth['author_passwd'].$auth['author_login']);

				$_SESSION['SESS_AUTH']['NAME']		= isset($auth['author_name']) ? $auth['author_name'] : $auth['author_comment'];
				if(empty($_SESSION['SESS_AUTH']['NAME'])) $_SESSION['SESS_AUTH']['NAME']		= $auth['author_company'];
						$_SESSION['ADMIN']['TPL']           = 'public'; // for login after admin's switcher Design
						$_SESSION['SESS_AUTH']['PART']		= $auth['author_part'];
				$_SESSION['SESS_AUTH']['PERSON']	= (!empty($_SESSION['SESS_AUTH']['NAME']))?$_SESSION['SESS_AUTH']['NAME']:$_SESSION['SESS_AUTH']['LOGIN'];
				$_SESSION['SESS_AUTH']['AVATAR']	= (!empty($auth['doc']))?'/data/auth/'.$auth['doc']:'';
				$_SESSION['SESS_AUTH']['ALL']	= $auth;
				unset($_SESSION['SESS_AUTH']['ALL']['author_passwd']);
        SQL::upd(DB_TABLE_PREFIX.'auth_pers',"AUTORISE='".time()."'", "author_id = ".$auth['author_id']);
        return true;
    }
    return false;
  }	
	/**
	 * ����������� ������������
	 *
	 * @param unknown_type $profile
	 * @return unknown
	 */
	function regUser ($profile) {
		global $_CONF, $_CORE, $_AUTH;
		// ������ ��������� ����� �������
		$LoginzaProfile = new LoginzaUserProfile($profile);
		
		// ��������������� ������
		$gen_password = $LoginzaProfile->genRandomPassword();
        if(!isset($profile->uid)){
    		$arr_ident = explode('/',$profile->identity);
            $ident = array_pop($arr_ident);
        }else{
            $ident = $profile->uid;
        }
        $author_bd = '';
        if(substr($profile->dob,0,4)!='0000' && !empty($profile->dob) ) $author_bd = date('Y-m-d', strtotime($profile->dob));
		$data = array(
            'author_comment'        => iconv("UTF-8", "windows-1251",(string)$profile->name->first_name),
            'author_fam'            => iconv("UTF-8", "windows-1251",(string)$profile->name->last_name),
			'author_nik'		    => iconv("UTF-8", "windows-1251",$LoginzaProfile->genNickname()),
			'author_passwd'			=> $gen_password,
			'author_login'			=> 'loginza'.$ident,//strtolower($profile->email),
			'author_bd'			    => $author_bd,
      'author_sex'			  => (string)$profile->gender,
			'doc' 			=> (string)$profile->photo,
			'loginza_identity' 		=> $profile->identity,
			'loginza_provider'		=> $profile->provider
		);
    if(!empty($profile->email))
      $data['author_login']	= (string)$profile->email;
		// ���� ���� ������
//		if (!count($error)) {
			// ����������� ������ ������������
    	if ($fp = @fopen($data['doc'], 'r')) { 
        list($w_i, $h_i, $type) = getimagesize($data['doc']); // �������� ������� � ��� ����������� (�����)
        $types = array("", ".gif", ".jpg", ".png"); // ������ � ������ �����������
        $ext = $types[$type]; // ���� "��������" ��� �����������, ����� �������� ����
  		  $new_name = time().basename($data['doc']).$ext;
        $abspath	= $_SERVER['DOCUMENT_ROOT'].'/data/auth/';    
    		if (!copy($data['doc'], $abspath.$new_name )) {
    			$data['doc'] = '';
    		}else {

    		  $data['doc'] = $new_name;
    		}
    		fclose($fp);
    	}
			$user_id = $this->save_usr($data);
			return $user_id;
			
/*		} 		
		return false;*/
	}
  
	/**
	*
	*/
	function save_usr( $usr ){
	 if(!empty($usr['loginza_identity'])){
      $next_id = SQL::getval( "MAX(author_id) as max", DB_TABLE_PREFIX.'auth_pers')+1;
      $var = "author_id, alias, author_login, author_passwd, author_comment, author_sex, author_bd, doc, loginza_identity, loginza_provider, accept, created, MODIFY, AUTORISE";
      
      $val = "'".$next_id."'"; // author_id
      $val .= ",'".$next_id."'"; // alias
      $val .= ",'".$usr['author_login']."'"; // author_login
      $val .= ",'".$usr['author_passwd']."'"; // author_passwd
      $val .= ",'".$usr['author_comment']."'"; // author_comment
      $val .= ($usr['author_sex']=='M')?",'1'":",'2'"; // author_sex
      $val .= ",'".$usr['author_bd']."'"; // author_bd
      $val .= ",'".$usr['doc']."'"; // doc
      $val .= ",'".$usr['loginza_identity']."'"; // loginza_identity
      $val .= ",'".$usr['loginza_provider']."'"; // loginza_provider
      $val .= ",'2'";
      $val .= ",'".time()."'";
      $val .= ",'".time()."'";
      $val .= ",'".time()."'";
      SQL::ins(DB_TABLE_PREFIX.'auth_pers', $var, $val);
      if(!empty($next_id)){
          SQL::ins(DB_TABLE_PREFIX.'event_log', 'alias, name, from_auth, ev_type', "'".$next_id."_".(microtime(1)+0)."', '���� �������������� ��� �� ����� SymParty. ������� �� �����������', '".$next_id."', 2 ", DEBUG);
          SQL::ins(DB_TABLE_PREFIX.'event_log', 'alias, name, from_auth, ev_type', "'".$next_id."_".(microtime(1)+1)."', '���������� ������� ��� ����������� (+15��)', '".$next_id."', 2 ", DEBUG);
      }      
      global $_CONF;
      if(!empty($_CONF['settings']['action_vip'])){
          $_CONF['max_balance'] = 30;
      
          $set = " balance = '30'";
          
          $set .= ", vip = '".time()."'";
                  
          SQL::upd(DB_TABLE_PREFIX.'auth_pers',$set,"author_id = '".$next_id."'", DEBUG ) ; 
         
          $_SESSION['SESS_AUTH']['ALL']['balance'] =  $auth['balance']; 
          $_SESSION['SESS_AUTH']['ALL']['vip'] = time();
      
          SQL::ins(DB_TABLE_PREFIX.'event_log', 'alias, name, from_auth, ev_type', "'".$next_id."_".(microtime(1)+2)."', '��� ��������� ������ VIP-������������. ������� ���������� ������� (+15��).', '".$next_id."', 2 ", DEBUG);
      }      
      return $next_id;
    }
    return false;
	}
}

$loginza = new loginza_register();
if(isset($_POST['token'])&& !empty($_POST['token'])){
    $_POST['token'] = mysql_real_escape_string($_POST['token']);
    $loginza->main();
}

?>