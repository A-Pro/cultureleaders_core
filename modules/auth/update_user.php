<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : update_user.php 
// PROJECT NAME : REGISTERS
// DATE         : 04.11.2002
// Author       : Andrew Lyadkov
// Discription  : 
// ...........  : redaktirovanie pol'zovatelja
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////
////////   SETTINGS   /////////////////////////////////
///////////////////////////////////////////////////////

global $_CORE, $CONF;

if ($_CORE->NeedUsers != $_CONF['AddUsersTypes'][2])
	if ($_CORE->NeedUsers==$_CONF['AddUsersTypes'][1] && !$_CORE->IS_ADMIN)
		return;

include_once "lib_includer.php"; // + $db = new


# a zaregistrirovan li?
if (isset($_SESSION['SESS_AUTH'])&&!is_array($_SESSION['SESS_AUTH'])||empty($_SESSION['SESS_AUTH']['LOGIN'])) {
	header("Location: /auth/login.php\n\n");
	exit;
}


global $REG_RESULT, $REG_RESULT_STR, $ERROR_MSG, $auth, $_CORE, $DATA;
$auth	= $_POST['auth'];
$submit	= $_POST['submit'];
  if(is_array($auth)){
		foreach ($auth as $key => $value) {
		  if(is_array($value)){
				$value = implode(',',$value);
			}
			$tr	= array( "'" => '"' );
			$auth[$key]	= stripslashes(strtr($value,$tr));
		}
  }
//$FILE_FORM	= (is_file($_CORE->SiteModDir.'/auth_pers.form.html')) ? 
//				$_CORE->SiteModDir.'/auth_pers.form.html' : 
//				$_CORE->CoreModDir.'/auth_pers.form.html';

Main::comm_inc("auth_pers.form.html", $FILE_FORM, 'auth');

$ERROR_MSG	= '';


///////////////////////////////////////////////////////
////////   CONTROL    /////////////////////////////////
///////////////////////////////////////////////////////


$REG_RESULT = false; 
$REG_RESULT_STR = '';
	
# esli sej pol'zovatel' ne zaregistrirovan to ego na vhod
$val = getval_sql( "author_passwd", TAB_AUTH_PERS, "author_id = '".$_SESSION['SESS_AUTH']['ID']."' " );

if ( md5($val.$_SESSION['SESS_AUTH']['LOGIN']) != $_SESSION['SESS_AUTH']['HASH'] ) {
	session_destroy();															// <--
	header("Location: /auth/login.php?session_lost\n\n");
	exit;
}

_load_land_arr($DATA, $_CONF['LandDatPath'].'/auth_pers.dat');
  if (!empty($_CONF['FEATURES_USED']['partners']) && !$_CORE->IS_ADMIN){
    /* ����� ����, ����� ��� ����, ����� ������������ ? )) ������ ��������?
  $DATA['prtn'] = 
    array (
      'field_name' => 'prtn',
      'name' => 'auth[prtn]',
      'title' => '�������',
      'type' => (empty($_SESSION['SESS_AUTH']['ALL']['prtn'])?'textbox':'hidden'),
      'must' => '0',
      'size' => '20',
      'maxlen' => '20',
      'default' =>  (empty($_SESSION['SESS_AUTH']['ALL']['prtn'])?"":$_SESSION['SESS_AUTH']['ALL']['prtn'])
    );*/
  }
  if (!empty($_CONF['FEATURES_USED']['youtubeapi'])){
if ($_CORE->IS_ADMIN ) {
	  $DATA['youtube_login'] = 
	    array (
	      'field_name' => 'youtube_login',
	      'name' => 'auth[youtube_login]',
	      'title' => 'youtube login',
	      'type' => 'textbox',
	      'must' => '0',
	      'size' => '20',
	      'maxlen' => '255'
	    );
	  $DATA['youtube_pasw'] = 
	    array (
	      'field_name' => 'youtube_pasw',
	      'name' => 'auth[youtube_pasw]',
	      'title' => 'youtube password',
	      'type' => 'textbox',
	      'must' => '0',
	      'size' => '20',
	      'maxlen' => '255'
	    );
  }
}
// dlja administratora ne menjat' login
if ($_CORE->IS_ADMIN) {
	$DATA['author_login']['readonly'] = true;
	if(isset($DATA['author_comment']))
		$DATA['author_comment']['readonly'] = true;
}

global $cform;
$cform	= new cForm($DATA);

# dejstvija 
if (!empty($submit)){ 		// proverjaem prishedshij massiv
	if (check_form( $auth )){
		if ( make_update( ) ) {
			$REG_RESULT = true;

			$_SESSION['SESS_AUTH']['ALL']	= $auth;
			unset($_SESSION['SESS_AUTH']['ALL']['2']);
			unset($_SESSION['SESS_AUTH']['ALL']['author_passwd']);
			$_SESSION['SESS_AUTH']['ALL']['doc'] = ($_REQUEST['doc']) ? $_REQUEST['doc'] : $_REQUEST['old_doc'];
			$_SESSION['SESS_AUTH']['ALL']['doc1'] = ($_REQUEST['doc1']) ? $_REQUEST['doc1'] : $_REQUEST['old_doc1'];
			$_SESSION['SESS_AUTH']['ALL']['doc2'] = ($_REQUEST['doc2']) ? $_REQUEST['doc2'] : $_REQUEST['old_doc2'];

			$REG_RESULT_STR = _get_error_str("������ ��������!");
		}
	} else {
		$REG_RESULT_STR = $ERROR_MSG;
	}
} else {
	load_data($_SESSION['SESS_AUTH']['ID']);
}


# form 
//include $_CORE->CoreModDir."/menu.php";

if(!defined('MOBILE_FACE')) {
	echo ($REG_RESULT_STR) ? "<p class=warning>".$REG_RESULT_STR."</p>" : '';
}

if (!$REG_RESULT) {
	if (empty($auth['author_photo'])&& !empty($old_author_photo))
		$auth['author_photo'] = $old_author_photo;
	include "$FILE_FORM";
	
}
else {
	Main::comm_inc("after_update.html", $file, 'auth');
	if ($file) include "$file";

}

///////////////////////////////////////////////////////
////////   FUNCTIONS  /////////////////////////////////
///////////////////////////////////////////////////////

 //////////////////////////////////////////////////////	 check_form( &$arr )
//
function check_form( &$arr )
{
	global $ERROR_MSG, $DATA, $_CONF, $cform, $_CORE;

//	for(reset($DATA);list($k,$v)=each($DATA);){
//		# objazatel'nye 
//		if (!empty($v['must'])&&(!isset($arr[$k]) || $arr[$k]=='')) {
//			$ERROR_MSG .= _get_error_str('���� "'.$v['title'].'" �����������.');
//		}
//		# dlinna 
//		if (strlen($arr[$k])>$v['maxlen'] || $$k) {
//			$ERROR_MSG .= _get_error_str('� ���� "'.$v['title'].'" ������� ����� ��������.');
//		}
//	}

	if ($cform->Chk_come())
		$ERROR_MSG .= _get_error_str($cform->Error);

	if (!empty($cform->Bug_mes))
		echo $cform->Bug_mes;
	if (!empty($arr['author_passwd']) && $arr['author_passwd']!=$arr['author_passwd2']) {
		$ERROR_MSG .= _get_error_str('������ �� ���������.');
	}

	if (!empty($arr['author_email']) && !_is_email($arr['author_email'])) {
		$ERROR_MSG .= _get_error_str('������� ������ E-MAIL.');
	}

	// esli ne admin, to logi - e-mail
	if (empty($arr['author_email']) && $arr['auth_login'] != 'admin' && !_is_email($arr['author_login']) && !$_CORE->IS_ADMIN) {
		$ERROR_MSG .= _get_error_str('������� ������ E-mail.');
	}
	
	# check login (sql search)
	$login = $arr['author_login'];
	if (getval_sql( "author_login", TAB_AUTH_PERS, " author_login = '$login' AND author_id != '".$_SESSION['SESS_AUTH']['ID']."' ")>0) {
		$ERROR_MSG .= _get_error_str('����� ����� ��� ���������������.');
	}
//	if (getval_sql( "COUNT(*) as count", TAB_AUTH_PERS, "author_id != '".$_SESSION['SESS_AUTH']['ID']."' AND author_email = '$arr[author_email]' ", '', 0)>0) {
//		$ERROR_MSG .= _get_error_str('����� � ����� Email`�� ��� ��� :( .');
//	}

	# check data
	for(reset($arr);list($n,$v)=each($arr);) {
		if (strchr($v,"'")) {
			$arr[$n] = stripslashes($v);
			$ERROR_MSG .= _get_error_str('������������ ������ � ����: '.$DATA[$n]['title']);
		} else {
			$arr[$n] = clear_data($v);
		}
	}
	return ($ERROR_MSG == '');
}

 //////////////////////////////////////////////////////	 make_insert_add( &$arr )
//
function make_update( )
{

	global $DATA, $auth, $cform,$doc, $doc1, $doc2;
	#
	# esli parol' ne menjaem, to vykinim ego iz spiska
	#
	if ($auth['author_passwd'] == '' ) {
		unset($cform->Arr['author_passwd']);
	} else {
		$auth['author_passwd']	=  ($_CONF['PWDCRYPTED']) ? crypt($auth['author_passwd'],SALT) : $auth['author_passwd'];
		$_SESSION['SESS_AUTH']['PASSWD']	= $auth['author_passwd'];
	}
	unset($cform->Arr['author_passwd2']);
	// nel'zja smenit' login !!! 14.08.2011 teepr' mozhno tol'ko ne adminu
	if ($auth['auth_login'] == 'admin') {
		unset($cform->Arr['author_login']);
	}
	

	$now_time = time();
	$upd	= $cform->GetSQL('update');
	$upd	.= (!empty($upd)) ? ", " : '';
	$upd	.= " modify = '$now_time'";

	$update = update_sql( TAB_AUTH_PERS, $upd, "author_id = '". $_SESSION['SESS_AUTH']['ID']."'");

//	if (!empty($auth['author_passwd']))

		$_SESSION['SESS_AUTH']['NAME']		= isset($auth['author_name']) ? $auth['author_name'] : $auth['author_comment'];
		if(empty($_SESSION['SESS_AUTH']['NAME'])) $_SESSION['SESS_AUTH']['NAME']		= $auth['author_company'];	
        $_SESSION['SESS_AUTH']['PERSON']	= (!empty($_SESSION['SESS_AUTH']['NAME'])) ? $_SESSION['SESS_AUTH']['NAME']:$_SESSION['SESS_AUTH']['LOGIN'];

		$_SESSION['SESS_AUTH']['ALL']	= $auth;
		unset($_SESSION['SESS_AUTH']['ALL']['2']);
		unset($_SESSION['SESS_AUTH']['ALL']['author_passwd']);   
                 
        unset($_REQUEST['submit']);
             
        
	if (!empty($doc)) {
		$_SESSION['SESS_AUTH']['AVATAR']	= '/data/auth/' . $doc;
	}
	if (!empty($doc1)) {
		$_SESSION['SESS_AUTH']['AVATAR']	= '/data/auth/' . $doc1;
	}
    
    if ($result->Result)
	   return true;
      else
        return false;   
}

 //////////////////////////////////////////////////////
//
function load_data( $author_id ){

	$get = select_sql('*', TAB_AUTH_PERS, "author_id = '$author_id'");
	$get->FetchArray(0);
	global $auth, $doc, $doc1, $doc2;
	$auth = $get->FetchArray;
    //print_r($auth); exit;
	$auth['author_passwd2'] = $auth['author_passwd'] = '';
	$doc = $auth['doc'];
	$doc1 = $auth['doc1'];
	$doc2 = $auth['doc2'];
}



/*

DATA:
----

auth = Array
(
	pishem.

    [author_login] => auth_pers
    [author_passwd] => auth_pers
    [author_name] => auth_pers
	[author_male] => auth_pers
    [author_email] => auth_pers

	ubivaem.
    [author_passwd2] => auth_pers
)


*/
?>