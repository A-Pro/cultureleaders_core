<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : sql_query_lib.php 
// PROJECT NAME : SQL/Lib
// DATE         : 12.06.2002 Last 29.10.2002
// Author       : Andrew Lyadkov
// Discription  : 
// ...........  :
// Comment      : for USE class QUERY, CONNECTION
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
/*
LOGO: 

29.10.2002 - getval_sql();

*/

///////////////////////////////////////////////////////
////////   DB FUNCTIONS  //////////////////////////////
///////////////////////////////////////////////////////


 ////////////////////////////////////////////////////// insert_sql()
//
function insert_sql( $into, $var, $val, $debug = 0 )
{
	global $db;
	$cmd	= "INSERT INTO $into ( $var ) VALUES ( $val ) ";
	echo	($debug) ? "<p>".$cmd."</p>\r\n": '';
	$result	= new Query( $db->Connect, $cmd );
	echo (!$result->Result) ? "Error : ".$result->ErrorQuery : '';
	return $result;
}

 //////////////////////////////////////////////////////	 update_sql()
//
function update_sql( $table, $set, $where='', $debug = 0 )
{
	global $db;
	$cmd = "UPDATE $table SET $set ";
	$cmd .= (!empty($where)) ? " WHERE $where " : '';
	echo	($debug) ? "<p>".$cmd."</p>\r\n" : '';
	$result	= new Query( $db->Connect, $cmd );
	echo (!$result->Result) ? "Error : ".$result->ErrorQuery : '';
	return $result;
}

 ////////////////////////////////////////////////////// select_sql() 
//
function select_sql( $what, $from, $where='', $tail='', $debug=0 )
{
	global $db;
	$cmd = "SELECT $what FROM $from ";
	$cmd .= (!empty($where)) ? " WHERE $where " : '';
	$cmd .= $tail;
	echo ($debug) ? "<p>".$cmd."</p>\r\n": '';
	$result	= new Query( $db->Connect, $cmd );
	echo (!$result->Result) ? "Error : ".$result->ErrorQuery : '';
	return $result;
}

 //////////////////////////////////////////////////////	 delete_sql()
//
function delete_sql( $table, $where='', $debug = 0 )
{
	global $db;
	$cmd = "DELETE FROM $table ";
	$cmd .= (!empty($where)) ? " WHERE $where " : '';
	echo	($debug) ? "<p>".$cmd."</p>\r\n": '';
	$result	= new Query( $db->Connect, $cmd );
	echo (!$result->Result) ? "Error : ".$result->ErrorQuery : '';
	return $result;
}

 //////////////////////////////////////////////////////
//
function getval_sql( $what, $from, $where='', $tail='', $debug=0 ){
	$res = select_sql($what,$from,$where,$tail, $debug);
	if ($res->NumRows > 0) {
		$res->FetchArray(0);
		return $res->FetchArray[0];
	}else
		return false;
}

/**
*
*/
function limit_sql( $offset, $limit)
{
	global $db; 
	if (empty($offset)) $offset = 0;
	if (empty($limit)) $limit = 0;
	switch ($db->DBType) {
		case 'mysql':
			return 	 " LIMIT $offset, $limit ";
			break;
		case 'pgsql':
			return 	 " OFFSET $offset LIMIT $limit ";
			break;
	}
}
/**
*
*/
function get_offset_sql($o,$l,$type='pgsql')
{
	global $db; 
	if (empty($offset)) $offset = 0;
	if (empty($limit)) $limit = 0;
	switch ($type) {
		case 'mysql':
			return 	 " LIMIT $o, $l ";
			break;
		case 'pgsql':
			return 	 " OFFSET $o LIMIT $l ";
			break;
	}
} 
?>