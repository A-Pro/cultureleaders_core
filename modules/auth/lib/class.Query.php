<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : class.Query.php
// PROJECT NAME : CLASSES/Land
// DATE         : 26.09.2002
// Author       : Andrew Lyadkov (andrey@lyadkov.nnov.ru)
// Discription  : ��������� �������� ���� ������ MySQL
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////

	class Query {
		// ����������� ���������� ��� ������ � ����� ������
		var $Result;	// ��������� ��������� �������
		var $NumRows;	// ���������� �������
		var $FetchRow;	// ������ ���������� ������� ������
		var $FetchArray;// Added by Land 10.12.01
		var $Tuples;	// ���������� �����������, ���������� � ��������� �������
		var $Destroy;	// ��������� �� ������������ ������ ���������� ���������

		// ����������� ���������� ������
		var $ErrorQuery;	// ������ ��������� �������
		var $ErrorFetchRow;	// ������ ��������� ���������� �������
		var $ErrorNumRows;	// ������ �������� ���������� ������� ���������� �������
		var $ErrorTuples;	// ������ �������� ���������� �����������, ���������� � ��������� �������

		//////////////////////////////////////////////
		//			��������� ��������				//
		//////////////////////////////////////////////
		function Query($Connect_id,$Query) {
			if (empty($Connect_id)) {
				$this->ErrorQuery = "No connect to database.";
				return;
			}
			// ������������ ������ � ���� ������
			$this->Result = mysql_query($Query);

			// �������� ������������ ������������� �������
			if (!$this->Result) {
				$this->ErrorQuery = mysql_error($Connect_id);
				return;
			}
			else {
				// ������ ���������� �������
				$this->NumRows = @mysql_num_rows($this->Result);
			}
		}// End of function Query

		//////////////////////////////////////////////
		//      ��������� ����������� �������       //
		//////////////////////////////////////////////
		function FetchRow($Row) {
			// ������������ ��������� ������� ���������� ������
			$this->FetchRow = mysql_fetch_object($this->Result);

			// �������� ������������ ��������� �������
			if (!$this->FetchRow) {
				$this->ErrorFetchRow = mysql_error($this->FetchRow);
			}
		}// End of function FetchRow

		//////////////////////////////////////////////
		//      ADDED by Land (FETCH_ARRAY)       //
		//////////////////////////////////////////////
		function FetchArray($Row) {
			// ������������ ��������� ������� ���������� ������
			$this->FetchArray = mysql_fetch_array($this->Result);

			// �������� ������������ ��������� �������
			if (!$this->FetchArray) {
				$this->ErrorFetchRow = mysql_error($this->FetchRow);
			}
		}// End of function FetchRow


		//////////////////////////////////////////////
		//		��������� ����������� �������		//
		//////////////////////////////////////////////
		function Destroy() {
			// ������������ ������������ ������
			if (!$this->Result) return;
			$this->Destroy = @mysql_free_result($this->Result);

			// �������� ������������ ������������ ������
			if (!$this->Destroy) {
				$this->ErrorDestroy = mysql_error();
			}
		}// End of function Destroy
	}// End of class Query
?>
