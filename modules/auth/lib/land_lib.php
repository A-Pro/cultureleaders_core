<?


	//////////////////////////////////////////////////////////////////////
	//	land_lib.php - ���������� ���� ��� ������ � ����������			//
	//******************************************************************//
	//					(c) Copyright 2002 Inforis						//
	//						http://www.inforis.ru						//
	//------------------------------------------------------------------//
	//					Programming by Andrey Lyadkov					//
	//							land@inforis.ru							//
	//////////////////////////////////////////////////////////////////////


  ///////////////////////////////////////////////////////////////////
 //
// Useful  Functions for different projects
// Author: Land
/* LOGO

07.04.2003 changed: _tab_HTML2select
20.08.02  Added : function date2str( $str )

*/
/////////////////////////////////////////////////////////////////////

if ( !defined('IN_LAND') )
{
	die("Hacking attempt");
}
/**												get_post_var( $var_name )
*
*/
function get_post_var( $var_name ){
	$ret = post_var($var_name);
	if ($ret == '') {
		$ret = get_var( $var_name );
	}
	return  $ret;
}

/**												get_var( $var_name )
*
*/
function get_var( $var_name ){
	global $HTTP_GET_VARS;
	return (!isset($HTTP_GET_VARS[$var_name])) ? '' : $HTTP_GET_VARS[$var_name];
}

/**												post_var( $var_name )
*
*/
function post_var( $var_name ){
	global $HTTP_POST_VARS;
	return (!isset($HTTP_POST_VARS[$var_name])) ? '' : $HTTP_POST_VARS[$var_name];
}



///////////////////////////////////////////////////////////   is_email 
//----------------------------------------------------------------------
//  - simple checker string for email, checking structure: *@*.* + !' '
function _is_email ( $email ) 
{
	if ( empty( $email ) || ! strchr( $email, '@' )) return 0;
	list($first, $domain) = explode("@", $email);
	$point = strpos($domain, ".");
	if ( strstr($email, " ") || $point == 0 || $point == strlen( $domain ) || strlen($first) == 0 ) return 0;
	else return 1;
}


///////////////////////////////////////////////////////////   _convert_str
//------------------------------------------------------------------------
//  Converting String: 
//	1. Strip Tags	
//	2. Strip Slashes
//	3. Replace ' -> `
function _convert_str ( $string )
{
	return str_replace("'","`",stripslashes(strip_tags($string)));	
}


 ///////////////////////////////////////////////////////////  _tab_HTML2select
//------------------------------------------------------------------------
//  Creating HTML code <SELECT> with field of table: $table
//    name : $pref[$id]
//   value : field = $id
//  String : field = $field
//   
//----------
// if ($add_all) then adding line "<option value='0'>���</option>"
// GLOBAL $DB = class Connection
//
// TESTED 4/17/02
// 
function _tab_HTML2select ( $pref, $val, $id, $field, $table, $add = false, $where='', $tail='', $foreign_id='' ) 
{
	global $db;
	$str = "<select name=\"". sprintf ("%s[%s]",$pref,$id)."\">";
	if ($add) {
		switch ($add) {
			case 'empty':
				$str .= "<option value='0'></option>";
				break;
			default:
				$str .= "<option value='0'>���</option>";
		}
	}
	$sql = "SELECT * FROM $table ";
	$sql .= (empty($where)) ? '' : "WHERE $where";
	$sql .= (empty($tail)) ? '' : " $tail ";
	$result = new Query( $db->Connect, $sql );
	if (!$result->Result) 
	{
		echo "Base Error:".$result->ErrorQuery."SQL: $sql";
		return '';
	}
	for($j=0; $j < $result->NumRows; $j++) 
	{
		$result->FetchRow($j);
		$row = $result->FetchRow;
		$str .= ($foreign_id=='')?sprintf("<option value=\"%s\"", $row->$id):sprintf("<option value=\"%s\"", $row->$foreign_id);
		if ($foreign_id!='') {
			if ( $row->$foreign_id == $val ) 
				$str .= " SELECTED ";
		}else
			if ( $row->$id == $val ) 
				$str .= " SELECTED ";
		$str .= sprintf(">%s</option>", $row->$field);
	}
	$str .= "</select>\n\n";

	return $str;
}

 ////////////////////////////////////////////////////////  _tab_Col2Arr
//  -----------------------------------------------------------------
//  converting table columnS into Array
//  ver 2.0 simple
//
//  tested 12/03/02
// 
function _tab_Col2Arr( &$arr, $tab_name, $field=1, $key=0, $where='' )
{
	global $db;
	$get_cols = (is_array($field)) ? join(",",$field) : $field;
	$select = (!empty($field) && !empty($key)) ? " $get_cols, $key " : " * ";
	$cmd = "SELECT $select FROM $tab_name";
	$cmd .= (empty($where)) ? '' : " WHERE $where ";
	$result = new Query( $db->Connect, $cmd );
	if ($result->Result && $result->NumRows > 0)
	{
		for ( $i=0; $i < $result->NumRows; $i++ )
		{
			$result->FetchArray($i);
			$arr[$result->FetchArray[$key]] = (is_array($field)) ? $result->FetchArray : $result->FetchArray[$field];
		}
		return 1;
	}
	else
	{
		global $LandError; 
		$LandError= $cmd;
		return 0;
	}
}	



 ////////////////////////////////////////////////////////  _tab_GetVal()
//  -----------------------------------------------------------------
//  Take one Box from the Table
//  ver 1.0 simple
//
function _tab_GetVal( $tab_name, $where, $field=1 )
{
	global $db;
	$cmd = "SELECT $field FROM $tab_name WHERE $where";
	$result = new Query( $db->Connect, $cmd );
	if ($result->Result && $result->NumRows > 0)
	{
		$result->FetchArray(0);
		return $result->FetchArray[$field];
	}
	else
		return 0;
}	


///////////////////////////////////////////////////////////  _arrHTML2select
//------------------------------------------------------------------------
//  Creating HTML code <SELECT> with array: $array
//    name : $pref[$name]
//   value : $key
//  String : $value
//   
//----------
// if ($add_empty) then adding line "<option value=''>- ������� -</option>"
// GLOBAL $DB = class Connection
//
// NOT TESTED 3/11/02
// 
function _arrHTML2select ( $pref, $val, $name, $array, $add_empty=false )
{
	$str = "<select name=\"".sprintf ("%s[%s]",$pref,$name) ."\">";
	if ( $add_empty ) 
		$str .= "<option value=''>- ������� -</option>";
	foreach( $array as $key=>$value) 
	{
		$str .= sprintf("<option value=\"%s\"", $key);
		if ( $key == $val ) 
			$str .= " SELECTED ";
		$str .= sprintf(">%s</option>", $value);
	}
	$str .= "</select>";
	return $str;
} 

///////////////////////////////////////////////////////////  _arrHTML2select
//------------------------------------------------------------------------
//  print script to confirm smthing,(del), and after that change window location
//   
//----------
// TESTED 3/27/02
// 
function _print_js_real_del( $str = '�� ������������� ������ �������'  )
{
//<A HREF=\"#\" OnClick=\"real_del('action.php?cont=delparam&time_id=$time_id&event_id=$event_id');return false\"; >�������</A>";
?><script language="Javascript">
<!--
function real_del( link ){
if(confirm('<?=$str?>'))window.location = link;
}
//-->
</script><?
} // func

///////////////////////////////////////////////////////////  _show_calendar
//------------------------------------------------------------------------
//  ������� ������� � ����������, 
//  
//  ������� ���������: 
//  $month - �� ����� ����� �������� ��������� (���� ����������� ��� ����, �� �� ������� �����)
//	$link - 
//			������ ��� ������� ���. ���� �� ������ �� � ����� 
//			����� ������: HREF="$link$a.$month.$year" - ��� $a - ���� DD, $mounth - ����� MM, $year - ��� YY
//			�����: HREF="#"
//	$OnClick_func, $submit - 
//			�������� JS ������� ���� �� ������ �� ����������� �������: 		
//			�����:  OnClick=\"$OnClick_func( '$a-$month-$year,' ); return $submit;\"
//   
//	$day, $year - ���������������. � �� ������������
//----------
// TESTED 3/27/02 by Land
// 
function _show_calendar( $month=0, $link = '', $yType = 'y', $OnClick_func='', $submit=false, $day=0, $year=0 )
{
	if (empty($month)) $month = date("n",time());
	$year = (empty($year)) ? date("$yType",time()) : $year;
	$padding = date("m",time());
	$day = (empty($day)) ? date("d",time()) : $day;

	//	Get info for Calendar generation
	$daysmonth = date("t",mktime(0,0,0,$month,$day,$year));
	$firstday = date("w",mktime(0,0,0,$month,1,$year));
	$padmonth = date("m",mktime(0,0,0,$month,$day,$year));
	$padday = date("d",mktime(0,0,0,$month,$day,$year));

	$currdate = date("d",time());
	$currmonth = date("n",time());

	//	Customize according to $calendar_format
			if ($firstday == 0) 
				$firstday = 7;
			$date = "$year-$padmonth-$padday";
			$dayletter = array(1 => "<font class='cal_days' color='#000000'>��</font>", 2 => "<font class='cal_days' color='#000000'>��</font>", 3 => "<font class='cal_days' color=#000000>��</font>", 4 => "<font class='cal_days' color=#000000>��</font>", 5 => "<font class='cal_days' color=#000000>��</font>", 6 => "<font class='cal_days' color='#ff0000'>��</font>", 7 => "<font class='cal_days' color=#ff0000>��</font>");
			$monthlet = array(1 => "������", '01' => "������",2 => "�������", '02' => "�������", '03' => "����", 3 => "����", 4 => "������", '04' => "������", 5 => "���", '05' => "���", 6 => "����", '06' => "����", 7 => "����", '07' => "����", 8 => "������", '08' => "������", 9 => "��������", '09' => "��������", 10 => "�������", 11 => "������", 12 => "�������");
			$daymod = 0;

        if  ($month > 12 ) {
            $month = $month % 13 + 1 ;
            if (++$year < 10) $year = '0'.$year;
        }

		if ($month == 1) {
			$pyear = $year - 1;
			$pmonth = 12;
		} else {
			$pyear = $year;
			$pmonth = $month - 1;
		}

		if ($month == 12) {
			$nyear = $year + 1;
			$nmonth = 1;
		} else {
			$nyear = $year;
			$nmonth = $month + 1;
		}
		
	//	Print the dayletters
	echo "<TABLE CELLPADDING=2 CELLSPACING=0 BORDER=0>";
	echo "<TR><TD align=\"center\" colspan=7 class='cal_month'><B><font color='#3399ff'><u>$monthlet[$month]</u></font></B></TD></tr><tr>";
	for($daynumber = 1; $daynumber < 8; $daynumber++) {
		echo "<td align=center><div class=small>$dayletter[$daynumber]</div></a></td>";
	}
	echo "</tr><tr>";
	//	Print the calendar
	for ($i = 1; $i < $daysmonth + $firstday + $daymod; $i++) 
	{
		$a = $i - $firstday + 1 - $daymod;
		$day = $i - $firstday + 1 - $daymod;
		if (strlen($a) == 1)
			$a = "0$a";
		if (strlen($month) == 1)
			$month = "0$month";
		//	Empty images to pad the calendar
		if (($i < $firstday) || ($a == "00")) 
			echo "<td align=\"center\">&nbsp;</td>";
		else 
		{
			if (($currdate == $a) && ($currmonth == $month))
			{
				echo "<td align=\"center\"><a ";
				echo (empty($OnClick_func)) ? '' : " OnClick=\"$OnClick_func( '$a-$month-$year,' ); return $submit;\" ";
				echo (empty($link)) ? "  HREF='#' " : " HREF=\"$link$a.$month.$year\" ";
				echo "><div class=small><FONT color=red>$a</FONT></div></a></td>";
			}
			else
			{
				echo "<td align=\"center\"><a  ";
				echo (empty($OnClick_func)) ? '' : " OnClick=\"$OnClick_func( '$a-$month-$year,' ); return $submit;\" ";
				echo (empty($link)) ? "  HREF='#' " : " HREF=\"$link$a.$month.$year\" ";
				echo "><div class=small>$a</a></div></td>";
			} // else ($curdate)
		} // else ($i < $firstday
		if (($i%7) == 0) 
			echo "</tr><tr>\n";
	}
	if (($i%7) != 1) 
		echo "</tr>\n";
	echo "</TABLE>";
}


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
//////////////////// ���������� ��� ������ � ������ /////////////////
//////////////////// � �������                      /////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////      FOR ADD     ////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// $function - keep the name of function wich return SQLQuery String ecoding with form
// *** ���������� � �����
// Include file: $check_file, run the $function 
// And it must return Result, and array of SQL Query Strings,
// And after, if Everithing is OK, Exec that Queries.
function add_to_base ( $function='' ) // ���������� ������ (add)
{
	if (empty($function)) return 0;
	global $db, $check_file;
	if (!empty($check_file)) 
		include $check_file; // INCLUDE
	$event = "\$chk_res = $function;";
	eval($event);
	if ( $chk_res ) 
	{
		$OK = 1;
		// ��������� � ����
		foreach ( $chk_res as $value )
		{
			$result = new Query( $db->Connect, $value );
			//echo $value."<BR>";
			$OK = $OK && $result->Result;
			echo ($result->Result) ? "" : "<p class=warning>".$result->ErrorQuery."</p>SQL:$value<br>";
			if (!$OK) 
				break;
		}
		// ��������� ��� ���� �������������
		if ( $OK ) 
		{
			echo "<P class=message>�������� �������</P>"; //string
			return true;
		}
		else
		{
			echo "<P class=warning>�������� �� �������</P>"; // string
			return false;
		}
	}
	else 
		return false;
} // func add_to_base



 //////////////////////////////////////////////////////
//
// ��������� ���� ������ : 1,2,3 => � 1 �� 3
function date2str( &$str )
{
	$monthName = array('01' => '������', '02' => '�������', '03' => '�����', '04' => '������', '05' => '���', '06' => '����', '07' => '����', '08' => '�������', '09' => '��������', '10' => '�������', '11'  => '������', '12' => '�������');
	$dayName = array('<font class=small>[��]</font>','<font class=small>[��]</font>', '<font class=small>[��]</font>', '<font class=small>[��]</font>','<font class=small>[��]</font>', '<font class=small>[��]</font>', '<font class=small>[��]</font>');
	$str2arr = array_unique(explode(',',$str));
	$out		= '';
	$start		= -2;
	$start_m	= -2;
	$middle		= false;
	$first		= true;
	foreach ($str2arr as $key => $value) 
	{
		if (empty($value)) continue;
		if ($first) 
		{
			list($day,$month,$year) = explode('.', $value);						// ����� ����
			$prev		= $day;$prev_m		= $month;$prev_y = $year;
			$first		= false;
			continue;
		}
		list($day,$month,$year) = explode('.', $value);							// ����� ����
		$day = intval($day);
		$inLastMonth = intval(date('t',mktime(0,0,0,$month-1,2,$year)));		// ���� � ������� ������
		if (intval($day)-intval($prev) == 1 || intval($day)==1 && intval($prev) == $inLastMonth)
		{
			// ���� ��� ������
			if (!$middle) 
			{
				// ������ ��� ������
				$start = $prev;$start_m = $prev_m;$start_y = $prev_y;
				$prev = $day; $prev_m = $month; $prev_y = $year;				// ���������
				$middle = true;
			}
			else
			{	// ��� ������ ������
				$prev = $day; $prev_m = $month;	$prev_y = $year;				// ���������
			}
		}
		else 
		{
			$out .= (empty($out)) ? '' : ',';

			// ����� ��� ���� �� ������
			if ($middle) 
			{
				// ���� ��� ���������
				$out .= " c ".$start." ".$monthName[$start_m]." ".$dayName[date('w',mktime(0,0,1,$start_m,$start,$start_y))]." �� ".$prev." ".$monthName[$prev_m]." ".$dayName[date('w',mktime(0,0,1,$prev_m,$prev,$prev_y))]; // ��������� 
				$start = -2; $start_m = -2;$middle=false; 						// ��� � ������
				$prev = $day; $prev_m = $month;									// ���������
			}
			else 
			{
				// � ������� ������ 
				$out .= " ".$prev." ".$monthName[$prev_m]." ".$dayName[date('w',mktime(0,0,1,$prev_m,$prev,$prev_y))];
				$prev = $day; $prev_m = $month;									// ���������
			}
		}//if intval...
	}//foreach
	if ($middle) // � ���� � ����� �������� ����, �� ������� ���
	{
		$out .= (empty($out)) ? '' : ',';
		$out .= " c ".$start." ".$monthName[$start_m]." ".$dayName[date('w',mktime(0,0,1,$start_m,$start,$start_y))]." �� ".$prev." ".$monthName[$prev_m]." ".$dayName[date('w',mktime(0,0,1,$prev_m,$prev,$prev_y))]; // ��������� 
	}
	if ($out=='') {
		$out .= $day." ".$monthName[$month]." ".$dayName[date('w',mktime(0,0,1,$month,$day,$year))]; // ��������� 
	}
	return $out;
}


?>
