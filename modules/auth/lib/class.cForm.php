<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : class.cForm.php 
// PROJECT NAME : BASE Work
// DATE         : 17.03.2002  (Last UpDate: 10.10.02 )
// Author       : Andrew Lyadkov
// Discription  : For pleasant working with html forms
// ...........  :
// Comment      : Ver: 2.0
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
/*
LOGO
07.04.2003	- change ->show(). for sub_type == 'mp3,photo'
10.10.02 -	add Print_styled ���� ������ ���� ������� � �� ���� cont['alt']
			�� ��� ���� ������� 
01.10.02 -	add: chg_val($val['name'],$value);
			����� ���� ���������������� �������� ���� new_func ��� �������� � ������ ��������
26.09.02 -	htmlspecialchars in output
31.03.02 -	����� ������� Print_styled ( &$styles, &$table = '' )
3/29/02  -	����������� ����� PrintCome( $how, $what, $titlef )
3/26/02  -	����������� ����� PrintCome( $how, $what )



class cForm DESCRIPTION:

VARS:	
-----
		var $Error;					//  Last Error Message
		var $Bug_mes;				//  Message for caching bugs
		var $Arr = array();			//  Array wich we using

		// CSS
		var $Form_TD_title = array(		// Body_print <TD>title</TD> ATTRIBUTE
			'class'	=>	'',
			'align'	=>	'',
			'bgcolor'	=> ''
		);
		var $Form_TD_val = array(		// Body_print <TD>values</TD> ATTRIBUTE
			'class'	=>	'',
			'align'	=>	'',
			'bgcolor'	=> ''
		);

FUNCTIONS:
----------
		PUBLIC:
			cForm( & $Array ) - Constructor
			Body_print ( )
			Chk_come ( )
			GetSQL ( $action = 'insert' )		
			Print_come ( $how='column', $what='', $titlef = '' )
			Print_styled ( &$styles, &$table = '' )
		
		PRIVATE;
			is_must( $val )
			show ( $field, $showval=1 ) 
			Conv_str ( $string )
			global_val( $name )
			get_key( $string )
			chk_photo( &$field )
			copy_file( & $field )

USAGE:
------
	$Arr = array( element = array () )
	==================================
	[COMMON]
		'title'	=> string,
		'name'	=> string,
		'must'	=> [ 1 | 0 ],
		'type'	=> 'textbox | password | hidden | textarea | select | checkbox | file | photo'
		'func'	=> string = eval( 'func' ) - must print on page
*/

class cForm
{
	var $Error;					//  Last Error Message
	var $Bug_mes;				//  Message for caching bugs
	var $Arr = array();			//  Array wich we using
	var	$NoTag = false;			// strip_tags or not
	var $EnabledTags	= "<b><a><i><h1>";
	var $skipfoto;

	// CSS
	var $Form_TD_title = array(		// Body_print <TD>title</TD> ATTRIBUTE
		'class'	=>	'tab',
		'align'	=>	'left',
		'bgcolor'	=> ''
	);
	var $Form_TD_val = array(		// Body_print <TD>values</TD> ATTRIBUTE
		'class'	=>	'tab',
		'align'	=>	'left',
		'bgcolor'	=> ''
	);
	var $Print_Table = array(		// Print_come <TABLE ATTRIBUTES>
		'width'	=> '',
		'spacing'	=> '1',
		'padding'	=> '3',
		'border'	=> '0'
	);


	///////////////////////////////////////////////////////////////////// cForm
	function cForm( & $Array, $NoTag = false ) // CONSTRUCTOR
	{
		$this->Arr = $Array;		
		$this->NoTag = $NoTag;
	}

	 ///////////////////////////////////////////////////// Body_print ()
	// -----------------------------------------------------------------------
	// $form_arr = array ( array( 'title', 'func', 'must', 'name', 'type' .... 
	// Include: is_must(), show()
	// 17.03.2002
	// PUBLIC
	function Body_print ( )
	{
		foreach ($this->Arr as $key => $val)
		{
			echo "<TR><TD valign=top  align=right ";
			echo (!empty($this->Form_TD_title['class'])) ? ' class = '.$this->Form_TD_title['class'] : '';
			echo (!empty($this->Form_TD_title['align'])) ? ' align = '.$this->Form_TD_title['align'] : '';
			echo (!empty($this->Form_TD_title['bgcolor'])) ? ' bgcolor = '.$this->Form_TD_title['bgcolor'] : '';
			echo  ">".$this->is_must($val).$val['title']."</TD><TD";
			echo (!empty($this->Form_TD_val['class'])) ? ' class = '.$this->Form_TD_val['class'] : '';
			echo (!empty($this->Form_TD_val['align'])) ? ' align = '.$this->Form_TD_val['align'] : '';
			echo (!empty($this->Form_TD_val['bgcolor'])) ? ' bgcolor = '.$this->Form_TD_val['bgcolor'] : '';
			echo ">";
			echo (empty($val['func'])) ? $this->show($val) :  eval($val['func']);
			echo  "</TD></TR>";
		}
	} //func

	 ///////////////////////////////////////////////////// Chk_come ()
	// -----------------------------------------------------------------------
	// Checking Incoming form data for "MUST BE" fields
	// $arr = array ( array( 'title', 'must', 'name', 'type' ) )
	// Include:	global_val;
	// 17.03.2002 (01.10.02)
	// PUBLIC
	function Chk_come ( )
	{
		$BAD = false;
		$this->Error = "��������� ���������� �����:<BR>\n"; // string
		foreach ($this->Arr as $key => $val)
		{
			$value = $this->global_val( $val['name'] );
			// ���� ����� ������������� 
//			if (isset($val['newname_func'])) 
//			{
//				echo $value;
//				eval("$val[newname_func]");
//				echo $value;
//				$this->chg_val($val['name'],$value);
//				$value = $this->global_val( $val['name'] );
//				echo $value;
//			}
			if ( !empty($val['must']) && empty($value) )
			{
				$this->Error .= "- ����������� ������������ ����: ".$val['title'].".<BR>"; // string
				$BAD = true;
			}
			$BAD = ($BAD || ( $val['type'] == 'photo' && !$this->chk_photo( $val ) ));
			// echo "BAD(".intval($BAD).")-$val[type]-$value-\n<br>";
		}
		if (!$BAD) 
			$this->Error = '';
		return $BAD;
	} //func

	 ///////////////////////////////////////////////////// GetSQL ()
	// -----------------------------------------------------------------------
	// Make SQL Query String by using incomming form information.
	// $action = [update|insert]; if !update it`s mean: insert
	// Include: global_val, get_key, Conv_str 
	// 17.03.2002
	// PUBLIC
	function GetSQL ( $action = 'insert' )
	{
		global $CHATSET, $SOURCE_CHARSET;
		$empty = 1;
		$sql_vars = $sql_vals = $sql_update = '';
		foreach ($this->Arr as $key => $val)
		{
			$value = $this->global_val ( $val['name'] );
			// Checking Char-Coding
			if (!empty($CHARSET) && !empty($SOURCE_CHARSET) && $CHARSET != $SOURCE_CHARSET)
				$value = convert_cyr_string ($value, $CHARSET, $SOURCE_CHARSET);
			// Go next
			if (!empty( $value ) || $action == 'update') 
			{
				if (empty($val['parse_getin'])) // if don`t need any parsing before
				{
					if ($val['type'] == 'photo') // Photo comin
					{
						if (!empty($value) && !$this->copy_file( $val )){ # bad coping
							return ($action == 'update' ) ? false : array(false,$this->Error);
						}
						// If come here, so the name of a picture already changed
						$value = $this->global_val ( $val['name'] );
					} 
					$field_name = (empty($val['field_name'])) ? $this->get_key( $val['name'] ) : $val['field_name'];
					$value = $this->Conv_str( $value );
					if ($val['type'] == 'photo' && $this->skipfoto) true;
					else
					{
						$sql_vars .= ($empty) ? $field_name : ",".$field_name;
						$sql_vals .= ($empty) ? "'".$value."'" : ",'".$value."'";
						$sql_update .= ($empty) ? sprintf(" %s = '%s'", $field_name, $value ) : sprintf(", %s = '%s'", $field_name, $value );
						$empty = 0;
					}
				}//if val[parse_getin]
			}//if empty
		}//foreach
		$this->Error = '';
		return ($action == 'update' ) ? $sql_update : array($sql_vars,$sql_vals);
	} //func

	 ///////////////////////////////////////////////////// Print_come ()
	// -----------------------------------------------------------------------
	// $form_arr = array ( array( 'title', 'name' ) )
	// Include: global_val
	// 17.03.2002
	// PUBLIC
	function Print_come ( $how='column', $what='', $titlef = '' )
	{
		$str = "<TABLE ";
		$str .= (empty($this->Print_Table['border'])) ? '' : " BORDER=\"".$this->Print_Table['border']."\"";
		$str .= (empty($this->Print_Table['width'])) ? '' : " WIDTH=\"".$this->Print_Table['width']."\"";
		$str .= (empty($this->Print_Table['spacing'])) ? '' : " cellspacing=\"".$this->Print_Table['spacing']."\"";
		$str .= (empty($this->Print_Table['padding'])) ? '' : " cellpadding=\"".$this->Print_Table['padding']."\"";
		$str .= ">\n";
		foreach ($this->Arr as $val)
		{
			$value = $this->global_val( $val['name'] );
			// ��������� ��������� + ������� � ������ ���������(���� ������� ����)
			if (!empty($value) && (is_array($what) && in_array($val['field_name'], $what) || !is_array($what))) 
			{
				$str .= ($how=='column') ? '<TR>' : '';
				switch ($val['type']) 
				{
					case 'photo':
						$str .= "<TD align=center colspan=2><IMG SRC=\"".$val['path']."/".$value."\" BORDER=0 ALT=\"".$val['title']."\">";
						break;
					default :
						$str .= ($how=='column') ? "<TD>".$val['title']."<TD>" : "<TD>";
						if (!empty($val['get_func']))
							eval ($val['get_func']);
						$value = (is_array($titlef) && !empty($titlef[$val['field_name']])) ? sprintf($titlef[$val['field_name']],$value) : $value;
						$str .= $value."\n";
				}//switch
			}//if
		}//foreach
		$str .= "</TABLE>";
		return $str;
	} //func

	 ///////////////////////////////////////////////////// Print_come_styled ()
	// -----------------------------------------------------------------------
	// Include: global_val
	// 10.10.02
	// PUBLIC
	function Print_styled ( &$styles, &$tab_style ) 
	{
		if (!is_array($tab_style)) $tab_style = $this->Print_Table;
		// TABLE 
		$str = '';
		if (!empty($tab_style['draw_table'])) 
		{
			$str = "<TABLE ";
			$str .= (empty($tab_style['border'])) ? '' : " BORDER=\"".$tab_style['border']."\"";
			$str .= (empty($tab_style['width'])) ? '' : " WIDTH=\"".$tab_style['width']."\"";
			$str .= (empty($tab_style['spacing'])) ? '' : " cellspacing=\"".$tab_style['spacing']."\"";
			$str .= (empty($tab_style['padding'])) ? '' : " cellpadding=\"".$tab_style['padding']."\"";
			$str .= ">\n";
		}
		// OTHER
		foreach ($styles as $name => $cont) 
		{
			// ������ ������ �� ���������, �������� �������
			$val = $this->Arr[$name];	  // ������ ������� ������ ������ � ��������� ������ �����
			// ������� ��������
			$value = $this->global_val( $val['name'] );
			// ��������� ��������� + ������� � ������ ���������(���� ������� ����)
			if (!empty($value)&&str_replace(" ",'',$value) !='') 
			{
				if (!empty($val['get_func']))
					eval ($val['get_func']);
				if (empty($cont['var_val']))
					$cont['var_val'] = '';
				switch  ($cont['var_val'])
				{
					case 'val'	:
							$str .= sprintf($cont['printf'],$value);
							break;
					case "val_val":
							$str .= sprintf($cont['printf'],$value,$value);
							break;
					case 'dbval' : 
							$str .= sprintf($cont['printf'],$val['title'],$value, $value);
							break;
					default:
							$str .= sprintf($cont['printf'],$val['title'],$value);							
				}//switch
			}elseif(isset($cont['alt'])){
				$str .= $cont['alt'];
			}//if
		}//foreach

		if (!empty($tab_style['draw_table'])) 
			$str .= "</TABLE>";
		return $str;
	} //func


	 ////////////////////////////////////////////////////// Is_must ()
	//
	// PRIVATE
	function is_must( $val )
	{
		return (empty($val['must'])) ? '' : '&nbsp;*&nbsp;';
	}// func
		

	 ///////////////////////////////////////////////////////////////////// Show()
	// Show
	// ^^^^
	//   ������� ������ ������ ���� �����, �� ����:
	//	������ field, � ������ 'name', 'type' ���� ����� 'size', 'maxlength' ...
	//	�� ������ ������ + php-���(���� �����)
	// ----------------------
	//  version 1.2 - OUT: 
	// ----------------------
	//	textbox		- (need $filed [ type, name, size, maxlen, hideval],  $showval )
	//	password	- (need $filed [ type, name, size, maxlen, hideval],  $showval )
	//	hidden		- (need $filed [ type, name ] )
	//	textarea	- (need $filed [ type, name, rows, cols, hideval],  $showval )
	//	select		- (need $field [ type, name, arr, hideval ] ), $showval )
	//	checkbox	- (need $field [ type, name, hideval ], $showval)
	//	file		- (need $field [ type, name, hideval ] )
	//
	// Inclide: global_val, Conv_str;
	// 17.03.2002
	// PRIVATE
	function show ( $field, $showval=1 ) 
	{
		$value = $this->global_val( $field['name'] );
		$value = (isset($value)) ? $this->Conv_str($value) : '';
		$showval = (!isset($field['hideval']));

		$str = '';
		switch ($field['type']) {
			// ---------------------------------   TEXTBOX
			case 'textbox'	: 
				$str .= '<input type ="text" name= "'.$field['name'].'" ';
				$str .= (!empty($field['size'])) ? ' size="'.$field['size'].'" ' : '';
				$str .= (!empty($field['maxlen'])) ? ' maxlength = "'.$field['maxlen'].'" ' : '';
				$str .= ' class = "';
				$str .= (!empty($field['class'])) ? $field['class'].' ' : '';
				$str .= (!empty($_CORE->CURR_TPL)) ? 'form-control ' : '';
				$str .= '" ';
				$str .= ( $showval ) ? ' value = "'.htmlspecialchars($value).'"' : '';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= " >\n";
				break;

			// ---------------------------------   PASSWORD
			case 'password'	: 
				$str .= '<input type ="password" name= "'.$field['name'].'" ';
				$str .= (!empty($field['size'])) ? ' size="'.$field['size'].'" ' : '';
				$str .= (!empty($field['maxlen'])) ? ' maxlength = "'.$field['maxlen'].'" ' : '';
				$str .= (!empty($field['class'])) ? ' class = "'.$field['class'].'" ' : '';
				$str .= ( $showval ) ? ' value = "'.$value.'"' : '';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= " >\n";
				break;

			// ---------------------------------   HIDDEN
			case 'hidden'	: 
				$str .= '<input type ="hidden" name="'.$field['name'].'" value = "'.$value.'" >'."\n";
				break; 

			// ---------------------------------   TEXTAREA
			case 'textarea'	:
				$str .='<textarea name="'.$field['name'].'" rows="'.$field['rows'].'" cols="'.$field['cols'].'"'; 
				$str .= (!empty($field['class'])) ? ' class = "'.$field['class'].'" ' : '';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= '>';
				if ( $showval ) 
					$str .= $value;
				$str .= "</textarea>\n";
				if (isset($field['wysiwyg'])){// && !empty($field['wysiwyg_path']) && is_file($field['wysiwyg_path'])) {
					if ($field['wysiwyg'] == 'htmlarea') {
						if (empty($field['wysiwyg_path']) || !is_file($_SERVER['DOCUMENT_ROOT'].'/'.$field['wysiwyg_path'].'editor.js')) {
							$this->Error	= "Wrong HTMLAREA Path! ".$_SERVER['DOCUMENT_ROOT'].'/'.$field['wysiwyg_path'];
							$str	= $this->Error;
							break;
						}
						$str	.= ' 
									<script language="Javascript1.2">
									<!-- // load htmlarea
									_editor_url = "'.$field['wysiwyg_path'].'";                 // URL to htmlarea files and images
									var win_ie_ver = parseFloat(navigator.appVersion.split("MSIE")[1]);
									if (navigator.userAgent.indexOf("Mac")        >= 0) { win_ie_ver = 0; }
									if (navigator.userAgent.indexOf("Windows CE") >= 0) { win_ie_ver = 0; }
									if (navigator.userAgent.indexOf("Opera")      >= 0) { win_ie_ver = 0; }
									if (win_ie_ver >= 5.5) {
									 document.write(\'<scr\' + \'ipt src="\'+_editor_url+\'editor.js"\');
									 document.write(\' language="Javascript1.2"></scr\' + \'ipt>\');
									} else { document.write("<scr"+"ipt>function editor_generate() { return false; }</scr"+"ipt>"); }
									//-->
									</script>
									<script language="javascript1.2">
									   var config = new Object(); // create new config object
									   config.width = "100%";
									   config.height = "300px";
									   config.bodyStyle = "'.$field['wysiwyg_body'].';";
									   config.debug = 0;
									   config.toolbar = [
//									   ["fontname"],
//									   ["fontsize"],
									   ["bold","italic","underline","separator"],
//									   ["strikethrough","subscript","superscript","separator"],
									   ["justifyleft","justifycenter","justifyright","separator"],
									   ["OrderedList","UnOrderedList","Outdent","Indent","fontstyle"],
//									   ["forecolor","backcolor","separator"],
									   ["HorizontalRule","Createlink", "InsertTable","separator","htmlmode", "popupeditor"]];
									   editor_generate(\''.$field['name'].'\',config);
									   //editor_setmode("'.$field['name'].'");
										</script>
									';
					}
				}

				break;

			// ---------------------------------   SELECT 
			case 'select'	:
				$str .= '<select name="'.$field['name'].'"';
				$str .= (!empty($field['class'])) ? ' class = "'.$field['class'].'" ' : '';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= ">\n";
				for ( $i = 0;$i < count($field['arr']); $i++ )
					$str .= ( $value == $field['arr'][$i] && $showval )	? "<option selected value=\"$i\" >".$field['arr'][$i]."\n" : 
																	"<option value=\"$i\" >".$field['arr'][$i]."\n"; 
				$str .= "</select>\n";
				break;

			// ---------------------------------   CHECKBOX 
			case 'checkbox'	:
				$str .= '<input type="checkbox" name="'.$field['name'].'" VALUE = "1"   ';
				$str .= (!empty($field['class'])) ? ' class = "'.$field['class'].'" ' : '';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= ( $showval && !empty( $value ) ) ? " checked >\n" : ">\n";
				break;

			// ---------------------------------   FILE
			case 'file' : 
				$str .= '<input type=file name="'.$field['name'].'"';
				$str .= (!empty($field['class'])) ? ' class = "'.$field['class'].'" ' : '';
				$str .= (!empty($field['readonly'])) ? " readonly " : '';
				$str .= ">\n";
				break;

			// ---------------------------------   PHOTO
			case 'photo':
				if (!empty($value) && $showval) 
				{
					# ���� ���� ������... (mp3,photo)
					switch ($field['sub_type']) {
						case 'mp3':
							$str .= "<a href='".$field['path']."/".$value."' title='MP3:".$field['title']."'>".$field['path']."/".$value."</A><BR>";
							break;
						case 'photo':
							$str .= "<img src=\"".$field['path']."/".$value."\" border=0 alt=\"".$field['title']."\"><br>";
							break;
						default: 
							$str .= "<img src=\"".$field['path']."/".$value."\" border=0 alt=\"".$field['title']."\"><br>";
					}
					$str .= '<input type ="hidden" name="old_'.$field['name'].'" value = "'.$value.'" >'."\n";
					$str .= '�������: <input type="checkbox" name="kill_'.$field['name'].'" value = "1"   ';
					$str .= (!empty($field['class'])) ? ' class = "'.$field['class'].'" ' : '';
					$str .= "><BR>��������:\n";
				}
				$str .= '<input type=file name="'.$field['name'].'"';
				$str .= (!empty($field['class'])) ? ' class = "'.$field['class'].'" ' : '';
				$str .= ">\n";
				break;
		} // end switch
		return $str;
	}

  	 ///////////////////////////////////////////////////////////   Conv_str ()
	//------------------------------------------------------------------------
	//  Converting String: 
	//	1. Strip Tags	
	//	2. Strip Slashes
	//	3. Replace ' -> `
	// PRIVATE
	function Conv_str ( $string )
	{
		$string = str_replace(chr(0), '', $string);
		return ($this->NoTag)?
		str_replace("'","`",stripslashes(strip_tags($string,$this->EnableTags))):
		str_replace("'","`",stripslashes($string));
	}

	 ///////////////////////////////////////////////// global_val ()
	// geting the value of variable
	// PRIVATE
	function global_val( $name )
	{
		preg_match("/(.*)\[(.*)\]/",$name, $matches);
		if (!empty($matches[2])) 
		{
			global $$matches[1];
			return @str_replace(chr(0), '', ${$matches[1]}[$matches[2]]);
		}
		else
		{
			global $$name;
			return str_replace(chr(0), '', $$name);
		}
	}

	 ///////////////////////////////////////////////// chg_val ()
	// geting the value of variable
	// 01.10.02 PRIVATE
	function chg_val( $name, $value )
	{
		preg_match("/(.*)\[(.*)\]/",$name, $matches);
		if (!empty($matches[2])) 
		{
			global $$matches[1];
			@${$matches[1]}[$matches[2]] = $value;
		}
		else
		{
			global $$name;
			$$name = $value;
		}
	}
	 ///////////////////////////////////////////////// global_val ()
	// geting the key of array: 
	// => some[some_key] 
	// <= some_key
	//
	// PRIVATE
	//
	function get_key( $string )
	{
		preg_match("/(.*)\[(.*)\]/",$string, $matches);
		return (!empty($matches[2])) ? $matches[2] : $string;
	}


	 ///////////////////////////////////////////////////////////////////// chk_foto()
	// chk_foto
	// ^^^^^^^^
	//   ������� ������ ������ ���� �����, �� ����:
	//	������ field, � ������ 'name', 'path' ���� ����� 'func newname','maxsize'
	// ----------------------
	//  version 1.2 - OUT: 
	// ----------------------
	//
	// 3/22/02
	// PRIVATE
	function chk_photo( &$field )
	{
		global ${'kill_'.$field['name']}, ${'old_'.$field['name']}, ${$field['name']}, ${$field['name']."_type"}, ${$field['name']."_name"};
		$kill_foto	= ${'kill_'.$field['name']};
		$oldfoto	= ${'old_'.$field['name']};
		$foto		= ${$field['name']};
		$foto_type  = ${$field['name']."_type"};
		$foto_name	= ${$field['name']."_name"};

		// If we want Kill Photo
		if (!empty($kill_foto) && !empty($oldfoto)) 
		{
			$destination = (!isset($field['abspath'])) ? $field['path'] . "/" . $oldfoto :  $field['abspath']. "/" . $oldfoto; 
			unlink( $destination );
		}

		$this->skipfoto = (!empty($kill_foto) || !empty($foto_name)) ? 0 : 1;

		if ( !empty($foto_name)) 
		{
			if (  $foto_type == "image/jpeg" || $foto_type == "image/pjpeg"  || $foto_type == "image/gif"  || $foto_type == "audio/mpeg")
			{
				// If We must check Size of a Picture
				if (!empty($field['maxsize']))
				{
					$size = FileSize( $foto );
					if ($size > $field['maxsize'] ) 
					{
						$this->Error .= "- ���� ����� ������� ������� ������: ". $size ."Bt , � �� ����� ��������, ��������: ".$field['maxsize']."Bt.<BR>\n"; // string
						return false;
					} 
				}
				else
				{
					// if update, so must kill old one photo.
					if ( ! empty( $oldfoto ) ) {
						$destination = (!isset($field['abspath'])) ? $field['path'] . "/" . $oldfoto :  $field['abspath']. "/" . $oldfoto; 
						unlink( $destination );
					}
					return true;
				}// IF
			} 
			else 
			{
				$this->Error .= "- ������������ ������ ����������. $foto_type.<BR>\n"; // string
				return false;
			}//if
		}//if (!$foto)
		${$field['name']} = '';
		unset(${$field['name']});
		return true;
	} // func

	 ///////////////////////////////////////////////// copy_file()
	// �������� ����, ������ ����� ��� �����. 
	// �����:  ${$field['name']}, ${$field['name']."_name"};
	// field[ 'name', 'newname_func' 'path']
	//
	// 24.03.2002
	// PRIVATE
	function copy_file( & $field )
	{
		/// GLOBALIZE
		global ${$field['name']}, ${$field['name']."_name"};
		$foto		= ${$field['name']};
		$foto_name	= ${$field['name']."_name"};

		// GET NEW NAME
		$type = strrchr( $foto_name,'.');
		if (empty($field['newname_func'])) 
			$foto_new_name = $field['name'] . $type;
		else 
			eval("\$foto_new_name = ".$field['newname_func']."; \$foto_new_name .= \$type;");

		// MUST COPY FILE NOW. ALL CHEKS COMPLETE
		$destination = (!isset($field['abspath'])) ? $field['path'] . "/" . $foto_new_name :  $field['abspath']. "/" . $foto_new_name; 
		if (@copy($foto,"$destination")) 
		{
			${$field['name']} = $foto_new_name;
			return true;
		}
		else // ELSE WARNINGS
		{
			$this->Bug_mes = "<p><B>BUG MESSAGE => Trying : copy( \"$foto\", \"$destination\") - false</B></p>";
			$this->Error = "<p class=warning>���������� ����������� ����.</p>\n"; //string		
			${$field['name']} = '';
			unset( ${$field['name']} );
			return false;
		}//else
	} // func


}//class
?>