<?
/** send_mail by PHP "mail"
*
*/
function send_mail($to_email,$to_name='',$from_email,$from_name='',$subj,$message,$type = 'text')
{
    $message = convert_cyr_string ($message, "w", "k");
    $subject = convert_cyr_string ($subj, "w", "k");
    //------- проверка заголовка --------
    if ($type=="html"){
     $headers="MIME-Version: 1.0\r\nContent-Type: text/html; charset=koi8-r;\r\nContent-transfer-Encoding: 8bit\r\nX-Mailer: PHP/";
    }else{
      $headers="MIME-Version: 1.0\r\nContent-Type: text/plain; charset=koi8-r\r\nContent-transfer-Encoding: 8bit\r\nX-Mailer: PHP/";
    }
	$from	= "From: $from_name <$from_email>\r\n".$headers;
//	echo $to_email, $subject, $message, $from;
    return (mail($to_email, $subject, $message, $from));
}

?>