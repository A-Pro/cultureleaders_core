<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : class.lPagesLine.php 
// PROJECT NAME : CLASSES/Land
// DATE         : 26.09.2002
// Author       : Andrew Lyadkov (_land@mail.ru)
// Discription  : ����� ��� ������ ������ � �������� �� ������ ��������
// ...........  : 
// Comment      : ver 1.0
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
/*
FUNCTIONS
---------
function PagesLine( $ScriptUrl, $ElemOnPage='', $PagesInLine='', $QueryString='', $VarName='', $TypeOut='', $Delimeter='' )
function PrintLine( $ElemAll='', $return = false)
function pages_line ( $All,$count,$page,$action,$query='') // ALONE


usega 1.:
---------

< ?php //test1.php
	// ������ �� test.php, 29 ��������� �� ��������, 10 ������� � ����� (������ >>), 
	// ��� ���������� 'page', ��� ������: ������ ���������, ����������� ������
	//------------
	// �������
	$l = new PagesLine('test.php',29,10,'','page','num','');
	// ����� ����� 30 ���������
	$All = 30;
	//���������� � ����� ������� ������
	$l->PrintLine($All);
	// ������� ������ ����������� � ������������ �� ����������
	for ($i=$l->Offset+1;$i<=$l->LastNum;$i++) {
		echo $i."<br>";
	}
	// �������� ��������� �����
	echo $l->LastLine."-<BR>";
? >
usega 2.:
---------
< ?php //test1.php
	// ������ �� test.php, 20 ��������� �� ��������, 10 ������� � ����� (������ >>), 
	// ��� ���������� 'p', ��� ������: ������ �������, ����������� '|'
	//------------
	// �������
	$l = new PagesLine('test.php');
	// ����� ����� 30 ���������
	$All = 30;
	//���������� � ������� � ����������
	$line = $l->PrintLine($All, 'back');
	// ���� ���� �� �������
	if ($line!='') echo "��������: ".$line."<br>";
	// ������� ������ ����������� � ������������ �� ����������
	for ($i=$l->Offset+1;$i<=$l->LastNum;$i++) {
		echo $i."<br>";
	}
	// �������� ��������� �����
	echo $l->LastLine."-<BR>";
? >


*/

class lPagesLine {
	
	// ��� �� ���������
	var $ElemAll	=	0;
	// ������� ��������
	var $CurPage	=	1;
	// ��������� �� ��������
	var $ElemOnPage	=	20;
	// ������� � �������
	var $PagesInLine=	10;
	// ������� ����� �������
	var $PagesCount	=	1;

	// ���������� ��� ����������
	var $Offset	=	0; 
	var $LastNum =	0; 

	// ��� ������ (������ �������|��������� ���������)
	var $TypeOut	= 'pages'; // [intervals]
	// ����� ��������� ������ [������>> | >>] [<<�����|<<]
	var $Forward	= '>>';
	// ����� ��������� ������ [<<�����|<<]	  
	var $Backward	= '<<';
	// ����������� 
	var $Delimeter	= '|';
	// ��� ���������� ������ �������� � ������� 
	var $VarName	= 'p';
	
	//  ��� ����� �������, ������������ ����� (��� ?...)
	var $ScriptUrl	=	'';
	// ���� ����� ��������� ������ ��� ����� �������, �� ���� ��� ��� ����� � (?...) (�.�. ����� �����)
	var $Query =	'';

	// ������� ������.
	var $err_msg	=	'';
	// ��������� ����� (DUMP)
	var $LastLine	=	'';
	// ��������� ���������� ��������
	var $Result	=	1;
	// ������
	var $ver	=	'1.0';
	// ������
	var $Author =	'Andrew Lyadkov emailto: _land@mail.ru';
	var $a_class = '';
	var $p_class = '';

	 ////////////////////////////////////////////////////// CONSTRUCTOR
	function lPagesLine( $ScriptUrl, $ElemOnPage='', $PagesInLine='', $QueryString='', $VarName='', $TypeOut='', $Delimeter='' ){
		$this->ScriptUrl = $ScriptUrl;	
		if ($ElemOnPage !='') $this->ElemOnPage = $ElemOnPage;
		if ($PagesInLine !='') $this->PagesInLine = $PagesInLine;
		if ($QueryString !='') $this->QueryString = $QueryString;
		if ($VarName !='') $this->VarName = $VarName;
		if ($TypeOut !='') $this->TypeOut = $TypeOut;
		if ($TypeOut !='' || $Delimeter !='') $this->Delimeter = $Delimeter;
		$this->LastLine	=	'';
	}

	 //////////////////////////////////////////////////////
	function err_msg(){	echo $this->err_msg;}

	 //////////////////////////////////////////////////////
	//
	function PrintLine( $ElemAll='', $return = false){
		if ($ElemAll !='') $this->ElemAll = $ElemAll;
		if ($ElemAll==0) {
			$this->PagesCount	= 0;
			$this->LastLine		= '';
			$this->Result		= 0;
			$this->err_msg		.= "<B>class.PrintLine</B> PrintLine: ElemAll == 0.<br>\n";
			return '';
		}
		$QUERY_STRING = $this->QueryString;
		global ${$this->VarName};
		// �������� ������ ��������
		if (isset(${$this->VarName})) $this->CurPage = ${$this->VarName};
		// �������� � ������
		$this->Offset = ($this->CurPage-1)*$this->ElemOnPage;
		$this->LastNum = min($this->ElemAll, $this->Offset + $this->ElemOnPage);
		// ����� �������
		$this->PagesCount = ceil($this->ElemAll / $this->ElemOnPage); 
		// ������ ��������
		$string = str_replace("&".$this->VarName."=".$this->CurPage,"",$QUERY_STRING);
		$tmp = '';
		// ������ �������
		if ( $this->PagesCount > 1 ){
			// ���� ������� �������� ������ ����������� �� ��������
			if ($this->CurPage > $this->PagesInLine) {
				$from = $this->PagesInLine*floor(($this->CurPage - 1) / $this->PagesInLine); 
				$tmp .= "<A HREF='".$this->ScriptUrl.'?'.$string.'&'.$this->VarName.'='.$from."' class=".$this->a_class.">".$this->Backward."</A> ".$this->Delimeter." ";
			}
			else
				$from = 0;
			$to = min($from+$this->PagesInLine, $this->PagesCount); 
			// ��������...
			for ( $i = $from+1 ; $i <= $to ; $i++) {
				// ��� ������, ������ �������  | ������ ���������
				if ($this->TypeOut=='pages')
					$TypedStr	= $i;
				else {
					$numFrom	= ($i-1)*$this->ElemOnPage+1;
					$numTo		= min($this->ElemAll, $i*$this->ElemOnPage);
					$TypedStr	= ($numFrom==$numTo)?$numFrom:"[".$numFrom."-".$numTo."]";
				}
				if ($i == $this->CurPage) $tmp .= "<b>".$TypedStr."</b>";
				else $tmp .="<A href='".$this->ScriptUrl."?".$string."&".$this->VarName."=".$i."' class=".$this->a_class.">".$TypedStr."</A>";
				if ($i != $to) $tmp .= " ".$this->Delimeter." ";
			} 
			// ���� ���� ������
			if ($to < $this->PagesCount){
				$tmp .= " ".$this->Delimeter." <A href='".$this->ScriptUrl."?".$string."&".$this->VarName."=".($to+1)."' class=".$this->a_class.">".$this->Forward."</A>";
			}
		}
		$this->LastLine = $tmp;
		$this->Result	=	1;
		if ($return)
			return ($this->PagesCount > 0) ? $tmp : ''; // ���� �� ������� �� �� ����������
		else
			echo ($this->PagesCount > 0) ? $tmp : ''; // ���� �� ������� �� �� ����������
	}



///////////////////////////////////////////////////////////   pages_line (Alone)    //////
// Author	: Land 2/27/02
// *** ����� ������ �� ������� ������ �� ��������, ������ ������ �������, ��� ��������
// ������������ �� ���� ������ NNOV, ��� ������ �����������
// $All		- ������� ����� �������
// $count	- ������� ��������� ��������� �� �������
// $page	- ������� �������� (�� ��������� ������)
// $action	- ��� ����� �������, ������������ ����� (��� ?...)
// $query	- ���� ����� ��������� ������ ��� ����� �������, �� ���� ��� ��� ����� � (?...)

function pages_line ( $All,$count,$page,$action,$query='') // ALONE
{
	global $QUERY_STRING;
	if ($count == 0)
		return 0;
	$Allpages = ceil($All / $count); // ����� �������
	$pages_count = 10;				 // ������� ������� � ������ �������
	$string = str_replace("&p=$page","",$QUERY_STRING);
	$list_str = "<BR>������� - <B>$All</B>. "; 
	// ������ �������
	if ( $Allpages > 1 ){
		$list_str .= "��������: ";
		if ($page > $pages_count) {
			$from = $pages_count*floor(($page - 1) / $pages_count); 
			$list_str .= "<A href='$action?$string&p=$from'><<�����</A> | ";
		}
		else
			$from = 0;
		$to = min($from+$pages_count, $Allpages); 
		for ( $i = $from+1 ; $i <= $to ; $i++) {
			if ($i == $page) $list_str .= "<b>$i</b>";
			else $list_str .="<A href='$action?$string&p=$i'>$i</A>";
			if ($i != $to) $list_str .= " | ";
		} // end for
		if ($to < $Allpages){
			$int_to = $to+1;
			$list_str .= " | <A href='$action?$string&p=$int_to'>�����>></A>";
		}
		$list_str .= "<BR><BR>";
	}
		echo ($Allpages > 0) ? $list_str : ''; // ���� �� ������� �� �� ����������
} // function 


	
} 

?>