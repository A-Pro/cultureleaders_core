<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : adm_close.php
// PROJECT NAME : NyFLIRT / Close Banns
// DATE         : 03.09.2003
// Author       : Andrew Lyadkov (andrey@lyadkov.nnov.ru)
// Discription  : �������������� �������� IP, email, pwd
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
/*
�������� �����������
�� �������: ������ �� N ��������� ����������� [edit][del]
*/
if (!$_CORE->IS_ADMIN || empty($_CORE->NeedUsers)) return;

$id = $key = $field = '';
$action = 'add';
define('IN_LAND',true);
define('THIS_FILE','/auth/admin/adm.php');
global $EDIT_ROOT;
$EDIT_ROOT	= $_CORE->CoreModDir.'/admin/';
$old	= error_reporting(E_ALL ^ E_NOTICE);

include $EDIT_ROOT . "edit_common.php";
include $EDIT_ROOT . "include/adm.inc.php";
global $form;
$form	= get_post_var('form');
$action	= get_post_var('action');
$p		= get_post_var('p');
if ($p == '') $p = 1;

head();
switch ($action) {
	case 'add':
		if (insert($form))
			$action = 'search';		
		break;
	case "edit":
		$id = post_var('author_id');
		if (update($id, $form))
		$action = 'search';
		break;
	case "load":
		$id = get_var('author_id');
		load($id, $form);
		break;
	case "del":
		$id = get_var('author_id');
		del( $id );
		$action = 'search';		
		break;
	case "search":
		$key = post_var('key');
		$field = post_var('f');
		break;
	case "move":
		$where	= get_var('where');
		$num	= get_var('num');
		$id		= get_var('author_id');
		move_ud($id,$num,$where);
		break;
	default:
		$action = 'add';
}
if (!in_array($action, array('edit','add','load'))) {
	# �����
	search_from($key,$field);
	# ������
	show_list($p,$key,$field);
}else {
	# ����� ��������������
	show_form($form, $action, $id);
}

if (isset($db)) $db->Destroy();

foot();
error_reporting($old);

?>