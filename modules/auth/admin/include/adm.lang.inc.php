<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : close.inc.php
// PROJECT NAME : MyFLIRT/close
// DATE         : 17.03.2003
// Author       : Andrew Lyadkov (land@inforis.ru)
// Discription  : ��������� ...
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
global $lang;
$lang['IdIsEmapty']	= "ID ������ ����!";
$lang['IDNotFound']	= "ID ������ �� ������� � ����!";
$lang['TableEmpty']	= "������� �����!";
$lang['GoodUpdate']	= "������ ������� ���������������.";
$lang['GoodInsert']	= "������ ������� ���������.";
$lang['GoodDelete']	= "������ ������� �������.";
$lang['UnknownField']		= "��� ������ ������� ����������� ����!";
$lang['BadSearch']			= "�������, ��������������� ������� �� �������!";
$lang['BadConnection']		= "���������� ������������ � ���� ������!";
$lang['UnKnownToMove']		= "��������� ���� �������!";
// $lang[]		= ;

?>