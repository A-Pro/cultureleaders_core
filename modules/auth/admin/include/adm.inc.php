<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : close.inc.php
// PROJECT NAME : MyFLIRT/close
// DATE         : 11.03.2003
// Author       : Andrew Lyadkov (andrey@lyadkov.nnov.ru)
// Discription  : ������� ������� �������� IP
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
/*
public:
------
load( $id, &$form )
del( $id='' )
update( $id, &$form )
insert( &$form )
show_list($page = 1, $key='', $field='')

print_list_result( &$rows, $pages, $now_page=1)
show_form( &$form, $action = 'add', $id='' )
search_from( $key='', $f='')

head()
foot()

table()
get_where( $key='', $field = '')
*/

include "common.inc.php";
if ( !defined('IN_LAND') ) die("Hacking attempt");

define ("TAB2WORK","auth_pers");
define ("ROWS_ON_PAGE","20");
define ("DEBUG","0");
global $EDIT_ROOT;
include $EDIT_ROOT . "/include/adm.lang.inc.php";



/**
*
*/
function print_list_result( &$rows, $pages, $now_page=1)
{
	global $_CORE;
	_print_js_real_del();
	?>
	<?table('50%')?>
	<tr><td colspan=6><?=$pages?></td></tr>
	<tr align="right"><th  height=24>N</th><th>&nbsp;������&nbsp;</th><th>&nbsp;������&nbsp;</th><th>&nbsp;����������&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th> <!--<th width=20>&nbsp;</th><th width=20>&nbsp;</th> --> </tr>
	<?foreach ($rows as $k=>$v) {
		echo "<tr align=right height=21><td>&nbsp;".(($now_page-1)*ROWS_ON_PAGE+$k+1)."&nbsp;</td><td>&nbsp;".clrstr($v['author_login'])."&nbsp;</td><td>&nbsp;".clrstr($v['author_passwd'])."&nbsp;</td><td>&nbsp;".clrstr($v['author_comment'])."&nbsp;</td><td align=center><A HREF=\"#\" OnClick=\"real_del('".THIS_FILE."?action=del&author_id=".$v['author_id']."');return false\"; TITLE='�������'><IMG SRC=\"".$_CORE->DIRMODS."/doctxt/images/a_delete.gif\" WIDTH=\"16\" HEIGHT=\"16\" BORDER=0 ALT=\">\"></A></td><td align=center><A HREF='".THIS_FILE."?action=load&author_id=".$v['author_id']."' TITLE='��������'><IMG SRC=\"".$_CORE->DIRMODS."/doctxt/images/a_edit.gif\" WIDTH=\"16\" HEIGHT=\"16\" BORDER=0 ALT=\">\"></A></td> <!-- <td align=center><A HREF='./".THIS_FILE."?action=move&id=".$v['id']."&num=".$v['num']."&where=down' >up</A></td><td align=center><A HREF='./".THIS_FILE."?action=move&id=".$v['id']."&num=".$v['num']."&where=up' >down</A> </td> -->";
	}?>
	</table>
	<?
}
global $COLUMNS_CONF;
$COLUMNS_CONF	= Array
(
  'author_login' => 
  array (
    'field_name' => 'author_login',
    'name' => 'form[author_login]',
    'title' => '�����',
    'type' => 'textbox',
    'must' => '1',
    'size' => '20',
    'maxlen' => '20',
  ),
  'author_passwd' => 
  array (
    'field_name' => 'author_passwd',
    'name' => 'form[author_passwd]',
    'title' => '������',
    'maxlen' => '25',
    'must' => '0',
    'type' => 'textbox',
    'size' => '20',
  ),
  'author_comment' => 
  array (
    'field_name' => 'author_comment',
    'name' => 'form[author_comment]',
    'title' => '�����������',
    'maxlen' => '128',
    'must' => '0',
    'type' => 'textbox',
    'size' => '20',
  ),
  'author_part' => 
  array (
    'field_name' => 'author_part',
    'name' => 'form[author_part]',
    'title' => '��������',
    'type' => 'textbox',
    'must' => '0',
    'size' => '20',
    'maxlen' => '20',
  ),
  'author_info1' => 
  array (
    'field_name' => 'author_info1',
    'name' => 'form[author_info1]',
    'title' => '��������',
    'type' => 'textarea',
    'must' => '0',
    'cols' => '50',
    'rows' => '7',
  ),
  'author_info2' => 
  array (
    'field_name' => 'author_info2',
    'name' => 'form[author_info2]',
    'title' => '���������',
    'type' => 'textarea',
    'must' => '0',
    'cols' => '50',
    'rows' => '7',
  ),

);
// used for quick take (search)
$n_FIELDS = array('id','author_login','author_passwd', 'author_comment');
/***
CREATE TABLE close (
id			serial PRIMARY KEY,
ip_close			varchar(16),
email_close			varchar(64),
pwd_close			varchar(64)
);
CREATE  INDEX "ip_close_close_key" ON "close" ("ip_close");
***/

/**
*
*/
function head()
{
	?>
	<H2>�������������� �������������</H2><BR>
	<?
}
/**
*
*/
function foot()
{
	?>
	<p align=left>
	<A HREF="<?php echo THIS_FILE?>">��������</A> | <A HREF="<?php echo THIS_FILE?>?action=search">������</A>
	<?
}


?>