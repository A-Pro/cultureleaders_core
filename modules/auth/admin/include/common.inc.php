<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : close.inc.php
// PROJECT NAME : MyFLIRT/close
// DATE         : 11.03.2003
// Author       : Andrew Lyadkov (andrey@lyadkov.nnov.ru)
// Discription  : ������� ������� �������� IP
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
/*
public:
------
load( $id, &$form )
del( $id='' )
update( $id, &$form )
insert( &$form )
show_list($page = 1, $key='', $field='')

print_list_result( &$rows, $pages, $now_page=1)
show_form( &$form, $action = 'add', $id='' )
search_from( $key='', $f='')

head()
foot()

table()
get_where( $key='', $field = '')
*/

if ( !defined('IN_LAND') ) die("Hacking attempt");

//define ("TAB2WORK","closed");
//define ("ROWS_ON_PAGE","20");
//define ("DEBUG","1");
//
//include $EDIT_ROOT . "include/close.lang.inc.php";


/**
*
*/
function move_ud($id='',$num='',$where='up')
{
	global $lang;
	if ($id=='' || $num=='') {
		error($lang['IdIsEmapty']);
		return false;
	}
	if (!load($id, $old_form)) {
		return false;
	}
	global $db;
	$numb=intval($num);
	$sign = ($where!='up')?">":"<";
	$sort = ($where!='up')?"ASC":"DESC";
	$result = select_sql('author_id,num', TAB2WORK,'num'.$sign.$numb , "ORDER BY num $sort LIMIT 1", DEBUG);
	if ($result->NumRows>0)
	{
		$result->FetchRow(0);
		$num = $result->FetchRow->num;
		$ret_id = $result->FetchRow->author_id;
		$result = update_sql (TAB2WORK, "num='$num'","author_id='$id'",DEBUG);
		$result = update_sql (TAB2WORK, "num='$numb'","author_id='$ret_id'",DEBUG);
		$result->Destroy();
	}
}

/**
*
*/
function del( $id='' )
{
	global $lang;
	if (empty($id)) {
		error($lang['IdIsEmapty']);
		return false;
	}
	if (!load($id, $old_form)) {
		return false;
	}
	global $db;
	$res = delete_sql(TAB2WORK, "author_id = '$id'",DEBUG);
	//$res->Destroy();
	good($lang['GoodDelete']);
	return true;
}

/**
*
*/
function load( $id='', &$form, $where='' )
{
	global $lang;
	if (empty($id)&&$where=='') {
		error($lang['IdIsEmapty']);
		return false;
	}
	global $db;
	$where	= ($where=='')?"WHERE author_id = '$id'":$where;
	$res	= select_sql('*',TAB2WORK,'',$where,DEBUG);
	if (!$res->Result) die($res->ErrorQuery);
	if ($res->NumRows == 0) {
		error($lang['IDNotFound']);
		return false;
	}
	$res->FetchArray(0);
	$form = $res->FetchArray;
//	global $photo;
//	$photo	= $form['photo'];
	$res->Destroy();
	return true;
}

/**
*  use $n_FILEDS;
*/
function get_where( $key='', $field = '')
{
	if ($key == '' || $field == '') return '';
	global $n_FIELDS;
	if (!in_array($field,$n_FIELDS)) {
		error($lang['UnknownField']);
		return '';
	}else{
		return "$field LIKE '%$key%' OR $field = '$key'";
	}
}

/**
*
*/
function update( $id, &$form )
{
	global $COLUMNS_CONF, $lang;
	global $cform;
//	global $photo;
	$old_form	= array();
	if (!$cform)
		$cform	= new cForm( $COLUMNS_CONF );
	// ��������, ��� � load ��� �����������
//	$tmp_photo = $photo;
	if (!load($id, $old_form)) {
		return false;
	}
//	$photo = $tmp_photo;
	if ($cform->Chk_come()){
		error($cform->Error);
		return false;
	}else{
		global $db;
		$sql_set = $cform->GetSQL('update');
		if (!$sql_set) {
			error($cform->Error);
			if (DEBUG) 	echo $cform->Bug_mes;
			return false;
		}
		$res = update_sql(TAB2WORK, $sql_set, "author_id = '$id'",DEBUG);
		if (!$res->Result)
			return false;			
		//$res->Destroy();
		good($lang['GoodUpdate']);
		$form = array();
		return true;
	}
}

/**
*
*/
function load_list($page = 1, $key='', $field='', &$rows, &$line, $order='num', $how='DESC')
{
	global $lang;
	global $db, $All, $QUERY_STRING;
	$where = '';

	if (!$db){ 
		error($lang['BadConnection']);
		return false;
	}
	// � ��� �� ������� ������ ? 
	if ($key != '' && $field != '') {
		$where = get_where($key, $field);
	}
	// ����������
	$res	= select_sql('count(*) as count',TAB2WORK, $where, '',DEBUG);
	if (!$res->Result) die("SQL Error:".$res->ErrorQuery);
	$res->FetchArray(0);
	$All	= $res->FetchArray['count'];
	$res->Destroy();
	if ($All == 0) {
		if ($where == '') {
			good($lang['TableEmpty']);
		}else{
			good($lang['BadSearch']);
		}
		return false;
	}
	// pages line
	if (empty($QUERY_STRING)) $Query_STRING='action=search';
	$l		= new lPagesLine(THIS_FILE,ROWS_ON_PAGE,10,$QUERY_STRING,'p','num','');
	$line	= $l->PrintLine($All, 'back');
	// loading...
	if (empty($order)) $order = 'num';
	if (empty($how)) $order = 'DESC';
	$offset		= get_offset_sql($l->Offset,ROWS_ON_PAGE, SQL);
	$res	= select_sql('*', TAB2WORK, $where, " ORDER BY $order $how ".$offset, DEBUG);
	if (!$res->Result) die($res->ErrorQuery);
	for ($i = 0; $i < $res->NumRows; $i++ ) {
		$res->FetchArray($i);
		$rows[]	= $res->FetchArray;
	}
	$res->Destroy();
	return (sizeof($rows)>0);
}

function show_list($page = 1, $key='', $field='', $order='num', $how='DESC')
{
	$data	= array();
	$line	= '';
	if (load_list($page,$key,$field,$data, $line,$order,$how)){
		print_list_result($data,$line,$page);
		return true;
	}else{
		good("");
		return false;
	}
}

/**
*	�������
*/
function insert( &$form )
{
	global $COLUMNS_CONF, $lang;
	global $cform,$form;
//	$tmp_path = $cform['photo']['path'];
//	$cform['photo']['path'] = '/web/virtual/nnov/bards/n_img';
	if (!$cform)
		$cform	= new cForm( $COLUMNS_CONF );
	if ($cform->Chk_come()){
//		$cform['photo']['path'] = $tmp_path;
		error($cform->Error);
		return false;
	}else{
		global $db;
		list($sql_vars, $sql_vals) = $cform->GetSQL('insert');
//		echo "<HR><b>------- DEBUG -----------</b> <PRE>";
//		echo $sql_vars.",".$sql_vals;
		if (!$sql_vars) {
			error($cform->Error);
			if (DEBUG) 	echo $cform->Bug_mes;
			return false;
		}
		$res_max = select_sql("max(num) as max",TAB2WORK,'','',DEBUG);
		if (!$res_max->Result) {
			error($lang['BadConnection']);
			return false;
		}
		$res_max->FetchArray(0);
		$res = insert_sql(TAB2WORK, $sql_vars.", num", $sql_vals.", '".($res_max->FetchArray['max']+1)."'", DEBUG);
		if (!$res->Result) {
			error('');
//			error($lang['BadConnection']);
			return false;
		}
		//$res->Destroy();
		good($lang['GoodInsert']);
		$form = array();
	}
//	$cform['photo']['path'] = $tmp_path;
	return true;
}


/**
*  ����� �����
*/
function show_form( &$form, $action = 'add', $id='' )
{
	global $COLUMNS_CONF;
	global $cform;
	if (!$cform)
		$cform	= new cForm( $COLUMNS_CONF );
	?>
	<fieldset><legend><?=($action=='add')?'����������':'��������������'?><legend>
	<FORM METHOD=POST enctype='multipart/form-data'><CENTER>
	<?php 
	table();
	echo($action != 'add')?'<INPUT TYPE="hidden" name="author_id" value="'.$id.'">':'';
	?>
	<?$cform->Body_print();?>
	<TR><TD>&nbsp;</TD><Td align=left><INPUT TYPE="submit" name=action value=<?php echo($action=='add')?'add':'edit'?> class="buttons"></Td></TR>
	</FORM>
	</TABLE>
	</CENTER>
	</fieldset>
	<?
}

/**
*	����� ��� ������
*/
function search_from( $key='', $f='')
{	global $COLUMNS_CONF;
	?>
	<fieldset><legend>�����<legend><CENTER>
	<?	table();?>
	<FORM METHOD=POST >
	<TR><td colspan=2>
		<INPUT TYPE="text" NAME="key" value="<?php echo $key?>">
		<SELECT name="f">
			<OPTION value=author_id>ID
		<?foreach ($COLUMNS_CONF as $v) {
			echo ($f!=$v['field_name'])?'<OPTION VALUE="'.$v['field_name'].'">'.$v['title']:'<OPTION VALUE="'.$v['field_name'].'" selected>'.$v['title'];
		}?>
		</SELECT>
	<TR><TD>&nbsp;</TD><Td align=right><INPUT TYPE="submit" name=action value=search class=buttons></Td></TR>
	</FORM>
	</TABLE>
	</fieldset>
	<?
}

function getPhotoName(){
	return time();
}

?>