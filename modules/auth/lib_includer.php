<?php

 //////////////////////////////////// About  this file ////////////////
//
// FILE         : lib_includer.php 
// PROJECT NAME : PASSPORT
// DATE         : 29.10.2002
// Author       : Andrew Lyadkov
// Discription  : 
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
#
#	sessions work MUST BE OUT SIDE !!!
#

include_once $_CORE->CoreModDir."/".PATH_TO_LIB."/func.send_mail2.php";
include_once $_CORE->CoreModDir."/".PATH_TO_LIB."/lib.sql_query.php";

// FOR CORE
$_CORE->dll_load('class.cForm');
$_CORE->dll_load('class.DBCQ');
global $db,$SQL_DBCONF;

$db	= new Connection('', $SQL_DBCONF);


//////////////////////////////////////////////////////
//		������ ����� � ��������	�� ����������		//
//////////////////////////////////////////////////////
function _load_land_arr( &$DATA, $file='' ){
	global $ERROR_MSG;
	if (!file_exists($file)) {
		$ERROR_MSG .= "�� ������ ����. ($file)<BR>";
		return false;
	}
	include $file;
	if (!isset($DATA))
		$ERROR_MSG .= "������ �� ��������� (���� '$file').<BR>";
}

//////////////////////////////////////////////////////
//		������ ����� � ��������						//
//////////////////////////////////////////////////////
function _load_land_arr2( &$DATA, $file='' ){
	global $ERROR_MSG;
	if ($file=='') {
		$ERROR_MSG .= "�� ����� ����.<BR>";		
		return false;
	}
	$str = @implode("", file($file));
	$str = str_replace('\\','',$str);
	if ($str!=''){
		$DATA	=  unserialize($str);
	}else
		$ERROR_MSG .= "������ �� ��������� (���� '$file').<BR>";
}

 //////////////////////////////////////////////////////
//
// ������ ������ ������
function _get_error_str( $str =''){
	return "$str <br>";
}

///////////////////////////////////////////////////////////   is_email 
//----------------------------------------------------------------------
//  - simple checker string for email, checking structure: *@*.* + !' '
//function _is_email ( $email ) 
//{
//	if ( empty( $email ) || ! strchr( $email, '@' )) 
//		return 0;
//	list($first, $domain) = explode("@", $email);
//	$point = strpos($domain, ".");
//	if ( strchr($email, " ") || $point == 0 || $point == strlen( $domain ) || strlen($first) == 0 ) 
//		return 0;
//	else 
//		return 1;
//}

///////////////////////////////////////////////////////////   _is_valid_date 
//----------------------------------------------------------------------
//  - simple checker of date, strong and not so
function _is_valid_date( $d,$m,$y,$is_strong) 
{
	$d = intval($d);
	$m = intval($m);
	$y = intval($y);
	if ($is_strong==STRONG) {
		# ���� ������ ���� ������ � ������
		return checkdate($m,$d,$y);
	}else{
		$res = true;
		if (!empty($d)) {
			$res = ($d>0&&$d<=31);
		}
		if (!empty($m)) {
			$res = $res&&($m>0&&$m<=13);
		}		
		if (!empty($y)) {
			$res = $res&&($y>0&&$y<=3000);
		}
		return $res;
	}
}


 //////////////////////////////////////////////////////
//
function clear_data( $str=''){
	global $CHARSET, $SOURCE_CHARSET;
	if ($CHARSET != $SOURCE_CHARSET) 
		$str = convert_cyr_string ($str, $CHARSET, $SOURCE_CHARSET);
	return strip_tags(stripslashes(str_replace("'","`",$str)));	
}

 //////////////////////////////////////////////////////
//
function get_photo_name(){
	global $SESS_AUTH, $id;
	return (!empty($SESS_AUTH['ID']))?$SESS_AUTH['ID']:(empty($id))?time():$id;
}

/**
*
*/
function MAIL_LOGO2ADMIN( $mess='Sorry', $type='text' )
{
	GLOBAL $_CONF;
	return send_mail( 'support@vnn.ru', PRJ_NAME.' Admin', ADMIN_EMAIL, 'Admin', 'Admin: Registered on '.$_SERVER['HTTP_HOST'],$mess,$type);
}

function MAIL_LOGO2USER( $user_email, $user_name, $mess='Sorry', $type='text' )
{
	GLOBAL $_CONF;
	return send_mail( $user_email, $user_name, ADMIN_EMAIL, 'Admin', 'Registered on '.$_SERVER['HTTP_HOST'] ,$mess,$type);
}

/**
*
*/
function _get_auth( $table, $where)
{
	global $db, $ERROR_MSG;
	if (!$db->Connect) {
		$ERROR_MSG .= _get_error_str("����������� ����������� � ����. (_get_auth)");
		return array();
	}
	if (!defined('DEBUG')) {
		define('DEBUG',0);
	}
	$r = select_sql('*', $table, $where,'',DEBUG);
	if ($r->NumRows == 0) {
		$ERROR_MSG .= _get_error_str("����� �� ������. (_get_auth)");
		return array();
	}elseif($r->NumRows==1){
		$r->FetchArray(0);
		return $r->FetchArray;
	}else{
		$ERROR_MSG .= _get_error_str("������� ��������� �������. (_get_auth)");
		return array();
	}
}
/**
 * is ID for present author?
 *
 * @param int $id
 * @return mixed
 */
function is_auth( $id )
{
	global $ERROR_MSG;

	$auth = SQL::sel('*', TAB_AUTH_PERS, "author_id = '$id'", '', DEBUG);
	
	if($auth->NumRows==1){
		$auth->FetchArray(0);
		return $auth->FetchArray;
	}else{
		return false;	
	}
}

///////////////////////////////////////////////////////////   is_email 
//----------------------------------------------------------------------
//  - simple checker string for email, checking structure: *@*.* + !' '
function _is_email ( $email ) 
{
	if ( empty( $email ) || ! strchr( $email, '@' )) return 0;
	list($first, $domain) = explode("@", $email);
	$point = strpos($domain, ".");
	if ( strstr($email, " ") || $point == 0 || $point == strlen( $domain ) || strlen($first) == 0 ) return 0;
	else return 1;
}

///////////////////////////////////////////////////////////   is_phone 
//----------------------------------------------------------------------
//  - simple checker string for email, checking structure: *@*.* + !' '
function _is_phone ( $phone ) 
{
	if ( empty( $phone ) || strchr( $phone, '@' )) return false;
	$phone = mysql_real_escape_string(strip_tags($phone));
	$phone = preg_replace('/\D/', '', $phone);
	if(strlen($phone)==11 && in_array( substr($phone,0,1),array(7,8) ) )
		$phone = substr($phone,1);
	if(strlen($phone)!=10) 
		return false;
	else 
		return "+7(".substr($phone,0,3).')'.substr($phone,3,3).'-'.substr($phone,6,2).'-'.substr($phone,8,2);
}

/////////////////////////////////////////////////////////// rand_code - ��������� ����
function rand_code () 
{
	$am=mt_rand(5,7);
	$eng='qwertyuiopasdfghjklzxcvbnm';
	$num='1234567890';
	$full_text=$eng.$num;
	$e=strlen($full_text)-1;
	$p='';
	for($n=0;$n!=$am;$n++){
		$p.=substr($full_text,mt_rand(0,$e),1);
	} 
    return $p;
}


class AUTH {
	/** 
	* SHOW AUTHOR NAME 
	*
	*/
	function show_name( $id )
	{
		$val = SQL::getval( 'author_comment', TABLE_AUTH_PERS, " author_id = '$id'", '', DEBUG);
		return '<A HREF="http://'.$_SERVER['HTTP_HOST'].'/auth/'.$id.'" TITLE="About" class=user>' .(($val)?$val:$id) . "</A> <A HREF=\"/clear/db/messages/add?edit&form[to_auth]=$val\" class=green TITLE=\"Send message\" rel=facebox><IMG SRC=\"/images/mess.gif\" WIDTH=\"11\" HEIGHT=\"8\" BORDER=\"0\" ALT=\"M\"></A>";
	}

		/** 
	* SHOW AUTHOR AVATAR 
	*
	*/
	function get_avatar( $id )
	{
		$val = SQL::getval( 'doc', (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'auth_pers', " author_id = '$id'", '', DEBUG);
		return '/data/auth/'.$val;
	}
		/** 
	* SHOW AUTHOR full name 
	*
	*/
	function get_name( $id )
	{
		$val = SQL::getval( 'author_comment', (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'auth_pers', " author_id = '$id'", '', DEBUG);
		return $val;
	}


	/** 
	* CREATE SYSTEM MESSAGE
	*
	*/
	function add_message( $to_id, $from_id, $subject='report', $message, $date = 'none', $email = '')
	{
		global $_CORE;

		$email = SQL::getval('author_login', (( defined('DB_TABLE_PREFIX') ) ? DB_TABLE_PREFIX : '') . 'auth_pers', "author_id = '$to_id'", '', DEBUG);

		if ($date == 'none' || empty($date)) $date = date("Y-m-d H:i:s");
		
		$result = SQL::ins('messages', 'from_auth, to_auth, name, about, ts', "'".$from_id."','".$to_id."','".$subject."', '".$message."', '".$date."'", DEBUG);

		if (!$_CORE->dll_load ('class.lMail'))
			die ($_CORE->error_msg());

		$message	.= $_CORE->lang['messages_footer'];

		// �������� ������
		$data['body']		= $message;
		$data['name']		= $_CORE->lang['forget_name'];
		$data['subject']	= strip_tags($subject);
		$data['email']		= $_CORE->lang['forget_email'];
		$data['mailto']		= $email;
		$data['type']		= 'html';


		lMail::send($data);
		
	}
	
	function may_see ( $viewer_id )
	{
		$res = ITEM::sql_is_partners($viewer_id, $_SESSION['SESS_AUTH']['ID']);
		return (!empty($res));
	}
	
	function get_author( $id )
	{
		global $ERROR_MSG;
	
		$auth = SQL::sel( '*', TAB_AUTH_PERS, "author_id = '$id'", '', DEBUG);
		
		if($auth->NumRows==1){
			$auth->FetchArray(0);
			return $auth->FetchArray;
		}else{
			return false;	
		}
	}
    
    /*  
    *   ������� ������:
    *       ������ ������ $data (�������� ������ ������������)
    *           ��� ��������� ���������� ������ ������������ ������ $data
    *           ����������� ������ ��������� $data['author_login'] � ������� 
    *           �������� email ������������
    */
	function add_author( $arr )
	{
	   global $ERROR_MSG, $auth, $DATA, $_CONF, $db, $_AUTH;
	   if (!empty($arr['author_login'])){

        	_load_land_arr($DATA, $_CONF['LandDatPath'].'/auth_reg.dat');
        	
        	for(reset($DATA);list($k,$v)=each($DATA);){
               if (!empty($arr[$k])) $new_arr[$k] = $arr[$k]; 
        		# ������ 
        		if (strlen($arr[$k])>$v['maxlen']) {
        			$ERROR_MSG .= _get_error_str('� ���� "'.$v['title'].'" ������� ����� ��������.');
        		}
        	}
            $arr = $new_arr;
            unset($new_arr);
        	if ($_AUTH['login_email']) {
        		if (!empty($arr['author_login']) && !_is_email($arr['author_login'])){
        			$ERROR_MSG .= _get_error_str('������� ������ E-MAIL.');
        		}
        	}
        	# check login (sql search)
        
        	$login = $arr['author_login'];
        	if (getval_sql( "COUNT(*) as count", TAB_AUTH_PERS, " author_login = '$login' ", '', $DEBUG)>0) {
        		$ERROR_MSG .= _get_error_str('����� ����� ��� ���������������.');
        	}
        
        	# check data
        	for(reset($arr);list($n,$v)=each($arr);) {
        		if (strchr($v,"'")) {
        			$arr[$n] = stripslashes($v);
        			$ERROR_MSG .= _get_error_str('������������ ������ � ����: '.$DATA[$n]['title']);
        		} else {
        			$arr[$n] = clear_data($v); 
        		}
        	}
        	if (!empty($ERROR_MSG) ) return false;
            
            if (!isset($arr['author_passwd'])){

                $arr['author_passwd'] = rand_code();
            }
            $arr['author_passwd']	= ($_CONF['PWDCRYPTED']) ? crypt($arr['author_passwd'],SALT) : $arr['author_passwd'];
            $auth = $arr;
        	$form		= new cForm($DATA);
        	$now_time	= time();
        	list($vars,$vals) = $form->GetSQL('insert');
        	$max_id = getval_sql( "MAX(author_id) as max", TAB_AUTH_PERS);
        	$next_id_lost = intval($max_id) + 1;

            $ins = insert_sql( TAB_AUTH_PERS, "$vars, author_id, created, modify".$var_add, "$vals, $next_id_lost, $now_time, $now_time".$val_add );
            
        	$next_id = mysql_insert_id();
        	if (!$ins->Result){
        		$ERROR_MSG 	= $inc->ErrorQuery;
        		return false;
        	}
            $ERROR_MSG 	= "";
            return $next_id;
            
	   }
		$ERROR_MSG 	= _get_error_str("�� ��������� ������������ ���� author_login");
        return false;
	

	}

	/**
	*
	*/
	function create_table()
	{
		$tabs = SQL::tlist();
		if (!in_array(TAB_AUTH_PERS, $tabs)) {
		
			$query = "		
				CREATE TABLE IF NOT EXISTS `".TAB_AUTH_PERS."` (
			`author_id` int(11) NOT NULL default '1',
			`author_login` varchar(25) default NULL,
			`author_passwd` varchar(25) default NULL,
			`created` int(11) default NULL,
			`MODIFY` int(11) default NULL,
			`ts` date default NULL,
			`hidden` varchar(1) default NULL,
			`alias` varchar(128) default NULL,
			`author_comment` varchar(255) NOT NULL,
			PRIMARY KEY  (`author_id`)
			) ENGINE=MyISAM DEFAULT CHARSET=cp1251;";
			$res = SQL::query( $query );
			if (!$res->Result)
				echo $res->ErrorQuery."<br />";
			else
				echo "����������� ������� `".TAB_AUTH_PERS."`<br />";

			$query = "		
			INSERT INTO `".TAB_AUTH_PERS."` (`author_id`, `author_login`, `author_passwd`, `created`, `MODIFY`, `ts`, `hidden`, `alias`, `author_comment`) VALUES
			(1, 'admin', '123456', 1104416611, 1305528969, NULL, NULL, '1', '')";

			$res = SQL::query( $query );
			if (!$res->Result)
				echo $res->ErrorQuery."<br />";
			else
				echo "�������� ������������ `admin`<br />";
		}
	}
} 

?>