<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : logoff.php 
// PROJECT NAME : REGISTER 
// DATE         : 11.11.2002
// Author       : Andrew Lyadkov
// Discription  : 
// ...........  : Vyhod izi sistemy, t.e. udalenie sessii.
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////

include_once "lib_includer.php"; // + $db = new
$LOGOFF_MSG = '';
global $_COOKIE;

# ���������� ��� ������
if (isset($_SESSION['SESS_AUTH'])) {


setcookie('login', '', time()+3600*7*24,'/',".".$_SERVER['HTTP_HOST']);
$_COOKIE['login'] = '';

setcookie('passwd', '', time()+3600*7*24,'/',".".$_SERVER['HTTP_HOST']);
$_COOKIE['passwd'] = '';

	$name = $_SESSION['SESS_AUTH']['PERSON'];
	
	session_destroy();
	$_SESSION['SESS_AUTH'] = array();

	// FOR CORE
	global $_CORE;
	$_CORE->reset_admin();
	
	$LOGOFF_MSG .= _get_error_str("$name, ".Main::get_lang_str('you_enter', 'auth'));

} else {

	$LOGOFF_MSG .= _get_error_str(Main::get_lang_str('you_not_login', 'auth'));

}

header('Location: /');
exit;

include "menu.php";
echo "<P><BR>".$LOGOFF_MSG;
?>