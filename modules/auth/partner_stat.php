<?php

global $_CORE, $CONF, $_AUTH;

// include_once "lib_includer.php"; // + $db = new
if (!$_CORE->dll_load('class.DBCQ'))
	die ($_CORE->error_msg());

# a zaregistrirovan li?
if (isset($_SESSION['SESS_AUTH'])&&!is_array($_SESSION['SESS_AUTH'])||empty($_SESSION['SESS_AUTH']['LOGIN'])) {
	header("Location: /auth/login.php\n\n");
	exit;
}
# esli sej pol'zovatel' ne zaregistrirovan to ego na vhod
$val = SQL::getval( "author_passwd", TAB_AUTH_PERS, "author_id = '".$_SESSION['SESS_AUTH']['ID']."' " ); 
// проверим по хеш коду
if ( md5($val.$_SESSION['SESS_AUTH']['LOGIN']) != $_SESSION['SESS_AUTH']['HASH'] ) {
	session_destroy();
	header("Location: /auth/login.php?session_lost\n\n");
	exit;
}

// проверим является ли пользователь партнером 
if ( empty($_SESSION['SESS_AUTH']['ALL']['partner']) ) {
	header("Location: /auth/update\n\n");
	exit;
}


// если все хорошо получаем статистические данные партнера
// сегодня
$data = array();
// посещений
$visits = SQL::getall('ts, count(*) as cnt', DB_TABLE_PREFIX.'partners_referal', "ts <= '".date('Y-m-d')."' AND ts >= '".date('Y-m-d', strtotime(date("Y-m")."-01"))."' AND user_id = ".$_SESSION['SESS_AUTH']['ID'], "GROUP BY ts ORDER BY ts DESC");
// по умолчанию 
$data['today']['visits'] = 0;
$data['yesterday']['visits'] = 0;
$data['month']['visits'] = 0;
if(is_array($visits) && count($visits))
foreach($visits as $visit){
	if($visit['ts']== date('Y-m-d')) $data['today']['visits'] = $visit['cnt'];
	if($visit['ts']== date('Y-m-d', (time()-3600*24))) $data['yesterday']['visits'] = $visit['cnt'];
	$data['month']['visits'] += $visit['cnt'];
}
// заказов и заработок
$orders = SQL::getall('ts, count(*) as cnt, SUM(bonus) as bonus ', DB_TABLE_PREFIX.'partners_basket', "ts <= '".date('Y-m-d')."' AND ts >= '".date('Y-m-d', strtotime(date("Y-m")."-01"))."' AND user_id = ".$_SESSION['SESS_AUTH']['ID'], "GROUP BY ts ORDER BY ts DESC");
// по умолчанию 
$data['today']['orders'] = 0;
$data['yesterday']['orders'] = 0;
$data['month']['orders'] = 0;
$data['today']['bonus'] = 0;
$data['yesterday']['bonus'] = 0;
$data['month']['bonus'] = 0;
if(is_array($orders) && count($orders))
foreach($orders as $order){
	if($order['ts']== date('Y-m-d')){ 
		$data['today']['orders'] = $order['cnt'];
		$data['today']['bonus'] = $order['bonus'];
	}
	if($order['ts']== date('Y-m-d', (time()-3600*24))){ 
		$data['yesterday']['orders'] = $order['cnt'];
		$data['yesterday']['bonus'] = $order['bonus'];
	}
	$data['month']['orders'] += $order['cnt'];
	$data['month']['bonus'] += $order['bonus'];
}
// получим сколько было заработано за все время
$data['alltime']['visits'] = (int)SQL::getval('count(*)',DB_TABLE_PREFIX.'partners_referal', "user_id = ".$_SESSION['SESS_AUTH']['ID'], "");

$alltime_orders = SQL::getrow('SUM(bonus) as bonus, count(*) as cnt',DB_TABLE_PREFIX.'partners_basket', "user_id = ".$_SESSION['SESS_AUTH']['ID'], "");
if(is_array($alltime_orders) && count($alltime_orders)){
	$data['alltime']['orders'] = (int)$alltime_orders['cnt'];
	$data['alltime']['bonus'] = (int)$alltime_orders['bonus'];
}else{
	$data['alltime']['orders'] = 0;
	$data['alltime']['bonus'] = 0;
}
/***************************************************************************/

  if(Main::comm_inc("partners_stat.html", $f, 'auth'))
  	include "$f";
    


?>