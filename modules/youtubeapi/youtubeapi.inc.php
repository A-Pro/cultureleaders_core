<?php
global $_CORE, $_YOUTUBEAPI, $_CONF;

if (!$_CORE->dll_load('class.DBCQ'))
	die ($_CORE->error_msg());

// ��������� �� ������ API 3.0
require_once $_CONF['CorePath']. '/lib/google/autoload.php';

/**
 * �lass YoutubeApi 
 *
 */
 

class YoutubeApi {
	

	protected $_developerKey;
	protected $_client;
	protected $_youtube;
	public $error;
	
	/**
	 * ����������� ������
	 * 
	 * @param $params ������ ���������� 
	 */
	public function __construct( $params = array()){
		global $_YOUTUBEAPI;
		$this->_developerKey = $_YOUTUBEAPI['developerKey'];
		$this->_client = new Google_Client();
		$this->_client->setDeveloperKey($_YOUTUBEAPI['developerKey']);
		
		// ���������� ������, ������� ����� ��������������, ����� ������� ��� ������� API.
		$this->_youtube = new Google_Service_YouTube($this->_client);

	}
	
	
	function getChannels4user($username){
		
		$searchResponse = $this->_youtube->channels->listChannels('id,snippet', array(
			'forUsername' =>  $username,
			'maxResults' => 20,
		));
		$Result = $searchResponse->getItems();
		return $Result[0];
		
	}
	
	function getChannel($channel){
		
		$searchResponse = $this->_youtube->channels->listChannels('id,snippet', array(
			'id' =>  $channel,
			'maxResults' => 20,
		));
		$Result = $searchResponse->getItems();
		return $Result[0]->getSnippet();
		
	}
	
	
	function getPlaylists($channelId){
		
		$searchResponse = $this->_youtube->playlists->listPlaylists('id,snippet', array(
			'channelId' =>  $channelId,
			'maxResults' => 50,
		));
		$Result = $searchResponse->getItems();
		return $Result;
		
	}
	
	
	function getVideosPlaylists($playlistId){
		
		$searchResponse = $this->_youtube->playlistItems->listPlaylistItems('id,snippet', array(
			'playlistId' =>  $playlistId,
			'maxResults' => 50,
		));
		$Result = $searchResponse->getItems();
		return $Result;
	}
	
	function getVideos($channelId,$PageToken=''){
		$param = array(
			'q' => '',
			'channelId' =>  $channelId,
			'maxResults' => 50,
			'order' => 'date',
			'type' => 'video',
		);
		$videos = array();
		if(!empty($PageToken)) $param['pageToken'] = $PageToken;
		$searchResponse = $this->_youtube->search->listSearch('id,snippet', $param);
		$videos = $searchResponse->getItems();
		
		if($searchResponse->nextPageToken){
			$videos2 = $this->getVideos($channelId, $searchResponse->nextPageToken);
			$videos = array_merge($videos,$videos2);
		}
		return $videos;
	}
	
	
	
	function savePlaylist($playlist,$user_id)
	{
		$snippet = $playlist->getSnippet();
		$name = iconv('UTF-8','windows-1251',$snippet->title);
		$cont = iconv('UTF-8','windows-1251',$snippet->description);
		$url = "http://www.youtube.com/playlist?list=".iconv('UTF-8','windows-1251',$playlist->id);
		$updated = $snippet->publishedAt;
		$playlistId  = $playlist->id;
		if(!empty($name) ){
			// ������� ������� ��������
			$res = SQL::upd((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'youtubeapi_playlists', "cont = '".str_replace("'", "`", $cont)."', alias= '".time().'-'.rand(0,500)."', name = '".$name."', url = '".$url."',updated = '".$updated."', user_id = '".(int)$user_id."', playlistId = '".$playlistId."'","url = '".$url."'", DEBUG );
			
			if (empty($res) || !@$res->Result || ($res->Result && !@$res->Tuples) || $res->ErrorQuery ) {
				$res = SQL::ins_set((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'youtubeapi_playlists', "cont = '".str_replace("'", "`", $cont)."', alias= '".time().'-'.rand(0,500)."', name = '".$name."', url = '".$url."',updated = '".$updated."', user_id = '".(int)$user_id."', playlistId = '".$playlistId."'", DEBUG );
			}
			return SQL::getrow('*',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'youtubeapi_playlists',"url = '".$url."'");
		}
		return false;
	}
	
	
	
	
	function saveVideo($video,$user_id)
	{
		$snippet = $video->getSnippet();
		$name = iconv('UTF-8','windows-1251',$snippet->getTitle());
		$cont = iconv('UTF-8','windows-1251',$snippet->description);
		$cont = str_replace(array("'",'"'),array("&lsquo;", "&quot;"),$cont);
		$name = str_replace(array("'",'"'),array("&lsquo;", "&quot;"),$name);
		$videoId = (empty($video->id->videoId))?$snippet->resourceId->videoId:$video->id->videoId;
		$url = "http://www.youtube.com/watch?v=".$videoId;
		$pub = $snippet->publishedAt;
		$code = $videoId;
		$time = time();
		if(!empty($code) ){
			// ������� ������� ��������
			$res = SQL::upd((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'youtubeapi_video', "updated = '$time', published = '$pub', cont = '".$cont."', alias = '".$code."', code = '".$code."', name = '".$name."', user_id = '".(int)$user_id."'" ,"code = '".$code."'" );
			if (empty($res) || !@$res->Result || ($res->Result && !@$res->Tuples) || $res->ErrorQuery ) {
				$res2 = SQL::ins_set((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'youtubeapi_video', "updated = '$time', published = '$pub', cont = '".$cont."', alias = '".$code."', code = '".$code."', name = '".$name."', user_id = '".(int)$user_id."'"  );
			}
			if($res->ErrorQuery) echo $res->ErrorQuery;
			$row_video = SQL::getrow('*',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'youtubeapi_video',"code = '".$code."'");
			if(!empty($row_video['code']))
				return $row_video;
		}
		
		return false;
	}
	
	
	
	function saveVideo2playlist($code,$plist_id,$user_id){
		$res = SQL::getrow('*',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'youtubeapi_video_playlist',"code = '".$code."' and plist_id = '$plist_id'");
		if(is_array($res) && count($res) && !empty($res['id'])){
			return false;
		}
		$res = SQL::ins_set((defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '').'youtubeapi_video_playlist', "code = '$code', plist_id = '$plist_id', user_id = '".(int)$user_id."'"  );
		return true;
	}
	
	
	
	/**
	 * ���������� ��������� ������ � ��
	 * youtubeapi_channel2users
	 * youtubeapi_playlists
	 * youtubeapi_video
	 * youtubeapi_video_playlist
	 */
	function install_module()
	{	
		$query = "CREATE TABLE IF NOT EXISTS `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."youtubeapi_channel2users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `link` varchar(255) NOT NULL,
  `channel` varchar(255) NOT NULL,
  `cont` text CHARACTER SET utf8 NOT NULL,
  `ts` date NOT NULL,
  `hidden` int(1) NOT NULL ,
  `show_main` int(1) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ";
		$res = SQL::query( $query );
		$query = "CREATE TABLE IF NOT EXISTS `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."youtubeapi_playlists` (
					`id` tinyint(20) NOT NULL AUTO_INCREMENT,
					`name` varchar(255) CHARACTER SET utf8 NOT NULL,
					`url` varchar(255) CHARACTER SET utf8 NOT NULL,
					`updated` varchar(255) CHARACTER SET utf8 NOT NULL,
					`alias` varchar(255) CHARACTER SET utf8 NOT NULL,
					`cont` text CHARACTER SET utf8 NOT NULL,
					`user_id` tinyint(20) NOT NULL,
					PRIMARY KEY (`id`)
				) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1";
		$res = SQL::query( $query );
		$query = "CREATE TABLE IF NOT EXISTS `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."youtubeapi_video` (
					`id` tinyint(20) NOT NULL AUTO_INCREMENT,
					`code` varchar(255) CHARACTER SET utf8 NOT NULL,
					`alias` varchar(255) CHARACTER SET utf8 NOT NULL,
					`name` varchar(255) CHARACTER SET utf8 NOT NULL,
					`updated` varchar(255) CHARACTER SET utf8 NOT NULL,
					`published` varchar(255) CHARACTER SET utf8 NOT NULL,
					`cont` text CHARACTER SET utf8 NOT NULL,
					`user_id` tinyint(20) NOT NULL,
					PRIMARY KEY (`id`)
				) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ";
		$res = SQL::query( $query );
		$query = "CREATE TABLE IF NOT EXISTS `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."youtubeapi_video_playlist` (
					`id` tinyint(20) NOT NULL AUTO_INCREMENT,
					`code` VARCHAR( 255 ) NOT NULL,
					`plist_id` tinyint(20) NOT NULL,
					`user_id` tinyint(20) NOT NULL,
					PRIMARY KEY (`id`)
				) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1";
		$res = SQL::query( $query );
		// ������� ��������� ���� � ������� �������������
		$query = "ALTER TABLE `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."auth_pers` ADD `youtube_login` VARCHAR( 255 ) NOT NULL ,
ADD `youtube_pasw` VARCHAR( 255 ) NOT NULL";
		$res = SQL::query( $query );
		
		$res = SQL::query( "ALTER TABLE  `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."youtubeapi_playlists` ADD UNIQUE (`url`)" );
		$res = SQL::query( "ALTER TABLE  `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."youtubeapi_playlists` ADD  `playlistId` VARCHAR( 255 ) NOT NULL" );
		
		$res = SQL::query( "ALTER TABLE  `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."youtubeapi_video` ADD UNIQUE (`code`)" );
		$res = SQL::query( "ALTER TABLE  `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."youtubeapi_video_playlist` ADD UNIQUE (`code`,`plist_id`)" );
		$res = SQL::query($src = "CREATE OR REPLACE 
ALGORITHM = 
MERGE 
VIEW  `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."youtubeapi_view` AS 
SELECT v.id, v.code, v.name, pl.id plid, pl.name plname,pl.updated plupdated,  v.user_id, v.code alias, pl.url plurl, pl.cont plcont, v.cont vcont, v.updated vupdated, v.published vpublished 
FROM  `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."youtubeapi_video` v , `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."youtubeapi_video_playlist` vpl,  `".(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."youtubeapi_playlists` pl
WHERE vpl.code = v.code
AND vpl.plist_id = pl.id  ORDER BY plupdated desc" );
// echo $src;
	}
	
}

?>
