<?
$_MAP['DB_DEEP']['def'] = 1000; 
$_MAP['SEARCH_FORM']['def'] = array( 'hidden' => 0 );
#
#	DRAW MAP
#
class MAP {
	function draw($type = '')
	{
		global $_PROJECT, $_MAP, $_CORE;
		
		include Main::comm_inc($_MAP['MODULE_NAME'].'.head'.$type.'.html', $f, $_MAP['MODULE_NAME']);
		$_MAP['CURR_NUM'] = 0;

		foreach($_PROJECT['TREE'] as $k => $v )
			_map_tree_walk($v, $k, 0,$type);

		@include Main::comm_inc($_MAP['MODULE_NAME'].'.foot'.$type.'.html', $f, $_MAP['MODULE_NAME']);
	}

	function draw_item( &$item, $deep, $type = '' )
	{
		global $_CORE, $_MAP;
		if (empty($item['hidden']) && empty($item['link']) ) {
			@include Main::comm_inc($_MAP['MODULE_NAME'].'.item'.$type.'.html', $f, $_MAP['MODULE_NAME']);
		}
	}
	
	function loadDB( &$item, $tab , $path = ''){
	    global $_MAP, $_KAT;
//	    echo 'db/'.$tab.'/last/';
	    $_KAT['LAST']['form'] = (is_array($_MAP['SEARCH_FORM'][$tab]))?$_MAP['SEARCH_FORM'][$tab]:$_MAP['SEARCH_FORM']['def'];
	    cmd('db/'.$tab.'/last/reset');
	    //echo cmd('/db/'.$tab.'/last/'.(($_MAP['DB_DEEP'][$tab])?$_MAP['DB_DEEP'][$tab]:$_MAP['DB_DEEP']['def']));			echo 'db/'.$tab.'/last/'.(($_MAP['DB_DEEP'][$tab])?$_MAP['DB_DEEP'][$tab]:$_MAP['DB_DEEP']['def']);
			//print_r($_KAT['SEARCH_RES']);
	    for ($i = 0; $i < sizeof($_KAT['SEARCH_RES']); $i++){
		$data = $_KAT['SEARCH_RES'][$i];
		if (!empty($data['ts']))
		    $item['next'][$i]['ts'] = $data['ts'];
		if(!empty($path) && !empty($data['alias'))
			$item['next'][$i]['path'] = $path.$data['alias'];
		else
			$item['next'][$i]['path'] = '/db/'.$tab.'/'.$data['alias'];//.'+'.$data['m3'].'-'.$data['hidden'].'='.$i;
		if (!empty($data['name']))
		    $item['next'][$i]['name'] = $data['name'];//.'+'.$data['m3'].'-'.$data['hidden'].'='.$i;
	    }
	}
} 
/**
*
*/

function _map_tree_walk(&$item, $key, $deep, $type = '')
{
	global $_MAP;
	if (!empty($item['hidden'])) return;
	MAP::draw_item($item, $deep, $type);
		
	// loading db, if configured
	list($empty, $module, $table, $alias) = explode( '/', $item['path']); 
	$p = explode( '/', rtrim($item['path'],'/')); 
	// print_r($p); echo $table, $module.  "<br>"; //exit;
	if ($p[sizeof($p)-2] == 'db' && is_array($_MAP['DB_USE']) && in_array($p[sizeof($p)-1], $_MAP['DB_USE']) ){
	    MAP::loadDB( $item, $p[sizeof($p)-1] , $item['path']);
        
	}
		    
	if (!empty($item['next']) && is_array($item['next'])) {		
		@include Main::comm_inc($_MAP['MODULE_NAME'].'.nl_head'.$type.'.html', $f, $_MAP['MODULE_NAME']);
		foreach($item['next'] as $k => $v )
			_map_tree_walk($v, $k, $deep+1, $type);
		@include Main::comm_inc($_MAP['MODULE_NAME'].'.nl_foot'.$type.'.html', $f, $_MAP['MODULE_NAME']);
	}
}

?>