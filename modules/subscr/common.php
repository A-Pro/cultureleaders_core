<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : common.php
// PROJECT NAME : SUBSCRIBE
// DATE         : 09.03.2003
// Author       : Andrew Lyadkov (land@inforis.ru)
// Discription  : common functions
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////


if ( !defined('IN_LAND') )
{
	die("Hacking attempt");
}

error_reporting  (E_ERROR | E_WARNING | E_PARSE); // This will NOT report uninitialized variables
set_magic_quotes_runtime(0); // Disable magic_quotes_runtime

//
// addslashes to vars if magic_quotes_gpc is off
// this is a security precaution to prevent someone
// trying to break out of a SQL statement.
//
if( !get_magic_quotes_gpc() ){
	if( is_array($HTTP_GET_VARS) )	{
		while( list($k, $v) = each($HTTP_GET_VARS) )		{
			if( is_array($HTTP_GET_VARS[$k]) )			{
				while( list($k2, $v2) = each($HTTP_GET_VARS[$k]) )				{
					$HTTP_GET_VARS[$k][$k2] = addslashes($v2);
				}
				@reset($HTTP_GET_VARS[$k]);
			}else{
				$HTTP_GET_VARS[$k] = addslashes($v);
			}
		}
		@reset($HTTP_GET_VARS);
	}

	if( is_array($HTTP_POST_VARS) )	{
		while( list($k, $v) = each($HTTP_POST_VARS) )		{
			if( is_array($HTTP_POST_VARS[$k]) )		{
				while( list($k2, $v2) = each($HTTP_POST_VARS[$k]) )		{
					$HTTP_POST_VARS[$k][$k2] = addslashes($v2);
				}
				@reset($HTTP_POST_VARS[$k]);
			}else{
				$HTTP_POST_VARS[$k] = addslashes($v);
			}
		}
		@reset($HTTP_POST_VARS);
	}
}

//
// Define some basic configuration arrays this also prevents
// malicious rewriting of language and otherarray values via
// URI params
//
$lang = array();

include($land_root . 'config.'.$phpEx);

//if( !defined("LAND_SUBSCR_INSTALLED") )
//{
//	header("Location: install/install.$phpEx");
//	exit;
//}

include_once $land_root . "lang/". $CONF['lang']. ".php"; 
include_once($land_root . 'include/functions.'.$phpEx); 
include_once($land_root . 'include/plugins.'.$phpEx); 
include_once($land_root . 'include/mail.'.$phpEx); 
include_once($land_root . 'include/db.'.$phpEx);


//
// Obtain and encode users IP
//
if( getenv('HTTP_X_FORWARDED_FOR') != '' )
{
	$client_ip = ( !empty($HTTP_SERVER_VARS['REMOTE_ADDR']) ) ? $HTTP_SERVER_VARS['REMOTE_ADDR'] : ( ( !empty($HTTP_ENV_VARS['REMOTE_ADDR']) ) ? $HTTP_ENV_VARS['REMOTE_ADDR'] : $REMOTE_ADDR );
	if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", getenv('HTTP_X_FORWARDED_FOR'), $ip_list) ){
		$private_ip = array('/^0\./', '/^127\.0\.0\.1/', '/^192\.168\..*/', '/^172\.16\..*/', '/^10.\.*/', '/^224.\.*/', '/^240.\.*/');
		$client_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);
	}
}else{
	$client_ip = ( !empty($HTTP_SERVER_VARS['REMOTE_ADDR']) ) ? $HTTP_SERVER_VARS['REMOTE_ADDR'] : ( ( !empty($HTTP_ENV_VARS['REMOTE_ADDR']) ) ? $HTTP_ENV_VARS['REMOTE_ADDR'] : $REMOTE_ADDR );
}
$user_ip = encode_ip($client_ip);

//
//
//if (file_exists('install') || file_exists('contrib'))
//{
//	error('Please ensure both the install/ and contrib/ directories are deleted');
//}
?>