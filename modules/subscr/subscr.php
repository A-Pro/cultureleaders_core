<?php  
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : subscribe.php
// PROJECT NAME : SUBSCRIBE_NEWS
// BEGIN        : 09.03.2003
// Author       : Andrew Lyadkov (andruha@land.nnov.ru)
// Discription  : ���� ����������� email �� ��������
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
/*
USE:
----

-->	$land_email

<--	1. insert into table
	2. sending testing message with link to confirm

*/
define("IN_LAND", true);
global $_CORE, $_SUBSCR, $_CONF, $user_ip;
$user_ip = $_SERVER['REMOTE_ADDR'];

if (in_array("subscr",$_CONF['USED_MODULES'])){  
  $land_root	=	$_CORE->CoreModDir.'/';
  include $land_root . "extension.inc.php"; 
  include $land_root . "common.".$phpEx;
  switch ($Cmd) {
    case "adm_menu": // 13.08.2012
      global $_PROJECT;
      if (!empty($_SUBSCR['ADM_MENU']))
              $_PROJECT['ADM_MENU']        = array_merge($_PROJECT['ADM_MENU'], $_SUBSCR['ADM_MENU']         );  
      break;
  	case 'content/title/short':
  	case 'content/title':
  		global $_PROJECT;
  		$_PROJECT['PATH_2_CONTENT'][0]	= array(
  			'name'	=> ($_CORE->LANG != 'en')?'�������� ��������':'Subscribe news' ,
  			'path'	=> '/subscr/'
  		);
  		echo $_CORE->cmd_exec(array('user','/'.$Cmd));
      break;
  	case 'content/path':
  		echo ' ';
      break;
  	case "install": 
  		if ($_CORE->IS_ADMIN){
  			include_once $_CORE->CoreModDir."/install.php";
  		}
  		break;
  	case "subscribe":
  			$land_email = post_var('land_email');
  			$remail = post_var('remail');
        
        if (empty($_POST) ){
            include Main::comm_inc("subscribe.form.html", $f, 'subscr');
            break;
        }	     
  
  
  			if ($land_email == '' ) {
  				error_fin($lang['Email_empty']);
  			}elseif (!empty($remail)) {
  				$key = db_getkey($land_email);
  			//	echo $key."-".$land_email;
  				if ($key === 0) {
  			//		echo $key;
  					error_fin($lang['EmailNotInBase']);
  
  				}else {
  					send_registration( $land_email, $key );
  					good_fin($lang['Email_unSubscribe']);	
  				}
  			}elseif (plugin_checking_email($land_email) && !db_getkey($land_email)){
          $key = db_insert_email( $land_email );
  				send_registration( $land_email, $key );
  				good_fin($lang['Email_inserted']);	
  			}else {
          $key = db_getkey($land_email);
		      send_registration( $land_email, $key );
  				good_fin($lang['db_rec_is_present']);
  			}
  	break;
  	case "unsubscribe":
  	case "confirm":
  		include $land_root.$Cmd.'.php';
  		break;
  	case "admin":
      if ($_CORE->IS_ADMIN){
  		  include $land_root."admin/index.php";
      }
  		break;
  	case "":
  	case "admin/users":
      if ($_CORE->IS_ADMIN){
  		  include $land_root."admin/admin_subscribe.php";
      }
  		break;
      case 'module/title':
      	echo @$_SUBSCR['MODULE_TITLE'];
  		break;                
      case 'module/desc':
      	echo @$_SUBSCR['MODULE_DESC'];
  		break;                		
  	default:
  		if ($db) $db->Destroy();
      
  }
}
?>