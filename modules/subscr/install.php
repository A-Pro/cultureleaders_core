<H3>Installing Module::SUBSCR</H3>
<?
/*

UPGRADE 15.08.2005
==================
1. ��������� �������� ������ ��� ������
2. �����, �������� ��� �����������

INSTAL
======

1. 
	���� ����� ���� �������� �� .php ����������, �� �������� ��� �
	����� /extension.inc.php
2. 
	����������� config.php ��� ���� �����, ��� ��������� ����������� �����
	��������� ����������� � ���� ������, ������ � �������� �����
3.
	������� ������� � ����:

	CREATE TABLE land_subscribe (
	email		varchar(50) PRIMARY KEY NOT NULL DEFAULT 'some@email.ru',
	ip			varchar(20),
	stamp		int4,
	code_key	varchar(64) NOT NULL DEFAULT '1',
	confirm		int4 NOT NULL DEFAULT 1,
	confirm_ip	varchar(20)
	)
4.
	�� ��������� ������������ ����� ��� ������ � PostgreSQL, �� ���� ������������ MySQL
	�� ����������� 
	/db/my/* -> /db/*
	����� ����������:
	"class.Connection.php";
	"class.Query.php";


������:
------
5.
	���� ��� ����� �� �������� ����� ������, ����������� ��� �����������
	/includes/mail.php::function send_registration()
6.
	�������� ������ � ����� ������
	/includes/mail.php::fadd2message()

������� ���:
-----------
7.	
	��������� /include/header.html & /include/footer/html
	��� ����� ������� ����������, ����� � ����� ��������� �� ������� � �������.

8.
	����� ��� ���������� ��������:
	<FORM METHOD=POST ACTION="subscribe.php">
	<INPUT TYPE="text" NAME="land_email"><INPUT TYPE="submit">
	</FORM>
	���
	<FORM METHOD=POST ACTION="/subscr/subscribe.php">
	�������� �� �������: 
	<INPUT TYPE="text" NAME="land_email" value='< ������� E-mail >' OnFocus='if (this.value == "< ������� E-mail >") this.value=""'>&nbsp;<INPUT TYPE="submit" value=">"  class=small onmouseover="this.style.backgroundColor='#465893'" onmouseout="this.style.backgroundColor='#253B6C'">
	<INPUT TYPE="submit" name="remail" value="�����"  class=small onmouseover="this.style.backgroundColor='#465893'" onmouseout="this.style.backgroundColor='#253B6C'">


�������� �����:
--------------
9.
	� ���������� /admin ��������� ������ send_news.php �� ������ ��� �������� �����
	�������� ����� ��� ���� ������������ �� ���� �������������� �������� �������,
	���������� � ���� ������.
10.
	�������� ����: http://httpd.apache.org/docs/howto/auth.html

# htpasswd -c /usr/local/apache/passwd/passwords rbowen
New password: mypassword
Re-type new password: mypassword
Adding password for user rbowen

#chown root.nogroup /usr/local/apache/passwd/passwords
#chmod 640 /usr/local/apache/passwd/passwords

>.htaccess
AuthType Basic
AuthName "By Invitation Only"
AuthUserFile /usr/local/apache/passwd/passwords
Require user rbowen sungo
Require valid-user

*/

$tables = SQL::tlist();
echo "<PRE>
<b>Tables</b>:
";
//print_r($tables);

if (!in_array($CONF['tab_prefix'].'subscribe', $tables)){
	echo "NOPE\n";
	
	$query = "
	CREATE TABLE ".$DB_CONF['tab_prefix'].'subscribe'." (
	email		varchar(50) PRIMARY KEY NOT NULL DEFAULT 'some@email.ru',
	ip			varchar(20),
	stamp		int(11),
	code_key	varchar(255) NOT NULL DEFAULT '1',
	confirm		int(11) NOT NULL DEFAULT 1,
	confirm_ip	varchar(20)
	)	
	";
	echo "<b>QUERY</b>\n".$query;
	$res = SQL::query($query,0);
	if (!$res->Result)
		echo $res->ErrorQuery;	
	
}else{
	echo "Table ".$DB_CONF['tab_prefix'].'subscribe'." exists!!!";
}

?>