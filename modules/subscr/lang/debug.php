<?php
	if ( !defined('IN_LAND') )
	{
		die("Hacking attempt");
	}


	$lang['Email_empty']		= 'Empty email';
	$lang['Email_inserted']		= 'Email_inserted';

	$lang['db_rec_is_present']	= 'db_rec_is_present';
	$lang['KeyEmpty']			= 'Key is empty';
	$lang['EmailNotInBase']		= 'E-mail not found in base';
	$lang['KeyIsWrong']			= 'Wrong key for this E-mail';

	$lang['ConfirmSubscribe']	= 'Email Confirmed';
	$lang['AlreadyConfirmed']	= 'Email Allready Confirmed';
	$lang['NoConfirmedEmails']	= 'No Confirmed emails';
	$lang['ConfirmUnSubscribe']	= 'Email removed';

	$lang['NewsSent']			= 'News sent successfuly';

	$lang['MessResend']			= 'Message with Link were sent.';

?>
