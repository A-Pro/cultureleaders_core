<?php

if ( !defined('IN_LAND') ){
	die("Hacking attempt");
}

global $CONF, $DB_CONF;

$land_root_path = './';
/*
�����, ������� ���������� � ������� � ������� 
*/
$CONF['url']			= $_SERVER['HTTP_HOST'];
/*
���� �� ����� ������ �� ������� � �������������� �� ��������
(����������� � ������)
*/
$CONF['subscribe']		= "/subscr/confirm";
/*
���� �� ����� ������ �� ������� ��� ������ �� ��������
(����������� � ������)
*/
$CONF['unsubscribe']	= "/subscr/unsubscribe";
/*
���� From: � �������
*/
$CONF['from']			= TO;
/*
���� ���������� (���� ���� ��� debug)
*/
$CONF['lang']			= "russian";
/*
�������� ��� ����������� � ���� ������
*/
$DB_CONF['tab_prefix']	= (defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : 'ti_subscr_'); // ����� ��������

$_SUBSCR['MODULE_TITLE'] = "��������";
$_SUBSCR['MODULE_DESC'] = "���������� ��������� ���������� v.0.1";

$_SUBSCR['ADM_MENU'] = array(
	"/subscr/" => array("/ico/a_lk.gif", $_SUBSCR['MODULE_TITLE'], "subscr/")
);

?>