<?php
/*
USE functions:

 - is_email();
	
*/
if ( !defined('IN_LAND') ) die("Hacking attempt");

$MAIL_ERROR = '';


/**
*
*/
function send_mass_email( &$email_arr, &$text )
{
	global $CONF, $MAIL_ERROR;
	if ($text == '') {
		$MAIL_ERROR .= "Text is empty! :( [send_mass_email()]";
		return false;
	}

	$text .= 
	add2message($text);

	if (!is_array($email_arr)) {
		if (is_email($email_arr)) 
			send_mail_s( $email_arr, "������� � " . $CONF['url'], $text, $CONF['from']);
		else {
			$MAIL_ERROR .= "$email_arr - is not valid email! :( [send_mass_email()]";
			return false;
		}
	}else{

		foreach ($email_arr as $email) {
			send_mail_s( $email, "������� � " . $CONF['url'], $text, $CONF['from']);
		}
	}
}

/**
*
*/
function send_registration( $email, $key )
{
	global $CONF;
	$tmp = str_replace("@","::",$email);
	$text = "�� ��� ���-�� ���� ��������� ����� ��� ��������� �������� � �����
" . $CONF['url'] . "
��� ������������� �������� �������� �� ��������� ������:
http://" . $CONF['url'] .$CONF['subscribe'] . "?email=$tmp&id=$key
��� ������ �� �������� ������ ��� ����� �������� �� ��������� ������:
http://" . $CONF['url'] .$CONF['unsubscribe'] . "?email=$tmp&id=$key";
	add2message($text);
	send_mail_s( $email.", ".TO, "����������� �������� " . $CONF['url'], $text, $CONF['from']);
}

/**
*
*/
function send_mail_s( $emailto, $subj, $body, $from )
{
	global $MAIL_CONF;
	global $MAIL_ERROR;
	// ������� ��������� ��� � ������ ���������
	// ��������� � ���������
	$bodytext	= convert_cyr_string ($body, "w", "k");
	$subj		= convert_cyr_string ($subj, "w", "k");
	$from		= "From: " . $from ."\nContent-Type: text/plain; charset=koi8-r\nContent-Transfer-Encoding: 8bit";
	// ��������
	@mail($emailto, $subj, $bodytext, $from);
//	if (@mail($emailto, $subj, $bodytext, $from)){
//		return true;
//	}else{
//		$MAIL_ERROR .= "������ c ������� �� ����������.";
//		error_fin( $MAIL_ERROR." [mail::send_mail_s()]<br>����� ������: <br><PRE>$body" );
//		return false;
//	}
} 

/**
*
*/
function add2message( &$text )
{
	global $CONF;
	$text = "������������,
	".$text."
---
������������� ����� " . $CONF['url'];
}
?>