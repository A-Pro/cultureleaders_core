<?php
if ( !defined('IN_LAND') ) die("Hacking attempt");

/**
*
*/
function plugin_checking_email( $email )
{
	if (!is_email($email)) {
		error_fin("Недопустимый e-mail [plugin_check_email::is_email]");
		return false;
	}else {
		return true;
	}

}

/**
*
*/
function is_email( $email )
{
	if ( empty( $email ) || ! strchr( $email, '@' )) return 0;
	list($first, $domain) = explode("@", $email);
	$point = strpos($domain, ".");
	if ( strstr($email, " ") || $point == 0 || $point == strlen( $domain ) || strlen($first) == 0 ) return 0;
	else return 1;
}
?>