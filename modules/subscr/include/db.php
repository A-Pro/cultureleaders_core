<?php
/*
CREATE TABLE land_subscribe (
email		varchar(50) PRIMARY KEY NOT NULL DEFAULT('some@email.ru'),
ip			varchar(20),
stamp		int4,
key			varchar(64) NOT NULL DEFAULT(1),
confirm		int4 NOT NULL DEFAULT(1),
confirm_ip	varchar(20)
)
*/
if ( !defined('IN_LAND') )
{
	die("Hacking attempt");
}
if (!$_CORE->dll_load('class.DBCQ'))
	die ($_CORE->error_msg());

$DB_ERROR	= '';


/**													db_insert_email()
*
*/
function db_insert_email( $email, $quick = false )
{
	global $user_ip;
	global $lang;
	global $DB_ERROR;
	global $db, $DB_CONF;

//	$old_key	= db_recispresent( $email );
//	if ($old_key == 1) { // ������ ��� ����������� � ���� ��������
//		error_fin($lang['db_rec_is_present']);
//		return false;
//	}elseif ($old_key != 0) {
//		return $old_key; // ������ ������� ������
//	}

	$key = get_key();

	$confirm = ($quick) ? time() : 1;
		
//	$cmd	= "INSERT INTO " . $DB_CONF['tab_prefix'] ."subscribe (email, ip, stamp, code_key, confirm) VALUES ( '$email', '$user_ip', '" . time() . "', '$key', '$confirm' )";
	$res	= SQL::ins($DB_CONF['tab_prefix'] ."subscribe", "email, ip, stamp, code_key, confirm", "'$email', '$user_ip', '" . time() . "', '$key', '$confirm'");

	if (!$res->Result ) {
		if($_CORE->IS_ADMIN) error_fin( $res->ErrorQuery." [db_insert_email()::insert]".$cmd );
		return false;
	}
	$res->Destroy();
	return $key;
}


/**													db_recispresent( $email, $key = '' )
*
*/
function db_recispresent( $email, $key = '')
{
	global $db, $DB_CONF;

	$res	= SQL::sel('confirm, code_key', $DB_CONF['tab_prefix'] . "subscribe", "email = '". $email ."' ".(($key != '')?" AND code_key = '". $key ."'":''));
	if (!$res->Result) {
		error_fin( $res->ErrorQuery."[db_recispresent()::insert]" );
	}
	if ($res->NumRows > 0) {
		$res->FetchArray(0);
		$return = $res->FetchArray['confirm'];
//		$return = ($key != '') ? $res->FetchArray['confirm']: (($res->FetchArray['confirm'] == 1) ? $res->FetchArray['code_key'] : 1);
	}else {
		$return = 0;
	}
	$res->Destroy();
	return $return;
}
/**													db_recispresent( $email, $key = '' )
*
*/
function db_getkey( $email )
{
	global $db, $DB_CONF;

	$res	= SQL::sel('confirm, code_key', $DB_CONF['tab_prefix'] . "subscribe", "email = '". $email ."' ");
	if (!$res->Result) {
		error_fin( $res->ErrorQuery."[db_recispresent()::insert]" );
	}
	if ($res->NumRows > 0) {
		$res->FetchArray(0);
		$return = $res->FetchArray['code_key'];
	}else {
		$return = 0;
	}
	$res->Destroy();
	return $return;
}
/**													db_recupdate( $email, $key )
*
*/
function db_recupdate( $email, $key )
{
	global $user_ip;
	global $lang;
	global $db, $DB_CONF;
	
	if ($key == '') 
		error_fin( $lang['KeyEmpty'] );

	$confirm = db_recispresent($email, $key);
	if ( $confirm == 0 ) {
		error_fin( (!db_recispresent($email)) ? $lang['EmailNotInBase'] : $lang['KeyIsWrong'].$key.$email );
		return false;
	}
	if ( $confirm > 1) {
		error_fin( $lang['AlreadyConfirmed'] );
		return false;
	}

	$res	= SQL::upd($DB_CONF['tab_prefix'] . "subscribe", "confirm = '". time() ."', confirm_ip = '". $user_ip ."'", "email = '". $email ."' AND code_key = '". $key ."'" );
	if (!$res->Result) {
		error_fin( $res->ErrorQuery."[db_recupdate()::update]-><br>".$cmd );
		return false;
	}
	return true;
}

/**													db_recdelete( $email, $key )
*
*/
function db_recdelete( $email, $key )
{
	global $user_ip;
	global $lang;
	global $db, $DB_CONF;
  
	if (!defined('ADMIN_SERVICE') && $key == '') 
		error_fin( $lang['KeyEmpty'] );

	$confirm = db_recispresent($email, $key);
	if ( $confirm == 0 ) 
		error_fin( (!db_recispresent($email)) ? $lang['EmailNotInBase'] : $lang['KeyIsWrong'] );
	// for admin delete
	$and = (!defined('ADMIN_SERVICE')) ? "AND code_key = '". $key ."'" : '';
	
	$res	= SQL::del($DB_CONF['tab_prefix'] . "subscribe", "email = '". $email ."' ".$and);
	if (!$res->Result) {
		error_fin( $res->ErrorQuery."[db_recdelete()::delete]<br>".$cmd );
	}
	return true;
}

/**
*
*/
function db_getemaillist( &$email_arr, $all=false, $exit = true )
{
	global $lang;
	global $db, $DB_CONF;
		
	$select = ($all) ? '*' : 'email';

	$res	= SQL::sel($select, $DB_CONF['tab_prefix'] . "subscribe", "confirm != '1'");
	if (!$res->Result) {
		error_fin( $res->ErrorQuery."[db_getmaillist()::select]", $exit );
	}
	if ($res->NumRows == 0) {
		error_fin( $lang['NoConfirmedEmails'], $exit );
		$res->Destroy();
		return false;
	}else {
		for ($i = 0; $i < $res->NumRows ; $i++) {
			$res->FetchArray($i);
			$email_arr[]	= ($all) ? $res->FetchArray : $res->FetchArray['email'];
		}
		return true;
	}
}
?>