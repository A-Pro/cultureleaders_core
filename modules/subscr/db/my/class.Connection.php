<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : class.Connection.php 
// PROJECT NAME : CLASSES/Land
// DATE         : 26.09.2002
// Author       : Andrew Lyadkov (andrey@lyadkov.nnov.ru)
// Discription  : ����� ���������� � ����� ������ MySQL
// ...........  : 
// Comment      : ver 1.0
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
	class Connection {
		// ����������� ���������� ����������� � ���� ������
		var $HostName;		// ��� �����
		var $UserName;		// ��� ������������
		var $UserPassword;	// ������ ������������
		var $Port="5432";	// ����� �����
		var $DataBase;		// ��� ���� ������

		// ����������� ���������� ��� ������ � ����� ������
		var $Connect;		// ��������� �� ���������� � ����� ������
		var $CloseConnect;	// ��������� �� �������� ���������� � ����� ������

		// ����������� ���������� ������
		var $ErrorOpenConnect;	// ������ ���������� � ����� ������
		var $ErrorCloseConnect;	// ������ �������� ���������� � ����� ������

		//////////////////////////////////////////////
		//  ����������� ������ (���������� � ��)    //
		//////////////////////////////////////////////
		function Connection($ConfigFile='',$Host_Name='',$User_Name='',$User_Password='',$Data_Base='',$Post='') {
			if ($ConfigFile!='' && $ConfigFile != 'none') {
				require "$ConfigFile";
			}
			// ����������� ��������� ����������� � ��
			$this->HostName     = $Host_Name;
			$this->UserName     = $User_Name;
			$this->UserPassword = $User_Password;
			$this->DataBase     = $Data_Base;
		        //echo $Data_Base;
			// ���������� � PostgreSQL	$cfg_host,$cfg_user,$cfg_passwd
			$this->Connect = @mysql_connect($this->HostName,$this->UserName,$this->UserPassword);
			// �������� ����������
			if (!$this->Connect) {
				$this->ErrorOpenConnect = mysql_error();
			if (!mysql_select_db($Data_Base))
				$this->ErrorOpenConnect .= mysql_error();

				return;
			}
		}// End of fucntion Connect()

		//////////////////////////////////////////////
		//          ����������� �������             //
		//////////////////////////////////////////////
		function Destroy() {
			// ������������ �������� ���������� � ����� ������
			$this->CloseConnect = mysql_close($this->Connect);

			// �������� ������������ �������� ����������
			if (!$this->CloseConnect) {
				$this->ErrorCloseConnect = mysql_error($this->CloseConnect);
				return;
			}
			else
				// ������� ������ ������
				unset($this);
		}// End of Destroy
	}// End of class Connect
?>
