<?php

    //////////////////////////////////////////////////
    //	 Query  -  ��������� �������� ���� ������	//
    //**********************************************//
    //      (c) Copyright 2001 Inforis              //
    //          http://www.inforis.ru               //
    //----------------------------------------------//
    //      Programming by Artem Abramyan           //
    //              art@inforis.ru                  //
    //////////////////////////////////////////////////

	class Query {
		// ����������� ���������� ��� ������ � ����� ������
		var $Result;	// ��������� ��������� �������
		var $NumRows;	// ���������� �������
		var $NumFields;	// ���������� ��������  / Added by Elena
		var $FetchRow;	// ������ ���������� ������� ������
		var $FetchArray;// Added by Land 10.12.01
		var $Tuples;	// ���������� �����������, ���������� � ��������� �������
		var $Destroy;	// ��������� �� ������������ ������ ���������� ���������

		// ����������� ���������� ������
		var $ErrorQuery;	// ������ ��������� �������
		var $ErrorFetchRow;	// ������ ��������� ���������� �������
		var $ErrorNumRows;	// ������ �������� ���������� ������� ���������� �������
		var $ErrorTuples;	// ������ �������� ���������� �����������, ���������� � ��������� �������

		//////////////////////////////////////////////
		//			��������� ��������				//
		//////////////////////////////////////////////
		function Query($Connect_id,$Query) {
			// ������������ ������ � ���� ������
			$this->Result = @pg_exec($Connect_id,$Query);

			// �������� ������������ ������������� �������
			if (!$this->Result) {
				$this->ErrorQuery = pg_errormessage($Connect_id);
				return;
			}
			else {
				// ������ ���������� �������
				$this->NumRows = pg_numrows($this->Result);
				$this->NumFields = pg_numfields($this->Result);  // Added by Elena
				
				// ������ ���������� �����������, ���������� � ��������� �������
				$this->Tuples = pg_cmdtuples($this->Result);
			}
		}// End of function Query

		//////////////////////////////////////////////
		//      ��������� ����������� �������       //
		//////////////////////////////////////////////
		function FetchRow($Row) {
			// ������������ ��������� ������� ���������� ������
			$this->FetchRow = @pg_fetch_object($this->Result,$Row);

			// �������� ������������ ��������� �������
			if (!$this->FetchRow) {
				$this->ErrorFetchRow = @pg_errormessage($this->FetchRow);
			}
		}// End of function FetchRow

		//////////////////////////////////////////////
		//      ADDED by Land (FETCH_ARRAY)       //
		//////////////////////////////////////////////
		function FetchArray($Row) {
			// ������������ ��������� ������� ���������� ������
			$this->FetchArray = pg_fetch_array($this->Result,$Row);

			// �������� ������������ ��������� �������
			if (!$this->FetchArray) {
				$this->ErrorFetchRow = pg_errormessage($this->FetchArray);
			}
		}// End of function FetchRow

		//////////////////////////////////////////////
		//		��������� ����������� �������		//
		//////////////////////////////////////////////
		function Destroy() {
			// ������������ ������������ ������
			$this->Destroy = pg_freeresult($this->Result);

			// �������� ������������ ������������ ������
			if (!$this->Destroy) {
				$this->ErrorDestroy = pg_errormessage($this->Destroy);
			}
		}// End of function Destroy


	}// End of class Query
?>
