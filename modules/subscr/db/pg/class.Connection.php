<?php

    //////////////////////////////////////////////////
    // Connection - ����� ���������� � ����� ������	//
    //**********************************************//
    //      (c) Copyright 2001 Inforis              //
    //          http://www.inforis.ru               //
    //----------------------------------------------//
    //      Programming by Artem Abramyan           //
    //              art@inforis.ru                  //
    //////////////////////////////////////////////////

	class Connection {
		// ����������� ���������� ����������� � ���� ������
		var $HostName;		// ��� �����
		var $UserName;		// ��� ������������
		var $UserPassword;	// ������ ������������
		var $Port="5432";	// ����� �����
//		var $Port;
		var $DataBase;		// ��� ���� ������

		// ����������� ���������� ��� ������ � ����� ������
		var $Connect;		// ��������� �� ���������� � ����� ������
		var $CloseConnect;	// ��������� �� �������� ���������� � ����� ������

		// ����������� ���������� ������
		var $ErrorOpenConnect;	// ������ ���������� � ����� ������
		var $ErrorCloseConnect;	// ������ �������� ���������� � ����� ������

		//////////////////////////////////////////////
		//  ����������� ������ (���������� � ��)    //
		//////////////////////////////////////////////
		function Connection($ConfigFile='none',$HostName='none',$UserName='none',$UserPassword='none',$DataBase = 'none', $Port='none') {			
			if (isset($ConfigFile) && !empty($ConfigFile) && $ConfigFile != "none") {				
				require( "$ConfigFile");
				
				// ����������� ��������� ����������� � ��
				$this->HostName     = $Host_Name;
				$this->UserName     = $User_Name;
				$this->UserPassword = $User_Password;
				$this->DataBase     = $Data_Base;
				if (isset($Port) && $Port != "none")
					$this->Port			= $Port;
			//	else $this->Port="5432";
					
			}
			else {
				$this->HostName     = $HostName;
				$this->UserName     = $UserName;
				$this->UserPassword = $UserPassword;
				$this->DataBase     = $DataBase;
				if (isset($Port) && $Port != "none")
					$this->Port		= $Port;
			//	else $this->Port="5432";
				
			}

			// ���������� � PostgreSQL
			$this->Connect = @pg_connect("host=$this->HostName port=$this->Port dbname=$this->DataBase user=$this->UserName password=$this->UserPassword");

			// �������� ����������
			if (!$this->Connect) {
				$this->ErrorOpenConnect = pg_errormessage($this->Connect);
				return;
			}
		}// End of fucntion Connect()

		//////////////////////////////////////////////
		//          ����������� �������             //
		//////////////////////////////////////////////
		function Destroy() {
			// ������������ �������� ���������� � ����� ������
			$this->CloseConnect = pg_close($this->Connect);

			// �������� ������������ �������� ����������
			if (!$this->CloseConnect) {
				$this->ErrorCloseConnect = pg_errormessage($this->CloseConnect);
				return;
			}
			else
				// ������� ������ ������
				unset($this);
		}// End of Destroy
	}// End of class Connect
?>
