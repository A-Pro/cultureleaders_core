<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : class.Connection.php 
// PROJECT NAME : CLASSES/Land
// DATE         : 15.09.2004
// Author       : Andrew Lyadkov (andrey@lyadkov.nnov.ru)
// Discription  : ����� ���������� � ����� ������ MySQL
// ...........  : 
// Comment      : ver 1.2
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
	class Connection {
		// ����������� ���������� ����������� � ���� ������
		var $HostName;		// ��� �����
		var $UserName;		// ��� ������������
		var $UserPassword;	// ������ ������������
		var $Port="5432";	// ����� �����
		var $DataBase;		// ��� ���� ������
		var $DBType	= 'mysql';

		// ����������� ���������� ��� ������ � ����� ������
		var $Connect;		// ��������� �� ���������� � ����� ������
		var $CloseConnect;	// ��������� �� �������� ���������� � ����� ������
		var	$Tables;		// ������ ������ ����
		var	$Field;			// ������ ����� �������

		// ����������� ���������� ������
		var $ErrorOpenConnect;	// ������ ���������� � ����� ������
		var $ErrorCloseConnect;	// ������ �������� ���������� � ����� ������

		//////////////////////////////////////////////
		//  ����������� ������ (���������� � ��)    //
		//////////////////////////////////////////////
		function Connection($ConfigFile='',$Host_Name='',$User_Name='',$User_Password='',$Data_Base='',$Post='') {
			if ($ConfigFile!='' && $ConfigFile!='none' && is_file($ConfigFile)) {
				require "$ConfigFile";
			}
			if (is_array($Host_Name)) {
				$User_Name		= $Host_Name['user'];
				$User_Password	= $Host_Name['pwd'];
				$Data_Base		= $Host_Name['name'];
				$Port			= $Host_Name['port'];
				$Host_Name		= $Host_Name['host'];
			}
			// ����������� ��������� ����������� � ��
			$this->HostName     = $Host_Name;
			$this->UserName     = $User_Name;
			$this->UserPassword = $User_Password;
			$this->DataBase     = $Data_Base;
			$this->Port			= $Port;

				//echo $Data_Base;
			// ���������� � SQL
			$this->Connect = @mysql_connect($this->HostName,$this->UserName,$this->UserPassword);
			// �������� ����������
			if (!$this->Connect) {
				$this->ErrorOpenConnect = mysql_error();
				return false;
			}
			if (!mysql_select_db($Data_Base))
				$this->ErrorOpenConnect = mysql_error();

		}// End of fucntion Connect()

		//////////////////////////////////////////////
		//  ������ ������						    //
		//////////////////////////////////////////////
		function FieldsList( $table='' )
		{
			if (!$this->Connect) return false;
			$result= mysql_list_fields($this->DataBase, $table, $this->Connect);
			if (!$result) { $this->Error = mysql_error();return false;}
			else for ($i=0; $i < mysql_num_fields($result); $i++){
				$this->Field[$i]['name']	= mysql_field_name($result, $i);
				$this->Field[$i]['type']	= mysql_field_type($result, $i);
				$this->Field[$i]['len']	= mysql_field_len($result, $i);
				$this->Field[$i]['flags']	= mysql_field_flags($result, $i);
			}
			return $this->Field;
		}

		//////////////////////////////////////////////
		//  ������ ������						    //
		//////////////////////////////////////////////
		function TablesList()
		{
			if (!$this->Connect) return false;
			$result = mysql_list_tables($this->DataBase, $this->Connect);
			if (!$result) { $this->Error = mysql_error();return false;}
			else for ($i=0; $i < mysql_num_rows($result); $i++){
				$row	= mysql_fetch_row( $result);
				$this->Tables[] = $row[0];
			}
			return $this->Tables;
		}

		//////////////////////////////////////////////
		//          ����������� �������             //
		//////////////////////////////////////////////
		function Destroy() {
			// ������������ �������� ���������� � ����� ������
			$this->CloseConnect = mysql_close($this->Connect);

			// �������� ������������ �������� ����������
			if (!$this->CloseConnect) {
				$this->ErrorCloseConnect = mysql_error($this->CloseConnect);
				return;
			}
			else
				// ������� ������ ������
				unset($this);
		}// End of Destroy
	}// End of class Connect
?>
