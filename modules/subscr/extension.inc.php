<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : extension.inc.php
// PROJECT NAME : 
// DATE         : 09.03.2003
// Author       : Andrew Lyadkov (andruha@land.nnov.ru)
// Discription  : php file Extension
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////


if ( !defined('IN_LAND') )
{
	die("Hacking attempt");
}

//
// Change this if your extension is not .php!
//
$phpEx = "php";

$starttime = 0;

?>