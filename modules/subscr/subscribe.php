<?php
 //////////////////////////////////// About  this file ////////////////
//
// FILE         : subscribe.php
// PROJECT NAME : SUBSCRIBE_NEWS
// BEGIN        : 09.03.2003
// Author       : Andrew Lyadkov (andruha@land.nnov.ru)
// Discription  : ���� ����������� email �� ��������
// ...........  :
// Comment      :
// ...........  :
// Coding       : CP-1251
//
//////////////////////////////////////////////////////////////////////
/*
USE:
----

-->	$land_email

<--	1. insert into table
	2. sending testing message with link to confirm

*/
global $_CORE;
define("IN_LAND", true);
$land_root	=	$_CORE->CoreModDir."/";
include $land_root . "extension.inc.php";
include $land_root . "common.".$phpEx;

$land_email = post_var('land_email');
$remail = post_var('remail');

if ($land_email == '') 
	error_fin($lang['Email_empty']);	

plugin_checking_email($land_email);

if (!empty($remail)) {
	$key = db_getkey($land_email);
//	echo $key."-".$land_email;
	if ($key === 0) {
//		echo $key;
		error_fin($lang['EmailNotInBase']);

	}else {
		send_registration( $land_email, $key );
		good_fin($lang['MessResend']);	
	}
} else {
	$key = db_insert_email( $land_email );
	send_registration( $land_email, $key );
	good_fin($lang['Email_inserted']);	
}

?>