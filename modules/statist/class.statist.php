<?php

  define( "Sday", "86400");

  global $_STATIST;

  class STATIST
    {

            function STATIST()
              {       global $_STATIST_TABLES;
                      if( empty( $_STATIST_TABLES)) die("\$_STATIST_TABLES is empty!!!");
                      $this->Connect();
              }
            function Connect()
              {
                      global $SQL_DBCONF,$SQL_DBLINK, $_STATIST_TABLES;
                      if( empty( $SQL_DBLINK))
                              return SQL::connect();
                      else
                              return true;
              }
//////////////��������  � �������� ������/////////
            function CreateUsersIDTable( $debug=0 )
              {       GLOBAL $_STATIST,$SQL_DBLINK;
                      if( empty( $SQL_DBLINK )) die ( "$SQL_DBLINK not init!!!" );

                       $SSqlQuery = "CREATE TABLE `".$_STATIST['CONST']['users_id']['tab']."` (
									  `val` bigint(20) unsigned default NULL,
									  `lastGroup` bigint(9) NOT NULL default '0',
									  `lastSeedIP` mediumint(9) NOT NULL default '0',
									  `lastSpidersTime` mediumint(9) NOT NULL default '0'
									)";
                        if( !empty($debug))  echo "$SSqlQuery<br>";
                        $res = new Query($SQL_DBLINK, $SSqlQuery);
                        SQL::ins( $_STATIST['CONST']['users_id']['tab'], 'val', "'1'");
              }
            function DeleteUsersIDTable( $debug=0 )
              {       GLOBAL $_STATIST,$SQL_DBLINK; if( empty( $SQL_DBLINK )) die ( "$SQL_DBLINK not init!!!" );
                      $SSqlQuery = "DROP TABLE ".$_STATIST['CONST']['users_id']['tab'];
                      if( !empty($debug))  echo "$SSqlQuery<br>";
                      $res = new Query($SQL_DBLINK, $SSqlQuery);
              }

             function CreateTables($debug=0)
              {
                      global $SQL_DBLINK, $_STATIST;
                      if( empty($SQL_DBLINK) ) die ( "\$SQL_DBLINK not init!!!" );
                      STATIST::CreateUsersIDTable( @$debug );

                      $SSqlQuery = "CREATE TABLE ".$_STATIST['TABLES']['All'];
                      $SSqlQuery .= " ( id INT UNSIGNED auto_increment PRIMARY KEY, Date DATE, Time MEDIUMINT UNSIGNED";
					  $SSqlQuery .=", id".$_STATIST['TABLES']['Addr']."  ".$_STATIST['auto_type'];
					  $SSqlQuery .=", id".$_STATIST['TABLES']['IP']."  ".$_STATIST['auto_type'];
					  $SSqlQuery .=", id".$_STATIST['TABLES']['Title']."  ".$_STATIST['auto_type'];
					  $SSqlQuery .=", id".$_STATIST['TABLES']['Ref']."  ".$_STATIST['auto_type'];
					  $SSqlQuery .=", User MEDIUMINT UNSIGNED, Size MEDIUMINT UNSIGNED )";
                      if( !empty($debug))  echo "$SSqlQuery<br>";
                      $res = new Query($SQL_DBLINK, $SSqlQuery);

                      $SSqlQuery = "CREATE TABLE ".$_STATIST['TABLES']['Spiders'];
                      $SSqlQuery .= " ( id INT UNSIGNED auto_increment PRIMARY KEY, Date DATE, Time MEDIUMINT UNSIGNED";
					  $SSqlQuery .=", id".$_STATIST['TABLES']['IP']."  ".$_STATIST['auto_type'].")";
                      if( !empty($debug))  echo "$SSqlQuery<br>";
                      $res = new Query($SQL_DBLINK, $SSqlQuery);

                      $SSqlQuery = "CREATE TABLE ".$_STATIST['TABLES']['Need'];
                      $SSqlQuery .= " ( id INT UNSIGNED auto_increment PRIMARY KEY, Date DATE, Time MEDIUMINT UNSIGNED";
					  $SSqlQuery .=", id".$_STATIST['TABLES']['Addr']."  ".$_STATIST['auto_type'];
					  $SSqlQuery .=", id".$_STATIST['TABLES']['IP']."  ".$_STATIST['auto_type'];
					  $SSqlQuery .=", id".$_STATIST['TABLES']['Title']."  ".$_STATIST['auto_type'];
					  $SSqlQuery .=", id".$_STATIST['TABLES']['Search']."  ".$_STATIST['auto_type'];
					  $SSqlQuery .=", id".$_STATIST['TABLES']['Words']."  ".$_STATIST['auto_type'].")";
                      if( !empty($debug))  echo "$SSqlQuery<br>";
                      $res = new Query($SQL_DBLINK, $SSqlQuery);
                      
                      $SSqlQuery = "CREATE TABLE ".$_STATIST['TABLES']['From'];
                      $SSqlQuery .= " ( id INT UNSIGNED auto_increment PRIMARY KEY, Date DATE, Time MEDIUMINT UNSIGNED";
					  $SSqlQuery .=", id".$_STATIST['TABLES']['Addr']."  ".$_STATIST['auto_type'];
					  $SSqlQuery .=", id".$_STATIST['TABLES']['Ref']."  ".$_STATIST['auto_type'];
					  $SSqlQuery .=", val MEDIUMINT UNSIGNED)";
                      if( !empty($debug))  echo "$SSqlQuery<br>";
                      $res = new Query($SQL_DBLINK, $SSqlQuery);

                      $SSqlQuery = "CREATE TABLE ".$_STATIST['TABLES']['Pages'];
                      $SSqlQuery .= " ( id INT UNSIGNED auto_increment PRIMARY KEY, Date DATE, Time MEDIUMINT UNSIGNED";
					  $SSqlQuery .=", id".$_STATIST['TABLES']['Addr']."  ".$_STATIST['auto_type'];
					  $SSqlQuery .=", id".$_STATIST['TABLES']['Ref']."  ".$_STATIST['auto_type'];
					  $SSqlQuery .=", val MEDIUMINT UNSIGNED)";
                      if( !empty($debug))  echo "$SSqlQuery<br>";
                      $res = new Query($SQL_DBLINK, $SSqlQuery);

                      $SSqlQuery = "CREATE TABLE ".$_STATIST['TABLES']['Group'];
                      $SSqlQuery .= " ( id INT UNSIGNED auto_increment PRIMARY KEY, Date DATE";
					  $SSqlQuery .=", AllUser MEDIUMINT UNSIGNED, AllIP MEDIUMINT UNSIGNED";
					  $SSqlQuery .=", Allof MEDIUMINT UNSIGNED, AllSearch MEDIUMINT UNSIGNED";
					  $SSqlQuery .=", AllSize MEDIUMINT UNSIGNED, AllMistake MEDIUMINT UNSIGNED )";
                      if( !empty($debug))  echo "$SSqlQuery<br>";
                      $res = new Query($SQL_DBLINK, $SSqlQuery);

                      $SSqlQuery = "CREATE TABLE ".$_STATIST['TABLES']['mbSpiders'];
                      $SSqlQuery .= " ( id INT UNSIGNED auto_increment PRIMARY KEY, Date DATE, Time MEDIUMINT UNSIGNED";
					  $SSqlQuery .=", id".$_STATIST['TABLES']['IP']."  ".$_STATIST['auto_type'].")";
                      if( !empty($debug))  echo "$SSqlQuery<br>";
                      $res = new Query($SQL_DBLINK, $SSqlQuery);

                      $SSqlQuery = "CREATE TABLE ".$_STATIST['TABLES']['Files'];
                      $SSqlQuery .= " ( id INT UNSIGNED auto_increment PRIMARY KEY, Date DATE";
					  $SSqlQuery .=", val MEDIUMINT UNSIGNED, Size MEDIUMINT UNSIGNED)";
                      if( !empty($debug))  echo "$SSqlQuery<br>";
                      $res = new Query($SQL_DBLINK, $SSqlQuery);
					
                      STATIST::CreateAutoTables($debug);
					  return true;
              }

             function CreateAutoTables($debug=0)
              {		global $SQL_DBLINK,  $_STATIST;
                      if( empty($SQL_DBLINK) ) die ( "\$SQL_DBLINK not init!!!" );
                      foreach( $_STATIST['TABLES']['auto'] as $v=>$k )
                        {   if( !empty($_STATIST['TABLES'][$v]) ){
		                        $SSqlQuery = "CREATE TABLE ".$_STATIST['TABLES'][$v];
		                        $SSqlQuery .= " ( id".$_STATIST['TABLES'][$v]." ".$_STATIST['auto_type']." auto_increment primary key";
		                        $SSqlQuery .= ",  ".$_STATIST['TABLES'][$v]." TINYTEXT  )";
                                if( !empty($debug))  echo "$SSqlQuery<br>";
                                $res = new Query($SQL_DBLINK, $SSqlQuery);
                            }
                        }
                      return true;
              }

              function DeleteAutoTables($debug=0)
              {       global $SQL_DBLINK, $_STATIST;
                      if( empty( $SQL_DBLINK )) die ( "$SQL_DBLINK not init!!!" );

                      foreach( $_STATIST['TABLES']['auto'] as $v=>$k )
                        {   if( !empty($_STATIST['TABLES'][$v]) ){
                                $SSqlQuery = "DROP TABLE ".$_STATIST['TABLES'][$v];
                                if( !empty($debug))  echo "$SSqlQuery<br>";
                                $res = new Query($SQL_DBLINK, $SSqlQuery);
                            }
                        }
                      return true;  
              }

//////////////�������� ������/////////
            function DeleteTables($debug=0)
              {       global $SQL_DBLINK, $_STATIST;
                      if( empty( $SQL_DBLINK )) die ( "\$SQL_DBLINK not init!!!" );
                      foreach( $_STATIST['TABLES'] as $k=>$v )
                      if( !is_array( $v ) )
                        {       $SSqlQuery = "DROP TABLE ".$v;
                        		if( !empty( $debug )) echo "$SSqlQuery<br>";
                        		$res = new Query($SQL_DBLINK, $SSqlQuery);
                        }
                      STATIST::DeleteUsersIDTable( @$debug );
              }
//////////////����� �����/////////////
            function ResetTables($debug=0)
              {		global $SQL_DBLINK,  $_STATIST;
              		foreach( $_STATIST['TABLES'] as $k=>$v )
              		{	
                        if( !empty($debug))  echo "TRUNCATE TABLE ".$v."<br>";
                        $res = new Query($SQL_DBLINK,  "TRUNCATE TABLE ".$v);
              		}              		
                    if( !empty($debug))  echo "TRUNCATE TABLE ".$_STATIST['CONST']['users_id']['tab'];
                    $res = new Query($SQL_DBLINK,  "TRUNCATE TABLE ".$_STATIST['CONST']['users_id']['tab']);
              }
//////////////�������� � ����������
            // ��������� �� �  ����

            function ParseStat(  $debug = 0 )
              {       global $_STATIST, $_CORE, $_STATIST_ADD, $is404;
                      STATIST::Servise($debug);
                      
                      if( !empty($_CORE->is404) ){ echo "������"; return true; }
                      
               		  if( is_file( $_CORE->ROOT.$_CORE->DIRMODS.$_STATIST_ADD['Addr'] ))	{ STATIST::AddToFileStat(  $debug ); return true; }
               		  if( is_file( $_CORE->ROOT.$_STATIST_ADD['Addr'] ))					{ STATIST::AddToFileStat(  $debug ); return true; }

                      if(  STATIST::AddStat( @$debug )  ){

		                    if( !STATIST::AddToSearchStat( $debug ) )  
                      		STATIST::AddToFromStat(  $debug );  
		                    STATIST::AddToPagesStat( $debug ); 
                      }
              }

			function GetNumber( $table, $what, $debug = 0 )
			{
                                if( !($id = SQL::getval( "id".$table, $table, $table." = '$what'",'')) )
                                  {
                                          SQL::ins( $table, $table, "'$what'", @$debug );
                                          $id = SQL::getval( "LAST_INSERT_ID()", $table );
                                  }
                                return $id;  
				
			}

			function AddStat( $debug=0 )
              {       global $_STATIST, $_STATIST_ADD;
              			$id_ip = STATIST::GetNumber($_STATIST['TABLES']['IP'],$_STATIST_ADD['IP'], $debug);
              		
		              if( SQL::getval( "id".$_STATIST['TABLES']['IP'], $_STATIST['TABLES']['Spiders'], "id".$_STATIST['TABLES']['IP']." = '".$id_ip."'",'', $debug)!='' ) return false;
              
					  $what = "'".$_STATIST_ADD['Date']."','".$_STATIST_ADD['Time']."','".$_STATIST_ADD['Size']."','".$_STATIST_ADD['User']."'";
					  $where = 'Date, Time, Size, User';
					  
					  $where .= ",id".$_STATIST['TABLES']['Addr'];
					  $what .= ",'".STATIST::GetNumber($_STATIST['TABLES']['Addr'],$_STATIST_ADD['Addr'], $debug)."'";
					  $where .= ',id'.$_STATIST['TABLES']['Ref'];
					  $what .= ",'".STATIST::GetNumber($_STATIST['TABLES']['Ref'],$_STATIST_ADD['Ref'], $debug)."'";
					  $where .= ',id'.$_STATIST['TABLES']['Title'];
					  $what .= ",'".STATIST::GetNumber($_STATIST['TABLES']['Title'],$_STATIST_ADD['Title'], $debug)."'";
					  $where .= ',id'.$_STATIST['TABLES']['IP'];
					  $what .= ",'".$id_ip."'";
                      SQL::ins( $_STATIST['TABLES']['All'], $where, $what, @$debug);
                      return true;
              }
              
            function AddToFileStat( $debug=0 )
            {            
   					  global $_STATIST, $_STATIST_ADD;
   					  $id = STATIST::GetNumber($_STATIST['TABLES']['IP'],$_STATIST_ADD['IP'], $debug);
   					  if( !SQL::getval( "id".$_STATIST['TABLES']['IP'], $_STATIST['TABLES']['mbSpiders'], "id".$_STATIST['TABLES']['IP']." = '$id'",'', $debug) )
   					  		SQL::ins( $_STATIST['TABLES']['mbSpiders'], "Date, Time, id".$_STATIST['TABLES']['IP'], "'".$_STATIST_ADD['Date']."','".$_STATIST_ADD['Time']."','".$id."'", $debug );
   					  SQL::del( $_STATIST['TABLES']['Spiders'], "id".$_STATIST['TABLES']['IP']." = '".$id."'", 0 );

					  if(  SQL::getval( "val", $_STATIST['TABLES']['Files'], "Date = '".$_STATIST_ADD['Date']."'",'', $debug) ) 	
					  {		$set = "val=(val+1),Size=(Size+".$_STATIST_ADD['Size'].")";
   					  		SQL::upd( $_STATIST['TABLES']['Files'], $set, "Date = '".$_STATIST_ADD['Date']."'", $debug ); 
					  }
					  else 
					  {		
							$where = 'Date, val, Size';
							$what = "'".$_STATIST_ADD['Date']."', '1','".$_STATIST_ADD['Time']."'";
							SQL::ins( $_STATIST['TABLES']['Files'], $where, $what, $debug );
					  }
            	
            }
            

			/* Added form http://sarry.com.ru/2005/09/08/show-search-words/ */
			function searchwords($ss_string, $s_word, $s_holdr, $s_machine) 
			{		global $reffer, $searchmach, $searchword;
			if (strpos($reffer, $ss_string)) 
				{	$reffer_parsed=parse_url($reffer);
					parse_str($reffer_parsed['query'], $parsed_params);
					$searchword=$parsed_params[$s_word];
					$searchmach=$s_machine;
					$reffer = '';
				}
			}
			/**
			 * ������������� �� UTF-8 � win-1251
			 *
			 * @param string $s
			 * @return string
			 */
			function utf2win($s) {
			        $tgt = '';
			        for($i=0,$len=strlen($s); $i<$len; $i++) {
			                $c = $s[$i];
			                $x = ord($c);
			                if ($x < 0x80) {
			                        $tgt .= $c;
			                        continue;
			                } elseif (($x & 0xC0) == 0xC0) {
			                        $n = 1;
			                        while (($x & (0x40 >> $n)) > 0) $n++;
			                        $code = $x & (0x3F >> $n);
			                        for ($k=1; $k<=$n; $k++) {
			                                $y = ord($s[$i+$k])        & 0x3F;
			                                $code = ($code << 6) + $y;
			                        }
			                        $i += $n;
			                        $tgt .= chr($code - 1104);
			                } else {
			                        $tgt .= '?';
			                }
			        }
			        return $tgt;
			}

            function AddToSearchStat( $debug=0 )
            {
   					 global $_STATIST, $_STATIST_ADD;
					GLOBAL $reffer, $searchmach, $searchword;
					$reffer=strtolower($_STATIST_ADD['Ref']);
					$reffer=urldecode($reffer);

					STATIST::searchwords('yandex.ru/yandsearch?', 'text', 'holdreq', 'www.yandex.ru');
					STATIST::searchwords('go.mail.ru/search', 'q', 'bbbbbb', 'mail.ru');
					STATIST::searchwords('search.msn.com/', 'q', 'bbbbbb', 'search.msn.com');
					STATIST::searchwords('search.yahoo.com/', 'p', 'bbbbbb', 'search.yahoo.com');
					STATIST::searchwords('rambler.ru/', 'words', 'holdreq', 'www.rambler.ru');
					STATIST::searchwords('ie5.rambler.ru/cgi-bin/query_ie5?', 'words', 'holdreq', 'www.rambler.ru');
					STATIST::searchwords('sm.aport.ru/scripts/template.dll?', 'r', 'holdreq', 'www.aport.ru');
					STATIST::searchwords('altavista.com', 'q', 'holdreq', 'altavista.com');
					STATIST::searchwords('google.ru/custom?', 'q', 'bbbbbb', 'www.google.ru');
					STATIST::searchwords('www.google.', 'q', 'bbbbbb', 'www.google.com');
					
//					if (strpos($reffer, 'yandex.ru/yandpage?')){
//						$reffer=urldecode($reffer);
//						STATIST::searchwords('yandex.ru/yandpage?', 'qs', 'holdreq', 'www.yandex.ru');
//						$searchword = substr( convert_cyr_string(utf8_decode($searchword),'i','w'), 5 );
//					} 
					
					if( empty( $searchword ) )return false;
					$_STATIST_ADD['Search'] = $searchmach;$_STATIST_ADD['Words'] = $searchword;
					
					if( $searchmach == 'www.google.com') 		$_STATIST_ADD['Words'] = STATIST::utf2win($searchword);
					if( $searchmach == 'altavista.com') 		$_STATIST_ADD['Words'] = STATIST::utf2win($searchword);
					if( $searchmach == 'search.msn.com') 		$_STATIST_ADD['Words'] = STATIST::utf2win($searchword);
					if( $searchmach == 'search.yahoo.com') 		$_STATIST_ADD['Words'] = STATIST::utf2win($searchword);						
					if( $searchmach == 'www.yandex.ru') 		$_STATIST_ADD['Words'] = STATIST::utf2win($searchword);						
					
					$where = 'Date, Time';
					  $what = "'".$_STATIST_ADD['Date']."','".$_STATIST_ADD['Time']."'";
					  $where .= ',id'.$_STATIST['TABLES']['Addr'];
					  $what .= ",'".STATIST::GetNumber($_STATIST['TABLES']['Addr'],$_STATIST_ADD['Addr'], $debug)."'";
					  $where .= ',id'.$_STATIST['TABLES']['Words'];
					  $what .= ",'".STATIST::GetNumber($_STATIST['TABLES']['Words'],$_STATIST_ADD['Words'], $debug)."'";
					  $where .= ',id'.$_STATIST['TABLES']['Search'];
					  $what .= ",'".STATIST::GetNumber($_STATIST['TABLES']['Search'],$_STATIST_ADD['Search'], $debug)."'";
					  $where .= ',id'.$_STATIST['TABLES']['Title'];
					  $what .= ",'".STATIST::GetNumber($_STATIST['TABLES']['Title'],$_STATIST_ADD['Title'], $debug)."'";
                      SQL::ins( $_STATIST['TABLES']['Need'], $where, $what, @$debug);
                      return true;         	
            }
            
            function AddToFromStat( $debug=0 )
            {
   					  global $_STATIST, $_STATIST_ADD;
   					  $ref = substr( $_STATIST_ADD['Ref'], 0, strpos( $_STATIST_ADD['Ref'].'?', '?' ));
   					  if( !strpos( $ref, $_SERVER['SERVER_NAME']) ){
   					  
	   					  $id = STATIST::GetNumber($_STATIST['TABLES']['Ref'],$_STATIST_ADD['Ref'], $debug);
	   					  $id_addr = STATIST::GetNumber($_STATIST['TABLES']['Addr'],$_STATIST_ADD['Addr'], $debug);
	   					  
						  if(   SQL::getval( "id".$_STATIST['TABLES']['Ref'], $_STATIST['TABLES']['From'], "id".$_STATIST['TABLES']['Ref']." = '$id'",'', $debug) )	
						  {		$set = "Date='".$_STATIST_ADD['Date']."',Time='".$_STATIST_ADD['Time']."',val=(val+1),id".$_STATIST['TABLES']['Addr']." = '$id_addr'";
	   					  		SQL::upd( $_STATIST['TABLES']['From'], $set, "id".$_STATIST['TABLES']['Ref']." = '$id'", $debug ); 
						  }
						  else 
						  {		
						  $where = 'Date, Time, val';
						  $what = "'".$_STATIST_ADD['Date']."','".$_STATIST_ADD['Time']."', '1'";
						  $where .= ',id'.$_STATIST['TABLES']['Ref'];
						  $what .= ",'".$id."'";
						  $where .= ',id'.$_STATIST['TABLES']['Addr'];
						  $what .= ",'".$id_addr."'";
						  		SQL::ins( $_STATIST['TABLES']['From'], $where, $what, $debug );
					  }}
                      return true;         	
            }
            
            function AddToPagesStat( $debug=0 )
            {
   					  global $_STATIST, $_STATIST_ADD;
   					  $id = STATIST::GetNumber($_STATIST['TABLES']['Addr'],$_STATIST_ADD['Addr'], $debug);
   					  $id_ref = STATIST::GetNumber($_STATIST['TABLES']['Ref'],$_STATIST_ADD['Ref'], $debug);
   					  
					  if(   SQL::getval( "id".$_STATIST['TABLES']['Addr'], $_STATIST['TABLES']['Pages'], "id".$_STATIST['TABLES']['Addr']." = '$id'",'', $debug) )	
					  {		$set = "Date='".$_STATIST_ADD['Date']."',Time='".$_STATIST_ADD['Time']."',val=(val+1),id".$_STATIST['TABLES']['Ref']." = '$id_ref'";
   					  		SQL::upd( $_STATIST['TABLES']['Pages'], $set, "id".$_STATIST['TABLES']['Addr']." = '$id'", $debug ); 
					  }
					  else 
					  {		
					  $where = 'Date, Time, val';
					  $what = "'".$_STATIST_ADD['Date']."','".$_STATIST_ADD['Time']."', '1'";
					  $where .= ',id'.$_STATIST['TABLES']['Addr'];
					  $what .= ",'".$id."'";
					  $where .= ',id'.$_STATIST['TABLES']['Ref'];
					  $what .= ",'".$id_ref."'";
					  		SQL::ins( $_STATIST['TABLES']['Pages'], $where, $what, $debug );
					  }
                      return true;         	
            }
            
            function AddToGroupStat( $Date, $debug=0 )
            {
   					 global $_STATIST;
					  $where = 'Date';
					  $what = "'".$Date."'";
					  $p = SQL::getval( 'count(*)', $_STATIST['TABLES']['All'], "( Date = '$Date' )",'',@$debug);
					  $where .= ',Allof';
					  $what .= ",'".$p."'";
					  $p = SQL::getval( 'count(DISTINCT id'.$_STATIST['TABLES']['IP'].')', $_STATIST['TABLES']['All'], "( Date = '$Date' )",'',@$debug);
					  $where .= ',AllIP';
					  $what .= ",'".$p."'";
					  $p = SQL::getval( 'count(DISTINCT User)', $_STATIST['TABLES']['All'], "( Date = '$Date' )",'',@$debug);
					  $where .= ',AllUser';
					  $what .= ",'".$p."'";
					  $p = SQL::getval( 'count(*)', $_STATIST['TABLES']['Need'], "( Date = '$Date' )",'',@$debug);
					  $where .= ',AllSearch';
					  $what .= ",'".$p."'";
					  $p = SQL::getval( 'sum(Size)', $_STATIST['TABLES']['All'], "( Date = '$Date' )",'',@$debug);
					  $p += SQL::getval( 'sum(Size)', $_STATIST['TABLES']['Files'], "( Date = '$Date' )",'',@$debug );
					  $where .= ',AllSize';
					  $what .= ",'".$p."'";
            		  SQL::ins( $_STATIST['TABLES']['Group'], $where, $what, @$debug);
            		  
            		  SQL::del( $_STATIST['TABLES']['All'], "Date='".$Date."'", $debug );
            		  
            }
            
            
            function Servise( $debug = 0 )
            {
            	global $_STATIST, $_STATIST_ADD, $SQL_DBLINK;
            	$res = SQL::sel("lastGroup, lastSpidersTime, lastSeedIP", $_STATIST['CONST']['users_id']['tab'], '', '', $debug);
            	if( $res->NumRows==0 )
            		{	SQL::ins( $_STATIST['CONST']['users_id']['tab'], "lastGroup, lastSpidersTime, lastSeedIP, val", "'0000-00-00','0', '0', '0'", $debug );
            			$res = SQL::sel("lastGroup, lastSpidersTime, lastSeedIP", $_STATIST['CONST']['users_id']['tab'], '', '', $debug);
            		}
            	$res->FetchArray(0);
            	$lastGroup	= $res->FetchArray['lastGroup'];
            	$lastTime	= $res->FetchArray['lastSpidersTime'];
            	$lastIP		= $res->FetchArray['lastSeedIP'];
            	$countIP	= SQL::getval( "count(*)", $_STATIST['TABLES']['IP'], '', '', $debug )+1;
            	
            	$today = strtotime( $_STATIST_ADD['Date'] )-$_STATIST['CONST']['ps']*86400;
            	if( $today > $lastGroup )
	            	{
	            		STATIST::AddToGroupStat( date('Y-m-d',$today), $debug );
		            	SQL::upd( $_STATIST['CONST']['users_id']['tab'], "lastGroup='".$today."'",  '', $debug);            	
	            	}            		
            	if( (($_STATIST_ADD['Time']-$lastTime)>600)||(($lastTime-$_STATIST_ADD['Time'])>600) )
	            	{
				            for( $i=$lastIP+1; $i<$countIP; $i++)
								if( !SQL::getval( "id".$_STATIST['TABLES']['IP'], $_STATIST['TABLES']['mbSpiders'], "id".$_STATIST['TABLES']['IP']." = '$i'",'', $debug) )
								{					
										SQL::ins( $_STATIST['TABLES']['Spiders'], "id".$_STATIST['TABLES']['IP'], $i, $debug );
										SQL::del( $_STATIST['TABLES']['All'], "id".$_STATIST['TABLES']['IP']."='".$i."'", $debug );										
								}
							$countIP--;	
			            	SQL::upd( $_STATIST['CONST']['users_id']['tab'], "lastSpidersTime='".$_STATIST_ADD['Time']."',lastSeedIP='".$countIP."'",  '', $debug);            	
	            	}
				
            }
////////////////////////////  ������ � ��������� ID
            function GetNewUsersID( $debug =0 )
              {       global $_STATIST;
                      $val = SQL::getval("val", $_STATIST['CONST']['users_id']['tab'],'','',@$debug);
                      $nextval = $val+1;
                      SQL::upd( $_STATIST['CONST']['users_id']['tab'], "val='$nextval'",@$debug);
                      return @$nextval;
              }

              ///////////////////////////////////////////////////////
            function whr($L_Date=0, $R_Date=0, $L_TIME=0, $R_TIME=0)
              {       $where='';
                      if( $L_TIME > 0 ) $where .= "AND( '$L_TIME' <= Time )";
                      if( $R_TIME > 0 ) $where .= "AND( Time <= '$R_TIME' )";
                      if( $L_Date > 0 ) $where .= "AND( '$L_Date' <= Date )";
                      if( $R_Date > 0 ) $where .= "AND( Date <= '$R_Date' )";
                      return $where;
              }
              
			function plisting( $curr, $count, $all, $long, $link, $type=0 )
			{	$currpos = $curr/$count+1;
				if( $all<$count )return '';
				$maxpos = ($all%$count>0)?$all/$count+1:$all/$count;
				$pline='';
				for( $i=$currpos-$long; $i<$currpos+$long+1; $i++ )
					if(( $i>0 )&&( $i<=$maxpos )){
						if( $i == $currpos )
						{
							$pline .="<span>".$i."</span>";
						}
						else
						{	if( $type == 0 )
								{
									if( strpos( $link, "?" )>0 ) $newlink = $link."&first=".($i-1)*$count."&count=".$count;
									else $newlink = $link."?first=".($i-1)*$count."&count=".$count;
								}
							$pline .= '<a href="'.$newlink.'" target="_self" title='.$i.'>'.$i.'</a>';
						}
					if(( $i!=$maxpos )&&( $i!=$currpos+$long )) $pline.='&nbsp|&nbsp';
					}
				if( $currpos-$long-1 > 0 )
					{	if( $type == 0 )
								{
									if( strpos( $link, "?" )>0 ) $newlink = $link."&first=".($currpos-$long-2)*$count."&count=".$count;
									else $newlink = $link."?first=".($currpos-$long-2)*$count."&count=".$count;
								}
						$pline = '<a href="'.$newlink.'" target="_self" title="� ������">...</a>&nbsp|&nbsp'.$pline;
					}
				if( $currpos+$long < $maxpos )
					{	if( $type == 0 )
								{
									if( strpos( $link, "?" )>0 ) $newlink = $link."&first=".($currpos+$long)*$count."&count=".$count;
									else $newlink = $link."?first=".($currpos+$long)*$count."&count=".$count;
								}
						$pline .= '&nbsp|&nbsp<a href="'.$newlink.'" target="_self" title="� �����">...</a>';
					}
				return $pline;
			}
			
			function GetDataByDate( $Date='', $debug=0)
			{	$data['All'] = $data['AllUser'] = $data['AllIP'] = $data['AllSearch'] = $data['AllSize'] = 0;//
				global $_STATIST;
				if( $Date != '' ){
					$res = SQL::sel( '*', $_STATIST['TABLES']['Group'], " Date='".$Date."' ",'',$debug );
					if( $res->NumRows == 1 )
					{	$res->FetchArray(0);
						$data['All'] 		= $res->FetchArray['Allof'];
						$data['AllUser'] 	= $res->FetchArray['AllUser'];
						$data['AllSearch']	= $res->FetchArray['AllSearch'];
						$data['AllIP'] 		= $res->FetchArray['AllIP'];
						$data['AllSize'] 	= $res->FetchArray['AllSize'];
					}
					else 
					{
						$data['All'] 		= SQL::getval( 'count(*)', $_STATIST['TABLES']['All'], "( Date = '$Date' )",'',@$debug);
						$data['AllUser'] 	= SQL::getval( 'count(DISTINCT User)', $_STATIST['TABLES']['All'], "( Date = '$Date' )",'',@$debug);
						$data['AllSearch']	= SQL::getval( 'count(*)', $_STATIST['TABLES']['Need'], "( Date = '$Date' )",'',@$debug);
						$data['AllIP'] 		= SQL::getval( 'count(DISTINCT id'.$_STATIST['TABLES']['IP'].')', $_STATIST['TABLES']['All'], "( Date = '$Date' )",'',@$debug);
						$data['AllSize'] 	= SQL::getval( 'sum(Size)', $_STATIST['TABLES']['All'], "( Date = '$Date' )",'',@$debug);
						$data['AllSize']	+=SQL::getval( 'Size', $_STATIST['TABLES']['Files'],"( Date = '$Date' )",'',@$debug );
					}
					return $data;
				}
				else
				{
					$data['All'] 		= SQL::getval( 'count(*)', $_STATIST['TABLES']['All'], "",'',@$debug);
					$data['All'] 		+=SQL::getval( 'sum(Allof)', $_STATIST['TABLES']['Group'], "",'',@$debug);
					$data['AllUser'] 	= SQL::getval( 'count(DISTINCT User)', $_STATIST['TABLES']['All'], "",'',@$debug);
					$data['AllUser'] 	+=SQL::getval( 'sum(AllUser)', $_STATIST['TABLES']['Group'], "",'',@$debug);
					$data['AllSearch'] 	= SQL::getval( 'count(*)', $_STATIST['TABLES']['Need'], "",'',@$debug);
					$data['AllSearch'] 	+=SQL::getval( 'sum(AllSearch)', $_STATIST['TABLES']['Group'], "",'',@$debug);
					$data['AllIP']	 	= SQL::getval( 'count(DISTINCT id'.$_STATIST['TABLES']['IP'].')', $_STATIST['TABLES']['All'], "",'',@$debug);
					$data['AllIP']	 	+=SQL::getval( 'sum(AllIP)', $_STATIST['TABLES']['Group'], "",'',@$debug);
					$data['AllSize']	= SQL::getval( 'sum(Size)', $_STATIST['TABLES']['Files'], "",'',@$debug);
					$data['AllSize']	+=SQL::getval( 'sum(AllSize)', $_STATIST['TABLES']['Group'], "",'',@$debug);
					$data['AllSize'] 	+=SQL::getval( 'sum(Size)', $_STATIST['TABLES']['All'], '','',@$debug);
					return $data;					
				}
			}
			function date2word( $d )
			{	global $_STATIST;
				$tmp = substr($d, -2)." ".$_STATIST['MONTH'][substr($d, 5,2)];
				return ($tmp[0]=='0')?substr( $tmp, 1):$tmp;
			}
			function listday( $curday,$max, $today, $path )
			{	$res = "";
				$del = strpos( $path, '?' )?'&':'?';
				if( $curday>0 )
					$res .= "<a href=".$path.$del."curday=".($curday-1)."><-".STATIST::date2word(date( 'Y-m-d',$today + 86400))."</a>";
				$res .= "&nbsp&nbsp&nbsp&nbsp ".STATIST::date2word(date( 'Y-m-d',$today))."&nbsp&nbsp&nbsp&nbsp";
				if( $curday<$max )
					$res .= "<a href=".$path.$del."curday=".($curday+1).">".STATIST::date2word(date( 'Y-m-d',$today - 86400))."-></a>";
				return $res;	
			}			
}//endof STATIST
?>