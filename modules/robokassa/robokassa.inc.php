<?php
/**
 * ������� �������� �����
 *
*/
	
	
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! $user_tranzak - �� ��������� !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
	
	
global $_CORE;
$_ROBOKASSA['TITLE']	    	= ($_CORE->LANG == 'en' )?'Robokassa - pay system' : 'Robokassa - ������� ������';
$_ROBOKASSA['MODULE_TITLE']		= '����������';
$_ROBOKASSA['MODULE_DESC']		= '������ ����� ��������.';
$_ROBOKASSA['ERROR']	   	 	= '';
$_ROBOKASSA['MODULE_NAME']		= 'robokassa';
$_ROBOKASSA['HISTORY_FILE']		= $_CORE->SiteModDir.'/data/robo_history.txt';
$_ROBOKASSA['HISTORY_LEN']		= 3;

/**
 * ������� �������� ������
 *
 * ������� �������� ������ (���� ����)...
 */	

class ROBOKASSA {

	function comm_inc($tpl, &$file){ /* ��� ������� ���� ����������� �� ������ DOCTXT */
		global $_CORE, $_ROBOKASSA;
		$files	=  array( 
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_ROBOKASSA['MODULE_NAME'].'/'.$tpl,
			$_CORE->SiteModDir.'/'.$tpl,
			$_CORE->CORE_PATHTPLS.'/'.$_ROBOKASSA['MODULE_NAME'].'/_'.$tpl,
			$_CORE->CoreModDir.'/_'.$tpl,
		);
 		$tmp = FILE::chk_files($files, $file); 
		return $tmp;
	}
	
	
	function result_url($type = 'result'){ 																	/* ������ ������������� �������� ����� ����, ���������� �� ������ (ResultURL) */
		
		global $_CORE, $_ROBOKASSA; 														/* ���������� ���������� , ���������� ����� �������� */
		$mrh_pass1 	= $_ROBOKASSA['pass_1']; 												/* ������ ��� ����������� � ������ �������� ��������� */
		$mrh_pass2 	= $_ROBOKASSA['pass_2']; 												/* ������ ��� ����������� � ������ �������� ��������� */
		$tm 		= getdate(time()+9*3600);												/* ���� */
		$date 		= "$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]"; 	/* ���� 2 */
		$out_summ 	= $_REQUEST["OutSum"]; 													/* ����� ������ */
		$inv_id 	= $_REQUEST["InvId"]; 													/* ���������� ����� �������� */
		$shp_item 	= $_REQUEST["Shp_item"]; 												/* ��� ������ */
		$crc 		= strtoupper($_REQUEST["SignatureValue"]); 								/* ��� ��� ��������� */
		$rem_addr 	= $_ROBOKASSA['REMOTE_ADDR']; 											/* ip user */
		$my_crc 	= ($type == 'result')
								?strtoupper(md5("$out_summ:$inv_id:$mrh_pass2:Shp_item=$shp_item")) 	/* ������������ ���� */
								:strtoupper(md5("$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item")); 	/* ��� success */
		$in_curr	= 0; /* �� ���������� ������, ������� �� ������������ */
		
		// ��������� ��� �� success (��� result) ��� ������������� (in_curr)
		if ($val = SQL::getval('inv_id', $_ROBOKASSA['robo_core_table'], "inv_id = '$inv_id' and success_result = '1' and type = '2'", '', DEBUG)) {
			if (Main::comm_inc('robokassa.already.html', $file, 'robokassa')){
				include_once "$file";
			}		
			return;
		}

		if ($my_crc !=$crc){ 																/* ���� �� ������� */
			$_ROBOKASSA['result'] = 0;
			$title = "Result Inc is bad\n";
			$template = 'robokassa.success_bad.item.html'; // ���� ����� success
			/* ����� ��� ��������� */
//		elseif($in_curr == 1 ){ 																				/* ���� ������� �� ������  */
//			$_ROBOKASSA['result'] = 2;
//			$title = "Already done $inv_id\n";															/* ����� ��� ��������� */
//			$template = 'robokassa.success_yes.item.html';
		}else{
			$_ROBOKASSA['result'] = 1;
			$title = "OK$inv_id";															/* ����� ��� ��������� */
			$template = 'robokassa.success_yes.item.html';
		}
		

		$success = ($type=='result') ? 3 : 5;
			$res = SQL::ins($_ROBOKASSA['robo_core_table'], 
						"time, 		ip, 		type, summa,      success_result, shp_item, in_curr, culture, inv_id, inv_desc, crc1, crc2, user_tranzak", 
						"'".time()."', '$rem_addr', '".$_ROBOKASSA['result']."', '$out_summ', '$success', '$shp_item', '$in_curr', '$culture', '$inv_id', '$inv_desc', '$crc', '$my_crc', '$type check robokassa'",DEBUG); /* SQL ������ */
		if (is_file( $_CORE->SiteModDir.'/robokassa.'.$type.'_action.php')) {
			include_once($_CORE->SiteModDir.'/robokassa.'.$type.'_action.php'); 	
			/* ���������� ���� ��������� �� ����� �����, ���� ��������� success �� ������ in_curr - ������ ��������� ������ */
		}
		// if it's check of robokassa (result) - then just exit, if 'success' url asked then show the answer
		if ($type == 'result') {
			ob_end_clean();
			die($title);

		}else{

			if (Main::comm_inc($template, $file, 'robokassa')){
				include_once "$file";
			}
		}

		/*
		OutSum			- ���������� �����. ����� ����� �������� � ��� ������, ������� ���� ������� ��� ����������� ��������. ������ ������������� ����� - ����������� �����.
		InvId			- ����� ����� � ��������
		SignatureValue	- ����������� ����� MD5 - ������ �������������� ����� 32-��������� ����� � 16-������ ����� � ����� �������� (����� 32 ������� 0-9, A-F). ����������� �� ������, ���������� ��������� ���������, ����������� ':', � ����������� sMerchantPass2 - (��������������� ����� ��������� �����������������) �.�. nOutSum:nInvId:sMerchantPass2[:���������������� ���������, � ��������������� �������]
						  � ������� ���� ��� ������������� �������� ���� �������� ���������������� ��������� shpb=xxx � shpa=yyy �� ������� ����������� �� ������ ...:sMerchantPass2:shpa=yyy:shpb=xxx 
		*/
	}
	
	
		/*
		OutSum			- ���������� �����. ����� ����� �������� � ��� ������, ������� ���� ������� ��� ����������� ��������. ������ ������������� ����� - ����������� �����.
		InvId				- ����� ����� � ��������
		SignatureValue	- ����������� ����� MD5 - ������ �������������� ����� 32-��������� ����� � 16-������ ����� � ����� �������� (����� 32 ������� 0-9, A-F). ����������� �� ������, ���������� ��������� ���������, ����������� ':', � ����������� sMerchantPass1 (����������� ��� �����������) �.�. nOutSum:nInvId:sMerchantPass1[:���������������� ���������, � ��������������� �������]. 
						  � ������� ���� ��� ������������� �������� ���� �������� ���������������� ��������� shpb=xxx � shpa=yyy �� ������� ����������� �� ������ ...:sMerchantPass1:shpa=yyy:shpb=xxx
		Culture 		- ���� ������� � ��������, ��������� ��� ������������� ������. ��������: en, ru. 
		*/
//	}
	
	function fail_url(){ 																	/* ������������� ������������ ��� ������ �� ������ (FailURL) */
		global $_CORE, $_ROBOKASSA; 														/* ���������� ���������� , ���������� ����� �������� */
		$mrh_pass1 	= $_ROBOKASSA['pass_1']; 												/* ������ ���� ����������� � ������ �������� ��������� */
		$out_summ 	= $_REQUEST["OutSum"]; 													/* ����� ������ */
		$inv_id 	= $_REQUEST["InvId"]; 													/* ���������� ����� �������� */
		$shp_item 	= $_REQUEST["Shp_item"]; 												/* ��� ������ */
		$crc 		= strtoupper($_REQUEST["SignatureValue"]); 								/* ��� ��� ��������� */
		$rem_addr 	= $_ROBOKASSA['REMOTE_ADDR']; 											/* ip user */
		$my_crc 	= strtoupper(md5("$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item")); 	/* ��� ��� ��������� */

		if ( Main::comm_inc('robokassa.fail_action.php', $file, 'robokassa')) { /* ���������� ���� �� ����-����� ��� ��������� ���������� �������� */
			include_once "$file"; 
		}

		$res = SQL::ins($_ROBOKASSA['robo_core_table'], 
						"time, 		ip, 		type, summa,      success_result, shp_item, in_curr, culture, inv_id, inv_desc, crc1, crc2, user_tranzak", 
						"'".time()."', '$rem_addr', '0', '$out_summ', 'fail', '$shp_item', '$in_curr', '$culture', '$inv_id', '$inv_desc', '$crc', '$my_crc', 'fail url robokassa asked'",DEBUG); /* SQL ������ */

		if (Main::comm_inc('robokassa.fail.item.html', $file, 'robokassa')){
			include_once "$file";
		}
		/*
		OutSum				- ���������� �����. ����� ����� �������� � ��� ������, ������� ���� ������� ��� ����������� ��������. ������ ������������� ����� - ����������� �����.
		InvId 				- ����� ����� � ��������
		SignatureValue  	- ����������� ����� MD5 - ������ �������������� ����� 32-��������� ����� � 16-������ ����� � ����� �������� (����� 32 ������� 0-9, A-F). ����������� �� ������, ���������� ��������� ���������, ����������� ':', � ����������� sMerchantPass1 (����������� ��� �����������) �.�. nOutSum:nInvId:sMerchantPass1[:���������������� ���������, � ��������������� �������].
							� ������� ���� ��� ������������� �������� ���� �������� ���������������� ��������� shpb=xxx � shpa=yyy �� ������� ����������� �� ������ ...:sMerchantPass1:shpa=yyy:shpb=xxx
		sCulture 			- ���� ������� � ��������, ��������� ��� ������������� ������. ��������: en, ru. 
		
		*/

	}
}

?>