<?
$_SEARCH['TITLE'] 		= '����� �� �����';
$_SEARCH['TITLE_EN'] 	= 'Search';
$_SEARCH['PATH'] 		= '/'.$_SEARCH['MODULE_NAME'].'/';
$_SEARCH['CntLen']		= 150;
$_SEARCH['ONPAGE']		= 10;

class SEARCH {
	function search_doctxt($cmd, $search_in){
		
		global $_DOC, $_SEARCH, $_PROJECT;
		
		$mod = $_DOC['MODULE_NAME'];
		
		$_DOC['SEARCH_SUBTREE'] = "/$cmd";
		$_DOC['ERROR_NOSHOW'] = 'true';
//					echo "<h2>".cmd("/$mod/field/$cmd/title")."</h2>";
		// ����
		$res = cmd("/$mod/".$_SEARCH['MODULE_NAME']);
		
		if (sizeof($_DOC['SEARCH_RES']) > 1){
			unset($_DOC['SEARCH_RES'][0]); // ��� ������ ������
			
			// ���� ����� ��������� �� ��������
			if ($search_in == '' || $search_in == $cmd){
//							echo $res;
				$_SEARCH['RESULT'][$cmd]['eval']	=  'include DOCTXT::inc_tpl("search.item.html", $f);';
				$_SEARCH['RESULT'][$cmd]['arr']	=  'data';
				$_SEARCH['RESULT'][$cmd]['data']	=  $_DOC['SEARCH_RES'];
			}
				
			// ���������� � �������
			$_SEARCH['RESULT'][$cmd]['count']	=  sizeof($_DOC['SEARCH_RES']);
			
			// ��������� ����� 
			$_SEARCH['RESULT_ALL']['count']		+=  $_SEARCH['RESULT'][$cmd]['count'];
			
			// ���� ���-�� �����, ����� ���
			if ($_SEARCH['RESULT'][$cmd]['count'] > 0 )
			$_PROJECT['SEEALSO'] 	.= ($search_in != $cmd) 
									? "<li><a href='/".$_SEARCH['MODULE_NAME']."/go/$cmd?go=".$_REQUEST['go']."'>".cmd("/$mod/field/$cmd/title")."</a>&nbsp;".$_SEARCH['RESULT'][$cmd]['count']."</li>"
									: "<li>".cmd("/$mod/field/$cmd/title")."&nbsp;".$_SEARCH['RESULT'][$cmd]['count']."</li>";
		}		
	}
	
	function search_db($cmd, $search_in){

		global $_KAT, $_SEARCH, $_PROJECT;
		
		$mod = 'db';
		$p = $_REQUEST['p']; 
		unset($_REQUEST['p']); //����� �� ����������� �� �������.
		
		$_REQUEST['form']['cont'] 	= $_REQUEST['go'];
		$_REQUEST['form']['anons'] 	= $_REQUEST['go'];
		$_REQUEST['form']['name'] 	= $_REQUEST['go'];
		$_KAT['searching'] = true;
//		echo "<li>:$mod:$cmd.";
		$res = cmd("/$mod/$cmd");
		$_KAT['searching'] = false;
		
		// ���� ���� � ���� ������� ��� ������, ����� ����
		if ($search_in == '' || $search_in == $cmd){
//			echo $res;
			$_SEARCH['RESULT'][$cmd]['cmd']		=  "/$mod/$cmd";
			$_SEARCH['RESULT'][$cmd]['eval']	=  '$_KAT["KUR_ALIAS"]="'.$_KAT["KUR_ALIAS"].'";include KAT::inc_tpl("search.item.html", $f);';
			$_SEARCH['RESULT'][$cmd]['arr']		=  'data';
			$_SEARCH['RESULT'][$cmd]['data']	=  $_KAT['SEARCH_RES'];
		}					
		
		// ����� �����, �� � ������
		if (is_array($_SEARCH['RESULT'][$cmd]['data'])) foreach ($_SEARCH['RESULT'][$cmd]['data'] as $k => $v) {
			foreach (array('cont', 'anons', 'name') as $field) {
				if (false !== ($str = SEARCH::_scan($v[$field], $_REQUEST['go']))){
					$_SEARCH['RESULT'][$cmd]['data'][$k]['text'] = $str;
					break;
				}
			}
			$_SEARCH['RESULT'][$cmd]['data'][$k]['url'] = "/$mod/".$_KAT["KUR_ALIAS"]."/".$v['alias'];
			$_SEARCH['RESULT'][$cmd]['data'][$k]['title'] = $v['name'];
		}

		// ���������� � �������
		$_SEARCH['RESULT'][$cmd]['count']	=  sizeof($_KAT['SEARCH_RES']);
		
		// ��������� ����� 
		$_SEARCH['RESULT_ALL']['count']		+=  $_SEARCH['RESULT'][$cmd]['count'];
		
		// ���� ���-�� �����, ����� ���
		if ($_SEARCH['RESULT'][$cmd]['count'] > 0 )
		$_PROJECT['SEEALSO'] 	.= ($search_in != $cmd) 
								? "<li><a href='/".$_SEARCH['MODULE_NAME']."/go/$cmd?go=".$_REQUEST['go']."'>".$_KAT['TITLES'][$_KAT['KUR_ALIAS']]."</a>&nbsp;".$_SEARCH['RESULT'][$cmd]['count']."</li>"
								: "<li>".$_KAT['TITLES'][$_KAT['KUR_ALIAS']]."&nbsp;".$_SEARCH['RESULT'][$cmd]['count']."</li>";
		$_REQUEST['p'] = $p;
		
	}
	
	function _scan ( $str, $w )
	{
		global $_SEARCH;
		$lw		= strtolower($w);
		
		$cont_st= @strip_tags($str);
		$cont	= strtolower($cont_st);

		$delta	= $_SEARCH['CntLen'];

		// scan content
		$is		= strpos($cont, $lw);
		if ($is !== false) {
			$pref = ($is > $delta) ? '...' : '';
			$postf = ($is + $delta + strlen($lw) < strlen($cont)) ? '...' : '';

			$from	= ($is - $delta < 0) ? 0 : strpos($cont, ' ', $is - $delta);
			$to		= strpos($cont, ' ', min($is + $delta + strlen($lw), strlen($cont) - $is)) - $is+1;

			$tmp['text']	= preg_replace("/($w)/i",
			"<span class=mark>$1</span>",
			$pref.substr($cont_st, $from, ($to > 0)?$to:strlen($cont)  ).$postf);
		} 

		if (!empty($tmp['text'])) {
//			$tmp['text']	= preg_replace("/($w)/i", "<span class=mark>$1</span>", $d['title']);
			return $tmp['text'];
		}else
			return false;
	}	
	
}
?>