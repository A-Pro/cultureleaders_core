<?
global $SQL_DBCONF, $_CORE, $_TPLS;
if (!$_CORE->dll_load('class.DBCQ'))
	die ($_CORE->error_msg());
$_TPLS['MODULE_NAME']	= 'tpls';
$_TPLS['NO_PLINE']		= 0;
include $_CORE->CoreModDir.'/_conf.inc.php';

if (!defined('TPLS_FILE_NOT_FOUND')) define ("TPLS_FILE_NOT_FOUND","���� �� ������.");
if (!defined('TPLS_IMPORT_WRONG_DELIMETER')) define ("TPLS_IMPORT_WRONG_DELIMETER","�������� ����������� (�� �� ������).");
if (!defined('TPLS_IMPORT_WRONG_SETTINGS')) define ("TPLS_IMPORT_WRONG_SETTINGS","�� �������� �������� �������, ��� ������� ��������.");

class TPLS {

	function get_cont( $alias, $field = 'cont')
	{
		global $_TPLS, $_CORE, $form, $FORM_DATA;
		$form	= TPLS::load( $alias, $field);
		if (is_array($form)) {
			$file	=  $_CORE->SiteModDir.'/'.$_TPLS['KUR_ALIAS'].'.view.item.html';
			if (is_file($file)) {
				include "$file";
			}else {
				if (empty($_TPLS['cForm'])) {
					$_CORE->dll_load('class.cForm');
					$_TPLS['cForm']	= new cForm($FORM_DATA);
				}
				unset($_TPLS['cForm']->Arr['alias']);
				unset($_TPLS['cForm']->Arr['id']);
				unset($_TPLS['cForm']->Arr['anons']);
				echo $_TPLS['cForm']->Print_come();
			}
			return $form;
		}
	}

	/**
	*
	*/
	function load( $alias = '' )
	{
		global $_TPLS, $FORM_SELECT, $FORM_WHERE, $FORM_FROM;
		$add_sel = (!empty($FORM_SELECT)) ? $FORM_SELECT : '';
		// ������������� ��� ������������ ������� (������� � EFES)
		$add_where	= (!empty($FORM_WHERE)) ? $FORM_WHERE : '';

		$data	= array();
		if (empty($alias))
			return false;
		$where	= " alias = '$alias' ";
		$res	= SQL::sel(' * '.$add_sel,  $_TPLS['KUR_TABLE'].$FORM_FROM, $where.$add_where, '', 0);
		if ($res->NumRows > 0) {
			$res->FetchArray(0);
			$data	= $res->FetchArray;
			$res->Destroy();
		}
		/*
		* ��� ������������� ������ 
		*/
		global $doc;
		global $big_doc;
		$doc	= @$data['doc'];
		$big_doc	= @$data['big_doc'];
		for ($i=0;$i<10;$i++) {
			global ${'doc'.$i},${'big_doc'.$i};
			${'doc'.$i}	= @$data['doc'.$i];
			${'big_doc'.$i}	= @$data['big_doc'.$i];
		}

		/*
		*	�������� ����   1 ��������� �� DB
		*/
		if (!is_file($_SERVER['DOCUMENT_ROOT'].$data['path2file'])) {
			$data['about'] = TPLS_FILE_NOT_FOUND;			
		}else
			$data['about'] = str_replace("\r\n","\n",implode('',file($_SERVER['DOCUMENT_ROOT'].$data['path2file'])));

		return $data;
	}

	/**
	*
	*/
	function del( $alias)
	{
		global $_TPLS, $_CORE;
		// ���� ����
		if ($_CORE->IS_DEMO || !empty($_TPLS[$_TPLS['KUR_ALIAS']]['PERM']['ud'])) return true;

		$data	= array();
		if (empty($alias))
			return false;
		$where	= " alias = '$alias' ";
		$res	= SQL::del( $_TPLS['KUR_TABLE'], $where.$add_where, 0);
		if ($res->Result) {
			return true;
		}else {
			return false;
		}
	}

	/**
	*
	*/
	function show_form( $data )
	{
		global $_CORE, $form, $_TPLS;		
		$form	= $data;
		$_CORE->dll_load('class.cForm');
		global $FORM_DATA;
		$_TPLS['cForm']	= new cForm( $FORM_DATA );
		if (is_file($_CORE->SiteModDir."/tpls.form.inc.php"))
			include $_CORE->SiteModDir."/tpls.form.inc.php";
		else
			@include $_CORE->CoreModDir."/_tpls.form.inc.php";
	}

	/**
	*
	*/
	function check_form( &$form, $path )
	{
		global $_CORE, $_TPLS;
		# ��������� ����������� �������
		if (empty($_TPLS['cForm'])) {
			global $FORM_DATA;
			$_CORE->dll_load('class.cForm');
			$_TPLS['cForm']	= new cForm( $FORM_DATA );
		}
		#
		#	������������ ��������
		#
//		global $doc0;
//		echo "Check: ".$doc0;
		if ($_TPLS['cForm']->Chk_come()) {										// if BAD
			$_TPLS['ERROR'] = $_TPLS['cForm']->Error;
			for ($i=0;$i<10;$i++) {
				global ${'doc'.$i};
				${'doc'.$i}	= '';
			}
			return false;
		}
		$form['path'] = $path; // STR::translit($form['title'], 4);
//		global $doc0;
//		echo "Check: ".$doc0;
		return true;
	}

	/**
	*
	*/
	function save( &$form )
	{
		global $_TPLS, $_CORE, $FORM_DATA,
			$FORM_FIELD4ALIAS,  // ���� ���������� � ������������� ����� �������� �� "name"
			$FORM_FIELDLONG;    // ������� ���� ������������ �� ������ ����� ��� ����������� alias
		// ���� ����
		if ($_CORE->IS_DEMO) return true;

//		global $doc0;
//		echo "Save: ".$doc0;

		$long = (empty($FORM_FIELDLONG)) ? 8 : $FORM_FIELDLONG;

		echo $form['about'];
		$insert	= '';
		$tr	= array( "'" => '"' );
		foreach ($form as $key => $value) {
			if ($key != "about")
				$form[$key]	= stripslashes(strtr($value,$tr));
			else
				$form[$key]	= stripslashes($value);
		}
		echo $form['about'];
//		$form['alias']	= strtr(STR::translit($form['name'], 4),$tr);
//			? preg_replace('/[^a-z0-9_]/','', STR::translit($form['name'], 5))

		$pkey	= (empty($_TPLS['INDEX'][$_TPLS['KUR_ALIAS']])) ? 'id' : $_TPLS['INDEX'][$_TPLS['KUR_ALIAS']];

		if ($pkey != 'alias') {
			$form['alias']	= (empty($FORM_FIELD4ALIAS)) 
				? STR::translit($form['name'], $long , $long , true)
				: STR::translit($form[$FORM_FIELD4ALIAS], $FORM_FIELDLONG,$long,true);
		}

		/* 
		* � ���� ��� �� ������  3 ������� �� DB
		*/
		$content = $form['about'];
		echo  $content;
//		exit;
		$form['about'] = "��. ���� ��������������� ���� � ���� path2file.";

		// ��������� �� ���� ��� ��������� ������������
		$unsave = !empty($form['savefile']);
		unset($_TPLS['cForm']->Arr['savefile']);
		// ---

		if (!empty($form[$pkey]) && empty($_TPLS[$_TPLS['KUR_ALIAS']]['PERM']['ue'])) {
			$set	= $_TPLS['cForm']->GetSql('update');
			echo $_TPLS['cForm']->Error;
//			echo $set;
			$res	= SQL::upd( $_TPLS['KUR_TABLE'], $set, "$pkey = '".$form[$pkey]."'", 1);
			echo $res->ErrorQuery;
		}else {
			unset($form['id']);
		}
		#
		# ���� �� �������������, ������ �������
		if (!@$res->Result && empty($_TPLS[$_TPLS['KUR_ALIAS']]['PERM']['ua'])) {
			$_TPLS['ERROR']	.= '����������.';
			list($var, $val) = $_TPLS['cForm']->GetSql('insert');
			$res	= SQL::ins( $_TPLS['KUR_TABLE'], $var, $val, '', '');
		}else
			$_TPLS['ERROR']	.= '����������.';

		/*
		*  ���������� ����� 2-� ������� �� DB
		*/ 
		if (!$unsave) {
			$fp = fopen($_SERVER['DOCUMENT_ROOT'].$form['path2file'], 'w');
			fputs($fp, $content);
			fclose($fp);
		}
		// ---- 
	
		if ($res->Result) {
			$_TPLS['ERROR']	= '������ ���������';
			return true;
		} else {
			$_TPLS['ERROR']	= $res->ErrorQuery;
			return false;
		}
	}


/********************************************************************/
/********************************************************************/
/********************************************************************/
/********************************************************************/

	function search ( $search, $order='', $limit = '', $page = -1 )
	{
		global $_TPLS, $FORM_SELECT, $FORM_WHERE, $FORM_SELECT_BEFORE, $FORM_FROM, $FORM_DATA;
		$add_sel = (!empty($FORM_SELECT)) ? $FORM_SELECT : '';
		$add_sel_bef = (!empty($FORM_SELECT_BEFORE)) ? $FORM_SELECT_BEFORE : '';

		$page	= ($page < 0 && !empty($_REQUEST['p'])) ? intval($_REQUEST['p']) : max($page,1);
		if (empty($limit))	$limit	= $_TPLS['onpage'];
		else $_TPLS['onpage'] = $limit;
		$sel	= '';
		if (is_array($search)) foreach ($search as $f => $v) {
			if (isset($v)){
				$rel = (empty($FORM_DATA[$f]['search'])) ? "='$v'" : sprintf($FORM_DATA[$f]['search'], $v);
				$sel .= " AND $f".$rel." ";
			}
		}
		$tail	= ($limit >= $_TPLS['onpage']) 
				? SQL::of_li( $_TPLS['onpage'] * ($page - 1), $limit) 
				: '';

		// ������������� ��� ������������ ������� (������� � EFES)
		$add_where	= (!empty($FORM_WHERE)) ? $FORM_WHERE : '';

		$res	= SQL::sel($add_sel_bef.' * '.$add_sel,  $_TPLS['KUR_TABLE'].$FORM_FROM, '1 '.$sel." ".$add_where, $order.$tail, 0);
		if ($res->NumRows > 0) {
			for ($i = 0; $i < $res->NumRows; $i++ ) {
				$res->FetchArray($i);
				$_TPLS['SEARCH_RES'][]	= $res->FetchArray;
			}
			#
			# ���� � ������� ������ ��� ����� � ������ ������ �����, �� ����� ������.
			if ($limit >= $_TPLS['onpage']) {
				$All	= SQL::getval($add_sel_bef.' count(*) ', $_TPLS['KUR_TABLE'].$FORM_FROM, '1 '.$sel." ".$add_where,'',0);
				$_TPLS['all'] = $All;
				if ($All > $res->NumRows)
					$_TPLS['SEARCH_PLINE']	= TPLS::pline($All, $page, $_TPLS['onpage']);
			}
		}
	}
	/**
	*
	*/
	function last( $order ='', $limit = 1, $offset = 1)
	{
		global $_TPLS;
		if (empty($_TPLS['SEARCH_LAST'])) {
			$_TPLS['SEARCH_RES']	= array();
			TPLS::search('', $order, $limit, $offset); // ������ 1 �������� ��� ��������, ����� �� ������� �� QS � ���� �� �� N �������� �������� �� N �������� ������� ����� ������.
			if (sizeof($_TPLS['SEARCH_RES']) > 0) {
				$_TPLS['SEARCH_LAST']	= $_TPLS['SEARCH_RES'][0];
			}
		}
	}
	/**
	*
	*/
	function last_reset()
	{
		global $_TPLS;
		$_TPLS['SEARCH_LAST'] = '';
		$_TPLS['SEARCH_RES'] = '';
	}

	function pline( $all, $p, $onpage)
	{
			global $th, $mount, $year, $_CORE;
			if (intval($onpage) == 0) return '';
			$QS		= $_SERVER['QUERY_STRING'];
			$string = str_replace("&p=$p","",$QS);
			if ($string != '') $string .= '&';									// ��� ���������� ����� /?&p - �� ��������
			$uri	 = $_SERVER['REQUEST_URI'];
			ob_start();

			// �����  (� ��������� ������� ������, ��� �������� ���������� ��������)
			if (is_file($_CORE->SiteModDir."/pline.head.html"))
				include $_CORE->SiteModDir."/pline.head.html";
			elseif (is_file($_CORE->CoreModDir."/_pline.head.html"))
				include $_CORE->CoreModDir."/_pline.head.html";

			$tmp	.= ($_CORE->LANG == 'en') ? 'Pages :' : '��������: ';
			$pages = ceil($all / $onpage);
			$inline = min(10, $pages); // ������� ������ �� ������ �������� � �����
			if ($p < $inline) {
					$from = 1; $to = $from + $inline -1;
			}elseif( $p > $pages - $inline) {
					$from = max($pages - $inline, 1); $to = $pages;
			}else {
					$from   = max(1, $p - floor($inline / 2)); $to  = min($pages, $p + floor($inline / 2))+1;
			}

			if ($from > 1) {
				$dot_mean = $from-1;
				// �����������
				if (is_file($_CORE->SiteModDir."/pline.dot.html"))
					include $_CORE->SiteModDir."/pline.dot.html";
				elseif (is_file($_CORE->CoreModDir."/_pline.dot.html"))
					include $_CORE->CoreModDir."/_pline.dot.html";
				else 
					echo "<a href='$uri?".$string."p=".$dot_mean."'>...</a>";

				// �����������
				if (is_file($_CORE->SiteModDir."/pline.delimeter.html"))
					include $_CORE->SiteModDir."/pline.delimeter.html";
				elseif (is_file($_CORE->CoreModDir."/_pline.delimeter.html"))
					include $_CORE->CoreModDir."/_pline.delimeter.html";
				else 
					echo "&nbsp;|&nbsp;";
			}

			for( $i = $from; $i <= $to; $i++ ) {
					if ($i == $p) {

						// �������  
						if (is_file($_CORE->SiteModDir."/pline.current.html"))
							include $_CORE->SiteModDir."/pline.current.html";
						elseif (is_file($_CORE->CoreModDir."/_pline.current.html"))
							include $_CORE->CoreModDir."/_pline.current.html";
						else
							echo $i;
					}else{

						// ������
						if (is_file($_CORE->SiteModDir."/pline.link.html"))
							include $_CORE->SiteModDir."/pline.link.html";
						elseif (is_file($_CORE->CoreModDir."/_pline.link.html"))
							include $_CORE->CoreModDir."/_pline.link.html";
						else
							echo "<a href='$uri?".$string."p=$i'>".($i)."</a>";

					}
					if ($i < $to || $pages-1 > $to) {
						// �����������
						if (is_file($_CORE->SiteModDir."/pline.delimeter.html"))
							include $_CORE->SiteModDir."/pline.delimeter.html";
						elseif (is_file($_CORE->CoreModDir."/_pline.delimeter.html"))
							include $_CORE->CoreModDir."/_pline.delimeter.html";
						else 
							echo "&nbsp;|&nbsp;";
					}
			}
			if ($pages-1 > $to) {
				$dot_mean = $i;
				// �����������
				if (is_file($_CORE->SiteModDir."/pline.dot.html"))
					include $_CORE->SiteModDir."/pline.dot.html";
				elseif (is_file($_CORE->CoreModDir."/_pline.dot.html"))
					include $_CORE->CoreModDir."/_pline.dot.html";
				else 
					echo "<a href='$uri?".$string."p=".$dot_mean."'>...</a>";
			}

			// �����
			if (is_file($_CORE->SiteModDir."/pline.foot.html"))
				include $_CORE->SiteModDir."/pline.foot.html";
			elseif (is_file($_CORE->CoreModDir."/_pline.foot.html"))
				include $_CORE->CoreModDir."/_pline.foot.html";

			$tmp = ob_get_contents();
			ob_end_clean();
			return $tmp;
	}

	/**
	*
	*/
	function import($type='')
	{
		global $_TPLS,$_CORE, $FORM_DATA, $FORM_IMPORT, $FORM_IMPORT_TYPE, $FORM_FIELD4ALIAS, $FORM_FIELDLONG;

		echo $type;

		// ��� �������: insert/update ������������ ��, ��� �������� � �������, ����� ������ �� *.data.inc.php
		if (empty($type) && !empty($FORM_IMPORT_TYPE)) {
			$type = @$FORM_IMPORT_TYPE;
		}
		
		if (empty($FORM_IMPORT)) {
			$_TPLS['ERROR'] = TPLS_IMPORT_WRONG_SETTINGS;
			return false;
		}

		// ������ �� ����
		if (!is_file($_FILES['import_data']['tmp_name']) || !empty($_FILES['import_data']['error'])) {
			$_TPLS['ERROR'] = TPLS_FILE_NOT_FOUND;
			return false;
		}

		// ���� ��� ���� �������� ��� ������
		$new_file	= $_CORE->SiteModDir.'/f_'.$_TPLS['KUR_ALIAS'].'/im_'.date('d.m.Y.H.i.s').".csv";

		copy($_FILES['import_data']['tmp_name'], $new_file);

		// ����������� �����������
		$darr	= array('',',',';','\t');
		if (!empty($darr[$_POST['import_delimeter']])) {
			$delimeter = $darr[$_POST['import_delimeter']];
		}else {
			$_TPLS['ERROR'] = TPLS_IMPORT_WRONG_DELIMETER;
			return false;
		}
		// ���� ...

//		global $SQL_DBCONF;
//		$db	= new Connection('', $SQL_DBCONF);
//
//		// ************** ������� ������
//		$SQL	= "	LOAD DATA LOCAL INFILE '".$new_file."' 
//					INTO TABLE ".$_TPLS['KUR_TABLE']." 
//					FIELDS TERMINATED BY '".$delimeter."' ESCAPED BY '' LINES TERMINATED BY '\\n'
//					( ".$_TPLS['IMPORT'][$_TPLS['KUR_ALIAS']]." )";
//		$load	= new Query( $db->Connect, $SQL);
//		$good	= $load->Tuples;														// ��������� �������
//		if (!$load->Result) {
//			$_TPLS['ERROR']	= $load->ErrorQuery. "(".$load->ErrorQueryNum.")\n";
//			return false;
//		}
//		2.

		$lines	= file($new_file);
		$good	= $bad	= 0;

		// ������ �� ������ ������
		for ($i = 0; $i < sizeof($lines); $i++ ) if (!empty($lines[$i])) {
			$data	= explode($delimeter, trim($lines[$i]));

			// ���� ��� ���� ������� (������ ������� �� �� ��������� �������� � �� ������� � �������, �.�. ���� select)
			// �� ������� ���� ����������� ������
			foreach ($data as $n => $value) {
				if ($FORM_DATA[$FORM_IMPORT[$n]]['type'] == 'select') {
					// ������ ������
					$new_val	= array_search(trim($value,"� \t"), $FORM_DATA[$FORM_IMPORT[$n]]['arr']);
					// ������� �� ������
					$data[$n]	= $new_val;
					echo "<BR>tmp: $value,".trim($value,"� \t").", $new_val, ".$FORM_IMPORT[$n].",".$data[$n];
				}
			}

			// ������� ������ ������ ��� ������ ������, 
			// ��� �� ���� ����������� �������. ����� �������, ������� � ������� FORM_IMPORT
			if (sizeof($data) < sizeof($FORM_IMPORT)) {
				$data	= array_pad($data, sizeof($FORM_IMPORT), '');
			}
			if (sizeof($data) > sizeof($FORM_IMPORT)) {
				$data = array_slice($data, 0, sizeof($FORM_IMPORT));				
			}

			// ��� �������� ������, ��������� ������ ���������� ��������, � ���������� ����.
			$long = (empty($FORM_FIELDLONG)) ? 8 : $FORM_FIELDLONG;

			if (empty($type) || $type == 'insert') {
				// ������� ������ ��� ����������� INSERT
				$val	= "'".implode("','", $data)."'";
				$var	= "`".implode("`,`", $FORM_IMPORT)."`";
			}elseif ($type == 'update') {
				// ������� ������ ��� ���������� UPDATE
				$set = '';
				for ($j=sizeof($FORM_IMPORT)-1; $j >= 0; $j --) {
					$set .= "`".$FORM_IMPORT[$j]."` = '".$data[$j]."',";
				}
				$set = rtrim($set, ",");
			}


			// ���� ��������� ������ �� ��������, �� ������ �� ������ ���� � ������� �����.
			if (empty($_TPLS['INDEX']['autoinc_'.$_TPLS['KUR_ALIAS']])) {
				$pkey	= (empty($_TPLS['INDEX'][$_TPLS['KUR_ALIAS']])) ? $FORM_IMPORT[0] : $_TPLS['INDEX'][$_TPLS['KUR_ALIAS']];
				if ($pkey != 'alias') {
					$pkey_val	= (empty($FORM_FIELD4ALIAS)) ? array_search('name', $FORM_IMPORT) : array_search($FORM_FIELD4ALIAS, $FORM_IMPORT);
					$data_add	= STR::translit($data[$pkey_val],$long, $long, true);
				}

				if (empty($type) || $type == 'insert') {
					$var .= ", `alias`";
					$val .= ", '".$data_add."'";
				}elseif ($type == 'update') {
					$where = " `alias` = '".$data_add."'";
				}
			}

//			echo "<br>$i".$var." | <B>".$val."</B>"; 
//			echo  $set." - ".$where."=".$type;

			if (empty($type) || $type == 'insert') {
				$res	= SQL::ins($_TPLS['KUR_TABLE'], $var, $val, 0);
			}elseif ($type == 'update') {
				$res	= SQL::upd($_TPLS['KUR_TABLE'], $set, $where, 0);
			}

			if ($res->Tuples > 0) {
				$good+=$res->Tuples;
			}else {
				echo $res->ErrorQuery."<br>";
				$bad++;
			}
		}
		unlink($new_file);
		return array($good, $bad);
	}

	/**
	*
	*/
	function export($die = true)
	{
		global $_TPLS, $FORM_IMPORT;
		$var	= "`".implode("`,`", $FORM_IMPORT)."`";
		$res	= SQL::sel( $var, $_TPLS['KUR_TABLE'] );

		if ($res->NumRows > 0){
			for ($i=0;$i < $res->NumRows; $i++ ) {
				$res->FetchRow($i);
				$str .= implode(";",$res->FetchRow)."\n";
			}
		}

		if ($die) {
			ob_end_clean();
			header("Content-Type: application/ms-excel");
			die($str);
		} else {
			echo $str;
		}
	}

	/**
	*
	*/
	function truncate()
	{
		global $SQL_DBCONF, $_TPLS;
		$db	= new Connection('', $SQL_DBCONF);

		// ************** ������� ������
		$SQL	= "TRUNCATE TABLE ".$_TPLS['KUR_TABLE'];
		$trun	= new Query( $db->Connect, $SQL);
		$good	= $trun->Tuples;														// ��������� �������
		if (!$trun->Result) {
			$_TPLS['ERROR']	= $trun->ErrorQuery. "(".$trun->ErrorQueryNum.")\n";
			return false;
		}else {
			return true;
		}
	}

	/**
	*
	*/
	function scan_dirs( $dir )
	{
		global $_TPLS;
		if (is_dir($dir)) {
			$res = FILE::read_dir($dir);
			foreach ($res as $val) {
				if (is_dir($dir."/".$val)) {
					if (!empty($_TPLS['skip_dir'][$dir."/".$val])) continue;
					TPLS::scan_dirs($dir."/".$val);
				}elseif (strstr($val, ".html")) {
					$_TPLS['html'][] = str_replace($_SREVER['DOCUMENT_ROOT'],"/",$dir)."/".$val;
				}
			}
		}
		else
			echo "file";
		
	}

}

/**
*
*/
function tpls_get_file_name($str='')
{
	global $_TPLS;
	return $_TPLS['KUR_ALIAS'].'_'.$str."_".basename($_SERVER['REQUEST_URI'])."_".date("s");
}

/**
*
*/
function tpls_get_small_file_name()
{
	global $_TPLS;
	return $_TPLS['KUR_ALIAS'].'_'.basename($_SERVER['REQUEST_URI']).'_s';
}


?>