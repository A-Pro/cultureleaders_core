<?php
global $_CORE, $_CONF;

include_once $_CONF['CorePath'].'/lib/class.Modules.php';

/**
 * Сlass Basketcrm 
 *
 */
 

class Basketcrm extends Modules {
	
	
	/**
	 * Конструктор класса
	 * 
	 */
	public function __construct()
	{
		parent::__construct('basketcrm');
	}
	
	
	
	/**
	 * Получаем данные для вывода
	 * 
	 */
	public function get_data($alias)
	{
		$email = $this->_sql->getval('email',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."basket","alias = '$alias'");
		if(empty($email)) return false;
		$user_id = $this->_sql->getval('author_id',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."auth_pers","author_login = '$email'");
		if(empty($user_id)) return false;
		$user_orders = $this->_sql->getall('*',(defined('DB_TABLE_PREFIX') ? DB_TABLE_PREFIX : '')."basket","email = '$email'");
		if(empty($user_orders)) return false;
		
		return array('alias' => $alias, 'user_id' => $user_id, 'user_orders' => $user_orders);
	}
}