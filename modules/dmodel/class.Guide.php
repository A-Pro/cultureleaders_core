<?php
	include_once $_CORE->CoreModDir.'/class.Error.php';		
	
	
	class tGuide extends tError{
		var $path;
		var $pre_path;
		var $tree;
		var $title;
		var $skin;

		function tGuide( $skin, $path, $old_path )
		{
			$this->path 	= 	!empty($path)?		$path:		$this->setError( "\$path is empty" );
			$this->skin 	= 	!empty($skin)?		$skin:		$this->setError( "\$skin is empty" );
			$this->pre_path	= 	!empty($old_path)?	$old_path:	$this->setError( "\$old_path is empty" );
		}
		

		function showMap( $module_name )
		{	
			$skin = basename($this->skin);
			$path = $this->path;
			if (Main::comm_inc('guide.head.html', $f, $module_name ))include "$f";
			if( empty( $this->tree ))  $this->setError( "tree is empty (showMap)" );
			else {
				if( is_array( $this->tree ))
				foreach( $this->tree as $file=>$v ){
					$name = basename( $file );
					$title = $this->title[$file];
					$is_into = $v;
					$date = date( "H:i (d.m)" , $this->mtime( $file."/") );
					$skin = basename($this->skin);
					if (Main::comm_inc('guide.item.html', $f, $module_name ))include "$f";
				}else $this->setError( $path." - \"������\" ���� (showMap)" );
			}
			if (Main::comm_inc('guide.foot.html', $f, $module_name ))include "$f";
		}

		
		
		function loadList( $white_list )
		{	
            global $_DMODEL;
			$dir = @opendir( $this->pre_path.$this->skin.$this->path  ); 
			if( !$dir ) {$this->setError($this->pre_path.$this->skin.$this->path." - ������������ ���� (loadList)");}
			else {
				while( ($file=readdir($dir))!==false )
				{
//							if(( $file=='.' )||( $file==".." )||( $file=="CVS" )||( $file=="cvs" )) continue;
							if( in_array( $file, $_DMODEL['NO_DIRS']) ) continue;
							
							if( is_dir( $this->pre_path.$this->skin.$this->path.$file ) )
							{ 	
								$this->title[$this->path.$file."/"] = $this->loadTitle( $this->path.$file."/", $white_list );
								$this->tree[$this->path.$file."/"] = $this->isInto( $this->path.$file."/" );
							}
			}	closedir($dir);	}
			
		}
		
		function loadTitle( $path, $white_list )
		{	
			$dir = @opendir( $this->pre_path.$this->skin.$path ); 
			if( !$dir ) {$this->setError( $this->pre_path.$this->skin.$path." - ������������ ����(loadTitle)");}
			else {
				while( ($file=readdir($dir))!==false )
				{			
							if(( $file=='.' )||( $file==".." )||is_dir($file)) continue;
							$ext = pathinfo(  $this->pre_path.$this->skin.$path.$file);
							if( is_file($this->pre_path.$this->skin.$path.$file)&&( in_array($ext['extension'], $white_list )))	$title .= $file.";";
				}
			closedir($dir);
			}
			return $title;
		}
		
		function isInto( $path )
		{	
            global $_DMODEL;
			$dir = @opendir( $this->pre_path.$this->skin.$path );
			if( !$dir ) {$this->setError( $this->pre_path.$this->skin.$path." - ������������ ����(isInto)");}
			
			else{	while( ($file=readdir($dir))!==false )
					{	
//						if(( $file=='.' )||( $file==".." )||( $file=="CVS" )) continue;
						if( in_array( $file, $_DMODEL['NO_DIRS']) ) continue;
						if( is_dir($this->pre_path.$this->skin.$path.$file) ) return true;
					}
					closedir($dir);
			}
			return false;
		}
		
		function mtime( $path )
		{	
            global $_DMODEL;
            $time = @filemtime( $this->pre_path.$this->skin.$path );
			if( is_dir( $this->pre_path.$this->skin.$path )){
					$dir = @opendir( $this->pre_path.$this->skin.$path );
				
					while( ($file=readdir($dir))!==false )
						{	
//							if(( $file=='.' )||( $file==".." )||( $file=="CVS" )) continue;
							if( in_array( $file, $_DMODEL['NO_DIRS']) ) continue;
							$tmp_time = $this->mtime( $path.$file."/" );
							if( $tmp_time  >  $time )  $time = $tmp_time;
						}
					closedir($dir);
			
			}
			return $time;
		}

	}
?>