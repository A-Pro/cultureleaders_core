<?
	GLOBAL $_DMODEL, $_CORE;

	include_once $_CORE->CoreModDir.'/class.Error.php';		
	include_once $_CORE->CoreModDir.'/class.Event.php';		
	include_once $_CORE->CoreModDir.'/class.Editor.php';		
	include_once $_CORE->CoreModDir.'/class.PicEditor.php';		
	include_once $_CORE->CoreModDir.'/class.Guide.php';		
	
	$err = new tError;
	$ev  = new tEvent;	
	
	$_DMODEL['DEF_SKIN'] = 'def';
	$_DMODEL['DEFAULT_SKIN'] = 'default';
	$_DMODEL['DEF_SKIN_NAME'] = '�������';
	$_DMODEL['PATHTPLS'] = $_CORE->PATHTPLS;
	$_DMODEL['PATHTPLS_DEF'] = 	$_DMODEL['PATHTPLS']."/".$_DMODEL['DEF_SKIN'];
	$_DMODEL['PATHTPLS_DEFAULT'] = 	$_DMODEL['PATHTPLS']."/".$_DMODEL['DEFAULT_SKIN'];
	$_DMODEL['FORM_NAME'] = 'dataForm';
	$_DMODEL['IMAGES_DIR'] = 'images';

	$_DMODEL['MODULE_NAME'] = "dmodel";
	$_DMODEL['MODULE_DESC'] = "�������������� �������� ������ � ������ �������.";
	$_DMODEL['MODULE_TITLE'] = "������ �������";

	$_DMODEL['ADM_MENU']	= array(
		"/dmodel/" => array("/ico/a_tree.gif", $_DMODEL['MODULE_TITLE'], $_DMODEL['MODULE_NAME']),
		);
	
	
	$_DMODEL['WHITE_LIST'][] = 'php';
	$_DMODEL['WHITE_LIST'][] = 'html';
	$_DMODEL['WHITE_LIST'][] = 'phtml';
	$_DMODEL['WHITE_LIST'][] = 'css';
	$_DMODEL['WHITE_LIST'][] = 'js';
	
	$_DMODEL['NO_DIRS'][] = 'cvs';
	$_DMODEL['NO_DIRS'][] = 'CVS';
	$_DMODEL['NO_DIRS'][] = '.svn';
	$_DMODEL['NO_DIRS'][] = '.SVN';
	$_DMODEL['NO_DIRS'][] = '_svn';
	$_DMODEL['NO_DIRS'][] = '_SVN';
	$_DMODEL['NO_DIRS'][] = 'admin';
	$_DMODEL['NO_DIRS'][] = $_DMODEL['DEFAULT_SKIN'];
	$_DMODEL['NO_DIRS'][] = '.';
	$_DMODEL['NO_DIRS'][] = '..';

	$_DMODEL['WHITE_LIST_IMG'][] = 'gif';
	$_DMODEL['WHITE_LIST_IMG'][] = 'jpeg';
	$_DMODEL['WHITE_LIST_IMG'][] = 'jpg';
	$_DMODEL['WHITE_LIST_IMG'][] = 'png';
	$_DMODEL['WHITE_LIST_IMG'][] = 'bmp';
	
	class tDmodel extends tError{
		
		function createDir( $path )
		{	
			$tmp = explode( "/", $path );
			if( empty( $tmp )) $this->setError( $path." - ����������� ���� (createDir)" );
			else
			foreach( $tmp as $v )
				if( $v != '' )
				{	$node .= $v.'/';
					if( !is_dir( $node )) mkdir( $node );
				}
		}

		
		function getSkinList( $tmpl_path )
		{	global $_DMODEL;
			$dir = @opendir( $tmpl_path ); 
			if( !$dir ) {$this->setError( $tmpl_path." - ������������ ���� (getSkinList)" );}
			else{
					while( ($file=readdir($dir))!==false )
					{		if( in_array( $file, $_DMODEL['NO_DIRS']) ) continue;
							if( is_dir( $tmpl_path.$file ))
								$tmp[$file] = $file;
					}
					closedir( $dir );
			}
			return $tmp;
		}

		function delDir( $path )
		{	$path .= "/";
			$dir = @opendir( $path ); 
			if( !$dir ) {$this->setError( $path." - ������������ ���� (getSkinList)" );}
			else{
					while( ($file=readdir($dir))!==false )
					{		if(( $file=='.' )||( $file==".." )) continue;
							if( is_dir( $path.$file ))  tDmodel::delDir( $path.$file );
							else unlink( $path.$file );
					}
			closedir( $dir );
			rmdir( $path );
			}
		}
	
	}	
?>