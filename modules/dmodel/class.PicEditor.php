<?php
	include_once $_CORE->CoreModDir.'/class.Error.php';		
	
	
	class tPic extends tError{
		var $path;
		var $skin;
		var $pre_path;

		function tPic( $skin, $pic_path, $old_path )
		{
			$this->path 	= 	!empty($pic_path)?	"/".$pic_path:	$this->setError( "\$path is empty" );
			$this->skin 	= 	!empty($skin)?		$skin:		$this->setError( "\$skin is empty" );
			$this->pre_path	= 	!empty($old_path)?	$old_path."/":	$this->setError( "\$old_path is empty" );
		}
		

		function sList( $white_list, $module_name )
		{	
            global $_DMODEL;
			$skin = $this->skin;
			$dir = @opendir( $this->pre_path.$this->skin.$this->path  ); 
			if( !$dir ) {$this->setError($this->pre_path.$this->skin.$this->path." - ������������ ���� (tPic::sList)");}
			else {
				if (Main::comm_inc('pic.head.html', $f, $module_name ))include "$f";
				while( ($file=readdir($dir))!==false )
				{			
//                            if(( $file=='.' )||( $file==".." )||( $file=="CVS" )||( $file=="cvs" )) continue;
                            if( in_array( $file, $_DMODEL['NO_DIRS']) )continue;
				
							$ext = pathinfo(  $this->pre_path.$this->skin.$this->path.$file);
							if(  in_array($ext['extension'], $white_list )){
								$date = date( "H:i (d.m)" , $this->mtime( $file) );
								if (Main::comm_inc('pic.item.html', $f, $module_name ))include "$f";
							}
				}						
				if (Main::comm_inc('pic.foot.html', $f, $module_name ))include "$f";
				closedir($dir);
		}	}

		function delete( $file )
		{
			unlink( $this->pre_path.$this->skin.$this->path."/".$file );
		}

		function copyImg( $proto_file )
		{
			if( is_file($proto_file) ) copy( $proto_file, $this->pre_path."/".$this->skin."/".$this->path."/".basename($proto_file) );
			else $this->setError( "\$proto_file it is not file(copyImg)" );
		}
		
		function mtime( $file )
		{	$time = filemtime( $this->pre_path."/".$this->skin."/".$this->path."/".$file );
			return $time;
		}

	}
?>