<?
global $SQL_DBCONF, $_CORE, $_BAN;
if (!$_CORE->dll_load('class.DBCQ'))
	die ($_CORE->error_msg());


$_BAN['MODULE_NAME']	= 'bannet';
$_BAN['MODULE_TITLE']	= '������';
$_BAN['MODULE_DESC']	= '������ �������������� ������� �����.';
include $_CORE->PATHMODS.$_BAN['MODULE_NAME'].'/conf.inc.php';
if (is_file($_CORE->SiteModDir.'/conf.inc.php'))
	include_once($_CORE->SiteModDir.'/conf.inc.php');


if (!defined('KAT_FILE_NOT_FOUND')) define ("KAT_FILE_NOT_FOUND","���� �� ������.");
if (!defined('KAT_IMPORT_WRONG_DELIMETER')) define ("KAT_IMPORT_WRONG_DELIMETER","�������� ����������� (�� �� ������).");
if (!defined('KAT_IMPORT_WRONG_SETTINGS')) define ("KAT_IMPORT_WRONG_SETTINGS","�� �������� �������� �������, ��� ������� ��������.");
if (!defined('BAN_LOOKIG_DATA_DIR')) define ("BAN_LOOKIG_DATA_DIR",'dir');

class BAN {

	function get_cont( &$alias, $field = 'cont', $any = 0)
	{
		global $_BAN, $_CORE, $form, $FORM_DATA, $_PROJECT;
		$form	= BAN::load( $alias, $any);

		$_PROJECT['loaded'][] = array( 'parent' => "/".$_BAN['MODULE_NAME']."/".$_BAN['KUR_ALIAS']."/", 'data' => $form );
		$_PROJECT['loaded']['last'] = array( 'parent' => "/".$_BAN['MODULE_NAME']."/".$_BAN['KUR_ALIAS']."/", 'data' => $form );

		if (is_array($form)) {
			if (!empty($form['cont']) &&  !empty($form['doc']) ){
				$form['cont'] = str_replace('[doc]', $form['doc'], $form['cont']);
			}
			if (BAN::inc_tpl('view.item.html', $file)) {
				include "$file";
			}else {
				if (empty($_BAN['cForm'])) {
					$_CORE->dll_load('class.cForm');
					$_BAN['cForm']	= new cForm($FORM_DATA);
				}
				unset($_BAN['cForm']->Arr['alias']);
				unset($_BAN['cForm']->Arr['id']);
				unset($_BAN['cForm']->Arr['cont']);
				echo $_BAN['cForm']->Print_come();
			}
			return $form;
		}
	}

	/**
	*
	*/
	function load( $alias, $any = 0 )
	{
		
		global $_BAN, $FORM_SELECT, $FORM_WHERE, $FORM_FROM;
		$add_sel = (!empty($FORM_SELECT)) ? $FORM_SELECT : '';
		// ������������� ��� ������������ ������� (������� � EFES)
		$add_where	= (!empty($FORM_WHERE)) ? $FORM_WHERE : '';
		$limit = (!empty($any)) ? " ORDER BY RAND() LIMIT 1 " : "";
		$add_where .= (!empty($any)) ? " AND active = 1 " : "";

		$data	= array();
		if (empty($any) && empty($alias))
			return false;
		$where	= ($alias != 'any' && empty($any)) ? " AND alias = '$alias' " : '';
		
		$res	= SQL::sel(' * '.$add_sel,  $_BAN['KUR_TABLE'].$FORM_FROM, ' 1 '.$where.$add_where, $limit, DEBUG);
		if ($res->NumRows > 0) {
			$res->FetchArray(0);
			$data	= $res->FetchArray;
			$res->Destroy();
		}
		/*
		* ��� ������������� ������ 
		*/
		global $doc;
		global $big_doc;
		$doc	= @$data['doc'];
		$big_doc	= @$data['big_doc'];
		for ($i=0;$i<10;$i++) {
			global ${'doc'.$i},${'big_doc'.$i};
			${'doc'.$i}	= @$data['doc'.$i];
			${'big_doc'.$i}	= @$data['big_doc'.$i];
		}
		return $data;
	}

	/**
	*
	*/
	function del( $alias)
	{
		global $_BAN, $_CORE;
		// ���� ����
		if ($_CORE->IS_DEMO || !empty($_BAN[$_BAN['KUR_ALIAS']]['PERM']['ud'])) return true;

		$data	= array();
		if (empty($alias))
			return false;
		$where	= " alias = '$alias' ";
		$res	= SQL::del( $_BAN['KUR_TABLE'], $where.$add_where, 0);
		if ($res->Result) {
			return true;
		}else {
			return false;
		}
	}


/**
 * ����� ���������� ������� $data
 * 
 * ���������� �������: form.inc.phtml, ������������ ��� ������ KAT::inc_tpl().
 * ��������������� array $form � ����� ���� ����������� � �������.
 * 
 * 
 * @uses $_CORE
 * @uses $FORM_DATA
 * @uses KAT::inc_tpl()
 * @uses $_CORE->dll_load()
 * @param array $data
 */
	function show_form( $data )
	{
		global $_CORE, $form, $_BAN;		
		$form	= $data;

		$_CORE->dll_load('class.cForm');
		global $FORM_DATA;
		$_BAN['cForm']	= new cForm( $FORM_DATA );

		/* 
		*
		*   ��� ������������� ��������� ���� ($_BAN['alias']['wall'] ���� ���������� �� �������)
		*
		*/
		if (!empty($_BAN[$_BAN['KUR_ALIAS']]['wall'])) {
			if (!$_CORE->dll_load('class.lProtect'))
				die ($_CORE->error_msg());
			$_BAN[$_BAN['KUR_ALIAS']]['wall_obj'] = new lProtect('/antispam/show');
		}

//		include $_CORE->SiteModDir."/kat.form.inc.php";
		//  ������ ������
		if (BAN::inc_tpl('form.inc.phtml', $file)) {
			include "$file";
		}else {
			Echo "<B>No file:</B> form.inc.phtml";
		}
	}


/**
 * �������� �� ����������� (������������ $FORM_DATA) ������ $form
 *  
 * ���������� true, ���� ������ ��������, ����� false
 * ���� �������� ������ ������, ��������� $form['path']  = $path
 * ���������� ����� cForm, ������������ ��� ������ ���������� $_CORE->dll_load('class.cForm');
 * 
 * 
 * @uses $FORM_DATA
 * @uses $_BAN['cForm']->Chk_come()
 * @param array $form
 * @param string $path
 * @return bool
 */
	function check_form( &$form, $path='' )
	{
		global $_CORE, $_BAN;
		# ��������� ����������� �������
		if (empty($_BAN['cForm'])) {
			global $FORM_DATA;
			$_CORE->dll_load('class.cForm');
			$_BAN['cForm']	= new cForm( $FORM_DATA );
		}

		/* 
		*
		*   ��� ������������� ��������� ���� ($_KAT['alias']['wall'] ���� ���������� �� �������)
		*
		*/
		if (!empty($_BAN[$_BAN['KUR_ALIAS']]['wall'])) {
			if (!$_CORE->dll_load('class.lProtect'))
				die ($_CORE->error_msg());
			$_BAN[$_BAN['KUR_ALIAS']]['wall_obj'] = new lProtect('/antispam/show');
		}

		#
		#	������������ ��������
		#
		// ������� "�����" ���, ���� �����������
		if (empty($_BAN[$_BAN['KUR_ALIAS']]['wall']) || $_BAN[$_BAN['KUR_ALIAS']]['wall_obj']->check()>0) {
			// ������ ���� �����
			if ($_BAN['cForm']->Chk_come()) {								// if BAD
				$_BAN['ERROR'] = $_BAN['cForm']->Error;
				ob_clean();
//				echo "<script>window.alert('".addslashes($set)."');</script>";
//				exit();
				for ($i=0;$i<10;$i++) {
					global ${'doc'.$i};
					${'doc'.$i}	= '';
				}
				return false;
			}
		} else {
			$_BAN['ERROR'] = $_BAN[$_BAN['KUR_ALIAS']]['wall_obj']->err_msg;
			return false;
		}
		$form['path'] = $path; // STR::translit($form['title'], 4);
		return true;
	}	


    /**
	*
	*/
	function save( &$form )
	{
		global $_BAN, $_CORE,
			$FORM_FIELD4ALIAS,  // ���� ���������� � ������������� ����� �������� �� "name"
			$FORM_FIELDLONG;    // ������� ���� ������������ �� ������ ����� ��� ����������� alias
		// ���� ����
		if ($_CORE->IS_DEMO) return true;

		$long = (empty($FORM_FIELDLONG)) ? 8 : $FORM_FIELDLONG;

		$insert	= '';
		foreach ($form as $key => $value) {
			$tr	= array( "'" => '"' );
			$form[$key]	= stripslashes(strtr($value,$tr));
		}
//		$form['alias']	= strtr(STR::translit($form['name'], 4),$tr);
//			? preg_replace('/[^a-z0-9_]/','', STR::translit($form['name'], 5))

		$pkey	= (empty($_BAN['INDEX'][$_BAN['KUR_ALIAS']])) ? 'id' : $_BAN['INDEX'][$_BAN['KUR_ALIAS']];
		
		if ($pkey != 'alias') {
			$form['alias']	= (empty($FORM_FIELD4ALIAS)) 
				? STR::translit($form['name'], $long , $long , true)
				: STR::translit($form[$FORM_FIELD4ALIAS], $FORM_FIELDLONG,$long,true);
		}
		if (!empty($form[$pkey]) && empty($_BAN[$_BAN['KUR_ALIAS']]['PERM']['ue'])) {
			$set	= $_BAN['cForm']->GetSql('update');
			echo $_BAN['cForm']->Error;
			$res	= SQL::upd( $_BAN['KUR_TABLE'], $set, "$pkey = '".$form[$pkey]."'", 0);
		}else {
			unset($form['id']);
		}

		#
		# ���� �� �������������, ������ �������
		if (!@$res->Result && empty($_BAN[$_BAN['KUR_ALIAS']]['PERM']['ua'])) {
			$_BAN['ERROR']	.= '����������.';
			list($var, $val) = $_BAN['cForm']->GetSql('insert');
			
			$res	= SQL::ins( $_BAN['KUR_TABLE'], $var, $val);
		}else
			$_BAN['ERROR']	.= '����������.';
	
		if ($res->Result) {
			$_BAN['ERROR']	= '������ ���������';
			return true;
		} else {
			$_BAN['ERROR']	= $res->ErrorQuery;
			return false;
		}
	}


/********************************************************************/
/********************************************************************/
/********************************************************************/
/********************************************************************/

	function search ( $search, $order='', $limit = '', $page = -1 )
	{
		global $_BAN, $FORM_SELECT, $FORM_WHERE, $FORM_SELECT_BEFORE, $FORM_FROM, $FORM_DATA, $_CORE;
		$add_sel = (!empty($FORM_SELECT)) ? $FORM_SELECT : '';
		$add_sel_bef = (!empty($FORM_SELECT_BEFORE)) ? $FORM_SELECT_BEFORE : '';

		$page	= ($page < 0 && !empty($_REQUEST['p'])) ? intval($_REQUEST['p']) : max($page,1);
		if (empty($limit))	$limit	= $_BAN['onpage'];
		else $_BAN['onpage'] = $limit;
		$sel	= '';
		if (is_array($search)) foreach ($search as $f => $v) {
			if (!empty($v) && is_array($FORM_DATA[$f])){
				$rel = (empty($FORM_DATA[$f]['search'])) ? "='$v'" : sprintf($FORM_DATA[$f]['search'], $v);
				$sel .= " AND $f".$rel." ";
			}
		}
		$tail	= ($limit >= $_BAN['onpage']) 
				? SQL::of_li( $_BAN['onpage'] * ($page - 1), $limit) 
				: '';

		// ������������� ��� ������������ ������� (������� � EFES)
		$add_where	= (!empty($FORM_WHERE)) ? $FORM_WHERE : '';
    if(!$_CORE->IS_ADMIN){
      $add_where .= " AND active = 1";
    }

		$res	= SQL::sel($add_sel_bef.' * '.$add_sel,  $_BAN['KUR_TABLE'].$FORM_FROM, '1 '.$sel." ".$add_where, $order.$tail, 0);
		if ($res->NumRows > 0) {
			for ($i = 0; $i < $res->NumRows; $i++ ) {
				$res->FetchArray($i);
				$_BAN['SEARCH_RES'][]	= $res->FetchArray;
			}
			#
			# ���� � ������� ������ ��� ����� � ������ ������ �����, �� ����� ������.
			if ($limit >= $_BAN['onpage']) {
				$All	= SQL::getval($add_sel_bef.' count(*) ', $_BAN['KUR_TABLE'].$FORM_FROM, '1 '.$sel." ".$add_where,'',0);
				if ($All > $res->NumRows)
					$_BAN['SEARCH_PLINE']	= BAN::pline($All, $page, $_BAN['onpage']);
			}
		}
	}
	/**
	*
	*/
	function last( $order ='', $limit = 1, $offset = 1)
	{
		global $_BAN;
		if (empty($_BAN['SEARCH_LAST'])) {
			$_BAN['SEARCH_RES']	= array();
			BAN::search('', $order, $limit, $offset); // ������ 1 �������� ��� ��������, ����� �� ������� �� QS � ���� �� �� N �������� �������� �� N �������� ������� ����� ������.
			if (sizeof($_BAN['SEARCH_RES']) > 0) {
				$_BAN['SEARCH_LAST']	= $_BAN['SEARCH_RES'][0];
			}
		}
	}
	/**
	*
	*/
	function last_reset()
	{
		global $_BAN;
		$_BAN['SEARCH_LAST'] = '';
		$_BAN['SEARCH_RES'] = '';
	}

	function pline( $all, $p, $onpage)
	{
			global $th, $mount, $year;
			if (intval($onpage) == 0) return '';
			$QS		= $_SERVER['QUERY_STRING'];
			$string = str_replace("&p=$p","",$QS);
			if ($string != '') $string .= '&';									// ��� ���������� ����� /?&p - �� ��������
			$uri	 = $_SERVER['REQUEST_URI'];
			$tmp	= '
				<TABLE border="0" cellpadding="0" cellspacing="0">
				<TR bgcolor="F0F0F0">
					<TD height="18" width="28"></TD>
					<TD></TD>
					<TD valign="middle">';
			$tmp	.= ($_CORE->LANG == 'en') ? 'Pages :' : '��������: ';
			$pages = ceil($all / $onpage);
			$inline = min(10, $pages); // ������� ������ �� ������ �������� � �����
			if ($p < $inline) {
					$from = 1; $to = $from + $inline -1;
			}elseif( $p > $pages - $inline) {
					$from = max($pages - $inline, 1); $to = $pages;
			}else {
					$from   = max(1, $p - floor($inline / 2)); $to  = min($pages, $p + floor($inline / 2))+1;
			}

			if ($from > 1) $tmp .= "<a href='$uri?".$string."p=".($from-1)."'>...</a>&nbsp;|&nbsp;";
			for( $i = $from; $i <= $to; $i++ ) {
					$tmp .= ($i == $p) ? "<span class='nlink'>".($i)."</span>" : "<a href='$uri?".$string."p=$i'>".($i)."</a>";
					$tmp .= "&nbsp;|&nbsp;";
			}
			if ($pages-1 > $to) $tmp .= "<a href='$uri?".$string."p=$i' >...</a>&nbsp;&nbsp;";
			$tmp	.= '</TD>
					<TD width="28">&nbsp;</TD>
				</TR>
				</TABLE>
			';
			return $tmp;
	}

	/**
	*
	*/
	function import()
	{
		global $_BAN,$_CORE, $FORM_DATA, $FORM_IMPORT, $FORM_FIELD4ALIAS, $FORM_FIELDLONG;
		if (empty($FORM_IMPORT)) {
			$_BAN['ERROR'] = KAT_IMPORT_WRONG_SETTINGS;
			return false;
		}
		if (!is_file($_FILES['import_data']['tmp_name']) || !empty($_FILES['import_data']['error'])) {
			$_BAN['ERROR'] = KAT_FILE_NOT_FOUND;
			return false;
		}
		$new_file	= $_CORE->SiteModDir.'/f_'.$_BAN['KUR_ALIAS'].'/import_'.date('d.m.Y.s').".csv";
		copy($_FILES['import_data']['tmp_name'], $new_file);
		$darr	= array('',',',';','\t');
		$darr[$_POST['import_delimeter']];
		if (!empty($darr[$_POST['import_delimeter']])) {
			$delimeter = $darr[$_POST['import_delimeter']];
		}else {
			$_BAN['ERROR'] = KAT_IMPORT_WRONG_DELIMETER;
			return false;
		}
		// ���� ...

//		global $SQL_DBCONF;
//		$db	= new Connection('', $SQL_DBCONF);
//
//		// ************** ������� ������
//		$SQL	= "	LOAD DATA LOCAL INFILE '".$new_file."' 
//					INTO TABLE ".$_BAN['KUR_TABLE']." 
//					FIELDS TERMINATED BY '".$delimeter."' ESCAPED BY '' LINES TERMINATED BY '\\n'
//					( ".$_BAN['IMPORT'][$_BAN['KUR_ALIAS']]." )";
//		$load	= new Query( $db->Connect, $SQL);
//		$good	= $load->Tuples;														// ��������� �������
//		if (!$load->Result) {
//			$_BAN['ERROR']	= $load->ErrorQuery. "(".$load->ErrorQueryNum.")\n";
//			return false;
//		}

//		2.
		$lines	= file($new_file);
		$good	= $bad	= 0;
		for ($i = 0; $i < sizeof($lines); $i++ ) if (!empty($lines[$i])) {
			$data	= explode($delimeter, trim($lines[$i]));
			// ���� ��� ���� �������
			foreach ($data as $n => $value) {
				if ($FORM_DATA[$FORM_IMPORT[$n]]['type'] == 'select') {
					// ������ ������
					$new_val	= array_search(trim($value,"� \t"), $FORM_DATA[$FORM_IMPORT[$n]]['arr']);
					// ������� �� ������
					$data[$n]	= $new_val;
//					echo "<BR>tmp: $value,".trim($value,"� \t").", $new_val, ".$FORM_IMPORT[$n].",".$data[$n];
				}
			}
			// �������� ���� ��� ������ � ����
			if (sizeof($data) < sizeof($FORM_IMPORT)) {
				$data	= array_pad($data, sizeof($FORM_IMPORT), '');
			}
			if (sizeof($data) > sizeof($FORM_IMPORT)) {
				$data = array_slice($data, 0, sizeof($FORM_IMPORT));				
			}

			$long = (empty($FORM_FIELDLONG)) ? 8 : $FORM_FIELDLONG;

			$val	= "'".implode("','", $data)."'";
			$var	= "`".implode("`,`", $FORM_IMPORT)."`";

			// ������� ����� 
			if (empty($_BAN['INDEX']['autoinc_'.$_BAN['KUR_ALIAS']])) {
				$pkey	= (empty($_BAN['INDEX'][$_BAN['KUR_ALIAS']])) ? $FORM_IMPORT[0] : $_BAN['INDEX'][$_BAN['KUR_ALIAS']];
				if ($pkey != 'alias') {
					$pkey_val	= (empty($FORM_FIELD4ALIAS)) ? array_search('name', $FORM_IMPORT) : array_search($FORM_FIELD4ALIAS, $FORM_IMPORT);
					$data_add	= STR::translit($data[$pkey_val],$long, $long, true);
				}
				$var .= ", `alias`";
				$val .= ", '".$data_add."'";
			}

//			echo "<br>$i".$var." | <B>".$val."</B>"; 
			$res	= SQL::ins($_BAN['KUR_TABLE'], $var, $val, 0);
			if ($res->Result) {
				$good++;
			}else {
				echo $res->ErrorQuery."<br>";
				$bad++;
			}
		}
		unlink($new_file);
		return $good;
	}

	/**
	*
	*/
	function truncate()
	{
		global $SQL_DBCONF, $_BAN;
		$db	= new Connection('', $SQL_DBCONF);

		// ************** ������� ������
		$SQL	= "TRUNCATE TABLE ".$_BAN['KUR_TABLE'];
		$trun	= new Query( $db->Connect, $SQL);
		$good	= $trun->Tuples;														// ��������� �������
		if (!$trun->Result) {
			$_BAN['ERROR']	= $trun->ErrorQuery. "(".$trun->ErrorQueryNum.")\n";
			return false;
		}else {
			return true;
		}
	}
	/**
	*
	*/
	function get_file_name($str='')
	{
		global $_BAN;
		return $_BAN['KUR_ALIAS'].'_'.$str."_".basename($_SERVER['REQUEST_URI'])."_".date("s");
	}


	/**
	*
	*/
	function inc_data( $alias, $what='view')
	{
		global $_BAN;
		switch ($what) {
			case "view":$set = " views = views + 1 ";break;
			case "click":$set = " clicks = clicks + 1 ";break;
			default: return;
		}
		$res	= SQL::upd( $_BAN['KUR_TABLE'], $set, "alias = '$alias'");
	}

/**
 * �������� �� ������������� �����-������� � ������ $tpl
 *
 * ��� �������� ���������� ��������� ������:
 * <code>
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$_BAN['KUR_ALIAS']."/$tpl",
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$_BAN['KUR_ALIAS'].'.'.$tpl,
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$tpl,
 *$_CORE->CORE_PATHTPLS.'/'.$_BAN['MODULE_NAME'].'/_'.$tpl,
 * </code>
 * � ������� �� ������������ ������-��������, ��� ��� � ���� �� ���� ����������� � $file. � � ���������� ������� ������������ true, ����� false.
 * ������ ������������� (������ db, ������� /list/ ):
 * <code>
 *  if (BAN::inc_tpl('db.list.foot.html', $file))
 *            include "$file";
 * </code>
 * 
 * @uses FILE::chk_files()
 * @uses $_CORE->PATHTPLS
 * @uses $_CORE->CURR_TPL
 * @uses $_CORE->CORE_PATHTPLS
 * $_BAN['MODULE_NAME']
 * $_BAN['KUR_ALIAS']
 * @param string $tpl
 * @param string $file
 * @return string
 */
	function inc_tpl($tpl, &$file)
	{
		global $_CORE, $_BAN;
			$files	=  array( 
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$_BAN['KUR_ALIAS']."/$tpl", // �� ����� � ������� �������, ���������� ������, ���������� ������
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$_BAN['KUR_ALIAS'].'.'.$tpl, // �� ����� � ������� �������, ���������� ������, ����� ������
				$_CORE->CORE_PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$_BAN['KUR_ALIAS'].'/_'.$tpl, // � ����, ���������� ������ �������, ���������� ������ � "_"
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$tpl, // 23.08.2011 (.slk �������� ����) �� ����� � ������� �������, ���������� ������, ������ ����
				$_CORE->CORE_PATHTPLS.'/'.$_BAN['MODULE_NAME'].'/_'.$tpl, // � ����, ���������� ������, ����� � "_" � ��������
				// ������ � ��������
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$_BAN['SRC'][$_BAN['KUR_ALIAS']].'/'.$tpl, // ������������ ������ �� �����
				$_CORE->CORE_PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$_BAN['SRC'][$_BAN['KUR_ALIAS']].'/'.$tpl, // ������������ ������ � ����
				
				// ���������� ������
				$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$_BAN['MODULE_NAME'].'/'.$tpl, // �� �����, ������ �� ��������� ����� ������
				$_CORE->CORE_PATHTPLS.'/'.$_BAN['MODULE_NAME'].'/'.$_BAN['MODULE_NAME'].'/_'.$tpl, // � ����, ������ �� ��������� ����� ������
			);

		$tmp = FILE::chk_files($files, $file);
		return $tmp;
	}

/**
 * �������� �� ������������� �����-������� $tpl.
 * 
 * ��� �������� ���������� ��������� ��������:
 * <code>
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$_BAN['KUR_ALIAS']."/$tpl",
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$_BAN['KUR_ALIAS'].'.'.$tpl,
 *$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$tpl,
 * </code>
 * � ������� �� ������������ ������, ��� ��� � ���� ����������� � $file, � ��� ���� � ���� $_CORE->PATHTPLS �������� �� $_CORE->TEMPLATES.
 * � � ���������� ������� ������������ $file.
 * 
 * @uses FILE::chk_files()
 * @uses $_CORE->PATHTPLS
 * @uses $_CORE->CURR_TPL
 * @uses $_CORE->TEMPLATES
 * $_BAN['MODULE_NAME']
 * $_BAN['KUR_ALIAS']
 * @param string $tpl
 * @param string $file
 * @return string
 */
	function inc_link($tpl, &$file)
	{
		global $_CORE, $_BAN;
		$files	=  array( 
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$_BAN['KUR_ALIAS']."/$tpl",
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$_BAN['KUR_ALIAS'].'.'.$tpl,
			$_CORE->PATHTPLS.'/'.$_CORE->CURR_TPL.'/'.$_BAN['MODULE_NAME'].'/'.$tpl,
			$_CORE->PATHTPLS.'/admin/'.$_BAN['MODULE_NAME'].'/'.$tpl,
		);
		$tmp = $file = str_replace($_CORE->PATHTPLS, $_CORE->TEMPLATES, FILE::chk_files($files, $file));
		return $tmp;
	}

	/**
	 * Lookig for data LINK, need after idea to keep all data separete
	 *
	 * @param string $looking
	 * @param string $found
	 * @param const $look4dir
	 * @return string LINK PATH to the source or empty
	 */
	function get_data_link($looking, &$found, $look4dir = true){
		global $_CORE, $_BAN;
		$files	=  array( 
			$_CORE->SiteModDir.'/'.$looking,
			$_CORE->ROOT.$_CORE->DATA_PATH.$_BAN['MODULE_NAME'].'/'.$looking,
			$_CORE->SiteModDir.'/_'.$looking,
		);
//		$tmp = $found = str_replace($_CORE->PATHTPLS, $_CORE->TEMPLATES, ($look4dir == BAN_LOOKIG_DATA_DIR) ? FILE::chk_dirs($files, $looking) : FILE::chk_files($files, $looking));
		$tmp = $found = str_replace('//','/',str_replace($_CORE->ROOT, '/', ($look4dir == BAN_LOOKIG_DATA_DIR) ? FILE::chk_dirs($files, $looking) : FILE::chk_files($files, $looking)));
		return $tmp;		
	}
	
	/**
	 * Lookig for data PATH, need after idea to keep all data separete
	 *
	 * @param string $looking
	 * @param string $found
	 * @param const $look4dir
	 * @return string FULL PATH to the source of empty
	 */
	function get_data_path($looking, &$found, $look4dir = BAN_LOOKIG_DATA_DIR){
		global $_CORE, $_BAN;
		$files	=  array( 
			$_CORE->SiteModDir.'/'.$looking,
			$_CORE->ROOT.$_CORE->DATA_PATH.$_BAN['MODULE_NAME'].'/'.$looking,
			$_CORE->SiteModDir.'/_'.$looking,
		);
		
		$tmp = $found = ($look4dir == BAN_LOOKIG_DATA_DIR) ? FILE::chk_dirs($files, $looking) : FILE::chk_files($files, $looking);
		return $tmp;		
	}
    




}
?>