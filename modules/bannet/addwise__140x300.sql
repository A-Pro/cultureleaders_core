-- phpMyAdmin SQL Dump
-- version 3.4.3.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 03 2011 г., 22:05
-- Версия сервера: 5.1.40
-- Версия PHP: 5.2.12

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `addwise`
--

-- --------------------------------------------------------

--
-- Структура таблицы `addwise__140x300`
--

CREATE TABLE IF NOT EXISTS `addwise__140x300` (
  `alias` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` varchar(3) DEFAULT NULL,
  `doc` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `cont` text NOT NULL,
  `ts` date DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `clicks` bigint(20) NOT NULL,
  `views` bigint(20) NOT NULL,
  PRIMARY KEY (`alias`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=4 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
